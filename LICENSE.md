The EUPL for SCHOOLABY


Copyright © 2022 Net Group OÜ (www.netgroup.com)

Schoolaby (www.schoolaby.com) is a web-based software platform, a Learning Management System (LMS) for the administration, documentation, tracking, reporting, automation, and delivery of educational courses. Schoolaby brings all teaching materials together on to the study materials marketplace, allowing uberization of digital study content. Schoolaby features personalized study paths for students based on their skills, and study history.



Schoolaby program is free software: you can redistribute it and/or modify it under the terms of the European Union Public License as published by the European Commission, either version 1.2 or later. By using Schoolaby software, you are agreeing to be bound by the terms of the license.



Schoolaby software platform is using some third-party openly licensed components and they are referred below:

For fonts in Schoolaby UI: Copyright © 2018 Caroline Hadilaksono & Micah Rich  SIL Open Front License

For webserver: Copyright © 2004 Apache (www.apache.org) v.2.0 License

For delivering high-performance Javascript: Copyright © 2021 by Form.io, Lodash Modified MIT License

For improving usability and design D3 library: Copyright © 2022 Mike Bostock ISC License

For editing Javascript Markdown easyMDE library: Copyright © 2017 Jeroen Akkerman MIT License

For camera usage Uppy library: Copyright © 2019 Transloadit MIT License

For configuration of front-end compilation: Copyright © 2007 Free Software Foundation, Inc, GNU Lesser General Public License



This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the EUPL for more details.

You should have received a copy of the EUPL v. 1.2 along with the program. If not, see https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12

 