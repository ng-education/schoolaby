const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const MergeJsonWebpackPlugin = require('merge-jsons-webpack-plugin');
const utils = require('./utils.js');

const getTsLoaderRule = env => {
  const rules = [
    {
      loader: 'ts-loader',
      options: {
        transpileOnly: true,
      },
    },
  ];
  if (env === 'development') {
    rules.unshift({
      loader: 'react-hot-loader/webpack',
    });
  }
  return rules;
};

module.exports = options => ({
  cache: options.env !== 'production',
  resolve: {
    extensions: ['.js', '.jsx', '.ts', '.tsx', '.json'],
    modules: ['node_modules'],
    symlinks: false,
    cacheWithContext: false,
    alias: utils.mapTypescriptAliasToWebpackAlias(),
    fallback: { buffer: false, path: false },
    //   If you want to include a polyfill, you need to:
    //     - add a fallback 'resolve.fallback: { "buffer": require.resolve("buffer/") }'
    // - install 'buffer'
    //   If you want to include a polyfill, you need to:
    //     - add a fallback 'resolve.fallback: { "path": require.resolve("path-browserify") }'
    // - install 'path-browserify'
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: getTsLoaderRule(options.env),
        include: [utils.root('./src/main/webapp/app')],
        exclude: [utils.root('node_modules'), utils.root('./testcafe')],
      },
      {
        test: /\.(jpe?g|png|gif|svg|woff2?|ttf|eot)$/i,
        loader: 'file-loader',
        options: {
          digest: 'hex',
          hash: 'sha512',
          name: 'content/[contenthash].[ext]',
        },
      },
      {
        enforce: 'pre',
        test: /\.jsx?$/,
        loader: 'source-map-loader',
      },
      {
        test: /\.(j|t)sx?$/,
        enforce: 'pre',
        loader: 'eslint-loader',
        exclude: [utils.root('node_modules'), utils.root('./testcafe')],
      },
    ],
  },
  stats: {
    children: false,
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        // NODE_ENV: `'${options.env}'`,
        BUILD_TIMESTAMP: `'${new Date().getTime()}'`,
        VERSION: `'${process.env.hasOwnProperty('APP_VERSION') ? process.env.APP_VERSION : 'DEV'}'`,
        DEBUG_INFO_ENABLED: options.env === 'development',
        SERVER_API_URL: `''`,
      },
    }),
    new ForkTsCheckerWebpackPlugin({
      eslint: {
        files: './src/**/*.{ts,tsx}',
      },
    }),
    new HtmlWebpackPlugin({
      template: './src/main/webapp/index.html',
      inject: 'body',
    }),
    new MergeJsonWebpackPlugin({
      output: {
        groupBy: [
          { pattern: './src/main/webapp/i18n/en/*.json', fileName: './i18n/en.json' },
          { pattern: './src/main/webapp/i18n/et/*.json', fileName: './i18n/et.json' },
          { pattern: './src/main/webapp/i18n/fi/*.json', fileName: './i18n/fi.json' },
          { pattern: './src/main/webapp/i18n/sw/*.json', fileName: './i18n/sw.json' },
          { pattern: './src/main/webapp/i18n/uk/*.json', fileName: './i18n/uk.json' },
        ],
      },
    }),
  ],
});
