import axios from 'axios';
import { ICrudPutAction, loadMoreDataWhenScrolled, parseHeaderForLinks } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { FAILURE, REQUEST, SUCCESS } from 'app/shared/reducers/action-type.util';

import { defaultValue, IAssignment } from 'app/shared/model/assignment.model';
import { Material } from 'app/shared/model/material.model';

export const ACTION_TYPES = {
  FETCH_ASSIGNMENT_LIST: 'assignment/FETCH_ASSIGNMENT_LIST',
  FETCH_ASSIGNMENT: 'assignment/FETCH_ASSIGNMENT',
  CREATE_ASSIGNMENT: 'assignment/CREATE_ASSIGNMENT',
  UPDATE_ASSIGNMENT: 'assignment/UPDATE_ASSIGNMENT',
  DELETE_ASSIGNMENT: 'assignment/DELETE_ASSIGNMENT',
  RESET: 'assignment/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IAssignment>,
  entity: defaultValue,
  links: { next: 0 },
  updating: false,
  totalItems: 0,
  updateSuccess: false,
};

export type AssignmentState = Readonly<typeof initialState>;

// Reducer

export default (state: AssignmentState = initialState, action): AssignmentState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_ASSIGNMENT_LIST):
    case REQUEST(ACTION_TYPES.FETCH_ASSIGNMENT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_ASSIGNMENT):
    case REQUEST(ACTION_TYPES.UPDATE_ASSIGNMENT):
    case REQUEST(ACTION_TYPES.DELETE_ASSIGNMENT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_ASSIGNMENT_LIST):
    case FAILURE(ACTION_TYPES.FETCH_ASSIGNMENT):
    case FAILURE(ACTION_TYPES.CREATE_ASSIGNMENT):
    case FAILURE(ACTION_TYPES.UPDATE_ASSIGNMENT):
    case FAILURE(ACTION_TYPES.DELETE_ASSIGNMENT):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_ASSIGNMENT_LIST): {
      const links = parseHeaderForLinks(action.payload.headers.link);

      return {
        ...state,
        loading: false,
        links,
        entities: loadMoreDataWhenScrolled(state.entities, action.payload.data, links),
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
      };
    }
    case SUCCESS(ACTION_TYPES.FETCH_ASSIGNMENT): {
      const entity = action.payload.data;
      entity.materials = entity.materials?.map(Material.fromJson);
      return {
        ...state,
        loading: false,
        entity,
      };
    }
    case SUCCESS(ACTION_TYPES.CREATE_ASSIGNMENT):
    case SUCCESS(ACTION_TYPES.UPDATE_ASSIGNMENT): {
      const entity = action.payload.data;
      entity.materials = entity.materials?.map(Material.fromJson);
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity,
      };
    }
    case SUCCESS(ACTION_TYPES.DELETE_ASSIGNMENT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/assignments';

// Actions

export const createEntity: ICrudPutAction<IAssignment> = entity => async dispatch => {
  return await dispatch({
    type: ACTION_TYPES.CREATE_ASSIGNMENT,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
};

export const updateEntity: ICrudPutAction<IAssignment> = entity => async dispatch => {
  return await dispatch({
    type: ACTION_TYPES.UPDATE_ASSIGNMENT,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
