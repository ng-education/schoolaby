import React, { useContext, useEffect, useState } from 'react';
import './student-solution.scss';
import ContentCard from 'app/shared/layout/content-card/content-card';
import { useSelectedSubmissionState, useSelectedUserState } from 'app/shared/contexts/entity-detail-context';
import { translate } from 'react-jhipster';
import LtiResults from 'app/assignment/assignment-detail/students/lti-results/lti-results';
import SubmissionDetails from 'app/assignment/assignment-detail/students/submission-details/submission-details';
import StudentUploads from 'app/assignment/assignment-detail/students/student-uploads/student-uploads';
import { useGetLtiResources, useGetLtiScores } from 'app/shared/services/lti-api';
import { AssignmentContext } from 'app/journey/journey-detail/journey-detail-new';
import { ILtiScore } from 'app/shared/model/lti-score.model';
import { subscribeToLtiScores, unsubscribeFromLtiScores } from 'app/config/websocket-middleware';

const StudentSolution = () => {
  const { assignment } = useContext(AssignmentContext);
  const { selectedUser } = useSelectedUserState();
  const { selectedSubmission } = useSelectedSubmissionState();
  const [currentScores, setCurrentScores] = useState<ILtiScore[]>([]);
  const [socketMessage, setSocketMessage] = useState<ILtiScore>();
  const { data: ltiResources } = useGetLtiResources({ assignmentId: assignment.id }, !!assignment?.id);
  const { data: ltiScores } = useGetLtiScores(
    {
      assignmentId: assignment.id,
      userId: selectedUser?.id,
    },
    !!assignment?.id && !!ltiResources?.length && !!selectedUser?.id
  );

  const handleSocketMessage = (score: ILtiScore) => {
    setSocketMessage(score);
  };

  const isScoreResourceInAssignmentResources = (score: ILtiScore) => {
    return ltiResources.map(resource => resource.id).includes(score.resourceId);
  };

  const addScore = (score: ILtiScore) => {
    if (!isScoreResourceInAssignmentResources(score)) {
      return;
    }

    const newScores = [...currentScores];
    let existingPendingScoreIndex = null;
    newScores.forEach((currentScore, index) => {
      if (!currentScore.score && currentScore.resourceId === score.resourceId) {
        existingPendingScoreIndex = index;
      }
    });
    existingPendingScoreIndex !== null && newScores.splice(existingPendingScoreIndex, 1);
    setCurrentScores([score, ...newScores]);
  };

  useEffect(() => {
    subscribeToLtiScores(handleSocketMessage);

    return () => {
      unsubscribeFromLtiScores();
    };
  }, []);

  useEffect(() => !!ltiScores && setCurrentScores(ltiScores), [ltiScores]);

  useEffect(() => {
    if (socketMessage) {
      addScore(socketMessage);
    }
  }, [socketMessage]);

  const isSelectedSolutionEmpty = () => {
    return !selectedSubmission?.value?.length && !selectedSubmission?.uploadedFiles?.length && !ltiScores?.length;
  };

  const NoSubmission = () => (
    <ContentCard className="no-submission d-flex flex-column align-items-center">
      <img src="content/images/solution_not_submitted_new.svg" alt={translate('schoolabyApp.submission.noSubmission')} />
      <h3 className="my-5">{translate('schoolabyApp.submission.noSubmission')}</h3>
    </ContentCard>
  );

  const SolutionContent = () => {
    return selectedSubmission && !isSelectedSolutionEmpty() ? (
      <>
        <SubmissionDetails />
        <StudentUploads />
        <LtiResults scores={ltiScores} />
      </>
    ) : (
      <NoSubmission />
    );
  };

  const SelectStudent = () => (
    <ContentCard className="d-flex flex-column select-student align-items-center">
      <img alt={'No student selected'} src="content/images/select_student_for_feedback.svg" className="img-fluid" />
      <h3 className={'my-5'}>{translate('schoolabyApp.assignment.detail.selectStudent')}</h3>
    </ContentCard>
  );

  return (
    <div className="student-solution d-flex flex-column gap-2 pb-4" data-testid="student-solution">
      {selectedUser ? <SolutionContent /> : <SelectStudent />}
    </div>
  );
};

export default StudentSolution;
