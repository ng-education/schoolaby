import React, { FC, useState } from 'react';
import { ILtiScore } from 'app/shared/model/lti-score.model';
import { Row, Col, Tooltip } from 'reactstrap';
import { convertAppDateTimeFromServer, orderDateTimes } from 'app/shared/util/date-utils';
import { translate, TextFormat } from 'react-jhipster';
import { APP_LOCAL_DATE_TIME_FORMAT } from 'app/config/constants';

interface LtiResultsItemProps {
  ltiScore: ILtiScore;
  scoreIndex: number;
}

const LtiResultsItem: FC<LtiResultsItemProps> = ({ ltiScore, scoreIndex }) => {
  const [tooltipOpen, setTooltipOpen] = useState(false);
  const toggleTooltip = () => setTooltipOpen(!tooltipOpen);

  const convertLtiResultToPercentage = (result: string, resultMax: string): string => {
    if (!result) {
      return translate('schoolabyApp.assignment.lti.result.pending');
    } else if (resultMax) {
      return `${result} / ${resultMax}`;
    }
    return `${+result * 100}%`;
  };

  return (
    <Row noGutters className="p-2 lti-results-item">
      <Col xs={4}>{scoreIndex === 0 && ltiScore.title}</Col>
      <Col xs={2}>{convertLtiResultToPercentage(ltiScore.score, ltiScore.scoreMax)}</Col>
      <Col xs={4}>
        <TextFormat type={'date'} value={ltiScore.createdDate} format={APP_LOCAL_DATE_TIME_FORMAT} />
      </Col>
      <Col xs={2}>
        <span id={`launches-${ltiScore.resourceId.toString() + scoreIndex}`}>{ltiScore.launches?.length}</span>
      </Col>
      <Tooltip
        placement="bottom"
        isOpen={tooltipOpen}
        target={`launches-${ltiScore.resourceId.toString() + scoreIndex}`}
        boundariesElement={'window'}
        toggle={toggleTooltip}
      >
        <ul className={'block-list'}>
          {orderDateTimes(ltiScore.launches, 'desc').map(launch => (
            <li key={ltiScore.resourceId + launch}>{convertAppDateTimeFromServer(launch)}</li>
          ))}
        </ul>
      </Tooltip>
    </Row>
  );
};

export default LtiResultsItem;
