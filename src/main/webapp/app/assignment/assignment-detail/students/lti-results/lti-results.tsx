import React, { FC } from 'react';
import './lti-results.scss';
import groupBy from 'lodash/groupBy';
import { Col, Row } from 'reactstrap';
import { translate } from 'react-jhipster';
import { ILtiScore } from 'app/shared/model/lti-score.model';
import map from 'lodash/map';
import LtiResultsItem from 'app/assignment/assignment-detail/students/lti-results/lti-results-item';
import ContentCard from 'app/shared/layout/content-card/content-card';

interface LtiResultsProps {
  scores: ILtiScore[];
}

const LtiResults: FC<LtiResultsProps> = ({ scores }) => {
  const getScoresGroupedByResourceId = () => {
    return groupBy(scores, 'resourceId');
  };

  return scores?.length ? (
    <ContentCard className="lti-results">
      <h6 className="card-title ml-2">{translate('schoolabyApp.submission.testResults')}</h6>
      <Row className="p-2 lti-results-heading" noGutters>
        <Col xs={4} className="border-0 bg-transparent px-0 text-uppercase">
          {translate('schoolabyApp.submission.submittedLTI.asset')}
        </Col>
        <Col xs={2} className="border-0 bg-transparent px-0 text-uppercase">
          {translate('schoolabyApp.submission.submittedLTI.score')}
        </Col>
        <Col xs={4} className="border-0 bg-transparent px-0 text-uppercase">
          {translate('schoolabyApp.submission.submittedLTI.scoreDate')}
        </Col>
        <Col xs={2} className="border-0 bg-transparent px-0 text-uppercase">
          {translate('schoolabyApp.submission.submittedLTI.launches')}
        </Col>
      </Row>
      {map(getScoresGroupedByResourceId(), resourceScores =>
        resourceScores.map((ltiScore, scoreIndex) => (
          <LtiResultsItem key={ltiScore.resourceId + ltiScore.createdDate} ltiScore={ltiScore} scoreIndex={scoreIndex} />
        ))
      )}
    </ContentCard>
  ) : null;
};

export default LtiResults;
