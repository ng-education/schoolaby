import React, { FC, useContext } from 'react';
import './feedback-history.scss';
import { ISubmissionFeedback } from 'app/shared/model/submission/submission-feedback.model';
import { APP_LOCAL_DATE_TIME_FORMAT } from 'app/config/constants';
import { TextFormat, translate } from 'react-jhipster';
import ContentCard from 'app/shared/layout/content-card/content-card';
import { getGradingScore } from 'app/shared/util/grade-util';
import { useGetGradingScheme } from 'app/shared/services/grading-scheme-api';
import { AssignmentContext } from 'app/journey/journey-detail/journey-detail-new';
import FileGrid from 'app/shared/layout/file-grid/file-grid';
import { UploadedFileProvider } from 'app/shared/contexts/uploaded-file-context';
import { useGetSubmissionFeedback } from 'app/shared/services/submission-feedback-api';
import ReactMarkdown from 'react-markdown';
import RubricPoints from 'app/assignment/assessment-rubric-new/rubric-points/rubric-points';
import { getGradingContainerClassNameByGradingType } from 'app/assignment/assignment-detail/submission/shared/grade-select-util';
import { ISubmission } from 'app/shared/model/submission/submission.model';

interface FeedbackHistoryProps {
  submission: ISubmission;
  studentId: number;
}

const FeedbackHistory: FC<FeedbackHistoryProps> = ({ submission, studentId }) => {
  const { assignment, isAllowedToModify, rubric } = useContext(AssignmentContext);
  const { data: feedbacks } = useGetSubmissionFeedback(
    {
      submissionId: submission?.id,
      studentId,
    },
    !!studentId && !!submission?.id
  );
  const { data: gradingScheme } = useGetGradingScheme(assignment?.gradingSchemeId, !!assignment?.gradingSchemeId);

  const GradeContainer = ({ feedback }) =>
    !gradingScheme?.isNonGradable() ? (
      <span className={`grade ${getGradingContainerClassNameByGradingType(gradingScheme?.code, feedback?.grade)} grade-container`}>
        {translate('schoolabyApp.assignment.detail.grade')} {getGradingScore(gradingScheme, feedback)}
      </span>
    ) : null;

  const FeedbackCard = ({ feedback, withTitle }: { feedback: ISubmissionFeedback; withTitle: boolean }) => (
    <ContentCard className="feedback-card">
      {withTitle && (
        <div className="d-flex align-items-center justify-content-between mb-2">
          <h6 className={'card-title mb-0'}>{translate('schoolabyApp.submission.teachersFeedback')}</h6>
          <GradeContainer feedback={feedback} />
        </div>
      )}
      <div className="d-flex align-items-center justify-content-between">
        <span className="feedback-date pr-1">
          {translate('schoolabyApp.submission.feedbackGiven')}{' '}
          <TextFormat type="date" value={feedback.feedbackDate} format={APP_LOCAL_DATE_TIME_FORMAT} />
        </span>
        {!withTitle && <GradeContainer feedback={feedback} />}
      </div>
      {!!feedback?.selectedCriterionLevels && (
        <RubricPoints rubric={rubric} selectedCriterionLevels={feedback.selectedCriterionLevels} className="mt-3" />
      )}
      <div className="feedback-content my-3">
        <ReactMarkdown
          source={feedback?.value}
          renderers={{
            code: ({ value }) => <span>{value}</span>,
          }}
        />
      </div>
      <div>
        <UploadedFileProvider>
          <FileGrid files={feedback.uploadedFiles} />
        </UploadedFileProvider>
      </div>
    </ContentCard>
  );

  return (
    <div className="feedback-history d-flex flex-column gap-2">
      {feedbacks?.map((feedback, i) => (
        <FeedbackCard key={`feedback-${feedback.feedbackDate}`} feedback={feedback} withTitle={!i && !isAllowedToModify} />
      ))}
    </div>
  );
};

export default FeedbackHistory;
