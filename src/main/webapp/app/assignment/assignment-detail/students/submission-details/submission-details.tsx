import React from 'react';
import './submission-details.scss';
import { getFullName } from 'app/shared/util/string-utils';
import ReactMarkdown from 'react-markdown';
import { useSelectedSubmissionState, useSelectedUserState } from 'app/shared/contexts/entity-detail-context';
import { translate } from 'react-jhipster';
import ContentCard from 'app/shared/layout/content-card/content-card';

const SubmissionDetails = () => {
  const { selectedUser } = useSelectedUserState();
  const { selectedSubmission } = useSelectedSubmissionState();

  const getStudentSubmissionTranslation = () => {
    const fullName = getFullName(selectedUser);
    return fullName.toLowerCase().endsWith('s')
      ? translate('schoolabyApp.submission.submissionInfo.studentNameSubmissionEndingWithS', { fullName })
      : translate('schoolabyApp.submission.submissionInfo.studentNameSubmission', { fullName });
  };

  return (
    <ContentCard className="submission-details">
      <h6 className="card-title ml-2">{getStudentSubmissionTranslation()}</h6>
      <ReactMarkdown
        source={selectedSubmission.value}
        renderers={{
          code: ({ value: codeValue }) => <div>{codeValue}</div>,
        }}
        className="p-2"
      />
    </ContentCard>
  );
};

export default SubmissionDetails;
