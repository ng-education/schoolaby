import './feedback.scss';
import React, { FC, useEffect, useContext, useState } from 'react';
import { Button, Col, Label, Row } from 'reactstrap';
import Icon from 'app/shared/icons';
import { Input, MarkdownEditorInput } from 'app/shared/form';
import { Field, Form } from 'react-final-form';
import { useSelectedUserState } from 'app/shared/contexts/entity-detail-context';
import { translate } from 'react-jhipster';
import FooterButton from 'app/shared/layout/buttons/footer-button';
import { getFullName } from 'app/shared/util/string-utils';
import { useCreateSubmissionFeedback } from 'app/shared/services/submission-feedback-api';
import FileUploadNew from 'app/shared/form/file-upload-new';
import { IUploadedFile } from 'app/shared/model/uploaded-file.model';
import { FileUploadProvider } from 'app/shared/form/file-upload-context';
import { ISubmission } from 'app/shared/model/submission/submission.model';
import { convertDateTimeToServer } from 'app/shared/util/date-utils';
import { ISubmissionGradingPatch } from 'app/shared/model/submission/submission-grading-patch.model';
import { getSubmissionsByAssignmentId } from 'app/shared/services/submission-api';
import { IRootState } from 'app/shared/reducers';
import { hasAnyAuthority } from 'app/shared/auth/private-route';
import { AUTHORITIES } from 'app/config/constants';
import { connect } from 'react-redux';
import { DateTime } from 'luxon';
import { IAssignment } from 'app/shared/model/assignment.model';
import { useFeedbackFilesState, useSubmittedSubmissionsState } from 'app/shared/contexts/submission-grading-context';
import { getGradeText, getInputMaxLength, restrictCharacters } from 'app/assignment/assignment-detail/submission/shared/grade-select-util';
import { useGetGradingScheme } from 'app/shared/services/grading-scheme-api';
import { NARRATIVE, NONE, PASS_FAIL } from 'app/shared/model/grading-scheme.model';
import ContentCard from 'app/shared/layout/content-card/content-card';
import { useDeleteUploadedFile } from 'app/shared/services/uploaded-file-api';
import RubricOverview from 'app/assignment/assessment-rubric-new/rubric-overview/rubric-overview';
import { useSelectedRubricState } from 'app/shared/contexts/entity-update-context';
import RubricViewModes from 'app/assignment/assessment-rubric-new/rubric-overview/rubric-view-modes';
import { ISelectedCritreionLevel } from 'app/shared/model/rubric/selected-critreion-level.model';
import RubricPoints from 'app/assignment/assessment-rubric-new/rubric-points/rubric-points';
import { AssignmentContext } from 'app/journey/journey-detail/journey-detail-new';

interface IFeedback extends StateProps {
  assignment: IAssignment;
}

const Feedback: FC<IFeedback> = ({ isAllowedToModify, assignment }) => {
  const deleteUploadedFile = useDeleteUploadedFile();
  const { rubric } = useContext(AssignmentContext);
  const { selectedUser } = useSelectedUserState();
  const { setSubmittedSubmissions } = useSubmittedSubmissionsState();
  const { selectedRubric, setSelectedRubric } = useSelectedRubricState();
  const [selectedCriterionLevels, setSelectedCriterionLevels] = useState<ISelectedCritreionLevel[]>([]);
  const { mutateAsync: createSubmissionFeedback } = useCreateSubmissionFeedback();
  const { feedbackFiles, setFeedbackFiles } = useFeedbackFilesState();
  const { data: gradingScheme } = useGetGradingScheme(assignment.gradingSchemeId, !!assignment?.gradingSchemeId);
  const [openRubricOverview, setOpenRubricOverview] = useState<boolean>(false);
  const [resetUppyOn, setResetUppyOn] = useState<boolean>(false);

  useEffect(() => {
    !selectedRubric && setOpenRubricOverview(false);
  }, [selectedRubric]);

  const resetUppy = () => setResetUppyOn(prevState => !prevState);

  const onFileUpload = (file: IUploadedFile) => {
    setFeedbackFiles(previousState => [...previousState, file]);
  };

  const onFileDelete = (file: IUploadedFile) => {
    deleteUploadedFile.mutateAsync(file.id);
    setFeedbackFiles(previousState => previousState.filter(prevFile => prevFile.id !== file.id));
  };

  const submit = async values => {
    const feedbackDate = convertDateTimeToServer(DateTime.local().toString());

    const entity = {
      resubmittable: values.resubmittable,
      feedbackFiles,
    };

    const newSubmission: ISubmission = {
      value: '',
      assignmentId: assignment?.id,
      submittedForGradingDate: feedbackDate,
      authors: [selectedUser],
    };

    const feedback: ISubmissionGradingPatch = {
      ...entity,
      validateExistingFeedback: false,
      submittedForGradingDate: feedbackDate,
      submissionFeedback: {
        studentId: selectedUser && selectedUser.id,
        grade: values.grade,
        value: values.value,
        feedbackDate,
        submission: newSubmission,
        selectedCriterionLevels,
      },
      className: '.SubmissionGradingPatchDTO',
    };

    await createSubmissionFeedback(feedback);

    getSubmissionsByAssignmentId(assignment.id).then(res => {
      setSubmittedSubmissions(res);
    });
  };

  useEffect(() => {
    resetUppy();
  }, [selectedUser]);

  const onSubmit = async values => {
    if (!isAllowedToModify) {
      return;
    }
    await submit(values);
    setFeedbackFiles([]);
    setSelectedCriterionLevels([]);
    resetUppy();
  };

  const GradeField = () => {
    switch (gradingScheme?.code) {
      case NARRATIVE:
        return <Field name={'grade'} component={Input} type={'hidden'} value={'PASS'} />;
      case NONE:
        return <Field name={'grade'} component={Input} type={'hidden'} value={'PASS'} />;
      case PASS_FAIL:
        return (
          <>
            <Label>{translate('schoolabyApp.assignment.detail.grade')}</Label>
            <Field
              id={'grade-fail'}
              name={'grade'}
              component={Input}
              type={'radio'}
              value={'FAIL'}
              label={translate('schoolabyApp.gradingSchemeValue.fail')}
              labelAfterInput
              containerClassName={'d-flex align-items-center'}
              labelClassName={'mb-0'}
              className={'mr-1 mt-0'}
            />
            <Field
              id={'grade-pass'}
              name={'grade'}
              component={Input}
              type={'radio'}
              value={'PASS'}
              label={translate('schoolabyApp.gradingSchemeValue.pass')}
              labelAfterInput
              containerClassName={'d-flex align-items-center mb-3'}
              labelClassName={'mb-0'}
              className={'mr-1 mt-0'}
            />
          </>
        );
      default:
        return (
          <Field
            onKeyPress={e => restrictCharacters(e, gradingScheme?.code)}
            maxLength={getInputMaxLength(gradingScheme?.code)}
            placeholder={getGradeText(gradingScheme?.code)}
            label={translate('schoolabyApp.assignment.detail.grade')}
            id={'grade'}
            name={'grade'}
            component={Input}
          />
        );
    }
  };

  return (
    <>
      <ContentCard className="feedback box-container bg-white p-4">
        <Form onSubmit={onSubmit} initialValues={{ resubmittable: false }}>
          {({ handleSubmit, form }) => (
            <form onSubmit={event => handleSubmit(event).then(form.reset)}>
              <Row noGutters>
                <Col className="d-flex mb-3">
                  <h6>{translate('schoolabyApp.assignment.detail.feedbackTitle', { name: getFullName(selectedUser) })}</h6>
                </Col>
                {!!rubric && (
                  <Col>
                    <Button
                      color=""
                      className="float-right no-outline-on-focus p-0"
                      type="button"
                      onClick={() => {
                        setSelectedRubric(rubric);
                        setOpenRubricOverview(true);
                      }}
                    >
                      <Icon name={'penAndRuler'} />
                      <span className={'ml-2'}>{translate('schoolabyApp.assignment.rubric.viewTitle')}</span>
                    </Button>
                  </Col>
                )}
              </Row>
              {!!selectedCriterionLevels?.length && <RubricPoints rubric={rubric} selectedCriterionLevels={selectedCriterionLevels} />}
              <GradeField />
              <Field
                id={'value'}
                name={'value'}
                type={'text'}
                component={MarkdownEditorInput}
                containerClassName={'mb-0'}
                label={translate('schoolabyApp.assignment.detail.feedback')}
              />
              <FileUploadProvider>
                <FileUploadNew
                  videoFileNamePrefix={assignment?.title}
                  existingFiles={feedbackFiles}
                  onUpload={onFileUpload}
                  onDelete={onFileDelete}
                  uploadDisabled={false}
                  maxFiles={10}
                  resetOn={resetUppyOn}
                />
              </FileUploadProvider>
              <Field
                id={'resubmittable'}
                name={'resubmittable'}
                type={'checkbox'}
                component={Input}
                className={'mr-1 mt-0'}
                containerClassName={'d-flex align-items-center mt-4'}
                labelClassName={'mb-0'}
                label={translate('schoolabyApp.submission.submissionInfo.resubmittable')}
                labelAfterInput
              />
              <Row noGutters className={'justify-content-end'}>
                <FooterButton className={'ml-2 mt-2'} type={'confirm'} label={translate('schoolabyApp.assignment.detail.submitGrade')} />
              </Row>
            </form>
          )}
        </Form>
      </ContentCard>
      {!!selectedRubric && openRubricOverview && (
        <RubricOverview
          viewMode={RubricViewModes.GRADE}
          studentFullName={getFullName(selectedUser)}
          selectedLevels={selectedCriterionLevels}
          saveSelectedLevels={setSelectedCriterionLevels}
        />
      )}
    </>
  );
};

const mapStateToProps = ({ authentication }: IRootState) => ({
  isAllowedToModify: hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.TEACHER, AUTHORITIES.ADMIN]),
});

type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(Feedback);
