import React, { FC, useContext, useEffect, useState } from 'react';
import './students-list.scss';
import { translate } from 'react-jhipster';
import { Button, Col, Dropdown, DropdownItem, DropdownMenu, DropdownToggle, Row } from 'reactstrap';
import { IUser } from 'app/shared/model/user.model';
import { useHistory } from 'react-router-dom';
import { useQueryParam } from 'app/shared/hooks/useQueryParam';
import { useSelectedSubmissionState, useSelectedUserState } from 'app/shared/contexts/entity-detail-context';
import { useGetJourneyStudents } from 'app/shared/services/journey-api';
import { getJourneyStudentsUsers } from 'app/shared/util/journey-utils';
import sortBy from 'lodash/sortBy';
import StudentListItem from './students-list-item';
import ContentCard from 'app/shared/layout/content-card/content-card';
import InviteModal from 'app/journey/journey-detail/invite-modal/invite-modal';
import { AssignmentContext } from 'app/journey/journey-detail/journey-detail-new';
import { ROLE } from 'app/config/constants';
import { useGetSubmissions } from 'app/shared/services/submission-api';

type SortBy = 'firstName' | 'lastName';

const StudentsList: FC = () => {
  const { assignment, journey } = useContext(AssignmentContext);
  const history = useHistory();
  const { setSelectedUser } = useSelectedUserState();
  const [{ studentId }] = useQueryParam<{ studentId: string }>();
  const { data: journeyStudents } = useGetJourneyStudents(assignment?.journeyId, !!assignment?.id);
  const [dropdownOpen, setDropdownOpen] = useState<boolean>(false);
  const [sortStudentsBy, setSortStudentsBy] = useState<SortBy>('firstName');
  const [assignedStudents, setAssignedStudents] = useState<IUser[]>([]);
  const [inviteModal, setInviteModal] = useState(false);
  const toggleInviteModal = () => setInviteModal(!inviteModal);
  const { data: submittedSubmissions } = useGetSubmissions({ assignmentId: assignment?.id, unPaged: true }, !!assignment?.id);
  const { setSelectedSubmission } = useSelectedSubmissionState();

  useEffect(() => {
    if (studentId && journeyStudents) {
      setSelectedUser(journeyStudents.find(s => s.user.id === +studentId).user);
      history.replace({ search: `?journeyId=${assignment?.journeyId}` });
    }
  }, [studentId, journeyStudents]);

  useEffect(() => {
    const students = assignment?.students?.length ? assignment.students : getJourneyStudentsUsers(journeyStudents);
    setAssignedStudents(
      sortBy(students, student => {
        if (sortStudentsBy === 'firstName') {
          return student.firstName.toUpperCase();
        }
        return student.lastName.toUpperCase();
      })
    );
  }, [journeyStudents, assignment?.students, sortStudentsBy]);

  const toggleDropDown = () => setDropdownOpen(prevState => !prevState);

  const handleOnStudentClick = (student: IUser) => {
    setSelectedUser(student);
    setSelectedSubmission(submittedSubmissions?.find(s => s?.authors?.find(a => a.id === student?.id)));
  };

  const InviteStudents = () => (
    <ContentCard className="d-flex flex-column">
      <img
        alt={translate('schoolabyApp.journey.detail.inviteStudents')}
        src="content/images/invite_students_assignment.svg"
        className="img-fluid w-100 mt-5 px-3"
      />
      <Button size="lg" outline color={'secondary'} className="mt-5" onClick={toggleInviteModal}>
        <span className="px-5">{translate('schoolabyApp.journey.detail.inviteStudents')}</span>
      </Button>
      <InviteModal modal={inviteModal} toggle={toggleInviteModal} type={ROLE.STUDENT} journey={journey} />
    </ContentCard>
  );

  return assignedStudents?.length ? (
    <ContentCard className="box container students-list">
      <div className="d-flex justify-content-between ml-2 mb-3">
        <h6 className={'card-title'}>{translate('schoolabyApp.assignment.member.students')}</h6>
        <Dropdown toggle={toggleDropDown} isOpen={dropdownOpen} aria-label={translate('schoolabyApp.assignment.detail.sortDropdown')}>
          <DropdownToggle className={'sort-button'}>{translate('schoolabyApp.assignment.detail.sortBy')}</DropdownToggle>
          <DropdownMenu>
            <DropdownItem onClick={() => setSortStudentsBy('firstName')}>
              {translate('schoolabyApp.assignment.detail.sortByFirstName')}
            </DropdownItem>
            <DropdownItem onClick={() => setSortStudentsBy('lastName')}>
              {translate('schoolabyApp.assignment.detail.sortByLastName')}
            </DropdownItem>
          </DropdownMenu>
        </Dropdown>
      </div>
      <Row className="p-2 students-list-heading" noGutters>
        <Col xs={6} className="border-0 bg-transparent px-0 text-uppercase">
          {translate('global.student')}
        </Col>
        <Col xs={2} className="border-0 bg-transparent px-0 text-uppercase">
          {translate('schoolabyApp.assignment.detail.grade')}
        </Col>
        <Col xs={4} className="border-0 bg-transparent px-0 text-uppercase">
          {translate('schoolabyApp.assignment.detail.submitted')}
        </Col>
      </Row>
      {assignedStudents?.map(student => (
        <StudentListItem
          submittedSubmissions={submittedSubmissions}
          key={student.id}
          student={student}
          onStudentClick={handleOnStudentClick}
        />
      ))}
    </ContentCard>
  ) : (
    <InviteStudents />
  );
};

export default StudentsList;
