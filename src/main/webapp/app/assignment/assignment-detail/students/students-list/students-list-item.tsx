import React, { FC, useEffect, useState } from 'react';
import { getFullName } from 'app/shared/util/string-utils';
import Avatar from 'app/shared/layout/header/avatar';
import { APP_LOCAL_DATE_TIME_FORMAT } from 'app/config/constants';
import { Col, Row } from 'reactstrap';
import classNames from 'classnames';
import { TextFormat } from 'react-jhipster';
import { ISubmission } from 'app/shared/model/submission/submission.model';
import { ISubmissionFeedback } from 'app/shared/model/submission/submission-feedback.model';
import { IUser } from 'app/shared/model/user.model';
import { useGradingSchemeState, useSelectedUserState } from 'app/shared/contexts/entity-detail-context';
import {
  DEFAULT_NEW_STYLE,
  DEFAULT_STYLE,
  getGradingContainerClassNameByGradingType,
  SUCCESS_STYLE,
} from 'app/assignment/assignment-detail/submission/shared/grade-select-util';
import { getGradingScore } from 'app/shared/util/grade-util';
import Icon from 'app/shared/icons';
import { PRIMARY } from 'app/shared/util/color-utils';

interface StudentListItemProps {
  student: IUser;
  onStudentClick: (student: IUser) => void;
  submittedSubmissions: ISubmission[];
}

const StudentListItem: FC<StudentListItemProps> = ({ student, onStudentClick, submittedSubmissions }) => {
  const [studentSubmission, setStudentSubmission] = useState<ISubmission>(undefined);
  const [studentSubmissionFeedback, setStudentSubmissionFeedback] = useState<ISubmissionFeedback>(undefined);
  const { gradingScheme } = useGradingSchemeState();
  const { selectedUser } = useSelectedUserState();

  const getSubmissionByStudent = (): ISubmission => {
    return submittedSubmissions?.find(s => s?.authors?.find(a => a.id === student?.id));
  };

  const getFeedbackByStudent = (submission: ISubmission): ISubmissionFeedback => {
    return submission?.submissionFeedbacks?.find(feedback => +feedback.studentId === student?.id);
  };

  const getAvatarColor = () => {
    if (gradingScheme?.isNonGradable()) {
      return studentSubmission?.submittedForGradingDate ? SUCCESS_STYLE : DEFAULT_NEW_STYLE;
    }

    const avatarColor = getGradingContainerClassNameByGradingType(gradingScheme?.code, studentSubmissionFeedback?.grade);
    return !!avatarColor && avatarColor !== DEFAULT_STYLE ? avatarColor : DEFAULT_NEW_STYLE;
  };

  useEffect(() => {
    setStudentSubmission(getSubmissionByStudent());
  }, [student, submittedSubmissions]);

  useEffect(() => {
    setStudentSubmissionFeedback(getFeedbackByStudent(studentSubmission));
  }, [studentSubmission]);

  return (
    <Row
      className={classNames({
        'students-list-item p-2': true,
        selected: student.id === selectedUser?.id,
      })}
      noGutters
      onClick={() => onStudentClick(student)}
    >
      <Col xs={6} className="border-0 px-0">
        <Avatar fullName={getFullName(student)} showName avatarContainerClassName={getAvatarColor()} />
      </Col>
      <Col xs={2}>
        {gradingScheme?.isNonGradable() && !!studentSubmission?.submittedForGradingDate ? (
          <Icon name={'checkmark'} width={'12px'} height={'8px'} fill={PRIMARY} stroke={PRIMARY} />
        ) : (
          getGradingScore(gradingScheme, studentSubmissionFeedback)
        )}
      </Col>
      <Col xs={4}>
        {studentSubmission?.submittedForGradingDate && (
          <TextFormat type="date" value={studentSubmission.submittedForGradingDate} format={APP_LOCAL_DATE_TIME_FORMAT} />
        )}
      </Col>
    </Row>
  );
};

export default StudentListItem;
