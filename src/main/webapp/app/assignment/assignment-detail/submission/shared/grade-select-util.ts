import {
  ALPHABETICAL_A_F,
  GradeType,
  NARRATIVE,
  NUMERICAL_1_12,
  NUMERICAL_1_5,
  PASS_FAIL,
  PERCENTAGE_0_100,
} from 'app/shared/model/grading-scheme.model';

export const SUCCESS_STYLE = 'bg-success text-white';
export const WARNING_STYLE = 'bg-warning text-white';
export const DANGER_STYLE = 'bg-danger text-white';
export const DEFAULT_STYLE = 'bg-white';
export const DEFAULT_NEW_STYLE = 'bg-grey';

export const getGradeText = (gradeType: GradeType): string => {
  if (gradeType === NUMERICAL_1_5) {
    return ' / 5';
  }
  if (gradeType === ALPHABETICAL_A_F) {
    return ' / A-F';
  }
  if (gradeType === PASS_FAIL) {
    return '';
  }
  if (gradeType === NARRATIVE) {
    return '';
  }
  if (gradeType === PERCENTAGE_0_100) {
    return ' / 100%';
  }
  if (gradeType === NUMERICAL_1_12) {
    return ' / 12';
  }
};

export const restrictCharacters = (event, gradeType: GradeType) => {
  if (gradeType === NUMERICAL_1_5) {
    const re = new RegExp('[1-5]');
    if (!re.test(event.key)) {
      event.preventDefault();
    }
  }
  if (gradeType === NUMERICAL_1_12) {
    const newValue = Number(event.target.value.concat(event.key));
    const re = new RegExp('[0-9]');
    if (!re.test(event.key) || newValue < 1 || newValue > 12) {
      event.preventDefault();
    }
  }
  if (gradeType === ALPHABETICAL_A_F) {
    const re = new RegExp('[a-fA-F]');
    if (!re.test(event.key)) {
      event.preventDefault();
    }
  }
  if (gradeType === PERCENTAGE_0_100) {
    const re = new RegExp('^([0-9]?\\d|100)$');
    if (!re.test(event.target.value.concat(event.key))) {
      event.preventDefault();
    }
  }
};

export const getInputMaxLength = (gradeType: GradeType): number => {
  if (gradeType === NUMERICAL_1_5 || gradeType === ALPHABETICAL_A_F) {
    return 1;
  }

  if (gradeType === NUMERICAL_1_12) {
    return 2;
  }

  if (gradeType === PERCENTAGE_0_100) {
    return 3;
  }
};

export const getInputClassName = (gradeType: GradeType): string => {
  if (gradeType === NUMERICAL_1_5 || gradeType === ALPHABETICAL_A_F) {
    return 'input-one-character';
  }

  if (gradeType === NUMERICAL_1_12) {
    return 'input-two-character';
  }

  if (gradeType === PERCENTAGE_0_100) {
    return 'input-three-character';
  }
};

export const getGradingContainerClassNameByGradingType = (gradeType: GradeType, grade) => {
  if (gradeType === NUMERICAL_1_5) {
    if (Number(grade) <= 2) {
      return DANGER_STYLE;
    }
    if (Number(grade) === 3) {
      return WARNING_STYLE;
    }
    if (Number(grade) > 3) {
      return SUCCESS_STYLE;
    } else {
      return DEFAULT_STYLE;
    }
  }
  if (gradeType === ALPHABETICAL_A_F) {
    grade = grade?.toUpperCase();
    if (grade === 'A' || grade === 'B') {
      return SUCCESS_STYLE;
    }
    if (grade === 'C' || grade === 'D') {
      return WARNING_STYLE;
    }
    if (grade === 'E' || grade === 'F') {
      return DANGER_STYLE;
    } else {
      return DEFAULT_STYLE;
    }
  }
  if (gradeType === PASS_FAIL) {
    if (grade === 'PASS') {
      return SUCCESS_STYLE;
    }
    if (grade === 'FAIL') {
      return DANGER_STYLE;
    } else {
      return DEFAULT_STYLE;
    }
  }
  if (gradeType === NARRATIVE) {
    return '';
  }
  if (gradeType === PERCENTAGE_0_100) {
    if (Number(grade) <= 33) {
      return DANGER_STYLE;
    }
    if (Number(grade) <= 66) {
      return WARNING_STYLE;
    }
    if (Number(grade) > 66) {
      return SUCCESS_STYLE;
    } else {
      return DEFAULT_STYLE;
    }
  }

  if (gradeType === NUMERICAL_1_12) {
    if (Number(grade) <= 3) {
      return DANGER_STYLE;
    }

    if (Number(grade) <= 6) {
      return WARNING_STYLE;
    }

    if (Number(grade) > 6) {
      return SUCCESS_STYLE;
    }

    return DEFAULT_STYLE;
  }
};
