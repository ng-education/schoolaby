import React, { useContext, useEffect, useState } from 'react';
import { Button, Col, Dropdown, DropdownItem, DropdownMenu, DropdownToggle, Row } from 'reactstrap';
import { translate } from 'react-jhipster';
import { useAssignmentState, useSelectedUserState } from 'app/shared/contexts/entity-detail-context';
import { ISubmission } from 'app/shared/model/submission/submission.model';
import Avatar from 'app/shared/layout/header/avatar';
import { getFullName } from 'app/shared/util/string-utils';
import { IUser } from 'app/shared/model/user.model';
import GradingBox from 'app/assignment/assignment-detail/submission/submission-grading-body/grading-box';
import { useSubmittedSubmissionsState } from 'app/shared/contexts/submission-grading-context';
import EntityListBox from 'app/shared/layout/entity-list-box/entity-list-box';
import { useGetJourneyStudents } from 'app/shared/services/journey-api';
import { ISubmissionFeedback } from 'app/shared/model/submission/submission-feedback.model';
import Icon from 'app/shared/icons';
import { ICON_GREY } from 'app/shared/util/color-utils';
import sortBy from 'lodash/sortBy';
import { getJourneyStudentsUsers } from 'app/shared/util/journey-utils';
import InviteModal from 'app/journey/journey-detail/invite-modal/invite-modal';
import { AssignmentContext } from 'app/journey/journey-detail/journey-detail-new';
import { useQueryParam } from 'app/shared/hooks/useQueryParam';

type SortBy = 'firstName' | 'lastName';

const SubmissionStudentList = () => {
  const { submittedSubmissions } = useSubmittedSubmissionsState();
  const { selectedUser, setSelectedUser } = useSelectedUserState();
  const assignment = useAssignmentState().entity;
  const [{ studentId }] = useQueryParam<{ studentId: string }>();
  const [inviteModal, setInviteModal] = useState(false);
  const toggleInviteModal = () => setInviteModal(!inviteModal);
  const [dropdownOpen, setDropdownOpen] = useState<boolean>(false);
  const [sortStudentsBy, setSortStudentsBy] = useState<SortBy>('firstName');
  const [assignedStudents, setAssignedStudents] = useState<IUser[]>([]);

  const journey = useContext(AssignmentContext)?.journey;
  const { data: journeyStudents } = useGetJourneyStudents(assignment?.journeyId, !!assignment?.id);

  useEffect(() => {
    studentId && journeyStudents && setSelectedUser(journeyStudents.find(s => s.user.id === +studentId).user);
  }, [studentId, journeyStudents]);

  useEffect(() => {
    const students = assignment?.students?.length ? assignment.students : getJourneyStudentsUsers(journeyStudents);
    setAssignedStudents(
      sortBy(students, student => {
        if (sortStudentsBy === 'firstName') {
          return student.firstName?.toUpperCase();
        }
        return student.lastName?.toUpperCase();
      })
    );
  }, [journeyStudents, assignment?.students, sortStudentsBy]);

  const getSubmissionByStudentId = (id: number): ISubmission => {
    return submittedSubmissions?.length && submittedSubmissions.find(s => s?.authors?.find(a => a.id === id));
  };

  const getFeedbackByStudentId = (id: number): ISubmissionFeedback => {
    const submission = getSubmissionByStudentId(id);
    return submission?.submissionFeedbacks?.find(feedback => +feedback.studentId === id);
  };

  const handleOnClick = (student: IUser) => {
    setSelectedUser(student);
  };

  const toggleDropDown = () => setDropdownOpen(prevState => !prevState);

  return (
    <Row className={'m-0 p-0'}>
      <Row className={'align-items-center w-100'}>
        <Col>
          <h3 className={'my-4 px-3'}>{translate('schoolabyApp.assignment.detail.students')}</h3>
        </Col>
        <Col className={'p-0 d-flex align-items-center justify-content-end'}>
          <Dropdown toggle={toggleDropDown} isOpen={dropdownOpen} aria-label={translate('schoolabyApp.assignment.detail.sortDropdown')}>
            <DropdownToggle className={'d-flex align-items-center justify-content-end p-0'}>
              {translate('schoolabyApp.assignment.detail.sortBy')}
              <Icon name={'preferences'} className={'pt-2 ml-2'} stroke={ICON_GREY} />
            </DropdownToggle>
            <DropdownMenu>
              <DropdownItem onClick={() => setSortStudentsBy('firstName')}>
                {translate('schoolabyApp.assignment.detail.sortByFirstName')}
              </DropdownItem>
              <DropdownItem onClick={() => setSortStudentsBy('lastName')}>
                {translate('schoolabyApp.assignment.detail.sortByLastName')}
              </DropdownItem>
            </DropdownMenu>
          </Dropdown>
        </Col>
      </Row>
      <Col md="12" className={'p-0 student-list'}>
        <div>
          {assignedStudents?.length ? (
            assignedStudents?.map((student, i) => (
              <EntityListBox
                avatar={<Avatar fullName={getFullName(student)} showName />}
                key={i}
                additionalComponent={
                  <GradingBox feedback={getFeedbackByStudentId(student.id)} submission={getSubmissionByStudentId(student.id)} />
                }
                entity={student}
                isActive={selectedUser?.id === student.id}
                onClick={() => handleOnClick(student)}
              />
            ))
          ) : (
            <span className="d-flex justify-content-center align-items-center flex-column">
              <img alt={'No students invited'} src="content/images/invite_students_assignment.svg" className="img-fluid w-100 mt-5 px-3" />
              <Button size="lg" outline color={'secondary'} className="mt-5" onClick={toggleInviteModal}>
                <span className="px-5">{translate('schoolabyApp.journey.detail.inviteStudents')}</span>
              </Button>
              <InviteModal modal={inviteModal} toggle={toggleInviteModal} type={'STUDENT'} journey={journey} />
            </span>
          )}
        </div>
      </Col>
    </Row>
  );
};

export default SubmissionStudentList;
