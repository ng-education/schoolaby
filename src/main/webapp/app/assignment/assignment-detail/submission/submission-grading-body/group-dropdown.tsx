import React, { FC, useEffect, useState } from 'react';
import GradingBox from 'app/assignment/assignment-detail/submission/submission-grading-body/grading-box';
import Icon from 'app/shared/icons';
import { ICON_GREY } from 'app/shared/util/color-utils';
import EntityListBox from 'app/shared/layout/entity-list-box/entity-list-box';
import Avatar from 'app/shared/layout/header/avatar';
import { getFullName } from 'app/shared/util/string-utils';
import { ISubmission } from 'app/shared/model/submission/submission.model';
import { IGroup } from 'app/shared/model/group.model';
import { useSubmittedSubmissionsState } from 'app/shared/contexts/submission-grading-context';
import { useSelectedGroupState, useSelectedUserState } from 'app/shared/contexts/entity-detail-context';
import { translate } from 'react-jhipster';
import { ISubmissionFeedback } from 'app/shared/model/submission/submission-feedback.model';
import { IUser } from 'app/shared/model/user.model';

interface GroupDropDownProps {
  group: IGroup;
}

const GroupDropdown: FC<GroupDropDownProps> = ({ group }) => {
  const { submittedSubmissions } = useSubmittedSubmissionsState();
  const { setSelectedUser, selectedUser } = useSelectedUserState();

  const { setSelectedGroup, selectedGroup } = useSelectedGroupState();
  const [expanded, setExpanded] = useState<boolean>(false);

  const toggleGroup = () => {
    setExpanded(prevState => !prevState);
  };

  useEffect(() => {
    selectedUser && group.students.some(student => student.id === selectedUser.id) && !expanded && toggleGroup();
  }, [selectedUser]);

  const getSubmissionByGroupId = (id: number): ISubmission => {
    return submittedSubmissions?.length && submittedSubmissions.find(submission => submission.groupId === id);
  };

  const getFeedbackByGroupId = (id: number): ISubmissionFeedback => {
    const submission = getSubmissionByGroupId(id);
    return submission?.submissionFeedbacks?.find(feedback => +feedback.groupId === id);
  };

  const getFeedbackByGroupIdAndStudentId = (groupId: number, studentId: number): ISubmissionFeedback => {
    const submission = getSubmissionByGroupId(groupId);
    return submission?.submissionFeedbacks?.find(feedback => +feedback.studentId === studentId);
  };

  const handleGroupOnClick = () => {
    !expanded && toggleGroup();
    setSelectedGroup(group);
    setSelectedUser(null);
  };

  const handleStudentOnClick = (student: IUser) => {
    setSelectedUser(student);
    setSelectedGroup(null);
  };

  return (
    <>
      <div
        className={`group-list-box d-flex align-items-center px-3 ${group.id === selectedGroup?.id ? 'active' : ''}`}
        onClick={() => handleGroupOnClick()}
      >
        <h3 className={'pointer'}>{group.name}</h3>
        <GradingBox feedback={getFeedbackByGroupId(group.id)} submission={getSubmissionByGroupId(group.id)} />
        <div
          aria-label={`${expanded ? translate('global.dropdown.collapse') : translate('global.dropdown.expand')}`}
          onClick={e => {
            e.stopPropagation();
            toggleGroup();
          }}
        >
          <Icon className={`ml-3 chevron ${expanded ? 'expanded' : 'collapsed'}`} name={'chevronLeft'} fill={ICON_GREY} />
        </div>
      </div>
      <div className={`${!expanded ? 'd-none' : ''}`}>
        {group.students.map((student, i) => (
          <EntityListBox
            avatar={<Avatar fullName={getFullName(student)} showName />}
            key={'student' + i}
            additionalComponent={
              <GradingBox
                feedback={
                  getFeedbackByGroupIdAndStudentId(group.id, student.id)
                    ? getFeedbackByGroupIdAndStudentId(group.id, student.id)
                    : getFeedbackByGroupId(group.id)
                }
                submission={getSubmissionByGroupId(group.id)}
              />
            }
            entity={student}
            isActive={selectedUser?.id === student.id}
            onClick={() => handleStudentOnClick(student)}
          />
        ))}
      </div>
    </>
  );
};

export default GroupDropdown;
