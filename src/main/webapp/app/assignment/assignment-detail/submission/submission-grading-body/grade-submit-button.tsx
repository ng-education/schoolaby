import React, { useEffect, useState, useRef } from 'react';

import { CustomInput } from 'reactstrap';
import { useGradingSchemeState, useSelectedGroupState, useSelectedUserState } from 'app/shared/contexts/entity-detail-context';
import { FAIL, GradeType, PASS, PASS_FAIL, PERCENTAGE_0_100 } from 'app/shared/model/grading-scheme.model';
import {
  getGradeText,
  getInputClassName,
  getInputMaxLength,
  restrictCharacters,
} from 'app/assignment/assignment-detail/submission/shared/grade-select-util';
import { translate } from 'react-jhipster';
import { useGradeState } from 'app/shared/contexts/submission-grading-context';

interface IGradeSubmitButton {
  grade?: string;
}

interface IInputProps {
  code: GradeType;
  initialGrade: string;
}

const RenderInput = ({ code, initialGrade }: IInputProps) => {
  const [userSelectedGrade, setUserSelectedGrade] = useState<string>();
  const { grade, setGrade } = useGradeState();
  const { selectedUser } = useSelectedUserState();
  const textInput = useRef(null);

  useEffect(() => {
    setUserSelectedGrade(undefined);
  }, [selectedUser]);

  const handleOnChange = e => {
    if (e.target.value > 100) {
      return;
    }
    if (e.target.value.substring(0, 1) === '0' && !!e.target.value.substring(1, 2)) {
      return;
    } else {
      setUserSelectedGrade(e.target.value);
      setGrade(e.target.value);
    }
  };

  const getInputValue = () => {
    return userSelectedGrade ? userSelectedGrade : initialGrade === undefined ? '' : grade ? grade : '';
  };

  function focusOnInput() {
    textInput.current.focus();
  }

  return (
    <div className={'grade-submit-input'}>
      <div className={'input-wrapper'}>
        <input
          className={getInputClassName(code)}
          onKeyPress={e => restrictCharacters(e, code)}
          maxLength={getInputMaxLength(code)}
          value={getInputValue()}
          onChange={e => handleOnChange(e)}
          ref={textInput}
        />
        <div onClick={() => focusOnInput()} className="right-side">{`${
          code === PERCENTAGE_0_100 ? '%' + getGradeText(code) : getGradeText(code)
        }`}</div>
      </div>
    </div>
  );
};

const renderCheckBoxes = () => {
  const { grade, setGrade } = useGradeState();

  const handlePassOptionChange = e => {
    setGrade(PASS);
  };
  const handleFailOptionChange = e => {
    setGrade(FAIL);
  };
  return (
    <div className="grade-submit-checkbox">
      <div className="checkbox-left checkbox-wrapper d-flex align-items-center">
        <CustomInput
          type="radio"
          id="gradePass"
          name="gradeRadio"
          label={translate('schoolabyApp.gradingSchemeValue.pass')}
          checked={grade === PASS}
          onChange={e => handlePassOptionChange(e)}
        />
      </div>
      <div className="checkbox-right checkbox-wrapper d-flex align-items-center">
        <CustomInput
          type="radio"
          id="gradeFail"
          name="gradeRadio"
          label={translate('schoolabyApp.gradingSchemeValue.fail')}
          checked={grade === FAIL}
          onChange={e => handleFailOptionChange(e)}
        />
      </div>
    </div>
  );
};

export const GradeSubmitButton = ({ grade }: IGradeSubmitButton) => {
  const { gradingScheme } = useGradingSchemeState();
  const { setGrade } = useGradeState();
  const { selectedUser } = useSelectedUserState();
  const { selectedGroup } = useSelectedGroupState();
  const gradingSchemeCode = gradingScheme?.code;

  useEffect(() => {
    setGrade(grade);
  }, [grade]);

  useEffect(() => {
    gradingScheme?.isNonGradable() && setGrade(PASS);
  }, [gradingSchemeCode, selectedUser, selectedGroup]);

  return (
    <>
      {!!gradingSchemeCode &&
        !gradingScheme.isNonGradable() &&
        (gradingSchemeCode === PASS_FAIL ? renderCheckBoxes() : <RenderInput initialGrade={grade} code={gradingSchemeCode} />)}
    </>
  );
};
