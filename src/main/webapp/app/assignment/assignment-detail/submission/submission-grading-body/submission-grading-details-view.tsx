import React, { useContext } from 'react';
import { Col, Row } from 'reactstrap';
import { translate } from 'react-jhipster';
import { useAssignmentState, useSelectedGroupState, useSelectedUserState } from 'app/shared/contexts/entity-detail-context';
import { ISubmission } from 'app/shared/model/submission/submission.model';
import { hasMaterials } from 'app/assignment/assignment-detail/assignment-util';
import AssignmentInfo from 'app/assignment/assignment-detail/assignment-info';
import StudentSubmissionDetails from 'app/assignment/assignment-detail/submission/submission-grading-body/student-submission-details';
import AssignmentMaterials from 'app/assignment/assignment-detail/assignment-materials';
import { useGetLtiResources } from 'app/shared/services/lti-api';
import { AssignmentContext } from 'app/journey/journey-detail/journey-detail-new';

interface IProps {
  submission: ISubmission;
  isAllowedToModify?: boolean;
}

const SubmissionGradingDetailsView = ({ submission, isAllowedToModify }: IProps) => {
  const { selectedUser } = useSelectedUserState();
  const { selectedGroup } = useSelectedGroupState();
  const assignment = useAssignmentState().entity ? useAssignmentState().entity : useContext(AssignmentContext)?.assignment;
  const { data: ltiResources } = useGetLtiResources({ assignmentId: assignment?.id }, !!assignment?.id);

  return (
    <Row className={'m-0 p-0'}>
      {(selectedUser || selectedGroup) && <StudentSubmissionDetails submission={submission} />}
      <Col md={'12'} className={'mt-4 pr-0'}>
        {assignment && <AssignmentInfo assignmentId={assignment?.id} showHiddenFields={isAllowedToModify} />}
      </Col>
      <h3 className="ml-3">{translate('schoolabyApp.assignment.detail.addedAssets')}</h3>
      <Col md={'12'} className={`material-list pr-1 ${!((selectedUser || selectedGroup) && submission) && 'assignment-materials'}`}>
        <Row className={'d-flex'} noGutters>
          {hasMaterials(assignment) || ltiResources?.length ? (
            <AssignmentMaterials assignment={assignment} />
          ) : (
            <p>{translate('schoolabyApp.assignment.detail.noAssets')}</p>
          )}
        </Row>
      </Col>
    </Row>
  );
};

export default SubmissionGradingDetailsView;
