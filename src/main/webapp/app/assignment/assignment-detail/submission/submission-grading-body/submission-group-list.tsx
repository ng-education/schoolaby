import React, { useContext, useEffect, useState } from 'react';
import { Col, Row } from 'reactstrap';
import { translate } from 'react-jhipster';
import { useAssignmentState, useSelectedUserState, useSelectedGroupState } from 'app/shared/contexts/entity-detail-context';

import './submission-group-list.scss';
import GroupDropdown from 'app/assignment/assignment-detail/submission/submission-grading-body/group-dropdown';
import { AssignmentContext } from 'app/journey/journey-detail/journey-detail-new';
import { useQueryParam } from 'app/shared/hooks/useQueryParam';
import { useGetJourneyStudents } from 'app/shared/services/journey-api';
import { IGroup } from 'app/shared/model/group.model';

const SubmissionGroupList = () => {
  const { setSelectedUser } = useSelectedUserState();
  const { setSelectedGroup } = useSelectedGroupState();
  const [assignedGroups, setAssignedGroups] = useState<IGroup[]>([]);
  const assignment = useAssignmentState().entity ? useAssignmentState().entity : useContext(AssignmentContext)?.assignment;
  const [{ studentId }] = useQueryParam<{ studentId: string }>();
  const [{ groupId }] = useQueryParam<{ groupId: string }>();
  const { data: journeyStudents } = useGetJourneyStudents(assignment?.journeyId, !!assignment?.journeyId);

  useEffect(() => {
    if (groupId && !studentId) {
      setSelectedGroup(assignedGroups.find(g => g.id === +groupId));
      setSelectedUser(undefined);
    } else {
      setSelectedGroup(undefined);
    }
    studentId && journeyStudents && setSelectedUser(journeyStudents.find(s => s.user.id === +studentId).user);
  }, [groupId, studentId, journeyStudents, assignedGroups]);

  useEffect(() => {
    if (assignment?.groups?.length) {
      setAssignedGroups(assignment.groups);
    }
  }, [assignment?.groups]);

  return (
    <Row className={'m-0 p-0'}>
      <h3 className={'my-4 px-3'}>{translate('schoolabyApp.assignment.detail.students')}</h3>
      <Col md="12" className={'p-0 student-list'}>
        <div>
          {assignedGroups?.length &&
            assignedGroups?.map((group, i) => {
              return <GroupDropdown key={i} group={group} />;
            })}
        </div>
      </Col>
    </Row>
  );
};

export default SubmissionGroupList;
