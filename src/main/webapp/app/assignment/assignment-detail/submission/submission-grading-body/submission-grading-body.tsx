import React, { useContext, useEffect, useState } from 'react';
import { Col, Row } from 'reactstrap';
import { translate } from 'react-jhipster';
import { connect } from 'react-redux';
import {
  useAssignmentState,
  useSelectedGroupState,
  useSelectedUserState,
  useSubmissionsState,
} from 'app/shared/contexts/entity-detail-context';
import { ISubmission } from 'app/shared/model/submission/submission.model';
import { IRootState } from 'app/shared/reducers';
import { hasAnyAuthority } from 'app/shared/auth/private-route';
import { AUTHORITIES } from 'app/config/constants';
import { getLtiResources } from 'app/shared/services/lti-api';
import { convertDateTimeToServer } from 'app/shared/util/date-utils';
import { DateTime } from 'luxon';
import { IMessage } from 'app/shared/model/message.model';
import { createMessage, findAllMessages, updateMessage } from 'app/shared/services/message-api';
import { getSubmissionsByAssignmentId, gradeSubmission } from 'app/shared/services/submission-api';
import { ISubmissionGradingPatch } from 'app/shared/model/submission/submission-grading-patch.model';
import { createChat, getChatsBySubmission } from 'app/shared/services/chat-api';
import SubmissionFeedbackBody from 'app/assignment/assignment-detail/submission/submission-grading-body/submission-feedback-body';
import SubmissionStudentList from 'app/assignment/assignment-detail/submission/submission-grading-body/submission-student-list';
import SubmissionGradingDetailsView from 'app/assignment/assignment-detail/submission/submission-grading-body/submission-grading-details-view';
import {
  useCommentFromEditorState,
  useFeedbackFilesState,
  useGradeState,
  useLtiResourcesState,
  useModifiedSubmissionState,
  useResubmittableState,
  useSelectedCriterionLevelsState,
  useSelectedFeedbackState,
  useSubmissionFeedbackState,
  useSubmittedSubmissionsState,
} from 'app/shared/contexts/submission-grading-context';
import { ILtiResource } from 'app/shared/model/lti-resource.model';
import { useQuery } from 'react-query';
import { ASSIGNMENT_LTI_RESOURCES_KEY } from 'app/config/reactQueryKeyConstants';
import { SubmitFooter } from './submit-footer/submit-footer';
import { useGetJourneyStudents } from 'app/shared/services/journey-api';
import SubmissionGroupList from './submission-group-list';
import { IUser } from 'app/shared/model/user.model';
import { ISubmissionFeedback } from 'app/shared/model/submission/submission-feedback.model';
import { IGroup } from 'app/shared/model/group.model';
import WarningModal from 'app/shared/layout/warning-modal/warning-modal';
import { useTeacherCreatingFeedbackState } from 'app/shared/contexts/root-context';
import { AssignmentContext } from 'app/journey/journey-detail/journey-detail-new';
import flatMap from 'lodash/flatMap';
import { useCreateSubmissionFeedback } from 'app/shared/services/submission-feedback-api';

export type ISubmissionGradingBody = StateProps;

export const SubmissionGradingBody = ({ isAllowedToModify, account }: ISubmissionGradingBody) => {
  const [chatParticipants, setChatParticipants] = useState<IUser[]>(null);
  const [showModal, setShowModal] = useState<boolean>(false);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const { setLtiResources } = useLtiResourcesState();
  const { commentFromEditor } = useCommentFromEditorState();
  const { submissionFeedback, setSubmissionFeedback } = useSubmissionFeedbackState();
  const { modifiedSubmission } = useModifiedSubmissionState();
  const { feedbackFiles, setFeedbackFiles } = useFeedbackFilesState();
  const { resubmittable, setIsResubmittable } = useResubmittableState();
  const { submittedSubmissions, setSubmittedSubmissions } = useSubmittedSubmissionsState();
  const { grade } = useGradeState();
  const assignment = useAssignmentState().entity ? useAssignmentState().entity : useContext(AssignmentContext)?.assignment;
  const { selectedUser } = useSelectedUserState();
  const { selectedGroup } = useSelectedGroupState();
  const { submissions, setSubmissions } = useSubmissionsState();
  const { selectedCriterionLevels, setSelectedCriterionLevels } = useSelectedCriterionLevelsState();
  const { setTeacherCreatingFeedback } = useTeacherCreatingFeedbackState();
  const { data: journeyStudents } = useGetJourneyStudents(assignment?.journeyId, !!assignment?.id);
  const createSubmissionFeedback = useCreateSubmissionFeedback();
  const assignedStudents = assignment?.students?.length ? assignment.students : journeyStudents;
  const assignedGroups = assignment?.groups?.length && assignment.groups;
  const { setSelectedFeedback } = useSelectedFeedbackState();

  useQuery<ILtiResource[]>([ASSIGNMENT_LTI_RESOURCES_KEY, +assignment?.id], () => getLtiResources({ assignmentId: assignment?.id }), {
    onSuccess: setLtiResources,
    enabled: !!assignment?.id,
  });

  useEffect(() => {
    setIsLoading(false);
  }, [selectedUser, selectedGroup]);

  useEffect(() => {
    setIsLoading(createSubmissionFeedback.isLoading);
  }, [createSubmissionFeedback.isLoading]);

  const isSelectedUserInAuthors = (submission: ISubmission) => {
    return submission?.authors?.some(a => a.id === selectedUser.id);
  };

  const getSubmissionBySelectedStudent = (): ISubmission => {
    return (
      selectedUser &&
      submittedSubmissions?.find(s => (assignedGroups ? s.groupId && isSelectedUserInAuthors(s) : isSelectedUserInAuthors(s)))
    );
  };

  const getSubmissionBySelectedGroup = (): ISubmission => {
    return selectedGroup && submittedSubmissions?.find(s => s.groupId === selectedGroup.id);
  };

  const getFeedbackBySelectedStudent = (): ISubmissionFeedback => {
    const submission = getSubmissionBySelectedStudent();
    return submission?.submissionFeedbacks?.find(feedback => +feedback.studentId === selectedUser?.id);
  };

  const getFeedbackBySelectedGroup = (): ISubmissionFeedback => {
    const submission = getSubmissionBySelectedGroup();
    return submission?.submissionFeedbacks?.find(feedback => +feedback.groupId === selectedGroup?.id);
  };

  const getGroupBySelectedStudent = (): IGroup => {
    return assignment?.groups?.find(group => group.students.find(student => student.id === selectedUser?.id));
  };

  const getGroupFeedbackBySelectedStudent = (): ISubmissionFeedback => {
    const submission = getSubmissionBySelectedStudent();
    const group = getGroupBySelectedStudent();
    return submission?.submissionFeedbacks?.find(feedback => +feedback.groupId === group.id);
  };

  const getFeedback = (): ISubmissionFeedback => {
    if (assignment?.groups?.length) {
      return getFeedbackBySelectedStudent() || getFeedbackBySelectedGroup() || getGroupFeedbackBySelectedStudent();
    } else {
      return getFeedbackBySelectedStudent();
    }
  };

  const hasIndividualFeedback = (submission: ISubmission): boolean => {
    return submission?.submissionFeedbacks?.some(feedback => feedback.studentId !== null && feedback.studentId !== undefined);
  };

  const hasGroupFeedback = (submission: ISubmission): boolean => {
    return submission?.submissionFeedbacks?.some(feedback => feedback.groupId !== null && feedback.groupId !== undefined);
  };

  useEffect(() => {
    const feedback = getFeedback();
    if (selectedGroup) {
      setChatParticipants([account, ...selectedGroup.students]);
    } else if (selectedUser) {
      setChatParticipants([account, selectedUser]);
    }
    !!feedback?.selectedCriterionLevels && setSelectedCriterionLevels(feedback.selectedCriterionLevels);
    setSelectedFeedback(feedback);
  }, [selectedUser, selectedGroup, submittedSubmissions]);

  useEffect(() => {
    setTeacherCreatingFeedback(true);
    return () => setTeacherCreatingFeedback(false);
  }, []);

  useEffect(() => {
    setSubmittedSubmissions(submissions.filter(s => !!s.submittedForGradingDate));
  }, [submissions]);

  useEffect(() => {
    const submission = selectedGroup ? getSubmissionBySelectedGroup() : getSubmissionBySelectedStudent();
    setIsResubmittable(submission?.resubmittable);

    if (submission) {
      findAllMessages({
        submissionId: submission.id,
        feedback: true,
      }).then(response => response?.data && setSubmissionFeedback(response.data[0]));

      const feedbackFilesLists = submission?.submissionFeedbacks?.map(feedback => feedback.uploadedFiles || []);
      setFeedbackFiles(flatMap(feedbackFilesLists) || []);
      setIsResubmittable(submission.resubmittable);
    } else {
      setSelectedCriterionLevels(undefined);
      setSubmissionFeedback(undefined);
      setFeedbackFiles([]);
      setIsResubmittable(false);
    }
  }, [selectedUser, selectedGroup, submittedSubmissions]);

  useEffect(() => {
    if (modifiedSubmission) {
      const existingSubmission = submittedSubmissions?.findIndex(submission => submission?.id === modifiedSubmission?.id);
      if (existingSubmission !== -1) {
        submittedSubmissions?.splice(existingSubmission, 1);
      }
      setSubmittedSubmissions([...submittedSubmissions, modifiedSubmission]);
    }
  }, [modifiedSubmission]);

  const updateSubmissionMessages = (message: IMessage) => {
    setSubmissionFeedback(message);
  };

  const saveMessage = async (submissionId?: number) => {
    if ((!assignment?.groups?.length || selectedGroup) && commentFromEditor && commentFromEditor !== '') {
      let entity: IMessage;
      let savedMessageResponse;
      if (submissionFeedback) {
        entity = {
          ...submissionFeedback,
          value: commentFromEditor,
        };
        savedMessageResponse = await updateMessage(entity);
      } else {
        let chat;
        await getChatsBySubmission(submissionId).then(resp => (chat = resp.data[0]));
        if (!chat) {
          const chatEntity = {
            title: assignment.title,
            submissionId,
            journeyId: assignment?.journeyId,
            people: chatParticipants,
          };
          await createChat(chatEntity).then(resp => (chat = resp.data));
        }
        entity = {
          value: commentFromEditor,
          creatorId: account.id,
          chatId: chat?.id,
          feedback: true,
        };
        savedMessageResponse = await createMessage(entity);
      }
      updateSubmissionMessages(savedMessageResponse.data);
    }
  };

  const submit = async (submission: ISubmission) => {
    const feedbackDate = convertDateTimeToServer(DateTime.local().toString());

    if (submission) {
      const patchEntity: ISubmissionGradingPatch = {
        resubmittable,
        feedbackFiles,
        submittedForGradingDate: submission.submittedForGradingDate,
        submissionFeedback: {
          studentId: selectedUser && selectedUser.id,
          groupId: selectedGroup && selectedGroup.id.toString(),
          grade,
          value: commentFromEditor,
          feedbackDate,
          selectedCriterionLevels,
          submissionId: submission.id.toString(),
        },
        className: '.SubmissionGradingPatchDTO',
      };

      await gradeSubmission(submission.id, patchEntity).then(savedFeedback => {
        saveMessage(+savedFeedback.submissionId);
        getSubmissionsByAssignmentId(assignment.id).then(res => {
          setSubmissions(res);
        });
      });
    } else {
      const newSubmission: ISubmission = {
        value: '',
        assignmentId: assignment?.id,
        submittedForGradingDate: feedbackDate,
        authors: assignment?.groups?.length ? selectedGroup?.students || getGroupBySelectedStudent().students : [selectedUser],
        groupId: selectedGroup?.id || getGroupBySelectedStudent()?.id,
      };

      const feedback: ISubmissionGradingPatch = {
        resubmittable,
        feedbackFiles,
        submittedForGradingDate: feedbackDate,
        submissionFeedback: {
          studentId: selectedUser && selectedUser.id,
          groupId: selectedGroup && selectedGroup.id.toString(),
          grade,
          value: commentFromEditor,
          feedbackDate,
          selectedCriterionLevels,
          submission: newSubmission,
        },
        className: '.SubmissionGradingPatchDTO',
      };

      await createSubmissionFeedback.mutateAsync(feedback).then(async response => {
        await saveMessage(+response?.data?.submissionId);

        getSubmissionsByAssignmentId(assignment.id).then(res => {
          setSubmissions(res);
        });
      });
    }
  };

  const onSubmit = async () => {
    if (!isAllowedToModify) {
      return;
    }

    const submission = selectedGroup ? getSubmissionBySelectedGroup() : getSubmissionBySelectedStudent();

    if ((selectedUser && hasGroupFeedback(submission)) || (selectedGroup && hasIndividualFeedback(submission))) {
      setShowModal(true);
    } else {
      await submit(submission);
    }
  };

  return (
    <>
      <Row className={'justify-content-center detail-body'}>
        <Col lg="3" className={'border-right flex-wrap students-container'}>
          {assignment?.groups?.length ? <SubmissionGroupList /> : <SubmissionStudentList />}
        </Col>
        <Col lg={'5'} className={'border-right submission-info-container no-gutters'}>
          <SubmissionGradingDetailsView
            submission={selectedGroup ? getSubmissionBySelectedGroup() : getSubmissionBySelectedStudent()}
            isAllowedToModify={isAllowedToModify}
          />
        </Col>
        <Col lg={'4'} className={'px-4 grading-container'}>
          {selectedGroup || selectedUser ? (
            <SubmissionFeedbackBody assignment={assignment} />
          ) : (
            <Col lg="12" className="mt-5 pt-5 px-5 d-flex justify-content-center align-items-center flex-column">
              <img alt={'No student selected'} src="content/images/select_student_for_feedback.svg" className="img-fluid w-100" />
              <h3 className={'text-secondary font-weight-light mt-5 text-center'}>
                {assignedGroups?.length
                  ? translate('schoolabyApp.assignment.detail.selectStudentOrGroup')
                  : assignedStudents?.length
                  ? translate('schoolabyApp.assignment.detail.selectStudent')
                  : translate('schoolabyApp.assignment.detail.inviteStudents')}
              </h3>
            </Col>
          )}
        </Col>
      </Row>
      <SubmitFooter
        disabled={(!selectedGroup && !selectedUser) || isLoading}
        onClick={onSubmit}
        name={translate('schoolabyApp.assignment.detail.submitGrade')}
      />
      <WarningModal
        showModal={showModal}
        setShowModal={setShowModal}
        headerText={
          selectedGroup
            ? translate('schoolabyApp.submission.groupGradingWarningModal.groupHeader')
            : translate('schoolabyApp.submission.groupGradingWarningModal.individualHeader')
        }
        bodyText={
          selectedGroup
            ? translate('schoolabyApp.submission.groupGradingWarningModal.groupBody')
            : translate('schoolabyApp.submission.groupGradingWarningModal.individualBody')
        }
        handleConfirmation={() => {
          setShowModal(false);
          submit(getSubmissionBySelectedGroup() || getSubmissionBySelectedStudent());
        }}
      />
    </>
  );
};

const mapStateToProps = ({ authentication }: IRootState) => ({
  isAllowedToModify: hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.TEACHER, AUTHORITIES.ADMIN]),
  account: authentication.account,
});

type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(SubmissionGradingBody);
