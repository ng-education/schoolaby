import React, { useContext, useEffect, useState } from 'react';
import { Row } from 'reactstrap';
import { TextFormat, translate } from 'react-jhipster';
import { useAssignmentState, useSelectedGroupState, useSelectedUserState } from 'app/shared/contexts/entity-detail-context';
import { ISubmission } from 'app/shared/model/submission/submission.model';
import { isPastDate } from 'app/assignment/assignment-detail/assignment-util';
import Icon from 'app/shared/icons';
import Avatar from 'app/shared/layout/header/avatar';
import { getFullName } from 'app/shared/util/string-utils';
import { APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import LtiResults from 'app/shared/layout/lti/lti-results';
import ReactMarkdown from 'react-markdown';
import { useLtiResourcesState } from 'app/shared/contexts/submission-grading-context';
import UploadedFilesList from 'app/shared/form/uploaded-files-list';
import { getLtiScores } from 'app/shared/services/lti-api';
import { ILtiScore } from 'app/shared/model/lti-score.model';
import { AssignmentContext } from 'app/journey/journey-detail/journey-detail-new';

interface IProps {
  submission: ISubmission;
}

const StudentSubmissionDetails = ({ submission }: IProps) => {
  const { selectedUser } = useSelectedUserState();
  const { selectedGroup } = useSelectedGroupState();
  const { ltiResources } = useLtiResourcesState();
  const [ltiScores, setLtiScores] = useState<ILtiScore[]>();
  const assignment = useAssignmentState().entity ? useAssignmentState().entity : useContext(AssignmentContext)?.assignment;

  useEffect(() => {
    setLtiScores(undefined);
    if (selectedUser && submission && ltiResources && ltiResources.find(res => res.syncGrade)) {
      getLtiScores({
        assignmentId: assignment?.id,
        userId: selectedUser.id,
      }).then(result => {
        setLtiScores(result.data);
      });
    }
  }, [selectedUser, ltiResources]);

  return (
    <div className="d-flex flex-row flex-grow-1 bg-white border-top border-bottom px-3 pt-4 mw-100">
      <div className="d-flex flex-column w-100">
        <Row>
          {!selectedGroup && <Avatar className={'ml-3'} fullName={getFullName(selectedUser)} />}
          <h3 className="ml-3">
            {(selectedGroup ? selectedGroup.name : getFullName(selectedUser)) +
              ' ' +
              translate('schoolabyApp.submission.submissionInfo.studentSubmission')}
          </h3>
        </Row>
        {submission?.submittedForGradingDate ? (
          <>
            <div className="d-flex flex-column flex-row pt-1">
              <div className={'d-flex'}>
                <span className={'text-muted mr-2'}>{translate('schoolabyApp.submission.submitted')}</span>
                <Icon name={'calendar'} className={'mb-1 mr-2'} />
                <span
                  className={`${
                    isPastDate(new Date(submission.submittedForGradingDate), new Date(assignment.deadline)) ? 'text-success' : 'text-danger'
                  }`}
                >
                  <TextFormat type="date" value={submission.submittedForGradingDate} format={APP_LOCAL_DATE_FORMAT} />
                </span>
                {selectedGroup && (
                  <span className={'ml-3'}>
                    <b>{`${submission.groupName} liikmed: `} </b>
                    {selectedGroup.students.map(student => getFullName(student)).join(', ')}
                  </span>
                )}
              </div>
              {!selectedGroup && submission.groupName && <p className={'text-muted mt-1'}>{submission.groupName}</p>}
            </div>
            <LtiResults scores={ltiScores} ltiResources={ltiResources} />
            <span className={'assignment-description text-break mt-4 preserve-space'}>
              <ReactMarkdown source={submission.value} renderers={{ code: ({ value }) => <span>{value}</span> }} />
            </span>
            {submission.submittedForGradingDate && (
              <div className={'d-flex flex-column flex-grow-1 uploaded-files px-0'}>
                <h3 className={'my-3'}>{translate('schoolabyApp.submission.submissionInfo.studentAssets')}</h3>
                <div className={'material-list d-flex flex-row'}>
                  <UploadedFilesList
                    files={submission.uploadedFiles}
                    alternativeText={translate('schoolabyApp.submission.submissionInfo.noAssets')}
                    canEdit
                  />
                </div>
              </div>
            )}
          </>
        ) : (
          <div className="d-flex justify-content-center align-items-center flex-column">
            <img className="mt-5" src="content/images/solution_not_submitted_new.svg" alt="no submission" />
            <h3 className="my-5">{translate('schoolabyApp.submission.noSubmission')}</h3>
          </div>
        )}
      </div>
    </div>
  );
};
export default StudentSubmissionDetails;
