import React, { useEffect, useState } from 'react';
import { Button, Col, Input, Label, Row } from 'reactstrap';
import { translate } from 'react-jhipster';
import { useSelectedGroupState, useSelectedUserState } from 'app/shared/contexts/entity-detail-context';
import { MarkdownEditor } from 'app/assignment/assignment-detail/submission/shared/markdown-editor';
import { GradeSubmitButton } from 'app/assignment/assignment-detail/submission/submission-grading-body/grade-submit-button';
import { IUploadedFile } from 'app/shared/model/uploaded-file.model';
import {
  useCommentFromEditorState,
  useFeedbackFilesState,
  useResubmittableState,
  useSelectedCriterionLevelsState,
  useSelectedFeedbackState,
} from 'app/shared/contexts/submission-grading-context';
import FileUpload from 'app/shared/form/file-upload';
import { IAssignment } from 'app/shared/model/assignment.model';
import { ISelectedCritreionLevel } from 'app/shared/model/rubric/selected-critreion-level.model';
import RubricPoints from 'app/assignment/assessment-rubric/rubric-points/rubric-points';
import { useGetRubric } from 'app/shared/services/rubric-api';
import { FileUploadProvider } from 'app/shared/form/file-upload-context';
import RubricViewModes from 'app/assignment/assessment-rubric-new/rubric-overview/rubric-view-modes';
import { useSelectedRubricState } from 'app/shared/contexts/entity-update-context';
import RubricOverview from 'app/assignment/assessment-rubric-new/rubric-overview/rubric-overview';

interface IProps {
  assignment?: IAssignment;
}

const SubmissionFeedbackBody = ({ assignment }: IProps) => {
  const { selectedUser } = useSelectedUserState();
  const { selectedGroup } = useSelectedGroupState();
  const { setCommentFromEditor } = useCommentFromEditorState();
  const { feedbackFiles, setFeedbackFiles } = useFeedbackFilesState();
  const { resubmittable, setIsResubmittable } = useResubmittableState();
  const { selectedCriterionLevels, setSelectedCriterionLevels } = useSelectedCriterionLevelsState();
  const { data: rubric } = useGetRubric(assignment?.id);
  const { selectedRubric, setSelectedRubric } = useSelectedRubricState();
  const { selectedFeedback } = useSelectedFeedbackState();
  const [feedbackComment, setFeedbackComment] = useState<string>(selectedFeedback?.value || '');

  useEffect(() => {
    setFeedbackComment(selectedFeedback?.value || '');
  }, [selectedUser, selectedGroup, selectedFeedback]);

  const onFileUpload = (file: IUploadedFile) => {
    setFeedbackFiles(previousState => [...previousState, file]);
  };

  const onFileDelete = (file: IUploadedFile) => {
    setFeedbackFiles(previousState => previousState.filter(prevFile => prevFile.id !== file.id));
  };

  const handleCriterionLevelSelectionSave = (selectedLevels: ISelectedCritreionLevel[]) => {
    setSelectedCriterionLevels(selectedLevels);
  };

  const handleEditorStateChange = data => {
    setCommentFromEditor(data);
    setFeedbackComment(data);
  };

  return (
    <Row>
      <Col md={'12'}>
        <h3 className={'mt-4 mb-2'}>{translate('schoolabyApp.assignment.detail.submitGrade')}</h3>
        {!!rubric && ((assignment?.groups?.length && selectedGroup) || !assignment?.groups?.length) && (
          <>
            <Button type="button" color="primary" className={'rubric-btn mb-3'} onClick={() => setSelectedRubric(rubric)}>
              {translate('schoolabyApp.assignment.rubric.viewTitle')}
            </Button>
            <RubricPoints rubric={rubric} selectedCriterionLevels={selectedCriterionLevels} />
          </>
        )}
        <div className={'mb-4'}>
          <MarkdownEditor value={feedbackComment} onEditorStateChange={handleEditorStateChange} />
        </div>
        <FileUploadProvider>
          <FileUpload
            videoFileNamePrefix={assignment.title}
            existingFiles={feedbackFiles}
            onUpload={onFileUpload}
            onDelete={onFileDelete}
            uploadDisabled={false}
            maxFiles={10}
            resetOn={selectedUser}
          />
        </FileUploadProvider>
      </Col>
      <Col md={'12'} className={'mt-3'}>
        <Input type={'checkbox'} checked={resubmittable} onChange={e => setIsResubmittable(e.target.checked)} />
        <Label id="flexibleDeadlineLabel" className={'ml-1'}>
          {translate('schoolabyApp.submission.submissionInfo.resubmittable')}
        </Label>
      </Col>
      <Col md={'12'} className="pt-2 pb-2">
        <GradeSubmitButton grade={(selectedUser || selectedGroup) && selectedFeedback?.grade} />
      </Col>
      {!!selectedRubric && (
        <RubricOverview
          viewMode={RubricViewModes.GRADE}
          selectedLevels={selectedCriterionLevels}
          saveSelectedLevels={handleCriterionLevelSelectionSave}
        />
      )}
    </Row>
  );
};

export default SubmissionFeedbackBody;
