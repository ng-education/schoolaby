import React from 'react';
import { UncontrolledTooltip } from 'reactstrap';
import { translate } from 'react-jhipster';
import { useGradingSchemeState } from 'app/shared/contexts/entity-detail-context';
import { ISubmission } from 'app/shared/model/submission/submission.model';
import { isPastDate } from 'app/assignment/assignment-detail/assignment-util';
import Icon from 'app/shared/icons';
import { SubmissionGradeContainer } from 'app/assignment/assignment-detail/submission/shared/submission-grade-container';
import { PRIMARY, SECONDARY } from 'app/shared/util/color-utils';
import { ISubmissionFeedback } from 'app/shared/model/submission/submission-feedback.model';

interface IProps {
  feedback: ISubmissionFeedback;
  submission: ISubmission;
}

const GradingBox = ({ feedback, submission }: IProps) => {
  const { gradingScheme } = useGradingSchemeState();

  const checkmarkColor = feedback?.grade ? PRIMARY : SECONDARY;

  const hasNewSubmission = () => !feedback?.feedbackDate && submission?.submittedForGradingDate;

  const hasNewSubmissionSinceLastFeedback = () =>
    isPastDate(new Date(submission?.submissionFeedbacks?.[0].feedbackDate), new Date(submission?.submittedForGradingDate));

  const AttentionIcon = () => (
    <span id={'student-submitted-icon'} role={'student-submitted-icon-span'} className={'my-auto'}>
      <Icon
        className={`${gradingScheme?.isNonGradable() ? 'mr-2' : 'mr-1'} ml-auto d-inline-block`}
        name={'exclamation'}
        stroke={PRIMARY}
        height="22px"
        width="22px"
      />
      <UncontrolledTooltip target="student-submitted-icon">
        {translate('schoolabyApp.assignment.detail.newSubmissionHasBeenMade')}
      </UncontrolledTooltip>
    </span>
  );

  return (
    <div className={'d-inline-block ml-auto my-auto d-flex flex-col'}>
      {(hasNewSubmission() || hasNewSubmissionSinceLastFeedback()) && <AttentionIcon />}
      {gradingScheme?.isNonGradable() ? (
        <div className="align-items-center float-right d-inline-flex icon-wrapper">
          <Icon className={'float-right'} name={'checkmark'} width={'18px'} height={'14px'} fill={checkmarkColor} stroke={checkmarkColor} />
        </div>
      ) : (
        <SubmissionGradeContainer feedback={feedback} />
      )}
    </div>
  );
};

export default GradingBox;
