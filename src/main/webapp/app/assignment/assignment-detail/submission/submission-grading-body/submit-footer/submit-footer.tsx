import './submit-footer.scss';
import React from 'react';
import debounce from 'lodash/debounce';
import { Button } from 'reactstrap';

interface ISubmitFooter {
  style?: React.CSSProperties;
  disabled?: boolean;
  name: string;
  className?: string;
  onClick?: () => void;
}

export const SubmitFooter = ({ className, disabled, name, onClick, style }: ISubmitFooter) => {
  return (
    <div className="submit-footer">
      <footer className={className} style={style}>
        <Button
          onClick={debounce(() => onClick(), 500)}
          className="capitalized-btn"
          color={'primary'}
          disabled={disabled}
          id={'submission-submit'}
          role={'submit'}
        >
          {name}
        </Button>
      </footer>
    </div>
  );
};
