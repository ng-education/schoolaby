import React from 'react';
import { Col, CustomInput, Row } from 'reactstrap';
import { TextFormat, translate } from 'react-jhipster';
import LinesEllipsis from 'react-lines-ellipsis';
import { APP_LOCAL_DATE_TIME_FORMAT } from 'app/config/constants';
import { isPastDeadline } from 'app/assignment/assignment-detail/assignment-util';
import Icon from 'app/shared/icons';
import ReactMarkdown from 'react-markdown';
import { IGroup } from 'app/shared/model/group.model';
import { getFullName } from 'app/shared/util/string-utils';
import { IRootState } from 'app/shared/reducers';
import { connect } from 'react-redux';
import { useGetAssignment } from 'app/shared/services/assignment-api';
import { useGetGradingScheme } from 'app/shared/services/grading-scheme-api';

interface IAssignmentInfoProps extends StateProps {
  showHiddenFields: boolean;
  assignmentId: number;
  userGroup?: IGroup;
  previewEnabled?: boolean;
}

const AssignmentInfo = ({ showHiddenFields, userGroup, account, assignmentId, previewEnabled }: IAssignmentInfoProps) => {
  const { data: assignment } = useGetAssignment(assignmentId, !!assignmentId, { template: previewEnabled });
  const { data: gradingScheme } = useGetGradingScheme(assignment?.gradingSchemeId, !!assignment?.gradingSchemeId);

  const isOverdue: boolean = !!assignment && isPastDeadline(assignment);
  const currentUrl = window.location.protocol + '//' + window.location.hostname + '/assignment/' + assignment?.id;

  return assignment ? (
    <>
      <h3 className={'mb-1'}>
        <LinesEllipsis text={assignment.title} maxLine={2} />
      </h3>
      <div className={'mb-3'}>
        <span className={'d-inline-block text-nowrap mr-3'}>
          <span className={'mr-2 text-secondary'}>{translate('schoolabyApp.assignment.deadline')}</span>
          <Icon name={'calendar'} className={'mb-1 mr-2'} />
          <span className={`mr-1 ${isOverdue && !previewEnabled ? 'text-danger' : 'text-dark'}`}>
            <TextFormat type="date" value={assignment.deadline} format={APP_LOCAL_DATE_TIME_FORMAT} />
          </span>
          {!assignment.flexibleDeadline && !previewEnabled && <Icon name={'exclamationCircleFilled'} className={'mb-1'} />}
        </span>

        <span className={'d-inline-block text-nowrap mr-3 mt-2'}>
          <span className={'text-secondary mr-2'}>{translate('schoolabyApp.assignment.gradingScheme')}</span>
          <span className={'text-white rounded border-primary p-1 grading-scheme-box'}>
            {gradingScheme?.code && translate(`schoolabyApp.gradingScheme.code.${gradingScheme.code}`)}
          </span>
        </span>

        {!!assignment.groups?.length && (
          <span className={'d-inline-block mt-2 mr-2'}>
            <span className={'text-secondary mr-2'}>{translate('schoolabyApp.assignment.type')}</span>
            <Icon name={'groupAssignment'} className={'mr-2'} />
            <span className="text-dark">{translate('schoolabyApp.assignment.groupAssignment')}</span>
          </span>
        )}

        {showHiddenFields && (
          <>
            <span className={'d-inline-block mt-2'}>
              <span className={'text-secondary mr-2'}>{translate('entity.publishedState.hidden')}</span>
              <CustomInput type="switch" id="hidden" disabled checked={assignment.hidden} className={'mb-3 d-inline-block'} />
            </span>
            <span className={'d-inline-block ml-1'}>
              <a
                href={`https://stuudium.link/jaga?url=${currentUrl}&title=${assignment.title}`}
                target="_blank"
                rel="noopener noreferrer"
                className={'mr-2'}
              >
                <span className={'text-secondary mr-1'}>{translate('schoolabyApp.assignment.shareToStuudium')}</span>
                <img src="content/images/stuudium_logo.png" alt="Stuudium_icon" width="22" height="22" />
              </a>
            </span>
          </>
        )}
      </div>
      {!showHiddenFields && !!userGroup && (
        <div>
          <span className="font-weight-bold">{translate('schoolabyApp.assignment.yourGroup')}: </span>
          {userGroup.name}
          {` - ${userGroup?.students
            .filter(student => student.id !== account.id)
            .map(student => getFullName(student))
            .join(', ')}`}
        </div>
      )}
      <Row noGutters>
        <Col className={'py-3 text-break'}>
          <div
            className={`text-dark font-weight-normal preserve-space text-break ${!showHiddenFields ? 'assignment-description-info' : ''}`}
          >
            <ReactMarkdown source={assignment.description} />
          </div>
        </Col>
      </Row>
    </>
  ) : null;
};

const mapStateToProps = ({ authentication }: IRootState) => ({
  account: authentication.account,
});

type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(AssignmentInfo);
