import './backpack.scss';
import React, { useState } from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';
import { IRootState } from 'app/shared/reducers';
import Heading, { ITabParams, PreviewTabType } from 'app/shared/layout/heading/heading';
import MenuDropdown from 'app/shared/layout/menu-dropdown/menu-dropdown';
import { hasAnyAuthority } from 'app/shared/auth/private-route';
import { AUTHORITIES } from 'app/config/constants';
import { getHeadingIcon } from 'app/shared/util/journey-utils';
import { ASSIGNMENT } from 'app/shared/util/entity-utils';
import MyJourneysBreadcrumbItem from 'app/shared/layout/heading/breadcrumbs/my-journeys-breadcrumb-item';
import JourneyTitleBreadcrumbItem from 'app/shared/layout/heading/breadcrumbs/journey-title-breadcrumb-item';
import MilestoneTitleBreadcrumbItem from 'app/shared/layout/heading/breadcrumbs/milestone-title-breadcrumb-item';
import AssignmentTitleBreadcrumbItem from 'app/shared/layout/heading/breadcrumbs/assignment-title-breadcrumb-item';
import BackpackBreadcrumbItem from 'app/shared/layout/heading/breadcrumbs/backpack-breadcrumb-item';
import { useGetAssignment } from 'app/shared/services/assignment-api';
import { useGetMilestone } from 'app/shared/services/milestone-api';
import { useGetJourney } from 'app/shared/services/journey-api';
import { Dropdown, DropdownToggle } from 'reactstrap';
import EntityUpdate from 'app/shared/layout/entity-update/entity-update';
import { useDisplayEntityUpdateState } from 'app/shared/contexts/entity-update-context';
import HeadingNew, { SUB_HEADING } from 'app/shared/layout/heading/heading-new';
import { AssignmentContext } from 'app/journey/journey-detail/journey-detail-new';
import { HomeBreadcrumbItem } from 'app/shared/layout/heading/breadcrumbs/home-breadcrumb-item';
import CustomButton from 'app/shared/layout/custom-button/custom-button';
import { translate } from 'react-jhipster';
import { getSubjectHeadingIcon } from 'app/shared/icons';
import BackpackContent from 'app/assignment/assignment-detail/backpack/backpack-content';

interface IProps extends StateProps, RouteComponentProps<{ id: string }> {
  previewEnabled?: boolean;
  assignmentId?: number;
  openedPreviewTab?: PreviewTabType;
  setOpenedPreviewTab?: (tab: PreviewTabType) => void;
  onBackClicked?: () => void;
}

const Backpack = ({
  isAllowedToModify,
  match,
  previewEnabled,
  assignmentId,
  openedPreviewTab,
  setOpenedPreviewTab,
  onBackClicked,
}: IProps) => {
  const { data: assignment } = useGetAssignment(match?.params?.id || assignmentId, !!match?.params?.id || !!assignmentId, {
    template: previewEnabled,
  });
  const { data: milestone } = useGetMilestone(assignment?.milestoneId, !!assignment?.id, previewEnabled);
  const { data: journey } = useGetJourney(milestone?.journeyId, !!milestone?.id && !previewEnabled);
  const { openAssignmentUpdate } = useDisplayEntityUpdateState();
  const [editDropdownOpen, setEditDropdownOpen] = useState(false);

  const tabParams: ITabParams = {
    entityType: ASSIGNMENT,
    tabEntityId: Number(match?.params?.id || assignmentId),
  };

  const toggleEditDropdown = () => setEditDropdownOpen(prevState => !prevState);

  const BackpackHeading = () =>
    previewEnabled ? (
      <Heading
        tabParams={tabParams}
        icon={getHeadingIcon(journey)}
        title={assignment?.title}
        dropdown={
          isAllowedToModify &&
          !previewEnabled && (
            <MenuDropdown
              entityId={assignment?.id}
              entityType={ASSIGNMENT}
              entityTitle={assignment?.title}
              onEditClicked={() => openAssignmentUpdate(assignment?.id)}
            />
          )
        }
        journey={journey}
        previewEnabled={previewEnabled}
        openedPreviewTab={openedPreviewTab}
        setOpenedPreviewTab={setOpenedPreviewTab}
        onBackClicked={onBackClicked}
      />
    ) : (
      <AssignmentContext.Provider value={{ isAllowedToModify, assignment, journey }}>
        <HeadingNew
          headingType={SUB_HEADING}
          tabEntityType={ASSIGNMENT}
          icon={getSubjectHeadingIcon(journey?.subject?.label)}
          title={assignment?.title}
          journey={journey}
          isAllowedToModify={isAllowedToModify}
        >
          {isAllowedToModify && (
            <div className={'heading-buttons d-flex justify-content-end h-100 ml-4'}>
              <Dropdown isOpen={editDropdownOpen} toggle={toggleEditDropdown}>
                <DropdownToggle tag={'div'} className={'h-100'}>
                  <CustomButton
                    iconName={'editNew'}
                    title={translate('entity.action.edit')}
                    buttonType={'secondary'}
                    className={'h-100'}
                    iconWidth={'13'}
                    iconHeight={'13'}
                    size={'md'}
                    outline
                  />
                </DropdownToggle>
                <MenuDropdown
                  entityId={assignment?.id}
                  entityType={ASSIGNMENT}
                  entityTitle={assignment?.title}
                  showDeleteButton={journey?.owner}
                  onEditClicked={() => openAssignmentUpdate(assignment.id)}
                />
              </Dropdown>
            </div>
          )}
        </HeadingNew>
      </AssignmentContext.Provider>
    );

  return (
    <div className="backpack">
      {!previewEnabled && (
        <>
          <HomeBreadcrumbItem />
          <MyJourneysBreadcrumbItem />
          <JourneyTitleBreadcrumbItem journeyId={journey?.id} journeyTitle={journey?.title} />
          <MilestoneTitleBreadcrumbItem journeyId={journey?.id} milestoneId={milestone?.id} milestoneTitle={milestone?.title} />
          <AssignmentTitleBreadcrumbItem journeyId={journey?.id} assignmentId={assignment?.id} assignmentTitle={assignment?.title} />
          <BackpackBreadcrumbItem assignmentId={match?.params?.id} journeyId={journey?.id} />
        </>
      )}
      <BackpackHeading />
      <BackpackContent milestone={milestone} />
      <EntityUpdate />
    </div>
  );
};

const mapStateToProps = ({ authentication, locale }: IRootState) => ({
  currentLocale: locale.currentLocale,
  isAllowedToModify: hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.TEACHER, AUTHORITIES.ADMIN]),
});

type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(Backpack);
