import React, { FC, useState } from 'react';
import { Dropdown, DropdownToggle } from 'reactstrap';
import MyJourneysBreadcrumbItem from 'app/shared/layout/heading/breadcrumbs/my-journeys-breadcrumb-item';
import { AnalyticsAssignmentBreadcrumbItem } from 'app/shared/layout/heading/breadcrumbs/analytics-breadcrumb-item';
import { RouteComponentProps } from 'react-router-dom';
import { ASSIGNMENT } from 'app/shared/util/entity-utils';
import { useGetAssignment } from 'app/shared/services/assignment-api';
import { Spinner } from 'app/shared/layout/spinner/spinner';
import AssignmentTitleBreadcrumbItem from 'app/shared/layout/heading/breadcrumbs/assignment-title-breadcrumb-item';
import JourneyTitleBreadcrumbItem from 'app/shared/layout/heading/breadcrumbs/journey-title-breadcrumb-item';
import MilestoneTitleBreadcrumbItem from 'app/shared/layout/heading/breadcrumbs/milestone-title-breadcrumb-item';
import { useGetJourney } from 'app/shared/services/journey-api';
import { useGetMilestone } from 'app/shared/services/milestone-api';
import HeadingNew, { SUB_HEADING } from 'app/shared/layout/heading/heading-new';
import { AssignmentContext } from 'app/journey/journey-detail/journey-detail-new';
import CustomButton from 'app/shared/layout/custom-button/custom-button';
import { translate } from 'react-jhipster';
import MenuDropdown from 'app/shared/layout/menu-dropdown/menu-dropdown';
import { useDisplayEntityUpdateState } from 'app/shared/contexts/entity-update-context';
import { HomeBreadcrumbItem } from 'app/shared/layout/heading/breadcrumbs/home-breadcrumb-item';
import './assignment-analytics.scss';
import EntityUpdate from 'app/shared/layout/entity-update/entity-update';
import { getSubjectHeadingIcon } from 'app/shared/icons';
import AnalyticsContent from 'app/assignment/assignment-detail/analytics/analytics-content';

export interface DataPoint {
  key: string;
  value: number;
}

const AssignmentAnalytics: FC<RouteComponentProps<{ id: string }>> = ({ match }) => {
  const [editDropdownOpen, setEditDropdownOpen] = useState(false);
  const { openAssignmentUpdate } = useDisplayEntityUpdateState();

  const { data: assignment } = useGetAssignment(match.params.id, !!match.params?.id);
  const { data: milestone } = useGetMilestone(assignment?.milestoneId, !!assignment?.id);
  const { data: journey } = useGetJourney(assignment?.journeyId, !!assignment?.id);

  const toggleEditDropdown = () => setEditDropdownOpen(prevState => !prevState);

  const BreadCrumbs = () => (
    <>
      <HomeBreadcrumbItem />
      <MyJourneysBreadcrumbItem />
      <JourneyTitleBreadcrumbItem journeyTitle={journey?.title} journeyId={journey?.id} />
      <MilestoneTitleBreadcrumbItem journeyId={journey?.id} milestoneId={milestone?.id} milestoneTitle={milestone?.title} />
      <AssignmentTitleBreadcrumbItem assignmentTitle={assignment?.title} assignmentId={assignment?.id} journeyId={assignment?.journeyId} />
      <AnalyticsAssignmentBreadcrumbItem assignmentId={assignment?.id} journeyId={assignment?.journeyId} />
    </>
  );

  return assignment ? (
    <div className="assignment-analytics">
      <BreadCrumbs />
      <AssignmentContext.Provider value={{ isAllowedToModify: true, assignment, journey }}>
        <HeadingNew
          isAllowedToModify
          headingType={SUB_HEADING}
          tabEntityType={ASSIGNMENT}
          title={assignment?.title}
          journey={journey}
          icon={getSubjectHeadingIcon(journey?.subject?.label)}
        >
          <div className={'heading-buttons d-flex justify-content-end h-100 ml-4'}>
            <Dropdown isOpen={editDropdownOpen} toggle={toggleEditDropdown}>
              <DropdownToggle tag={'div'} className={'h-100'}>
                <CustomButton
                  iconName={'editNew'}
                  title={translate('entity.action.edit')}
                  buttonType={'secondary'}
                  className={'h-100'}
                  iconWidth={'13'}
                  iconHeight={'13'}
                  size={'md'}
                  outline
                />
              </DropdownToggle>
              <MenuDropdown
                entityId={assignment.id}
                entityType={ASSIGNMENT}
                entityTitle={assignment?.title}
                showDeleteButton={journey?.owner}
                onEditClicked={() => openAssignmentUpdate(assignment.id)}
              />
            </Dropdown>
          </div>
        </HeadingNew>
      </AssignmentContext.Provider>
      <AnalyticsContent journey={journey} assignment={assignment} />
      <EntityUpdate />
    </div>
  ) : (
    <Spinner />
  );
};

export default AssignmentAnalytics;
