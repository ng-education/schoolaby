import './grade-distribution-bar-chart.scss';
import React, { useEffect, useState } from 'react';
import groupBy from 'lodash/groupBy';
import map from 'lodash/map';
import orderBy from 'lodash/orderBy';
import meanBy from 'lodash/meanBy';
import round from 'lodash/round';
import { ISubmission } from 'app/shared/model/submission/submission.model';
import { IGradingScheme } from 'app/shared/model/grading-scheme.model';
import BarChart from 'app/shared/chart/bar-chart';
import { Spinner } from 'app/shared/layout/spinner/spinner';
import Icon from 'app/shared/icons';
import { findClassNameByGradePercentage, findColorByGradingSchemeValue, SECONDARY } from 'app/shared/util/color-utils';
import { Col, Row } from 'reactstrap';
import { translate } from 'react-jhipster';
import inRange from 'lodash/inRange';
import flatMap from 'lodash/flatMap';
import { DataPoint } from 'app/assignment/assignment-detail/analytics/assignment-analytics';
import { IJourney } from 'app/shared/model/journey.model';
import { getGrade } from 'app/shared/util/grade-util';
import { NoData } from 'app/shared/chart/no-data';
import { IJourneyStudent } from 'app/shared/model/journey-student.model';

type IProps = {
  journey: IJourney;
  submissions: ISubmission[];
  gradingScheme: IGradingScheme;
  students: IJourneyStudent[];
};

const GradeDistributionBarChart = ({ journey, submissions, gradingScheme, students }: IProps) => {
  const [loading, setLoading] = useState<boolean>(true);
  const [averageGrade, setAverageGrade] = useState<number>();
  const [gradeDistribution, setGradeDistribution] = useState<DataPoint[]>();

  const findGradingSchemeValueByGrade = (grade: string) => {
    return gradingScheme.values?.find(value => value.grade?.toLowerCase() === grade?.toLowerCase());
  };

  const gradePercentage = grade => {
    const match = grade.match(/[0-9]+-([0-9]+)%$/);
    return match ? match[1] : grade;
  };

  const orderDataPointsByGradePercentage = (dataPoints: DataPoint[]) =>
    orderBy(
      dataPoints,
      dataPoint => {
        const percentageRangeStart = findGradingSchemeValueByGrade(gradePercentage(dataPoint.key))?.percentageRangeStart;
        return percentageRangeStart ? percentageRangeStart : -1;
      },
      'desc'
    );

  const getAllFeedbacks = () => {
    return flatMap(submissions, submission => submission.submissionFeedbacks);
  };

  const groupByPercentageGradingScheme = () => {
    const ranges = [
      { percentageStartRange: 0, percentageEndRange: 11 },
      { percentageStartRange: 11, percentageEndRange: 21 },
      { percentageStartRange: 21, percentageEndRange: 31 },
      { percentageStartRange: 31, percentageEndRange: 41 },
      { percentageStartRange: 41, percentageEndRange: 51 },
      { percentageStartRange: 51, percentageEndRange: 61 },
      { percentageStartRange: 61, percentageEndRange: 71 },
      { percentageStartRange: 71, percentageEndRange: 81 },
      { percentageStartRange: 81, percentageEndRange: 91 },
      { percentageStartRange: 91, percentageEndRange: 101 },
    ];
    return groupBy(getAllFeedbacks(), feedback => {
      const gradingSchemeValue = findGradingSchemeValueByGrade(feedback?.grade);
      if (gradingSchemeValue) {
        const range = ranges.find(r => inRange(gradingSchemeValue.percentageRangeStart, r.percentageStartRange, r.percentageEndRange));
        return `${range?.percentageStartRange}-${range?.percentageEndRange - 1}%`;
      } else {
        return '!';
      }
    });
  };

  const groupByGrade = () => {
    return groupBy(getAllFeedbacks(), feedback => (feedback?.grade && feedback.grade !== 'null' ? getGrade(feedback) : '!'));
  };

  const createGradeDistribution = () => {
    let grouped;
    if (gradingScheme.code === 'PERCENTAGE_0_100') {
      grouped = groupByPercentageGradingScheme();
    } else {
      grouped = groupByGrade();
    }

    const dataPoints: DataPoint[] = map(grouped, (values, grade) => ({
      key: grade,
      value: values?.length,
    }));

    const unsubmittedCount = students?.length - submissions?.length;
    if (unsubmittedCount > 0) {
      dataPoints.push({
        key: '?',
        value: unsubmittedCount,
      });
    }

    setGradeDistribution(orderDataPointsByGradePercentage(dataPoints));
  };

  const createAverageGrade = () => {
    const feedbacksWithGrade = getAllFeedbacks().filter(feedback => feedback?.grade);
    const average = meanBy(feedbacksWithGrade, value => {
      return findGradingSchemeValueByGrade(value.grade)?.percentageRangeEnd;
    });
    setAverageGrade(round(average, 2));
  };

  const getColors = () =>
    gradeDistribution.map(dataPoint => findColorByGradingSchemeValue(findGradingSchemeValueByGrade(gradePercentage(dataPoint.key))));

  const hasData = () => gradeDistribution?.length !== null && gradeDistribution?.length > 0;

  const gradeBoxClassName = isNaN(averageGrade) ? 'bg-grey' : `bg-${findClassNameByGradePercentage(averageGrade)}`;

  const BarChartFooter = () => (
    <>
      <hr className={'mt-0'} />
      <Row className={'px-3 px-sm-5 pb-3 pb-sm-4 pt-0 pt-sm-2'}>
        <Col xs={3} className={'d-flex align-items-center'}>
          <Icon width={'23px'} height={'23px'} name={'graduate'} fill={SECONDARY} stroke={'transparent'} />
          <span className={'text-muted ml-1'} aria-label={translate('schoolabyApp.assignment.analytics.barChart.studentCount')}>
            {students?.length}
          </span>
        </Col>
        <Col xs={9}>
          <div className={'d-flex align-items-center justify-content-end'}>
            <span className={'text-muted'}>{translate('schoolabyApp.assignment.analytics.barChart.footer')}:</span>
            <span className={`ml-4 average-grade-box rounded py-3 px-3 px-sm-5 ${gradeBoxClassName}`}>
              {isNaN(averageGrade) ? '-' : averageGrade} / 100%
            </span>
          </div>
        </Col>
      </Row>
    </>
  );

  const BarChartHeader = () => (
    <Row className={'p-3'}>
      <Col>
        <h2 className={'d-flex justify-content-center text-muted'}>{translate('schoolabyApp.assignment.analytics.barChart.title')}</h2>
      </Col>
    </Row>
  );

  useEffect(() => {
    if (loading && journey && submissions && gradingScheme) {
      createAverageGrade();
      createGradeDistribution();
      setLoading(false);
    }
  }, [journey, submissions, gradingScheme]);

  return loading ? (
    <Spinner />
  ) : (
    <div className={'grade-distribution d3-chart base-card mt-5 rounded'}>
      <BarChartHeader />
      {hasData() && (
        <>
          <BarChart data={gradeDistribution} colors={getColors()} barWidth={80} />
          <BarChartFooter />
        </>
      )}
      <NoData show={!hasData()} />
    </div>
  );
};

export default GradeDistributionBarChart;
