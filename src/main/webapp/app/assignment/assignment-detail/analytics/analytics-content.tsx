import React, { FC, useContext, useEffect, useState } from 'react';
import GradeDistributionBarChart from 'app/assignment/assignment-detail/analytics/grade-distribution-bar-chart';
import SubmissionTimesBarChart from 'app/assignment/assignment-detail/analytics/submission-times-bar-chart';
import { Row, Col } from 'reactstrap';
import { getSubmissionsByAssignmentId } from 'app/shared/services/submission-api';
import { getGradingScheme } from 'app/shared/services/grading-scheme-api';
import { ISubmission } from 'app/shared/model/submission/submission.model';
import { IGradingScheme } from 'app/shared/model/grading-scheme.model';
import { useGetJourneyStudents } from 'app/shared/services/journey-api';

import './analytics-content.scss';
import { IJourney } from 'app/shared/model/journey.model';
import { IAssignment } from 'app/shared/model/assignment.model';
import { AssignmentContext } from 'app/journey/journey-detail/journey-detail-new';

interface AnalyticsContentProps {
  journey?: IJourney;
  assignment?: IAssignment;
}

const AnalyticsContent: FC<AnalyticsContentProps> = ({ journey: passedJourney, assignment: passedAssignment }) => {
  const [submissions, setSubmissions] = useState<ISubmission[]>();
  const [gradingScheme, setGradingScheme] = useState<IGradingScheme>();
  const assignment = passedAssignment ? passedAssignment : useContext(AssignmentContext)?.assignment;
  const journey = passedJourney ? passedJourney : useContext(AssignmentContext)?.journey;
  const { data: students } = useGetJourneyStudents(assignment?.journeyId, !!assignment?.id);

  useEffect(() => {
    if (assignment && journey) {
      getSubmissionsByAssignmentId(assignment.id).then(resp => setSubmissions(resp));
    }
  }, [assignment, journey]);

  useEffect(() => {
    if (submissions) {
      getGradingScheme(assignment.gradingSchemeId).then(resp => setGradingScheme(resp));
    }
  }, [submissions]);

  return (
    <Row className={'justify-content-center'}>
      <Col xs={'12'} md={'10'}>
        <Row>
          <Col xs={12}>
            <GradeDistributionBarChart journey={journey} gradingScheme={gradingScheme} submissions={submissions} students={students} />
          </Col>
          <Col xs={12}>
            <SubmissionTimesBarChart
              journey={journey}
              assignment={assignment}
              submissions={submissions?.filter(submission => !!submission.submittedForGradingDate)}
              students={students}
            />
          </Col>
        </Row>
      </Col>
    </Row>
  );
};

export default AnalyticsContent;
