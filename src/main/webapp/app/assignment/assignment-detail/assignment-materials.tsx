import React, { FC, useMemo } from 'react';
import { Col } from 'reactstrap';
import { IAssignment } from 'app/shared/model/assignment.model';
import sortBy from 'lodash/sortBy';
import MaterialWideCard from 'app/shared/layout/material-card/material-wide-card';
import { mergeMaterials } from 'app/shared/util/lti-utils';
import { v4 as uuidv4 } from 'uuid';
import { useGetLtiResources } from 'app/shared/services/lti-api';

interface IProps {
  assignment: IAssignment;
  previewEnabled?: boolean;
}

const AssignmentMaterials: FC<IProps> = ({ assignment, previewEnabled }) => {
  const { data: ltiResources } = useGetLtiResources({ assignmentId: assignment?.id }, !!assignment?.id);

  return useMemo(
    () => (
      <>
        {sortBy(mergeMaterials(assignment, ltiResources), 'sequenceNumber')?.map(material => (
          <Col key={`material-card-${uuidv4()}`} sm={12} className={'mt-3 pr-1'}>
            <MaterialWideCard material={material} previewEnabled={previewEnabled} />
          </Col>
        ))}
      </>
    ),
    [assignment?.materials, ltiResources]
  );
};

export default AssignmentMaterials;
