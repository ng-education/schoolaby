import './assignment-detail.scss';
import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';
import { Col, Dropdown, DropdownToggle, Row } from 'reactstrap';
import { translate } from 'react-jhipster';
import { IRootState } from 'app/shared/reducers';
import MenuDropdown from 'app/shared/layout/menu-dropdown/menu-dropdown';
import { useAssignmentState, useGradingSchemeState, useSubmissionsState } from 'app/shared/contexts/entity-detail-context';
import { hasAnyAuthority } from 'app/shared/auth/private-route';
import { AUTHORITIES } from 'app/config/constants';
import { useGetGradingScheme } from 'app/shared/services/grading-scheme-api';
import { useGetSubmissions } from 'app/shared/services/submission-api';
import { ASSIGNMENT } from 'app/shared/util/entity-utils';
import { useGetAssignment } from 'app/shared/services/assignment-api';
import Heading, { ITabParams } from 'app/shared/layout/heading/heading';
import MyJourneysBreadcrumbItem from 'app/shared/layout/heading/breadcrumbs/my-journeys-breadcrumb-item';
import JourneyTitleBreadcrumbItem from 'app/shared/layout/heading/breadcrumbs/journey-title-breadcrumb-item';
import MilestoneTitleBreadcrumbItem from 'app/shared/layout/heading/breadcrumbs/milestone-title-breadcrumb-item';
import { useGetJourney } from 'app/shared/services/journey-api';
import AssignmentTitleBreadcrumbItem from 'app/shared/layout/heading/breadcrumbs/assignment-title-breadcrumb-item';
import { SubmissionGradingProvider } from 'app/shared/contexts/submission-grading-context';
import { Spinner } from 'app/shared/layout/spinner/spinner';
import SubmissionGradingBody from './assignment-detail/submission/submission-grading-body/submission-grading-body';
import SubmissionCreationBody from './assignment-detail/submission/submission-creation-body/submission-creation-body';
import { useGetMilestone } from 'app/shared/services/milestone-api';
import { useDisplayEntityUpdateState } from 'app/shared/contexts/entity-update-context';
import EntityUpdate from 'app/shared/layout/entity-update/entity-update';
import HeadingNew, { SUB_HEADING } from 'app/shared/layout/heading/heading-new';
import { AssignmentContext } from 'app/journey/journey-detail/journey-detail-new';
import { HomeBreadcrumbItem } from 'app/shared/layout/heading/breadcrumbs/home-breadcrumb-item';
import { getSubjectHeadingIcon } from 'app/shared/icons';
import CustomButton from 'app/shared/layout/custom-button/custom-button';
import SpinnerContainer from 'app/shared/layout/spinner-container/spinner-container';

export interface IAssignmentDetailProps extends StateProps, RouteComponentProps<{ id: string }> {
  previewEnabled?: boolean;
  assignmentId?: number;
  closeAssignment?: () => void;
}

export const AssignmentDetail = ({
  match,
  isTeacherOrAdmin,
  account,
  previewEnabled,
  assignmentId,
  closeAssignment,
}: IAssignmentDetailProps) => {
  const { setEntity } = useAssignmentState();
  const { setGradingScheme } = useGradingSchemeState();
  const { setSubmissions } = useSubmissionsState();
  const { data: assignment } = useGetAssignment(match?.params?.id || assignmentId, !!match?.params?.id || !!assignmentId, {
    template: previewEnabled,
  });
  const { data: milestone } = useGetMilestone(assignment?.milestoneId, !!assignment?.id && !previewEnabled);
  const { data: journey } = useGetJourney(assignment?.journeyId, !!assignment?.id && !previewEnabled);
  const isAllowedToModify = !previewEnabled && isTeacherOrAdmin && journey?.teachers?.some(teacher => teacher.user.id === account.id);
  useGetGradingScheme(+assignment?.gradingSchemeId, !!assignment?.gradingSchemeId, setGradingScheme);
  const { openAssignmentUpdate } = useDisplayEntityUpdateState();
  const [editDropdownOpen, setEditDropdownOpen] = useState(false);

  const { isFetching: isFetchingSubmissions } = useGetSubmissions(
    { assignmentId: +match?.params?.id, unPaged: true },
    match?.params?.id && !previewEnabled,
    setSubmissions
  );

  useEffect(() => {
    if (assignment) {
      setEntity(assignment);
    }
  }, [assignment]);

  const tabParams: ITabParams = {
    entityType: ASSIGNMENT,
    additionalSearchParam: `?journeyId=${journey?.id}`,
    tabEntityId: assignment?.id,
  };

  const toggleEditDropdown = () => setEditDropdownOpen(prevState => !prevState);

  const AssignmentHeading = () =>
    previewEnabled ? (
      <Heading
        tabParams={tabParams}
        title={translate('schoolabyApp.assignment.detail.title')}
        dropdown={
          isAllowedToModify && (
            <MenuDropdown
              entityId={assignment.id}
              entityType={ASSIGNMENT}
              journeyId={journey?.id}
              onEditClicked={() => openAssignmentUpdate(assignment?.id)}
              entityTitle={assignment?.title}
            />
          )
        }
        className={!isAllowedToModify ? 'submission-creation-heading' : ''}
        previewEnabled={previewEnabled}
        onBackClicked={closeAssignment}
        journey={journey}
      />
    ) : (
      <HeadingNew
        isAllowedToModify={isAllowedToModify}
        headingType={SUB_HEADING}
        icon={getSubjectHeadingIcon(journey?.subject?.label)}
        title={assignment.title}
        tabEntityType={ASSIGNMENT}
      >
        {isAllowedToModify && (
          <div className={'heading-buttons d-flex justify-content-end h-100 ml-4'}>
            <Dropdown isOpen={editDropdownOpen} toggle={toggleEditDropdown}>
              <DropdownToggle tag={'div'} className={'h-100'}>
                <CustomButton
                  iconName={'editNew'}
                  title={translate('entity.dropdown.edit')}
                  buttonType={'secondary'}
                  className={'h-100'}
                  iconWidth={'13'}
                  iconHeight={'13'}
                  size={'md'}
                  outline
                />
              </DropdownToggle>
              <MenuDropdown
                entityId={assignment.id}
                entityType={ASSIGNMENT}
                showDeleteButton={journey?.owner}
                onEditClicked={() => openAssignmentUpdate(assignment.id)}
                entityTitle={assignment?.title}
              />
            </Dropdown>
          </div>
        )}
      </HeadingNew>
    );

  return assignment && journey ? (
    <>
      <AssignmentContext.Provider value={{ assignment, journey, isAllowedToModify }}>
        <Row className="assignment-heading">
          <Col>
            {!previewEnabled && (
              <>
                <HomeBreadcrumbItem />
                <MyJourneysBreadcrumbItem />
                <JourneyTitleBreadcrumbItem journeyId={journey?.id} journeyTitle={journey?.title} />
                <MilestoneTitleBreadcrumbItem journeyId={journey?.id} milestoneId={milestone?.id} milestoneTitle={milestone?.title} />
                <AssignmentTitleBreadcrumbItem journeyId={journey?.id} assignmentId={assignment?.id} assignmentTitle={assignment?.title} />
              </>
            )}
            <AssignmentHeading />
          </Col>
        </Row>
        <div className="assignment-detail-view">
          <SpinnerContainer displaySpinner={isFetchingSubmissions}>
            <SubmissionGradingProvider>
              {isAllowedToModify ? <SubmissionGradingBody /> : <SubmissionCreationBody journey={journey} />}
            </SubmissionGradingProvider>
          </SpinnerContainer>
          <EntityUpdate />
        </div>
      </AssignmentContext.Provider>
    </>
  ) : (
    <Spinner />
  );
};

const mapStateToProps = ({ authentication }: IRootState) => ({
  account: authentication.account,
  isTeacherOrAdmin: hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.TEACHER, AUTHORITIES.ADMIN]),
});

type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(AssignmentDetail);
