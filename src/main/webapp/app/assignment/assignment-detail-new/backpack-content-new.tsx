import React, { FC, useContext } from 'react';
import MaterialNarrowCardNew from 'app/shared/layout/material-card-new/material-narrow-card-new/material-narrow-card-new';
import MaterialCardModal from 'app/shared/layout/material-card/material-card-modal';
import { useChosenMaterialState } from 'app/shared/contexts/material-context';
import { Col } from 'reactstrap';

import './backpack-content-new.scss';
import { translate } from 'react-jhipster';
import { Spinner } from 'app/shared/layout/spinner/spinner';
import { AssignmentContext } from 'app/journey/journey-detail/journey-detail-new';
import NoContentToDisplay from 'app/shared/layout/no-content-to-display/no-content-to-display';

const BackpackContentNew: FC = () => {
  const { milestone } = useContext(AssignmentContext);
  const { chosenMaterial, setChosenMaterial } = useChosenMaterialState();

  const toggleModal = () => setChosenMaterial(null);

  return !milestone ? (
    <Spinner />
  ) : (
    <div className={'backpack-content-new'}>
      <h5 className={'mb-3'}>{`${translate('schoolabyApp.milestone.detail.title')} "${milestone?.title}" ${translate(
        'schoolabyApp.assignment.materials'
      ).toLowerCase()}`}</h5>
      <Col xs={12} lg={8} className={'p-0 m-auto'}>
        {milestone?.materials?.length ? (
          <div className={'materials-grid w-100'}>
            {milestone?.materials
              .filter(material => material.type !== 'LTI')
              .map(material => (
                <MaterialNarrowCardNew key={material.getKey()} material={material} infoClassName={'px-3'} />
              ))}
          </div>
        ) : (
          <NoContentToDisplay message={translate('schoolabyApp.material.noMaterialsToDisplay')} />
        )}
      </Col>
      <MaterialCardModal toggleModal={toggleModal} material={chosenMaterial} isOpen={!!chosenMaterial} />
    </div>
  );
};

export default BackpackContentNew;
