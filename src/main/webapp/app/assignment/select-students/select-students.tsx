import React, { FC, useState } from 'react';
import { Input } from 'app/shared/form';
import { getFullName } from 'app/shared/util/string-utils';
import { Field } from 'react-final-form';
import { translate } from 'react-jhipster';

import './select-students.scss';
import InviteModal from 'app/journey/journey-detail/invite-modal/invite-modal';
import { Button } from 'reactstrap';
import { IJourneyStudent } from 'app/shared/model/journey-student.model';
import { IJourney } from 'app/shared/model/journey.model';

interface SelectStudentsProps {
  journeyStudents: IJourneyStudent[];
  selectedStudentsIds: number[];
  journey: IJourney;
  selectAll: (selectAll: boolean) => void;
}

const SelectStudents: FC<SelectStudentsProps> = ({ journeyStudents, selectedStudentsIds, journey, selectAll }) => {
  const [inviteModal, setInviteModal] = useState(false);
  const toggleInviteModal = () => setInviteModal(!inviteModal);

  const allSelected = selectedStudentsIds?.length === journeyStudents?.length;

  return (
    <div className={'select-students'}>
      <h4 className={'my-3'}>{translate('schoolabyApp.assignment.detail.students')}</h4>
      {journeyStudents?.length ? (
        <>
          <Field
            id={'select-all-students'}
            name={'selectAllStudents'}
            type={'checkbox'}
            checked={allSelected}
            component={Input}
            labelAfterInput
            labelClassName={'select-all-label'}
            label={translate('schoolabyApp.assignment.update.selectAll')}
            onClick={() => {
              if (allSelected) {
                selectAll(false);
              } else {
                selectAll(true);
              }
            }}
          />
          {journeyStudents?.map(student => (
            <Field
              id={`students-${student.user.id}`}
              key={`select-student-${student.user.id}`}
              name={'students'}
              type={'checkbox'}
              value={student.user.id}
              component={Input}
              labelAfterInput
              label={getFullName(student.user)}
            />
          ))}
        </>
      ) : (
        <span className="d-flex align-items-center justify-content-center flex-column position-relative">
          <img alt={'No students invited'} src="content/images/invite_students_assignment.svg" className="w-100 px-3" />
          <Button size="md" className={'px-5 position-absolute'} outline color={'info'} onClick={toggleInviteModal}>
            {translate('schoolabyApp.journey.detail.inviteStudents')}
          </Button>
          <InviteModal modal={inviteModal} toggle={toggleInviteModal} type={'STUDENT'} journey={journey} />
        </span>
      )}
    </div>
  );
};

export default SelectStudents;
