import React, { FC } from 'react';
import Icon from 'app/shared/icons';
import { BLACK, ICON_LIGHTER_GREY } from 'app/shared/util/color-utils';
import { Draggable } from 'react-beautiful-dnd';
import { IPerson } from 'app/shared/model/person.model';

interface IProps {
  student: IPerson;
  index: number;
  isDragDisabled?: boolean;
}

const StudentDraggable: FC<IProps> = ({ student, index, isDragDisabled = false }) => (
  <Draggable draggableId={student.id.toString()} index={index} isDragDisabled={isDragDisabled}>
    {provided => (
      <div
        data-testid={'student-draggable'}
        ref={provided?.innerRef}
        {...provided?.draggableProps}
        className={'d-flex student-draggable rounded mb-2 mr-1 p-0'}
      >
        <span className={`d-flex flex-grow-1 p-2 justify-content-center ${isDragDisabled && 'disabled'}`}>
          {student.firstName} {student.lastName}
        </span>
        <span className={'d-flex p-2'} {...provided?.dragHandleProps} data-testid={'student-draggable-handle'}>
          <Icon name={'draggable'} fill={isDragDisabled ? ICON_LIGHTER_GREY : BLACK} />
        </span>
      </div>
    )}
  </Draggable>
);

export default StudentDraggable;
