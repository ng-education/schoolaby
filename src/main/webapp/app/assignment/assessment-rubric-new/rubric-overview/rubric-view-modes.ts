enum RubricViewModes {
  GRADE,
  VIEW,
  TEMPLATE_PREVIEW,
}

export default RubricViewModes;
