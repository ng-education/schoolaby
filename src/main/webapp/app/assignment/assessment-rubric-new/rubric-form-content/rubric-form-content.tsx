import React, { FC, useMemo, useState } from 'react';
import './rubric-form-content.scss';
import { IAssignment } from 'app/shared/model/assignment.model';
import { Field, useForm } from 'react-final-form';
import { Input } from 'app/shared/form';
import ContentCard from 'app/shared/layout/content-card/content-card';
import { FieldArray } from 'react-final-form-arrays';
import { createDefaultCriterion, createDefaultCriterionLevel } from 'app/assignment/assessment-rubric-new/assessment-rubric';
import { translate } from 'react-jhipster';
import { Button, Col, Row } from 'reactstrap';
import sortBy from 'lodash/sortBy';
import { ICriterion } from 'app/shared/model/rubric/criterion.model';
import CustomButton from 'app/shared/layout/custom-button/custom-button';
import CriterionUpdate from 'app/assignment/assessment-rubric-new/rubric-form-content/criterion-update/criterion-update';
import DeleteModal from 'app/shared/layout/delete-modal/delete-modal';
import { ICriterionLevel } from 'app/shared/model/rubric/criterion-level.model';
import ConfirmationModalNew from 'app/shared/layout/confirmation-modal-new/confirmation-modal-new';
import { useRubricTemplateState, useSelectedRubricState } from 'app/shared/contexts/entity-update-context';
import { clearRubric } from 'app/shared/util/rubric-utils';
import { IRootState } from 'app/shared/reducers';
import { connect } from 'react-redux';

export const increaseSequenceNumbers = (rubricItems: ICriterion[] | ICriterionLevel[], startingAt: number) => {
  rubricItems.forEach(criterion => {
    if (criterion.sequenceNumber >= startingAt) {
      criterion.sequenceNumber++;
    }
    return criterion;
  });
};

interface RubricFormContentProps extends StateProps {
  assignment: IAssignment;
  setRubricAndRestartForm: (rubric, form) => void;
}

const RubricFormContent: FC<RubricFormContentProps> = ({ assignment, setRubricAndRestartForm, locale }) => {
  const [deleteCriterionIndex, setDeleteCriterionIndex] = useState(undefined);
  const { selectedRubric, setSelectedRubric } = useSelectedRubricState();
  const {
    savingRubricAsTemplate,
    unSavingRubricAsTemplate,
    setSavingRubricAsTemplate,
    setUnSavingRubricAsTemplate,
    selectingRubricTemplate,
    setSelectingRubricTemplate,
  } = useRubricTemplateState();
  const form = useForm();

  const createNewCriterion = (index: number) => {
    const rubric = form.getState().values;

    const newCriterion = {
      ...createDefaultCriterion(),
      sequenceNumber: index,
      levels: Array.from(Array(3), (level, levelIndex) => createDefaultCriterionLevel(levelIndex)),
    };
    increaseSequenceNumbers(rubric.criterions, index);
    rubric.criterions.push(newCriterion);
    rubric.criterions = sortBy(rubric.criterions, 'sequenceNumber');
    setRubricAndRestartForm(rubric, form);
  };

  const removeCriterion = () => {
    const rubric = form.getState().values;
    rubric.criterions.splice(deleteCriterionIndex, 1);
    rubric.criterions.forEach((criterion, index) => (criterion.sequenceNumber = index));
    setDeleteCriterionIndex(undefined);
    setRubricAndRestartForm(rubric, form);
  };

  const closeDeleteCriterionDialog = () => setDeleteCriterionIndex(undefined);

  const AddCriterionButton: FC<{ newCriterionIndex: number }> = ({ newCriterionIndex }) => (
    <div className="my-2 text-center">
      <Button color={''} onClick={() => createNewCriterion(newCriterionIndex)} className={'add-criterion p-0'} type="button">
        + {translate('schoolabyApp.assignment.rubric.criterion.addNew')}
      </Button>
    </div>
  );

  const handleSelectTemplate = () => {
    const rubric = { ...selectedRubric };
    clearRubric(rubric);
    setRubricAndRestartForm(rubric, form);
    setSelectedRubric(undefined);
    setSelectingRubricTemplate(false);
  };

  return useMemo(
    () => (
      <div className="rubric-form-content">
        <ContentCard>
          <Row>
            <Col lg={6}>
              <Field
                id="rubric-title"
                name="title"
                type="text"
                label={translate('schoolabyApp.assignment.rubric.title')}
                placeholder={translate('schoolabyApp.assignment.rubric.titlePlaceholder')}
                containerClassName="m-0"
                component={Input}
              />
            </Col>
            <Col lg={6} className="d-flex justify-content-end align-items-center">
              {form.getState().values?.isTemplate ? (
                <CustomButton
                  buttonType="danger"
                  title={translate('schoolabyApp.assignment.rubric.template.removeTemplate')}
                  className="px-4 mt-2 mt-lg-0"
                  onClick={() => setUnSavingRubricAsTemplate(true)}
                />
              ) : (
                <CustomButton
                  buttonType="primary"
                  title={translate('schoolabyApp.assignment.rubric.template.saveAsTemplate')}
                  className="px-4 mt-2 mt-lg-0"
                  onClick={() => setSavingRubricAsTemplate(true)}
                />
              )}
            </Col>
          </Row>
        </ContentCard>
        <AddCriterionButton newCriterionIndex={0} />
        <FieldArray name="criterions" subscription={{ pristine: true }}>
          {({ fields: criterionFields }) =>
            useMemo(
              () =>
                criterionFields.map((criterionName, index) => (
                  <div key={criterionName}>
                    <CriterionUpdate
                      formFieldName={criterionName}
                      onDeleteClick={() => setDeleteCriterionIndex(index)}
                      criterionIndex={index}
                      indicateDelete={deleteCriterionIndex === index}
                      setRubricAndRestartForm={setRubricAndRestartForm}
                      deleteDisabled={criterionFields.length === 1}
                    />
                    <AddCriterionButton newCriterionIndex={index + 1} />
                  </div>
                )),
              [criterionFields]
            )
          }
        </FieldArray>
        {deleteCriterionIndex !== undefined && (
          <DeleteModal
            isOpen
            toggle={closeDeleteCriterionDialog}
            deleteAction={removeCriterion}
            customDescription={translate('schoolabyApp.assignment.rubric.criterion.deleteQuestion')}
          />
        )}
        {savingRubricAsTemplate && (
          <ConfirmationModalNew
            isModalOpen
            closeModal={() => {
              form.getState().values.isTemplate = false;
              setSavingRubricAsTemplate(false);
            }}
            modalTitle={translate('schoolabyApp.assignment.rubric.template.saveModalTitle')}
            handleConfirmation={() => {
              form.getState().values.isTemplate = true;
              form.submit();
            }}
            errorMessage={form.getState().hasValidationErrors && translate('schoolabyApp.assignment.rubric.template.requiredFieldsError')}
            confirmationButtonDisabled={form.getState().hasValidationErrors}
          >
            <p className="mt-2">{translate('schoolabyApp.assignment.rubric.template.saveModalContent')}</p>
          </ConfirmationModalNew>
        )}
        {unSavingRubricAsTemplate && (
          <ConfirmationModalNew
            isModalOpen={unSavingRubricAsTemplate}
            closeModal={() => {
              form.getState().values.isTemplate = true;
              setUnSavingRubricAsTemplate(false);
            }}
            modalTitle={translate('schoolabyApp.assignment.rubric.template.unSaveModalTitle')}
            handleConfirmation={() => {
              form.getState().values.isTemplate = false;
              form.submit();
            }}
          >
            <p className="mt-2">{translate('schoolabyApp.assignment.rubric.template.unSaveModalContent')}</p>
          </ConfirmationModalNew>
        )}
        {selectingRubricTemplate && (
          <ConfirmationModalNew
            isModalOpen
            closeModal={() => {
              setSelectingRubricTemplate(false);
            }}
            modalTitle={translate('schoolabyApp.assignment.rubric.template.selectModalTitle')}
            handleConfirmation={handleSelectTemplate}
          >
            <p className="mt-2">{translate('schoolabyApp.assignment.rubric.template.selectModalContent')}</p>
          </ConfirmationModalNew>
        )}
      </div>
    ),
    [assignment, deleteCriterionIndex, savingRubricAsTemplate, unSavingRubricAsTemplate, selectingRubricTemplate, locale]
  );
};

const mapStateToProps = ({ locale }: IRootState) => ({
  locale: locale.currentLocale,
});

type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(RubricFormContent);
