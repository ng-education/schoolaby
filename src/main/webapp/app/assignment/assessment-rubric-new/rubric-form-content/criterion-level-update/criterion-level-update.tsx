import React, { FC } from 'react';
import './criterion-level-update.scss';
import { Input } from 'app/shared/form';
import Icon from 'app/shared/icons';
import { Field } from 'react-final-form';
import { translate } from 'react-jhipster';
import { Button } from 'reactstrap';
import classNames from 'classnames';

interface CriterionProps {
  formFieldName: string;
  onDeleteClick: () => void;
  deleteDisabled?: boolean;
  indicateDelete?: boolean;
}

const CriterionLevelUpdate: FC<CriterionProps> = ({ formFieldName, onDeleteClick, deleteDisabled = false, indicateDelete = false }) => (
  <div
    className={classNames({
      'criterion-level-update p-2 position-relative': true,
      delete: indicateDelete,
    })}
  >
    {!deleteDisabled && (
      <div className="criterion-level-remove position-absolute">
        <Button
          color={''}
          className="p-0"
          type="button"
          onClick={onDeleteClick}
          aria-label={translate('schoolabyApp.assignment.rubric.criterion.level.delete')}
        >
          <Icon name="trashNew" />
        </Button>
      </div>
    )}
    <Field
      id={`${formFieldName}.title`}
      name={`${formFieldName}.title`}
      type="text"
      label={translate('schoolabyApp.assignment.rubric.criterion.level.title')}
      placeholder={translate('schoolabyApp.assignment.rubric.addTitle')}
      component={Input}
    />
    <Field
      id={`${formFieldName}.points`}
      name={`${formFieldName}.points`}
      type="number"
      label={translate('schoolabyApp.assignment.rubric.criterion.level.points')}
      placeholder={translate('schoolabyApp.assignment.rubric.criterion.level.pointsPlaceholder')}
      component={Input}
    />
    <Field
      id={`${formFieldName}.description`}
      name={`${formFieldName}.description`}
      type="textarea"
      label={translate('schoolabyApp.assignment.rubric.criterion.level.description')}
      placeholder={translate('schoolabyApp.assignment.rubric.criterion.level.descriptionPlaceholder')}
      component={Input}
    />
  </div>
);

export default CriterionLevelUpdate;
