import React, { FC, useEffect, useRef, useState } from 'react';
import './assessment-rubric.scss';
import HeadingNew, { SUB_HEADING } from 'app/shared/layout/heading/heading-new';
import { translate } from 'react-jhipster';
import { useHistory } from 'react-router-dom';
import { IAssignment } from 'app/shared/model/assignment.model';
import { HistoryStateProps } from 'app/marketplace/marketplace';
import ContentContainer from 'app/shared/layout/content-container/content-container';
import { Col, Container, Row } from 'reactstrap';
import CustomButton from 'app/shared/layout/custom-button/custom-button';
import { Form } from 'react-final-form';
import RubricFormContent from 'app/assignment/assessment-rubric-new/rubric-form-content/rubric-form-content';
import arrayMutators from 'final-form-arrays';
import { ICriterion } from 'app/shared/model/rubric/criterion.model';
import { ICriterionLevel } from 'app/shared/model/rubric/criterion-level.model';
import { IRubric } from 'app/shared/model/rubric/rubric.model';
import { createYupValidator } from 'app/shared/util/form-utils';
import { useAssignmentRubricSchema } from 'app/shared/schema/update-entity';
import { IRootState } from 'app/shared/reducers';
import { connect } from 'react-redux';
import { useDeleteRubric, usePostRubric, useUpdateRubric } from 'app/shared/services/rubric-api';
import { clearRubric } from 'app/shared/util/rubric-utils';
import { useRubricTemplateState, useSelectedRubricState } from 'app/shared/contexts/entity-update-context';
import { toast } from 'react-toastify';
import RubricSearchDropdown from 'app/shared/layout/rubric-search-dropdown/rubric-search-dropdown';
import RubricOverview from 'app/assignment/assessment-rubric-new/rubric-overview/rubric-overview';
import cloneDeep from 'lodash/cloneDeep';
import RubricViewModes from 'app/assignment/assessment-rubric-new/rubric-overview/rubric-view-modes';
import { useGetJourney } from 'app/shared/services/journey-api';
import { AssignmentContext } from 'app/journey/journey-detail/journey-detail-new';
import { hasAnyAuthority } from 'app/shared/auth/private-route';
import { AUTHORITIES } from 'app/config/constants';

export const createDefaultCriterionLevel = (sequenceNumber = 0): ICriterionLevel => ({
  title: '',
  points: undefined,
  description: '',
  sequenceNumber,
});

export const createDefaultCriterion = (sequenceNumber = 0): ICriterion => ({
  title: '',
  description: '',
  sequenceNumber,
  levels: Array.from(Array(3), (levelItem, levelIndex) => createDefaultCriterionLevel(levelIndex)),
});

const createDefaultRubric = (): IRubric => ({
  title: '',
  isTemplate: false,
  criterions: Array.from(Array(3), (criterionItem, criterionIndex) => createDefaultCriterion(criterionIndex)),
});

const AssessmentRubric: FC<StateProps> = ({ locale, account, isTeacher }) => {
  const history = useHistory();
  const [back, setBack] = useState<Location>();
  const backHistoryState = useRef(null);
  const [assignment, setAssignment] = useState<IAssignment>(undefined);
  const [journeyId, setJourneyId] = useState<number>(undefined);
  const { data: journey } = useGetJourney(journeyId, !!journeyId);
  const [currentRubric, setCurrentRubric] = useState(createDefaultRubric());
  const { mutate: createRubric } = usePostRubric();
  const { mutate: updateRubric } = useUpdateRubric();
  const { mutate: deleteRubric } = useDeleteRubric();
  const { selectedRubric, setSelectedRubric } = useSelectedRubricState();
  const {
    savingRubricAsTemplate,
    unSavingRubricAsTemplate,
    setSavingRubricAsTemplate,
    setUnSavingRubricAsTemplate,
  } = useRubricTemplateState();
  const isAllowedToModify = isTeacher && journey?.teachers.some(teacher => teacher.user.id === account.id);

  const setRubricAndRestartForm = (rubric, form) => {
    setCurrentRubric(rubric);
    form.restart();
  };

  const setValuesAndCurrentRubric = (currentValues, newValues) => {
    currentValues = newValues;
    setCurrentRubric(currentValues);
  };

  const showToastBasedOnState = () => {
    if (savingRubricAsTemplate) {
      toast.success(translate('schoolabyApp.assignment.rubric.template.savedToast'));
    }
    if (unSavingRubricAsTemplate) {
      toast.success(translate('schoolabyApp.assignment.rubric.template.unSavedToast'));
    }
  };

  useEffect(() => {
    return () => {
      if (history.action === 'POP') {
        const historyState = backHistoryState.current;
        history.replace({
          ...historyState.back,
          state: { ...historyState.entity },
        });
      }
    };
  }, [history]);

  useEffect(() => {
    if (history.location.state) {
      const historyState = history.location.state as HistoryStateProps<IAssignment>;
      setAssignment(historyState.entity);
      setBack(historyState.back);
      backHistoryState.current = historyState;
      setJourneyId(historyState.journeyId);
      !!historyState.entity.rubric && setCurrentRubric(cloneDeep(historyState.entity.rubric));
    } else {
      history.push('/not_found');
    }
  }, [history.location.state]);

  const handleCancel = () => {
    history.replace({
      ...back,
      state: { ...assignment },
    });
  };

  const calculateMaxPoints = (rubric: IRubric) => {
    return rubric.criterions.reduce((total, criterion) => total + Math.max(...criterion.levels.map(level => level.points)), 0);
  };

  const attachRubricToHistoryStateAndClose = (rubric: IRubric) => {
    rubric.maxPoints = calculateMaxPoints(rubric);
    history.replace({
      ...back,
      state: {
        ...assignment,
        rubric,
      },
    });
  };

  const updateHistoryState = (rubric: IRubric) => {
    const currentState = history.location.state as HistoryStateProps<IAssignment>;
    currentState.entity = { ...currentState.entity, rubric };
    currentState.back = back;

    history.replace({
      ...history.location,
      state: currentState,
    });
  };

  const decideTemplateCreateOrUpdateOrDelete = values => {
    if (!selectedRubric && !values.id) {
      createRubric(values, {
        onSuccess({ data }) {
          setValuesAndCurrentRubric(values, data);
          updateHistoryState(data);
          showToastBasedOnState();
        },
      });
    } else if (unSavingRubricAsTemplate && selectedRubric ? !selectedRubric.assignmentId : !values.assignmentId) {
      deleteRubric(selectedRubric ? selectedRubric.id : values.id, {
        onSuccess() {
          if (selectedRubric) {
            setSelectedRubric(undefined);
            values.isTemplate = currentRubric.isTemplate;
          } else {
            clearRubric(values);
            values.assignmentId = undefined;
            setCurrentRubric(values);
          }
          updateHistoryState(values);
          showToastBasedOnState();
        },
      });
    } else {
      updateRubric(selectedRubric ? { ...selectedRubric, isTemplate: false } : values, {
        onSuccess({ data }) {
          if (selectedRubric) {
            setSelectedRubric(undefined);
            values.isTemplate = currentRubric.isTemplate;
            updateHistoryState(values);
          } else {
            updateHistoryState(data);
            setValuesAndCurrentRubric(values, data);
          }
          showToastBasedOnState();
        },
      });
    }
  };

  const decideRubricSaveOrUpdate = values => {
    if (values.id && values.isTemplate) {
      updateRubric(values, {
        onSuccess({ data }) {
          attachRubricToHistoryStateAndClose(data);
        },
      });
    } else {
      attachRubricToHistoryStateAndClose(values);
    }
  };

  const saveRubric = values => {
    if (savingRubricAsTemplate || unSavingRubricAsTemplate) {
      decideTemplateCreateOrUpdateOrDelete(values);

      setSavingRubricAsTemplate(false);
      setUnSavingRubricAsTemplate(false);
    } else {
      decideRubricSaveOrUpdate(values);
    }
  };

  return (
    <div className="assessment-rubric-view">
      <ContentContainer spinner={!assignment} className="pt-0">
        <Row className="align-items-center">
          <Col md={8} className="assessment-rubric-heading">
            <HeadingNew
              isAllowedToModify
              title={translate('schoolabyApp.assignment.rubric.headingTitle', { assignmentTitle: assignment?.title })}
              headingType={SUB_HEADING}
              hideBreadcrumbs
            />
          </Col>
          <Col md={4}>
            <RubricSearchDropdown />
          </Col>
        </Row>

        <Form
          onSubmit={saveRubric}
          initialValues={currentRubric}
          mutators={{ ...arrayMutators }}
          validate={createYupValidator(useAssignmentRubricSchema(locale))}
          validateOnBlur={true}
          subscription={{ values: true, pristine: true }}
        >
          {({ handleSubmit }) => (
            <form onSubmit={handleSubmit} className="mt-4">
              <Container>
                <RubricFormContent assignment={assignment} setRubricAndRestartForm={setRubricAndRestartForm} />
              </Container>
              <footer className="bg-white position-absolute w-100 py-3">
                <Container>
                  <div className="d-flex justify-content-end">
                    <CustomButton
                      title={translate('entity.action.cancel')}
                      outline
                      buttonType={'cancel'}
                      className="mr-2"
                      onClick={handleCancel}
                    />
                    <CustomButton title={translate('entity.action.save')} buttonType={'primary'} onClick={handleSubmit} />
                  </div>
                </Container>
              </footer>
            </form>
          )}
        </Form>
      </ContentContainer>
      {!!selectedRubric && (
        <AssignmentContext.Provider value={{ isAllowedToModify, assignment, journey }}>
          <RubricOverview viewMode={RubricViewModes.TEMPLATE_PREVIEW} />
        </AssignmentContext.Provider>
      )}
    </div>
  );
};

const mapStateToProps = ({ locale, authentication }: IRootState) => ({
  locale: locale.currentLocale,
  account: authentication.account,
  isTeacher: hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.TEACHER, AUTHORITIES.ADMIN]),
});

type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(AssessmentRubric);
