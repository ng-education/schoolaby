import './conversations.scss';
import React, { useEffect, useRef, useState } from 'react';
import { parseHeaderForLinks, translate } from 'react-jhipster';
import { Col, Row } from 'reactstrap';
import { IRootState } from 'app/shared/reducers';
import { connect } from 'react-redux';
import { RouteComponentProps, useHistory, withRouter } from 'react-router-dom';
import EntityListBox from 'app/shared/layout/entity-list-box/entity-list-box';
import Avatar from 'app/shared/layout/header/avatar';
import { IMessage } from 'app/shared/model/message.model';
import { createMessage, findAllMessages } from 'app/shared/services/message-api';
import { createChat, getChatsByJourney } from 'app/shared/services/chat-api';
import { IChat } from 'app/shared/model/chat.model';
import { JOURNEY, SUBMISSION } from 'app/shared/util/entity-utils';
import { useQueryParam } from 'app/shared/hooks/useQueryParam';
import { hasAnyAuthority } from 'app/shared/auth/private-route';
import { AUTHORITIES } from 'app/config/constants';
import { getFullName, getFullNames } from 'app/shared/util/string-utils';
import { getJourneys, useGetJourney } from 'app/shared/services/journey-api';
import { IJourney } from 'app/shared/model/journey.model';
import ReactMarkdown from 'react-markdown';
import { useGeneratedChatState } from 'app/shared/contexts/chat-context';
import InfiniteScrollComponent from 'app/shared/layout/infinite-scroll-component/infinite-scroll-component';
import JourneyAvatar from 'app/conversations/journey-avatar/journey-avatar';
import HeadingNew, { SUB_HEADING } from 'app/shared/layout/heading/heading-new';
import { useGetSubmission } from 'app/shared/services/submission-api';

export interface IConversations extends StateProps, RouteComponentProps<{ url: string }> {}

let chatNextPage: number;
let journeyNextPage: number;

export const Conversations = ({ isStudent, account, location }: IConversations) => {
  const [journeys, setJourneys] = useState<IJourney[]>([]);
  const [allJourneyChats, setAllJourneyChats] = useState<IChat[]>();
  const [input, setInput] = useState('');
  const [messages, setMessages] = useState<IMessage[]>([]);
  const [selectedChat, setSelectedChat] = useState<IChat>();
  const [selectedJourney, setSelectedJourney] = useState<IJourney>();
  const [{ submissionId, journeyId }] = useQueryParam<{ submissionId: string; journeyId: string }>();
  const { data: submission } = useGetSubmission(submissionId, !!submissionId);
  const { generatedChat } = useGeneratedChatState();
  const { data: journey } = useGetJourney(journeyId, !!journeyId);
  const history = useHistory();

  const ref = useRef(null);
  useEffect(() => {
    ref && ref.current && ref.current.scrollIntoView({ inline: 'center' });
  });

  const getMessages = () => {
    findAllMessages({
      submissionId: selectedChat.submissionId,
      journeyId: !selectedChat.submissionId ? selectedChat.journeyId : undefined,
      sort: 'id,desc',
    }).then(response => setMessages(response.data.slice().reverse()));
  };

  const checkURL = (url: string) => {
    const currentUrl = location.pathname + location.search;
    currentUrl !== url && history.push(url);
  };

  useEffect(() => {
    getJourneys().then(resp => {
      journeyNextPage = parseHeaderForLinks(resp.headers.link).next;
      setJourneys(resp.data);
    });
  }, []);

  useEffect(() => {
    if (selectedJourney) {
      getChatsByJourney(SUBMISSION, selectedJourney.id).then(submissionChatsData => {
        chatNextPage = parseHeaderForLinks(submissionChatsData.headers.link).next;
        getChatsByJourney(JOURNEY, selectedJourney.id).then(journeyChatData => {
          let journeyGeneralChat;
          if (journeyChatData?.data?.length) {
            journeyGeneralChat = journeyChatData.data[0];
          } else if (selectedJourney) {
            journeyGeneralChat = {
              journeyId: selectedJourney.id,
              title: selectedJourney.title,
            };
          }
          setAllJourneyChats([journeyGeneralChat, ...submissionChatsData.data]);
          setSelectedChat(journeyGeneralChat);
        });
      });
    }
  }, [selectedJourney]);

  useEffect(() => {
    if (selectedChat?.submissionId) {
      getMessages();
      checkURL(`/chats?journeyId=${selectedChat.journeyId}&submissionId=${selectedChat.submissionId}`);
    } else if (selectedChat?.journeyId) {
      getMessages();
      checkURL(`/chats?journeyId=${selectedChat.journeyId}`);
    } else {
      setMessages([]);
    }
  }, [selectedChat]);

  useEffect(() => {
    const hasJourney = journeys.find(j => j.id === +journeyId);
    if (!hasJourney && journey) {
      setSelectedJourney(journey);
      setJourneys([...journeys, journey]);
    } else if (journey) {
      setSelectedJourney(journey);
    }
  }, [journey]);

  useEffect(() => {
    if (submissionId && allJourneyChats?.length) {
      const submissionChat = allJourneyChats?.find(c => c.submissionId === +submissionId);
      if (!submissionChat) {
        if (generatedChat?.journeyId === selectedJourney.id) {
          setAllJourneyChats([...allJourneyChats, generatedChat]);
          setSelectedChat(generatedChat);
        }
      } else {
        setSelectedChat(submissionChat);
      }
    }
  }, [submissionId, allJourneyChats]);

  const messageContainer = (message: IMessage, key?: number) => {
    const isOwner = message.creatorId === account.id;

    return (
      <div key={key} ref={messages[messages.length - 1] === message ? ref : null}>
        {isOwner ? (
          <div className="message-wrap position-right my-4">
            <div className="message border border-success message-owner p-3 mr-5 preserve-space">
              <ReactMarkdown source={message.value} />
            </div>
            <Avatar fullName={getFullName(account)} className={'message-avatar position-right'} />
          </div>
        ) : (
          message?.creator && (
            <div className="message-wrap position-left my-4">
              <Avatar fullName={message?.creator} className={'float-left message-avatar position-left'} />
              <div className="message border border-warning p-3 ml-5 preserve-space">
                <ReactMarkdown source={message.value} />
              </div>
            </div>
          )
        )}
      </div>
    );
  };

  const handleChatOnClick = entity => setSelectedChat(entity);

  const getRecipients = (conversation: IChat) => {
    return conversation.people?.filter(p => p.id !== account.id) || [account];
  };

  const generateChat = () => {
    const chatEntity = generatedChat
      ? generatedChat
      : {
          journeyId: selectedChat.journeyId,
          title: selectedChat.title,
          submissionId: selectedChat.submissionId,
        };
    return createChat(chatEntity);
  };

  const saveMessage = async event => {
    event.preventDefault();
    if (input && input !== '') {
      let chatResponse;
      if (!messages.length) {
        chatResponse = await generateChat();
        setSelectedChat(chatResponse.data);
      }
      const entity: IMessage = {
        value: input,
        creatorId: account.id,
        chatId: selectedChat?.id ? selectedChat.id : chatResponse.data.id,
      };
      createMessage(entity).then(() => {
        setInput('');
        getMessages();
      });
    }
  };

  const isChatActive = (chat: IChat): boolean => {
    if (chat.id) {
      return chat.id === selectedChat?.id;
    } else if (chat.submissionId) {
      return chat.submissionId === selectedChat?.submissionId;
    } else if (chat.journeyId) {
      return chat.journeyId === selectedChat?.journeyId && !selectedChat?.submissionId;
    } else {
      return false;
    }
  };

  const getNewChats = async () => {
    const response = await getChatsByJourney(SUBMISSION, selectedJourney.id, chatNextPage);
    chatNextPage = parseHeaderForLinks(response.headers.link).next;
    setAllJourneyChats([...allJourneyChats, ...response.data]);
  };

  const getNewJourneys = async () => {
    const response = await getJourneys('', journeyNextPage);
    journeyNextPage = parseHeaderForLinks(response.headers.link).next;
    setJourneys([...journeys, ...response.data]);
  };

  function getOnClick(j) {
    return setSelectedJourney(j);
  }

  const renderJourneys = () => {
    return (
      <div id={'journeyList'} className={'p-0 conversations-list border-bottom'}>
        <InfiniteScrollComponent
          dataLength={journeys.length}
          next={getNewJourneys}
          hasMore={!isNaN(journeyNextPage)}
          scrollableTarget="journeyList"
        >
          {journeys?.map((j, i) => (
            <EntityListBox
              avatar={<JourneyAvatar journey={j} />}
              title={j.title}
              entity={j}
              onClick={e => getOnClick(j)}
              key={i}
              isActive={j.id === selectedJourney?.id}
            />
          ))}
        </InfiniteScrollComponent>
      </div>
    );
  };

  const renderJourneyChats = () => {
    return (
      <div id={'chatList'} className={'p-0 conversations-list'}>
        <InfiniteScrollComponent
          dataLength={allJourneyChats?.length}
          next={getNewChats}
          hasMore={!isNaN(chatNextPage)}
          scrollableTarget="chatList"
        >
          {allJourneyChats?.map((conv, i) => (
            <div key={i}>
              {conv.submissionId ? (
                <EntityListBox
                  avatar={<Avatar fullName={getFullNames(getRecipients(conv))} />}
                  entity={conv}
                  onClick={handleChatOnClick}
                  subTitle={isStudent ? `${getFullNames(getRecipients(conv))}` : conv.title}
                  isActive={isChatActive(conv)}
                  title={isStudent ? conv.title : `${getFullNames(getRecipients(conv))}`}
                />
              ) : (
                <EntityListBox
                  avatar={<JourneyAvatar journey={selectedJourney} />}
                  entity={conv}
                  onClick={handleChatOnClick}
                  subTitle={conv.title}
                  isActive={isChatActive(conv)}
                  title={translate('schoolabyApp.chat.generalChat')}
                />
              )}
            </div>
          ))}
        </InfiniteScrollComponent>
      </div>
    );
  };

  const ConversationTitle = () => {
    const getConversationTitle = () => {
      if (selectedChat?.submissionId && submission?.assignmentTitle) {
        return `${getFullNames(getRecipients(selectedChat))} (${submission?.assignmentTitle})`;
      }
      if (selectedChat?.journeyId && !selectedChat?.submissionId) {
        return translate('schoolabyApp.chat.generalChat');
      }
      return null;
    };

    return <h2 className="conversation-title text-truncate m-0">{getConversationTitle()}</h2>;
  };

  return (
    <div className="conversations-view d-flex flex-column">
      <Row className="align-items-center">
        <Col md={4} lg={3} className="conversations-heading">
          <HeadingNew isAllowedToModify={false} headingType={SUB_HEADING} title={translate('schoolabyApp.chat.title')} hideBreadcrumbs />
        </Col>
        <Col md={8} lg={9}>
          <ConversationTitle />
        </Col>
      </Row>
      <Row className="justify-content-center conversations-body border-top flex-grow-1">
        <Col lg="3" className="chats border-right flex-wrap bg-white p-0">
          <Row className={'m-0 p-0 h-100'}>
            <Col md={'12'} className={'chat-selections'}>
              <div className={'chat-title d-flex align-items-center'}>
                <h3>{translate('schoolabyApp.chat.journeys')}</h3>
              </div>
              {renderJourneys()}
            </Col>
            <Col md={'12'} className={'chat-selections'}>
              <div className={'chat-title d-flex align-items-center'}>
                <h3>{translate('schoolabyApp.chat.conversations')}</h3>
              </div>
              {allJourneyChats?.length > 0 && renderJourneyChats()}
            </Col>
          </Row>
        </Col>
        <Col lg={'9'}>
          <Row className={'m-0 p-0 h-100'}>
            <Col md="12" className={'py-2 mt-2 conversation'}>
              {messages.map((message, i) => messageContainer(message, i))}
            </Col>
            {selectedChat && (
              <Col className={'mt-4 mb-2'}>
                <form>
                  <input value={input} onChange={e => setInput(e.target.value)} />
                  <button onClick={e => saveMessage(e)}>{translate('schoolabyApp.chat.send')}</button>
                </form>
              </Col>
            )}
          </Row>
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = ({ authentication }: IRootState) => ({
  account: authentication.account,
  isStudent: hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.STUDENT]),
});

type StateProps = ReturnType<typeof mapStateToProps>;

export default withRouter(connect(mapStateToProps)(Conversations));
