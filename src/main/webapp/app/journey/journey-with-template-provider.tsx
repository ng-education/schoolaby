import React from 'react';
import { JourneyTemplateProvider } from 'app/shared/contexts/journey-template-context';
import Journey from 'app/journey/journey';

const JourneyWithTemplateProvider = props => (
  <JourneyTemplateProvider>
    <Journey {...props} />
  </JourneyTemplateProvider>
);

export default JourneyWithTemplateProvider;
