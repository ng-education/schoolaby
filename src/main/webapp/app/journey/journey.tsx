import React, { useMemo, useState } from 'react';
import { translate } from 'react-jhipster';
import { RouteComponentProps, useHistory } from 'react-router-dom';
import { Button, Col, Row } from 'reactstrap';
import { IJourney, JourneyState } from 'app/shared/model/journey.model';
import { connect } from 'react-redux';
import EntityCard, { IEntityCardProps } from 'app/shared/layout/entity-card/entity-card';
import ListFooterButton from 'app/shared/layout/list-footer-button/list-footer-button';
import MenuDropdown from 'app/shared/layout/menu-dropdown/menu-dropdown';
import { IRootState } from 'app/shared/reducers';
import { hasAnyAuthority } from 'app/shared/auth/private-route';
import { AUTHORITIES } from 'app/config/constants';
import { NavigationPath } from 'app/shared/util/navigation-utils';
import { Spinner } from 'app/shared/layout/spinner/spinner';
import { JOURNEY } from 'app/shared/util/entity-utils';
import MyJourneysBreadcrumbItem from 'app/shared/layout/heading/breadcrumbs/my-journeys-breadcrumb-item';
import { useGetUserJourneysPaginated } from 'app/shared/services/journey-api';
import flatMap from 'lodash/flatMap';
import JourneyTemplate from '../shared/layout/journey-template/journey-template';
import JourneyOptionsModal from 'app/shared/layout/journey-options/journey-options-modal/journey-options-modal';
import { useShowJourneyTemplatesViewState } from 'app/shared/contexts/journey-template-context';
import HeadingNew, { SUB_HEADING } from 'app/shared/layout/heading/heading-new';
import { HomeBreadcrumbItem } from 'app/shared/layout/heading/breadcrumbs/home-breadcrumb-item';
import './journey.scss';
import AddButtonNew from 'app/shared/layout/add-button-new/add-button-new';

export interface IJourneyProps extends RouteComponentProps<{ url: string }>, StateProps {}

export const Journey = ({ isTeacherOrAdmin, userId, locale }: IJourneyProps) => {
  const [displayJourneyOptions, setDisplayJourneyOptions] = useState<boolean>(false);
  const history = useHistory();
  const { showTemplatesView } = useShowJourneyTemplatesViewState();

  const {
    data: ongoing,
    isFetching: isOngoingFetching,
    hasNextPage: hasOngoingNextPage,
    fetchNextPage: fetchOngoingNextPage,
  } = useGetUserJourneysPaginated(JourneyState.IN_PROGRESS, userId);
  const {
    data: upcoming,
    isFetching: isUpcomingFetching,
    hasNextPage: hasUpcomingNextPage,
    fetchNextPage: fetchUpcomingNextPage,
  } = useGetUserJourneysPaginated(JourneyState.NOT_STARTED, userId);
  const {
    data: completed,
    isFetching: isCompletedFetching,
    hasNextPage: hasCompletedNextPage,
    fetchNextPage: fetchCompletedNextPage,
  } = useGetUserJourneysPaginated(JourneyState.COMPLETED, userId);

  const JourneyCards = ({ journeys, color, greyedOut }) =>
    journeys?.map(({ subject, endDate, id, teacherName, title, teachers, studentCount, owner }: IJourney) => {
      const isAllowedToModify = isTeacherOrAdmin && teachers?.some(teacher => teacher.user.id === userId);
      const entityCardProps: IEntityCardProps = {
        entityId: id,
        color,
        deadline: endDate,
        title,
        href: `/journey/${id}`,
        studentCount,
        dropdown: isAllowedToModify ? (
          <MenuDropdown showDeleteButton={owner} entityId={id} entityType={JOURNEY} entityTitle={title} />
        ) : null,
        greyedOut,
        teacherName,
        subject,
      };

      return (
        <Col key={id} lg={'4'} className={'mb-4'}>
          <EntityCard key={id} {...entityCardProps} />
        </Col>
      );
    });

  const ButtonCard = ({ addButton, content }) => {
    if (addButton) {
      if (isTeacherOrAdmin) {
        return (
          <Col lg={'4'} className={'mb-4'}>
            <ListFooterButton onClick={() => setDisplayJourneyOptions(true)} className={'h-100'} label={'entity.add.journey'} />
          </Col>
        );
      } else if (!content?.length) {
        return (
          <Col lg={'4'} className={'mb-4'}>
            <ListFooterButton link={NavigationPath.JOIN_JOURNEY} label={'schoolabyApp.journey.home.joinButton'} className={'h-100'} />
          </Col>
        );
      }
    }
    return null;
  };

  const ContentRow = ({
    content,
    isFetching,
    color,
    title,
    hasNextPage = false,
    fetchNextPage = () => null,
    greyedOut = false,
    addButton = false,
  }) => {
    return (
      <Col xs={'12'}>
        <h3 className={'my-4'}>{translate(title)}</h3>
        {isFetching ? (
          <Spinner className="auto my-2 position-relative" />
        ) : (
          <>
            <Row>
              <JourneyCards journeys={flatMap(content?.pages, 'data')} color={color} greyedOut={greyedOut} />
              <ButtonCard addButton={addButton} content={content?.data} />
            </Row>
            {hasNextPage && (
              <Button outline color={'info'} onClick={() => fetchNextPage()}>
                {translate('schoolabyApp.journey.home.loadMore')}
              </Button>
            )}
          </>
        )}
      </Col>
    );
  };

  return useMemo(
    () =>
      showTemplatesView ? (
        <JourneyTemplate />
      ) : (
        <>
          <div className="journey">
            <HomeBreadcrumbItem />
            <MyJourneysBreadcrumbItem />
            <HeadingNew isAllowedToModify={isTeacherOrAdmin} headingType={SUB_HEADING} title={translate('schoolabyApp.journey.home.title')}>
              <div className="ml-auto my-auto mr-3">
                {isTeacherOrAdmin ? (
                  <AddButtonNew onClick={() => setDisplayJourneyOptions(true)} title={translate('entity.add.journey')} />
                ) : (
                  <AddButtonNew
                    onClick={() => history.push(NavigationPath.JOIN_JOURNEY)}
                    title={translate('schoolabyApp.journey.home.joinButton')}
                  />
                )}
              </div>
            </HeadingNew>
            <ContentRow
              addButton
              content={ongoing}
              isFetching={isOngoingFetching}
              hasNextPage={hasOngoingNextPage}
              fetchNextPage={fetchOngoingNextPage}
              color={'warning'}
              title={'schoolabyApp.journey.ongoing'}
            />
            {!!isTeacherOrAdmin && (
              <ContentRow
                addButton
                content={upcoming}
                isFetching={isUpcomingFetching}
                hasNextPage={hasUpcomingNextPage}
                fetchNextPage={fetchUpcomingNextPage}
                color={'info'}
                title={'schoolabyApp.journey.upcoming'}
              />
            )}
            <ContentRow
              greyedOut
              content={completed}
              isFetching={isCompletedFetching}
              hasNextPage={hasCompletedNextPage}
              fetchNextPage={fetchCompletedNextPage}
              color={'secondary'}
              title={'schoolabyApp.journey.completed'}
            />
          </div>
          <JourneyOptionsModal isModalOpen={displayJourneyOptions} closeModal={() => setDisplayJourneyOptions(false)} />
        </>
      ),
    [showTemplatesView, ongoing, upcoming, completed, displayJourneyOptions, locale]
  );
};

const mapStateToProps = ({ authentication, locale }: IRootState) => ({
  isTeacherOrAdmin: hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.TEACHER, AUTHORITIES.ADMIN]),
  userId: authentication.account.id,
  locale: locale.currentLocale,
});

type StateProps = ReturnType<typeof mapStateToProps>;
export default connect(mapStateToProps)(Journey);
