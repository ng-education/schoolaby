import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps, useLocation } from 'react-router-dom';
import { Alert, Button, Col } from 'reactstrap';
import { Field, FieldRenderProps, Form } from 'react-final-form';
import { translate as t } from 'react-jhipster';
import { IRootState } from 'app/shared/reducers';
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { useSelectedSubjectState } from 'app/shared/contexts/entity-update-context';
import { createYupValidator } from 'app/shared/util/form-utils';
import { DatePicker, Input, Radio } from 'app/shared/form';
import { useUpdateJourneyEntitySchema } from 'app/shared/schema/update-entity';
import { ROLE } from 'app/config/constants';
import { IJourney } from 'app/shared/model/journey.model';
import { useGetEducationalLevels } from 'app/shared/services/educational-level-api';
import { useCreateJourney, useGetJourney, useGetJourneyStudents, useUpdateJourney } from 'app/shared/services/journey-api';
import { DateTime } from 'luxon-business-days';
import { ISchool } from 'app/shared/model/school.model';
import { useGetMilestones } from 'app/shared/services/milestone-api';
import './journey-update.scss';
import SubjectSearchDropdownNew from 'app/shared/layout/subject-search-dropdown-new/subject-search-dropdown-new';
import Icon from 'app/shared/icons';
import PictureUploadButtonNew from 'app/shared/form/picture-upload-button-new';
import ConfirmationModalNew from 'app/shared/layout/confirmation-modal-new/confirmation-modal-new';
import { useGetCurricula } from 'app/shared/services/curriculum-api';
import { ILtiConfig } from 'app/shared/model/lti-config.model';
import { IMilestone } from 'app/shared/model/milestone.model';
import { IAssignment } from 'app/shared/model/assignment.model';
import { clearRubric } from 'app/shared/util/rubric-utils';
import { isLtiAdvantage } from 'app/shared/util/lti-utils';
import { Duration } from 'luxon';
import { v4 as uuidv4 } from 'uuid';

export interface IJourneyUpdateProps extends StateProps, RouteComponentProps<{ id: string }> {}

interface InitialValues extends IJourney {
  schoolIds?: number[];
  educationalLevelId?: number;
}

const JourneyUpdate: React.FC<IJourneyUpdateProps> = ({ history, locale, match, country, userPersonRoles }) => {
  const [isNew] = useState(!match?.params?.id);
  const [templateJourney, setTemplateJourney] = useState<any>();
  const [initialValues, setInitialValues] = useState<InitialValues>();
  const [schoolOptions, setSchoolOptions] = useState<ISchool[]>();
  const [showingWarningModal, setShowingWarningModal] = useState<boolean>(false);
  const { data: journey, isFetching } = useGetJourney(match.params.id, !!match?.params?.id);
  const { data: educationalLevels } = useGetEducationalLevels(country);
  const { mutate: createJourney } = useCreateJourney();
  const { mutate: updateJourney } = useUpdateJourney();
  const { selectedSubject, setSelectedSubject } = useSelectedSubjectState();
  const schema = useUpdateJourneyEntitySchema(locale);
  const { data: students } = useGetJourneyStudents(journey?.id, !!journey?.id);
  const { data: curricula } = useGetCurricula(country);
  const search = useLocation().search;
  const [templateJourneyId, setTemplateJourneyId] = useState<number>(undefined);

  const getTemplateIdFromUrl = () => {
    const urlSearchParams = new URLSearchParams(search);
    return urlSearchParams.get('template');
  };
  useGetJourney(templateJourneyId, !!templateJourneyId, true, setTemplateJourney);
  const { data: templateMilestones } = useGetMilestones(templateJourney?.id, !!templateJourney?.id, !!getTemplateIdFromUrl());

  useEffect(() => {
    if (search) {
      const templateId = getTemplateIdFromUrl();
      templateId?.length && setTemplateJourneyId(+templateId);
    }
  }, [search]);

  useEffect(() => {
    if (!isFetching) {
      const schools = userPersonRoles
        ?.filter(personRole => personRole.role === ROLE.TEACHER && !!personRole.school)
        .map(teacherRole => teacherRole.school);
      const schoolIds = schools?.map(school => school.id);
      if (journey?.schools?.length) {
        const filteredJourneySchools = journey.schools.filter(school => !schoolIds.includes(school.id));
        setSchoolOptions([...schools, ...filteredJourneySchools]);
      } else {
        setSchoolOptions(schools);
      }
    }
  }, [journey, userPersonRoles, isFetching]);

  useEffect(() => {
    if (templateMilestones?.length) {
      templateJourney.milestones = templateMilestones;
      setTemplateJourney(templateJourney);
    }
  }, [templateMilestones]);

  useEffect(() => {
    if (!journey?.educationalLevel?.id && educationalLevels.length) {
      setInitialValues({ ...initialValues, educationalLevelId: educationalLevels[0].id });
    }
    if (!journey?.schools?.length && schoolOptions?.length) {
      setInitialValues({ ...initialValues, schoolIds: [schoolOptions[0].id] });
    }
  }, [schoolOptions, educationalLevels]);

  const calculateDifferenceBetweenDatesByDays = (date1: string, date2: string) => {
    return DateTime.fromISO(date1, { zone: 'utc' })
      .diff(DateTime.fromISO(date2, { zone: 'utc' }), ['days'])
      .toObject();
  };

  const calculateDateRelativeToJourneyStartDate = (date: string, calculatedDifferenceInDays: DateTime) => {
    let calculatedDate = DateTime.fromISO(date).plus(Duration.fromObject(calculatedDifferenceInDays));
    if (!calculatedDate.isBusinessDay()) {
      calculatedDate = calculatedDate.plusBusiness();
    }
    return calculatedDate.toUTC().toISO();
  };

  const calculateDifferenceBetweenJourneyStartDateAndTemplateStartDateByDays = (newJourneyStartDate: string) => {
    const calculatedDifferenceInDays = calculateDifferenceBetweenDatesByDays(newJourneyStartDate, templateJourney?.startDate);
    const isLeapYear = DateTime.fromISO(templateJourney?.startDate) % 4 === 0;
    if (isLeapYear) {
      calculatedDifferenceInDays.days = calculatedDifferenceInDays.days - 1;
    }
    calculatedDifferenceInDays.days = Math.ceil(calculatedDifferenceInDays.days);
    return calculatedDifferenceInDays;
  };

  const initializeFormValues = (values: IJourney) => {
    const { title, educationalLevel, startDate, endDate, schools, subject } = values;
    setSelectedSubject(subject);
    const journeySchoolIds = schools?.map(school => school?.id);

    return {
      ...values,
      title: title || '',
      startDate: convertDateTimeFromServer(startDate),
      endDate: convertDateTimeFromServer(endDate),
      educationalLevelId: educationalLevel.id,
      schoolIds: journeySchoolIds?.length && !templateJourney ? journeySchoolIds : [schoolOptions?.[0]?.id],
    };
  };

  useEffect(() => {
    let journeyInitialValues = {};
    if (!isNew && journey?.id) {
      journeyInitialValues = initializeFormValues(journey);
    } else if (isNew && templateJourney) {
      const newJourneyStartDate = DateTime.local().toString();
      const calculatedDifferenceInDays = calculateDifferenceBetweenJourneyStartDateAndTemplateStartDateByDays(newJourneyStartDate);

      journeyInitialValues = {
        ...initializeFormValues(templateJourney),
        id: undefined,
        startDate: newJourneyStartDate,
        endDate: calculateDateRelativeToJourneyStartDate(templateJourney.endDate, calculatedDifferenceInDays),
        videoConferenceUrl: '',
      };
    }
    setInitialValues(journeyInitialValues);
  }, [journey, schoolOptions, templateJourney]);

  const addLevelIfNotExists = educationalLevel => {
    if (!!educationalLevel && !educationalLevels.filter(level => educationalLevel.id === level.id).length) {
      educationalLevels.push(educationalLevel);
    }
  };

  useEffect(() => {
    journey && addLevelIfNotExists(journey.educationalLevel);
  }, [journey, educationalLevels]);

  const getJourneyTitleFromUrl = () => {
    const urlSearchParams = new URLSearchParams(search);
    return urlSearchParams.get('journeyTitle');
  };

  useEffect(() => {
    if (!isNew) {
      return;
    }
    const currentInitialValues: InitialValues = {
      startDate: DateTime.local().toString(),
      endDate: DateTime.local().toString(),
      title: getJourneyTitleFromUrl(),
      curriculum: curricula?.[0],
    };

    if (educationalLevels?.length) {
      currentInitialValues.educationalLevelId = +educationalLevels[0].id;
    }
    if (schoolOptions?.length) {
      currentInitialValues.schoolIds = [schoolOptions[0].id];
    }
    if (curricula?.length) {
      currentInitialValues.curriculum = curricula?.[0];
    }
    setInitialValues(currentInitialValues);
  }, [educationalLevels, schoolOptions, curricula]);

  const redirectToKanbanView = (journeyId: number) => {
    history.push(`/journey/${journeyId}`);
  };

  const clearLtiResources = entity => {
    entity?.ltiResources?.forEach(ltiResource => {
      ltiResource.id = undefined;
      ltiResource.entityId = undefined;

      if (isLtiAdvantage(ltiResource.ltiApp.version)) {
        ltiResource.resourceLinkId = uuidv4();
      }
      ltiResource.lineItems = [];
    });
  };

  const addTemplateValues = values => {
    const calculatedDifferenceInDays = calculateDifferenceBetweenJourneyStartDateAndTemplateStartDateByDays(values.startDate);
    values.states = undefined;
    values.teachers = undefined;
    values.template = false;
    values.ltiConfigs = templateJourney?.ltiConfigs;
    values.milestones = templateJourney?.milestones;
    values?.ltiConfigs?.forEach((ltiConfig: ILtiConfig) => {
      ltiConfig.id = undefined;
      ltiConfig.journeyId = undefined;
      ltiConfig.consumerKey = undefined;
      ltiConfig.sharedSecret = undefined;
    });
    values?.milestones?.forEach((milestone: IMilestone) => {
      milestone.id = undefined;
      milestone.messages = undefined;
      milestone.journeyId = undefined;
      milestone.endDate = calculateDateRelativeToJourneyStartDate(milestone.endDate, calculatedDifferenceInDays);
      milestone?.materials?.forEach(m => (m.id = undefined));
      clearLtiResources(milestone);

      milestone.assignments?.forEach((assignment: IAssignment) => {
        assignment.id = undefined;
        assignment.students = undefined;
        assignment.milestoneId = undefined;
        clearRubric(assignment.rubric);
        assignment.deadline = calculateDateRelativeToJourneyStartDate(assignment.deadline, calculatedDifferenceInDays);
        assignment?.materials?.forEach(m => (m.id = undefined));
        assignment.groups = [];
        clearLtiResources(assignment);
      });
    });
  };

  const saveEntity = values => {
    values.startDate = convertDateTimeToServer(values.startDate);
    values.endDate = convertDateTimeToServer(values.endDate);

    isNew && addTemplateValues(values);

    const entity: IJourney = {
      ...journey,
      ...values,
      students,
      educationalLevel: educationalLevels.find(level => level.id === +values.educationalLevelId),
      subject: selectedSubject || null,
      schools: schoolOptions.filter(school => values.schoolIds.includes(school.id)),
    };

    if (isNew) {
      createJourney(entity, {
        onSuccess({ data }) {
          redirectToKanbanView(data.id);
        },
      });
    } else {
      updateJourney(entity, {
        onSuccess({ data }) {
          redirectToKanbanView(data.id);
        },
      });
    }
  };

  const getCurriculumTranslationKey = (curriculum: string) => `schoolabyApp.journey.curriculum.${curriculum}`;

  const customDropdownStyles = {
    menuList: provided => ({
      ...provided,
      maxHeight: '10rem',
    }),
  };

  const onCurriculumChange = (input, form) => {
    const selectedCurriculum = curricula.find(curriculum => curriculum.id === Number(input.value));
    form.change(input.name, selectedCurriculum);
  };

  return (
    <div className="journey-update full-height">
      <Form onSubmit={saveEntity} validate={createYupValidator(schema)} initialValues={initialValues}>
        {({ handleSubmit, form, values, errors }) => (
          <form
            onSubmit={handleSubmit}
            onKeyPress={e => {
              if (e.key === 'Enter') e.preventDefault();
            }}
          >
            <div>
              <Field
                id="cover-image"
                name="imageUrl"
                render={({ input: { value, onChange } }: FieldRenderProps<string>) => (
                  <PictureUploadButtonNew
                    label={t('schoolabyApp.journey.changeCoverImage')}
                    existingImage={value?.length ? value : '/content/images/default_cover.svg'}
                    onUpload={onChange}
                    uploadDisabled={false}
                  />
                )}
              />
            </div>
            <div className="form-wrapper d-flex justify-content-center py-4 bg-white">
              <div className="journey-update-form-content">
                <h3 className="mb-4">{isNew ? t('schoolabyApp.journey.addJourney') : t('schoolabyApp.journey.editJourney')}</h3>
                <Field
                  id="journey-title"
                  type="text"
                  name="title"
                  placeholder={t('schoolabyApp.journey.journeyTitlePlaceholder')}
                  component={Input}
                  label={t('schoolabyApp.journey.journeyTitle')}
                />
                <div className="form-row">
                  <Col sm={12} md={6}>
                    <Field
                      id="journey-start-date"
                      type="text"
                      name="startDate"
                      initialValue={!isNew && convertDateTimeFromServer(journey?.startDate)}
                      render={dateProps => (
                        <>
                          <label>{t('schoolabyApp.journey.journeyBeginning')}</label>
                          <div className={'position-relative'}>
                            <DatePicker meta={dateProps.meta} input={dateProps.input} />
                            <Icon name={'calendarNew'} width={'20'} height={'21'} className={'position-absolute calendar-icon'} />
                          </div>
                        </>
                      )}
                    />
                  </Col>
                  <Col sm={12} md={6}>
                    <Field
                      id="journey-end-date"
                      type="text"
                      name="endDate"
                      initialValue={!isNew && convertDateTimeFromServer(journey?.endDate)}
                      render={dateProps => (
                        <>
                          <label>{t('schoolabyApp.journey.journeyEnding')}</label>
                          <div className={'position-relative'}>
                            <DatePicker meta={dateProps.meta} input={dateProps.input} />
                            <Icon name={'calendarNew'} width={'20'} height={'21'} className={'position-absolute calendar-icon'} />
                          </div>
                        </>
                      )}
                    />
                  </Col>
                </div>
                <Field
                  id="journey-video-conference-url"
                  type="text"
                  name="videoConferenceUrl"
                  placeholder={t('schoolabyApp.journey.videoConferenceLinkPlaceholder')}
                  component={Input}
                  label={t('schoolabyApp.journey.videoConferenceLink')}
                />
                <Field
                  id="journey-description"
                  type="textarea"
                  name="description"
                  placeholder={t('schoolabyApp.journey.descriptionPlaceholder')}
                  component={Input}
                  label={t('schoolabyApp.journey.description')}
                />
                <div className="mb-2">
                  <h5 className="mb-0">{t('schoolabyApp.journey.selectEducationalInstitution')}</h5>
                  {!!errors.schoolIds && <p className="invalid-feedback">{errors.schoolIds}</p>}
                </div>
                {schoolOptions?.length ? (
                  schoolOptions.map((schoolOption, index) => (
                    <Field key={name + index} name={'schoolIds'} className="d-inline-block" type="checkbox" value={schoolOption.id}>
                      {({ input }) => {
                        return (
                          <div className="mb-2">
                            <label>
                              <input
                                name={input.name}
                                type="checkbox"
                                value={input.value}
                                checked={input.checked}
                                onChange={input.onChange}
                                disabled={schoolOptions.length === 1}
                              />
                              <span className="pl-2">{schoolOption.name}</span>
                            </label>
                          </div>
                        );
                      }}
                    </Field>
                  ))
                ) : !isFetching ? (
                  <Alert color="danger">{t('schoolabyApp.journey.noTeacherRoleYetMessage')}</Alert>
                ) : null}
                <h5>{t('schoolabyApp.journey.educationalLevel')}</h5>
                {educationalLevels?.map((educationalLevel, index) => (
                  <Radio
                    key={`${name}-${index}`}
                    name="educationalLevelId"
                    value={educationalLevel.id}
                    label={educationalLevel.name}
                    onChange={input => form.change(input.name, Number(input.value))}
                  />
                ))}
                <Field
                  id="journey-subject"
                  type="text"
                  name="subject"
                  component={SubjectSearchDropdownNew}
                  customStyles={customDropdownStyles}
                  selectedSubject={selectedSubject}
                  setSelectedSubject={setSelectedSubject}
                  containerClassName="mt-3"
                  label={t('schoolabyApp.journey.subject')}
                  disabled={!values.curriculum?.requiresSubject}
                />
                <div className="mt-3">
                  {curricula?.map(curriculum => (
                    <Radio
                      key={`${name}-${curriculum.id}`}
                      name="curriculum"
                      value={curriculum.id}
                      label={t(getCurriculumTranslationKey(curriculum.title))}
                      onChange={input => onCurriculumChange(input, form)}
                      checked={values.curriculum?.id === curriculum.id}
                    />
                  ))}
                </div>
              </div>
            </div>
            <div className="journey-update-form-footer d-flex justify-content-between pt-2 pb-4">
              <Button
                type="button"
                className="cancel-btn bg-white px-4 py-2"
                onClick={() => {
                  setShowingWarningModal(true);
                }}
              >
                {t('entity.action.cancel')}
              </Button>
              <Button type="submit" className="btn-primary border-0 px-4 py-2">
                {t('entity.action.save')}
              </Button>
            </div>
          </form>
        )}
      </Form>
      {showingWarningModal ? (
        <ConfirmationModalNew
          isModalOpen={showingWarningModal}
          closeModal={() => setShowingWarningModal(false)}
          handleConfirmation={() => history.push('/journey/')}
          modalTitle={t('schoolabyApp.journey.warningModal.title')}
        >
          <p>{t('schoolabyApp.journey.warningModal.description')}</p>
        </ConfirmationModalNew>
      ) : null}
    </div>
  );
};

const mapStateToProps = ({ authentication: { account }, locale }: IRootState) => ({
  locale: locale.currentLocale,
  country: account.country,
  userPersonRoles: account.personRoles || [],
});

type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(JourneyUpdate);
