import React, { useEffect, useMemo, useState } from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';
import { IRootState } from 'app/shared/reducers';
import { hasAnyAuthority } from 'app/shared/auth/private-route';
import { AUTHORITIES } from 'app/config/constants';
import { Spinner } from 'app/shared/layout/spinner/spinner';
import { IMilestone } from 'app/shared/model/milestone.model';
import { IJourney } from 'app/shared/model/journey.model';
import { IAssignment } from 'app/shared/model/assignment.model';
import { useGetJourney } from 'app/shared/services/journey-api';
import MilestoneDetail from 'app/milestone/milestone-detail';
import AssignmentPreview from 'app/shared/layout/journey-template/assignment-preview/assignment-preview';
import JourneyDetailView from './journey-detail-view/journey-detail-view';
import JourneyAppsView from 'app/journey/journey-detail/apps/journey-apps-view';
import { PreviewTabType } from 'app/shared/layout/heading/heading';
import { EntityUpdateProvider } from 'app/shared/contexts/entity-update-context';

export interface IJourneyDetailProps extends StateProps, RouteComponentProps<{ id: string }> {
  previewEnabled?: boolean;
  previewJourneyId?: number;
}

export const JourneyDetailContext = React.createContext(null);

export const MilestoneContext = React.createContext<{
  milestone: IMilestone;
  isAllowedToModify: boolean;
  journey: IJourney;
  previewEnabled?: boolean;
  toggleMilestoneView?: (milestoneId?: number) => void;
  toggleAssignmentView?: (assignmentId?: number) => void;
}>(null);

export const AssignmentContext = React.createContext<{
  assignment: IAssignment;
  isAllowedToModify: boolean;
  journey: IJourney;
  previewEnabled?: boolean;
  toggleAssignmentView?: (assignmentId?: number) => void;
}>(null);

export const JourneyDetailOld = ({
  previewEnabled,
  previewJourneyId,
  isTeacherOrAdmin,
  currentLocale,
  match,
  account,
}: IJourneyDetailProps) => {
  const { data: journey, isFetching } = useGetJourney(
    match?.params?.id || previewJourneyId,
    !!match?.params?.id || !!previewJourneyId,
    previewEnabled
  );

  const [selectedMilestoneId, setSelectedMilestoneId] = useState<number>(undefined);
  const [selectedAssignmentId, setSelectedAssignmentId] = useState<number>(undefined);
  const [openedPreviewTab, setOpenedPreviewTab] = useState<PreviewTabType>('JOURNEY');

  const isAllowedToModify = !previewEnabled && isTeacherOrAdmin && journey?.teachers?.some(teacher => teacher.user.id === account.id);

  useEffect(() => {
    setSelectedAssignmentId(undefined);
    setSelectedMilestoneId(undefined);
  }, [previewJourneyId]);

  const [activeTab, setActiveTab] = useState('1');
  const toggleTab = tab => activeTab !== tab && setActiveTab(tab);

  const toggleMilestoneView = (milestoneId?: number) => {
    setSelectedMilestoneId(milestoneId);
  };

  const toggleAssignmentView = (assignmentId?: number) => {
    assignmentId ? setOpenedPreviewTab('ASSIGNMENT') : setOpenedPreviewTab('JOURNEY');
    setSelectedAssignmentId(assignmentId);
  };

  const JourneyDetailContent = () => {
    if (previewEnabled) {
      const routeComponentProps = {
        match: null,
        history: null,
        location: null,
      };

      if (selectedMilestoneId) {
        return (
          <MilestoneDetail
            milestoneId={selectedMilestoneId}
            previewEnabled
            closeMilestone={() => toggleMilestoneView(undefined)}
            {...routeComponentProps}
          />
        );
      } else if (selectedAssignmentId) {
        return (
          <EntityUpdateProvider>
            <AssignmentPreview
              assignmentId={selectedAssignmentId}
              openedPreviewTab={openedPreviewTab}
              setOpenedPreviewTab={setOpenedPreviewTab}
              onBackClicked={() => toggleAssignmentView(undefined)}
            />
          </EntityUpdateProvider>
        );
      } else if (openedPreviewTab === 'JOURNEY_APPS') {
        return (
          <JourneyAppsView
            previewEnabled={previewEnabled}
            openedPreviewTab={openedPreviewTab}
            setOpenedPreviewTab={setOpenedPreviewTab}
            templateJourney={journey}
            {...routeComponentProps}
          />
        );
      }
    }

    return <JourneyDetailView />;
  };

  return useMemo(
    () =>
      isFetching ? (
        <Spinner />
      ) : (
        <JourneyDetailContext.Provider
          value={{
            toggleTab,
            activeTab,
            journey,
            previewEnabled,
            isAllowedToModify,
            currentLocale,
            toggleMilestoneView,
            toggleAssignmentView,
            openedPreviewTab,
            setOpenedPreviewTab,
          }}
        >
          <JourneyDetailContent />
        </JourneyDetailContext.Provider>
      ),
    [isFetching, selectedMilestoneId, openedPreviewTab]
  );
};

const mapStateToProps = ({ authentication, locale }: IRootState) => ({
  currentLocale: locale.currentLocale,
  account: authentication.account,
  isTeacherOrAdmin: hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.TEACHER, AUTHORITIES.ADMIN]),
});

type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(JourneyDetailOld);
