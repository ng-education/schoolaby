import React, { useContext, useMemo, useState } from 'react';
import MyJourneysBreadcrumbItem from 'app/shared/layout/heading/breadcrumbs/my-journeys-breadcrumb-item';
import JourneyTitleBreadcrumbItem from 'app/shared/layout/heading/breadcrumbs/journey-title-breadcrumb-item';
import Heading, { ITabParams } from 'app/shared/layout/heading/heading';
import { getHeadingIcon } from 'app/shared/util/journey-utils';
import MenuDropdown from 'app/shared/layout/menu-dropdown/menu-dropdown';
import { JOURNEY } from 'app/shared/util/entity-utils';
import { ICON_GREY } from 'app/shared/util/color-utils';
import Icon from 'app/shared/icons';
import { withHttp } from 'app/shared/util/file-utils';
import JourneyOverview from 'app/journey/journey-detail/journey-overview/journey-overview';
import { useHistory } from 'react-router-dom';
import { useQueryParam } from 'app/shared/hooks/useQueryParam';
import { JourneyDetailContext } from 'app/journey/journey-detail/journey-detail-old';
import { translate } from 'react-jhipster';
import { Button } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { JourneyBoardOld } from 'app/journey/journey-detail/journey-board/journey-board-old';
import InviteDropdown from 'app/journey/journey-detail/invite-dropdown';

const JourneyDetailView = () => {
  const { journey, isAllowedToModify, previewEnabled, openedPreviewTab, setOpenedPreviewTab } = useContext(JourneyDetailContext);
  const [{ welcome }] = useQueryParam<{ welcome: string }>();
  const [welcomeModal, setWelcomeModal] = useState(!!welcome && !previewEnabled);
  const history = useHistory();

  const toggleWelcomeModal = () => {
    history.replace({
      search: '',
    });
    return setWelcomeModal(!welcomeModal);
  };

  const tabParams: ITabParams = {
    entityType: JOURNEY,
    tabEntityId: journey?.id,
  };

  return useMemo(
    () => (
      <>
        {!previewEnabled && (
          <>
            <MyJourneysBreadcrumbItem />
            <JourneyTitleBreadcrumbItem journeyId={journey.id} journeyTitle={journey.title} />
          </>
        )}
        <Heading
          previewEnabled={previewEnabled}
          tabParams={tabParams}
          icon={getHeadingIcon(journey)}
          title={journey.title}
          journey={journey}
          openedPreviewTab={openedPreviewTab}
          setOpenedPreviewTab={setOpenedPreviewTab}
          dropdown={
            isAllowedToModify && (
              <MenuDropdown entityId={journey.id} entityType={JOURNEY} showDeleteButton={journey?.owner} entityTitle={journey?.title} />
            )
          }
        >
          {!previewEnabled && (
            <div className={'my-auto mr-3 hand'} onClick={toggleWelcomeModal}>
              <FontAwesomeIcon icon={['fas', 'info-circle']} size="lg" color={ICON_GREY} />
            </div>
          )}
          <div className="d-none d-md-flex align-items-md-center my-auto">
            {isAllowedToModify && <InviteDropdown oldDesign journey={journey} />}
            {journey.videoConferenceUrl && !previewEnabled && (
              <a href={withHttp(journey.videoConferenceUrl)} target="_blank" rel="noopener noreferrer">
                <Button className="btn-sm btn-primary d-flex align-items-center text-nowrap ml-3">
                  <Icon name={'video'} className={'mr-2'} stroke="white" width="19px" height="17px" />
                  {translate('schoolabyApp.journey.detail.viewLive')}
                </Button>
              </a>
            )}
          </div>
        </Heading>
        <JourneyBoardOld />
        <JourneyOverview toggle={toggleWelcomeModal} modal={welcomeModal} journey={journey} />
      </>
    ),
    [welcomeModal]
  );
};

export default JourneyDetailView;
