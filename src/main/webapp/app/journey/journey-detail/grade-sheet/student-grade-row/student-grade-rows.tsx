import './student-grade-rows.scss';
import React, { FC, useState } from 'react';
import { IUser } from 'app/shared/model/user.model';
import { IAssignment } from 'app/shared/model/assignment.model';
import { ISubmission } from 'app/shared/model/submission/submission.model';
import { GradingScheme } from 'app/shared/model/grading-scheme.model';
import { Tooltip } from 'reactstrap';
import { translate } from 'react-jhipster';
import { Link } from 'react-router-dom';
import times from 'lodash/times';
import { convertAppDateTimeFromServer } from 'app/shared/util/date-utils';
import Icon from 'app/shared/icons';
import { getClassNameForGrade, getGrade } from 'app/shared/util/grade-util';
import { YELLOW } from 'app/shared/util/color-utils';
import { ISubmissionFeedback } from 'app/shared/model/submission/submission-feedback.model';

interface IProps {
  studentSubmissions: ISubmission[];
  maxRowCount: number;
  student: IUser;
  assignments: IAssignment[];
  gradingSchemes: GradingScheme[];
  journeyId: number;
}

const StudentGradeRows = ({ studentSubmissions, student, assignments, gradingSchemes, journeyId, maxRowCount }: IProps) => {
  const findGradingScheme = (submission: ISubmission): GradingScheme => {
    const gradingSchemeId = assignments?.find(a => a?.id === submission?.assignmentId)?.gradingSchemeId;
    return gradingSchemes.find(g => g.id === gradingSchemeId);
  };

  const renderTrophyIcon = (key: number) => (
    <div key={key} className="mr-1 trophy-wrapper">
      <Icon name={'trophy'} width={'18px'} height={'16px'} fill={YELLOW} stroke={YELLOW} />
    </div>
  );

  const getTrophyIcons = () => {
    const highestGradeSubmissions = studentSubmissions?.filter(s => s.highestGrade);
    let element;
    if (highestGradeSubmissions.length > 3) {
      const multiplier = highestGradeSubmissions.length - 3;
      element = (
        <td>
          {times(3, i => {
            return renderTrophyIcon(i);
          })}
          {`${multiplier}x`}
        </td>
      );
    } else {
      element = <td>{studentSubmissions?.filter(s => s.highestGrade).map((s, i) => renderTrophyIcon(i))}</td>;
    }
    return element;
  };

  const getAdditionalRows = () => {
    const toAdd = maxRowCount - studentSubmissions.length;
    return times(toAdd, i => {
      return <td key={i} />;
    });
  };

  const getSubmissionFeedbackForIndividualStudentOrGroup = (submission: ISubmission) => {
    const submissionFeedbacksWithGrade = submission.submissionFeedbacks?.filter(feedback => feedback.grade?.length);
    const individualFeedback = submissionFeedbacksWithGrade?.find(feedback => feedback.studentId === student.id);
    const groupFeedback = submissionFeedbacksWithGrade?.find(feedback => !!feedback.groupId);
    return individualFeedback || groupFeedback;
  };

  const findGradingSchemeValueByGrade = (submission: ISubmission) => {
    return findGradingScheme(submission)?.values?.find(
      value => value.grade === getSubmissionFeedbackForIndividualStudentOrGroup(submission)?.grade
    );
  };

  return (
    <>
      {studentSubmissions && getTrophyIcons()}
      {studentSubmissions.map((submission, i) => (
        <GradeRow
          key={i}
          studentId={student.id}
          submission={submission}
          feedback={getSubmissionFeedbackForIndividualStudentOrGroup(submission)}
          className={getClassNameForGrade(findGradingSchemeValueByGrade(submission))}
          assignment={assignments.find(a => a?.id === submission?.assignmentId)}
          journeyId={journeyId}
        />
      ))}
      {getAdditionalRows()}
    </>
  );
};

interface GradeRowProps {
  studentId: number;
  submission: ISubmission;
  feedback: ISubmissionFeedback;
  className: string;
  assignment: IAssignment;
  journeyId: number;
}

const GradeRow: FC<GradeRowProps> = ({ studentId, submission, feedback, className, assignment, journeyId }) => {
  const [tooltipOpen, setTooltipOpen] = useState(false);
  const toggle = () => setTooltipOpen(!tooltipOpen);

  return (
    <td>
      {assignment && (
        <Tooltip isOpen={tooltipOpen} placement="top" target={'gradeTooltip' + studentId + '-' + submission.id}>
          <Link to={`/assignment/${assignment.id}?journeyId=${journeyId}`}>{assignment.title}</Link>
          <div>
            {translate('schoolabyApp.gradeSheet.graded')}
            {convertAppDateTimeFromServer(feedback?.feedbackDate)}
          </div>
        </Tooltip>
      )}

      <div id={'gradeTooltip' + studentId + '-' + submission.id} className={`grade-row ${className || ''}`} onClick={toggle}>
        {getGrade(feedback)}
      </div>
    </td>
  );
};

export default React.memo(StudentGradeRows);
