import './grade-sheet.scss';
import React, { FC, useEffect, useState } from 'react';
import { Col, Row, Table } from 'reactstrap';
import { getStudentsAverageGrades, useGetJourneyStudents } from 'app/shared/services/journey-api';
import Avatar from 'app/shared/layout/header/avatar';
import { getFullName } from 'app/shared/util/string-utils';
import { findClassNameByGradePercentage } from 'app/shared/util/color-utils';
import StudentGradeRows from 'app/journey/journey-detail/grade-sheet/student-grade-row/student-grade-rows';
import { useGetMilestones } from 'app/shared/services/milestone-api';
import { ISubmission } from 'app/shared/model/submission/submission.model';
import { GradingScheme } from 'app/shared/model/grading-scheme.model';
import { useGetGradingSchemes } from 'app/shared/services/grading-scheme-api';
import { IAssignment } from 'app/shared/model/assignment.model';
import { getSubmissionsInJourney } from 'app/shared/services/submission-api';
import { IUser } from 'app/shared/model/user.model';
import { IJourney } from 'app/shared/model/journey.model';

interface GradeSheetProps {
  journey?: IJourney;
}

const GradeSheet: FC<GradeSheetProps> = ({ journey }) => {
  const { data: journeyStudents } = useGetJourneyStudents(journey?.id, !!journey?.id);
  const { data: milestones } = useGetMilestones(journey?.id, true);
  const [averageGrades, setAverageGrades] = useState([]);
  const [journeySubmissions, setJourneySubmissions] = useState<ISubmission[]>([]);
  const [maxRowCount, setMaxRowCount] = useState<number>(0);
  const [gradingSchemes, setGradingSchemes] = useState<GradingScheme[]>([]);
  useGetGradingSchemes(setGradingSchemes);

  const getJourneyAssignments = (): IAssignment[] => {
    return milestones && [].concat(...milestones?.map(m => m.assignments));
  };

  const findGradingScheme = (sub: ISubmission): GradingScheme => {
    const journeyAssignments = getJourneyAssignments();
    const gradingSchemeId = journeyAssignments?.find(a => a?.id === sub.assignmentId)?.gradingSchemeId;
    return gradingSchemes?.find(g => g.id === gradingSchemeId);
  };

  useEffect(() => {
    if (journey && gradingSchemes?.length && milestones?.length) {
      getStudentsAverageGrades(journey.id).then(res => setAverageGrades(res.data));
      getSubmissionsInJourney(journey.id, true).then(res => {
        res = res.filter(submission => submission.submissionFeedbacks?.some(feedback => !!feedback.grade));
        setMaxRowCount(
          Math.max(...res?.map(sub => res.filter(r => r.authors?.find(a => sub?.authors?.length && a.id === sub?.authors[0]?.id)).length))
        );
        setJourneySubmissions(res?.filter(s => !findGradingScheme(s)?.isNonGradable()));
      });
    }
  }, [journey, gradingSchemes, milestones]);

  const findStudentSubmission = (s: IUser) => {
    return journeySubmissions?.find(sub => sub.authors?.find(a => a.id === s.id));
  };

  const getRows = () => {
    return (
      <>
        {journeyStudents?.map(({ user }, index) => (
          <tr key={index}>
            <td className={'d-flex align-items-center'}>
              <Avatar className={'mr-2'} fullName={getFullName(user)} />
              <span>{getFullName(user)}</span>
            </td>
            <td>
              {averageGrades[user.id] > 0 && findStudentSubmission(user) && (
                <div className={`average-grade text-white rounded bg-${findClassNameByGradePercentage(averageGrades[user.id])}`}>
                  {Math.round(averageGrades[user.id] * 100) / 100}
                </div>
              )}
            </td>
            <StudentGradeRows
              studentSubmissions={journeySubmissions.filter(sub => sub.authors?.find(a => a.id === user.id))}
              maxRowCount={maxRowCount}
              student={user}
              assignments={getJourneyAssignments()}
              gradingSchemes={gradingSchemes}
              journeyId={journey.id}
            />
          </tr>
        ))}
      </>
    );
  };

  return (
    <Row className="mt-4">
      <Col className="grade-sheet-wrapper">
        <Table striped className={'grade-sheet-table'}>
          <tbody>{journey && getRows()}</tbody>
        </Table>
      </Col>
    </Row>
  );
};

export default GradeSheet;
