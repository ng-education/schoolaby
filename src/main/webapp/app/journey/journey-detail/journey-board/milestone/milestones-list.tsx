import React, { FC, useContext, useEffect, useState } from 'react';
import { Spinner } from 'app/shared/layout/spinner/spinner';
import { JourneyDetailContext, MilestoneContext } from 'app/journey/journey-detail/journey-detail-new';
import MilestoneWithAssignmentsNew from 'app/journey/journey-detail/journey-board/milestone/milestone-with-assignments-new';
import { IMilestone } from 'app/shared/model/milestone.model';
import { translate } from 'react-jhipster';
import { Row, Col } from 'reactstrap';
import { EntityState } from 'app/shared/model/enumerations/entity-state';
import sortBy from 'lodash/sortBy';
import { useDisplayEntityUpdateState, useEntityUpdateIdState } from 'app/shared/contexts/entity-update-context';

interface MilestonesListProps {
  milestones: IMilestone[];
}
const MilestonesList: FC<MilestonesListProps> = ({ milestones }) => {
  const { isAllowedToModify, journey } = useContext(JourneyDetailContext);
  const [scrollToMilestoneId, setScrollToMilestoneId] = useState<number>(undefined);
  const { clearEntityUpdateIds } = useEntityUpdateIdState();
  const { openMilestoneUpdate } = useDisplayEntityUpdateState();

  useEffect(() => {
    const activeMilestones = milestones?.length
      ? sortBy(milestones, 'endDate').filter(milestone => ![EntityState.COMPLETED, EntityState.FAILED].includes(milestone.state))
      : [];

    activeMilestones.length && setScrollToMilestoneId(activeMilestones[0].id);
  }, [milestones]);

  const openCreateMilestoneForm = () => {
    clearEntityUpdateIds();
    openMilestoneUpdate();
  };

  const AddMilestonePlaceholder = () => (
    <div className={'d-flex align-items-center justify-content-center add-milestone-placeholder'}>
      <Row>
        <Col className={'d-flex justify-content-center align-items-center'} xs={12} onClick={openCreateMilestoneForm} />
      </Row>
    </div>
  );

  const AddButton = () =>
    isAllowedToModify && (
      <div className={'list milestone-new add-milestone cursor-pointer'} onClick={openCreateMilestoneForm}>
        <p>+ {translate('entity.add.milestone')}</p>
        <AddMilestonePlaceholder />
      </div>
    );

  return !milestones ? (
    <Spinner />
  ) : (
    <>
      {milestones?.map((milestone, i) => {
        return (
          <div className={'list'} key={i}>
            <MilestoneContext.Provider value={{ milestone, isAllowedToModify, journey, scrollToMilestoneId }}>
              <MilestoneWithAssignmentsNew milestoneIndex={i} />
            </MilestoneContext.Provider>
          </div>
        );
      })}
      <AddButton />
    </>
  );
};

export default MilestonesList;
