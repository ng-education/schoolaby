import React, { FC, useContext, useEffect, useRef, useState } from 'react';
import { useResize } from 'app/shared/hooks/useResize';
import { EntityState } from 'app/shared/model/enumerations/entity-state';
import { MilestoneContainerOld } from 'app/journey/journey-detail/journey-board/milestone/milestone-container-old';
import { getClassNameByMilestoneState } from 'app/shared/util/color-utils';
import ListFooterButton from 'app/shared/layout/list-footer-button/list-footer-button';
import { NavigationPath } from 'app/shared/util/navigation-utils';
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd';
import { AssignmentContext, JourneyDetailContext, MilestoneContext } from 'app/journey/journey-detail/journey-detail-old';
import { AssignmentContainer } from 'app/journey/journey-detail/journey-board/assignment/assignment-container';
import { DateTime } from 'luxon';
import { IAssignment } from 'app/shared/model/assignment.model';
import { convertDateTimeToServer } from 'app/shared/util/date-utils';
import { useUpdateAssignment } from 'app/shared/services/assignment-api';
import { useGetMilestones } from 'app/shared/services/milestone-api';
import sortBy from 'lodash/sortBy';
import { useDeadlineTargetState, useNewDeadlineState } from 'app/shared/contexts/entity-detail-context';
import clone from 'lodash/clone';
import { useGetLtiResources } from 'app/shared/services/lti-api';

interface IProps {
  milestoneIndex: number;
}

const MilestoneWithAssignmentsOld: FC<IProps> = ({ milestoneIndex }) => {
  const { milestone } = useContext(MilestoneContext);
  const { isAllowedToModify, journey, previewEnabled } = useContext(JourneyDetailContext);
  const { mutate: updateAssignment, isSuccess } = useUpdateAssignment();
  useGetMilestones(milestone?.journeyId, isSuccess, previewEnabled);
  const { setDeadlineTarget, deadlineTarget } = useDeadlineTargetState();
  const { newDeadline, setNewDeadline } = useNewDeadlineState();
  const [srcIndex, setSrcIndex] = useState<number>();
  const [destIndex, setDestIndex] = useState<number>();

  const [initialAssignments, setInitialAssignments] = useState<IAssignment[]>();
  const [assignments, setAssignments] = useState<IAssignment[]>([]);
  const ref = useRef(null);
  const { height } = useResize(ref);
  const { data: ltiResources } = useGetLtiResources({ assignmentId: deadlineTarget?.id }, !!deadlineTarget?.id);

  const closeDeadlineModal = () => {
    setNewDeadline(null);
    setDeadlineTarget(null);
  };

  const updateDeadline = () => {
    const updatedAssignment = {
      ...deadlineTarget,
      ltiResources,
      deadline: newDeadline,
    };

    const updatedAssignments = [...assignments];
    const assignmentIndex = updatedAssignments.findIndex(assignment => assignment.id === updatedAssignment.id);
    updatedAssignments.splice(assignmentIndex, 1);
    updatedAssignments.splice(destIndex, 0, updatedAssignment);
    setAssignments(updatedAssignments);

    updateAssignment(updatedAssignment, {
      onError() {
        setAssignments(initialAssignments);
      },
    });

    closeDeadlineModal();
  };

  useEffect(() => {
    if (srcIndex && assignments[srcIndex]?.deadline === newDeadline) {
      closeDeadlineModal();
    }

    if (milestone?.id === deadlineTarget?.milestoneId && newDeadline) {
      updateDeadline();
    }
  }, [newDeadline]);

  useEffect(() => {
    const sortedAssignments = sortBy(milestone.assignments, assignment => new Date(assignment.deadline));
    setAssignments(sortedAssignments);
    setInitialAssignments(sortedAssignments);
  }, [milestone]);

  const onDragEnd = result => {
    if (!result.destination) {
      return;
    }

    const sourceIndex = result.source.index;
    const destinationIndex = result.destination.index;

    if (sourceIndex === destinationIndex) {
      return;
    }

    let updatedDeadline;

    const previousDeadlineISO =
      destinationIndex > sourceIndex || destinationIndex === 0
        ? assignments[destinationIndex].deadline
        : assignments[destinationIndex - 1].deadline;
    const nextDeadlineISO =
      destinationIndex < sourceIndex || destinationIndex === assignments.length - 1
        ? assignments[destinationIndex].deadline
        : assignments[destinationIndex + 1].deadline;

    const previousDeadline = DateTime.fromISO(previousDeadlineISO);
    const nextDeadline = DateTime.fromISO(nextDeadlineISO);

    if (destinationIndex === assignments.length - 1 || destinationIndex === 0) {
      updatedDeadline = destinationIndex === 0 ? previousDeadline.minus({ days: 1 }) : previousDeadline.plus({ days: 1 });
    } else if (nextDeadline.ordinal === previousDeadline.ordinal) {
      updatedDeadline = previousDeadline;
    } else {
      updatedDeadline = previousDeadline.plus({ days: 1 });
    }

    updatedDeadline = convertDateTimeToServer(updatedDeadline.toISO());
    const clonedAssignment = clone(assignments[sourceIndex]);
    clonedAssignment.deadline = updatedDeadline;

    setDeadlineTarget(clonedAssignment);
    setSrcIndex(sourceIndex);
    setDestIndex(destinationIndex);
  };

  return (
    <span className={`content-wrapper ${!previewEnabled && milestone.state === EntityState.COMPLETED ? 'completed' : ''}`}>
      <MilestoneContainerOld ref={ref} />
      <DragDropContext onDragEnd={onDragEnd}>
        <Droppable droppableId={milestone.id?.toString()}>
          {provided => (
            <ul
              ref={provided.innerRef}
              {...provided.droppableProps}
              key={milestoneIndex}
              style={{ maxHeight: `calc(100% - ${height}px)` }}
              className={`assignment-list rounded-bottom ${getClassNameByMilestoneState(
                previewEnabled ? EntityState.NOT_STARTED : milestone.state,
                'bg',
                'medium'
              )}`}
            >
              {assignments && <AssignmentsList assignments={Object.values(assignments)} />}
              {provided.placeholder}
              {isAllowedToModify && (
                <>
                  <ListFooterButton
                    label={'entity.add.task'}
                    link={`${NavigationPath.ADD_ASSIGNMENT}?milestoneId=${milestone?.id}&journeyId=${journey.id}`}
                  />
                </>
              )}
            </ul>
          )}
        </Droppable>
      </DragDropContext>
    </span>
  );
};

const AssignmentsList = ({ assignments }: { assignments: IAssignment[] }) => {
  const { isAllowedToModify, journey, previewEnabled, toggleAssignmentView } = useContext(MilestoneContext);

  return (
    <>
      {assignments.length > 0 &&
        assignments.map((assignment, j) => (
          <Draggable
            key={assignment.id?.toString()}
            draggableId={assignment.id?.toString()}
            index={j}
            isDragDisabled={!isAllowedToModify || previewEnabled || assignment.state === EntityState.COMPLETED}
          >
            {provided => (
              <li key={j} ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
                <AssignmentContext.Provider value={{ assignment, isAllowedToModify, journey, previewEnabled, toggleAssignmentView }}>
                  <AssignmentContainer />
                </AssignmentContext.Provider>
              </li>
            )}
          </Draggable>
        ))}
    </>
  );
};

export default MilestoneWithAssignmentsOld;
