import React, { forwardRef, SyntheticEvent, useContext, useEffect, useRef, useState } from 'react';
import { Dropdown, DropdownToggle } from 'reactstrap';
import { translate } from 'react-jhipster';
import { useHistory } from 'react-router-dom';
import { MilestoneContext } from 'app/journey/journey-detail/journey-detail-old';
import { getClassNameByMilestoneState, ICON_GREY } from 'app/shared/util/color-utils';
import { EntityState } from 'app/shared/model/enumerations/entity-state';
import MenuDropdown from 'app/shared/layout/menu-dropdown/menu-dropdown';
import Icon from 'app/shared/icons';
import { MILESTONE } from 'app/shared/util/entity-utils';
import './milestone-container-old.scss';
import LinesEllipsis from 'react-lines-ellipsis';
import ReactMarkdown from 'react-markdown';

export const MilestoneContainerOld = forwardRef((props, ref?: () => void) => {
  const { isAllowedToModify, milestone, journey, previewEnabled, toggleMilestoneView } = useContext(MilestoneContext);
  const currentState = previewEnabled ? EntityState.NOT_STARTED : milestone.state;
  const history = useHistory();

  const scrollRef = useRef(null);
  useEffect(() => {
    scrollRef?.current?.scrollIntoView({ behavior: 'smooth', inline: 'center' });
  }, [scrollRef?.current]);

  const [dropdownOpen, setDropdownOpen] = useState(false);
  const toggleDropDown = (event: SyntheticEvent) => {
    event.preventDefault();
    setDropdownOpen(prevState => !prevState);
  };

  const handleMilestoneClick = () => {
    if (!previewEnabled) {
      history.push(`/milestone/${milestone.id}?journeyId=${journey.id}`);
    } else {
      toggleMilestoneView(milestone.id);
    }
  };

  return (
    <div className={'line-holder'} ref={ref} onClick={handleMilestoneClick}>
      <div
        className={`milestone text-center ${getClassNameByMilestoneState(currentState, 'border')}`}
        ref={[EntityState.IN_PROGRESS, EntityState.URGENT].includes(milestone.state) ? scrollRef : null}
      >
        <div className={`milestone-title ${getClassNameByMilestoneState(currentState, 'text')}`}>
          <LinesEllipsis text={milestone.title} maxLine={2} />
        </div>
        {isAllowedToModify && (
          <Dropdown
            style={{ float: 'right' }}
            className={'milestone-dropdown'}
            isOpen={dropdownOpen}
            toggle={toggleDropDown}
            aria-label={translate('global.heading.dropdown')}
            onClick={e => e.stopPropagation()}
          >
            <DropdownToggle>
              <Icon
                name={'dropdown'}
                ariaLabel={translate('global.heading.dropdown')}
                width={'16px'}
                height={'16px'}
                stroke={ICON_GREY}
                fill={ICON_GREY}
              />
            </DropdownToggle>
            <MenuDropdown
              entityId={milestone.id}
              entityType={MILESTONE}
              entityTitle={milestone?.title}
              journeyId={journey?.id || milestone?.journeyId}
            />
          </Dropdown>
        )}
        <div className="description preserve-space text-break">
          {milestone.description && <ReactMarkdown source={milestone.description} />}
        </div>
      </div>
    </div>
  );
});
