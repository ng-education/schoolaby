import React, { useContext, useEffect } from 'react';
import { JourneyDetailContext } from 'app/journey/journey-detail/journey-detail-new';
import { useGetMilestones } from 'app/shared/services/milestone-api';
import './journey-board-new.scss';
import DeadlineModalNew from 'app/journey/journey-detail/journey-board/assignment/deadline-modal-new';
import { DragDropContext } from 'react-beautiful-dnd';
import { convertDateTimeToServer } from 'app/shared/util/date-utils';
import { DateTime } from 'luxon';
import clone from 'lodash/clone';
import { useDeadlineTargetState, useNewDeadlineState } from 'app/shared/contexts/entity-detail-context';
import { useUpdateAssignment } from 'app/shared/services/assignment-api';
import { sortBy } from 'lodash';
import { EntityUpdateProvider } from 'app/shared/contexts/entity-update-context';
import EntityUpdate from 'app/shared/layout/entity-update/entity-update';
import { useGetLtiResources } from 'app/shared/services/lti-api';
import MilestonesList from 'app/journey/journey-detail/journey-board/milestone/milestones-list';

export const JourneyBoardNew = () => {
  const { isAllowedToModify, journey } = useContext(JourneyDetailContext);
  const { data: milestones, refetch } = useGetMilestones(journey?.id, !!journey?.id);
  const { setDeadlineTarget, deadlineTarget } = useDeadlineTargetState();
  const { newDeadline, setNewDeadline } = useNewDeadlineState();
  const { mutate: updateAssignment, status } = useUpdateAssignment();
  const { data: ltiResources } = useGetLtiResources({ assignmentId: deadlineTarget?.id }, !!deadlineTarget?.id);

  const closeDeadlineModal = () => {
    setNewDeadline(null);
    setDeadlineTarget(null);
  };

  const updateDeadline = () => {
    const updatedAssignment = {
      ...deadlineTarget,
      ltiResources,
      deadline: newDeadline,
    };

    updateAssignment(updatedAssignment);

    closeDeadlineModal();
  };

  useEffect(() => {
    if (status === 'success') {
      refetch();
    }
  }, [status]);

  useEffect(() => {
    if (newDeadline) {
      updateDeadline();
    }
  }, [newDeadline]);

  const onDragEnd = result => {
    if (!result.destination) {
      return;
    }

    const destinationMilestone = milestones?.find(milestone => milestone?.id === +result.destination.droppableId);
    const sourceMilestone = milestones?.find(milestone => milestone?.id === +result.source.droppableId);
    const destinationAssignments = sortBy(destinationMilestone?.assignments, assignment => new Date(assignment.deadline));
    const sourceAssignments = sortBy(sourceMilestone?.assignments, assignment => new Date(assignment.deadline));

    const sourceIndex = result.source.index;
    let destinationIndex = result.destination.index;

    if (sourceMilestone?.id === destinationMilestone?.id && sourceIndex === destinationIndex) {
      return;
    }

    let updatedDeadline;

    // If we have no assignments take the endDate of milestone as a reference
    if (!destinationMilestone?.assignments || destinationMilestone?.assignments?.length === 0) {
      updatedDeadline = DateTime.fromISO(destinationMilestone?.endDate);
    } else {
      if (!destinationAssignments[destinationIndex]) {
        destinationIndex -= 1;
      }
      const previousDeadlineISO =
        destinationIndex > sourceIndex || destinationIndex === 0
          ? destinationAssignments[destinationIndex].deadline
          : destinationAssignments[destinationIndex - 1].deadline;
      const nextDeadlineISO =
        destinationIndex < sourceIndex || destinationIndex === destinationAssignments.length - 1
          ? destinationAssignments[destinationIndex].deadline
          : destinationAssignments[destinationIndex + 1].deadline;

      const previousDeadline = DateTime.fromISO(previousDeadlineISO);
      const nextDeadline = DateTime.fromISO(nextDeadlineISO);

      if (destinationIndex === destinationAssignments.length - 1 || destinationIndex === 0) {
        updatedDeadline = destinationIndex === 0 ? previousDeadline.minus({ days: 1 }) : previousDeadline.plus({ days: 1 });
      } else if (nextDeadline.ordinal === previousDeadline.ordinal) {
        updatedDeadline = previousDeadline;
      } else {
        updatedDeadline = previousDeadline.plus({ days: 1 });
      }
    }

    updatedDeadline = convertDateTimeToServer(updatedDeadline.toISO());
    const clonedAssignment = clone(sourceAssignments[sourceIndex]);
    clonedAssignment.deadline = updatedDeadline;
    clonedAssignment.milestoneId = destinationMilestone?.id;
    setDeadlineTarget(clonedAssignment);
  };

  return (
    <div className={`journey-content new${isAllowedToModify ? ' mobile-nav-enabled' : ''}`}>
      <div className="milestones">
        <div className="ui">
          <nav />
          <nav />
          <div id="lists" className="lists new">
            <EntityUpdateProvider>
              <DragDropContext onDragEnd={onDragEnd}>
                <MilestonesList milestones={milestones} />
              </DragDropContext>
              <DeadlineModalNew />
              <EntityUpdate />
            </EntityUpdateProvider>
          </div>
        </div>
      </div>
    </div>
  );
};
