import React, { FC, useEffect, useState } from 'react';
import { Button } from 'reactstrap';
import { translate } from 'react-jhipster';
import './assignment-container.scss';
import DatePicker from 'react-datepicker';
import { DateTime } from 'luxon';
import { useDeadlineTargetState, useNewDeadlineState } from 'app/shared/contexts/entity-detail-context';
import ConfirmationModalNew from 'app/shared/layout/confirmation-modal-new/confirmation-modal-new';
import { connect } from 'react-redux';
import { IRootState } from 'app/shared/reducers';

const DeadlineModalNew: FC<StateProps> = ({ locale }) => {
  const { setDeadlineTarget, deadlineTarget } = useDeadlineTargetState();
  const { setNewDeadline } = useNewDeadlineState();
  const [currentDeadline, setCurrentDeadline] = useState(null);

  useEffect(() => {
    if (deadlineTarget?.deadline) {
      // @ts-ignore
      if (deadlineTarget?.deadline instanceof Date) {
        setCurrentDeadline(deadlineTarget?.deadline);
      } else {
        setCurrentDeadline(DateTime.fromISO(deadlineTarget?.deadline).setZone('local', { keepLocalTime: false }).toJSDate());
      }
    }
  }, [deadlineTarget?.deadline]);

  const closeModal = () => setDeadlineTarget(null);

  const ConfirmationButton = () => (
    <Button
      onClick={() => {
        setNewDeadline(currentDeadline);
      }}
      type="button"
      className="btn-primary border-0 px-4 py-2 mr-3 mt-3"
    >
      {translate('entity.action.save')}
    </Button>
  );

  const CancelButton = () => (
    <Button type="button" className="cancel-btn bg-white px-4 py-2 ml-3 mt-3" onClick={closeModal}>
      {translate('entity.action.cancel')}
    </Button>
  );

  return (
    <span>
      <ConfirmationModalNew
        modalTitle={translate('schoolabyApp.assignment.deadlineModal.choose')}
        closeModal={closeModal}
        customCancelButton={<CancelButton />}
        customConfirmationButton={<ConfirmationButton />}
        isModalOpen={!!deadlineTarget}
        className={'deadline-modal'}
      >
        <DatePicker
          selected={currentDeadline}
          timeCaption={translate('tracker.table.time')}
          onChange={date => setCurrentDeadline(DateTime.fromJSDate(date).setZone('local', { keepLocalTime: true }).toJSDate())}
          open
          customInput={<input type={'hidden'} />}
          showTimeSelect
          locale={locale}
        />
      </ConfirmationModalNew>
    </span>
  );
};

const mapStateToProps = ({ locale }: IRootState) => ({
  locale: locale.currentLocale,
});

type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(DeadlineModalNew);
