import './journey-board.scss';
import React, { useContext } from 'react';
import { getFormattedDate } from 'app/shared/util/date-utils';
import { NavigationPath } from 'app/shared/util/navigation-utils';
import { getClassNameByMilestoneState } from 'app/shared/util/color-utils';
import ListFooterButton from 'app/shared/layout/list-footer-button/list-footer-button';
import { JourneyDetailContext, MilestoneContext } from 'app/journey/journey-detail/journey-detail-old';
import { EntityState } from 'app/shared/model/enumerations/entity-state';
import { DateTime } from 'luxon';
import MilestoneWithAssignmentsOld from 'app/journey/journey-detail/journey-board/milestone/milestone-with-assignments-old';
import { useGetMilestones } from 'app/shared/services/milestone-api';
import { Spinner } from 'app/shared/layout/spinner/spinner';
import { DeadlineModal } from 'app/journey/journey-detail/journey-board/assignment/deadline-modal';

export const JourneyBoardOld = () => (
  <div className="journey-content">
    <div className="milestones">
      <div className="ui">
        <nav />
        <nav />
        <div id="lists" className="lists">
          <MilestonesList />
          <DeadlineModal />
        </div>
      </div>
    </div>
  </div>
);

const MilestonesList = () => {
  const { currentLocale, isAllowedToModify, previewEnabled, journey, toggleMilestoneView, toggleAssignmentView } = useContext(
    JourneyDetailContext
  );
  const { data: milestones } = useGetMilestones(journey?.id, !!journey?.id, previewEnabled);

  const getMilestoneDateContainerClassName = (state: EntityState) => {
    if (state === EntityState.COMPLETED || state === EntityState.NOT_STARTED) {
      return getClassNameByMilestoneState(state, 'bg', 'transparent');
    } else {
      return getClassNameByMilestoneState(state, 'bg');
    }
  };

  return !milestones ? (
    <Spinner />
  ) : (
    <>
      {milestones?.map((milestone, i) => {
        const currentState = previewEnabled ? EntityState.NOT_STARTED : milestone.state;
        const lineColor = getClassNameByMilestoneState(currentState, 'bg');

        return (
          <div className={`list ${getClassNameByMilestoneState(currentState, 'scrollbar')}`} key={i}>
            <div className={'milestone-dates'}>
              <div className={`left-line ${isAllowedToModify && 'extended'} ${getClassNameByMilestoneState(currentState, 'bg')}`} />
              <div className={`rounded vertical-pointer ${getMilestoneDateContainerClassName(currentState)}`}>
                {getFormattedDate(milestone.endDate, DateTime.DATE_FULL, currentLocale)}
              </div>
              <div className={`vertical-line ${lineColor}`} />
            </div>
            <MilestoneContext.Provider
              value={{ milestone, isAllowedToModify, journey, previewEnabled, toggleMilestoneView, toggleAssignmentView }}
            >
              <MilestoneWithAssignmentsOld milestoneIndex={i} />
            </MilestoneContext.Provider>
          </div>
        );
      })}
      {isAllowedToModify && (
        <div className={'list old'}>
          <div className={'line-holder'}>
            <div className={'right-line'} />
            <ListFooterButton
              className={'milestone-footer'}
              label={'entity.add.milestone'}
              link={`${NavigationPath.ADD_MILESTONE}?journeyId=${journey.id}`}
            />
          </div>
        </div>
      )}
    </>
  );
};
