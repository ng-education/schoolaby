import { IClass } from 'app/shared/model/class.model';

export const classes: IClass[] = [
  {
    name: '9b',
    timeRange: '2020-2021',
    students: [
      {
        firstName: 'Mari',
        lastName: 'Maasikas',
        email: 'mari.maasikas@gmail.com',
        personRoles: [
          {
            grade: '9',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Eerik',
        lastName: 'Kaartmenn',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '9',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Juhan',
        lastName: 'Jõgisoo',
        email: 'juhan.jõgisoo@gmail.com',
        personRoles: [
          {
            grade: '9',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Mati',
        lastName: 'Maaküla',
        email: 'mati.maaküla@gmail.com',
        personRoles: [
          {
            grade: '9',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Peep',
        lastName: 'Ratas',
        email: 'peep.ratas@gmail.com',
        personRoles: [
          {
            grade: '9',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Mihkel',
        lastName: 'Kaasik',
        email: 'mihkel.kaasik@gmail.com',
        personRoles: [
          {
            grade: '9',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Henri',
        lastName: 'Leht',
        email: 'henri.leht@gmail.com',
        personRoles: [
          {
            grade: '9',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Jaak',
        lastName: 'Lahesoo',
        email: 'jaak.lahesoo@gmail.com',
        personRoles: [
          {
            grade: '9',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Sten',
        lastName: 'Paju',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '9',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Riho',
        lastName: 'Päring',
        email: 'riho.päring@gmail.com',
        personRoles: [
          {
            grade: '9',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Kati',
        lastName: 'Maasing',
        email: 'kati.maasing@gmail.com',
        personRoles: [
          {
            grade: '9',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Joonathan',
        lastName: 'Pihlakas',
        email: 'joonathan.pihlakas@gmail.com',
        personRoles: [
          {
            grade: '9',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Miikael',
        lastName: 'Tamm',
        email: 'miikael.tamm@gmail.com',
        personRoles: [
          {
            grade: '9',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Mart',
        lastName: 'Tammeleht',
        email: 'mart.tammeleht@gmail.com',
        personRoles: [
          {
            grade: '9',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Tony',
        lastName: 'Wind',
        email: 'tony.wind@gmail.com',
        personRoles: [
          {
            grade: '9',
            parallel: 'b',
          },
        ],
      },
    ],
  },
  {
    name: '6a',
    timeRange: '2020-2021',
    students: [
      {
        firstName: 'Eerik',
        lastName: 'Kaartmenn',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '6',
            parallel: 'a',
          },
        ],
      },
      {
        firstName: 'Sten',
        lastName: 'Paju',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '6',
            parallel: 'a',
          },
        ],
      },
      {
        firstName: 'Liisbeth',
        lastName: 'Toomingas',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '6',
            parallel: 'a',
          },
        ],
      },
      {
        firstName: 'Liis',
        lastName: 'Pajusalu',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '6',
            parallel: 'a',
          },
        ],
      },
    ],
  },
  {
    name: '5',
    timeRange: '2020-2021',
    students: [
      {
        firstName: 'Sten',
        lastName: 'Paju',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '5',
            parallel: '',
          },
        ],
      },
      {
        firstName: 'Tiit',
        lastName: 'Tõnisson',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '5',
            parallel: '',
          },
        ],
      },
      {
        firstName: 'Ilja',
        lastName: 'Pärg',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '5',
            parallel: '',
          },
        ],
      },
      {
        firstName: 'Reet',
        lastName: 'Leier',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '5',
            parallel: '',
          },
        ],
      },
      {
        firstName: 'Liis',
        lastName: 'Pajusalu',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '5',
            parallel: '',
          },
        ],
      },
      {
        firstName: 'Mart',
        lastName: 'Tammeleht',
        email: 'mart.tammeleht@gmail.com',
        personRoles: [
          {
            grade: '5',
            parallel: '',
          },
        ],
      },
      {
        firstName: 'Margus',
        lastName: 'Lai',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '5',
            parallel: '',
          },
        ],
      },
    ],
  },
  {
    name: '8b',
    timeRange: '2020-2021',
    students: [
      {
        firstName: 'Mari',
        lastName: 'Maasikas',
        email: 'mari.maasikas@gmail.com',
        personRoles: [
          {
            grade: '8',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Juhan',
        lastName: 'Jõgisoo',
        email: 'juhan.jõgisoo@gmail.com',
        personRoles: [
          {
            grade: '8',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Mati',
        lastName: 'Maaküla',
        email: 'mati.maaküla@gmail.com',
        personRoles: [
          {
            grade: '8',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Peep',
        lastName: 'Ratas',
        email: 'peep.ratas@gmail.com',
        personRoles: [
          {
            grade: '8',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Mihkel',
        lastName: 'Kaasik',
        email: 'mihkel.kaasik@gmail.com',
        personRoles: [
          {
            grade: '8',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Margus',
        lastName: 'Lai',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '8',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Henri',
        lastName: 'Leht',
        email: 'henri.leht@gmail.com',
        personRoles: [
          {
            grade: '8',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Jaak',
        lastName: 'Lahesoo',
        email: 'jaak.lahesoo@gmail.com',
        personRoles: [
          {
            grade: '8',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Riho',
        lastName: 'Päring',
        email: 'riho.päring@gmail.com',
        personRoles: [
          {
            grade: '8',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Kati',
        lastName: 'Maasing',
        email: 'kati.maasing@gmail.com',
        personRoles: [
          {
            grade: '8',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Joonathan',
        lastName: 'Pihlakas',
        email: 'joonathan.pihlakas@gmail.com',
        personRoles: [
          {
            grade: '8',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Miikael',
        lastName: 'Tamm',
        email: 'miikael.tamm@gmail.com',
        personRoles: [
          {
            grade: '8',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Mart',
        lastName: 'Tammeleht',
        email: 'mart.tammeleht@gmail.com',
        personRoles: [
          {
            grade: '8',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Tony',
        lastName: 'Wind',
        email: 'tony.wind@gmail.com',
        personRoles: [
          {
            grade: '8',
            parallel: 'b',
          },
        ],
      },
    ],
  },
  {
    name: '6b',
    timeRange: '2020-2021',
    students: [
      {
        firstName: 'Mari',
        lastName: 'Maasikas',
        email: 'mari.maasikas@gmail.com',
        personRoles: [
          {
            grade: '6',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Juhan',
        lastName: 'Jõgisoo',
        email: 'juhan.jõgisoo@gmail.com',
        personRoles: [
          {
            grade: '6',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Mati',
        lastName: 'Maaküla',
        email: 'mati.maaküla@gmail.com',
        personRoles: [
          {
            grade: '6',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Peep',
        lastName: 'Ratas',
        email: 'peep.ratas@gmail.com',
        personRoles: [
          {
            grade: '6',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Mihkel',
        lastName: 'Kaasik',
        email: 'mihkel.kaasik@gmail.com',
        personRoles: [
          {
            grade: '6',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Henri',
        lastName: 'Leht',
        email: 'henri.leht@gmail.com',
        personRoles: [
          {
            grade: '6',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Jaak',
        lastName: 'Lahesoo',
        email: 'jaak.lahesoo@gmail.com',
        personRoles: [
          {
            grade: '6',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Riho',
        lastName: 'Päring',
        email: 'riho.päring@gmail.com',
        personRoles: [
          {
            grade: '6',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Kati',
        lastName: 'Maasing',
        email: 'kati.maasing@gmail.com',
        personRoles: [
          {
            grade: '6',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Joonathan',
        lastName: 'Pihlakas',
        email: 'joonathan.pihlakas@gmail.com',
        personRoles: [
          {
            grade: '6',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Miikael',
        lastName: 'Tamm',
        email: 'miikael.tamm@gmail.com',
        personRoles: [
          {
            grade: '6',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Mart',
        lastName: 'Tammeleht',
        email: 'mart.tammeleht@gmail.com',
        personRoles: [
          {
            grade: '6',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Tony',
        lastName: 'Wind',
        email: 'tony.wind@gmail.com',
        personRoles: [
          {
            grade: '6',
            parallel: 'b',
          },
        ],
      },
    ],
  },
  {
    name: '6c',
    timeRange: '2020-2021',
    students: [
      {
        firstName: 'Eerik',
        lastName: 'Kaartmenn',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '6',
            parallel: 'c',
          },
        ],
      },
      {
        firstName: 'Sten',
        lastName: 'Paju',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '6',
            parallel: 'c',
          },
        ],
      },
      {
        firstName: 'Mari',
        lastName: 'Maasikas',
        email: 'mari.maasikas@gmail.com',
        personRoles: [
          {
            grade: '6',
            parallel: 'c',
          },
        ],
      },
      {
        firstName: 'Liisbeth',
        lastName: 'Toomingas',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '6',
            parallel: 'c',
          },
        ],
      },
      {
        firstName: 'Liis',
        lastName: 'Pajusalu',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '6',
            parallel: 'c',
          },
        ],
      },
    ],
  },
  {
    name: '12',
    timeRange: '2020-2021',
    students: [
      {
        firstName: 'Tiit',
        lastName: 'Tõnisson',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '12',
            parallel: '',
          },
        ],
      },
      {
        firstName: 'Liisbeth',
        lastName: 'Toomingas',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '12',
            parallel: '',
          },
        ],
      },
      {
        firstName: 'Ilja',
        lastName: 'Pärg',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '12',
            parallel: '',
          },
        ],
      },
      {
        firstName: 'Mari',
        lastName: 'Maasikas',
        email: 'mari.maasikas@gmail.com',
        personRoles: [
          {
            grade: '12',
            parallel: '',
          },
        ],
      },
      {
        firstName: 'Tambet',
        lastName: 'Pärnoja',
        email: 'tambet.pärnoja@gmail.com',
        personRoles: [
          {
            grade: '12',
            parallel: '',
          },
        ],
      },
      {
        firstName: 'Reet',
        lastName: 'Leier',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '12',
            parallel: '',
          },
        ],
      },
      {
        firstName: 'Margus',
        lastName: 'Lai',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '12',
            parallel: '',
          },
        ],
      },
    ],
  },
  {
    name: '10c',
    timeRange: '2020-2021',
    students: [
      {
        firstName: 'Mari',
        lastName: 'Maasikas',
        email: 'mari.maasikas@gmail.com',
        personRoles: [
          {
            grade: '10',
            parallel: 'c',
          },
        ],
      },
      {
        firstName: 'Eerik',
        lastName: 'Kaartmenn',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '10',
            parallel: 'c',
          },
        ],
      },
      {
        firstName: 'Mati',
        lastName: 'Maaküla',
        email: 'mati.maaküla@gmail.com',
        personRoles: [
          {
            grade: '10',
            parallel: 'c',
          },
        ],
      },
      {
        firstName: 'Peep',
        lastName: 'Ratas',
        email: 'peep.ratas@gmail.com',
        personRoles: [
          {
            grade: '10',
            parallel: 'c',
          },
        ],
      },
      {
        firstName: 'Mihkel',
        lastName: 'Kaasik',
        email: 'mihkel.kaasik@gmail.com',
        personRoles: [
          {
            grade: '10',
            parallel: 'c',
          },
        ],
      },
      {
        firstName: 'Henri',
        lastName: 'Leht',
        email: 'henri.leht@gmail.com',
        personRoles: [
          {
            grade: '10',
            parallel: 'c',
          },
        ],
      },
      {
        firstName: 'Jaak',
        lastName: 'Lahesoo',
        email: 'jaak.lahesoo@gmail.com',
        personRoles: [
          {
            grade: '10',
            parallel: 'c',
          },
        ],
      },
      {
        firstName: 'Riho',
        lastName: 'Päring',
        email: 'riho.päring@gmail.com',
        personRoles: [
          {
            grade: '10',
            parallel: 'c',
          },
        ],
      },
      {
        firstName: 'Kati',
        lastName: 'Maasing',
        email: 'kati.maasing@gmail.com',
        personRoles: [
          {
            grade: '10',
            parallel: 'c',
          },
        ],
      },
      {
        firstName: 'Joonathan',
        lastName: 'Pihlakas',
        email: 'joonathan.pihlakas@gmail.com',
        personRoles: [
          {
            grade: '10',
            parallel: 'c',
          },
        ],
      },
      {
        firstName: 'Miikael',
        lastName: 'Tamm',
        email: 'miikael.tamm@gmail.com',
        personRoles: [
          {
            grade: '10',
            parallel: 'c',
          },
        ],
      },
      {
        firstName: 'Mart',
        lastName: 'Tammeleht',
        email: 'mart.tammeleht@gmail.com',
        personRoles: [
          {
            grade: '10',
            parallel: 'c',
          },
        ],
      },
      {
        firstName: 'Tony',
        lastName: 'Wind',
        email: 'tony.wind@gmail.com',
        personRoles: [
          {
            grade: '10',
            parallel: 'c',
          },
        ],
      },
      {
        firstName: 'Ilja',
        lastName: 'Pärg',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '10',
            parallel: 'c',
          },
        ],
      },
    ],
  },
  {
    name: '9a',
    timeRange: '2020-2021',
    students: [
      {
        firstName: 'Tiit',
        lastName: 'Tõnisson',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '9',
            parallel: 'a',
          },
        ],
      },
      {
        firstName: 'Sten',
        lastName: 'Paju',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '9',
            parallel: 'a',
          },
        ],
      },
      {
        firstName: 'Ilja',
        lastName: 'Pärg',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '9',
            parallel: 'a',
          },
        ],
      },
      {
        firstName: 'Tiit',
        lastName: 'Tõnisson',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '9',
            parallel: 'a',
          },
        ],
      },
      {
        firstName: 'Reet',
        lastName: 'Leier',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '9',
            parallel: 'a',
          },
        ],
      },
      {
        firstName: 'Margus',
        lastName: 'Lai',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '9',
            parallel: 'a',
          },
        ],
      },
    ],
  },
  {
    name: '11b',
    timeRange: '2020-2021',
    students: [
      {
        firstName: 'Tiit',
        lastName: 'Tõnisson',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '11',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Eerik',
        lastName: 'Kaartmenn',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '11',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Liisbeth',
        lastName: 'Toomingas',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '11',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Ilja',
        lastName: 'Pärg',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '11',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Reet',
        lastName: 'Leier',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '11',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Mari',
        lastName: 'Maasikas',
        email: 'mari.maasikas@gmail.com',
        personRoles: [
          {
            grade: '11',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Margus',
        lastName: 'Lai',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '11',
            parallel: 'b',
          },
        ],
      },
    ],
  },
  {
    name: '7',
    timeRange: '2020-2021',
    students: [
      {
        firstName: 'Eerik',
        lastName: 'Kaartmenn',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '7',
            parallel: '',
          },
        ],
      },
      {
        firstName: 'Sten',
        lastName: 'Paju',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '7',
            parallel: '',
          },
        ],
      },
      {
        firstName: 'Mari',
        lastName: 'Maasikas',
        email: 'mari.maasikas@gmail.com',
        personRoles: [
          {
            grade: '7',
            parallel: '',
          },
        ],
      },
      {
        firstName: 'Liisbeth',
        lastName: 'Toomingas',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '7',
            parallel: '',
          },
        ],
      },
      {
        firstName: 'Liis',
        lastName: 'Pajusalu',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '7',
            parallel: '',
          },
        ],
      },
    ],
  },
  {
    name: '10b',
    timeRange: '2020-2021',
    students: [
      {
        firstName: 'Henri',
        lastName: 'Leht',
        email: 'henri.leht@gmail.com',
        personRoles: [
          {
            grade: '10',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Jaak',
        lastName: 'Lahesoo',
        email: 'jaak.lahesoo@gmail.com',
        personRoles: [
          {
            grade: '10',
            parallel: 'b',
          },
        ],
      },
      {
        firstName: 'Riho',
        lastName: 'Päring',
        email: 'riho.päring@gmail.com',
        personRoles: [
          {
            grade: '10',
            parallel: 'b',
          },
        ],
      },
    ],
  },
  {
    name: '12c',
    timeRange: '2020-2021',
    students: [
      {
        firstName: 'Tiit',
        lastName: 'Tõnisson',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '12',
            parallel: 'c',
          },
        ],
      },
      {
        firstName: 'Sten',
        lastName: 'Paju',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '7',
            parallel: '',
          },
        ],
      },
      {
        firstName: 'Ilja',
        lastName: 'Pärg',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '12',
            parallel: 'c',
          },
        ],
      },
      {
        firstName: 'Reet',
        lastName: 'Leier',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '12',
            parallel: 'c',
          },
        ],
      },
      {
        firstName: 'Margus',
        lastName: 'Lai',
        email: 'eerik.kaartmenn@gmail.com',
        personRoles: [
          {
            grade: '12',
            parallel: 'c',
          },
        ],
      },
    ],
  },
];
