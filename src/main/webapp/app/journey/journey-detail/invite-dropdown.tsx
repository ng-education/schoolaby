import React, { FC, useState } from 'react';

import './journey-detail-new.scss';
import CustomButton from 'app/shared/layout/custom-button/custom-button';
import { Button, Dropdown, DropdownMenu, DropdownToggle } from 'reactstrap';
import { translate as t } from 'react-jhipster';
import InviteModal from 'app/journey/journey-detail/invite-modal/invite-modal';
import { DropdownItem } from 'reactstrap/lib';
import { IJourney } from 'app/shared/model/journey.model';
import Icon from 'app/shared/icons';

export interface IInviteDropdown {
  journey: IJourney;
  oldDesign?: boolean;
}

const InviteDropdown: FC<IInviteDropdown> = ({ journey, oldDesign }) => {
  const [studentInviteModal, setStudentInviteModal] = useState(false);
  const [teacherInviteModal, setTeacherInviteModal] = useState(false);
  const [inviteDropdownOpen, setInviteDropdownOpen] = useState(false);

  const toggleStudentInviteModal = () => setStudentInviteModal(!studentInviteModal);
  const toggleTeacherInviteModal = () => setTeacherInviteModal(!teacherInviteModal);

  const toggleInviteDropdown = () => setInviteDropdownOpen(prevState => !prevState);

  const DropdownButton = () =>
    oldDesign ? (
      <Button className="btn-sm btn-primary d-flex align-items-center text-nowrap invite-btn-old">
        <Icon name={'export'} className={'mr-2'} fill="white" width="19px" height="17px" />
        {t('schoolabyApp.journey.detail.invite')}
      </Button>
    ) : (
      <CustomButton
        iconName={'invite'}
        title={t('schoolabyApp.journey.detail.invite')}
        buttonType={'secondary'}
        size={'md'}
        className={'h-100'}
      />
    );

  return (
    <>
      <Dropdown isOpen={inviteDropdownOpen} toggle={toggleInviteDropdown}>
        <DropdownToggle tag={'div'} className={'h-100'}>
          <DropdownButton />
        </DropdownToggle>
        <DropdownMenu>
          <DropdownItem
            onClick={() => {
              toggleStudentInviteModal();
              toggleInviteDropdown();
            }}
            toggle={null}
          >
            {t('schoolabyApp.journey.detail.inviteStudents')}
          </DropdownItem>
          {journey?.teacherSignupCode && (
            <DropdownItem
              onClick={() => {
                toggleTeacherInviteModal();
                toggleInviteDropdown();
              }}
              toggle={null}
            >
              {t('schoolabyApp.journey.detail.inviteTeachers')}
            </DropdownItem>
          )}
        </DropdownMenu>
      </Dropdown>
      <InviteModal type={'STUDENT'} toggle={toggleStudentInviteModal} modal={studentInviteModal} journey={journey} />
      <InviteModal type={'TEACHER'} toggle={toggleTeacherInviteModal} modal={teacherInviteModal} journey={journey} />
    </>
  );
};

export default InviteDropdown;
