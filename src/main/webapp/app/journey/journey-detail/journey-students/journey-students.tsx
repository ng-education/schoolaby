import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { IRootState } from 'app/shared/reducers';
import { hasAnyAuthority } from 'app/shared/auth/private-route';
import { AUTHORITIES } from 'app/config/constants';
import StudentListView from 'app/students/students-list-view';
import { EntityState } from 'app/shared/model/enumerations/entity-state';
import { useGetMilestones } from 'app/shared/services/milestone-api';
import './journey-students.scss';
import { translate } from 'react-jhipster';
import CoTeachersListView from 'app/teachers/co-teachers-list-view';
import { IJourneyTeacher } from 'app/shared/model/journey-teacher.model';

const JourneyStudents = ({ journey, isTeacherOrAdmin, account }) => {
  const [coTeachers, setCoTeachers] = useState<IJourneyTeacher[]>([]);
  const { data: milestones } = useGetMilestones(journey?.id, true);

  const filterAndSetCoTeachers = (allTeachers: IJourneyTeacher[]) => {
    setCoTeachers(allTeachers.filter(teacher => teacher.user.id !== journey.creatorId));
  };

  useEffect(() => {
    journey?.teachers?.length && filterAndSetCoTeachers(journey.teachers);
  }, [journey]);

  const isAllowedToModify = isTeacherOrAdmin && journey?.teachers?.some(teacher => teacher.user.id === account.id);

  const renderMilestoneProcess = () => {
    const milestoneCount = milestones?.length;
    const milestoneCompletedCount = milestones?.filter(milestone => milestone.state === EntityState.COMPLETED).length;
    let processClassNames = '';

    if (Math.round((milestoneCompletedCount / milestoneCount) * 100) / 100 <= 0.33) {
      processClassNames = 'border-red';
    }
    if (Math.round((milestoneCompletedCount / milestoneCount) * 100) / 100 <= 0.66) {
      processClassNames = 'border-orange';
    }
    if (Math.round((milestoneCompletedCount / milestoneCount) * 100) / 100 > 0.66) {
      processClassNames = 'border-green completed-bg';
    }
    return <div className={`text-center process-container ${processClassNames}`}>{`${milestoneCompletedCount} / ${milestoneCount}`}</div>;
  };

  return (
    <div className="journey-students">
      {!!(journey.owner && coTeachers?.length) && (
        <>
          <h3>{translate('schoolabyApp.journey.members.coTeachers')}</h3>
          <CoTeachersListView journeyId={journey?.id} coTeachers={coTeachers} />
        </>
      )}
      <h3>{translate('schoolabyApp.journey.members.students')}</h3>
      <StudentListView renderProcess={renderMilestoneProcess()} journeyId={journey?.id} isAllowedToModify={isAllowedToModify} />
    </div>
  );
};

const mapStateToProps = ({ authentication }: IRootState) => ({
  isTeacherOrAdmin: hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.TEACHER, AUTHORITIES.ADMIN]),
  account: authentication.account,
});

export default connect(mapStateToProps)(JourneyStudents);
