import React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';

import { IRootState } from 'app/shared/reducers';
import Heading, { ITabParams } from 'app/shared/layout/heading/heading';
import MenuDropdown from 'app/shared/layout/menu-dropdown/menu-dropdown';
import { hasAnyAuthority } from 'app/shared/auth/private-route';
import { AUTHORITIES } from 'app/config/constants';
import { getHeadingIcon } from 'app/shared/util/journey-utils';
import { JOURNEY } from 'app/shared/util/entity-utils';
import MyJourneysBreadcrumbItem from 'app/shared/layout/heading/breadcrumbs/my-journeys-breadcrumb-item';
import JourneyTitleBreadcrumbItem from 'app/shared/layout/heading/breadcrumbs/journey-title-breadcrumb-item';
import StudentsBreadcrumbItem from 'app/shared/layout/heading/breadcrumbs/students-breadcrumb-item';
import { useGetJourney } from 'app/shared/services/journey-api';
import './journey-students.scss';
import JourneyStudents from './journey-students';
import SpinnerContainer from 'app/shared/layout/spinner-container/spinner-container';

export interface IJourneyStudentProps extends StateProps, RouteComponentProps<{ id: string }> {}

export const JourneyStudentsView = ({ isTeacherOrAdmin, match, account }: IJourneyStudentProps) => {
  const { data: journey, isFetching: isFetchingJourney } = useGetJourney(match.params.id);

  const isAllowedToModify = isTeacherOrAdmin && journey?.teachers?.some(teacher => teacher.user.id === account.id);

  const tabParams: ITabParams = {
    entityType: JOURNEY,
    tabEntityId: Number(match.params.id),
  };

  return (
    <>
      <MyJourneysBreadcrumbItem />
      <JourneyTitleBreadcrumbItem journeyId={journey?.id} journeyTitle={journey?.title} />
      <StudentsBreadcrumbItem journeyId={journey?.id} />
      <Heading
        journey={journey}
        tabParams={tabParams}
        icon={getHeadingIcon(journey)}
        title={journey?.title}
        dropdown={isAllowedToModify && <MenuDropdown entityId={journey?.id} entityType={JOURNEY} entityTitle={journey?.title} />}
      />
      <SpinnerContainer displaySpinner={isFetchingJourney}>{journey ? <JourneyStudents journey={journey} /> : null}</SpinnerContainer>
    </>
  );
};

const mapStateToProps = ({ authentication, locale }: IRootState) => ({
  currentLocale: locale.currentLocale,
  account: authentication.account,
  isTeacherOrAdmin: hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.TEACHER, AUTHORITIES.ADMIN]),
});

type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(JourneyStudentsView);
