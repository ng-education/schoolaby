import React, { useEffect, useState } from 'react';
import AppCard from 'app/shared/layout/lti/app-card';
import ListFooterButton from 'app/shared/layout/list-footer-button/list-footer-button';
import { AppListModal } from 'app/journey/journey-detail/app-list-modal';
import { AppConfigModal } from 'app/journey/journey-detail/app-config-modal/app-config-modal';
import { Col, DropdownItem, DropdownMenu, Row } from 'reactstrap';
import { translate } from 'react-jhipster';
import { ILtiConfig } from 'app/shared/model/lti-config.model';
import {
  createLtiConfiguration,
  deleteLtiConfiguration,
  getLtiApps,
  getLtiConfigurations,
  updateLtiConfiguration,
  useGetLtiConfigurations,
} from 'app/shared/services/lti-api';
import { ILtiApp } from 'app/shared/model/lti-app.model';
import Icon from 'app/shared/icons';
import AddButtonNew from 'app/shared/layout/add-button-new/add-button-new';

const JourneyApps = ({ previewEnabled = false, journey }) => {
  const [configToUpdate, setConfigToUpdate] = useState<ILtiConfig>();
  const [appToRegister, setAppToRegister] = useState<ILtiApp>();
  const [showAppListModal, setShowAppListModal] = useState(false);
  const [allLtiApps, setAllLtiApps] = useState<ILtiApp[]>([]);
  const [ltiConfigurations, setLtiConfigurations] = useState<ILtiConfig[]>([]);
  useGetLtiConfigurations({ journeyId: journey?.id }, !!journey, setLtiConfigurations);

  useEffect(() => {
    getLtiApps().then(response => setAllLtiApps(response.data));
  }, []);

  const toggleAppListModal = () => setShowAppListModal(!showAppListModal);

  const toggleAppRegistrationModal = () => {
    setConfigToUpdate(null);
    setAppToRegister(null);
  };

  const filteredAppSelection = () => {
    const appNames = ltiConfigurations?.map(value => value.ltiApp && value.ltiApp.name);
    return allLtiApps.filter(app => !appNames?.includes(app.name));
  };

  const registerConfig = (event, values) => {
    const entity = {
      ...values,
      journeyId: journey.id,
    };

    if (appToRegister) {
      createLtiConfiguration(entity).then(() => {
        toggleAppRegistrationModal();
        getLtiConfigurations({ journeyId: journey?.id }).then(response => setLtiConfigurations(response.data));
      });
    }
    if (configToUpdate) {
      entity.id = configToUpdate.id;
      updateLtiConfiguration(entity).then(() => {
        toggleAppRegistrationModal();
        getLtiConfigurations({ journeyId: journey?.id }).then(response => setLtiConfigurations(response.data));
      });
    }
  };

  const deleteAppConfig = (config: ILtiConfig) => {
    return deleteLtiConfiguration({ id: config.id }).then(() =>
      getLtiConfigurations({ journeyId: journey?.id }).then(response => {
        setLtiConfigurations(response.data);
      })
    );
  };

  const onModalCardClick = (app: ILtiApp) => {
    setAppToRegister(app);
    toggleAppListModal();
  };

  const appDropDown = (config: ILtiConfig) => {
    return (
      <DropdownMenu right>
        <DropdownItem onClick={() => setConfigToUpdate(config)}>{translate('entity.action.edit')}</DropdownItem>
        <DropdownItem divider />
        <DropdownItem onClick={() => deleteAppConfig(config)}>
          {translate('entity.action.delete')}
          <Icon name={'trash'} className={'ml-2'} width={'18px'} height={'18px'} />
        </DropdownItem>
      </DropdownMenu>
    );
  };

  return (
    <div className="journey-apps">
      <Row noGutters={previewEnabled} className={previewEnabled ? 'mx-3' : undefined}>
        <Col className={'d-flex justify-content-start align-items-center'}>
          <h3 className={'mt-4 mb-4'}>{translate('schoolabyApp.journey.app.myapps')}</h3>
        </Col>
        {!!filteredAppSelection().length && !previewEnabled && (
          <Col className={'d-flex justify-content-end align-items-center'}>
            <AddButtonNew showIcon title={translate('schoolabyApp.journey.app.add')} onClick={toggleAppListModal} className="py-1" />
          </Col>
        )}
      </Row>
      <div className={`lti-apps${previewEnabled ? ' mx-3' : ''}`}>
        <Row noGutters={previewEnabled}>
          {ltiConfigurations?.map(
            (ltiConfig, i) =>
              journey && (
                <AppCard
                  dropDown={ltiConfig?.type !== 'PLATFORM' ? appDropDown(ltiConfig) : null}
                  title={ltiConfig?.ltiApp?.name}
                  description={ltiConfig?.ltiApp?.description}
                  imageSrc={ltiConfig?.ltiApp?.imageUrl}
                  consumerKey={ltiConfig?.consumerKey}
                  sharedSecret={ltiConfig?.sharedSecret}
                  ltiVersion={ltiConfig?.ltiApp?.version}
                  previewEnabled={previewEnabled}
                  key={i}
                  xs={'12'}
                  md={'6'}
                  xl={'4'}
                  onClick={() => !previewEnabled && setConfigToUpdate(ltiConfig)}
                />
              )
          )}
          {!!filteredAppSelection().length && !previewEnabled && (
            <Col xs={'12'} md={'6'} xl={'4'} className={'mb-4'}>
              <ListFooterButton label={'entity.add.app'} className={'h-100'} onClick={toggleAppListModal} />
            </Col>
          )}
        </Row>
      </div>
      <AppListModal onCardClick={onModalCardClick} toggle={toggleAppListModal} showModal={showAppListModal} apps={filteredAppSelection()} />
      <AppConfigModal
        onSubmit={registerConfig}
        toggle={toggleAppRegistrationModal}
        showModal={!!configToUpdate || !!appToRegister}
        ltiApp={appToRegister}
        ltiConfig={configToUpdate}
      />
    </div>
  );
};

export default JourneyApps;
