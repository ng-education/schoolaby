import React, { SetStateAction, useCallback, useEffect, useState } from 'react';
import { IMilestone } from 'app/shared/model/milestone.model';
import { IJourney } from 'app/shared/model/journey.model';
import { IAssignment } from 'app/shared/model/assignment.model';
import { IRootState } from 'app/shared/reducers';
import { hasAnyAuthority } from 'app/shared/auth/private-route';
import { AUTHORITIES } from 'app/config/constants';
import { connect } from 'react-redux';
import { RouteComponentProps, useHistory } from 'react-router-dom';

import './journey-detail-new.scss';
import ContentContainer from 'app/shared/layout/content-container/content-container';
import { useGetJourney, useUpdateJourney } from 'app/shared/services/journey-api';
import HeadingNew, { SUB_HEADING } from 'app/shared/layout/heading/heading-new';
import { getSubjectHeadingIcon } from 'app/shared/icons';
import CustomButton from 'app/shared/layout/custom-button/custom-button';
import { JourneyBoardNew } from 'app/journey/journey-detail/journey-board/journey-board-new';
import { APPS_TAB, GRADES_TAB, JOURNEY_TAB, SUBMISSIONS_TAB, TabType } from 'app/shared/layout/tabs/tabs';
import GradeSheet from 'app/journey/journey-detail/grade-sheet/grade-sheet';
import JourneyApps from 'app/journey/journey-detail/apps/journey-apps';
import JourneyStudents from 'app/journey/journey-detail/journey-students/journey-students';
import { Dropdown, DropdownToggle } from 'reactstrap';
import MenuDropdown from 'app/shared/layout/menu-dropdown/menu-dropdown';
import { JOURNEY } from 'app/shared/util/entity-utils';
import MyJourneysBreadcrumbItem from 'app/shared/layout/heading/breadcrumbs/my-journeys-breadcrumb-item';
import JourneyTitleBreadcrumbItem from 'app/shared/layout/heading/breadcrumbs/journey-title-breadcrumb-item';
import { HomeBreadcrumbItem } from 'app/shared/layout/heading/breadcrumbs/home-breadcrumb-item';
import { translate as t } from 'react-jhipster';
import InviteDropdown from 'app/journey/journey-detail/invite-dropdown';
import { IRubric } from 'app/shared/model/rubric/rubric.model';

export interface IJourneyDetailProps extends StateProps, RouteComponentProps<{ id: string }> {}

export interface IAssignmentContext {
  assignment: IAssignment;
  isAllowedToModify: boolean;
  journey: IJourney;
  milestone?: IMilestone;
  rubric?: IRubric;
}

export const JourneyDetailContext = React.createContext<{
  isAllowedToModify: boolean;
  journey: IJourney;
  activeTab: TabType;
  setActiveTab: React.Dispatch<SetStateAction<TabType>>;
}>(null);

export const MilestoneContext = React.createContext<{
  milestone: IMilestone;
  isAllowedToModify: boolean;
  journey: IJourney;
  scrollToMilestoneId: number;
}>(null);

export const AssignmentContext = React.createContext<IAssignmentContext>(null);

const JourneyDetailNew = ({ isTeacherOrAdmin, match, account }: IJourneyDetailProps) => {
  const history = useHistory();
  const [editDropdownOpen, setEditDropdownOpen] = useState(false);
  const { data: journey, isFetching, refetch } = useGetJourney(match.params.id);
  const [activeTab, setActiveTab] = useState<TabType>(null);
  const { mutate: updateJourney } = useUpdateJourney();

  const isAllowedToModify = isTeacherOrAdmin && journey?.teachers?.some(teacher => teacher.user.id === account.id);

  useEffect(() => {
    const path = history.location.pathname.split('/');

    if (path[3] === 'grades') {
      setActiveTab(GRADES_TAB);
    } else if (path[3] === 'apps') {
      setActiveTab(APPS_TAB);
    } else if (path[3] === 'students') {
      setActiveTab(SUBMISSIONS_TAB);
    } else {
      setActiveTab(JOURNEY_TAB);
    }
  }, [history.location.pathname]);

  const getTabContent = useCallback(() => {
    if (activeTab === JOURNEY_TAB) {
      return <JourneyBoardNew />;
    } else if (activeTab === GRADES_TAB) {
      return <GradeSheet journey={journey} />;
    } else if (activeTab === APPS_TAB) {
      return <JourneyApps journey={journey} />;
    } else if (activeTab === SUBMISSIONS_TAB) {
      return <JourneyStudents journey={journey} />;
    }
  }, [activeTab, journey]);

  const toggleEditDropdown = () => setEditDropdownOpen(prevState => !prevState);

  const PublishButton = {
    title: journey?.template ? t('schoolabyApp.journey.templates.unpublish') : t('schoolabyApp.journey.templates.publish'),
    onClick: () =>
      updateJourney(
        {
          ...journey,
          template: !journey.template,
        },
        {
          onSuccess: () => refetch(),
        }
      ),
  };

  return (
    <JourneyDetailContext.Provider value={{ journey, isAllowedToModify, activeTab, setActiveTab }}>
      <ContentContainer spinner={isFetching} className={'journey-detail-new'}>
        <HeadingNew
          isAllowedToModify={isAllowedToModify}
          headingType={SUB_HEADING}
          icon={getSubjectHeadingIcon(journey?.subject?.label)}
          title={journey?.title}
          tabEntityType={JOURNEY}
          journey={journey}
        >
          <div className={'heading-buttons d-flex justify-content-end h-100 ml-4'}>
            {isAllowedToModify && (
              <>
                <Dropdown isOpen={editDropdownOpen} toggle={toggleEditDropdown}>
                  <DropdownToggle tag={'div'} className={'h-100'}>
                    <CustomButton
                      iconName={'editNew'}
                      title={t('entity.action.edit')}
                      buttonType={'secondary'}
                      className={'h-100'}
                      iconWidth={'13'}
                      iconHeight={'13'}
                      size={'md'}
                      outline
                    />
                  </DropdownToggle>
                  <MenuDropdown
                    entityId={journey.id}
                    entityType={JOURNEY}
                    extraItems={[PublishButton]}
                    showDeleteButton={journey?.owner}
                    entityTitle={journey?.title}
                  />
                </Dropdown>
                <InviteDropdown journey={journey} />
              </>
            )}
            {journey?.videoConferenceUrl && (
              <CustomButton
                iconName={'video'}
                iconHeight={'10'}
                iconWidth={'14.97'}
                title={t('schoolabyApp.journey.detail.viewLive')}
                buttonType={'primary'}
                size={'md'}
                href={journey?.videoConferenceUrl}
              />
            )}
          </div>
        </HeadingNew>
        <>
          <HomeBreadcrumbItem />
          <MyJourneysBreadcrumbItem />
          {!!journey && <JourneyTitleBreadcrumbItem journeyId={journey.id} journeyTitle={journey.title} />}
        </>
        {getTabContent()}
      </ContentContainer>
    </JourneyDetailContext.Provider>
  );
};

const mapStateToProps = ({ authentication }: IRootState) => ({
  account: authentication.account,
  isTeacherOrAdmin: hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.TEACHER, AUTHORITIES.ADMIN]),
});

type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(JourneyDetailNew);
