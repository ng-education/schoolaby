import React, { FC } from 'react';
import { IJourney } from 'app/shared/model/journey.model';
import { Label, Modal, ModalBody, ModalHeader } from 'reactstrap';
import { translate } from 'react-jhipster';

import './invite-modal.scss';
import { NavigationPath } from 'app/shared/util/navigation-utils';
import UrlInput, { ShareData } from 'app/shared/layout/url-input/url-input';
import { isMobile, isTablet } from 'react-device-detect';
import CloseButtonNew from 'app/shared/layout/close-button/close-button-new';
import { ROLE } from 'app/config/constants';

export type InviteModalType = typeof ROLE.STUDENT | typeof ROLE.TEACHER;

export interface InviteModalProps {
  modal: boolean;
  toggle: () => void;
  journey: IJourney;
  type: InviteModalType;
}

const InviteModal: FC<InviteModalProps> = ({ toggle, modal, journey, type }) => {
  const getLink = () => (type === 'STUDENT' ? journey?.studentSignupCode : journey?.teacherSignupCode);

  const url = `${location.protocol}//${location.host}${NavigationPath.JOIN_JOURNEY}?signUpCode=${getLink()}`;

  const checkIsContentShareable = (shareData: ShareData) => {
    // @ts-ignore
    return (isMobile || isTablet) && navigator.canShare && navigator.canShare(shareData);
  };

  const getShareDataWithUrl = (): ShareData => ({
    url,
  });

  const getShareDataWithCode = (): ShareData => ({ text: journey?.studentSignupCode });

  const ModalTitle = () =>
    type === 'STUDENT' ? translate('schoolabyApp.journey.invite.students') : translate('schoolabyApp.journey.invite.teachers');

  return (
    <Modal
      isOpen={modal}
      toggle={toggle}
      backdropClassName={'dark-backdrop'}
      contentClassName={'invite-students-modal-content border-0'}
      className={'invite-students-modal d-flex'}
    >
      <ModalHeader tag={'h3'} className={'border-0 p-0 mb-3'}>
        <ModalTitle />
        <CloseButtonNew onClick={toggle} className={'p-0'} />
      </ModalHeader>
      <ModalBody className={'p-0'}>
        <img
          src="content/images/journey/invite_students.svg"
          alt={translate('schoolabyApp.journey.illustrations.inviteStudentsIllustration')}
          className="illustration d-lg-none mb-3"
        />
        <div className={'d-flex flex-column align-items-center flex-lg-row align-items-lg-end justify-content-lg-between'}>
          <div className="url-component">
            <Label id="invite-code-label" for="journey-code-value" className={'m-0'}>
              {translate('schoolabyApp.journey.signUpCode')}
            </Label>
            <UrlInput
              buttonAction={checkIsContentShareable(getShareDataWithCode()) ? 'share' : 'copy'}
              id={'journey-code-value'}
              defaultValue={getLink()}
              sharingContent={getShareDataWithCode()}
            />
          </div>
          <span className={'separator-text px-3 mt-3 mt-lg-0'}>{translate('global.or')}</span>
          <div className="url-component">
            <Label id="sharable-link-label" for="sharable-link-value" className={'m-0'}>
              {translate('schoolabyApp.journey.sharableLink')}
            </Label>
            <UrlInput
              buttonAction={checkIsContentShareable(getShareDataWithUrl()) ? 'share' : 'copy'}
              id={'sharable-link-value'}
              defaultValue={url}
              sharingContent={getShareDataWithUrl()}
            />
          </div>
        </div>
      </ModalBody>
    </Modal>
  );
};

export default InviteModal;
