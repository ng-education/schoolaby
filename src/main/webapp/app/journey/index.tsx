import React from 'react';
import { Switch } from 'react-router-dom';
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';
import JourneyUpdateNew from './journey-update';
import JoinJourney from 'app/journey/journey-detail/join-journey';
import ErrorBoundaryUpdateEntityRoute from 'app/shared/error/error-boundary-update-entity-route';
import JourneyClass from 'app/journey/journey-detail/class/journey-class';
import ErrorBoundaryDetailEntityRoute from 'app/shared/error/error-boundary-detail-entity-route';
import JourneyWithTemplateProvider from 'app/journey/journey-with-template-provider';
import JourneyDetailNew from 'app/journey/journey-detail/journey-detail-new';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryUpdateEntityRoute exact path={`${match.url}/new`} component={JourneyUpdateNew} />
      <ErrorBoundaryUpdateEntityRoute exact path={`${match.url}/:id/edit`} component={JourneyUpdateNew} />
      <ErrorBoundaryRoute exact path={`${match.url}/join`} component={JoinJourney} />
      <ErrorBoundaryDetailEntityRoute exact path={`${match.url}/:id`} component={JourneyDetailNew} />
      <ErrorBoundaryUpdateEntityRoute exact path={`${match.url}/:id/students`} component={JourneyDetailNew} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/students/add`} component={JourneyClass} />
      <ErrorBoundaryUpdateEntityRoute exact path={`${match.url}/:id/apps`} component={JourneyDetailNew} />
      <ErrorBoundaryUpdateEntityRoute exact path={`${match.url}/:id/grades`} component={JourneyDetailNew} />
      <ErrorBoundaryDetailEntityRoute path={match.url} component={JourneyWithTemplateProvider} />
    </Switch>
  </>
);

export default Routes;
