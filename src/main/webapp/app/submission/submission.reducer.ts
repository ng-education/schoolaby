import axios from 'axios';
import { ICrudPutAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { FAILURE, REQUEST, SUCCESS } from 'app/shared/reducers/action-type.util';

import { defaultValue, ISubmission } from 'app/shared/model/submission/submission.model';

export const ACTION_TYPES = {
  FETCH_SUBMISSION: 'submission/FETCH_SUBMISSION',
  CREATE_SUBMISSION: 'submission/CREATE_SUBMISSION',
  UPDATE_SUBMISSION: 'submission/UPDATE_SUBMISSION',
  DELETE_SUBMISSION: 'submission/DELETE_SUBMISSION',
  RESET: 'submission/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<ISubmission>,
  entity: defaultValue,
  links: { next: 0 },
  updating: false,
  totalItems: 0,
  updateSuccess: false,
};

export type SubmissionState = Readonly<typeof initialState>;

// Reducer

export default (state: SubmissionState = initialState, action): SubmissionState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_SUBMISSION):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_SUBMISSION):
    case REQUEST(ACTION_TYPES.UPDATE_SUBMISSION):
    case REQUEST(ACTION_TYPES.DELETE_SUBMISSION):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_SUBMISSION):
    case FAILURE(ACTION_TYPES.CREATE_SUBMISSION):
    case FAILURE(ACTION_TYPES.UPDATE_SUBMISSION):
    case FAILURE(ACTION_TYPES.DELETE_SUBMISSION):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_SUBMISSION):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_SUBMISSION):
    case SUCCESS(ACTION_TYPES.UPDATE_SUBMISSION):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_SUBMISSION):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/submissions';

// Actions

export const createEntity: ICrudPutAction<ISubmission> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_SUBMISSION,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  return result;
};
