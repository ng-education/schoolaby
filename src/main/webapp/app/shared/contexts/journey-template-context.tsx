import React, { useState } from 'react';
import ISubject from 'app/shared/model/subject.model';

export const JourneyTemplateContext = React.createContext(undefined);

function JourneyTemplateProvider({ children }) {
  const [templateSubject, setTemplateSubject] = useState<ISubject>(undefined);
  const [showTemplatesView, setShowTemplatesView] = useState<boolean>(false);
  const [selectedTemplateId, setSelectedTemplateId] = useState<number>(undefined);

  return (
    <JourneyTemplateContext.Provider
      value={{
        templateSubject,
        setTemplateSubject,
        showTemplatesView,
        setShowTemplatesView,
        selectedTemplateId,
        setSelectedTemplateId,
      }}
    >
      {children}
    </JourneyTemplateContext.Provider>
  );
}

const useJourneyTemplateSubjectState = (): {
  templateSubject: ISubject;
  setTemplateSubject: React.Dispatch<React.SetStateAction<ISubject>>;
} => {
  const context = React.useContext(JourneyTemplateContext);
  if (context === undefined) {
    throw new Error('useTemplateSubjectState must be used within a JourneyTemplateProvider');
  }
  return {
    templateSubject: context.templateSubject,
    setTemplateSubject: context.setTemplateSubject,
  };
};

const useShowJourneyTemplatesViewState = (): {
  showTemplatesView: boolean;
  setShowTemplatesView: React.Dispatch<React.SetStateAction<boolean>>;
} => {
  const context = React.useContext(JourneyTemplateContext);
  if (context === undefined) {
    throw new Error('useShowTemplatesViewState must be used within a JourneyTemplateProvider');
  }
  return {
    showTemplatesView: context.showTemplatesView,
    setShowTemplatesView: context.setShowTemplatesView,
  };
};

const useSelectedJourneyTemplateState = (): {
  selectedTemplateId: number;
  setSelectedTemplateId: React.Dispatch<React.SetStateAction<number>>;
} => {
  const context = React.useContext(JourneyTemplateContext);
  if (context === undefined) {
    throw new Error('useSelectedJourneyTemplateState must be used within a JourneyTemplateProvider');
  }
  return {
    selectedTemplateId: context.selectedTemplateId,
    setSelectedTemplateId: context.setSelectedTemplateId,
  };
};

export { JourneyTemplateProvider, useJourneyTemplateSubjectState, useShowJourneyTemplatesViewState, useSelectedJourneyTemplateState };
