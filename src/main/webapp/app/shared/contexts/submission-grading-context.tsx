import React, { useState } from 'react';
import { ILtiResource } from 'app/shared/model/lti-resource.model';
import { IMessage } from 'app/shared/model/message.model';
import { ISubmission } from 'app/shared/model/submission/submission.model';
import { IUploadedFile } from 'app/shared/model/uploaded-file.model';
import { ISelectedCritreionLevel } from 'app/shared/model/rubric/selected-critreion-level.model';
import { ISubmissionFeedback } from 'app/shared/model/submission/submission-feedback.model';

const SubmissionGradingContext = React.createContext(undefined);

function SubmissionGradingProvider({ children }) {
  const [ltiResources, setLtiResources] = useState<ILtiResource[]>();
  const [commentFromEditor, setCommentFromEditor] = useState<string>();
  const [submissionFeedback, setSubmissionFeedback] = useState<IMessage>();
  const [modifiedSubmission, setModifiedSubmission] = useState<ISubmission>(undefined);
  const [feedbackFiles, setFeedbackFiles] = useState<IUploadedFile[]>([]);
  const [resubmittable, setIsResubmittable] = useState<boolean>(false);
  const [submittedSubmissions, setSubmittedSubmissions] = useState<ISubmission[]>();
  const [grade, setGrade] = useState<string>();
  const [selectedCriterionLevels, setSelectedCriterionLevels] = useState<ISelectedCritreionLevel[]>([]);
  const [selectedFeedback, setSelectedFeedback] = useState<ISubmissionFeedback[]>(undefined);

  return (
    <SubmissionGradingContext.Provider
      value={{
        ltiResources,
        setLtiResources,
        commentFromEditor,
        setCommentFromEditor,
        submissionFeedback,
        setSubmissionFeedback,
        modifiedSubmission,
        setModifiedSubmission,
        feedbackFiles,
        setFeedbackFiles,
        resubmittable,
        setIsResubmittable,
        submittedSubmissions,
        setSubmittedSubmissions,
        grade,
        setGrade,
        selectedCriterionLevels,
        setSelectedCriterionLevels,
        selectedFeedback,
        setSelectedFeedback,
      }}
    >
      {children}
    </SubmissionGradingContext.Provider>
  );
}

function useLtiResourcesState(): { ltiResources: ILtiResource[]; setLtiResources: React.Dispatch<React.SetStateAction<ILtiResource[]>> } {
  const context = React.useContext(SubmissionGradingContext);
  if (context === undefined) {
    throw new Error('useLtiResourcesState must be used within a SubmissionGradingContext');
  }
  return { ltiResources: context.ltiResources, setLtiResources: context.setLtiResources };
}

function useCommentFromEditorState(): { commentFromEditor: string; setCommentFromEditor: React.Dispatch<React.SetStateAction<string>> } {
  const context = React.useContext(SubmissionGradingContext);
  if (context === undefined) {
    throw new Error('useCommentFromEditorState must be used within a SubmissionGradingContext');
  }
  return { commentFromEditor: context.commentFromEditor, setCommentFromEditor: context.setCommentFromEditor };
}

function useSubmissionFeedbackState(): {
  submissionFeedback: IMessage;
  setSubmissionFeedback: React.Dispatch<React.SetStateAction<IMessage>>;
} {
  const context = React.useContext(SubmissionGradingContext);
  if (context === undefined) {
    throw new Error('useSubmissionFeedbackState must be used within a SubmissionGradingContext');
  }
  return { submissionFeedback: context.submissionFeedback, setSubmissionFeedback: context.setSubmissionFeedback };
}

function useModifiedSubmissionState(): {
  modifiedSubmission: ISubmission;
  setModifiedSubmission: React.Dispatch<React.SetStateAction<ISubmission>>;
} {
  const context = React.useContext(SubmissionGradingContext);
  if (context === undefined) {
    throw new Error('useModifiedSubmissionState must be used within a SubmissionGradingContext');
  }
  return { modifiedSubmission: context.modifiedSubmission, setModifiedSubmission: context.setModifiedSubmission };
}

function useFeedbackFilesState(): {
  feedbackFiles: IUploadedFile[];
  setFeedbackFiles: React.Dispatch<React.SetStateAction<IUploadedFile[]>>;
} {
  const context = React.useContext(SubmissionGradingContext);
  if (context === undefined) {
    throw new Error('useFeedbackFilesState must be used within a SubmissionGradingContext');
  }
  return { feedbackFiles: context.feedbackFiles, setFeedbackFiles: context.setFeedbackFiles };
}

function useResubmittableState(): { resubmittable: boolean; setIsResubmittable: React.Dispatch<React.SetStateAction<boolean>> } {
  const context = React.useContext(SubmissionGradingContext);
  if (context === undefined) {
    throw new Error('useResubmittableState must be used within a SubmissionGradingContext');
  }
  return { resubmittable: context.resubmittable, setIsResubmittable: context.setIsResubmittable };
}

function useSelectedCriterionLevelsState(): {
  selectedCriterionLevels: ISelectedCritreionLevel[];
  setSelectedCriterionLevels: React.Dispatch<React.SetStateAction<ISelectedCritreionLevel[]>>;
} {
  const context = React.useContext(SubmissionGradingContext);
  if (context === undefined) {
    throw new Error('useFeedbackFilesState must be used within a SubmissionGradingContext');
  }
  return {
    selectedCriterionLevels: context.selectedCriterionLevels,
    setSelectedCriterionLevels: context.setSelectedCriterionLevels,
  };
}

function useSubmittedSubmissionsState(): {
  submittedSubmissions: ISubmission[];
  setSubmittedSubmissions: React.Dispatch<React.SetStateAction<ISubmission[]>>;
} {
  const context = React.useContext(SubmissionGradingContext);
  if (context === undefined) {
    throw new Error('useSubmittedSubmissionsState must be used within a SubmissionGradingContext');
  }
  return {
    submittedSubmissions: context.submittedSubmissions,
    setSubmittedSubmissions: context.setSubmittedSubmissions,
  };
}

function useGradeState(): { grade: string; setGrade: React.Dispatch<React.SetStateAction<string>> } {
  const context = React.useContext(SubmissionGradingContext);
  if (context === undefined) {
    throw new Error('useGradeState must be used within a SubmissionGradingContext');
  }
  return { grade: context.grade, setGrade: context.setGrade };
}

function useSelectedFeedbackState(): {
  selectedFeedback: ISubmissionFeedback;
  setSelectedFeedback: React.Dispatch<React.SetStateAction<ISubmissionFeedback>>;
} {
  const context = React.useContext(SubmissionGradingContext);
  if (context === undefined) {
    throw new Error('useSelectedFeedbackState must be used within a SubmissionGradingContext');
  }
  return { selectedFeedback: context.selectedFeedback, setSelectedFeedback: context.setSelectedFeedback };
}

export {
  SubmissionGradingProvider,
  useLtiResourcesState,
  useCommentFromEditorState,
  useSubmissionFeedbackState,
  useModifiedSubmissionState,
  useFeedbackFilesState,
  useResubmittableState,
  useSubmittedSubmissionsState,
  useGradeState,
  useSelectedCriterionLevelsState,
  useSelectedFeedbackState,
};
