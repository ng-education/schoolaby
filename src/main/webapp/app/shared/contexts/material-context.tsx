import React, { useState } from 'react';
import { Material } from 'app/shared/model/material.model';
import sortBy from 'lodash/sortBy';

const MaterialContext = React.createContext(undefined);

const MaterialProvider = ({ children }) => {
  const [materials, setMaterials] = useState<Material[]>([]);
  const [chosenMaterial, setChosenMaterial] = useState<Material>(null);

  const _setMaterials = (mats: Material[]) => {
    const sequencedMaterials = mats.map((value, index) => {
      value.sequenceNumber = index;
      return value;
    });
    setMaterials(sortBy(sequencedMaterials, 'sequenceNumber'));
  };

  const findMaterialIndex = (material: Material): number => {
    return materials.findIndex(
      existing =>
        (existing.id !== undefined && existing.id === material.id && existing.type === material.type) ||
        existing.getKey() === material.getKey()
    );
  };

  const addMaterial = (material: Material, index?: number): void => {
    index = index !== undefined ? index : materials.length;
    if (!materials.find(existing => existing.getKey() === material.getKey())) {
      materials.splice(index, 0, material);
      _setMaterials([...materials]);
    }
  };

  const updateMaterial = (material: Material, index?: number): void => {
    index = index !== undefined ? index : findMaterialIndex(material);
    materials.splice(index, 1, material);
    _setMaterials([...materials]);
  };

  const removeMaterial = (material: Material, index?: number): void => {
    index = index !== undefined ? index : findMaterialIndex(material);
    if (index >= 0) {
      materials.splice(index, 1);
      _setMaterials([...materials]);
    }
  };

  return (
    <MaterialContext.Provider
      value={{
        setMaterials: _setMaterials,
        materials,
        addMaterial,
        updateMaterial,
        removeMaterial,
        chosenMaterial,
        setChosenMaterial,
      }}
    >
      {children}
    </MaterialContext.Provider>
  );
};

const useMaterialState = (): {
  materials: Material[];
  setMaterials: React.Dispatch<React.SetStateAction<Material[]>>;
  addMaterial: (Material, number?) => void;
  removeMaterial: (Material, number?) => void;
  updateMaterial: (Material, number?) => void;
} => {
  const context = React.useContext(MaterialContext);
  if (context === undefined) {
    throw new Error('useMaterialState must be used within a MaterialProvider');
  }
  return {
    materials: context.materials,
    setMaterials: context.setMaterials,
    addMaterial: context.addMaterial,
    removeMaterial: context.removeMaterial,
    updateMaterial: context.updateMaterial,
  };
};

const useChosenMaterialState = (): {
  chosenMaterial: Material;
  setChosenMaterial: React.Dispatch<React.SetStateAction<Material>>;
} => {
  const context = React.useContext(MaterialContext);
  if (context === undefined) {
    throw new Error('useChosenMaterialState must be used within a MaterialProvider');
  }
  return {
    chosenMaterial: context.chosenMaterial,
    setChosenMaterial: context.setChosenMaterial,
  };
};

export { useMaterialState, MaterialProvider, useChosenMaterialState };
