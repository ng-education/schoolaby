import React, { useState } from 'react';
import { CameraMountTarget } from 'app/shared/layout/webcams/video-webcam';
import Uppy from '@uppy/core';

const CameraProviderContext = React.createContext(undefined);

function CameraProvider({ children }) {
  const [uppy, setUppy] = useState(null);
  const [cameraMountTarget, setCameraMountTarget] = useState<CameraMountTarget>(undefined);
  const [capturing, setCapturing] = useState<boolean>(false);
  const [takePicture, setTakePicture] = useState<boolean>(false);
  const [cameraFacingMode, setCameraFacingMode] = useState<'user' | 'environment'>('user');
  const [mediaStream, setMediaStream] = useState<MediaStream>(null);
  const [cameraError, setCameraError] = useState<string>(null);

  return (
    <CameraProviderContext.Provider
      value={{
        capturing,
        setCapturing,
        takePicture,
        setTakePicture,
        cameraMountTarget,
        setCameraMountTarget,
        uppy,
        setUppy,
        cameraFacingMode,
        setCameraFacingMode,
        mediaStream,
        setMediaStream,
        cameraError,
        setCameraError,
      }}
    >
      {children}
    </CameraProviderContext.Provider>
  );
}

const useCapturingState = (): {
  capturing: boolean;
  setCapturing: React.Dispatch<React.SetStateAction<boolean>>;
} => {
  const context = React.useContext(CameraProviderContext);
  if (context === undefined) {
    throw new Error('useCapturingState must be used within a CameraProvider');
  }
  return {
    capturing: context.capturing,
    setCapturing(capturing: boolean) {
      return context.setCapturing(capturing);
    },
  };
};

const useTakePictureState = (): {
  takePicture: boolean;
  setTakePicture: React.Dispatch<React.SetStateAction<boolean>>;
} => {
  const context = React.useContext(CameraProviderContext);
  if (context === undefined) {
    throw new Error('usePictureState must be used within a CameraProvider');
  }
  return {
    takePicture: context.takePicture,
    setTakePicture(takePicture: boolean) {
      return context.setTakePicture(takePicture);
    },
  };
};

const useCameraMountTargetState = (): {
  cameraMountTarget: CameraMountTarget;
  setCameraMountTarget: React.Dispatch<React.SetStateAction<CameraMountTarget>>;
} => {
  const context = React.useContext(CameraProviderContext);
  if (context === undefined) {
    throw new Error('useCameraMountTargetState must be used within a CameraProvider');
  }
  return {
    cameraMountTarget: context.cameraMountTarget,
    setCameraMountTarget(mountTarget: CameraMountTarget) {
      return context.setCameraMountTarget(mountTarget);
    },
  };
};

const useUppyState = (): {
  uppy: Uppy.Uppy<Uppy.StrictTypes>;
  setUppy: React.Dispatch<React.SetStateAction<Uppy.Uppy<Uppy.StrictTypes>>>;
} => {
  const context = React.useContext(CameraProviderContext);
  if (context === undefined) {
    throw new Error('useUppyState must be used within a CameraProvider');
  }
  return {
    uppy: context.uppy,
    setUppy(uppy: any) {
      return context.setUppy(uppy);
    },
  };
};

const useCameraFacingModeState = (): {
  cameraFacingMode: 'user' | 'environment';
  toggleCameraFacingMode: () => void;
} => {
  const context = React.useContext(CameraProviderContext);
  if (context === undefined) {
    throw new Error('useCameraFacingModeState must be used within a CameraProvider');
  }
  return {
    cameraFacingMode: context.cameraFacingMode,
    toggleCameraFacingMode() {
      context.setCameraFacingMode(context.cameraFacingMode === 'user' ? 'environment' : 'user');
    },
  };
};

const useMediaStreamState = (): {
  mediaStream: MediaStream;
  setMediaStream: React.Dispatch<React.SetStateAction<MediaStream>>;
} => {
  const context = React.useContext(CameraProviderContext);
  if (context === undefined) {
    throw new Error('useMediaStreamState must be used within a CameraProvider');
  }

  return {
    mediaStream: context.mediaStream,
    setMediaStream: context.setMediaStream,
  };
};

const useCameraErrorState = (): {
  cameraError: string;
  setCameraError: React.Dispatch<React.SetStateAction<string>>;
} => {
  const context = React.useContext(CameraProviderContext);
  if (context === undefined) {
    throw new Error('useCameraErrorState must be used within a CameraProvider');
  }

  return {
    cameraError: context.cameraError,
    setCameraError: context.setCameraError,
  };
};

export {
  CameraProvider,
  useCapturingState,
  useTakePictureState,
  useCameraMountTargetState,
  useUppyState,
  useCameraFacingModeState,
  useMediaStreamState,
  useCameraErrorState,
};
