import React, { useState } from 'react';

const UserNotificationContext = React.createContext(undefined);

const UserNotificationProvider = ({ children }) => {
  const [dropdownOpen, setDropdownOpen] = useState<boolean>(false);
  const [journeyId, setJourneyId] = useState<number>(null);

  return (
    <UserNotificationContext.Provider
      value={{
        dropdownOpen,
        setDropdownOpen,
        journeyId,
        setJourneyId,
      }}
    >
      {children}
    </UserNotificationContext.Provider>
  );
};

const useUserNotificationDropdownState = (): {
  dropdownOpen: boolean;
  setDropdownOpen: React.Dispatch<React.SetStateAction<boolean>>;
  journeyId: number;
  setJourneyId: React.Dispatch<React.SetStateAction<number>>;
  toggleDropdown: () => void;
} => {
  const context = React.useContext(UserNotificationContext);

  if (context === undefined) {
    throw new Error('useUserNotificationDropdownState must be used within a UserNotificationProvider');
  }

  return {
    dropdownOpen: context.dropdownOpen,
    setDropdownOpen: context.setDropdownOpen,
    journeyId: context.journeyId,
    setJourneyId: context.setJourneyId,
    toggleDropdown() {
      context.setDropdownOpen(!context.dropdownOpen);
    },
  };
};

export { UserNotificationProvider, useUserNotificationDropdownState };
