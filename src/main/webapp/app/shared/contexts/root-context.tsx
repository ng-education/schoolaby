import React, { CSSProperties, useState } from 'react';

export const RootContext = React.createContext(undefined);

function RootProvider({ children }) {
  const [teacherCreatingFeedback, setTeacherCreatingFeedback] = useState<boolean>(false);
  const [styles, setStyles] = useState<CSSProperties>({});

  const initializeStyles = () => {
    setStyles({});
  };

  return (
    <RootContext.Provider
      value={{
        teacherCreatingFeedback,
        setTeacherCreatingFeedback,
        styles,
        setStyles,
        initializeStyles,
      }}
    >
      {children}
    </RootContext.Provider>
  );
}

const useTeacherCreatingFeedbackState = (): {
  teacherCreatingFeedback: boolean;
  setTeacherCreatingFeedback: React.Dispatch<React.SetStateAction<boolean>>;
} => {
  const context = React.useContext(RootContext);
  if (context === undefined) {
    throw new Error('useTeacherCreatingFeedbackState must be used within a RootProvider');
  }
  return {
    teacherCreatingFeedback: context.teacherCreatingFeedback,
    setTeacherCreatingFeedback: context.setTeacherCreatingFeedback,
  };
};

const useStylesState = (): {
  styles: CSSProperties;
  setStyles: React.Dispatch<React.SetStateAction<CSSProperties>>;
  initializeStyles: () => void;
} => {
  const context = React.useContext(RootContext);
  if (context === undefined) {
    throw new Error('useBackgroundState must be used within a RootProvider');
  }
  return {
    styles: context.styles,
    setStyles: context.setStyles,
    initializeStyles: context.initializeStyles,
  };
};

export { RootProvider, useTeacherCreatingFeedbackState, useStylesState };
