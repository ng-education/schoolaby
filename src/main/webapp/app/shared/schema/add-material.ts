import { useMemo } from 'react';
import { translate as t } from 'react-jhipster';
import * as yup from 'yup';
import { CAMERA, FILE, SaveMaterialModalType } from 'app/shared/layout/save-material-modal-new/save-material-modal-new';

export const useAddMaterialSchema = (locale: string, type?: SaveMaterialModalType) => {
  return useMemo(
    () =>
      yup.object().shape({
        title: yup.string().required(t('entity.validation.required')),
        description: yup.mixed().notRequired(),
        url: yup
          .string()
          .required(
            type === CAMERA
              ? t('schoolabyApp.marketplace.modal.errorMessage.camera')
              : type === FILE
              ? t('schoolabyApp.marketplace.modal.errorMessage.file')
              : t('entity.validation.required')
          ),
      }),
    [locale, type]
  );
};
