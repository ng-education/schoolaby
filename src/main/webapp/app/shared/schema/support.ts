import { useMemo } from 'react';
import * as yup from 'yup';
import { translate as t } from 'react-jhipster';

export const useSupportSchema = (locale: string) =>
  useMemo(
    () =>
      yup.object().shape({
        subject: yup.string().required(t('entity.validation.required')),
        content: yup.string().required(t('entity.validation.required')),
      }),
    [locale]
  );
