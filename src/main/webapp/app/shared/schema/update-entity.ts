import { useMemo } from 'react';
import { translate as t } from 'react-jhipster';
import * as yup from 'yup';
import { useSelectedEducationalAlignmentsState, useSelectedSubjectState } from 'app/shared/contexts/entity-update-context';
import { useJourneyTemplateSubjectState } from 'app/shared/contexts/journey-template-context';
import { AnySchema } from 'yup';

export const useUpdateMilestoneEntitySchema = (locale: string, nationalCurriculum: boolean, shouldValidateEducationalAlignments = true) => {
  const { selectedEducationalAlignments } = useSelectedEducationalAlignmentsState();
  return useMemo(
    () =>
      yup.object().shape({
        title: yup.string().required(t('entity.validation.required')),
        description: yup.string().nullable().notRequired(),
        endDate: yup.date().required(t('entity.validation.required')),
        educationalAlignments: yup
          .mixed()
          .test('validate-selected-educational-alignments', t('schoolabyApp.educationalAlignment.errors.inputSelector'), function test() {
            return nationalCurriculum && shouldValidateEducationalAlignments ? !!selectedEducationalAlignments.length : true;
          }),
      }),
    [locale, selectedEducationalAlignments, nationalCurriculum]
  );
};

export const useUpdateAssignmentEntitySchema = (
  locale: string,
  nationalCurriculum: boolean,
  shouldValidateStudents = true,
  shouldValidateEducationalAlignments = true
) => {
  const { selectedEducationalAlignments } = useSelectedEducationalAlignmentsState();
  return useMemo(
    () =>
      yup.object().shape({
        title: yup.string().required(t('entity.validation.required')),
        description: yup.string().nullable().notRequired(),
        gradingSchemeId: yup.number().min(1, t('entity.validation.required')),
        deadline: yup.date().required(t('entity.validation.required')),
        students: yup.array().min(shouldValidateStudents ? 1 : 0, t('schoolabyApp.assignment.update.atLeastOneStudentRequired')),
        educationalAlignments: yup
          .mixed()
          .test('validate-selected-educational-alignments', t('schoolabyApp.educationalAlignment.errors.inputSelector'), function test() {
            return nationalCurriculum && shouldValidateEducationalAlignments ? !!selectedEducationalAlignments.length : true;
          }),
      }),
    [locale, selectedEducationalAlignments, nationalCurriculum, shouldValidateStudents]
  );
};

export const useUpdateJourneyEntitySchema = (locale: string) => {
  const { selectedSubject } = useSelectedSubjectState();
  return useMemo(
    () =>
      yup.object().shape({
        title: yup.string().required(t('entity.validation.required')),
        videoConferenceUrl: yup.string().nullable().notRequired(),
        startDate: yup.date().required(t('entity.validation.required')),
        description: yup.string().nullable().notRequired(),
        educationalLevelId: yup.string().required(t('entity.validation.required')),
        endDate: yup
          .date()
          .min(yup.ref('startDate'), t('schoolabyApp.journey.endDateGreaterThanStart'))
          .required(t('entity.validation.required')),
        schoolIds: yup.array().min(1, t('schoolabyApp.journey.educationalInstitution.required')),
        subject: yup.mixed().when('curriculum.requiresSubject', {
          is: true,
          then: yup.mixed().test('validate-selected-subject', t('schoolabyApp.journey.subject.required'), function test() {
            return !!selectedSubject;
          }),
        }),
      }),
    [locale, selectedSubject]
  );
};

export const useJoinJourneySchema = (locale: string): AnySchema =>
  useMemo(
    () =>
      yup.object().shape({
        signUpCodeValue: yup.string().required(t('entity.validation.required')),
      }),
    [locale]
  );

export const useNewJourneyOptionSchema = (locale: string): AnySchema =>
  useMemo(
    () =>
      yup.object().shape({
        journeyTitle: yup.string().required(t('entity.validation.required')),
      }),
    [locale]
  );

export const useNewJourneyFromTemplateOptionSchema = (locale: string): AnySchema => {
  const { templateSubject } = useJourneyTemplateSubjectState();
  return useMemo(
    () =>
      yup.object().shape({
        subject: yup.mixed().test('validate-journey-template-subject', t('schoolabyApp.journey.subject.required'), function test() {
          return !!templateSubject;
        }),
      }),
    [locale, templateSubject]
  );
};

export const useAssignmentRubricSchema = (locale: string) =>
  useMemo(
    () =>
      yup.object().shape({
        title: yup.string().required(t('schoolabyApp.assignment.rubric.errors.title.required')),
        criterions: yup.array().of(
          yup.object().shape({
            title: yup.string().required(t('schoolabyApp.assignment.rubric.errors.criterion.title.required')),
            description: yup.string().required(t('schoolabyApp.assignment.rubric.errors.criterion.description.required')),
            levels: yup.array().of(
              yup.object().shape({
                title: yup.string().required(t('schoolabyApp.assignment.rubric.errors.criterion.level.title.required')),
                points: yup.number().required(t('schoolabyApp.assignment.rubric.errors.criterion.level.points.required')),
                description: yup.string().required(t('schoolabyApp.assignment.rubric.errors.criterion.level.description.required')),
              })
            ),
          })
        ),
      }),
    [locale]
  );

export const useFeedbackRubricSchema = (locale: string) =>
  useMemo(
    () =>
      yup.object().shape({
        criterions: yup.array().of(
          yup.object().shape({
            selected: yup.string().required(t('schoolabyApp.submission.rubricSelectionRequired')),
            levels: yup.array().of(
              yup.object().shape({
                description: yup.string().required(t('schoolabyApp.assignment.rubric.errors.criterion.level.description.required')),
              })
            ),
          })
        ),
      }),
    [locale]
  );
