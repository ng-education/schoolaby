import { useMemo } from 'react';
import * as yup from 'yup';
import { translate as t } from 'react-jhipster';

export const useLoginSchema = (locale: string) =>
  useMemo(
    () =>
      yup.object().shape<any>({
        username: yup
          .string()
          .required(t('register.messages.validate.login.required'))
          .min(1, t('register.messages.validate.login.minlength'))
          .max(254, t('register.messages.validate.login.maxlength')),
        password: yup
          .string()
          .min(4, t('global.messages.validate.newpassword.minlength'))
          .max(50, t('global.messages.validate.newpassword.maxlength'))
          .required(t('global.messages.validate.newpassword.required')),
      }),
    [locale]
  );
