import React, { FC } from 'react';
import { IUploadedFile } from 'app/shared/model/uploaded-file.model';
import CloseButton from 'app/shared/layout/close-button/close-button';
import Icon, { getFileExtensionIcon } from 'app/shared/icons';
import { createDownloadLink, isPreviewableFile } from 'app/shared/util/file-utils';
import { usePreviewFileState } from 'app/shared/contexts/uploaded-file-context';
import { downloadUploadedFile } from 'app/shared/services/uploaded-file-api';

interface IBlockListFileItemProps {
  uploadedFile: IUploadedFile;
  onDelete?: () => void;
  deletable: boolean;
  deleteUploadedFile?: (uploadedFile: IUploadedFile) => void;
  fileIndex?: number;
}

const BlockListFileItem: FC<IBlockListFileItemProps> = ({ uploadedFile, onDelete, deletable, deleteUploadedFile }) => {
  // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
  // @ts-ignore
  const { setPreviewFile } = usePreviewFileState();
  const originalFileName = uploadedFile?.originalName;

  const PreviewButton = () => (
    <span className={'preview-button'} onClick={() => setPreviewFile(uploadedFile)}>
      <Icon name={'magnifyingGlass'} />
    </span>
  );

  const DownloadButton = () => {
    const downloadFile = () =>
      downloadUploadedFile(uploadedFile?.uniqueName).then(response => {
        const link = createDownloadLink(response);
        link.download = originalFileName;
        link.click();
      });

    return (
      <span className={'download-button'} onClick={downloadFile}>
        <Icon name={'download'} />
      </span>
    );
  };

  const DeleteButton = () => (
    <CloseButton
      className="delete-upload d-flex"
      onClick={() => {
        if (deleteUploadedFile) {
          deleteUploadedFile(uploadedFile);
        } else if (onDelete) {
          onDelete();
        }
      }}
      role={'delete-file'}
    />
  );

  return (
    <li role="listitem" className={'block-list-file-item d-flex flex-row flex-grow-1'}>
      <div className={`title d-flex flex-row flex-grow-1 align-items-center ${!deleteUploadedFile ? 'full' : ''}`}>
        {getFileExtensionIcon(uploadedFile.extension)}
        <span>{originalFileName}</span>
        <div className={'file-actions d-flex justify-content-end align-items-center ml-auto'}>
          {isPreviewableFile(uploadedFile) && <PreviewButton />}
          <DownloadButton />
          {(deleteUploadedFile || onDelete) && deletable && <DeleteButton />}
        </div>
      </div>
    </li>
  );
};

export default BlockListFileItem;
