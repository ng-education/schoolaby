import './picture-upload-button-new.scss';
import '@uppy/core/dist/style.css';
import '@uppy/file-input/dist/style.css';
import XHRUpload from '@uppy/xhr-upload';
import React, { FC, memo, useEffect, useRef } from 'react';
import { getAuthToken } from '../util/auth-utils';
import { connect } from 'react-redux';
import { IRootState } from 'app/shared/reducers';
import Compressor from 'app/shared/form/compressor';
import Informer from '@uppy/informer';
import { getFullFileUrl } from 'app/shared/services/uploaded-file-api';
import Icon from '../icons';
import { Uppy, UppyFile } from '@uppy/core';
import { toast } from 'react-toastify';
import { translate } from 'react-jhipster';

interface IProps extends StateProps {
  onUpload: (file: string) => void;
  existingImage: string;
  uploadDisabled: boolean;
  label: string;
  resetOn?: any;
}

const PictureUploadButtonNew: FC<IProps> = ({ uploadDisabled, existingImage, onUpload, resetOn, label }) => {
  const INPUT_ID = 'FileInput';
  const inputRef = useRef(null);

  const initializeUppy = () =>
    new Uppy({
      autoProceed: true,
      debug: process.env.NODE_ENV === 'development',
      restrictions: {
        maxFileSize: null,
        maxNumberOfFiles: 1,
        minNumberOfFiles: 0,
        allowedFileTypes: ['image/jpeg', 'image/png', 'image/gif'],
      },
    })
      .use(XHRUpload, {
        fieldName: 'file',
        endpoint: `/api/files`,
        headers: { Authorization: `Bearer ${getAuthToken()}` },
        limit: 1,
      })
      // @ts-ignore
      .use(Compressor, {
        quality: 0.9,
        maxWidth: 2500,
        maxHeight: 2500,
        convertSize: 3000000,
      })
      .use(Informer, {})
      .on('upload-success', (file: UppyFile, response: any) => {
        onUpload(getFullFileUrl(response.body.uniqueName));
      });

  const uppy = initializeUppy();

  const handleFileChange = event => {
    const files = Array.from(event.target.files);

    files.forEach((file: File) => {
      try {
        uppy.addFile({
          source: 'file input',
          name: file.name,
          type: file.type,
          data: file,
        });
      } catch (err) {
        if (err.isRestriction) {
          toast.error(translate('schoolabyApp.uploadedFile.error.fileType'));
        } else {
          console.error('Unknown error: ', err);
        }
      }
    });
  };

  useEffect(() => {
    inputRef.current.value = null;
  }, [resetOn]);

  useEffect(() => {
    return () => {
      if (inputRef.current) {
        inputRef.current.value = null;
      }
    };
  }, []);

  const UploadButton = () => (
    <div className="upload-button position-absolute">
      <label htmlFor={INPUT_ID} className="btn bg-white px-3 d-flex align-items-center">
        <Icon name="cameraBlack" width="18" height="16" className="mr-2" /> {label}
      </label>
      <input ref={inputRef} id={INPUT_ID} name="files" type="file" onChange={handleFileChange} />
    </div>
  );

  return (
    <div className="material-picture-new position-relative bg-white">
      <div className="upload-container">{!uploadDisabled && <UploadButton />}</div>
      {!!existingImage && <img src={existingImage} alt="uploaded-image" className="w-100" />}
    </div>
  );
};

const mapStateToProps = ({ locale }: IRootState) => ({
  locale,
});
type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(memo(PictureUploadButtonNew));
