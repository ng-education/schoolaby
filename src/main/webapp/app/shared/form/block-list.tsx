import React, { FC } from 'react';

interface IBlockRowProps {
  className?: string;
}

const BlockList: FC<IBlockRowProps> = ({ className, children }) => (
  <ul role="list" className={`block-list ${className || ''}`}>
    {children}
  </ul>
);

export default BlockList;
