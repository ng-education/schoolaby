import React, { useState } from 'react';

const FileUploadProviderContext = React.createContext(undefined);

const FileUploadProvider = ({ children }) => {
  const [fileName, setFileName] = useState<string>(null);
  const [maxFiles, setMaxFiles] = useState<number>(null);

  return (
    <FileUploadProviderContext.Provider
      value={{
        fileName,
        setFileName,
        maxFiles,
        setMaxFiles,
      }}
    >
      {children}
    </FileUploadProviderContext.Provider>
  );
};
const useFileNameState = (): {
  fileName: string;
  setFileName: React.Dispatch<React.SetStateAction<string>>;
} => {
  const context = React.useContext(FileUploadProviderContext);
  if (context === undefined) {
    throw new Error('useFileNameState must be used within a FileUploadProvider');
  }

  return {
    fileName: context.fileName,
    setFileName: context.setFileName,
  };
};

const useMaxFilesState = (): {
  maxFiles: number;
  setMaxFiles: React.Dispatch<React.SetStateAction<number>>;
} => {
  const context = React.useContext(FileUploadProviderContext);
  if (context === undefined) {
    throw new Error('useMaxFilesState must be used within a FileUploadProvider');
  }

  return {
    maxFiles: context.maxFiles,
    setMaxFiles: context.setMaxFiles,
  };
};

export { FileUploadProvider, useFileNameState, useMaxFilesState };
