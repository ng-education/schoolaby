import React, { FC } from 'react';
import { Field } from 'react-final-form';

const Condition: FC<{ when: string; is: any }> = ({ when, is, children }) => (
  <Field name={when} subscription={{ value: true }}>
    {({ input: { value } }) => (value === is ? children : null)}
  </Field>
);

export default Condition;
