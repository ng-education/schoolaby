import React, { FC, memo, useEffect } from 'react';
import BaseDatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { FieldRenderProps } from 'react-final-form';
import { FormFeedback, FormGroup, Input, Label } from 'reactstrap';
import { getStartOfDay } from 'app/shared/util/date-utils';
import { DateTime } from 'luxon';
import { translate } from 'react-jhipster';
import { IRootState } from 'app/shared/reducers';
import { connect } from 'react-redux';

export const DATEPICKER_DATE_FORMAT = 'dd/MM/yyyy';
export const DATEPICKER_DATE_TIME_FORMAT = 'dd/MM/yyyy HH:mm';

export type DatepickerDateTimeFormat = typeof DATEPICKER_DATE_FORMAT | typeof DATEPICKER_DATE_TIME_FORMAT;

interface IProps extends FieldRenderProps<any, any>, StateProps {
  label?: string;
  endOfDay?: boolean;
  dateTimeFormat?: DatepickerDateTimeFormat;
  showInputField?: boolean;
  noUndefined?: boolean;
}

const DatePicker: FC<IProps> = React.forwardRef(
  (
    {
      input,
      meta,
      label,
      endOfDay: fromEnd,
      dateTimeFormat = DATEPICKER_DATE_FORMAT,
      showInputField = true,
      id,
      locale,
      ...otherReactDatePickerProps
    },
    ref?: any
  ) => {
    const inputId = id ? id : `field-${input.name}`;
    const formatDate = (date: DateTime) => {
      if (!date) {
        return;
      }

      if (fromEnd) {
        return date.endOf('day');
      }

      return date;
    };

    const value = formatDate(
      input.value ? DateTime.fromISO(input.value).setZone('local', { keepLocalTime: false }) : DateTime.fromISO(getStartOfDay())
    ).toJSDate();
    const errorMessage = meta.touched && meta.error;
    const customInput = <Input id={id} value={value} invalid={!!errorMessage} type={showInputField ? 'text' : 'hidden'} />;

    const changeValueDate = (date: Date | null): void =>
      input.onChange(date ? formatDate(DateTime.fromJSDate(date).setZone('local', { keepLocalTime: true })).toString() : undefined);

    useEffect(() => {
      if (input.value === false) {
        changeValueDate(new Date(getStartOfDay()));
      }
    }, [input.value]);

    return (
      <FormGroup>
        {label && <Label for={inputId}>{label}</Label>}
        <BaseDatePicker
          id={inputId}
          ref={ref}
          dateFormat={dateTimeFormat}
          customInput={customInput}
          showTimeSelect={dateTimeFormat === DATEPICKER_DATE_TIME_FORMAT}
          timeIntervals={30}
          timeCaption={translate('tracker.table.time')}
          onFocus={input.onFocus}
          onBlur={input.onBlur}
          selected={value}
          locale={locale}
          {...otherReactDatePickerProps}
          onChange={changeValueDate}
        />
        {errorMessage && <FormFeedback>{errorMessage}</FormFeedback>}
      </FormGroup>
    );
  }
);

const mapStateToProps = ({ locale }: IRootState) => ({
  locale: locale.currentLocale,
});

type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(memo(DatePicker));
