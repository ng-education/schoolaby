import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { FC, useMemo } from 'react';
import { FieldRenderProps } from 'react-final-form';
import { FormFeedback, FormGroup, Input as BaseInput, Label, UncontrolledTooltip } from 'reactstrap';
import { InputType } from 'reactstrap/lib/Input';
import Icon, { IconName } from 'app/shared/icons';
import { DANGER } from 'app/shared/util/color-utils';

interface IProps extends FieldRenderProps<any, any> {
  type?: InputType;
  label?: string;
  labelClassName?: string;
  labelAfterInput?: boolean;
  tooltip?: string;
  containerClassName?: string;
  iconName?: IconName;
}

const Input: FC<IProps> = ({
  input,
  meta,
  label,
  labelClassName,
  labelAfterInput,
  tooltip,
  id,
  invalid,
  errMessage,
  containerClassName,
  iconName,
  ...props
}) => {
  const type = (input.type ?? 'text') as InputType;
  const errorMessage = errMessage || (meta.touched && meta.error);
  const isInvalid = !!errorMessage || invalid;

  const LabelComponent = () => (
    <Label hidden={props.hidden} className={labelClassName} for={id}>
      {label}
    </Label>
  );

  const memoizedLabelAfterInput = () => useMemo(() => label && labelAfterInput && <LabelComponent />, [label, labelAfterInput]);

  return (
    <FormGroup className={containerClassName}>
      {label && !labelAfterInput && <LabelComponent />}
      {tooltip && (
        <span id={`${id}-tooltip`} className="ml-2">
          <FontAwesomeIcon icon={'circle'} />
          <UncontrolledTooltip target={`${id}-tooltip`}>{tooltip}</UncontrolledTooltip>
        </span>
      )}
      {iconName ? (
        <div className={'position-relative'}>
          <BaseInput {...input} {...props} type={type} id={id} invalid={isInvalid} />
          <Icon name={iconName} fill={isInvalid && DANGER} className="position-absolute center-vertically mr-2" />
        </div>
      ) : (
        <BaseInput {...input} {...props} type={type} id={id} invalid={isInvalid} />
      )}
      {memoizedLabelAfterInput()}
      {errorMessage && <FormFeedback>{errorMessage}</FormFeedback>}
    </FormGroup>
  );
};

export default Input;
