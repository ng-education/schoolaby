// @ts-nocheck
import ImageCompressor from 'uppy-plugin-image-compressor';

export default class Compressor extends ImageCompressor {
  constructor(uppy, opts) {
    super(uppy, opts);
  }

  prepareUpload(fileIDs) {
    const promises = fileIDs.map(fileID => {
      const file = this.uppy.getFile(fileID);
      this.uppy.emit('preprocess-progress', file, {
        mode: 'indeterminate',
        message: this.i18n('compressingImages'),
      });

      if (file.type.split('/')[0] !== 'image' || file.type.split('/')[1] === 'gif') {
        return;
      }

      return this.compress(file.data)
        .then(compressedBlob => {
          this.uppy.log(`[Image Compressor] Image ${file.id} size before/after compression: ${file.data.size} / ${compressedBlob.size}`);
          this.uppy.setFileState(fileID, { data: compressedBlob });
        })
        .catch(err => {
          this.uppy.log(`[Image Compressor] Failed to compress ${file.id}:`, 'warning');
          this.uppy.log(err, 'warning');
        });
    });

    const emitPreprocessCompleteForAll = () => {
      fileIDs.forEach(fileID => {
        const file = this.uppy.getFile(fileID);
        this.uppy.emit('preprocess-complete', file);
      });
    };

    return Promise.all(promises).then(emitPreprocessCompleteForAll);
  }
}
