import './editable-text.scss';
import React, { FC, useState } from 'react';
import FooterButton from 'app/shared/layout/buttons/footer-button';
import { Field, Form } from 'react-final-form';
import Input from './components/input';
import { UseBaseMutationResult } from 'react-query';
import { translate } from 'react-jhipster';
import { MarkdownEditorInput } from '../../shared/form';
import DatePickerNew, { DatePickerProps } from 'app/shared/layout/date-picker-new/date-picker-new';
import { convertDateTimeToServer } from 'app/shared/util/date-utils';

type Editor = 'text' | 'markdown' | 'datepicker';

interface IEditableField {
  name: string;
  patchRequest: UseBaseMutationResult;
  initialValue: string;
  onChange?: (value: any) => void;
  validate?: (values: any) => any;
  setEditing?: (editing: boolean) => void;
  editor?: Editor;
  footerClassName?: string;
  editableFieldClassName?: string;
  datePickerProps?: DatePickerProps;
  isAllowedToModify?: boolean;
}

const EditableText: FC<IEditableField> = ({
  name,
  patchRequest,
  children,
  initialValue,
  validate,
  setEditing,
  editor = 'text',
  footerClassName = '',
  editableFieldClassName = '',
  datePickerProps = {},
  isAllowedToModify,
}) => {
  const [editingMode, setEditingMode] = useState<boolean>(false);

  const enterEditingMode = () => {
    setEditingMode(true);
    setEditing && setEditing(true);
  };

  const cancelEditingMode = values => {
    setEditingMode(false);
    setEditing && setEditing(false);
    values[name] = initialValue;
  };

  const patch = values => {
    if (values[name] === null) {
      values[name] = '';
    }
    if (editor === 'datepicker') {
      values[name] = convertDateTimeToServer(values[name]);
    }
    patchRequest.mutateAsync(values);
    setEditingMode(false);
    setEditing && setEditing(false);
  };

  const getEditorComponent = () => {
    switch (editor) {
      case 'markdown':
        return MarkdownEditorInput;
      case 'datepicker':
        return DatePickerNew;
      default:
        return Input;
    }
  };

  const EditField = (): JSX.Element => (
    <Field autoFocus name={name} type={editor === 'datepicker' ? 'date' : 'text'} {...datePickerProps} component={getEditorComponent()} />
  );

  const EditableContent = () => (
    <Form onSubmit={patch} validate={validate} initialValues={{ [name]: initialValue }}>
      {({ handleSubmit, values }) => (
        <form onSubmit={handleSubmit}>
          <div
            className={`${isAllowedToModify ? 'editable-field' : ''} editing ${editableFieldClassName}`}
            onClick={isAllowedToModify ? enterEditingMode : undefined}
          >
            {editingMode ? <EditField /> : <span className={'content d-block'}>{children}</span>}
          </div>
          {editingMode && (
            <div className={`footer mt-2 float-right ${footerClassName}`}>
              <FooterButton type={'cancel'} onClick={() => cancelEditingMode(values)} label={translate('entity.action.cancel')} />
              <FooterButton className={'ml-2'} type={'confirm'} label={translate('entity.action.save')} />
            </div>
          )}
        </form>
      )}
    </Form>
  );

  const TextContent = () => (
    <div className={`${isAllowedToModify ? 'editable-field' : ''}`} onClick={isAllowedToModify ? enterEditingMode : undefined}>
      <span className={'content d-block'}>{children}</span>
    </div>
  );

  return editingMode ? <EditableContent /> : <TextContent />;
};

export default EditableText;
