import './file-upload.scss';
import Uppy, { UppyFile } from '@uppy/core';
import '@uppy/core/dist/style.css';
import '@uppy/dashboard/dist/style.css';
import XHRUpload from '@uppy/xhr-upload';
import React, { FC, memo, useEffect, useRef } from 'react';
import { translate } from 'react-jhipster';
import { getAuthToken } from '../util/auth-utils';
import { IUploadedFile } from 'app/shared/model/uploaded-file.model';
import { connect } from 'react-redux';
import { IRootState } from 'app/shared/reducers';
import Compressor from 'app/shared/form/compressor';
import { useFileNameState, useMaxFilesState } from 'app/shared/form/file-upload-context';
import { CAMERA, FILE } from '../layout/save-material-modal-new/save-material-modal-new';
import FileDropzoneNew from 'app/shared/form/file-dropzone-new';
import UploadedFilesListNew from 'app/shared/form/uploaded-files-list-new';
import { UploadedFileProvider } from 'app/shared/contexts/uploaded-file-context';

interface IFileUpload extends StateProps {
  onUpload: (file: IUploadedFile) => void;
  onDelete: (file: IUploadedFile) => void;
  existingFiles: IUploadedFile[];
  uploadDisabled: boolean;
  maxFiles?: number;
  resetOn?: any;
  videoFileNamePrefix?: string;
  fileUploadType?: typeof FILE | typeof CAMERA;
}

const FileUploadNew: FC<IFileUpload> = ({
  locale,
  uploadDisabled,
  existingFiles,
  onUpload,
  onDelete,
  resetOn,
  maxFiles = 10,
  videoFileNamePrefix,
  fileUploadType,
}) => {
  const uploadedFilesRef = useRef(existingFiles);
  const { setFileName } = useFileNameState();
  const { setMaxFiles } = useMaxFilesState();

  useEffect(() => {
    if (videoFileNamePrefix?.length) {
      const prefix = videoFileNamePrefix.replace(/\s/g, '-').toLowerCase();
      setFileName(prefix);
    }
  }, [videoFileNamePrefix]);

  useEffect(() => {
    setMaxFiles(maxFiles);
  }, [maxFiles]);

  const uppyRef = useRef<Uppy.Uppy<Uppy.StrictTypes>>(
    Uppy({
      autoProceed: true,
      debug: process.env.NODE_ENV === 'development',
      restrictions: {
        maxFileSize: null,
        maxNumberOfFiles: maxFiles,
        minNumberOfFiles: 0,
        allowedFileTypes: null,
      },
      onBeforeFileAdded(newFile: UppyFile) {
        if (uploadedFilesRef.current?.find(existingFile => existingFile.originalName === newFile.name)) {
          uppyRef.current.info(translate('schoolabyApp.uploadedFile.error.nameConflict'));
          return false;
        }
        return true;
      },
    })
      .use(XHRUpload, {
        fieldName: 'file',
        endpoint: `/api/files`,
        headers: { Authorization: `Bearer ${getAuthToken()}` },
        limit: maxFiles,
      })
      // @ts-ignore
      .use(Compressor, {
        quality: 0.9,
        maxWidth: 2500,
        maxHeight: 2500,
        convertSize: 3000000,
      })
      .on('upload-success', (file: UppyFile, response: any) => {
        onUpload(response.body);
      })
  );

  useEffect(() => {
    uppyRef.current?.reset();
  }, [resetOn]);

  // Surround codecs with double quotes as Spring framework does not
  // support unquoted MediaType attributes (WebCam bug in Chrome)
  uppyRef?.current.on('file-added', file => {
    const codecsRegex = /(codecs=)([^"|'].*[^;])/;
    const mimeType = file.type.replace(codecsRegex, '$1"$2"');
    uppyRef.current.setFileMeta(file.id, { type: mimeType });
  });

  useEffect(() => {
    return () => {
      uppyRef.current.close();
    };
  }, []);

  useEffect(() => {
    uploadedFilesRef.current = existingFiles;
  }, [existingFiles]);

  useEffect(() => {
    uppyRef.current?.setOptions({
      locale: {
        strings: {
          youCanOnlyUploadX: {
            '0': translate('global.upload.youCanOnlyUpload1'),
            '1': translate('global.upload.youCanOnlyUploadX'),
          },
        },
      },
    });
  }, [locale]);

  const onFileDelete = (file: IUploadedFile) => {
    const uppyFile = uppyRef.current.getFiles().find(it => it.name === file.originalName);

    if (uppyFile) {
      uppyRef.current.removeFile(uppyFile.id);
    }
    onDelete(file);
  };

  return (
    <>
      <div className="mb-2 file-dropzone-container">
        {!uploadDisabled && (
          <FileDropzoneNew uppy={uppyRef?.current} fileUploadType={fileUploadType} disableCamera={maxFiles <= existingFiles.length} />
        )}
      </div>
      {!!existingFiles?.length && (
        <UploadedFileProvider>
          <UploadedFilesListNew files={existingFiles} onFileDelete={onFileDelete} />
        </UploadedFileProvider>
      )}
    </>
  );
};

const mapStateToProps = ({ locale }: IRootState) => ({ locale });
type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(memo(FileUploadNew));
