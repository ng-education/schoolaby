import React, { FC, useState } from 'react';
import { Dropdown, DropdownToggle, DropdownMenu } from 'reactstrap';
import CustomButton from 'app/shared/layout/custom-button/custom-button';

import './custom-dropdown.scss';

interface ICustomDropdown {
  title: string;
  button?: JSX.Element;
}

const CustomDropdown: FC<ICustomDropdown> = ({ children, title, button }) => {
  const [dropdownOpen, setDropdownOpen] = useState<boolean>(false);

  return (
    <Dropdown isOpen={dropdownOpen} toggle={() => setDropdownOpen(prevState => !prevState)}>
      <DropdownToggle tag={'div'} className={'h-100'}>
        {button ? button : <CustomButton title={title} buttonType={'primary'} onClick={e => e.preventDefault()} />}
      </DropdownToggle>
      <DropdownMenu className={'custom-dropdown'} right>
        {children}
      </DropdownMenu>
    </Dropdown>
  );
};

export default CustomDropdown;
