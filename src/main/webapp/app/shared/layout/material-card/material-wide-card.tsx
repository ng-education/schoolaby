import './material-wide-card.scss';
import React, { FC, useEffect, useState } from 'react';
import { Card, CardBody, CardImg, Col, Row } from 'reactstrap';
import { APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { TextFormat, translate } from 'react-jhipster';
import LinesEllipsis from 'react-lines-ellipsis';
import { E_KOOLIKOTT_MATERIAL, Material, OPIQ_MATERIAL } from 'app/shared/model/material.model';
import MaterialCardModal from 'app/shared/layout/material-card/material-card-modal';
import Icon from 'app/shared/icons';
import { LIGHT_ICON } from 'app/shared/util/color-utils';
import LtiModal from 'app/shared/layout/lti/lti-modal';
import { IRootState } from 'app/shared/reducers';
import { connect } from 'react-redux';
import OwnedByUserIcon from 'app/shared/layout/owned-by-user-icon/owned-by-user-icon';

const MaterialWideCard: FC<IProps> = ({ material, login, previewEnabled }: IProps) => {
  const [created, setCreated] = useState(material.created);
  const [title, setTitle] = useState(material.title);
  const [modal, setModal] = useState(false);
  const [imageUrl, setImageUrl] =
    Math.random() < 0.5 ? useState('/content/images/material_blue.png') : useState('/content/images/material_green.png');
  const toggleModal = () => setModal(!modal);

  useEffect(() => {
    (async () => {
      const _imageUrl = await material.getImageUrl();
      _imageUrl && setImageUrl(_imageUrl);
      const _created = await material.getCreated();
      setCreated(_created);
      const _title = await material.getTitle();
      setTitle(_title);
    })();
  }, [material]);

  const renderEkoolikottMaterialIcon = () => (
    <span className={'position-absolute rounded-circle d-flex justify-content-center align-items-center ekoolikott-material-card-icon'}>
      <img src={'content/images/ekoolikoti_logo.svg'} width={'30'} height={'30'} alt={'ekoolikott-icon'} />
    </span>
  );

  const renderOpiqMaterialIcon = () => (
    <img
      src={'content/images/opiqlogo.svg'}
      className={'position-absolute rounded-circle opiq-material-card-icon'}
      width={'60'}
      height={'60'}
      alt={'opiq-icon'}
    />
  );

  const renderIconByMaterial = () => {
    if (material.type === OPIQ_MATERIAL) {
      return renderOpiqMaterialIcon();
    } else if (material.type === E_KOOLIKOTT_MATERIAL) {
      return renderEkoolikottMaterialIcon();
    } else if (material.isManuallyAdded() && material.isOwnedBy(login)) {
      return <OwnedByUserIcon id={material?.id} />;
    } else {
      return null;
    }
  };

  return (
    <Card data-testid={'material-card'} className="material-wide-card h-100" onClick={toggleModal}>
      <Row className="d-flex w-100" noGutters>
        <Col xs="4" className="h-100 d-flex justify-content-center">
          {renderIconByMaterial()}
          <CardImg top src={imageUrl} />
        </Col>
        <Col xs="8" className="d-flex">
          <CardBody className="d-flex flex-column pb-0 pt-2 pl-3 pr-2">
            <Row noGutters>
              <Col xs="12">
                <h5 className={'break-all'}>
                  <LinesEllipsis text={title} maxLine="2" />
                </h5>
              </Col>
              {material?.description && (
                <Col xs="11">
                  <h6 className={'text-secondary font-weight-light mb-0 break-all'}>
                    <LinesEllipsis text={material.description.replace(/<\/?[^>]+(>|$)/g, '')} maxLine="2" />
                  </h6>
                </Col>
              )}
            </Row>
            <Row className={'mb-2 material-footer d-flex align-items-end'} noGutters>
              <Col xs={3} className={'material-sequence'}>
                {material?.sequenceNumber >= 0 && material?.sequenceNumber != null && (
                  <span className={'d-flex justify-content-center rounded'} aria-label={translate('schoolabyApp.material.sequenceNumber')}>
                    {material.sequenceNumber + 1}
                  </span>
                )}
              </Col>
              {created && (
                <Col xs={9} className="mt-auto text-light text-right">
                  {translate('schoolabyApp.material.createdAt')}
                  <b className="text-info font-weight-normal">
                    <Icon name={'calendar'} className={'mb-1 mx-2'} fill={LIGHT_ICON} />
                    <TextFormat type="date" value={created} format={APP_LOCAL_DATE_FORMAT} />
                  </b>
                </Col>
              )}
            </Row>
          </CardBody>
        </Col>
      </Row>
      {modal && material.type !== 'LTI' && <MaterialCardModal material={material} toggleModal={toggleModal} />}
      {modal && material.type === 'LTI' && (
        <LtiModal ltiResource={material.ltiResource} key={material.ltiResource.id} onClose={toggleModal} previewEnabled={previewEnabled} />
      )}
    </Card>
  );
};

interface IProps extends StateProps {
  material: Material;
  previewEnabled?: boolean;
}

const mapStateToProps = ({ authentication }: IRootState) => ({
  login: authentication?.account?.login,
});

type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(MaterialWideCard);
