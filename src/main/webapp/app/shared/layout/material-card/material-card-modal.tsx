import './material-card-modal.scss';
import React, { FC, memo, useEffect, useState } from 'react';
import { ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import { translate } from 'react-jhipster';
import { E_KOOLIKOTT_MATERIAL, Material, OPIQ_MATERIAL } from 'app/shared/model/material.model';
import { Spinner } from 'app/shared/layout/spinner/spinner';
import Iframe from 'app/shared/layout/iframe/iframe';
import { getNoEmbedHtml, getPdfEmbedUrl, isEmbeddable } from 'app/shared/services/embed-api';
import { getExtension } from 'app/shared/util/file-utils';
import CloseButton from 'app/shared/layout/close-button/close-button';
import UrlInput from 'app/shared/layout/url-input/url-input';
import { useMaterialState } from 'app/shared/contexts/material-context';
import { EmbeddedFile } from 'app/shared/form/embedded-file';
import CustomButton from 'app/shared/layout/custom-button/custom-button';
import CustomModal from 'app/shared/layout/custom-modal/custom-modal';
import { UseBaseMutationResult } from 'react-query';

interface IProps {
  toggleModal: () => void;
  material: Material;
  isOpen?: boolean;
  removable?: boolean;
  addable?: boolean;
  patchRequest?: UseBaseMutationResult;
}

const MaterialCardModal: FC<IProps> = ({
  toggleModal,
  material,
  isOpen = true,
  removable = false,
  addable = false,
  patchRequest,
}: IProps) => {
  const [spinner, setSpinner] = useState<boolean>(true);
  const [noEmbedHtml, setNoEmbedHtml] = useState<string>();
  const [iframeUrl, setIframeUrl] = useState<string>();
  const [iframeLoaded, setIframeLoaded] = useState<boolean>(false);
  const [chosen, setChosen] = useState<boolean>(false);
  const [linkUrl, setLinkUrl] = useState<string>();
  const { materials, addMaterial, removeMaterial } = useMaterialState();

  useEffect(() => {
    setChosen(materials.some(chosenMaterial => chosenMaterial.getKey() === material?.getKey()));
  }, [material, materials]);

  const containsInsecureIframeSrc = (html: string) => {
    return RegExp('<iframe[^>]*src=["\']http:\\/\\/').test(html);
  };

  const clearState = () => {
    setSpinner(true);
    setNoEmbedHtml(null);
    setIframeUrl(null);
    setIframeLoaded(false);
    setLinkUrl(null);
  };

  const toggle = () => {
    toggleModal();
    clearState();
  };

  const editable = addable || removable;

  useEffect(() => {
    setSpinner(true);
    (async () => {
      const materialUrl = await material?.getMaterialUrl();
      if (materialUrl && material?.type === E_KOOLIKOTT_MATERIAL) {
        setLinkUrl(materialUrl);
        setIframeUrl(materialUrl);
        return;
      }

      const url = await material?.getUrl();
      setLinkUrl(url);
      const extension = getExtension(url);

      if (extension === 'pdf') {
        const shouldBeProxied = new URL(url).host !== window.location.host;
        setIframeUrl(getPdfEmbedUrl(url, shouldBeProxied));
        return;
      }

      if (material?.uploadedFile && !extension) {
        setIframeUrl(url);
        return;
      }

      const html = await getNoEmbedHtml(url);
      if (html && !containsInsecureIframeSrc(html)) {
        setNoEmbedHtml(html);
        return;
      }

      if ((!extension || extension === 'html') && (await isEmbeddable(url)) && material?.type !== OPIQ_MATERIAL) {
        setIframeUrl(url);
        return;
      }

      setSpinner(false);
    })();
  }, [material]);

  useEffect(() => {
    if (noEmbedHtml || iframeLoaded) {
      setSpinner(false);
    }
  }, [noEmbedHtml, iframeLoaded]);

  const onRemove = () => {
    removeMaterial(material);
    if (patchRequest) {
      const index = materials.findIndex(mat => mat.id === material.id);
      const newMaterials = materials.splice(index, 1);
      patchRequest.mutateAsync({ materials: newMaterials });
    }
    toggle();
  };

  const CardModalFooter = () => (
    <ModalFooter className={'pt-0'}>
      {removable && chosen && <CustomButton title={translate('global.form.removeMaterial')} buttonType={'danger'} onClick={onRemove} />}
      {addable && !chosen && (
        <CustomButton
          title={translate('global.form.chooseMaterial')}
          buttonType={'primary'}
          onClick={() => {
            addMaterial(material);
            toggle();
          }}
        />
      )}
    </ModalFooter>
  );

  return (
    <CustomModal centered isOpen={isOpen} toggle={toggle} className="material-card-modal" contentClassName="border-0">
      <div className={'d-flex align-items-center justify-content-between m-3'}>
        <ModalHeader className={'p-0'}>{material?.title}</ModalHeader>
        <CloseButton onClick={toggle} className={'close-modal-button'} />
      </div>
      <ModalBody className={'pt-1' + (editable ? ' mb-3' : '')}>
        <div
          className={'text-muted h6 mt-2 preserve-space'}
          dangerouslySetInnerHTML={{
            __html: material?.description,
          }}
        />
        <Spinner loading={spinner} />
        {iframeUrl && (
          <div className={'embed-responsive embed-responsive-4by3 iframe-container'}>
            <Iframe
              role={'iframe'}
              className="embed-responsive-item"
              src={iframeUrl}
              onLoad={() => setIframeLoaded(true)}
              allowFullScreen
            />
          </div>
        )}
        {noEmbedHtml && (
          <div
            className={'embed-responsive embed-responsive-4by3'}
            dangerouslySetInnerHTML={{
              __html: noEmbedHtml,
            }}
          />
        )}
        {material?.uploadedFile && <EmbeddedFile file={material.uploadedFile} embed embedPdf={false} />}
        <br />
        <UrlInput buttonAction={'open'} role={'url-textbox'} defaultValue={linkUrl} disabled />
      </ModalBody>
      {editable && <CardModalFooter />}
    </CustomModal>
  );
};
export default memo(MaterialCardModal);
