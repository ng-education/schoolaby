import React, { FC, useState } from 'react';
import { Modal, Row, Col } from 'reactstrap';
import NewJourneyModal from 'app/shared/layout/journey-options/new-journey-modal/new-journey-modal';
import { translate } from 'react-jhipster';
import JourneyTemplateModal from '../new-journey-modal/journey-template-modal';
import { NewJourneyModalProps } from '../new-journey-modal-props';
import { isMobile } from 'react-device-detect';

import './journey-options-modal.scss';

const NEW_JOURNEY = 'NewJourney';
const JOURNEY_TEMPLATE = 'JourneyTemplate';

type JourneyOption = typeof NEW_JOURNEY | typeof JOURNEY_TEMPLATE;

const JourneyOptionsModal: FC<NewJourneyModalProps> = ({ isModalOpen, closeModal }) => {
  const [displayNewJourneyModal, setDisplayNewJourneyModal] = useState<boolean>(false);
  const [displayJourneyTemplateModal, setDisplayJourneyTemplateModal] = useState<boolean>(false);

  const JourneyOption = ({ imageSrc, text, onClick, blocked = false, imgAlt = undefined }) => (
    <div
      onClick={onClick}
      className={`journey-option bg-white p-4 d-flex flex-column justify-content-center position-relative h-100${
        blocked ? ' blocked' : ''
      }`}
    >
      <img src={imageSrc} className="mx-lg-4" alt={imgAlt} />
      <h4 className="text-center mx-3">{text}</h4>
      {blocked && <span className={'blocked-message'}>{translate('schoolabyApp.journey.useJourneyTemplateBlocked')}</span>}
    </div>
  );

  const openOptionModal = (option: JourneyOption, optionBlocked = false) => {
    if (optionBlocked) {
      return;
    }
    option === NEW_JOURNEY ? setDisplayNewJourneyModal(true) : setDisplayJourneyTemplateModal(true);
    closeModal();
  };

  const closeOptionModal = (option: JourneyOption) => {
    option === NEW_JOURNEY ? setDisplayNewJourneyModal(false) : setDisplayJourneyTemplateModal(false);
  };

  if (displayNewJourneyModal) {
    return <NewJourneyModal isModalOpen={displayNewJourneyModal} closeModal={() => closeOptionModal(NEW_JOURNEY)} />;
  } else if (displayJourneyTemplateModal) {
    return <JourneyTemplateModal isModalOpen={displayJourneyTemplateModal} closeModal={() => closeOptionModal(JOURNEY_TEMPLATE)} />;
  }

  return (
    <Modal
      isOpen={isModalOpen}
      toggle={closeModal}
      className="journey-options mx-5 mr-sm-4 ml-sm-auto"
      backdropClassName="dark-backdrop"
      contentClassName="bg-transparent journey-options-content"
    >
      <Row className="flex-row-reverse">
        <Col sm={6} className="mb-4">
          <JourneyOption
            imageSrc="content/images/template_journey_image.svg"
            text={translate('schoolabyApp.journey.useJourneyTemplate')}
            onClick={() => openOptionModal(JOURNEY_TEMPLATE, isMobile)}
            blocked={isMobile}
            imgAlt="Journey template image"
          />
        </Col>
        <Col sm={6} className="mb-4">
          <JourneyOption
            imageSrc="content/images/new_journey_image.svg"
            text={translate('schoolabyApp.journey.createNewJourney')}
            onClick={() => openOptionModal(NEW_JOURNEY)}
            imgAlt="New journey image"
          />
        </Col>
      </Row>
    </Modal>
  );
};

export default JourneyOptionsModal;
