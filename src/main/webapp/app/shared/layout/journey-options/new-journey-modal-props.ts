export interface NewJourneyModalProps {
  isModalOpen: boolean;
  closeModal: () => void;
}
