import React, { FC } from 'react';
import '../journey-option-modal/journey-option-modal.scss';
import { Field } from 'react-final-form';
import JourneyOptionModal from 'app/shared/layout/journey-options/journey-option-modal/journey-option-modal';
import { Input } from 'app/shared/form';
import { useNewJourneyOptionSchema } from 'app/shared/schema/update-entity';
import { translate } from 'react-jhipster';
import { useHistory } from 'react-router-dom';

interface NewJourneyModalProps {
  isModalOpen: boolean;
  closeModal: () => void;
}

const NewJourneyModal: FC<NewJourneyModalProps> = ({ isModalOpen, closeModal }) => {
  const history = useHistory();

  const openJourneyForm = (journeyTitle: string) => {
    history.push(`/journey/new?journeyTitle=${journeyTitle}`);
  };

  return (
    <JourneyOptionModal
      isModalOpen={isModalOpen}
      closeModal={closeModal}
      imageSrc="content/images/new_journey_image.svg"
      title={translate('schoolabyApp.journey.addJourneyTitle')}
      getValidationSchema={useNewJourneyOptionSchema}
      onSubmit={openJourneyForm}
    >
      <Field
        name="journeyTitle"
        className="py-4 px-3"
        type="text"
        placeholder={translate('schoolabyApp.journey.journeyTitlePlaceholder')}
        component={Input}
        autoFocus
      />
    </JourneyOptionModal>
  );
};

export default NewJourneyModal;
