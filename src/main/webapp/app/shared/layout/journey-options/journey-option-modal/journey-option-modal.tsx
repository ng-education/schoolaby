import React, { FC } from 'react';
import { Modal, Row, Col, ModalBody, ModalFooter, Button } from 'reactstrap';
import './journey-option-modal.scss';
import { Form } from 'react-final-form';
import { IRootState } from 'app/shared/reducers';
import { connect } from 'react-redux';
import { createYupValidator } from 'app/shared/util/form-utils';
import { AnySchema } from 'yup';
import { translate } from 'react-jhipster';

interface JourneyOptionModalProps extends StateProps {
  isModalOpen: boolean;
  closeModal: () => void;
  imageSrc: string;
  title: string;
  children: JSX.Element | JSX.Element[];
  getValidationSchema?: (locale) => AnySchema;
  onSubmit: (journeyTitle: string) => void;
}

const JourneyOptionModal: FC<JourneyOptionModalProps> = ({
  isModalOpen,
  closeModal,
  children,
  imageSrc,
  title,
  getValidationSchema,
  onSubmit,
  locale,
}) => {
  return (
    <Modal
      isOpen={isModalOpen}
      toggle={closeModal}
      backdropClassName="dark-backdrop"
      contentClassName="journey-option-content border-0 p-2 bg-white"
      centered
    >
      <Form
        onSubmit={({ journeyTitle }) => {
          onSubmit(journeyTitle);
        }}
        validate={getValidationSchema ? createYupValidator(getValidationSchema(locale)) : undefined}
      >
        {({ handleSubmit }) => (
          <form onSubmit={handleSubmit}>
            <ModalBody className="pb-0">
              <Row>
                <Col xs={12} className="d-flex journey-option-image justify-content-center">
                  <img src={imageSrc} />
                </Col>
                <Col xs={12}>
                  <h3 className="text-center mx-5 px-4 pb-2">{title}</h3>
                </Col>
                <Col xs={12}>{children}</Col>
              </Row>
            </ModalBody>
            <ModalFooter className="justify-content-between border-0 pt-0">
              <Button type="button" className="cancel-btn bg-white px-4 py-2" onClick={closeModal}>
                {translate('entity.action.cancel')}
              </Button>
              <Button type="submit" className="btn-primary border-0 px-4 py-2">
                {translate('entity.action.add')}
              </Button>
            </ModalFooter>
          </form>
        )}
      </Form>
    </Modal>
  );
};

const mapStateToProps = ({ locale }: IRootState) => ({
  locale: locale.currentLocale,
});
type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(JourneyOptionModal);
