import './header.scss';
import React from 'react';
import { Storage, translate } from 'react-jhipster';
import { DropdownMenu, DropdownToggle, UncontrolledDropdown } from 'reactstrap';
import { AccountMenu, LocaleMenu } from 'app/shared/layout/menus';
import { connect } from 'react-redux';
import { IRootState } from 'app/shared/reducers';
import { setLocale } from 'app/shared/reducers/locale';
import Avatar from 'app/shared/layout/header/avatar';
import { getFullName } from 'app/shared/util/string-utils';
import { Locale } from 'app/config/translation';

interface IAvatarMenuItemProps extends StateProps, DispatchProps {}

const AvatarMenuItem = (props: IAvatarMenuItemProps) => {
  const handleLocaleChange = event => {
    const langKey = event.target.value;
    Storage.session.set('locale', langKey);
    props.setLocale(langKey);
  };

  return (
    <>
      <UncontrolledDropdown nav inNavbar tag="div">
        <DropdownToggle nav className="d-flex align-items-center" aria-label={translate('global.header.profile')}>
          <Avatar fullName={getFullName(props.account)} />
        </DropdownToggle>
        <DropdownMenu className="admin-menu" right>
          <AccountMenu isAuthenticated={props.isAuthenticated} />
        </DropdownMenu>
      </UncontrolledDropdown>
      <LocaleMenu currentLocale={props.currentLocale} onClick={handleLocaleChange} />
    </>
  );
};

const mapStateToProps = ({ authentication, locale }: IRootState) => ({
  isAuthenticated: authentication?.isAuthenticated,
  currentLocale: locale?.currentLocale as Locale,
  account: authentication?.account,
});

const mapDispatchToProps = { setLocale };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(AvatarMenuItem);
