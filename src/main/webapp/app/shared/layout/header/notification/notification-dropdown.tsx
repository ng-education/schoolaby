import React, { memo, SetStateAction, useEffect, useState } from 'react';
import { translate } from 'react-jhipster';
import { Button, Container, Dropdown, DropdownItem, DropdownMenu, DropdownToggle, FormGroup, Input, NavItem, Row } from 'reactstrap';
import { getSubjectIcon } from 'app/shared/icons';
import { INFO } from 'app/shared/util/color-utils';
import Notification from 'app/shared/layout/header/notification/notification';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { filterNotifications, getUniqueNotificationsByLink } from 'app/shared/layout/header/notification/notification-util';
import { INotification } from 'app/shared/model/notification.model';
import groupBy from 'lodash/groupBy';
import map from 'lodash/map';
import { PatchNotificationsParam, usePatchNotifications } from 'app/shared/services/notification-api';
import { Link, useLocation } from 'react-router-dom';
import { DateTime } from 'luxon';
import { useUserNotificationDropdownState } from 'app/shared/contexts/user-notification-context';
import './notification-dropdown.scss';

interface IProps {
  icon: JSX.Element;
  title: JSX.Element;
  notifications: INotification[];
  unreadCount: number;
  isChats?: boolean;
  oneNotificationPerLink?: boolean;
  dropdownOpen: boolean;
  setDropdownOpen: React.Dispatch<SetStateAction<boolean>>;
  journeyId?: number;
}

interface NotificationGroupedByJourney {
  journeyName: string;
  notifications: INotification[];
  subject: string;
}

const NotificationDropdown = ({
  icon,
  title,
  notifications,
  unreadCount,
  isChats,
  oneNotificationPerLink,
  dropdownOpen,
  setDropdownOpen,
  journeyId,
}: IProps) => {
  const [searchQuery, setSearchQuery] = useState<string>(null);
  const [notificationsGroupedByJourney, setNotificationsGroupedByJourney] = useState<NotificationGroupedByJourney[]>();
  const { setJourneyId } = useUserNotificationDropdownState();
  const patchNotifications = usePatchNotifications();
  const location = useLocation();

  const getPatchParams = value => ({
    id: value.id,
    read: DateTime.utc().toISO(),
  });

  const markNotificationsReadOnURLMatch = () => {
    const currentURL = location.pathname + location.search;
    const matchingNotifications: PatchNotificationsParam[] = notifications
      .filter(value => value.link === currentURL && !value.read)
      .map(getPatchParams);
    matchingNotifications.length > 0 && patchNotifications.mutateAsync([...matchingNotifications]);
  };

  useEffect(() => {
    markNotificationsReadOnURLMatch();
  }, [notifications, location]);

  const groupNotificationsByJourney = (notificationsToGroup: INotification[]): NotificationGroupedByJourney[] => {
    const groupedByJourney: NotificationGroupedByJourney[] = [];
    map(groupBy(notificationsToGroup, 'journeyId'), values => {
      groupedByJourney.push({
        journeyName: values[0]?.journeyName,
        notifications: values,
        subject: values[0]?.subject?.label,
      });
    });
    return groupedByJourney;
  };

  useEffect(() => {
    const notificationsToFilter = oneNotificationPerLink ? getUniqueNotificationsByLink(notifications) : notifications;
    const notificationsByJourneyId = journeyId
      ? notificationsToFilter.filter(notification => notification.journeyId === journeyId)
      : notificationsToFilter;
    const notificationsByJourney = groupNotificationsByJourney(
      searchQuery ? filterNotifications(notificationsByJourneyId, searchQuery.toLowerCase()) : notificationsByJourneyId
    );
    setNotificationsGroupedByJourney(notificationsByJourney);
  }, [notifications, searchQuery, journeyId]);

  const toggleDropDown = e => {
    setJourneyId(null);
    setDropdownOpen(prevState => !prevState);
    e.stopPropagation();
  };

  const NotificationIcon = (): JSX.Element => {
    return (
      <>
        {unreadCount > 0 && <span className={'position-absolute unread-notifications-count'}>{unreadCount}</span>}
        {icon}
      </>
    );
  };
  const searchInput = (): JSX.Element => (
    <FormGroup className={'position-relative mb-1 px-4 w-100'}>
      <FontAwesomeIcon className={'position-absolute search-icon'} icon={['fas', 'search']} color={INFO} size={'lg'} />
      <Input
        key={'searchQuery'}
        onChange={e => setSearchQuery(e.target.value)}
        className={'pl-5 border-0'}
        placeholder={translate('entity.action.search')}
      />
    </FormGroup>
  );
  const getPatchNotificationsParameters = (): PatchNotificationsParam[] => {
    return notifications.filter(value => !value.read).map(getPatchParams);
  };
  const hasUnreadNotifications = () => {
    return notifications.filter(value => !value.read).length > 0;
  };

  return (
    <NavItem className={'notification-dropdown d-flex rounded-circle pt-1' + (dropdownOpen ? ' navbar-active' : '')}>
      <Dropdown
        toggle={toggleDropDown}
        className={'d-flex align-items-center'}
        isOpen={dropdownOpen}
        aria-label={translate('global.header.messages')}
      >
        <DropdownToggle className={'position-relative'}>
          <NotificationIcon />
        </DropdownToggle>
        <DropdownMenu className={'mt-3'}>
          <Container className={'d-block mt-2 px-0'}>
            <Row noGutters className={'justify-content-between align-items-center px-4 mb-3 w-100'}>
              {title}
              {!isChats && (
                <Button
                  color="info"
                  onClick={() => patchNotifications.mutateAsync([...getPatchNotificationsParameters()])}
                  disabled={!hasUnreadNotifications()}
                >
                  {translate('schoolabyApp.notification.markAllAsRead')}
                </Button>
              )}
            </Row>
            {isChats && searchInput()}
            {notificationsGroupedByJourney?.length ? (
              notificationsGroupedByJourney.map((journeyNotifications, i) => (
                <Row key={i} className={'text-break pt-3 pl-4 pb-5 notifications-by-educational-alignment'}>
                  <span className={'mr-2 position-relative pl-3'}>{getSubjectIcon(journeyNotifications.subject, '20', '20', INFO)}</span>
                  <span className={'educational-alignment-name mb-3'}>{journeyNotifications.journeyName}</span>
                  {journeyNotifications.notifications.map((n, j) => (
                    <Notification key={j} notification={n} onClick={toggleDropDown} />
                  ))}
                </Row>
              ))
            ) : (
              <p className="text-center mt-3">{translate('global.header.noNotifications')}</p>
            )}
          </Container>
          {isChats && (
            <div className="fixed-bottom bg-white">
              <DropdownItem divider />
              <div className="row justify-content-center">
                <Button tag={Link} to="/chats" onClick={e => toggleDropDown(e)}>
                  {translate('global.header.viewAll')}
                </Button>
              </div>
            </div>
          )}
        </DropdownMenu>
      </Dropdown>
    </NavItem>
  );
};

export default memo(NotificationDropdown);
