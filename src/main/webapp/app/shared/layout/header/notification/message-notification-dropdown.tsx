import React, { memo, useEffect, useState } from 'react';
import { translate } from 'react-jhipster';
import Icon from 'app/shared/icons';
import { ICON_GREY } from 'app/shared/util/color-utils';
import NotificationDropdown from 'app/shared/layout/header/notification/notification-dropdown';
import { useGetNotifications } from 'app/shared/services/notification-api';
import { getUniqueNotificationsByLink } from 'app/shared/layout/header/notification/notification-util';

const MessageNotificationDropdown = () => {
  const [unreadChatCount, setUnreadChatCount] = useState<number>(0);
  const [dropdownOpen, setDropdownOpen] = useState<boolean>(false);
  const { data: notifications } = useGetNotifications({ type: 'MESSAGE', sort: 'id,desc' });

  useEffect(() => {
    const oneNotificationPerLink = getUniqueNotificationsByLink(notifications);
    setUnreadChatCount(oneNotificationPerLink.filter(d => !d.read).length);
  }, [notifications]);

  const MessageIcon = (): JSX.Element => {
    return <Icon name={'mail'} width={'22px'} height={'16px'} stroke={ICON_GREY} />;
  };
  const MessageNotificationsTitle = (): JSX.Element => {
    return <h3>{translate('global.header.messages')}</h3>;
  };
  return (
    <span data-tour="messages">
      <NotificationDropdown
        notifications={notifications ? notifications : []}
        icon={<MessageIcon />}
        title={<MessageNotificationsTitle />}
        isChats
        unreadCount={unreadChatCount}
        oneNotificationPerLink
        dropdownOpen={dropdownOpen}
        setDropdownOpen={setDropdownOpen}
      />
    </span>
  );
};

export default memo(MessageNotificationDropdown);
