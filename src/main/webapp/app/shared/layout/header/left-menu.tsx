import React from 'react';
import { translate } from 'react-jhipster';
import { DropdownMenu, DropdownToggle, Nav, NavItem, NavLink, UncontrolledDropdown } from 'reactstrap';
import { NavLink as Link } from 'react-router-dom';
import Icon, { IconName } from 'app/shared/icons';
import { ICON_GREY } from 'app/shared/util/color-utils';
import { AdminMenu } from 'app/shared/layout/menus';
import MenuItem from 'app/shared/layout/menus/menu-item';
import { NavDropdown } from 'app/shared/layout/menus/menu-components';
import QuizzeriaButton from 'app/shared/layout/header/quizzeria-button';

interface IMenuItemProps {
  iconName: IconName;
  path: string;
  contentKey: string;
  exact?: boolean;
}

interface IProps {
  isAuthenticated: boolean;
  isAdmin: boolean;
  isSwaggerEnabled: boolean;
}

const HeaderButton = (menuItemProps: IMenuItemProps) => (
  <NavItem>
    <NavLink exact={menuItemProps.exact} tag={Link} to={menuItemProps.path} className="d-flex align-items-center rounded-sm p-1">
      <Icon name={menuItemProps.iconName} className={'nav-icon ml-2'} width={'22px'} height={'22px'} stroke={ICON_GREY} />
      <span className="ml-3 mr-2 py-1">{translate('global.menu.' + menuItemProps.contentKey)}</span>
    </NavLink>
  </NavItem>
);

const LeftMenu = (props: IProps) => {
  return (
    <Nav id="header-tabs" navbar>
      <HeaderButton exact contentKey="dashboard" iconName={'squaresEmpty'} path="/" />
      <HeaderButton contentKey="journeys" iconName={'map'} path="/journey" />
      <QuizzeriaButton />
      {props.isAuthenticated && props.isAdmin && (
        <NavItem>
          <UncontrolledDropdown nav inNavbar tag="div">
            <DropdownToggle nav className="d-flex align-items-center">
              <Icon name="gear" className={'nav-icon ml-2'} width={'22px'} height={'22px'} stroke={ICON_GREY} />
              <span className={'ml-3 mr-2'}>{translate('global.menu.settings')}</span>
            </DropdownToggle>
            <DropdownMenu className="admin-menu">
              <NavDropdown
                icon="th-list"
                name={translate('global.menu.entities.main')}
                id="entity-menu"
                style={{ maxHeight: '80vh', overflow: 'auto' }}
              >
                <MenuItem icon="asterisk" to="/grading-scheme">
                  {translate('global.menu.entities.gradingScheme')}
                </MenuItem>
                <MenuItem icon="asterisk" to="/grading-scheme-value">
                  {translate('global.menu.entities.gradingSchemeValue')}
                </MenuItem>
                <MenuItem icon="asterisk" to="/material">
                  {translate('global.menu.entities.material')}
                </MenuItem>
                <MenuItem icon="asterisk" to="/educational-alignment">
                  {translate('global.menu.entities.educationalAlignment')}
                </MenuItem>
                <MenuItem icon="asterisk" to="/person-role">
                  {translate('global.menu.entities.personRole')}
                </MenuItem>
              </NavDropdown>
              <AdminMenu showSwagger={props.isSwaggerEnabled} />
            </DropdownMenu>
          </UncontrolledDropdown>
        </NavItem>
      )}
    </Nav>
  );
};

export default LeftMenu;
