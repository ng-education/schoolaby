import React, { FC, useState } from 'react';
import { Modal, ModalBody, ModalFooter, Button, Label } from 'reactstrap';
import { Storage } from 'react-jhipster';
import { translate } from 'react-jhipster';
import { Input } from 'reactstrap';

import './info-modal.scss';

interface InfoModalProps {
  seenKey?: string;
  showModal: boolean;
  setShowModal: (boolean) => void;
  bodyText: string;
}

const InfoModal: FC<InfoModalProps> = ({ bodyText, showModal, setShowModal, seenKey }) => {
  const [seen, setSeen] = useState<boolean>(false);

  const toggleModal = () => {
    seenKey && Storage.local.set(seenKey, seen);
    setShowModal(!showModal);
  };

  return (
    <Modal centered isOpen={showModal} toggle={toggleModal} className={'info-modal'}>
      <ModalBody>
        <span className={'primary'}>{bodyText}</span>
      </ModalBody>
      <ModalFooter className={'border-0'}>
        <Label for={'info-seen'} className={'d-flex align-items-center my-0'}>
          <p className={'mr-2 mb-0'}>{translate('global.dontShowAgain')}</p>
          <Input id={'info-seen'} type={'checkbox'} className={'my-0'} onChange={e => setSeen(e.target.checked)} />
        </Label>
        <Button onClick={toggleModal} color="primary">
          {translate('global.ok')}
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default InfoModal;
