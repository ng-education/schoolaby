import React, { FC, useEffect, useState } from 'react';
import Select, { ActionMeta, InputActionMeta, ValueType } from 'react-select';

interface SearchBarProps {
  onChange: (input: string) => any;
  className?: string;
  placeholder?: string;
}

const SearchBar: FC<SearchBarProps> = ({ onChange, className, placeholder }) => {
  const [input, setInput] = useState('');
  let selectInnerRef;

  useEffect(() => {
    onChange(input);
  }, [input]);

  return (
    <Select
      aria-label={'search-bar'}
      placeholder={placeholder}
      inputValue={input}
      onInputChange={(newValue: string, meta: InputActionMeta) => {
        if (meta.action === 'input-change') {
          setInput(newValue);
        }
      }}
      onChange={(value: ValueType<any, false>, meta: ActionMeta<any>) => {
        if (meta.action === 'select-option') {
          setInput(value.value);
          selectInnerRef.select.onMenuClose();
        }
      }}
      ref={refs => {
        selectInnerRef = refs;
      }}
      components={{
        DropdownIndicator: undefined,
      }}
      /* If true, then selecting a value from the dropdown sets inputIsHiddenAfterUpdate=true.
       We want to keep showing input to allow the user to modify the query after selection */
      closeMenuOnSelect={false}
      isOptionSelected={() => false}
      noOptionsMessage={() => null}
      openMenuOnClick={false}
      controlShouldRenderValue={false}
      className={className}
      styles={{
        control(provided, state) {
          const borderColor = state.isFocused ? '#96c8da' : 'none';
          return { ...provided, boxShadow: 'none', borderColor, '&:hover': '' };
        },
      }}
    />
  );
};

export default React.memo(SearchBar);
