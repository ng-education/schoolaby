import React, { FC, useState } from 'react';
import CustomModal from 'app/shared/layout/custom-modal/custom-modal';
import { Button, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import { Field, Form } from 'react-final-form';
import { Input } from 'app/shared/form';
import './support-modal.scss';
import ISupportEmail from 'app/shared/model/support-email.model';
import { translate } from 'react-jhipster';
import { useSupportSchema } from 'app/shared/schema/support';
import { connect } from 'react-redux';
import { IRootState } from 'app/shared/reducers';
import { createYupValidator } from 'app/shared/util/form-utils';
import { useCreateSupportEmail } from 'app/shared/services/support-api';
import SuccessModal from 'app/shared/layout/success-modal/success-modal';

interface SupportModalProps extends StateProps {
  isOpen: boolean;
  toggle: () => void;
}

const SupportModal: FC<SupportModalProps> = ({ isOpen, toggle, locale, userEmail }) => {
  const schema = useSupportSchema(locale);
  const [displaySuccessModal, setDisplaySuccessModal] = useState<boolean>(false);

  const toggleSuccessModal = () => {
    if (displaySuccessModal) {
      setDisplaySuccessModal(false);
      toggle();
    } else {
      setDisplaySuccessModal(true);
    }
  };

  const { mutate: createEmail } = useCreateSupportEmail(toggleSuccessModal);

  const onSupportFormSubmit = (values: ISupportEmail) => {
    createEmail(values);
  };

  return (
    <>
      <CustomModal isOpen={isOpen && !displaySuccessModal} toggle={toggle} centered contentClassName="support-modal-content">
        <Form onSubmit={onSupportFormSubmit} validate={createYupValidator(schema)}>
          {({ handleSubmit }) => (
            <form onSubmit={handleSubmit}>
              <ModalHeader tag="h4">{translate('schoolabyApp.support.sendSupportMessage')}</ModalHeader>
              <ModalBody>
                <Field
                  id="support-form-subject"
                  name="subject"
                  label={translate('schoolabyApp.support.subject')}
                  type="text"
                  render={Input}
                />
                <Field
                  id="support-form-content"
                  name="content"
                  label={translate('schoolabyApp.support.content')}
                  type="textarea"
                  render={Input}
                />
              </ModalBody>
              <ModalFooter className="d-flex justify-content-between">
                <Button type="button" color="secondary" className="px-4 py-2" onClick={toggle}>
                  {translate('entity.action.cancel')}
                </Button>
                <Button type="submit" color="primary" className="px-4 py-2">
                  {translate('schoolabyApp.chat.send')}
                </Button>
              </ModalFooter>
            </form>
          )}
        </Form>
      </CustomModal>
      <SuccessModal
        modal={displaySuccessModal && isOpen}
        toggle={toggleSuccessModal}
        title={translate('schoolabyApp.support.emailSent')}
        description={translate('schoolabyApp.support.emailSentDescription', { email: userEmail })}
        signature={translate('schoolabyApp.support.schoolabyTeam')}
      />
    </>
  );
};

const mapStateToProps = ({ locale, authentication }: IRootState) => ({
  locale: locale.currentLocale,
  userEmail: authentication?.account?.email,
});

type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(SupportModal);
