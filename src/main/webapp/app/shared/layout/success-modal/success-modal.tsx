import './success-modal.scss';
import { Button, ModalBody, ModalFooter } from 'reactstrap';
import React, { FC, useMemo } from 'react';
import { translate } from 'react-jhipster';
import ReactMarkdown from 'react-markdown';
import CustomModal from 'app/shared/layout/custom-modal/custom-modal';
import CloseButton from 'app/shared/layout/close-button/close-button';

interface SuccessModalProps {
  modal: boolean;
  toggle: () => void;
  title: string;
  description: string;
  signature?: string;
  isJourneyModal?: boolean;
}

const SuccessModal: FC<SuccessModalProps> = ({ modal, isJourneyModal = false, toggle, title, description, signature }) =>
  useMemo(
    () =>
      modal && (
        <CustomModal isOpen={modal} toggle={toggle} centered size={'lg'} className="success-modal">
          <ModalBody className="text-center">
            <div className="d-flex align-items-end">
              <CloseButton onClick={toggle} />
            </div>
            <img src="/content/images/success.svg" className="img-fluid mb-5" alt="Responsive image" />
            <h2 className={'mt-2'}>{title}</h2>
            <h4 className={`text-secondary font-weight-normal preserve-space ml-5 mr-4 ${!isJourneyModal && 'text-left'}`}>
              <ReactMarkdown source={description} />
            </h4>
            {!!signature && <h5 className="mt-4">{signature}</h5>}
          </ModalBody>
          <ModalFooter className="border-0 d-flex justify-content-center my-2">
            <Button outline color="secondary" size="lg" onClick={toggle}>
              {translate('schoolabyApp.submission.ok')}
            </Button>
          </ModalFooter>
        </CustomModal>
      ),
    [modal]
  );

export default SuccessModal;
