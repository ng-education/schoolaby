import React, { FC, ReactNode } from 'react';
import { Button, FormFeedback, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import './confirmation-modal-new.scss';
import { translate } from 'react-jhipster';

interface ConfirmationModalNewProps {
  isModalOpen: boolean;
  closeModal: () => void;
  modalTitle: string;
  confirmationButtonDisabled?: boolean;
  handleConfirmation?: () => void;
  customCancelButton?: ReactNode;
  customConfirmationButton?: ReactNode;
  errorMessage?: string;
  children?: JSX.Element | JSX.Element[];
  className?: string;
}

const ConfirmationModalNew: FC<ConfirmationModalNewProps> = ({
  isModalOpen,
  closeModal,
  modalTitle,
  confirmationButtonDisabled,
  handleConfirmation,
  customCancelButton,
  customConfirmationButton,
  errorMessage,
  children,
  className,
}) => {
  return (
    <Modal
      isOpen={isModalOpen}
      toggle={closeModal}
      backdropClassName="dark-backdrop"
      contentClassName="confirmation-content border-0 p-2 bg-white"
      centered
      className={className}
    >
      <ModalHeader className="pb-1 border-0">{modalTitle}</ModalHeader>
      <ModalBody className="py-0">
        {children}
        {errorMessage && (
          <div>
            <FormFeedback className={'form-feedback'}>{errorMessage}</FormFeedback>
          </div>
        )}
      </ModalBody>
      <ModalFooter className="justify-content-between border-0 pt-0">
        {customCancelButton ? (
          customCancelButton
        ) : (
          <Button type="button" className="cancel-btn bg-white px-4 py-2" onClick={closeModal}>
            {translate('global.no')}
          </Button>
        )}
        {customConfirmationButton ? (
          customConfirmationButton
        ) : (
          <Button
            onClick={handleConfirmation}
            type="button"
            className="btn-primary border-0 px-4 py-2"
            disabled={confirmationButtonDisabled}
          >
            {translate('global.yes')}
          </Button>
        )}
      </ModalFooter>
    </Modal>
  );
};

export default ConfirmationModalNew;
