import React, { FC, useEffect, useState } from 'react';

import '../material-card-new.scss';
import './material-wide-card-new.scss';
import { MaterialNarrowCardProps } from 'app/shared/layout/material-card-new/material-narrow-card-new/material-narrow-card-new';
import { useChosenMaterialState, useMaterialState } from 'app/shared/contexts/material-context';
import { getRandomImage } from 'app/shared/util/image-utils';
import WideCardNew from 'app/shared/layout/material-card-new/material-wide-card-new/wide-card-new';

interface MaterialWideCardProps extends MaterialNarrowCardProps {
  deletable?: boolean;
}

const MaterialWideCardNew: FC<MaterialWideCardProps> = ({ material, materialAction, deletable, showButton = true }) => {
  const { materials } = useMaterialState();
  const { setChosenMaterial } = useChosenMaterialState();
  const [chosen, setChosen] = useState<boolean>(false);

  useEffect(() => {
    setChosen(materials.some(chosenMaterial => chosenMaterial.getKey() === material.getKey()));
  }, [materials]);

  return (
    <WideCardNew
      materialAction={materialAction}
      title={material.title}
      description={material.description}
      deletable={deletable}
      chosen={chosen}
      onClick={() => setChosenMaterial(material)}
      type={material.getType()}
      showButton={showButton}
      image={
        <img
          alt={material.title}
          src={material.imageUrl ? material.imageUrl : getRandomImage()}
          onError={e => {
            // @ts-ignore
            e.target.src = getRandomImage();
          }}
        />
      }
    />
  );
};

export default MaterialWideCardNew;
