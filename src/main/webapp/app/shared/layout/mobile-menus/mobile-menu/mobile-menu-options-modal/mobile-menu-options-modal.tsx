import React, { FC } from 'react';
import { Modal } from 'reactstrap';
import './mobile-menu-options-modal.scss';

interface MoreOptionsModalProps {
  isOpen: boolean;
  toggleModal: () => void;
}

const MobileMenuOptionsModal: FC<MoreOptionsModalProps> = ({ isOpen, toggleModal, children }) => {
  return (
    <Modal
      fade={false}
      isOpen={isOpen}
      toggle={toggleModal}
      onClick={toggleModal}
      backdropClassName="dark-backdrop"
      className="mobile-menu-more-options position-absolute m-0"
      contentClassName="mobile-menu-more-options-content bg-white pt-3 px-4 py-4"
    >
      {children}
    </Modal>
  );
};

export default MobileMenuOptionsModal;
