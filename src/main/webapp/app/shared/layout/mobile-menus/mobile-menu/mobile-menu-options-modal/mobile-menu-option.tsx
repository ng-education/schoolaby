import React, { BaseSyntheticEvent, FC } from 'react';
import Icon, { IconName } from 'app/shared/icons';
import { SBY_ICON_BEIGE } from 'app/shared/util/color-utils';

interface MobileOptionsMenuProps {
  iconName: IconName;
  text: string;
  onClick?: (event?: BaseSyntheticEvent) => void;
}

const MobileMenuOption: FC<MobileOptionsMenuProps> = ({ iconName, text, onClick }) => (
  <div className="w-100 cursor-pointer menu-option" onClick={onClick}>
    <Icon name={iconName} fill={SBY_ICON_BEIGE} stroke="none" width="20" height="20" />
    <span className="ml-3">{text}</span>
  </div>
);

export default MobileMenuOption;
