import React, { FC, ReactNode } from 'react';
import Icon from 'app/shared/icons';
import { SBY_ICON_BEIGE } from 'app/shared/util/color-utils';
import { NavItem, NavLink } from 'reactstrap';
import { translate } from 'react-jhipster';
import './mobile-menu.scss';

interface MobileMenuProps {
  options?: ReactNode;
  toggleOptionsModal?: () => void;
}

const MobileMenu: FC<MobileMenuProps> = ({ children, options, toggleOptionsModal }) => (
  <div className="mobile-menu d-lg-none w-100">
    <div className="mobile-menu-content bg-white">
      <div className="tab-container d-flex justify-content-around">
        {children}
        {options && (
          <NavItem className="tab-item-new d-flex more-options position-relative align-items-center h-100">
            <NavLink className="d-flex flex-column align-items-center justify-content-center pt-2" onClick={toggleOptionsModal}>
              <div className="iconContainer">
                <Icon name="dropdown" stroke="none" width="12" height="12" fill={SBY_ICON_BEIGE} />
              </div>
              <span className="tab-title">{translate('global.menu.more')}</span>
            </NavLink>
          </NavItem>
        )}
      </div>
    </div>
    {options}
  </div>
);

export default MobileMenu;
