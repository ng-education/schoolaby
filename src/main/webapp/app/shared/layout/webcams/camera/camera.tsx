import './camera.scss';
import React, { FC } from 'react';
import { CameraButtons } from 'app/shared/layout/webcams/camera-buttons';
import Icon from 'app/shared/icons';
import { translate } from 'react-jhipster';
import { INFO } from 'app/shared/util/color-utils';
import { useCameraErrorState, useMediaStreamState } from 'app/shared/contexts/camera-context';
import { Spinner } from 'app/shared/layout/spinner/spinner';

export const Camera: FC = () => {
  const { mediaStream } = useMediaStreamState();
  const { cameraError } = useCameraErrorState();

  const isCameraFound = () => cameraError === 'NotFoundError';
  const isCameraAllowed = () => cameraError === 'NotAllowedError';
  const shouldShowCamera = mediaStream && mediaStream.active;

  return cameraError === null ? (
    shouldShowCamera ? (
      <span className={'camera-container'}>
        <span className={'d-flex w-100 camera justify-content-center'}>
          {!!mediaStream && (
            <video
              ref={video => {
                if (video) {
                  video.srcObject = mediaStream;
                }
              }}
              muted
              playsInline
              autoPlay
            />
          )}
        </span>
        <span className={'d-flex w-100 buttons justify-content-center py-4'}>
          <CameraButtons />
        </span>
      </span>
    ) : (
      <Spinner className="auto transform-none position-static" containerClassName={'w-100'} />
    )
  ) : (
    <div className={'text-center my-5 d-flex flex-column justify-content-center p-2'}>
      <div className={'icon-wrapper'}>
        <Icon name={'camera'} width={'120px'} height={'110px'} stroke={INFO} />
      </div>
      {isCameraFound() && (
        <>
          <h1>{translate('schoolabyApp.submission.camera.cameraNotAvailableTitle')}</h1>
          <p>{translate('schoolabyApp.submission.camera.cameraNotAvailableMessage')}</p>
        </>
      )}
      {isCameraAllowed() && (
        <>
          <h1>{translate('schoolabyApp.submission.camera.grantPermissionTitle')}</h1>
          <p>{translate('schoolabyApp.submission.camera.grantPermissionMessage')}</p>
        </>
      )}
      {!isCameraFound() && !isCameraAllowed() && (
        <>
          <h1>{translate('schoolabyApp.submission.camera.generalCameraErrorTitle')}</h1>
          <p>{translate('schoolabyApp.submission.camera.generalCameraErrorMessage')}</p>
        </>
      )}
    </div>
  );
};
