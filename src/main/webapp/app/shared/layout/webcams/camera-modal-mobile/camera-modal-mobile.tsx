import React from 'react';
import CloseButton from 'app/shared/layout/close-button/close-button';
import { Camera } from 'app/shared/layout/webcams/camera/camera';
import { Col, Modal, ModalBody, Row } from 'reactstrap';
import { translate } from 'react-jhipster';
import { useCameraMountTargetState, useCapturingState } from 'app/shared/contexts/camera-context';
import './camera-modal-mobile.scss';

const CameraModalMobile = () => {
  const { cameraMountTarget, setCameraMountTarget } = useCameraMountTargetState();
  const { capturing } = useCapturingState();

  const closeModal = () => (capturing ? setCameraMountTarget('BODY') : setCameraMountTarget(undefined));
  return (
    <Modal backdrop={'static'} isOpen={cameraMountTarget === 'MODAL'} className="base-modal camera-modal-mobile">
      <ModalBody className="p-0">
        <Row noGutters>
          <span className={'mobile-modal-header d-flex align-items-center'}>
            <h3 className={'title'}>{translate('schoolabyApp.uploadedFile.cameraModal.title')}</h3>
            <CloseButton onClick={closeModal} />
          </span>
          <Camera />
          <Col xs={12} className={'camera-upload-statusbar'} />
        </Row>
      </ModalBody>
    </Modal>
  );
};

export default CameraModalMobile;
