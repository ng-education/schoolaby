import './camera-modal.scss';
import React, { FC } from 'react';
import CloseButton from 'app/shared/layout/close-button/close-button';
import { Col, ModalBody, Row } from 'reactstrap';
import { translate } from 'react-jhipster';
import { useCameraMountTargetState, useCapturingState } from 'app/shared/contexts/camera-context';
import { Camera } from 'app/shared/layout/webcams/camera/camera';
import CustomModal from 'app/shared/layout/custom-modal/custom-modal';

const CameraModal: FC = () => {
  const { cameraMountTarget, setCameraMountTarget } = useCameraMountTargetState();
  const { capturing } = useCapturingState();

  const closeModal = () => (capturing ? setCameraMountTarget('BODY') : setCameraMountTarget(undefined));

  return (
    <CustomModal backdrop={'static'} isOpen={cameraMountTarget === 'MODAL'} className="base-modal camera-modal">
      <span className={'border-wrapper'}>
        <ModalBody className="p-0">
          <Row>
            <Col xs={'11'} s={'10'}>
              <h1 className={'align-items-center d-flex'}>{translate('schoolabyApp.uploadedFile.cameraModal.title')}</h1>
            </Col>
            <Col className={'close-button-container align-items-center d-flex'} xs={'1'} s={'2'}>
              <CloseButton onClick={closeModal} />
            </Col>
          </Row>
          <Row noGutters>
            <Col xs={12}>
              <Camera />
            </Col>
            <Col xs={12} className={'camera-upload-statusbar'} />
          </Row>
        </ModalBody>
      </span>
    </CustomModal>
  );
};

export default CameraModal;
