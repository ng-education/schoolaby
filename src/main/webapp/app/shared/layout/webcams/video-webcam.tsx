import './camera-modal/camera-modal.scss';
import React, { FC, useCallback, useEffect, useRef, useState } from 'react';
import {
  useCameraErrorState,
  useCameraFacingModeState,
  useCameraMountTargetState,
  useCapturingState,
  useMediaStreamState,
  useTakePictureState,
  useUppyState,
} from 'app/shared/contexts/camera-context';
import { generateFileName } from 'app/shared/util/file-utils';
import Webcam from 'react-webcam';
import { toast } from 'react-toastify';
import { translate } from 'react-jhipster';
import { isMobile, isSafari, isTablet } from 'react-device-detect';
import { useFileNameState, useMaxFilesState } from 'app/shared/form/file-upload-context';

export type CameraMountTarget = 'MODAL' | 'BODY';

export const VideoWebcam: FC = () => {
  const { takePicture, setTakePicture } = useTakePictureState();
  const { setCameraMountTarget } = useCameraMountTargetState();
  const { maxFiles } = useMaxFilesState();
  const [imageData, setImageData] = useState<string>(null);
  const { capturing } = useCapturingState();
  const webcamRef = useRef(null);
  const mediaRecorderRef = React.useRef(null);
  const [recordedChunks, setRecordedChunks] = React.useState([]);
  const { uppy } = useUppyState();
  const { cameraFacingMode } = useCameraFacingModeState();
  const { setMediaStream } = useMediaStreamState();
  const { setCameraError } = useCameraErrorState();
  const { fileName: videoFileName } = useFileNameState();
  const imageType = 'image/png';
  const deskTopConstraints = {
    height: { min: 720, ideal: 1080 },
    width: { min: 1280, ideal: 1920 },
    frameRate: 30,
    facingMode: cameraFacingMode,
  };
  const phoneTabletConstraints = {
    height: { max: 1280 },
    width: { max: 720 },
    aspectRatio: 16 / 9,
    frameRate: 30,
    facingMode: cameraFacingMode,
  };

  useEffect(() => () => setMediaStream(null), []);

  const capture = React.useCallback(() => {
    const image = webcamRef.current.getScreenshot();
    setImageData(image);
  }, [webcamRef]);

  const convertBase64ToFile = (fileName: string) => {
    const byteString = atob(imageData.split(',')[1]);
    const ab = new ArrayBuffer(byteString.length);
    const ia = new Uint8Array(ab);
    for (let i = 0; i < byteString.length; i += 1) {
      ia[i] = byteString.charCodeAt(i);
    }
    return new File([ab], fileName, { type: imageType });
  };

  const uploadUppyFile = () => {
    const fileName = generateFileName('image', 'png');
    uppy.addFile({
      name: fileName,
      type: imageType,
      data: convertBase64ToFile(fileName),
    });
    // @ts-ignore
    uppy.once('upload-success', file => {
      if (file.extension === 'png' && imageData) {
        toast.success(translate('schoolabyApp.submission.camera.pictureSuccess'));
      }
      if (maxFiles === 1) {
        setCameraMountTarget(undefined);
      }
      setTakePicture(false);
    });
    // @ts-ignore
    uppy.once('upload-error', file => {
      if (file.extension === 'png' && imageData) {
        toast.error(translate('schoolabyApp.submission.camera.pictureError'));
      }
      setTakePicture(false);
    });
  };

  useEffect(() => {
    if (takePicture) {
      capture();
    }
  }, [takePicture]);

  useEffect(() => {
    if (imageData) {
      uploadUppyFile();
      setImageData(null);
    }
  }, [imageData]);

  const handleDataAvailable = React.useCallback(
    ({ data }) => {
      if (data.size > 0) {
        setRecordedChunks(prev => prev.concat(data));
      }
    },
    [setRecordedChunks, mediaRecorderRef]
  );

  const createMediaRecorder = useCallback(() => {
    if (webcamRef.current?.stream) {
      // @ts-ignore
      mediaRecorderRef.current = new MediaRecorder(webcamRef.current.stream, {
        mimeType: isSafari ? 'video/mp4' : 'video/webm;codecs=vp8,opus',
      });
      mediaRecorderRef.current.addEventListener('dataavailable', handleDataAvailable);
    }
  }, []);

  const startVideoCapturing = React.useCallback(() => {
    if (webcamRef?.current?.stream) {
      createMediaRecorder();
      mediaRecorderRef.current.start();
    }
  }, [webcamRef]);

  useEffect(() => {
    if (recordedChunks.length && !capturing) {
      uppy.addFile({
        name: generateFileName(videoFileName ?? 'video', isSafari ? 'mp4' : 'webm'),
        data: new Blob(recordedChunks, { type: isSafari ? 'video/mp4' : 'video/webm' }),
      });
      // @ts-ignore
      uppy.once('upload-success', file => {
        if (file.extension === 'webm' && recordedChunks.length) {
          toast.success(translate('schoolabyApp.submission.camera.videoSuccess'));
        }
        if (maxFiles === 1) {
          setCameraMountTarget(undefined);
        }
      });
      // @ts-ignore
      uppy.once('upload-error', file => {
        if (file.extension === 'webm' && recordedChunks.length) {
          toast.error(translate('schoolabyApp.submission.camera.videoError'));
          setRecordedChunks([]);
        }
      });
      setRecordedChunks([]);
    }
  }, [recordedChunks, capturing]);

  const stopVideoCapturing = () => {
    if (mediaRecorderRef?.current) {
      mediaRecorderRef.current.stop();
      setRecordedChunks([]);
    }
  };

  useEffect(() => {
    if (capturing) {
      startVideoCapturing();
    } else {
      stopVideoCapturing();
    }
  }, [capturing]);

  return (
    <Webcam
      onUserMedia={stream => {
        setCameraError(null);
        setMediaStream(stream);
      }}
      // @ts-ignore
      onUserMediaError={err => setCameraError(err.name)}
      className={'video-webcam'}
      screenshotFormat={imageType}
      videoConstraints={isMobile || isTablet ? phoneTabletConstraints : deskTopConstraints}
      minScreenshotHeight={isTablet || isMobile ? 720 : 1280}
      minScreenshotWidth={isTablet || isMobile ? 1280 : 720}
      ref={webcamRef}
    />
  );
};
