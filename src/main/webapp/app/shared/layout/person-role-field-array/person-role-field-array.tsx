import React, { FC } from 'react';
import SchoolSearchDropdown from 'app/shared/layout/school-search-dropdown/school-search-dropdown';
import { FieldArray } from 'react-final-form-arrays';
import { Field, useForm } from 'react-final-form';
import HorizontalRadio from 'app/shared/layout/horizontal-radio-button/horizontal-radio-button';
import { Label } from 'reactstrap';
import { translate } from 'react-jhipster';
import { ISchool } from 'app/shared/model/school.model';
import { getRoleBasedOnAuthority } from 'app/shared/util/auth-utils';
import roleOptions from 'app/account/common/role-options';
import AddButtonNew from 'app/shared/layout/add-button-new/add-button-new';

interface PersonRoleFieldArrayProps {
  selectedCountry: string;
  setCurrentAccount: (values) => void;
}

const PersonRoleFieldArray: FC<PersonRoleFieldArrayProps> = ({ selectedCountry, setCurrentAccount }) => {
  const { getState, restart: restartForm, change } = useForm();

  const saveCurrentValuesAndRestart = () => {
    setCurrentAccount(getState().values);
    restartForm();
  };

  const removePersonRole = (index: number) => {
    getState().values.personRoles.splice(index, 1);
    saveCurrentValuesAndRestart();
  };

  const selectSchool = index => (school: ISchool) => {
    change(`personRoles[${index}].school`, school);
    setCurrentAccount(getState().values);
  };

  const addRole = () => {
    getState().values.personRoles.push({
      role: getRoleBasedOnAuthority(getState().values.authority),
      school: {},
      active: true,
    });
    saveCurrentValuesAndRestart();
  };

  return (
    <>
      <FieldArray name={'personRoles'}>
        {({ fields }) =>
          fields.map((name, index) => {
            return (
              <div key={name} className={'mb-3'}>
                <Field name={`${name}.school`}>
                  {({ meta, input }) => (
                    <SchoolSearchDropdown
                      meta={meta}
                      input={input}
                      country={selectedCountry}
                      removePersonRole={() => removePersonRole(index)}
                      selectSchool={selectSchool(index)}
                    />
                  )}
                </Field>
                <HorizontalRadio>
                  {roleOptions.map(({ role, translationKey }) => (
                    <Field key={`${name}-${role}`} type={'radio'} value={role} name={`${name}.role`}>
                      {({ input }) => {
                        return (
                          <div className={'form-group mt-2 mr-2'}>
                            <input
                              id={`${name}-${role}`}
                              name={input.name}
                              type="radio"
                              value={role}
                              checked={input.checked}
                              onChange={input.onChange}
                              className={'mr-1'}
                            />
                            <Label for={`${name}-${role}`}>{translate(translationKey)}</Label>
                          </div>
                        );
                      }}
                    </Field>
                  ))}
                </HorizontalRadio>
              </div>
            );
          })
        }
      </FieldArray>
      <AddButtonNew
        aria-label={translate('entity.add.school')}
        className="justify-content-end px-3 py-2"
        onClick={addRole}
        title={translate('entity.add.school')}
      />
    </>
  );
};

export default PersonRoleFieldArray;
