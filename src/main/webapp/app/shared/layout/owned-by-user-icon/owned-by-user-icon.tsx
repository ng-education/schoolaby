import React, { FC } from 'react';
import Icon from 'app/shared/icons';
import { UncontrolledTooltip } from 'reactstrap';
import { translate } from 'react-jhipster';

interface OwnedByUserIconProps {
  id: number;
}

const OwnedByUserIcon: FC<OwnedByUserIconProps> = ({ id }: OwnedByUserIconProps) => {
  return (
    <>
      <UncontrolledTooltip trigger={'hover'} target={`ownedByUserIcon-${id}`}>
        {translate('schoolabyApp.material.createdByYou')}
      </UncontrolledTooltip>
      <div
        className={'position-absolute user-material-card-icon rounded-circle'}
        id={`ownedByUserIcon-${id}`}
        role={`ownedByUserIconContainer`}
      >
        <Icon name={'user'} width={'45'} height={'45'} className={'p-2'} />
      </div>
    </>
  );
};

export default OwnedByUserIcon;
