import './url-input.scss';
import React, { useEffect, useState } from 'react';
import { Input } from 'app/shared/form';
import { Button, Input as ReactstrapInput } from 'reactstrap';
import { translate } from 'react-jhipster';
import { OnChange } from 'react-final-form-listeners';
import { Field, useForm } from 'react-final-form';

export interface ShareData {
  title?: string;
  text?: string;
  url?: string;
}

interface IProps {
  disabled?: boolean;
  defaultValue?: string;
  isInForm?: boolean;
  role?: string;
  id?: string;
  buttonAction: 'copy' | 'open' | 'share';
  sharingContent?: ShareData;
}

const UrlInput = ({ disabled, defaultValue, isInForm, role, id, buttonAction, sharingContent }: IProps) => {
  const [containerFocus, setContainerFocus] = useState<boolean>(false);
  const [inputValue, setInputValue] = useState<string>('');
  const form = isInForm ? useForm() : undefined;

  useEffect(() => {
    if (defaultValue !== undefined) {
      setInputValue(defaultValue);
      form?.change('url', defaultValue);
    }
  }, [defaultValue]);

  const getButtonTranslationKey = (): string => {
    if (buttonAction === 'copy') {
      return 'entity.action.copy';
    } else if (buttonAction === 'open') {
      return 'entity.action.open';
    } else if (buttonAction === 'share') {
      return 'entity.action.share';
    }
  };

  return (
    <div className={`url-input-container-new d-flex align-items-center${containerFocus ? ' focus' : ''}`}>
      {isInForm ? (
        <>
          <Field
            id={id}
            type="text"
            name="url"
            defaultValue={defaultValue}
            disabled={disabled}
            className={`url-input ${disabled ? 'disabled-input' : 'border-primary'}`}
            role={role ? role : 'textbox'}
            render={props => <Input {...props} />}
          />
          <OnChange name="url">
            {(value: string) => {
              setInputValue(value);
            }}
          </OnChange>
        </>
      ) : (
        <ReactstrapInput
          id={id}
          role={role || 'textbox'}
          className="border-primary url-input"
          readOnly
          defaultValue={defaultValue}
          onFocus={() => setContainerFocus(true)}
          onBlur={() => setContainerFocus(false)}
          onClick={e => {
            // @ts-ignore
            e.target.select();
            // @ts-ignore
            e.target.scrollLeft = e.target.scrollWidth;
          }}
        />
      )}
      <Button
        className="open-link-button btn-primary d-flex align-items-center mr-1"
        type={'button'}
        disabled={!inputValue}
        onClick={() => {
          if (buttonAction === 'open') {
            window.open(inputValue);
          } else if (buttonAction === 'copy') {
            navigator.clipboard.writeText(inputValue);
          } else if (buttonAction === 'share') {
            window.getSelection().empty();
            // @ts-ignore
            if (navigator.canShare && navigator.canShare(sharingContent)) {
              // @ts-ignore
              navigator.share(sharingContent);
            }
          }
        }}
      >
        {translate(getButtonTranslationKey())}
      </Button>
    </div>
  );
};

export default UrlInput;
