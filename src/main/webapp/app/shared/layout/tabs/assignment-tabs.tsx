import React, { FC, useContext, useEffect, useState } from 'react';
import Tab from 'app/shared/layout/tabs/tab';
import { ANALYTICS_TAB, ASSIGNMENT_TAB, BACKPACK_TAB, SUBMISSIONS_TAB, TabType } from 'app/shared/layout/tabs/tabs';
import { ASSIGNMENT } from 'app/shared/util/entity-utils';
import { AssignmentContext } from 'app/journey/journey-detail/journey-detail-new';
import { useHistory } from 'react-router-dom';
import { SBY_ICON_BEIGE } from 'app/shared/util/color-utils';

interface AssignmentTabsProps {
  mobileTabs?: boolean;
  newAssignmentViewDesign?: boolean; // TODO: SCHOOL-970 Remove
}

const AssignmentTabs: FC<AssignmentTabsProps> = ({ mobileTabs, newAssignmentViewDesign = false }) => {
  const history = useHistory();
  const { assignment, isAllowedToModify } = useContext(AssignmentContext);
  const [tabType, setTabType] = useState<TabType>(ASSIGNMENT_TAB);

  useEffect(() => {
    const path = history.location.pathname.split('/');

    // TODO: SCHOOL-970 Keep 3, remove 4
    if (path[newAssignmentViewDesign ? 4 : 3]?.length) {
      setTabType(path[newAssignmentViewDesign ? 4 : 3] as TabType);
    } else {
      setTabType(ASSIGNMENT_TAB);
    }
  }, [history.location.pathname]);

  const TabContainer = ({ children }) =>
    mobileTabs ? <>{children}</> : <div className="tab-container-new d-flex align-items-end position-relative h-100">{children}</div>;

  return (
    <TabContainer>
      <Tab
        tabEntityId={assignment?.id}
        entityType={ASSIGNMENT}
        active={tabType === ASSIGNMENT_TAB}
        href={''}
        iconName={'destination'}
        title={'schoolabyApp.assignment.detail.tabs.assignment'}
        mobileTab={mobileTabs}
        newAssignmentViewDesign={newAssignmentViewDesign}
      />
      {newAssignmentViewDesign && isAllowedToModify && (
        <Tab
          tabEntityId={assignment?.id}
          entityType={ASSIGNMENT}
          active={tabType === SUBMISSIONS_TAB}
          href={'submissions'}
          iconName={'coloredUser'}
          iconProps={{
            width: '12',
            height: '12',
            fill: SBY_ICON_BEIGE,
          }}
          title={'schoolabyApp.assignment.submissions'}
          mobileTab={mobileTabs}
          newAssignmentViewDesign={newAssignmentViewDesign}
        />
      )}
      <Tab
        tabEntityId={assignment?.id}
        entityType={ASSIGNMENT}
        active={tabType === BACKPACK_TAB}
        href={'backpack'}
        iconName={'backpack'}
        title={'schoolabyApp.assignment.detail.tabs.backpack'}
        mobileTab={mobileTabs}
        newAssignmentViewDesign={newAssignmentViewDesign}
      />
      {isAllowedToModify && (
        <Tab
          tabEntityId={assignment?.id}
          entityType={ASSIGNMENT}
          active={tabType === ANALYTICS_TAB}
          href={'analytics'}
          iconName={'statisticsIcon'}
          title={'schoolabyApp.assignment.detail.tabs.analytics'}
          mobileTab={mobileTabs}
          newAssignmentViewDesign={newAssignmentViewDesign}
        />
      )}
    </TabContainer>
  );
};

export default AssignmentTabs;
