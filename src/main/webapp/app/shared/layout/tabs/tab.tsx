import React, { FC } from 'react';
import Icon, { IconName } from 'app/shared/icons';
import { NavItem, NavLink } from 'reactstrap';
import { translate } from 'react-jhipster';
import { useHistory } from 'react-router-dom';

import './tab.scss';
import { TabType } from 'app/shared/layout/tabs/tabs';
import { ASSIGNMENT, JOURNEY } from 'app/shared/util/entity-utils';
import classNames from 'classnames';

interface IconProps {
  width?: string;
  height?: string;
  viewBox?: string;
  fill?: string;
}

interface TabProps {
  href: string;
  title: string;
  iconName: IconName;
  tabEntityId: number;
  entityType: typeof JOURNEY | typeof ASSIGNMENT;
  active: boolean;
  tabType?: TabType;
  additionalSearchParam?: string;
  iconProps?: IconProps;
  shouldHaveAttention?: boolean;
  mobileTab?: boolean;
  newAssignmentViewDesign?: boolean; // TODO: SCHOOL-970 Remove
}

const Tab: FC<TabProps> = ({
  href,
  title,
  iconName,
  tabEntityId,
  entityType,
  active,
  additionalSearchParam,
  iconProps,
  shouldHaveAttention = false,
  mobileTab = false,
  newAssignmentViewDesign = false,
}) => {
  const history = useHistory();

  const url = { pathname: '', search: '' };
  if (entityType === JOURNEY) {
    url.pathname = `/journey/${tabEntityId}/${href}`;
  } else if (entityType === ASSIGNMENT) {
    url.pathname = `/assignment${newAssignmentViewDesign ? '/newDesign' : ''}/${tabEntityId}/${href}`;
    url.search = additionalSearchParam;
  }

  return (
    <NavItem
      className={classNames({
        'tab-item-new position-relative h-100 d-flex align-items-center justify-content-center': true,
        active,
        mobile: mobileTab,
      })}
    >
      <NavLink
        onClick={() => history.push(url)}
        className={mobileTab ? 'd-flex flex-column align-items-center justify-content-center pt-2 px-2' : undefined}
      >
        <div className={'iconContainer position-relative'}>
          <Icon name={iconName} width={iconProps?.width} height={iconProps?.height} viewBox={iconProps?.viewBox} fill={iconProps?.fill} />
          {shouldHaveAttention && <Icon name={'attentionIcon'} className={'attention-icon position-absolute'} />}
        </div>
        <span className={`tab-title ${shouldHaveAttention && 'attention'}`}>{translate(title)}</span>
      </NavLink>
      {active && <hr className={'activity-indicator position-absolute px-1'} />}
    </NavItem>
  );
};

export default Tab;
