import React, { FC, useContext, useEffect, useState } from 'react';
import Tab from 'app/shared/layout/tabs/tab';
import { SBY_ICON_BEIGE } from 'app/shared/util/color-utils';
import { APPS_TAB, ASSIGNMENT_TAB, GRADES_TAB, JOURNEY_TAB, SUBMISSIONS_TAB, TabType } from 'app/shared/layout/tabs/tabs';
import { useGetLtiConfigurations } from 'app/shared/services/lti-api';
import { isLtiConfigured } from 'app/shared/util/lti-utils';
import { JOURNEY } from 'app/shared/util/entity-utils';
import { JourneyDetailContext } from 'app/journey/journey-detail/journey-detail-new';
import { useHistory } from 'react-router-dom';

interface JourneyTabsProps {
  mobileTabs?: boolean;
}

const JourneyTabs: FC<JourneyTabsProps> = ({ mobileTabs = false }) => {
  const history = useHistory();
  const { journey, isAllowedToModify } = useContext(JourneyDetailContext);
  const [ltiConfigurationsConfigured, setLtiConfigurationsConfigured] = useState(true);
  const { data: ltiConfigs } = useGetLtiConfigurations({ journeyId: journey.id }, !!journey.id && isAllowedToModify);
  const [tabType, setTabType] = useState<TabType>(ASSIGNMENT_TAB);

  useEffect(() => {
    const path = history.location.pathname.split('/');

    if (path[3]?.length) {
      setTabType(path[3] as TabType);
    } else {
      setTabType(JOURNEY_TAB);
    }
  }, [history.location.pathname]);

  useEffect(() => {
    ltiConfigs?.length
      ? setLtiConfigurationsConfigured(
          ltiConfigs.every(ltiConfiguration =>
            isLtiConfigured(ltiConfiguration.ltiApp.version, ltiConfiguration.consumerKey, ltiConfiguration.sharedSecret)
          )
        )
      : setLtiConfigurationsConfigured(true);
  }, [ltiConfigs]);

  const TabContainer = ({ children }) =>
    mobileTabs ? <>{children}</> : <div className="tab-container-new d-flex align-items-end position-relative h-100">{children}</div>;

  return isAllowedToModify ? (
    <TabContainer>
      <Tab
        tabEntityId={journey.id}
        entityType={JOURNEY}
        active={tabType === JOURNEY_TAB}
        href={''}
        iconName={'destination'}
        title={'schoolabyApp.journey.detail.tabs.journey'}
        mobileTab={mobileTabs}
      />
      <Tab
        tabEntityId={journey.id}
        entityType={JOURNEY}
        active={tabType === GRADES_TAB}
        href={'grades'}
        iconName={'backpack'}
        title={'schoolabyApp.journey.detail.tabs.grades'}
        mobileTab={mobileTabs}
      />
      <Tab
        tabEntityId={journey.id}
        entityType={JOURNEY}
        active={tabType === APPS_TAB}
        href={'apps'}
        iconName={'apps'}
        title={'schoolabyApp.journey.detail.tabs.apps'}
        shouldHaveAttention={!ltiConfigurationsConfigured}
        mobileTab={mobileTabs}
      />
      <Tab
        tabEntityId={journey.id}
        entityType={JOURNEY}
        active={tabType === SUBMISSIONS_TAB}
        href={'students'}
        iconName={'coloredUser'}
        iconProps={{
          width: '12',
          height: '12',
          fill: SBY_ICON_BEIGE,
        }}
        title={'global.heading.tabs.students'}
        mobileTab={mobileTabs}
      />
    </TabContainer>
  ) : null;
};

export default JourneyTabs;
