import './heading.scss';
import React, { useEffect, useState } from 'react';
import { translate } from 'react-jhipster';
import { Button, Dropdown, DropdownToggle, Nav, NavItem, NavLink } from 'reactstrap';
import Icon, { IconName } from 'app/shared/icons';
import { useHistory } from 'react-router-dom';
import classnames from 'classnames';
import { IRootState } from 'app/shared/reducers';
import { hasAnyAuthority } from 'app/shared/auth/private-route';
import { AUTHORITIES } from 'app/config/constants';
import { connect } from 'react-redux';
import { useGetLtiConfigurations } from 'app/shared/services/lti-api';
import { isLtiConfigured } from 'app/shared/util/lti-utils';
import BreadcrumbsRenderer from 'app/shared/layout/heading/breadcrumbs-renderer';
import { ICON_GREY } from 'app/shared/util/color-utils';
import { ASSIGNMENT, EntityType, JOURNEY } from 'app/shared/util/entity-utils';
import { IJourney } from 'app/shared/model/journey.model';
import LtiWarningIcon from 'app/shared/layout/heading/lti-warning-icon';

export interface ITabParams {
  tabEntityId?: number;
  additionalSearchParam?: string;
  entityType: EntityType;
}

export interface ITab {
  href: string;
  title: string;
  iconName: IconName;
  tabType: PreviewTabType;
  isMobileTab?: boolean;
  attentionIcon?: JSX.Element;
  previewEnabled?: boolean;
  openedPreviewTab?: PreviewTabType;
  setOpenedPreviewTab?: (tabType: PreviewTabType) => void;
}

export type PreviewTabType =
  | 'JOURNEY'
  | 'JOURNEY_STUDENTS'
  | 'JOURNEY_APPS'
  | 'JOURNEY_GRADES'
  | 'ASSIGNMENT'
  | 'ASSIGNMENT_BACKPACK'
  | 'ASSIGNMENT_ANALYTICS';

export interface IHeadingProps {
  previewEnabled?: boolean;
  icon?: any;
  dropdown?: JSX.Element;
  title: string;
  tabParams?: ITabParams;
  children?: JSX.Element | JSX.Element[];
  journey?: IJourney;
  className?: string;
  onBackClicked?: () => void;
  openedPreviewTab?: PreviewTabType;
  setOpenedPreviewTab?: (tabType: PreviewTabType) => void;
}

export const HeadingContext = React.createContext<{
  tabParams?: ITabParams;
  previewEnabled?: boolean;
  openedPreviewTab?: PreviewTabType;
  setOpenedPreviewTab?: (tabType: PreviewTabType) => void;
}>(null);

const Heading = ({
  children,
  dropdown,
  icon,
  tabParams,
  title,
  className,
  journey,
  account,
  isTeacherOrAdmin,
  previewEnabled,
  onBackClicked,
  openedPreviewTab,
  setOpenedPreviewTab,
}: IHeadingProps & StateProps) => {
  const history = useHistory();
  const path = history.location.pathname.split('/');
  const isAllowedToModify = isTeacherOrAdmin && !previewEnabled && journey?.teachers?.some(teacher => teacher.user.id === account.id);
  const isJourneyTabs = tabParams?.entityType === JOURNEY;

  const [dropdownOpen, setDropdownOpen] = useState(false);
  const [ltiConfigurationsConfigured, setLtiConfigurationsConfigured] = useState(true);
  const { data: ltiConfigs } = useGetLtiConfigurations(
    { journeyId: tabParams?.tabEntityId },
    isJourneyTabs && !!tabParams?.tabEntityId && isAllowedToModify
  );

  const cleanup = () => {
    setLtiConfigurationsConfigured(true);
  };

  const toggleDropDown = () => setDropdownOpen(prevState => !prevState);

  const isAssignmentTabs = () => {
    return tabParams?.tabEntityId && tabParams.entityType === ASSIGNMENT;
  };

  useEffect(() => {
    if (!isAllowedToModify) {
      return;
    }

    ltiConfigs?.length
      ? setLtiConfigurationsConfigured(
          ltiConfigs.every(ltiConfiguration =>
            isLtiConfigured(ltiConfiguration.ltiApp.version, ltiConfiguration.consumerKey, ltiConfiguration.sharedSecret)
          )
        )
      : setLtiConfigurationsConfigured(true);
    return () => {
      !tabParams?.tabEntityId && cleanup();
    };
  }, [tabParams, ltiConfigs]);

  const Tab = (tabProps: ITab) => {
    const url = { pathname: '', search: '' };
    const id = tabParams.tabEntityId;
    if (isJourneyTabs) {
      url.pathname = `/journey/${id ? id : path[2]}/${tabProps.href}`;
    } else if (isAssignmentTabs()) {
      url.pathname = `/assignment/${id}/${tabProps.href}`;
      url.search = tabParams.additionalSearchParam;
    }

    const handleTabClick = () => {
      if (previewEnabled) {
        setOpenedPreviewTab(tabProps.tabType);
      } else {
        history.push(url);
      }
    };

    return (
      <NavItem className={'ml-auto'}>
        <NavLink
          onClick={handleTabClick}
          className={classnames({
            active:
              (!previewEnabled && (path[3] === tabProps.href || (!path[3] && !tabProps.href))) || openedPreviewTab === tabProps.tabType,
          })}
        >
          <Icon name={tabProps.iconName} className={'mr-2'} width={'22px'} height={'22px'} stroke={ICON_GREY} />
          <span>{translate(tabProps.title)}</span>
        </NavLink>
        {tabProps.attentionIcon}
      </NavItem>
    );
  };

  const JourneyTabs = () =>
    isAllowedToModify || previewEnabled ? (
      <div className="tab-container d-flex align-items-end ml-auto position-relative">
        <Tab href={''} iconName={'send'} title={'schoolabyApp.journey.detail.tabs.journey'} tabType={'JOURNEY'} />
        {!!tabParams.tabEntityId && !previewEnabled && (
          <Tab href={'students'} iconName={'people'} title={'global.heading.tabs.students'} tabType={'JOURNEY_STUDENTS'} />
        )}
        <Tab
          href={'apps'}
          iconName={'squaresEmptyThin'}
          title={'schoolabyApp.journey.detail.tabs.apps'}
          tabType="JOURNEY_APPS"
          attentionIcon={!ltiConfigurationsConfigured && <LtiWarningIcon />}
        />
        {!previewEnabled && (
          <Tab href={'grades'} iconName={'book'} title={'schoolabyApp.journey.detail.tabs.grades'} tabType={'JOURNEY_GRADES'} />
        )}
      </div>
    ) : null;

  const AssignmentTabs = () => (
    <div className="tab-container d-flex align-items-end ml-auto position-relative">
      <Tab href={''} iconName={'send'} title={'schoolabyApp.assignment.detail.tabs.assignment'} tabType={'ASSIGNMENT'} />
      <Tab href={'backpack'} iconName={'book'} title={'schoolabyApp.assignment.detail.tabs.backpack'} tabType={'ASSIGNMENT_BACKPACK'} />
      {isAllowedToModify && (
        <Tab
          href={'analytics'}
          iconName={'analytics'}
          title={'schoolabyApp.assignment.detail.tabs.analytics'}
          tabType={'ASSIGNMENT_ANALYTICS'}
        />
      )}
    </div>
  );

  const Tabs = () => {
    if (isJourneyTabs) {
      return <JourneyTabs />;
    } else if (isAssignmentTabs() || previewEnabled) {
      return <AssignmentTabs />;
    }
  };

  return (
    <HeadingContext.Provider value={{ tabParams, previewEnabled, openedPreviewTab, setOpenedPreviewTab }}>
      <div className={`header${className ? ' ' + className : ''}`}>
        <Nav className={`px-0 ${(tabParams && 'nav-with-tabs') || ''}`} tabs>
          <div className="d-flex flex-wrap w-100">
            {previewEnabled && onBackClicked && (
              <Button type="button" color="white" className="mr-3 my-2" onClick={onBackClicked}>
                {translate('entity.action.back')}
              </Button>
            )}
            <div className="heading-left-side d-flex justify-content-center mr-2">
              {icon && <span className={'mr-3 d-flex align-items-center'}>{icon}</span>}
              <h2 className="m-1 text-truncate">{title}</h2>
              {!!dropdown && (
                <Dropdown
                  isOpen={dropdownOpen}
                  toggle={toggleDropDown}
                  className="d-flex align-items-center"
                  aria-label={translate('global.heading.dropdown')}
                >
                  <DropdownToggle className="pl-1 pr-1">
                    <Icon name={'dropdown'} width={'16px'} height={'16px'} stroke={ICON_GREY} fill={ICON_GREY} />
                  </DropdownToggle>
                  {dropdown}
                </Dropdown>
              )}
              <div className="d-flex flex-grow-1">{children}</div>
            </div>
            {tabParams && <Tabs />}
          </div>
        </Nav>
        <hr />
        {!previewEnabled && (
          <>
            <BreadcrumbsRenderer />
            <hr />
          </>
        )}
      </div>
    </HeadingContext.Provider>
  );
};

const mapStateToProps = ({ authentication: { account } }: IRootState) => ({
  isTeacherOrAdmin: hasAnyAuthority(account.authorities, [AUTHORITIES.TEACHER, AUTHORITIES.ADMIN]),
  account,
});

type StateProps = ReturnType<typeof mapStateToProps>;
export default connect(mapStateToProps)(Heading);
