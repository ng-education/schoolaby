import React from 'react';
import { BreadcrumbsItem } from 'react-breadcrumbs-dynamic';
import { translate } from 'react-jhipster';

const CreateBreadcrumbItem = ({ href }) => <BreadcrumbsItem to={href}>{translate('schoolabyApp.breadcrumb.createLabel')}</BreadcrumbsItem>;

export default CreateBreadcrumbItem;
