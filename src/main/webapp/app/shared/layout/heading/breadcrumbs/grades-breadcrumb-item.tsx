import React from 'react';
import { BreadcrumbsItem } from 'react-breadcrumbs-dynamic';
import { translate } from 'react-jhipster';

const GradesBreadcrumbItem = ({ journeyId }) => (
  <BreadcrumbsItem to={`/journey/${journeyId}/grades`}>{translate('schoolabyApp.journey.detail.tabs.grades')}</BreadcrumbsItem>
);

export default GradesBreadcrumbItem;
