import React from 'react';
import { BreadcrumbsItem } from 'react-breadcrumbs-dynamic';

const MilestoneTitleBreadcrumbItem = ({ journeyId, milestoneId, milestoneTitle }) => {
  return <BreadcrumbsItem to={`/milestone/${milestoneId}?journeyId=${journeyId}`}>{milestoneTitle}</BreadcrumbsItem>;
};

export default MilestoneTitleBreadcrumbItem;
