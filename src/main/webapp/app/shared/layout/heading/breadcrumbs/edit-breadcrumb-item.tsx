import React from 'react';
import { BreadcrumbsItem } from 'react-breadcrumbs-dynamic';
import { translate } from 'react-jhipster';

const EditBreadcrumbItem = ({ href }) => <BreadcrumbsItem to={href}>{translate('schoolabyApp.breadcrumb.editLabel')}</BreadcrumbsItem>;

export default EditBreadcrumbItem;
