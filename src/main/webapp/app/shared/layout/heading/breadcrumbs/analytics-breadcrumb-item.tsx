import React from 'react';
import { BreadcrumbsItem } from 'react-breadcrumbs-dynamic';
import { translate } from 'react-jhipster';

export const AnalyticsAssignmentBreadcrumbItem = ({ assignmentId, journeyId }) => (
  <BreadcrumbsItem to={`/assignment/${assignmentId}/analytics?journeyId=${journeyId}`}>
    {translate('schoolabyApp.assignment.detail.tabs.analytics')}
  </BreadcrumbsItem>
);
