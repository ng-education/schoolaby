import React from 'react';
import { BreadcrumbsItem } from 'react-breadcrumbs-dynamic';
import { translate } from 'react-jhipster';

const AssignmentStudentsBreadcrumbItem = ({ assignmentId }) => (
  // TODO: SCHOOL-970 Remove new design
  <BreadcrumbsItem to={`/assignment/newDesign/${assignmentId}/submissions`}>
    {translate('schoolabyApp.assignment.submissions')}
  </BreadcrumbsItem>
);

export default AssignmentStudentsBreadcrumbItem;
