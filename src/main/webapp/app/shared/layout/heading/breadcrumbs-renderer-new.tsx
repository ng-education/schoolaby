import './breadcrumbs-renderer.scss';
import React from 'react';
import { Breadcrumb, BreadcrumbItem } from 'reactstrap';
import { Link } from 'react-router-dom';
import { Breadcrumbs } from 'react-breadcrumbs-dynamic';

interface IProps {
  children: string;
  href: string;
  active: boolean;
}

const LocationBasedBreadcrumbItem = ({ href, children, active }: IProps) => (
  <BreadcrumbItem active={active} className={'d-flex align-items-center border rounded mr-3 pl-0'}>
    <Link to={href || ''} className={'text-muted px-3'}>
      {children}
    </Link>
  </BreadcrumbItem>
);

const BreadcrumbsRendererNew = () => (
  <Breadcrumb className={'breadcrumbs-new'}>
    <Breadcrumbs renameProps={{ to: 'href' }} finalProps={{ active: true }} item={LocationBasedBreadcrumbItem} />
  </Breadcrumb>
);

export default BreadcrumbsRendererNew;
