import React, { FC } from 'react';

import './heading-new.scss';
import Tabs, { TabEntityType } from 'app/shared/layout/tabs/tabs';
import JourneyMobileMenu from 'app/shared/layout/mobile-menus/journey-mobile-menu/journey-mobile-menu';
import { ASSIGNMENT, JOURNEY } from 'app/shared/util/entity-utils';
import BreadcrumbsRendererNew from 'app/shared/layout/heading/breadcrumbs-renderer-new';
import Tag from 'app/shared/layout/tag/tag';
import { IJourney } from 'app/shared/model/journey.model';
import AssignmentMobileMenu from 'app/shared/layout/mobile-menus/assignment-mobile-menu/assignment-mobile-menu';
import { Row } from 'reactstrap';

export const MAIN_HEADING = 'main';
export const SUB_HEADING = 'sub';
export const TEMPLATE_HEADING = 'template';

type HeadingType = typeof MAIN_HEADING | typeof SUB_HEADING | typeof TEMPLATE_HEADING;

interface HeadingProps {
  title: string;
  isAllowedToModify: boolean;
  headingType?: HeadingType;
  journey?: IJourney;
  icon?: JSX.Element;
  tabEntityType?: TabEntityType;
  tags?: string[];
  hideBreadcrumbs?: boolean;
  additionalBreadcrumbRowItems?: JSX.Element;
  newAssignmentViewDesign?: boolean; // TODO: SCHOOL-970 Remove,
  keepRightNavVisible?: boolean;
}

const HeadingNew: FC<HeadingProps> = ({
  headingType,
  title,
  isAllowedToModify,
  journey,
  icon,
  children,
  tabEntityType,
  tags,
  hideBreadcrumbs = false,
  additionalBreadcrumbRowItems,
  newAssignmentViewDesign = false, // TODO: SCHOOL-970 Remove this
  keepRightNavVisible = false,
}) => {
  const getTabsDisplayClasses = () => {
    if (keepRightNavVisible) {
      return 'd-flex';
    }
    if (tabEntityType === JOURNEY) {
      return isAllowedToModify ? 'd-none d-lg-flex' : 'd-flex';
    }
    return 'd-none d-lg-flex';
  };

  return (
    <>
      <div className={`heading-container d-flex ${headingType}${tabEntityType ? ' with-tabs' : ''}`}>
        <div className={`heading-new d-flex align-items-center justify-content-between mx-0 w-100`} data-testid={'heading-new'}>
          <div className={'icon-and-title d-flex align-items-center mr-2'}>
            {icon}
            <h2 className={`title text-truncate m-0${icon ? ' ml-2' : ''}${tags ? ' mr-4' : ''}`}>{title}</h2>
            {tags?.map((tag, i) => (
              <Tag key={`${tag}-${i}`} title={tag} disabled />
            ))}
          </div>
          <div className={`${getTabsDisplayClasses()} align-items-center h-100`}>
            {tabEntityType && <Tabs entityType={tabEntityType} newAssignmentViewDesign={newAssignmentViewDesign} />}
            {children}
          </div>
        </div>
      </div>
      {journey && isAllowedToModify && tabEntityType === JOURNEY && <JourneyMobileMenu journey={journey} />}
      {tabEntityType === ASSIGNMENT && <AssignmentMobileMenu newAssignmentViewDesign={newAssignmentViewDesign} />}
      <Row noGutters className={'breadcrumbs-container align-items-center justify-content-between'}>
        {!hideBreadcrumbs && <BreadcrumbsRendererNew />}
        {additionalBreadcrumbRowItems}
      </Row>
    </>
  );
};

export default HeadingNew;
