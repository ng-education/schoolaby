import React from 'react';
import { Modal, ModalProps } from 'reactstrap';
import './custom-modal.scss';

const CustomModal = (props: ModalProps) => {
  const { className, contentClassName, children } = props;

  return (
    <Modal
      {...props}
      backdropClassName="dark-backdrop"
      className={`custom-modal ${className ? className : ''}`}
      contentClassName={`custom-modal-content bg-white ${contentClassName ? contentClassName : ''}`}
    >
      {children}
    </Modal>
  );
};

export default CustomModal;
