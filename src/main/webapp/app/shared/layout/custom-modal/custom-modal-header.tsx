import React, { FC } from 'react';
import CloseButton from 'app/shared/layout/close-button/close-button';

interface CustomModalHeaderProps {
  toggle: () => void;
  className?: string;
}

const CustomModalHeader: FC<CustomModalHeaderProps> = ({ children, toggle, className }) => (
  <div className={`d-flex justify-content-between align-items-center p-3 ${className ? className : ''}`}>
    {children}
    <CloseButton onClick={toggle} className="p-0" />
  </div>
);

export default CustomModalHeader;
