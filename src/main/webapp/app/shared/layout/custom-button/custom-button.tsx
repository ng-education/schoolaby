import React, { FC } from 'react';
import { Button, ButtonProps } from 'reactstrap';

import './custom-button.scss';
import { withHttp } from 'app/shared/util/file-utils';
import Icon, { IconName } from 'app/shared/icons';

type ButtonType = 'primary' | 'secondary' | 'cancel' | 'white' | 'danger';

interface CustomButtonProps extends ButtonProps {
  title: string;
  buttonType: ButtonType;
  iconName?: IconName;
  iconWidth?: string;
  iconHeight?: string;
  href?: string;
  size?: 'sm' | 'md';
  sharper?: boolean;
}

const CustomButton: FC<CustomButtonProps> = ({
  title,
  className = '',
  buttonType,
  outline,
  iconName,
  iconWidth,
  iconHeight,
  onClick,
  href,
  size = '',
  sharper,
  type = 'button',
  ...buttonProps
}) => (
  <Button
    color={''}
    onClick={onClick}
    className={`d-flex align-items-center custom-button ${className} ${buttonType} ${size} ${outline ? 'outlined' : ''} ${
      sharper ? 'sharper' : ''
    }`}
    type={type}
    {...buttonProps}
  >
    {iconName && <Icon width={iconWidth} height={iconHeight} name={iconName} />}
    {href ? (
      <a href={withHttp(href)} target={'_blank'} rel={'noopener noreferrer'}>
        {title}
      </a>
    ) : (
      title
    )}
  </Button>
);

export default CustomButton;
