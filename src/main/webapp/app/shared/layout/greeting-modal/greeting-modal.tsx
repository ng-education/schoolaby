import './greeting-modal.scss';
import { Button, ModalBody, ModalFooter } from 'reactstrap';
import React, { FC, useMemo } from 'react';
import { translate } from 'react-jhipster';
import ReactMarkdown from 'react-markdown';
import { IRootState } from 'app/shared/reducers';
import { connect } from 'react-redux';
import CustomModal from 'app/shared/layout/custom-modal/custom-modal';
import { hasAnyAuthority } from 'app/shared/auth/private-route';
import { AUTHORITIES } from 'app/config/constants';
import CloseButton from 'app/shared/layout/close-button/close-button';

interface GreetingModalProps extends StateProps {
  modal: boolean;
  toggle: () => void;
}

const GreetingModal: FC<GreetingModalProps> = ({ modal, toggle, isStudent, account }) =>
  useMemo(
    () =>
      modal && (
        <CustomModal isOpen={modal} toggle={toggle} centered size={'lg'} className="greeting-modal">
          <ModalBody className="text-center">
            <div className="d-flex align-items-end">
              <CloseButton onClick={toggle} />
            </div>
            <img src="/content/images/welcome_to_schoolaby.svg" className="img-fluid mb-5" alt="Responsive image" />
            <h2 className={'mt-2'}>
              {isStudent
                ? translate('schoolabyApp.dashboard.studentGreetingTitle', { name: account.firstName })
                : translate('schoolabyApp.dashboard.teacherGreetingTitle', { name: account.firstName })}
            </h2>
            <h4 className="text-secondary font-weight-normal preserve-space ml-5 mr-4 text-left">
              <ReactMarkdown
                source={translate(
                  isStudent ? 'schoolabyApp.dashboard.studentGreetingDescription' : 'schoolabyApp.dashboard.teacherGreetingDescription'
                )}
              />
            </h4>
          </ModalBody>
          <ModalFooter className="border-0 d-flex justify-content-center my-2">
            <Button outline color="secondary" size="lg" onClick={toggle}>
              {translate('schoolabyApp.submission.ok')}
            </Button>
          </ModalFooter>
        </CustomModal>
      ),
    [modal]
  );

const mapStateToProps = ({ authentication }: IRootState) => ({
  isStudent: hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.STUDENT]),
  account: authentication.account,
});

type StateProps = ReturnType<typeof mapStateToProps>;
export default connect(mapStateToProps)(GreetingModal);
