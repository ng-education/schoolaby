import React, { FC, useCallback, useEffect, useState } from 'react';
import { IJourney } from 'app/shared/model/journey.model';
import JourneyTemplateCard from 'app/shared/layout/journey-template/journey-template-card/journey-template-card';
import { translate } from 'react-jhipster';
import { useGetJourneyTemplatesBySearch } from 'app/shared/services/journey-api';
import { useJourneyTemplateSubjectState } from 'app/shared/contexts/journey-template-context';
import { IRootState } from 'app/shared/reducers';
import { connect } from 'react-redux';
import flatMap from 'lodash/flatMap';
import './template-list.scss';
import debounce from 'lodash/debounce';

const TEACHER_TEMPLATES = 'teacherTemplates';
const ALL_TEMPLATES = 'allTemplates';

type JourneyTemplateTab = typeof TEACHER_TEMPLATES | typeof ALL_TEMPLATES;

interface TemplateListProps extends StateProps {
  search: string;
}

const TemplateList: FC<TemplateListProps> = ({ search, userId }) => {
  const [templates, setTemplates] = useState<IJourney[]>([]);
  const [activeTab, setActiveTab] = useState<JourneyTemplateTab>(TEACHER_TEMPLATES);
  const { templateSubject } = useJourneyTemplateSubjectState();
  const [currentSearch, setCurrentSearch] = useState<string>(search);

  const {
    data: teacherTemplates,
    isFetching: isFetchingTeacherTemplates,
    hasNextPage: hasTeacherTemplatesNextPage,
    fetchNextPage: fetchTeacherTemplatesNextPage,
  } = useGetJourneyTemplatesBySearch(currentSearch, templateSubject, userId);
  const {
    data: allTemplates,
    isFetching: isFetchingAllTemplates,
    hasNextPage: hasAllTemplatesNextPage,
    fetchNextPage: fetchAllTemplatesNextPage,
  } = useGetJourneyTemplatesBySearch(currentSearch, templateSubject);

  const hasNextPage = activeTab === TEACHER_TEMPLATES ? hasTeacherTemplatesNextPage : hasAllTemplatesNextPage;

  const loadTeacherTemplates = () => {
    hasTeacherTemplatesNextPage && fetchTeacherTemplatesNextPage();
  };

  const loadAllTemplates = () => {
    hasAllTemplatesNextPage && fetchAllTemplatesNextPage();
  };

  const handleLoadMore = () => {
    activeTab === TEACHER_TEMPLATES ? loadTeacherTemplates() : loadAllTemplates();
  };

  const getTabClass = (tab: JourneyTemplateTab) => {
    return activeTab === tab ? 'selected-tab' : '';
  };

  const onTabClick = (tab: JourneyTemplateTab) => {
    setActiveTab(tab);
  };

  const getTemplatesFromPages = content => flatMap(content?.pages, 'data');

  const debounceCurrentSearch = useCallback(
    debounce(value => setCurrentSearch(value), 200),
    []
  );

  useEffect(() => {
    debounceCurrentSearch(search);
  }, [search]);

  useEffect(() => {
    setTemplates(activeTab === TEACHER_TEMPLATES ? getTemplatesFromPages(teacherTemplates) : getTemplatesFromPages(allTemplates));
  }, [teacherTemplates, allTemplates, activeTab]);

  return (
    <>
      <div className="journey-template-tabs d-flex justify-content-around">
        <span className={`px-2 pb-2 ${getTabClass(TEACHER_TEMPLATES)}`} onClick={() => onTabClick(TEACHER_TEMPLATES)}>
          {translate('schoolabyApp.journey.templates.myTemplatesTab')}
        </span>
        <span className={`px-2 pb-2 ${getTabClass(ALL_TEMPLATES)}`} onClick={() => onTabClick(ALL_TEMPLATES)}>
          {translate('schoolabyApp.journey.templates.allTemplatesTab')}
        </span>
      </div>
      <div className="templates flex-grow-1">
        {templates?.map((template: IJourney) => (
          <JourneyTemplateCard key={`template-${template.id}`} journey={template} />
        ))}
        {!!hasNextPage && !isFetchingTeacherTemplates && !isFetchingAllTemplates && (
          <p className="load-more-btn text-center cursor-pointer" onClick={handleLoadMore}>
            {translate('schoolabyApp.journey.home.loadMore')}
          </p>
        )}
      </div>
    </>
  );
};

const mapStateToProps = ({ authentication }: IRootState) => ({
  userId: authentication.account.id,
});

type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(TemplateList);
