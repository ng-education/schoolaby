import './list-footer-button.scss';
import React from 'react';
import { translate } from 'react-jhipster';
import { Link, useHistory } from 'react-router-dom';

interface ListFooterButtonInterface {
  style?: React.CSSProperties;
  link?: string;
  label: string;
  className?: string;
  onClick?: () => void;
}

const ListFooterButton = ({ className, link, label, onClick, style }: ListFooterButtonInterface) => {
  const history = useHistory();

  const onClickHandler = e => {
    if (onClick) {
      return onClick();
    }
    e.preventDefault();
    return history.push(link);
  };

  return (
    <Link to={link || '#'} className={'list-footer link'}>
      <footer className={className} style={style} onClick={onClickHandler}>
        <div>
          <div className={'plus-icon'}>+</div>
          {translate(label)}
        </div>
      </footer>
    </Link>
  );
};

export default ListFooterButton;
