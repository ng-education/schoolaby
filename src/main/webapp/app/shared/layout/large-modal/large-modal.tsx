import React, { FC } from 'react';
import { ModalProps } from 'reactstrap';
import './large-modal.scss';
import CustomModal from 'app/shared/layout/custom-modal/custom-modal';

const LargeModal: FC<ModalProps> = ({ modalClassName, className, contentClassName, children, ...otherProps }) => (
  <CustomModal
    scrollable={true}
    modalClassName={`large-modal-wrapping-element ${modalClassName ? modalClassName : ''}`}
    className={`large-modal mx-auto ${className ? className : ''}`}
    contentClassName={`large-modal-content ${contentClassName ? contentClassName : ''}`}
    {...otherProps}
  >
    {children}
  </CustomModal>
);

export default LargeModal;
