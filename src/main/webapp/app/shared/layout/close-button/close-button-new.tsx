import React, { FC } from 'react';
import { Button, ButtonProps } from 'reactstrap';
import Icon from 'app/shared/icons';
import { ICON_LIGHTER_GREY } from 'app/shared/util/color-utils';

interface AddButtonProps extends ButtonProps {
  onClick: React.MouseEventHandler<any>;
  className?: string;
  role?: string;
  crossWidth?: string;
  crossHeight?: string;
  crossColor?: string;
}

const CloseButtonNew: FC<AddButtonProps> = ({
  onClick,
  className,
  role,
  crossWidth,
  crossHeight,
  crossColor = ICON_LIGHTER_GREY,
  ...props
}) => (
  <Button
    color={crossColor || 'close'}
    className={`ml-auto modal-close ${className || ''}`}
    onClick={onClick}
    role={role || 'close-button'}
    {...props}
  >
    <Icon fill={crossColor} width={crossWidth} height={crossHeight} name={'close'} />
  </Button>
);

export default CloseButtonNew;
