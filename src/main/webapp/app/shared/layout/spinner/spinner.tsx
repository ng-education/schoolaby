import React from 'react';

export const Spinner = ({ loading = true, className = '', containerClassName = '' }) => {
  if (loading) {
    return (
      <div className={`d-flex justify-content-center align-items-center h-100 ${containerClassName}`}>
        <img alt="Schoolaby" className={`spinner ${className}`} src="content/images/schoolaby_spinner.gif" />
      </div>
    );
  } else {
    return null;
  }
};
