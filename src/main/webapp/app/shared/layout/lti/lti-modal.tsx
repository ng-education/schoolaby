import './lti-modal.scss';
import React, { useEffect, useRef, useState } from 'react';
import { Alert, Col, ModalBody, Row } from 'reactstrap';
import { getLtiResourceLaunchParameters, getLtiScores } from 'app/shared/services/lti-api';
import CloseButton from 'app/shared/layout/close-button/close-button';
import { Spinner } from 'app/shared/layout/spinner/spinner';
import { useLtiLaunchesState, useSelectedUserState } from 'app/shared/contexts/entity-detail-context';
import { ILtiResource } from 'app/shared/model/lti-resource.model';
import { isGradableLtiResource, isLtiAdvantage } from 'app/shared/util/lti-utils';
import { IRootState } from 'app/shared/reducers';
import { hasAnyAuthority } from 'app/shared/auth/private-route';
import { AUTHORITIES } from 'app/config/constants';
import { connect } from 'react-redux';
import { getAuthToken } from 'app/shared/util/auth-utils';
import { translate } from 'react-jhipster';
import CustomModal from 'app/shared/layout/custom-modal/custom-modal';

export interface ILtiModal extends StateProps {
  ltiResource?: ILtiResource;
  onClose: () => void;
  previewEnabled?: boolean;
}

export interface ILtiResourceParameters {
  lti_version: string;
  launch_url: string;
  oauth_nonce: string;
  oauth_signature: string;
  user_id: string;
  lti_message_type: string;
  roles: string;
  oauth_consumer_key: string;
  resource_link_id: string;
  oauth_timestamp: string;
  oauth_signature_method: string;
  oauth_version: string;
  message_type?: string;
}

/* eslint-disable @typescript-eslint/camelcase */
interface ILtiAdvantageLaunchParameters {
  launch_url: string;
  access_token: string;
  resourceId: number;
  studentId?: string;
}

export const LtiModal = ({ ltiResource, onClose, isStudent, previewEnabled }: ILtiModal) => {
  const [ltiParameters, setLtiParameters] = useState<ILtiAdvantageLaunchParameters | ILtiResourceParameters>();
  const formRef = useRef<HTMLFormElement>();
  const iframeRef = useRef<HTMLIFrameElement>();
  const [spinner, setSpinner] = useState(true);
  const { setLtiLaunches } = useLtiLaunchesState();
  const [ltiError, setLtiError] = useState<string>();
  const { selectedUser } = useSelectedUserState();

  useEffect(() => {
    if (iframeRef?.current) {
      iframeRef.current.addEventListener('load', () => setSpinner(false));
    }
  }, [iframeRef?.current]);

  useEffect(() => {
    if (ltiParameters) {
      iframeRef.current.addEventListener('load', () => setSpinner(false));
      formRef.current.submit();
    } else if (ltiError) {
      setSpinner(false);
    }
  }, [ltiParameters, ltiError]);

  useEffect(() => {
    if (isLtiAdvantage(ltiResource.ltiApp.version)) {
      setLtiParameters({
        launch_url: '/api/lti-advantage/launch',
        access_token: getAuthToken(),
        resourceId: ltiResource.id,
        studentId: selectedUser?.id ? selectedUser.id : null,
      });
    } else {
      getLtiResourceLaunchParameters({ ltiResourceId: ltiResource.id })
        .then(response => setLtiParameters(response.data))
        .catch(err => setLtiError(err?.response?.data?.detail));
    }
  }, []);

  const updateScores = (): void => {
    getLtiScores({ assignmentId: ltiResource.assignmentId }).then(response => setLtiLaunches(response.data));
  };

  const close = () => {
    if (isStudent && ltiParameters && isGradableLtiResource(ltiResource)) {
      updateScores();
    }
    onClose();
  };

  return (
    <>
      <Spinner loading={spinner} />
      <CustomModal backdrop={'static'} isOpen={true} className="modal-lg lti-content-modal">
        <div className={'p-1'}>
          <ModalBody className={'pl-4 pr-4 pb-4'}>
            <Row>
              <Col className={'d-flex'} xs={'9'} s={'10'}>
                <h2 className="font-weight-bold">{ltiResource.title}</h2>
              </Col>
              <Col className={'align-items-center d-flex'} xs={'3'} s={'9'}>
                <CloseButton onClick={close} />
              </Col>
            </Row>
            <Row className={'mt-4'}>
              <Col>
                {ltiError ? (
                  <Alert color="warning">{translate('schoolabyApp.ltiConfig.launchParamsFail') + ltiError}</Alert>
                ) : (
                  <iframe ref={iframeRef} name={'lti-content'} width={'100%'} style={{ height: '70vh' }} allowFullScreen />
                )}
              </Col>
            </Row>
          </ModalBody>
        </div>
        {!previewEnabled && (
          <form onSubmit={close} action={ltiParameters?.launch_url} method={'POST'} target="lti-content" ref={formRef}>
            {ltiParameters
              ? Object.entries(ltiParameters).map(
                  ([name, value]) => value && <input key={`input-${name}`} name={name} type={'hidden'} value={value} />
                )
              : null}
          </form>
        )}
      </CustomModal>
    </>
  );
};

const mapStateToProps = ({ authentication }: IRootState) => ({
  isStudent: hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.STUDENT]),
});

type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps, null)(LtiModal);
