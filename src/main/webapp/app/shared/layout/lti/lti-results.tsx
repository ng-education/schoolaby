import './lti-results.scss';
import React, { FC, memo, useEffect, useState } from 'react';
import { Table } from 'reactstrap';
import groupBy from 'lodash/groupBy';
import map from 'lodash/map';
import { translate } from 'react-jhipster';
import { ILtiScore } from 'app/shared/model/lti-score.model';
import LtiScoreRow from 'app/shared/layout/lti/lti-score-row';
import { subscribeToLtiScores, unsubscribeFromLtiScores } from 'app/config/websocket-middleware';
import { ILtiResource } from 'app/shared/model/lti-resource.model';

interface ILtiResults {
  scores: ILtiScore[];
  ltiResources: ILtiResource[];
}

const LtiResults: FC<ILtiResults> = ({ scores, ltiResources }) => {
  const [currentScores, setCurrentScores] = useState<ILtiScore[]>([]);
  const [socketMessage, setSocketMessage] = useState<ILtiScore>();

  const getScoresGroupedByResourceId = () => {
    return groupBy(currentScores, 'resourceId');
  };

  const handleSocketMessage = (score: ILtiScore) => {
    setSocketMessage(score);
  };

  useEffect(() => {
    subscribeToLtiScores(handleSocketMessage);

    return () => {
      unsubscribeFromLtiScores();
    };
  }, []);

  useEffect(() => setCurrentScores(scores), [scores]);

  const isScoreResourceInAssignmentResources = (score: ILtiScore) => {
    return ltiResources?.map(resource => resource.id).includes(score.resourceId);
  };

  const addScore = (score: ILtiScore) => {
    if (!isScoreResourceInAssignmentResources(score)) {
      return;
    }

    const newScores = [...currentScores];
    let existingPendingScoreIndex = null;
    newScores.forEach((currentScore, index) => {
      if (!currentScore.score && currentScore.resourceId === score.resourceId) {
        existingPendingScoreIndex = index;
      }
    });
    existingPendingScoreIndex !== null && newScores.splice(existingPendingScoreIndex, 1);
    setCurrentScores([score, ...newScores]);
  };

  useEffect(() => {
    if (socketMessage) {
      addScore(socketMessage);
    }
  }, [socketMessage]);

  return currentScores?.length ? (
    <>
      <hr />
      <div className={'lti-results-table w-100 pt-1'}>
        <Table borderless>
          <thead>
            <tr>
              <th className={'text-body text-center'}>{translate('schoolabyApp.submission.submittedLTI.asset')}</th>
              <th className={'text-body text-center'}>{translate('schoolabyApp.submission.submittedLTI.score')}</th>
              <th className={'text-body text-center'}>{translate('schoolabyApp.submission.submittedLTI.scoreDate')}</th>
              <th className={'text-body text-center'}>{translate('schoolabyApp.submission.submittedLTI.launches')}</th>
            </tr>
          </thead>
          <tbody>
            {currentScores?.length &&
              map(getScoresGroupedByResourceId(), ltiScores =>
                ltiScores.map((ltiScore, scoreIndex) => {
                  return <LtiScoreRow key={ltiScore.resourceId + ltiScore.createdDate} ltiScore={ltiScore} scoreIndex={scoreIndex} />;
                })
              )}
          </tbody>
        </Table>
        <hr />
      </div>
    </>
  ) : null;
};

export default memo(LtiResults);
