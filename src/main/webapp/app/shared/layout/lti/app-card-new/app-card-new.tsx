import React, { FC } from 'react';
import { Card, CardImg, Col, CardBody, UncontrolledTooltip } from 'reactstrap';
import './app-card-new.scss';
import { isLtiConfigured } from 'app/shared/util/lti-utils';
import Icon from 'app/shared/icons';
import { translate } from 'react-jhipster';

export interface AppCardNewProps {
  xs?: string | number;
  sm?: string | number;
  md?: string | number;
  lg?: string | number;
  xl?: string | number;
  onClick?: (ILtiApp) => void;
  imageSrc: string;
  title: string;
  description: string;
  consumerKey?: string;
  sharedSecret?: string;
  ltiVersion?: string;
  index?: number;
  configurationWarningsEnabled?: boolean;
}

const AppCardNew: FC<AppCardNewProps> = ({
  xs,
  sm,
  md,
  lg,
  xl,
  onClick,
  imageSrc,
  title,
  description,
  consumerKey,
  sharedSecret,
  ltiVersion,
  index,
  configurationWarningsEnabled = false,
}) => {
  return (
    <Col className="app-card-new-container pb-4" xs={xs} sm={sm} md={md} lg={lg} xl={xl}>
      <Card onClick={onClick} className="app-card-new">
        <CardImg top src={imageSrc} />
        <CardBody className="p-0 pt-4">
          <h3 className="pb-0">{title}</h3>
          <p>{description}</p>
        </CardBody>
      </Card>
      {configurationWarningsEnabled && !isLtiConfigured(ltiVersion, consumerKey, sharedSecret) && (
        <span className={'card-lti-warning position-absolute'}>
          <UncontrolledTooltip placement="top-start" target={`card-lti-warning-${index}`}>
            {translate('global.overlayWarning.ltiNotConfiguredWarning')}
          </UncontrolledTooltip>

          <span id={`card-lti-warning-${index}`}>
            <Icon className={'icon'} name={'attentionIcon'} width={'30px'} height={'30px'} />
          </span>
        </span>
      )}
    </Col>
  );
};

export default AppCardNew;
