import './lti-card.scss';
import React, { FC, memo, useState } from 'react';
import { convertAppDateTimeFromServer, orderDateTimes } from 'app/shared/util/date-utils';
import { ILtiScore } from 'app/shared/model/lti-score.model';
import { translate } from 'react-jhipster';
import { Tooltip } from 'reactstrap';

interface ILtiScoreRow {
  ltiScore: ILtiScore;
  scoreIndex: number;
}

const LtiScoreRow: FC<ILtiScoreRow> = ({ ltiScore, scoreIndex }: ILtiScoreRow) => {
  const [tooltipOpen, setTooltipOpen] = useState(false);
  const toggle = () => setTooltipOpen(!tooltipOpen);

  const convertLtiResultToPercentage = (result: string, resultMax: string): string => {
    if (!result) {
      return translate('schoolabyApp.assignment.lti.result.pending');
    } else if (resultMax) {
      return `${result} / ${resultMax}`;
    }
    return `${+result * 100}%`;
  };

  return (
    <tr key={`score-${ltiScore.resourceId.toString() + scoreIndex}`}>
      <td className={'text-secondary text-center'}>{scoreIndex === 0 && ltiScore.title}</td>
      <td className={'text-secondary text-center'}>{convertLtiResultToPercentage(ltiScore.score, ltiScore.scoreMax)}</td>
      <td className={'text-secondary text-center'}>{convertAppDateTimeFromServer(ltiScore.createdDate)}</td>
      <td className={'text-secondary text-center'}>
        <span id={`launches-${ltiScore.resourceId.toString() + scoreIndex}`}>{ltiScore.launches?.length}</span>
      </td>
      <Tooltip
        placement="bottom"
        isOpen={tooltipOpen}
        target={`launches-${ltiScore.resourceId.toString() + scoreIndex}`}
        boundariesElement={'window'}
        toggle={toggle}
      >
        <ul className={'block-list'}>
          {orderDateTimes(ltiScore.launches, 'desc')?.map(launch => (
            <li key={ltiScore.resourceId + launch}>{convertAppDateTimeFromServer(launch)}</li>
          ))}
        </ul>
      </Tooltip>
    </tr>
  );
};
export default memo(LtiScoreRow);
