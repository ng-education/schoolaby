import React, { FC } from 'react';
import { FieldRenderProps } from 'react-final-form';
import Icon from 'app/shared/icons';
import { DatePicker } from 'app/shared/form';
import './date-picker-new.scss';
import { DANGER } from 'app/shared/util/color-utils';
import { DATEPICKER_DATE_FORMAT, DATEPICKER_DATE_TIME_FORMAT, DatepickerDateTimeFormat } from 'app/shared/form/components/date-picker';
import { ReactDatePickerProps } from 'react-datepicker';

export interface DatePickerProps extends ReactDatePickerProps {
  label?: string;
  className?: string;
  dateTimeFormat?: DatepickerDateTimeFormat;
}

interface DatePickerFieldProps extends DatePickerProps, FieldRenderProps<any, any> {}

const DatePickerNew: FC<DatePickerFieldProps> = ({
  label,
  className = '',
  input,
  meta,
  id,
  dateTimeFormat = DATEPICKER_DATE_FORMAT,
  ...otherReactDatePickerProps
}) => {
  const isInvalid = meta.touched && meta.error;
  return (
    <div className={`date-picker-new ${className}`}>
      {label && <label htmlFor={id}>{label}</label>}
      <div className={'position-relative'}>
        <DatePicker
          meta={meta}
          input={input}
          id={id}
          endOfDay={dateTimeFormat !== DATEPICKER_DATE_TIME_FORMAT || !input.value}
          dateTimeFormat={dateTimeFormat}
          {...otherReactDatePickerProps}
        />
        <Icon
          name={'calendarNew'}
          width={'20'}
          height={'21'}
          fill={isInvalid ? DANGER : undefined}
          className={'position-absolute calendar-icon'}
        />
      </div>
    </div>
  );
};

export default DatePickerNew;
