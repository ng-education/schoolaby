import './remove-button.scss';
import React, { FC } from 'react';
import { Button, ButtonProps } from 'reactstrap';
import { ICON_WHITE } from 'app/shared/util/color-utils';
import Icon from 'app/shared/icons';

interface RemoveButtonProps extends ButtonProps {
  onClick: React.MouseEventHandler<any>;
  iconWidth?: string;
  iconHeight?: string;
  className?: string;
}

const RemoveButton: FC<RemoveButtonProps> = ({ onClick, className, iconWidth, iconHeight, ...other }) => {
  return (
    <div className={'d-flex align-items-center ' + (className || '')}>
      <div className="remove-btn-container">
        <Button
          className="btn-danger rounded-circle remove-btn p-0 m-0 d-flex justify-content-center align-items-center"
          type="button"
          onClick={onClick}
          {...other}
        >
          <Icon fill={ICON_WHITE} name={'cross'} width={iconWidth} height={iconHeight} />
        </Button>
      </div>
    </div>
  );
};

export default RemoveButton;
