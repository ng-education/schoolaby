import React, { FC } from 'react';

import './file-grid-item.scss';
import { IUploadedFile } from 'app/shared/model/uploaded-file.model';
import Icon, { getFileExtensionIcon } from 'app/shared/icons';
import {
  getFileTypeName,
  getGridItemClassName,
  isExcelDocument,
  isPdf,
  isPreviewableImg,
  isPreviewableVideo,
  isWordDocument,
} from 'app/shared/util/file-utils';
import { getFileUrl } from 'app/shared/services/uploaded-file-api';

interface FileGridItemProps {
  file: IUploadedFile;
  onClick?: () => void;
  overlay?: number;
}

const FileGridItem: FC<FileGridItemProps> = ({ file, onClick, overlay }) => {
  const isImageOrVideo = isPreviewableImg(file) || isPreviewableVideo(file);

  const ImageOrThumbnail = () =>
    isPreviewableImg(file) ? (
      <img src={getFileUrl(file?.uniqueName)} alt={file.originalName} role={'img-content'} />
    ) : (
      <div className={'thumbnail-container position-relative'}>
        <video src={getFileUrl(`${file?.uniqueName}#t=1`)} />
        <Icon className={'play-icon position-absolute center-vertically center-horizontally'} name={'play'} />
      </div>
    );

  return (
    <div
      data-testid={'file-grid-item'}
      className={`file-grid-item position-relative d-flex flex-column justify-content-between cursor-pointer ${getGridItemClassName(file)}`}
      onClick={() => onClick && onClick()}
    >
      {!isImageOrVideo ? (
        <>
          {getFileExtensionIcon(file.extension)}
          <div>
            <div className={'file-type-name'}>{getFileTypeName(file)}</div>
            <span className={'break'}>{file.originalName}</span>
          </div>
        </>
      ) : (
        <ImageOrThumbnail />
      )}
      {overlay ? (
        <div className="overlay w-100 h-100 position-absolute d-flex align-items-center justify-content-center">+ {overlay}</div>
      ) : null}
    </div>
  );
};

export default FileGridItem;
