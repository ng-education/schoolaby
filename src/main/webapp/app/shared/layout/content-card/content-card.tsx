import React, { FC } from 'react';
import './content-card.scss';

interface ContentCardProps {
  className?: string;
}

const ContentCard: FC<ContentCardProps> = ({ children, className }) => (
  <div className={`content-card bg-white p-4 ${className ? className : ''}`}>{children}</div>
);

export default ContentCard;
