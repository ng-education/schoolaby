import React, { useMemo } from 'react';
import { DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown } from 'reactstrap';
import { NavDropdown } from './menu-components';
import { languages, Locale, locales } from 'app/config/translation';
import { ICON_LIGHT_GREY } from 'app/shared/util/color-utils';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Icon from 'app/shared/icons';
import { translate } from 'react-jhipster';

export const LocaleMenu = ({ currentLocale, onClick }: { currentLocale: Locale; onClick: (e) => void }) =>
  Object.keys(languages).length > 1 ? (
    <span className="d-flex align-items-center">
      <FontAwesomeIcon icon="globe" size="lg" color={ICON_LIGHT_GREY} />
      <NavDropdown name={currentLocale?.toUpperCase()} caret>
        {locales.map(locale => (
          <DropdownItem key={locale} value={locale} onClick={onClick}>
            {locale.toUpperCase()}
          </DropdownItem>
        ))}
      </NavDropdown>
    </span>
  ) : null;

export const LocaleMenuWithFlags = ({ currentLocale, onClick }: { currentLocale: Locale; onClick: (locale) => void }) =>
  useMemo(
    () =>
      Object.keys(languages).length > 1 ? (
        <span className="d-flex align-items-center chat-bubble pl-2 pr-1">
          <UncontrolledDropdown nav inNavbar tag="div" aria-label={translate('global.locale')}>
            <DropdownToggle nav className="d-flex align-items-center pl-1 pr-2">
              {currentLocale && <Icon name={currentLocale} />}
              <Icon name="arrowDown" className="ml-2" />
            </DropdownToggle>
            <DropdownMenu right>
              {locales.map((locale: Locale) => (
                <DropdownItem key={locale} value={locale} onClick={() => onClick(locale)}>
                  <Icon name={locale} />
                </DropdownItem>
              ))}
            </DropdownMenu>
          </UncontrolledDropdown>
        </span>
      ) : null,
    [currentLocale]
  );
