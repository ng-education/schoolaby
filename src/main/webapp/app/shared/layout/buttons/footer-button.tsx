import './footer-button.scss';
import { FC } from 'react';
import React from 'react';
import { Button } from 'reactstrap';

type ButtonType = 'confirm' | 'cancel';

interface IFooterButton {
  onClick?: () => void;
  disabled?: boolean;
  label: string;
  type: ButtonType;
  className?: string;
}

const FooterButton: FC<IFooterButton> = ({ onClick, disabled = false, label, type, className = '' }) => {
  return (
    <Button
      onClick={onClick}
      className={`footer-button ${className}`}
      disabled={disabled}
      color={type === 'confirm' ? 'primary' : 'secondary'}
      type={onClick ? 'button' : 'submit'}
    >
      {label}
    </Button>
  );
};

export default FooterButton;
