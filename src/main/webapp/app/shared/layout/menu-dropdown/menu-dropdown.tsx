import './menu-dropdown.scss';
import React from 'react';
import { DropdownItem, DropdownMenu } from 'reactstrap';
import Icon from 'app/shared/icons';
import { useHistory } from 'react-router-dom';
import { translate } from 'react-jhipster';
import { IRootState } from 'app/shared/reducers';
import { hasAnyAuthority } from 'app/shared/auth/private-route';
import { AUTHORITIES } from 'app/config/constants';
import { connect } from 'react-redux';
import { AppContext } from 'app/app';
import { ASSIGNMENT, EntityType, JOURNEY, MILESTONE } from 'app/shared/util/entity-utils';
import { useDeleteJourney } from 'app/shared/services/journey-api';
import { useDeleteMilestone } from 'app/shared/services/milestone-api';
import { useDeleteAssignment } from 'app/shared/services/assignment-api';

interface MenuDropdownProps {
  entityId: number;
  journeyId?: number;
  entityType: EntityType;
  entityTitle: string;
  extraItems?: Array<{ title: string; onClick: React.MouseEventHandler<any>; className?: string }>;
  onEditClicked?: () => void;
  showDeleteButton?: boolean;
}

export const MenuDropdown = ({
  entityId,
  entityType,
  extraItems,
  journeyId,
  isAllowedToModify,
  onEditClicked,
  showDeleteButton = true,
  entityTitle,
}: StateProps & MenuDropdownProps) => {
  const EDIT_KEY = 'edit';
  const history = useHistory();
  const context = React.useContext(AppContext);
  const path = history.location.pathname.split('/');
  const deleteJourney = useDeleteJourney(+entityId);
  const deleteMilestone = useDeleteMilestone(+entityId);
  const deleteAssignment = useDeleteAssignment(+entityId);

  const getParameters = () => {
    const urlSearchParams = new URLSearchParams();
    journeyId && urlSearchParams.append('journeyId', journeyId?.toString());
    return urlSearchParams;
  };

  const deleteAction = () => {
    if (entityType === JOURNEY) {
      return deleteJourney.mutateAsync().then(() => history.push('/journey/'));
    } else if (entityType === ASSIGNMENT) {
      return deleteAssignment.mutateAsync().then(() => history.push(journeyId ? `/journey/${journeyId}` : '/journey/'));
    } else if (entityType === MILESTONE) {
      deleteMilestone.mutateAsync().then(() => history.push(journeyId ? `/journey/${journeyId}` : '/journey/'));
    }
  };

  const setIsOpen = () => {
    context.setModal({
      entityType,
      isOpen: true,
      entityId,
      journeyId,
      entityTitle,
      deleteAction,
    });
  };

  return isAllowedToModify && entityId ? (
    <DropdownMenu right key={`dropdown-${entityId}`}>
      {EDIT_KEY !== path[3] && (
        <DropdownItem
          onClick={e => {
            e.preventDefault();
            onEditClicked ? onEditClicked() : history.push(`/${entityType.toLowerCase()}/${entityId}/${EDIT_KEY}?${getParameters()}`);
          }}
          key="edit"
        >
          {translate('entity.action.edit')}
        </DropdownItem>
      )}

      {extraItems?.map(item => (
        <DropdownItem
          key={item.title}
          className={item?.className}
          onClick={e => {
            e.preventDefault();
            item.onClick(e);
          }}
        >
          {item.title}
        </DropdownItem>
      ))}
      {showDeleteButton && (
        <>
          <DropdownItem divider />
          <DropdownItem
            onClick={e => {
              e.preventDefault();
              setIsOpen();
            }}
            key={`delete-${entityId}`}
            className="d-flex align-items-center justify-content-between"
          >
            {translate('entity.action.delete')}
            <Icon name={'trashNew'} className={'ml-2'} width={'18px'} height={'18px'} />
          </DropdownItem>
        </>
      )}
    </DropdownMenu>
  ) : null;
};

const mapStateToProps = ({ authentication }: IRootState) => ({
  isAllowedToModify: hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.TEACHER, AUTHORITIES.ADMIN]),
});

type StateProps = ReturnType<typeof mapStateToProps>;
export default connect(mapStateToProps)(MenuDropdown);
