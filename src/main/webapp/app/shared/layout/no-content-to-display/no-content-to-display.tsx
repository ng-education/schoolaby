import React, { FC } from 'react';
import './no-content-to-display.scss';
import AddButtonNew from 'app/shared/layout/add-button-new/add-button-new';

interface NoContentToDisplayProps {
  action?: () => void;
  actionTitle?: string;
  message: string;
}

const NoContentToDisplay: FC<NoContentToDisplayProps> = ({ action, actionTitle, message }) => (
  <div className="no-content-to-display bg-white d-flex flex-column align-items-center justify-content-center p-4">
    <img src="/content/images/no_journeys_to_display.svg" alt={message} />
    <h5 className="pb-4 text-center">{message}</h5>
    {action && <AddButtonNew title={actionTitle} onClick={action} className="px-3" />}
  </div>
);

export default NoContentToDisplay;
