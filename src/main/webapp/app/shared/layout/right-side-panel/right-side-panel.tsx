import React, { FC } from 'react';
import './right-side-panel.scss';
import CloseButtonNew from 'app/shared/layout/close-button/close-button-new';

interface RightSidePanelProps {
  title: string;
  closePanel: () => void;
  className?: string;
}

const RightSidePanel: FC<RightSidePanelProps> = ({ title, closePanel, className, children }) => (
  <div className="right-side-panel position-fixed bg-white px-4 py-3 h-100">
    <div className={`d-flex align-items-center justify-content-between mb-3 ${className ? className : ''}`}>
      <h5 className="panel-title mb-0">{title}</h5>
      <CloseButtonNew onClick={closePanel} crossWidth={'20px'} crossHeight={'20px'} className="p-0" />
    </div>
    {children}
  </div>
);

export default RightSidePanel;
