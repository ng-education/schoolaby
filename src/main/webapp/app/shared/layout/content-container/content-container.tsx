import React from 'react';

import { Spinner } from 'app/shared/layout/spinner/spinner';
import './content-container.scss';

const ContentContainer = ({ children, spinner, className = '' }) => (
  <div className={`${className} content-container full-height`}>{spinner ? <Spinner /> : children}</div>
);

export default ContentContainer;
