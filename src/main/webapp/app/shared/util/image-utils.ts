import random from 'lodash/random';

export const getRandomImage = () => (random() < 0.5 ? '/content/images/material_green.png' : '/content/images/material_blue.png');
