import { getSubjectIcon } from 'app/shared/icons';
import { IJourney } from 'app/shared/model/journey.model';
import { IJourneyStudent } from 'app/shared/model/journey-student.model';
import { getFullFileUrl } from 'app/shared/services/uploaded-file-api';

type SchoolImage = {
  imageUrl: string;
  schoolName: string;
};

export const getHeadingIcon = (journeyEntity: IJourney) => {
  return journeyEntity?.subject ? getSubjectIcon(journeyEntity.subject.label, '30px', '38px') : getSubjectIcon('default', '30px', '38px');
};

export const getJourneyStudentsUsers = (journeyStudents?: IJourneyStudent[]) => journeyStudents?.map(student => student.user) || [];

export const getSchoolImage = (journeyEntity: IJourney): SchoolImage | null => {
  const schoolsWithImages = journeyEntity?.schools?.filter(school => !!school.uploadedFile);
  return schoolsWithImages?.length > 0
    ? {
        imageUrl: getFullFileUrl(schoolsWithImages[0]?.uploadedFile?.uniqueName),
        schoolName: schoolsWithImages[0]?.name,
      }
    : null;
};
