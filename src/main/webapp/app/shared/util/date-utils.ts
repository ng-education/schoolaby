import { DateTime, Interval } from 'luxon';

export const monthSW = [
  'Januari',
  'Februari',
  'Machi',
  'Aprili',
  'Mei',
  'Juni',
  'Julai',
  'Agosti',
  'Septemba',
  'Oktoba',
  'Novemba',
  'Desemba',
];
export const daySW = ['Jpl', 'Jtat', 'Jnne', 'Jtan', 'Alh', 'Ijm', 'Jmos'];

export const convertDateTimeFromServer = date => (date ? DateTime.fromISO(date).toISO() : null);

export const convertDateTimeToServer = date => (date ? DateTime.fromISO(date).toUTC().toISO() : null);

export const convertAppDateTimeFromServer = date =>
  date ? DateTime.fromISO(date).setLocale('et').toLocaleString(DateTime.DATETIME_SHORT) : null;

export const getStartOfDay = () => DateTime.utc().startOf('day').toISO();

export const getEndOfDay = () => DateTime.utc().setZone('local', { keepLocalTime: true }).endOf('day').toISO();

export const getFormattedDate = (date: string, format: string, locale?: string): string => {
  return DateTime.fromISO(date)
    .setLocale(locale ? locale : 'et')
    .toLocaleString(format);
};

export const formatToStartOfDay = (d: DateTime) => {
  return d.startOf('day');
};

export const orderDateTimes = (dateTimes: string[], order: 'asc' | 'desc'): string[] => {
  return dateTimes.slice().sort((a, b) => {
    const aDateTime = order === 'asc' ? DateTime.fromISO(a) : DateTime.fromISO(b);
    const bDateTime = order === 'asc' ? DateTime.fromISO(b) : DateTime.fromISO(a);
    return aDateTime < bDateTime ? -1 : aDateTime > bDateTime ? 1 : 0;
  });
};

export const days = (interval: Interval) => {
  const dayDateTimes: DateTime[] = [];
  let cursor = interval.start.startOf('day');
  while (cursor < interval.end) {
    dayDateTimes.push(cursor);
    cursor = cursor.plus({ days: 1 });
  }
  return dayDateTimes;
};
