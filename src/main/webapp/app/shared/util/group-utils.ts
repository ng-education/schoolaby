import { IPerson } from 'app/shared/model/person.model';
import { IGroup } from 'app/shared/model/group.model';
import flatMap from 'lodash/flatMap';
import differenceBy from 'lodash/differenceBy';
import find from 'lodash/find';

export const getUngroupedStudents = (students: IPerson[], groups: IGroup[]) => {
  const groupedStudents = flatMap(groups, 'students');
  return differenceBy(students, groupedStudents, 'id');
};

export const getGroupByStudentId = (studentId: number, groups: IGroup[]) => find(groups, { students: [{ id: studentId }] });
