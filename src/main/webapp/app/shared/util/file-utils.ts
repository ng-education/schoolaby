import { IUploadedFile } from 'app/shared/model/uploaded-file.model';
import { DateTime } from 'luxon';
import { translate } from 'react-jhipster';

export const withHttp = url => (!/^https?:\/\//i.test(url) ? `http://${url}` : url);

export const getExtension = (url: string): string => {
  if (url) {
    const splitArr = new URL(withHttp(url)).pathname.split('.');
    if (splitArr.length > 1) {
      return splitArr[splitArr.length - 1].toLowerCase();
    }
  }
  return null;
};

const imageExtensions = ['apng', 'avif', 'gif', 'jpg', 'jpeg', 'jfif', 'pjpeg', 'pjp', 'png', 'svg', 'webp', 'tif', 'tiff', 'bmp'];
const videoExtensions = ['mp4', 'webm', 'ogg', 'mkv'];
const audioExtensions = ['ogg', 'mp3', 'wav'];
const wordDocumentExtensions = ['docx', 'doc'];
const excelDocumentExtensions = ['xlsx', 'xls'];

export const isPreviewableImg = (uploadedFile: IUploadedFile) => {
  return imageExtensions.includes(uploadedFile.extension?.toLowerCase());
};

export const isPreviewableVideo = (uploadedFile: IUploadedFile) => {
  return videoExtensions.includes(uploadedFile.extension?.toLowerCase());
};

export const isPreviewableAudio = (uploadedFile: IUploadedFile) => {
  return audioExtensions.includes(uploadedFile.extension?.toLowerCase());
};

export const isWordDocument = (uploadedFile: IUploadedFile) => {
  return wordDocumentExtensions.includes(uploadedFile.extension?.toLowerCase());
};

export const isExcelDocument = (uploadedFile: IUploadedFile) => {
  return excelDocumentExtensions.includes(uploadedFile.extension?.toLowerCase());
};

export const isPdf = (uploadedFile: IUploadedFile) => {
  return uploadedFile.extension?.toLowerCase() === 'pdf';
};

export const isPreviewableFile = (file: IUploadedFile) => {
  return isPreviewableImg(file) || isPreviewableVideo(file) || isPreviewableAudio(file) || ['pdf'].includes(file.extension?.toLowerCase());
};

export const getFileTypeName = (uploadedFile: IUploadedFile) => {
  if (isWordDocument(uploadedFile)) {
    return translate('schoolabyApp.uploadedFile.extensionName.word');
  } else if (isExcelDocument(uploadedFile)) {
    return translate('schoolabyApp.uploadedFile.extensionName.excel');
  } else if (isPdf(uploadedFile)) {
    return translate('schoolabyApp.uploadedFile.extensionName.pdf');
  } else {
    return translate('schoolabyApp.uploadedFile.extensionName.generic');
  }
};

export const createDownloadLink = response => {
  const blob = new Blob([response.data], { type: 'application/octet-stream' });
  const link = document.createElement('a');
  link.href = window.URL.createObjectURL(blob);
  return link;
};

export const generateFileName = (prefix: string, extension?: string) => {
  return `${prefix}_${DateTime.local().toFormat('yy-MM-dd_HH-mm-ss')}.${extension || ''}`;
};

export const getGridItemClassName = (file: IUploadedFile) => {
  if (isPreviewableImg(file)) {
    return 'image';
  } else if (isPreviewableVideo(file)) {
    return 'video';
  } else if (isWordDocument(file)) {
    return 'word';
  } else if (isExcelDocument(file)) {
    return 'excel';
  } else if (isPdf(file)) {
    return 'pdf';
  } else {
    return 'generic';
  }
};
