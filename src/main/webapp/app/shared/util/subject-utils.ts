const subjectMapping = {
  'schoolabyApp.subject.estonian': 'Estonian',
  'schoolabyApp.subject.russian': 'Russian',
  'schoolabyApp.subject.englishForeign': 'English',
  'schoolabyApp.subject.russianForeign': 'Russian',
  'schoolabyApp.subject.estonianForeign': 'Estonian',
  'schoolabyApp.subject.mathematics': 'Mathematics',
  'schoolabyApp.subject.natureEducation': 'ScienceAndHumanEducation',
  'schoolabyApp.subject.biology': 'Biology',
  'schoolabyApp.subject.geography': 'Geography',
  'schoolabyApp.subject.physics': 'Physics',
  'schoolabyApp.subject.chemistry': 'Chemistry',
  'schoolabyApp.subject.history': 'History',
  'schoolabyApp.subject.music': 'Music',
  'schoolabyApp.subject.art': 'Art',
  'schoolabyApp.subject.technology': 'Technology',
  'schoolabyApp.subject.physicalEducation': 'PhysicalEducation',
  'schoolabyApp.subject.literature': 'Literature',
  'schoolabyApp.subject.economics': 'EconomyAndBusiness',
  'schoolabyApp.subject.informatics': 'Informatics',
  'schoolabyApp.subject.other': 'Varia',
};

export const getOpiqSubject = (key: string) => {
  return subjectMapping[key] || null;
};
