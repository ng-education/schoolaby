import { ENTITY_TYPE_ASSIGNMENT } from 'app/shared/services/lti-api';
import { LTI_MATERIAL, Material } from 'app/shared/model/material.model';
import { IAssignment } from 'app/shared/model/assignment.model';
import { IMilestone } from 'app/shared/model/milestone.model';
import { ILtiResource } from 'app/shared/model/lti-resource.model';

export const isGradableLtiResource = ltiResource => ltiResource.entityType === ENTITY_TYPE_ASSIGNMENT && ltiResource.syncGrade === true;

export const isLtiAdvantage = (version: string): boolean => {
  return version === '1.3.0';
};

export const isLtiConfigured = (version: string, consumerKey: string, sharedSecret: string) => {
  return isLtiAdvantage(version) || !!consumerKey || !!sharedSecret;
};

export const mergeMaterials = (entity: IAssignment | IMilestone, ltiResources: ILtiResource[]): Material[] => {
  const materials = entity?.materials?.map(material => Material.instantiate(material)) || [];
  const existingResources = materials.filter(material => material.type === LTI_MATERIAL).map(material => material.ltiResource);
  const resources = ltiResources?.filter(resource => !existingResources.includes(resource)).map(value => Material.fromLtiResource(value));

  const mergedMaterials = materials
    .concat(resources)
    .filter(material => material !== undefined)
    .filter(material => (material.type === LTI_MATERIAL ? !!material.ltiResource : true));
  return entity?.materials?.length || ltiResources?.length ? mergedMaterials : [];
};
