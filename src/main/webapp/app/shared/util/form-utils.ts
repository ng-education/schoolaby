import { ARRAY_ERROR, ValidationErrors } from 'final-form';
import noop from 'lodash/noop';
import set from 'lodash/set';
import toPath from 'lodash/toPath';
import * as yup from 'yup';
import get = Reflect.get;

const createEmptyArrays = <T>(object: ValidationErrors, pathString: string, schema) =>
  toPath(pathString)
    .map((_element, index, array) => array.slice(0, index))
    .map(path => ({
      path,
      schema: yup.reach(schema, path.join('.')),
    }))
    .filter(({ schema: aSchema }) => aSchema instanceof yup.array)
    .forEach(({ path }) => {
      const array = [];
      array[ARRAY_ERROR] = [];
      set(object, path, array);
    });

export const createYupValidator = <T>(schema) => (values: T): ValidationErrors | Promise<ValidationErrors> | undefined =>
  schema
    .validate(values, { abortEarly: false })
    .then(noop)
    .catch((error: yup.ValidationError) => {
      const formErrors: ValidationErrors = {};

      error.inner.forEach(innerError => createEmptyArrays(formErrors, innerError.path, schema));

      error.inner
        .filter(innerError => innerError.type !== ARRAY_ERROR)
        .forEach(innerError => set(formErrors, innerError.path, innerError.message));

      error.inner
        .filter(innerError => innerError.type === ARRAY_ERROR)
        .forEach(innerError => {
          const originalValue: any = get(formErrors, innerError.path);
          const value = originalValue || [];

          if (!(originalValue instanceof Array)) {
            set(formErrors, innerError.path, value);
          }

          value[ARRAY_ERROR] = value[ARRAY_ERROR] instanceof Array ? value[ARRAY_ERROR] : [];
          value[ARRAY_ERROR].push(innerError.message);
        });

      if (process.env.NODE_ENV === 'development') {
        // eslint-disable-next-line no-console
        console.log(formErrors);
      }

      return formErrors;
    });
