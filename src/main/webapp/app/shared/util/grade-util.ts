import { findColorByGradingSchemeValue, RED } from 'app/shared/util/color-utils';
import { IGradingSchemeValue } from 'app/shared/model/grading-scheme-value.model';
import { translate } from 'react-jhipster';
import { ISubmissionFeedback } from 'app/shared/model/submission/submission-feedback.model';
import { IGradingScheme, PASS_FAIL, PERCENTAGE_0_100 } from 'app/shared/model/grading-scheme.model';

export const FAILED_GRADE_STYLE = 'rounded border border-danger light-danger-bg text-danger';

export const getClassNameForGrade = (gradingSchemeValue: IGradingSchemeValue) => {
  if (findColorByGradingSchemeValue(gradingSchemeValue) === RED) {
    return FAILED_GRADE_STYLE;
  }
};

export const getGrade = (feedback: ISubmissionFeedback) => {
  if (feedback?.grade === 'PASS' || feedback?.grade === 'FAIL') {
    return translate(`schoolabyApp.gradingSchemeValue.${feedback?.grade.toLowerCase()}`);
  }
  return feedback?.grade;
};

export const getGradingScore = (gradingScheme: IGradingScheme, submissionFeedback: ISubmissionFeedback) => {
  if (gradingScheme?.code === PASS_FAIL) {
    return submissionFeedback?.grade ? getGrade(submissionFeedback) : undefined;
  }
  if (gradingScheme?.code === PERCENTAGE_0_100) {
    return submissionFeedback?.grade ? `${submissionFeedback.grade}%` : undefined;
  }
  return submissionFeedback?.grade;
};
