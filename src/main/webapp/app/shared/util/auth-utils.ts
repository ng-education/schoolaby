import { Storage } from 'react-jhipster';
import jwtDecode from 'jwt-decode';
import { AUTHORITIES } from 'app/config/constants';
import Cookies from 'js-cookie';

const AUTH_TOKEN_KEY = 'jhi-authenticationToken';

export const getAuthToken = () => Storage.local.get(AUTH_TOKEN_KEY) || Storage.session.get(AUTH_TOKEN_KEY);

export const setAuthToken = (result: any) => {
  const bearerToken = result?.value?.headers?.authorization;
  if (bearerToken && bearerToken.slice(0, 7) === 'Bearer ') {
    const jwt = bearerToken.slice(7, bearerToken.length);
    Storage.local.set(AUTH_TOKEN_KEY, jwt);
  }
};

interface JWT {
  exp: number;
}

export const hasValidJWT = (): boolean => {
  const jwt = Storage.local.get('jhi-authenticationToken') || Cookies.get('jhi-authenticationToken');
  if (jwt) {
    const decodedJWT: JWT = jwtDecode(jwt);
    return decodedJWT.exp >= Math.round(Date.now() / 1000);
  }
  return false;
};

export const getRoleBasedOnAuthority = (authority: string) => {
  if (authority === AUTHORITIES.TEACHER) {
    return 'TEACHER';
  }
  if (authority === AUTHORITIES.STUDENT) {
    return 'STUDENT';
  }
  return 'TEACHER';
};
