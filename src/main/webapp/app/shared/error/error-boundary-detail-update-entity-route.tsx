import React from 'react';
import { Route, RouteProps } from 'react-router-dom';
import ErrorBoundary from 'app/shared/error/error-boundary';
import { EntityDetailProvider } from 'app/shared/contexts/entity-detail-context';
import { MaterialProvider } from 'app/shared/contexts/material-context';
import { EntityUpdateProvider } from 'app/shared/contexts/entity-update-context';

const ErrorBoundaryDetailUpdateEntityRoute = ({ component: Component, ...rest }: RouteProps) => {
  const encloseInErrorBoundary = props => (
    <ErrorBoundary>
      <EntityDetailProvider>
        <EntityUpdateProvider>
          <MaterialProvider>
            <Component {...props} />
          </MaterialProvider>
        </EntityUpdateProvider>
      </EntityDetailProvider>
    </ErrorBoundary>
  );

  if (!Component) throw new Error(`A component needs to be specified for path ${(rest as any).path}`);

  return <Route {...rest} render={encloseInErrorBoundary} />;
};

export default ErrorBoundaryDetailUpdateEntityRoute;
