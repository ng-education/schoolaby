import React from 'react';
import { Route, RouteProps } from 'react-router-dom';
import ErrorBoundary from 'app/shared/error/error-boundary';
import { EntityDetailProvider } from 'app/shared/contexts/entity-detail-context';
import { MaterialProvider } from 'app/shared/contexts/material-context';

const ErrorBoundaryDetailEntityRoute = ({ component: Component, ...rest }: RouteProps) => {
  const encloseInErrorBoundary = props => (
    <ErrorBoundary>
      <EntityDetailProvider>
        <MaterialProvider>
          <Component {...props} />
        </MaterialProvider>
      </EntityDetailProvider>
    </ErrorBoundary>
  );

  if (!Component) throw new Error(`A component needs to be specified for path ${(rest as any).path}`);

  return <Route {...rest} render={encloseInErrorBoundary} />;
};

export default ErrorBoundaryDetailEntityRoute;
