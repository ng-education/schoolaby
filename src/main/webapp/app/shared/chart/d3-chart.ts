import { SECONDARY } from 'app/shared/util/color-utils';
import * as d3 from 'd3';
import { translate } from 'react-jhipster';

export const D3Chart = {
  clearCanvas(svg) {
    svg.selectAll('*').remove();
  },
  createXAxis(svg, xScale, height, xAxisPadding, paddingLeft, xAxisLegendHeight) {
    const xAxis = d3.axisBottom(xScale).tickSize(0);
    return svg
      .append('g')
      .attr('transform', `translate(${xAxisPadding + paddingLeft}, ${height + xAxisLegendHeight})`)
      .attr('class', 'x axis')
      .call(xAxis)
      .style('font-size', '16px')
      .style('color', SECONDARY);
  },
  createXScale(xValues, width) {
    return d3.scaleBand().domain(xValues).range([0, width]);
  },
  createYScale(maxY, height, xAxisLegendHeight) {
    return d3
      .scaleLinear()
      .domain([0, maxY])
      .range([0, height - xAxisLegendHeight]);
  },
  createCanvas(svg, paddingLeft, margin, data) {
    return svg
      .append('g')
      .attr('transform', `translate(${paddingLeft + margin.left}, ${margin.top})`)
      .selectAll('rect')
      .data(data)
      .enter()
      .append('g');
  },
  createBars(canvas, barWidth, barPadding, yScale, height, xAxisLegendHeight, fillCallback) {
    return canvas
      .append('rect')
      .attr('rx', 10)
      .attr('ry', 10)
      .attr('width', barWidth)
      .attr('height', d => yScale(d.value))
      .attr('x', (d, i) => i * (barWidth + barPadding))
      .attr('y', d => height - xAxisLegendHeight - yScale(d.value))
      .attr('fill', fillCallback);
  },
  createBarNumbers(canvas, barWidth, barPadding, height, xAxisLegendHeight, yScale) {
    return canvas
      .append('text')
      .attr('x', (d, i) => i * (barWidth + barPadding) + barWidth / 2)
      .attr('y', d => height - xAxisLegendHeight - yScale(d.value) / 2 + 6)
      .attr('aria-label', translate('schoolabyApp.assignment.analytics.barChart.barNumberLabel'))
      .text(d => d.value)
      .style('font-size', '16px')
      .style('fill', 'white')
      .style('text-anchor', 'middle');
  },
  dottedXAxisStyling(svg, data) {
    svg.select('.x.axis path').style('stroke-width', '3px').style('color', '#E5E5E5');
    svg.selectAll('.x.axis text').style('font-size', '12px').attr('y', '30');
    svg.selectAll('.tick').data(data).append('circle').attr('r', 6).style('fill', '#C3C3C3');
  },
  scrollable(svg, width, height) {
    svg.attr('viewBox', `0,0,${width},${height + 80}`);
  },
  getCenterAlignedPadding(bounds, barWidth, barPadding, pointCount) {
    return (bounds.width - (barWidth + barPadding) * pointCount) / 2;
  },
  getBounds(svg) {
    return svg.node().getBoundingClientRect();
  },
};
