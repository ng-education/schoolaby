import { IAssignment } from 'app/shared/model/assignment.model';
import { IMessage } from 'app/shared/model/message.model';
import { Material } from 'app/shared/model/material.model';
import { IEducationalAlignment } from 'app/shared/model/educational-alignment.model';
import { EntityState } from 'app/shared/model/enumerations/entity-state';

export interface IMilestone {
  id?: number;
  title?: string;
  description?: string;
  endDate?: string;
  deleted?: string;
  journeyId?: number;
  creatorId?: number;
  hidden?: boolean;
  state?: EntityState;
  assignments?: IAssignment[];
  materials?: Material[];
  messages?: IMessage[];
  educationalAlignments?: IEducationalAlignment[];
  published?: string;
}

export const defaultValue: Readonly<IMilestone> = {};
