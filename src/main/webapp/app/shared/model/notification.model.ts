import ISubject from 'app/shared/model/subject.model';

export interface INotification {
  id?: number;
  message?: string;
  creatorName?: string;
  createdDate?: string;
  journeyName?: string;
  journeyId?: number;
  assignmentName?: string;
  groupName?: string;
  educationalAlignmentTargetNames?: string[];
  link?: string;
  read?: string;
  subject?: ISubject;
}

export const defaultValue: Readonly<INotification> = {};
