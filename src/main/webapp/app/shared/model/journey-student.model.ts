import { IUser } from 'app/shared/model/user.model';

export interface IJourneyStudent {
  user?: IUser;
  pinnedDate?: string;
  averageGrade?: number;
}

export const mapToUserIdList = (journeyStudents: IJourneyStudent[]) => journeyStudents?.map(student => student.user.id);
