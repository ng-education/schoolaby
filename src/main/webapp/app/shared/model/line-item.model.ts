export interface ILineItem {
  id?: number;
  scoreMaximum: number;
  label?: string;
  tag?: string;
}
