import { ICriterion } from 'app/shared/model/rubric/criterion.model';

export interface IRubric {
  id?: number;
  title: string;
  assignmentId?: number;
  criterions: ICriterion[];
  isTemplate: boolean;
  maxPoints?: number;
  minPoints?: number;
  createdBy?: string;
}
