import { ICriterionLevel } from 'app/shared/model/rubric/criterion-level.model';

export interface ICriterion {
  id?: number;
  title: string;
  description: string;
  sequenceNumber: number;
  levels: ICriterionLevel[];
  maxPoints?: number;
  minPoints?: number;
}
