export interface IAuditEvent {
  principal?: string;
  timestamp?: string;
  type?: string;
  data?: {
    userId: string;
  };
}
