import { IJourney } from 'app/shared/model/journey.model';
import { IMilestone } from 'app/shared/model/milestone.model';
import { IMaterial } from 'app/shared/model/material.model';
import { IAssignment } from 'app/shared/model/assignment.model';
import ISubject from 'app/shared/model/subject.model';

export interface IEducationalAlignment {
  id?: number;
  title?: string;
  alignmentType?: string;
  country?: string;
  targetName?: string;
  targetUrl?: string;
  startDate?: string;
  endDate?: string;
  deleted?: string;
  children?: IEducationalAlignment[];
  journeys?: IJourney[];
  milestones?: IMilestone[];
  materials?: IMaterial[];
  assignments?: IAssignment[];
  parents?: IEducationalAlignment[];
  taxonId?: number;
  taxonIds?: number[];
  subject?: ISubject;
}

export const defaultValue: Readonly<IEducationalAlignment> = {};
