interface IEKoolikottText {
  language: string;
  text: string;
}

export interface IEKoolikottMaterial extends IEKoolikottReducedMaterial {
  description: string;
  added: string;
  thumbnailName: string;
}

export interface IEKoolikottReducedMaterial {
  id: number;
  title: string;
  type: string;
}
