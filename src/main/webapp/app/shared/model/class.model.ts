import { IPerson } from 'app/shared/model/person.model';

export interface IClass {
  students: IPerson[];
  name: string;
  timeRange: string;
}
