export interface IMessage {
  id?: number;
  value?: string;
  creator?: string;
  createdDate?: string;
  creatorId?: number;
  chatId?: number;
  feedback?: boolean;
}

export const defaultValue: Readonly<IMessage> = {};
