import ISubject from 'app/shared/model/subject.model';

export interface IMessageView {
  messageValue?: string;
  authorName?: string;
  messageCreatedDate?: string;
  journeyName?: string;
  assignmentName?: string;
  subject?: ISubject;
  urlToChat?: string;
  read?: string;
}

export const defaultValue: Readonly<IMessageView> = {};
