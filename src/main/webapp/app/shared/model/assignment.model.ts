import { Material } from 'app/shared/model/material.model';
import { IEducationalAlignment } from 'app/shared/model/educational-alignment.model';
import { ILtiResource } from 'app/shared/model/lti-resource.model';
import { EntityState } from 'app/shared/model/enumerations/entity-state';
import { IRubric } from 'app/shared/model/rubric/rubric.model';
import { IGroup } from 'app/shared/model/group.model';
import { IUser } from 'app/shared/model/user.model';

export interface IAssignment {
  id?: number;
  title?: string;
  description?: string;
  deadline?: string;
  flexibleDeadline?: boolean;
  deleted?: string;
  state?: EntityState;
  hasGrades?: boolean;
  gradingSchemeId?: number;
  milestoneId?: number;
  journeyId?: number;
  creatorId?: number;
  hidden?: boolean;
  highestGrade?: boolean;
  requiresSubmission?: boolean;
  materials?: Material[];
  students?: IUser[];
  ltiResources?: ILtiResource[];
  educationalAlignments?: IEducationalAlignment[];
  teacherName?: string;
  submissionsCount?: number;
  rubric?: IRubric;
  subject?: string;
  groups?: IGroup[];
  gradeGroupMembersIndividually?: boolean;
  published?: string;
  previousAssignmentId?: number;
  nextAssignmentId?: number;
}

export const defaultValue: Readonly<IAssignment> = {
  flexibleDeadline: false,
};
