import { ISchool } from 'app/shared/model/school.model';

export interface IPersonRole {
  id?: number;
  role?: string;
  active?: boolean;
  startDate?: string;
  endDate?: string;
  grade?: string;
  parallel?: string;
  deleted?: string;
  personId?: number;
  school?: ISchool;
}

export const defaultValue: Readonly<IPersonRole> = {
  active: false,
};
