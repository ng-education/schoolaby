export interface ILtiScore {
  title: string;
  resourceId: number;
  score?: string;
  scoreMax?: string;
  createdDate?: string;
  launches: string[];
}
