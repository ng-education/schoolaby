export interface ICurriculum {
  id?: number;
  title?: string;
  country?: string;
  sequenceNumber?: number;
  requiresSubject?: boolean;
}

export const defaultValue: Readonly<ICurriculum> = {};
