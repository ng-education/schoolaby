import { combineReducers } from 'redux';
import { loadingBarReducer as loadingBar } from 'react-redux-loading-bar';
import locale, { LocaleState } from './locale';
import authentication, { AuthenticationState } from './authentication';
import applicationProfile, { ApplicationProfileState } from './application-profile';
import administration, { AdministrationState } from 'app/administration/administration.reducer';
import userManagement, { UserManagementState } from 'app/administration/user-management/user-management.reducer';
import register, { RegisterState } from 'app/account/register/register.reducer';
import activate, { ActivateState } from 'app/account/activate/activate.reducer';
import password, { PasswordState } from 'app/account/password/password.reducer';
import settings, { SettingsState } from 'app/account/settings/settings.reducer';
import passwordReset, { PasswordResetState } from 'app/account/password-reset/password-reset.reducer';
import milestone, { MilestoneState } from 'app/milestone/milestone.reducer';
import gradingScheme, { GradingSchemeState } from 'app/administration/entities/grading-scheme/grading-scheme.reducer';
import gradingSchemeValue, { GradingSchemeValueState } from 'app/administration/entities/grading-scheme-value/grading-scheme-value.reducer';
import material, { MaterialState } from 'app/material/material.reducer';
import assignment, { AssignmentState } from 'app/assignment/assignment.reducer';
import submission, { SubmissionState } from 'app/submission/submission.reducer';
import educationalAlignment, {
  EducationalAlignmentState,
} from 'app/administration/entities/educational-alignment/educational-alignment.reducer';
import personRole, { PersonRoleState } from 'app/administration/entities/person-role/person-role.reducer';

export interface IRootState {
  readonly authentication: AuthenticationState;
  readonly locale: LocaleState;
  readonly applicationProfile: ApplicationProfileState;
  readonly administration: AdministrationState;
  readonly userManagement: UserManagementState;
  readonly register: RegisterState;
  readonly activate: ActivateState;
  readonly passwordReset: PasswordResetState;
  readonly password: PasswordState;
  readonly settings: SettingsState;
  readonly milestone: MilestoneState;
  readonly gradingScheme: GradingSchemeState;
  readonly gradingSchemeValue: GradingSchemeValueState;
  readonly material: MaterialState;
  readonly assignment: AssignmentState;
  readonly submission: SubmissionState;
  readonly educationalAlignment: EducationalAlignmentState;
  readonly personRole: PersonRoleState;
  readonly loadingBar: any;
}

const rootReducer = combineReducers<IRootState>({
  authentication,
  locale,
  applicationProfile,
  administration,
  userManagement,
  register,
  activate,
  passwordReset,
  password,
  settings,
  milestone,
  gradingScheme,
  gradingSchemeValue,
  material,
  assignment,
  submission,
  educationalAlignment,
  personRole,
  loadingBar,
});

export default rootReducer;
