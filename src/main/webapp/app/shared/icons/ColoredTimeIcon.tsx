import React from 'react';

const ColoredTimeIcon = ({
  ariaLabel = undefined,
  style = {},
  fill = 'none',
  width = '10px',
  height = '10px',
  viewBox = '0 0 10 10',
  className = '',
}) => (
  <svg
    aria-label={ariaLabel}
    className={className}
    width={width}
    height={height}
    style={style}
    viewBox={viewBox}
    fill={fill}
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M5 10C7.76142 10 10 7.76143 10 5C10 2.23858 7.76142 3.8147e-06 5 3.8147e-06C2.23858 3.8147e-06 0 2.23858 0 5C0 7.76143 2.23858 10 5 10Z"
      fill="#5557CE"
    />
    <path
      d="M5 1.19141C2.9 1.19141 1.19141 2.9 1.19141 5C1.19141 7.1 2.9 8.82813 5 8.82813C7.1 8.82813 8.80859 7.1 8.80859 5C8.80859 2.9 7.1 1.19141 5 1.19141Z"
      fill="#F0F7FF"
    />
    <path
      d="M5 2.36329C4.83807 2.36329 4.70703 2.49432 4.70703 2.65625V5.58594C4.70703 5.74787 4.83807 5.87891 5 5.87891C5.16193 5.87891 5.29297 5.74787 5.29297 5.58594V2.65625C5.29297 2.49432 5.16193 2.36329 5 2.36329Z"
      fill="#F78E1E"
    />
    <path
      d="M6.75781 4.70704H5H4.41406C4.25213 4.70704 4.12109 4.83807 4.12109 5C4.12109 5.16194 4.25213 5.29297 4.41406 5.29297H5H6.75781C6.91975 5.29297 7.05078 5.16194 7.05078 5C7.05078 4.83807 6.91975 4.70704 6.75781 4.70704Z"
      fill="#F78E1E"
    />
  </svg>
);

export default ColoredTimeIcon;
