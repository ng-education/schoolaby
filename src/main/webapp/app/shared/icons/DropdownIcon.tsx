import React from 'react';
import { ICON_STROKE_DEFAULT } from 'app/shared/util/color-utils';

const DropdownIcon = ({
  ariaLabel = undefined,
  style = {},
  fill = 'transparent',
  stroke = ICON_STROKE_DEFAULT,
  width = '4px',
  height = '19px',
  viewBox = '0 0 4 19',
  className = '',
}) => (
  <svg
    aria-label={ariaLabel}
    className={className}
    width={width}
    height={height}
    style={style}
    viewBox={viewBox}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M3.99951 2.50072C3.99951 1.39461 3.10588 0.500977 1.99977 0.500976C0.893663 0.500976 2.95248e-05 1.39461 2.94765e-05 2.50072C2.94281e-05 3.60682 0.893663 4.50046 1.99977 4.50046C3.10588 4.50046 3.99951 3.60682 3.99951 2.50072ZM3.99951 9.49981C3.99951 8.3937 3.10588 7.50007 1.99977 7.50007C0.893663 7.50007 2.92189e-05 8.3937 2.91706e-05 9.49981C2.91222e-05 10.6059 0.893663 11.4996 1.99977 11.4996C3.10588 11.4996 3.99951 10.6059 3.99951 9.49981ZM3.99951 16.4989C3.99951 15.3928 3.10588 14.4992 1.99977 14.4992C0.893663 14.4992 2.8913e-05 15.3928 2.88646e-05 16.4989C2.88163e-05 17.605 0.893663 18.4986 1.99977 18.4986C3.10588 18.4986 3.99951 17.605 3.99951 16.4989Z"
      fill={fill}
      stroke={stroke}
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);

export default DropdownIcon;
