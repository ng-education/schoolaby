import React from 'react';
import { ICON_STROKE_DEFAULT } from 'app/shared/util/color-utils';

const HomeIcon = ({
  name,
  style = {},
  fill = 'transparent',
  stroke = ICON_STROKE_DEFAULT,
  width = '28px',
  height = '32px',
  viewBox = '0 0 28 32',
  className = '',
}) => (
  <svg
    aria-label={name}
    className={className}
    width={width}
    height={height}
    style={style}
    viewBox={viewBox}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path d="M27 31H1V11L14 1L27 11V31Z" fill={fill} stroke={stroke} strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round" />
    <path
      d="M18 15H10C8.9 15 8 15.9 8 17V31H20V17C20 15.9 19.1 15 18 15Z"
      fill={fill}
      stroke={stroke}
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);

export default HomeIcon;
