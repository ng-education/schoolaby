import React from 'react';

const EstonianFlagIcon = ({ ariaLabel = '', style = {}, width = '20', height = '20', viewBox = '0 0 20 20', className = '' }) => (
  <svg
    aria-label={ariaLabel}
    className={className}
    width={width}
    height={height}
    style={style}
    viewBox={viewBox}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <g clipPath="url(#clip0)">
      <path
        d="M0.145996 10C0.145996 11.2232 0.366035 12.3949 0.767988 13.4783L10.146 13.9131L19.524 13.4783C19.926 12.3949 20.146 11.2232 20.146 10C20.146 8.77682 19.926 7.6051 19.524 6.52178L10.146 6.08698L0.767988 6.52174C0.366035 7.6051 0.145996 8.77682 0.145996 10H0.145996Z"
        fill="black"
      />
      <path d="M10.1461 0C5.84643 0 2.181 2.71375 0.768066 6.52176H19.5241C18.1112 2.71375 14.4457 0 10.1461 0Z" fill="#0052B4" />
      <path d="M19.5241 13.4782H0.768066C2.181 17.2862 5.84643 20 10.1461 20C14.4457 20 18.1112 17.2862 19.5241 13.4782Z" fill="#F0F0F0" />
    </g>
    <defs>
      <clipPath id="clip0">
        <rect width={width} height={height} fill="white" transform="translate(0.145996)" />
      </clipPath>
    </defs>
  </svg>
);

export default EstonianFlagIcon;
