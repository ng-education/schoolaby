import React from 'react';
import { ICON_STROKE_DEFAULT } from 'app/shared/util/color-utils';

const PeopleCircledIcon = ({
  name,
  style = {},
  fill = 'transparent',
  stroke = ICON_STROKE_DEFAULT,
  width = '50px',
  height = '50px',
  viewBox = '0 0 235 235',
  className = '',
}) => (
  <svg
    aria-label={name}
    className={className}
    width={width}
    height={height}
    style={style}
    viewBox={viewBox}
    fill={fill}
    xmlns="http://www.w3.org/2000/svg"
  >
    <circle cx="117.5" cy="117.5" r="112.5" stroke={stroke} strokeWidth="10" />
    <path
      d="M117.5 131.6C104.521 131.6 94 121.078 94 108.1C94 95.1209 104.521 84.5996 117.5 84.5996C130.479 84.5996 141 95.1209 141 108.1C141 121.078 130.479 131.6 117.5 131.6Z"
      stroke={stroke}
      strokeWidth="10"
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M84.5994 188C84.5994 167.051 99.1694 150.4 117.499 150.4C135.829 150.4 150.399 167.051 150.399 188H84.5994Z"
      stroke={stroke}
      strokeWidth="10"
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M73.1111 112.8C58.4889 112.8 47 100.392 47 84.5996C47 68.8076 58.4889 56.3996 73.1111 56.3996C81.4667 56.3996 89.3 60.9116 94 67.6796"
      stroke={stroke}
      strokeWidth="10"
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M69.8278 169.2H37.5992C37.5992 148.251 54.2506 131.6 75.1992 131.6"
      stroke={stroke}
      strokeWidth="10"
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M141 67.6796C145.7 60.9116 153.533 56.3996 161.889 56.3996C176.511 56.3996 188 68.8076 188 84.5996C188 100.392 176.511 112.8 161.889 112.8"
      stroke={stroke}
      strokeWidth="10"
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M159.799 131.6C180.748 131.6 197.399 148.251 197.399 169.2H165.171"
      stroke={stroke}
      strokeWidth="10"
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);

export default PeopleCircledIcon;
