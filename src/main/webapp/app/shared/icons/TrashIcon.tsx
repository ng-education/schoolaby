import React from 'react';
import { ICON_STROKE_DEFAULT } from 'app/shared/util/color-utils';

const TrashIcon = ({
  name,
  style = {},
  fill = 'transparent',
  stroke = ICON_STROKE_DEFAULT,
  width = '24px',
  height = '32px',
  viewBox = '0 0 24 32',
  className = '',
}) => (
  <svg
    aria-label={name}
    className={className}
    width={width}
    height={height}
    style={style}
    viewBox={viewBox}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path d="M21 9H3V31H21V9Z" fill={fill} stroke={stroke} strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round" />
    <path d="M23 5H1V9H23V5Z" fill={fill} stroke={stroke} strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round" />
    <path d="M8 5V1H16V5" fill={fill} stroke={stroke} strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round" />
    <path d="M9 14V31" fill={fill} stroke={stroke} strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round" />
    <path d="M15 14V31" fill={fill} stroke={stroke} strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round" />
  </svg>
);

export default TrashIcon;
