import React from 'react';
import { ICON_STROKE_DEFAULT } from 'app/shared/util/color-utils';

const ConversationIcon = ({
  name,
  style = {},
  fill = 'transparent',
  stroke = ICON_STROKE_DEFAULT,
  width = '32px',
  height = '32px',
  viewBox = '0 0 32 32',
  className = '',
}) => (
  <svg
    aria-label={name}
    className={className}
    width={width}
    height={height}
    style={style}
    viewBox={viewBox}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M21 1H3C1.9 1 1 1.9 1 3V15C1 16.1 1.9 17 3 17H6V21L10 17H21C22.1 17 23 16.1 23 15V3C23 1.9 22.1 1 21 1Z"
      fill={fill}
      stroke={stroke}
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M11 20C11 20.3 11 20.7 11 21C11 23.8 12.1 26.3 13.9 28.1L11 31H21C26.5 31 31 26.5 31 21C31 17.3 29 14.1 26 12.3"
      fill={fill}
      stroke={stroke}
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path d="M5 7H19" fill={fill} stroke={stroke} strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round" />
    <path d="M5 11H19" fill={fill} stroke={stroke} strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round" />
  </svg>
);

export default ConversationIcon;
