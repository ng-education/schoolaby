import React from 'react';
import { SBY_ICON_BEIGE } from 'app/shared/util/color-utils';

const ChatBubble = ({ width = '13', height = '13', viewBox = '0 0 13 13', fill = SBY_ICON_BEIGE }) => (
  <svg width={width} height={height} viewBox={viewBox} fill="none" xmlns="http://www.w3.org/2000/svg">
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M2.94531 1.07031H10.9453C11.4953 1.07031 11.9453 1.52031 11.9453 2.07031V8.07031C11.9453 8.62031 11.4953 9.07031 10.9453 9.07031H3.94531L1.94531 11.0703V2.07031C1.94531 1.52031 2.39531 1.07031 2.94531 1.07031ZM3.94531 8.07031H10.9453V2.07031H2.94531V9.07031L3.94531 8.07031Z"
      fill={fill}
    />
  </svg>
);

export default ChatBubble;
