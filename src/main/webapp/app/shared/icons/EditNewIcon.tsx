import React from 'react';
import { SBY_ICON_BEIGE } from 'app/shared/util/color-utils';

const EditNewIcon = ({ name, style = {}, fill = SBY_ICON_BEIGE, width = '17', height = '17', viewBox = '0 0 17 17', className = '' }) => (
  <svg
    aria-label={name}
    className={className}
    width={width}
    height={height}
    style={style}
    viewBox={viewBox}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M12.5729 5.45833L9.91667 2.80208L2.83333 9.88542V12.5417H5.48958L12.5729 5.45833ZM14.6696 3.36167C14.9458 3.08542 14.9458 2.63917 14.6696 2.36292L13.0121 0.705417C12.7358 0.429167 12.2896 0.429167 12.0133 0.705417L10.625 2.09375L13.2812 4.75L14.6696 3.36167Z"
      fill={fill}
    />
    <path opacity="0.36" d="M0 14.6667H17V17.5H0V14.6667Z" fill={fill} />
  </svg>
);
export default EditNewIcon;
