import React from 'react';

const GroupAssignmentIcon = ({
  name,
  style = {},
  fill = 'none',
  stroke = '#7B7B7B',
  width = '18',
  height = '15',
  viewBox = '0 0 18 15',
  className = '',
}) => (
  <svg
    aria-label={name}
    className={className}
    width={width}
    height={height}
    style={style}
    viewBox={viewBox}
    fill={fill}
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M8.99968 8.64737C10.5409 8.64737 11.7904 7.44908 11.7904 5.9709C11.7904 4.49273 10.5409 3.29443 8.99968 3.29443C7.45842 3.29443 6.20898 4.49273 6.20898 5.9709C6.20898 7.44908 7.45842 8.64737 8.99968 8.64737Z"
      stroke={stroke}
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M12.9067 13.9998C12.9067 11.8695 11.1765 10.1763 8.99975 10.1763C6.82301 10.1763 5.09277 11.8695 5.09277 13.9998H12.9067Z"
      stroke={stroke}
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M13.1967 6.35294C14.7017 6.35294 15.8841 5.17529 15.8841 3.67647C15.8841 2.17765 14.7017 1 13.1967 1C12.3368 1 11.5306 1.42824 11.0469 2.07059"
      stroke={stroke}
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M13.8109 12.0882H17.0002C17.0002 9.95793 15.3524 8.26465 13.2793 8.26465"
      stroke={stroke}
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M6.95342 2.07059C6.4697 1.42824 5.6635 1 4.80355 1C3.29864 1 2.11621 2.17765 2.11621 3.67647C2.11621 5.17529 3.29864 6.35294 4.80355 6.35294"
      stroke={stroke}
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M4.72093 8.26465C2.64784 8.26465 1 9.95793 1 12.0882H4.18937"
      stroke={stroke}
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);

export default GroupAssignmentIcon;
