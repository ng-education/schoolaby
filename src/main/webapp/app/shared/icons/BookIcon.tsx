import React from 'react';
import { ICON_STROKE_DEFAULT } from 'app/shared/util/color-utils';

const BookIcon = ({
  name,
  style = {},
  fill = 'transparent',
  stroke = ICON_STROKE_DEFAULT,
  width = '28px',
  height = '32px',
  viewBox = '0 0 28 32',
  className = '',
}) => (
  <svg
    aria-label={name}
    className={className}
    width={width}
    height={height}
    style={style}
    viewBox={viewBox}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M1.00001 27V5C1.00001 2.8 2.80001 1 5.00001 1H27V23H5.20001C3.10001 23 1.20001 24.5 1.00001 26.6C0.800009 29 2.70001 31 5.00001 31H27"
      fill={fill}
      stroke={stroke}
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path d="M25 23V31" fill={fill} stroke={stroke} strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round" />
  </svg>
);

export default BookIcon;
