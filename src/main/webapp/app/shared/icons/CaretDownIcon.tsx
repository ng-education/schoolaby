import React from 'react';
import { ICON_DARK_GREY, ICON_STROKE_DEFAULT } from 'app/shared/util/color-utils';

const CaretDownIcon = ({
  ariaLabel = undefined,
  style = {},
  fill = ICON_DARK_GREY,
  width = '18px',
  height = '18px',
  viewBox = '0 0 18 18',
  className = '',
}) => (
  <svg
    aria-label={ariaLabel}
    className={className}
    width={width}
    style={style}
    height={height}
    viewBox={viewBox}
    xmlns="http://www.w3.org/2000/svg"
  >
    <path d="M14.25 6.75L9 12L3.75 6.75" fill={fill} />
  </svg>
);

export default CaretDownIcon;
