import React from 'react';
import { ICON_STROKE_DEFAULT } from 'app/shared/util/color-utils';

const PreferencesIcon = ({
  name,
  style = {},
  fill = 'transparent',
  stroke = ICON_STROKE_DEFAULT,
  width = '32px',
  height = '32px',
  viewBox = '0 0 32 32',
  className = '',
}) => (
  <svg
    aria-label={name}
    className={className}
    width={width}
    height={height}
    style={style}
    viewBox={viewBox}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M7.29995 5.2C8.45975 5.2 9.39995 4.2598 9.39995 3.1C9.39995 1.9402 8.45975 1 7.29995 1C6.14015 1 5.19995 1.9402 5.19995 3.1C5.19995 4.2598 6.14015 5.2 7.29995 5.2Z"
      stroke={stroke}
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M15.7001 12.2C16.8599 12.2 17.8001 11.2598 17.8001 10.1C17.8001 8.9402 16.8599 8 15.7001 8C14.5403 8 13.6001 8.9402 13.6001 10.1C13.6001 11.2598 14.5403 12.2 15.7001 12.2Z"
      stroke={stroke}
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M7.29995 19.2C8.45975 19.2 9.39995 18.2598 9.39995 17.1C9.39995 15.9402 8.45975 15 7.29995 15C6.14015 15 5.19995 15.9402 5.19995 17.1C5.19995 18.2598 6.14015 19.2 7.29995 19.2Z"
      stroke={stroke}
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path d="M5.2 3.09961H1" stroke={stroke} strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round" />
    <path d="M10.8 10.1006H1" stroke={stroke} strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round" />
    <path d="M5.2 17.0996H1" stroke={stroke} strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round" />
    <path d="M22.0002 17.0996H12.2002" stroke={stroke} strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round" />
    <path d="M22 10.1006H17.8" stroke={stroke} strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round" />
    <path d="M22.0002 3.09961H12.2002" stroke={stroke} strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round" />
  </svg>
);

export default PreferencesIcon;
