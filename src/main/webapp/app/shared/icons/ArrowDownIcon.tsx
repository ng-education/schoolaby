import React from 'react';

const ArrowDownIcon = ({ ariaLabel = '', style = {}, width = '9', height = '5', viewBox = '0 0 9 5', className = '' }) => (
  <svg
    aria-label={ariaLabel}
    className={className}
    width={width}
    height={height}
    style={style}
    viewBox={viewBox}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path d="M1.59998 0.999994L4.79998 3.39999L7.99998 0.999994" stroke="black" strokeWidth="2" strokeLinecap="round" />
  </svg>
);

export default ArrowDownIcon;
