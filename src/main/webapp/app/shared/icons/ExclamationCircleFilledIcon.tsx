import React from 'react';

const BasketBallIcon = ({
  name,
  style = {},
  fill = '#1B1B1B',
  stroke = 'transparent',
  width = '13px',
  height = '13px',
  viewBox = '0 0 13 13',
  className = '',
}) => (
  <svg
    aria-label={name}
    className={className}
    width={width}
    height={height}
    style={style}
    viewBox={viewBox}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M6.5 13C10.0899 13 13 10.0899 13 6.5C13 2.91015 10.0899 0 6.5 0C2.91015 0 0 2.91015 0 6.5C0 10.0899 2.91015 13 6.5 13ZM5.926 8.0294H7.07305L7.26423 2.2941H5.73482L5.926 8.0294ZM5.93985 9.4298C5.80316 9.57095 5.73482 9.75025 5.73482 9.96769C5.73482 10.1775 5.80132 10.353 5.93431 10.4941C6.0673 10.6353 6.25571 10.7059 6.49953 10.7059C6.74334 10.7059 6.93175 10.6353 7.06474 10.4941C7.19773 10.353 7.26423 10.1775 7.26423 9.96769C7.26423 9.75025 7.19589 9.57095 7.0592 9.4298C6.92251 9.28483 6.73596 9.21235 6.49953 9.21235C6.26309 9.21235 6.07654 9.28483 5.93985 9.4298Z"
      fill={fill}
      fillRule={'evenodd'}
      clipRule={'evenodd'}
      stroke={stroke}
    />
  </svg>
);

export default BasketBallIcon;
