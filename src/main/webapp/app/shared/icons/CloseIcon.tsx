import React from 'react';
import { ICON_LIGHTER_GREY } from 'app/shared/util/color-utils';

const CloseIcon = ({
  ariaLabel = undefined,
  style = {},
  width = '18',
  height = '18',
  viewBox = '0 0 18 18',
  className = undefined,
  fill = ICON_LIGHTER_GREY,
}) => (
  <svg
    aria-label={ariaLabel}
    className={className}
    width={width}
    height={height}
    style={style}
    viewBox={viewBox}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M14.8633 4.8075L13.8058 3.75L9.61328 7.9425L5.42078 3.75L4.36328 4.8075L8.55578 9L4.36328 13.1925L5.42078 14.25L9.61328 10.0575L13.8058 14.25L14.8633 13.1925L10.6708 9L14.8633 4.8075Z"
      fill={fill}
    />
  </svg>
);

export default CloseIcon;
