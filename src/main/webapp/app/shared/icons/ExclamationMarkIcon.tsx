import React from 'react';
import { ICON_STROKE_DEFAULT } from 'app/shared/util/color-utils';

const ExclamationMarkIcon = ({
  name,
  style = {},
  fill = 'none',
  stroke = ICON_STROKE_DEFAULT,
  width = '18',
  height = '18',
  viewBox = '0 0 18 18',
  className = '',
  role = 'exclamation-mark-icon',
}) => (
  <svg
    aria-label={name}
    className={className}
    width={width}
    height={height}
    style={style}
    viewBox={viewBox}
    fill={fill}
    xmlns="http://www.w3.org/2000/svg"
    role={role}
  >
    <path
      d="M9 17C13.4183 17 17 13.4183 17 9C17 4.58172 13.4183 1 9 1C4.58172 1 1 4.58172 1 9C1 13.4183 4.58172 17 9 17Z M9 4V9 M9 13V14"
      stroke={stroke}
    />
  </svg>
);

export default ExclamationMarkIcon;
