import React from 'react';
import { ICON_WHITE } from 'app/shared/util/color-utils';

const PlayIcon = ({ className = '', ariaLabel = '', width = '24', height = '24', viewBox = '0 0 24 24', fill = ICON_WHITE }) => (
  <svg
    className={className}
    aria-label={ariaLabel}
    width={width}
    height={height}
    viewBox={viewBox}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M24 12.0056C23.997 15.1868 22.7316 18.2367 20.4817 20.4856C18.2317 22.7345 15.1812 23.9985 12 24C5.37378 24 -0.0159022 18.6056 3.5255e-05 11.985C0.0159727 5.36438 5.3916 -0.00842757 12.0075 9.92414e-06C18.6235 0.00844742 24.0066 5.39813 24 12.0056ZM8.57441 12C8.57441 13.1166 8.57441 14.2341 8.57441 15.3506C8.57441 16.1006 9.20722 16.4953 9.88691 16.1588C12.1144 15.0494 14.3403 13.9369 16.5647 12.8213C16.9088 12.6488 17.1469 12.3919 17.1403 11.985C17.1338 11.5781 16.9069 11.3456 16.5638 11.175C14.3494 10.0756 12.1366 8.97219 9.92534 7.8647C9.18753 7.50001 8.57628 7.87969 8.57534 8.69813C8.57347 9.80001 8.57316 10.9006 8.57441 12Z"
      fill={fill}
    />
  </svg>
);

export default PlayIcon;
