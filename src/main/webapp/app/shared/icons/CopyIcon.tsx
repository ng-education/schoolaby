import React from 'react';
import { PRIMARY } from 'app/shared/util/color-utils';

const CopyIcon = ({
  name,
  style = {},
  fill = 'transparent',
  stroke = PRIMARY,
  width = '28px',
  height = '32px',
  viewBox = '0 0 28 32',
  className = '',
}) => (
  <svg
    aria-label={name}
    className={className}
    width={width}
    height={height}
    style={style}
    viewBox={viewBox}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M15 9H27V31H9V15C11.3 12.7 12.7 11.3 15 9Z"
      stroke={stroke}
      strokeWidth="2"
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path d="M15 9V15H9" stroke={stroke} strokeWidth="2" strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round" />
    <path d="M13 21H23" stroke={stroke} strokeWidth="2" strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round" />
    <path d="M13 25H23" stroke={stroke} strokeWidth="2" strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round" />
    <path
      d="M19 5V1H7C4.7 3.3 3.3 4.7 1 7V23H5"
      stroke={stroke}
      strokeWidth="2"
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path d="M7 1V7H1" stroke={stroke} strokeWidth="2" strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round" />
  </svg>
);

export default CopyIcon;
