import axios from 'axios';
import { useMutation } from 'react-query';

export const useAgreeTerms = () => useMutation(() => axios.post('api/account/terms-agreed'));

export const useFirstLogin = () => useMutation(() => axios.post('api/account/first-login'));

export const useLogout = () => useMutation(() => axios.post('api/account/logout'));
