import axios from 'axios';
import { ISubmission } from 'app/shared/model/submission/submission.model';
import { cleanEntity } from 'app/shared/util/entity-utils';
import { ISubmissionPatch } from 'app/shared/model/submission/submission-patch.model';
import { ISubmissionFeedback } from 'app/shared/model/submission/submission-feedback.model';
import { useQuery } from 'react-query';
import { SUBMISSIONS_KEY } from 'app/config/reactQueryKeyConstants';

export interface UseGetSubmissionsParams {
  assignmentId?: number;
  journeyId?: number;
  authorId?: number;
  unPaged?: boolean;
}

export const useGetSubmissions = (params: UseGetSubmissionsParams, enabled = true, onSuccess?: (submissions: ISubmission[]) => void) =>
  useQuery<ISubmission[]>(
    [SUBMISSIONS_KEY, params.assignmentId, params.journeyId, params.authorId, params.unPaged],
    async () =>
      axios
        .get<ISubmission[]>('api/submissions', {
          params,
        })
        .then(response => response.data),
    { enabled, onSuccess }
  );

export const useGetSubmission = (submissionId: number | string, enabled = true) =>
  useQuery<ISubmission, Error>(
    [SUBMISSIONS_KEY, +submissionId],
    async () => axios.get<ISubmission>(`api/submissions/${submissionId}`).then(response => response.data),
    { enabled }
  );

export const getSubmissionsByAssignmentId = id => {
  return axios.get<ISubmission[]>(`api/submissions?assignmentId=${id}&size=1000`).then(res => {
    return res.data;
  });
};

export const getSubmissionsInJourney = (id: number, unPaged = false) => {
  return axios.get<ISubmission[]>(`api/submissions?journeyId=${id}&unPaged=${unPaged}`).then(res => {
    return res.data;
  });
};

export const createSubmission = (entity: ISubmission) => {
  return axios.post<ISubmission>('api/submissions', cleanEntity(entity)).then(res => {
    return res.data;
  });
};

export const patchSubmission = (id, entity: ISubmissionPatch) => {
  return axios.patch<ISubmission>(`api/submissions/${id}`, cleanEntity(entity)).then(res => {
    return res.data;
  });
};

export const gradeSubmission = (submissionId, entity: ISubmissionPatch) => {
  return axios.patch<ISubmissionFeedback>(`api/submission-feedback?submissionId=${submissionId}`, cleanEntity(entity)).then(res => {
    return res.data;
  });
};
