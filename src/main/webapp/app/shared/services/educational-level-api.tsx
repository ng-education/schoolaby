import axios from 'axios';
import { IEducationalLevel } from 'app/shared/model/EducationalLevel.model';
import { EDUCATIONAL_LEVELS_KEY } from 'app/config/reactQueryKeyConstants';
import { useQuery } from 'react-query';

export const useGetEducationalLevels = (country?: string) =>
  useQuery<IEducationalLevel[], Error>(
    [EDUCATIONAL_LEVELS_KEY, country],
    async () => {
      return axios
        .get<IEducationalLevel[]>('api/educational-levels', { params: { country } })
        .then(res => res.data);
    },
    {
      initialData: [],
    }
  );
