import axios from 'axios';
import { GradingScheme } from 'app/shared/model/grading-scheme.model';
import { GRADING_SCHEME_KEY } from 'app/config/reactQueryKeyConstants';
import { useQuery } from 'react-query';
import sortBy from 'lodash/sortBy';

export const getGradingScheme = id => {
  return axios.get<GradingScheme>(`api/grading-schemes/${id}`).then(response => {
    response.data = GradingScheme.fromJson(response.data);
    return response.data;
  });
};

export const useGetGradingScheme = (gradingSchemeId: string | number, enabled = true, onSuccess?: (param?: any) => void) =>
  useQuery<GradingScheme>([GRADING_SCHEME_KEY, gradingSchemeId], () => getGradingScheme(gradingSchemeId), {
    enabled,
    onSuccess,
  });

export const useGetGradingSchemes = (onSuccess?: (gradingSchemes: GradingScheme[]) => void) =>
  useQuery<GradingScheme[]>(
    [GRADING_SCHEME_KEY],
    () =>
      axios.get<GradingScheme[]>(`api/grading-schemes`).then(response => {
        response.data = response.data.map(g => GradingScheme.fromJson(g));
        return sortBy(response.data, 'sequenceNumber');
      }),
    {
      onSuccess,
    }
  );
