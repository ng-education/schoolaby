import axios from 'axios';
import { IAssignment } from 'app/shared/model/assignment.model';
import { Material } from 'app/shared/model/material.model';
import { useMutation, useQuery, useQueryClient } from 'react-query';
import {
  ASSIGNMENT_KEY,
  ASSIGNMENT_LTI_RESOURCES_KEY,
  GROUP_ASSIGNMENTS_KEY,
  JOURNEY_ASSIGNMENTS_KEY,
  JOURNEY_KEY,
} from 'app/config/reactQueryKeyConstants';
import { IGroupAssignment } from 'app/shared/model/group-assignment.model';
import { ILtiResource } from 'app/shared/model/lti-resource.model';

export interface UseGetAssignmentParams {
  template?: boolean;
  withPreviousAndNextIds?: boolean;
}

export interface PatchAssignmentParams {
  title?: string;
  description?: string;
  materials?: Material[];
  ltiResources?: ILtiResource[];
}

export const useGetAssignment = (id: string | number, enabled = true, params?: UseGetAssignmentParams) =>
  useQuery<IAssignment, Error>(
    [ASSIGNMENT_KEY, +id],
    async () =>
      axios
        .get<IAssignment>(`api/assignments/${id}`, {
          params,
        })
        .then(response => {
          response.data.materials = response.data.materials?.map(Material.fromJson);
          return response.data;
        }),
    { enabled }
  );

export const getAssignments = (fromDate, toDate, states, journeyId?, size = 50) =>
  axios.get<IAssignment[]>(`api/assignments`, {
    params: {
      size,
      fromDate,
      toDate,
      journeyId,
      sort: 'deadline,id,asc',
      states: states.toString(),
    },
  });

export const useGetAssignments = (states, journeyId?, fromDate?, toDate?, setTotalCount?, size = 50, enabled = true) =>
  useQuery<IAssignment[], Error>(
    [JOURNEY_ASSIGNMENTS_KEY, journeyId, { states }],
    async () =>
      getAssignments(fromDate, toDate, states, journeyId, size).then(response => {
        setTotalCount && setTotalCount(+response.headers['x-total-count']);
        return response.data;
      }),
    { enabled }
  );

export const useGetGroupAssignmentsBySearch = (journeyId: string, search: string) =>
  useQuery<IGroupAssignment[], Error>([GROUP_ASSIGNMENTS_KEY, journeyId, search], async () =>
    axios
      .get<IGroupAssignment[]>(`api/group-assignments`, {
        params: {
          journeyId,
          search,
        },
      })
      .then(response => response.data)
  );

export const useDeleteAssignment = (id: number) => {
  const queryClient = useQueryClient();
  return useMutation(async () => axios.delete(`api/assignments/${id}`), {
    onSuccess() {
      queryClient.invalidateQueries(JOURNEY_KEY);
    },
  });
};

export const useCreateAssignment = (onSuccess?: () => void) => {
  const queryClient = useQueryClient();
  return useMutation(async (assignment: IAssignment) => axios.post('api/assignments', assignment), {
    onSuccess() {
      onSuccess && onSuccess();
      queryClient.invalidateQueries(JOURNEY_KEY);
    },
  });
};

export const useUpdateAssignment = (assignmentId?: number, onSuccess?: () => void) => {
  const queryClient = useQueryClient();
  return useMutation(async (assignment: IAssignment) => axios.put('api/assignments', assignment), {
    onSuccess() {
      onSuccess && onSuccess();
      queryClient.invalidateQueries(JOURNEY_KEY);
      !!assignmentId && queryClient.invalidateQueries([ASSIGNMENT_KEY, assignmentId]);
    },
  });
};

export const patchAssignment = (id: number, params?: PatchAssignmentParams) => axios.patch<IAssignment>(`api/assignments/${id}`, params);

export const usePatchAssignment = (assignmentId?: number) => {
  const queryClient = useQueryClient();
  return useMutation(async (params?: PatchAssignmentParams) => patchAssignment(assignmentId, params), {
    onSuccess() {
      queryClient.invalidateQueries([ASSIGNMENT_KEY, assignmentId]);
      queryClient.invalidateQueries([ASSIGNMENT_LTI_RESOURCES_KEY, assignmentId]);
    },
  });
};
