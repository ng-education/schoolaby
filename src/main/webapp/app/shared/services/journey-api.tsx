import axios, { AxiosResponse } from 'axios';
import { IJourney } from 'app/shared/model/journey.model';
import { JOURNEY_KEY, JOURNEY_STUDENTS_KEY, JOURNEY_TEACHERS_KEY, JOURNEYS_KEY } from 'app/config/reactQueryKeyConstants';
import { InfiniteData, useInfiniteQuery, useMutation, useQuery, useQueryClient } from 'react-query';
import { parseHeaderForLinks } from 'react-jhipster/lib/src';
import { IJourneyStudent } from 'app/shared/model/journey-student.model';
import ISubject from 'app/shared/model/subject.model';

interface DeleteMemberFromJourneyProps {
  journeyId: number;
  userId: number;
}

export const useGetJourney = (id: number | string, enabled = true, template?: boolean, onSuccess?: (journey: IJourney) => void) =>
  useQuery<IJourney, Error>(
    [JOURNEY_KEY, id],
    async () =>
      axios
        .get<IJourney>(`api/journeys/${id}`, {
          params: { template },
        })
        .then(response => response.data),
    {
      enabled,
      onSuccess,
    }
  );

// TODO: Refactor this when dashboard-old is removed
export const useGetJourneysPaginated = (state = '', template = false, enabled = true, userId?) =>
  useInfiniteQuery<AxiosResponse<IJourney[]>>(
    [JOURNEYS_KEY, state, template, userId],
    ({ pageParam = 0 }) =>
      axios
        .get<IJourney[]>('api/journeys/', {
          params: {
            state,
            page: pageParam,
            template,
            sort: 'endDate,asc',
          },
          headers: {
            'JOURNEY-JSON-VERSION': '1',
          },
        })
        .then(response => response),
    {
      enabled,
      staleTime: Infinity,
      getNextPageParam: (lastPage, _) => parseHeaderForLinks(lastPage.headers.link).next,
    }
  );

export const useGetJourneyTemplatesBySearch = (search: string, subject?: ISubject, userId?: number, enabled = true) =>
  useInfiniteQuery<AxiosResponse<IJourney[]>>(
    [JOURNEYS_KEY, search, subject, userId],
    ({ pageParam = 0 }) =>
      axios
        .get<IJourney[]>('api/journeys/', {
          params: {
            search,
            subjectId: subject?.id,
            userId,
            template: true,
            page: pageParam,
          },
          headers: {
            'JOURNEY-JSON-VERSION': '2',
          },
        })
        .then(response => response),
    {
      enabled,
      staleTime: Infinity,
      getNextPageParam: (lastPage, _) => parseHeaderForLinks(lastPage.headers.link).next,
    }
  );

export const useGetUserJourneysPaginated = (state = '', userId?, enabled = true) => useGetJourneysPaginated(state, false, enabled, userId);

export const useGetJourneyStudents = (journeyId: number, enabled = true) =>
  useQuery<IJourneyStudent[]>(
    [JOURNEY_STUDENTS_KEY, journeyId],
    () => axios.get<IJourneyStudent[]>(`api/journeys/${journeyId}/students`, {}).then(response => response.data),
    { enabled }
  );

export const useToggleJourneyStudentPin = (journeyId: number) => {
  const queryClient = useQueryClient();

  return useMutation((studentId: number) => axios.put(`api/journeys/${journeyId}/students/${studentId}/pin`, {}), {
    onSuccess() {
      queryClient.invalidateQueries([JOURNEY_STUDENTS_KEY, journeyId]);
    },
  });
};

export const useCreateJourney = () => {
  const queryClient = useQueryClient();

  return useMutation((journey: IJourney) => axios.post('api/journeys', journey), {
    onSuccess() {
      queryClient.invalidateQueries(JOURNEYS_KEY);
    },
  });
};

export const useUpdateJourney = () => {
  const queryClient = useQueryClient();

  return useMutation((journey: IJourney) => axios.put('api/journeys', journey), {
    onSuccess() {
      queryClient.invalidateQueries(JOURNEYS_KEY);
    },
  });
};

export const useDeleteJourney = (journeyId: number) => {
  const queryClient = useQueryClient();

  return useMutation(() => axios.delete(`api/journeys/${journeyId}`), {
    onSuccess() {
      queryClient.setQueriesData<InfiniteData<AxiosResponse<IJourney[]>>>(JOURNEYS_KEY, data => {
        return {
          pages: data?.pages?.map(page => ({
            ...page,
            data: page.data.filter(journey => journey.id !== journeyId),
          })),
          pageParams: data?.pageParams,
        };
      });
    },
  });
};

export const useJoinJourney = (onSuccess?: () => (journeyId) => void, onError?: () => void) => {
  const queryClient = useQueryClient();

  return useMutation<number, Error, string>(
    (signUpCode: string) =>
      axios.post('api/journeys/join', { signUpCode }).then(res => {
        return res.data.journeyId;
      }),
    {
      onSuccess(journeyId) {
        queryClient.invalidateQueries(JOURNEYS_KEY);
        onSuccess && onSuccess()(journeyId);
      },
      onError() {
        onError && onError();
      },
    }
  );
};

export const useDeleteStudentFromJourney = () => {
  const queryClient = useQueryClient();

  return useMutation(
    (deleteEntity: DeleteMemberFromJourneyProps) => axios.delete(`api/journeys/${deleteEntity.journeyId}/students/${deleteEntity.userId}`),
    {
      onSuccess() {
        queryClient.invalidateQueries(JOURNEY_STUDENTS_KEY);
        queryClient.invalidateQueries(JOURNEY_KEY);
      },
    }
  );
};

export const useDeleteTeacherFromJourney = () => {
  const queryClient = useQueryClient();

  return useMutation(
    (deleteEntity: DeleteMemberFromJourneyProps) => axios.delete(`api/journeys/${deleteEntity.journeyId}/teachers/${deleteEntity.userId}`),
    {
      onSuccess() {
        queryClient.invalidateQueries(JOURNEY_TEACHERS_KEY);
        queryClient.invalidateQueries(JOURNEY_KEY);
      },
    }
  );
};

// TODO: Only use hooks, remove these:
export const getJourneys = (state = '', page = 0) => {
  return axios.get<IJourney[]>('api/journeys/', {
    params: { state, page, sort: 'endDate,asc' },
    headers: {
      'JOURNEY-JSON-VERSION': '1',
    },
  });
};

export const getStudentsAverageGrades = id => {
  return axios.get(`api/journeys/${id}/average-grade/`);
};
