import axios from 'axios';
import { IEducationalAlignment } from 'app/shared/model/educational-alignment.model';
import { EDUCATIONAL_ALIGNMENTS_KEY } from 'app/config/reactQueryKeyConstants';
import { useQuery } from 'react-query';

export const useGetEducationalAlignmentsByTitle = (title: string, country?: string) =>
  useQuery<IEducationalAlignment[], Error>(
    [EDUCATIONAL_ALIGNMENTS_KEY, title],
    async () =>
      axios
        .get<IEducationalAlignment[]>('api/educational-alignments', {
          params: {
            title,
            country,
          },
        })
        .then(res => res.data),
    {
      enabled: false,
      initialData: [],
    }
  );
