import axios from 'axios';
import { useQuery } from 'react-query';
import { SUBJECTS_KEY } from 'app/config/reactQueryKeyConstants';
import ISubject from 'app/shared/model/subject.model';

interface GetSubjectsParameters {
  country?: string;
}

export const useGetSubjects = (params: GetSubjectsParameters) =>
  useQuery<ISubject[], Error>(
    [SUBJECTS_KEY],
    async () =>
      axios
        .get<ISubject[]>('api/subjects', { params })
        .then(res => res.data),
    {
      initialData: [],
    }
  );
