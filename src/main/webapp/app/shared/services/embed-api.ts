import axios from 'axios';
import { getProxiedUrl } from 'app/shared/services/cors-proxy-api';

export const getNoEmbedHtml = (url: string): Promise<string | undefined> =>
  axios.get(`https://noembed.com/embed?url=${url}`, { headers: { Authorization: 'none' } }).then(({ data: { html } }) => {
    return html;
  });

export const getPdfEmbedUrl = (pdfUrl: string, proxied?: boolean): string =>
  `/pdfjs/web/viewer.html?file=${encodeURIComponent(proxied ? getProxiedUrl(pdfUrl) : pdfUrl)}`;

export const isEmbeddable = async (url: string): Promise<boolean> =>
  url
    ? await axios
        .get<boolean>(`/api/proxy/embeddable?url=${encodeURIComponent(url)}`)
        .then(response => response.data)
        .catch(() => false)
    : false;
