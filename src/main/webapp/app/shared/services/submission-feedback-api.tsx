import axios from 'axios';
import { ISubmissionFeedback } from 'app/shared/model/submission/submission-feedback.model';
import { useMutation, useQuery, useQueryClient } from 'react-query';
import { SUBMISSION_FEEDBACK_KEY, SUBMISSIONS_KEY } from 'app/config/reactQueryKeyConstants';
import { ISubmissionPatch } from 'app/shared/model/submission/submission-patch.model';
import { cleanEntity } from 'app/shared/util/entity-utils';

interface UseGetSubmissionFeedbackParams {
  submissionId?: number;
  studentId?: number;
}

export const useGetSubmissionFeedback = (params: UseGetSubmissionFeedbackParams, enabled = true) =>
  useQuery<ISubmissionFeedback[]>(
    [SUBMISSION_FEEDBACK_KEY, params.submissionId, params.studentId],
    async () =>
      axios
        .get<ISubmissionFeedback[]>('api/submission-feedback', {
          params,
        })
        .then(response => response.data),
    { enabled }
  );

export const useCreateSubmissionFeedback = () => {
  const queryClient = useQueryClient();
  return useMutation(async (entity: ISubmissionPatch) => axios.post<ISubmissionFeedback>('api/submission-feedback', cleanEntity(entity)), {
    onSuccess(response) {
      queryClient.invalidateQueries([SUBMISSIONS_KEY]);
      queryClient.invalidateQueries([SUBMISSION_FEEDBACK_KEY, response.data.submissionId, response.data.studentId]);
    },
  });
};
