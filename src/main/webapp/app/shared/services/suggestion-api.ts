import axios from 'axios';
import { IEKoolikottMaterial, IEKoolikottReducedMaterial } from 'app/shared/model/e-koolikott/e-koolikott-material.model';
import { Material } from 'app/shared/model/material.model';
import { MATERIALS_KEY } from 'app/config/reactQueryKeyConstants';
import { QueryObserverResult, useQuery } from 'react-query';
import { COUNTRY, EKOOLIKOTT, OPIQ } from 'app/config/constants';

interface SuggestionsRequestParams {
  taxon?: string;
  searchString?: string;
  subjects?: string;
  country: string;
}

export type MaterialSource = typeof EKOOLIKOTT | typeof OPIQ | '';

export const useGetMaterialSuggestions = (
  params: SuggestionsRequestParams,
  source: MaterialSource = '',
  limit = 80
): QueryObserverResult<Material[], Error> => {
  let enabled;

  if (params.country === COUNTRY.ESTONIA) {
    enabled =
      (source === EKOOLIKOTT && !!params.taxon) || (source === OPIQ && !!params.searchString) || (!!params.taxon && !!params.searchString);
  } else if (params.country === COUNTRY.UKRAINE) {
    enabled = false;
  } else {
    enabled = !!params.searchString;
  }

  return useQuery<Material[], Error>(
    [MATERIALS_KEY, params.taxon, params.searchString, source],
    async () =>
      axios
        .get('/api/suggestions/suggested', {
          params: {
            limit,
            sort: 'type',
            type: 'material',
            sortDirection: 'asc',
            source,
            ...params,
          },
        })
        .then(res => res.data?.map(Material.fromJson)),
    {
      enabled,
    }
  );
};

export const useGetMaterialSuggestionsBySearchString = (
  searchString: string,
  limit: number,
  source: MaterialSource = '',
  enabled = false
): QueryObserverResult<Material[], Error> =>
  useQuery<Material[], Error>(
    [MATERIALS_KEY, searchString, source],
    async () =>
      axios
        .get('/api/suggestions/search', {
          params: {
            searchString,
            limit,
            source,
          },
        })
        .then(res => res.data.map(Material.fromJson)),
    { enabled }
  );

export const getMaterial = (id: number): Promise<IEKoolikottMaterial | void> => {
  return axios
    .get('/api/suggestions/rest/v2/learningObject', { params: { id } })
    .then(res => res.data as IEKoolikottMaterial)
    .catch(e => {
      // eslint-disable-next-line no-console
      console.log(e);
    });
};

export const getImageUrlByName = (name: string): string => {
  return `/api/suggestions/rest/picture/${name}`;
};

export const getMaterialUrl = (koolikottMaterial: IEKoolikottReducedMaterial): string => {
  return `https://e-koolikott.ee/oppematerjal/${koolikottMaterial.id}`;
};
