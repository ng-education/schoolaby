import React, { useEffect, useState } from 'react';
import { Table } from 'reactstrap';
import Avatar from 'app/shared/layout/header/avatar';
import Icon from 'app/shared/icons';
import { IPerson } from 'app/shared/model/person.model';
import { useLocation } from 'react-router-dom';
import { toast } from 'react-toastify';
import { translate, TextFormat } from 'react-jhipster';
import { getFullName } from 'app/shared/util/string-utils';
import { ICON_GREY } from 'app/shared/util/color-utils';
import { useDeleteStudentFromJourney, useGetJourneyStudents } from 'app/shared/services/journey-api';
import { getAuditEventsByJourneyId } from 'app/shared/services/management-api';
import { IAuditEvent } from 'app/shared/model/auditEvent.model';
import { MockClassState } from 'app/journey/journey-detail/class/journey-class';
import { APP_LOCAL_DATE_TIME_FORMAT } from 'app/config/constants';

interface IStudentListViewProps {
  renderProcess?: JSX.Element;
  journeyId?: number;
  isAllowedToModify: boolean;
}

const StudentListView = ({ isAllowedToModify, journeyId, renderProcess }: IStudentListViewProps) => {
  const [events, setEvents] = useState<IAuditEvent[]>([]);
  const location = useLocation<MockClassState>();
  const { data: students } = useGetJourneyStudents(journeyId, !!journeyId);
  const { mutate: deleteStudentFromJourney } = useDeleteStudentFromJourney();

  useEffect(() => {
    if (location?.state?.class?.students) {
      toast.success(translate('schoolabyApp.journey.students.added'));
    }
  }, []);

  useEffect(() => {
    if (students?.length && isAllowedToModify) {
      getAuditEventsByJourneyId(journeyId).then(res => setEvents(res.data));
    }
  }, [students]);

  const deleteStudent = (userId: number) => {
    deleteStudentFromJourney({ journeyId, userId });
  };

  const StudentRow = (person: IPerson, i) => {
    const loginDate = events.find(e => +e.data.userId === person.id)?.timestamp;
    return (
      <tr key={i}>
        <td>
          <Avatar fullName={getFullName(person)} showName />
        </td>
        {renderProcess && isAllowedToModify && <td className={'text-center'}>{renderProcess}</td>}
        {isAllowedToModify && (
          <td className={'text-center'}>
            {loginDate ? <TextFormat type="date" format={APP_LOCAL_DATE_TIME_FORMAT} value={loginDate} /> : '-'}
          </td>
        )}
        {isAllowedToModify && (
          <td className={'text-center delete'}>
            <div className={'icon-delete'} onClick={() => deleteStudent(person?.id)}>
              <Icon name={'trashNew'} className={'m-2'} width={'18px'} height={'18px'} stroke={ICON_GREY} />
            </div>
          </td>
        )}
      </tr>
    );
  };

  const renderRows = () => (
    <>
      {students?.map(({ user }, i) => StudentRow(user, i))}
      {location?.state?.class?.students.map((person, i) => StudentRow(person, i))}
    </>
  );

  return (
    <Table striped bordered>
      <thead>
        <tr>
          <th>{translate('global.form.name')}</th>
          {renderProcess && isAllowedToModify && <th className={'text-center'}>{translate('global.form.progress')}</th>}
          {isAllowedToModify && <th className={'text-center'}>{translate('global.form.lastLogin')}</th>}
          {isAllowedToModify && <th className={'text-center'}>{translate('entity.action.delete')}</th>}
        </tr>
      </thead>
      <tbody>{renderRows()}</tbody>
    </Table>
  );
};

export default StudentListView;
