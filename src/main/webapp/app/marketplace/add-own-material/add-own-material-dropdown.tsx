import React, { FC, useState } from 'react';
import { DropdownItem } from 'reactstrap';
import { translate } from 'react-jhipster';
import CustomDropdown from 'app/shared/layout/custom-dropdown/custom-dropdown';
import SaveMaterialModalNew, {
  CAMERA,
  FILE,
  LINK,
  SaveMaterialModalType,
} from 'app/shared/layout/save-material-modal-new/save-material-modal-new';
import { UseBaseMutationResult } from 'react-query';

interface IAddOwnMaterialDropdown {
  toggleButton?: JSX.Element;
  patchRequest?: UseBaseMutationResult;
}

const AddOwnMaterialDropdown: FC<IAddOwnMaterialDropdown> = ({ children, toggleButton, patchRequest }) => {
  const [modalOpen, setModalOpen] = useState<boolean>(false);
  const [modalType, setModalType] = useState<SaveMaterialModalType>(LINK);

  const openModal = (type: SaveMaterialModalType) => {
    setModalType(type);
    setModalOpen(true);
  };

  return (
    <>
      <CustomDropdown button={toggleButton} title={translate('schoolabyApp.marketplace.addOwnMaterial')}>
        {children}
        <DropdownItem onClick={() => openModal(LINK)}>{translate('schoolabyApp.marketplace.add.link')}</DropdownItem>
        <DropdownItem onClick={() => openModal(FILE)}>{translate('schoolabyApp.marketplace.add.file')}</DropdownItem>
        <DropdownItem onClick={() => openModal(CAMERA)}>{translate('schoolabyApp.marketplace.add.camera')}</DropdownItem>
      </CustomDropdown>
      <SaveMaterialModalNew
        patchRequest={patchRequest}
        isOpen={modalOpen}
        toggleModal={() => setModalOpen(prevState => !prevState)}
        type={modalType}
      />
    </>
  );
};

export default AddOwnMaterialDropdown;
