import React, { FC, useEffect, useState } from 'react';
import { Card } from 'reactstrap';
import { useGetPublicSchoolabyMaterials, useGetPublicSchoolabyMaterialsBySearchString } from 'app/shared/services/material-api';
import { MaterialSource, useGetMaterialSuggestions, useGetMaterialSuggestionsBySearchString } from 'app/shared/services/suggestion-api';
import { Material } from 'app/shared/model/material.model';
import uniqBy from 'lodash/uniqBy';

import './materials-section.scss';
import MaterialWideCardNew from 'app/shared/layout/material-card-new/material-wide-card-new/material-wide-card-new';
import { useMaterialState } from 'app/shared/contexts/material-context';
import { translate } from 'react-jhipster';
import { useParentEducationalAlignmentsState, useSelectedEducationalAlignmentsState } from 'app/shared/contexts/entity-update-context';
import { IEducationalAlignment } from 'app/shared/model/educational-alignment.model';
import { getOpiqSubject } from 'app/shared/util/subject-utils';
import { IRootState } from 'app/shared/reducers';
import { connect } from 'react-redux';
import MaterialNarrowCardNew from 'app/shared/layout/material-card-new/material-narrow-card-new/material-narrow-card-new';
import { Spinner } from 'app/shared/layout/spinner/spinner';
import { COUNTRY, EKOOLIKOTT, OPIQ } from 'app/config/constants';

interface MaterialsSectionProps extends StateProps {
  search: string;
}

const MaterialsSection: FC<MaterialsSectionProps> = ({ search, country, isUserUkrainian }) => {
  const [searchMaterials, setSearchMaterials] = useState<Material[]>([]);
  const [suggestedMaterials, setSuggestedMaterials] = useState<Material[]>([]);
  const [taxon, setTaxon] = useState<string>('');
  const { addMaterial } = useMaterialState();
  const { selectedEducationalAlignments } = useSelectedEducationalAlignmentsState();
  const { parentEducationalAlignments } = useParentEducationalAlignmentsState();
  const { data: externalMaterials, isFetching: isFetchingExternalMaterials } = useGetMaterialSuggestionsBySearchString(
    search,
    80,
    '',
    !!search && !isUserUkrainian
  );
  const { data: opiqMaterials, isFetching: isFetchingOpiqMaterials } = useGetMaterialSuggestionsBySearchString(
    search,
    3,
    OPIQ,
    !!search && !isUserUkrainian
  );
  const { data: ekoolikottMaterials, isFetching: isFetchingEkoolikottMaterials } = useGetMaterialSuggestionsBySearchString(
    search,
    3,
    EKOOLIKOTT,
    !!search && !isUserUkrainian
  );
  const { data: schoolabyMaterials, isFetching: isFetchingSchoolabyMaterials } = useGetPublicSchoolabyMaterialsBySearchString(
    search,
    isUserUkrainian
  );
  const { data: suggestedSchoolabyMaterials, isFetching: isFetchingSuggestedSchoolaby } = useGetPublicSchoolabyMaterials(
    selectedEducationalAlignments
  );
  const { data: suggestedEkoolikottMaterials, isFetching: isFetchingSuggestedEkoolikottMaterials } = useGetMaterialSuggestions(
    {
      taxon,
      country,
    },
    EKOOLIKOTT,
    3
  );
  const { data: suggestedOpiqMaterials, isFetching: isFetchingSuggestedOpiqMaterials } = useGetMaterialSuggestions(
    {
      country,
      searchString: selectedEducationalAlignments?.length
        ? selectedEducationalAlignments[selectedEducationalAlignments.length - 1].title
        : '',
      subjects:
        uniqBy<IEducationalAlignment>(selectedEducationalAlignments, 'subject.label')
          .map(edAlignment => getOpiqSubject(edAlignment.subject?.label) || '')
          .join(',') || null,
    },
    OPIQ,
    3
  );
  const { data: suggestedExternalMaterials, isFetching: isFetchingSuggestedExternal } = useGetMaterialSuggestions({
    taxon,
    country,
    searchString: selectedEducationalAlignments?.length
      ? selectedEducationalAlignments[selectedEducationalAlignments.length - 1].title
      : '',
    subjects:
      uniqBy<IEducationalAlignment>(selectedEducationalAlignments, 'subject.label')
        .map(edAlignment => getOpiqSubject(edAlignment.subject?.label) || '')
        .join(',') || null,
  });

  const getTaxonIdsRequestString = (educationalAlignments: IEducationalAlignment[]): string => {
    const filteredAlignments = educationalAlignments?.filter(e => !!e?.taxonIds)?.filter(e => !!e.taxonId);
    return filteredAlignments?.reduce((acc, alignment, index) => acc + `${index !== 0 ? ',' : ''}${alignment.taxonIds.join(',')}`, '');
  };

  useEffect(() => {
    const taxonIds = getTaxonIdsRequestString([...selectedEducationalAlignments, ...parentEducationalAlignments]);
    setTaxon(taxonIds);
  }, [selectedEducationalAlignments]);

  const mergeSchoolabyAndExternalMaterials = (sbyMaterials: Material[], extMaterials: Material[]) => [
    ...(uniqBy(sbyMaterials, material => material.getKey()) || []),
    ...(extMaterials || []),
  ];

  useEffect(() => {
    setSearchMaterials(mergeSchoolabyAndExternalMaterials(schoolabyMaterials, externalMaterials));
  }, [externalMaterials, schoolabyMaterials]);

  useEffect(() => {
    setSuggestedMaterials(mergeSchoolabyAndExternalMaterials(suggestedSchoolabyMaterials, suggestedExternalMaterials));
  }, [suggestedExternalMaterials, suggestedSchoolabyMaterials]);

  const NoContent = () => (
    <div className={'w-100 h-100 d-flex flex-column align-items-center justify-content-center position-absolute no-content'}>
      <img alt={''} src={'content/images/no_results_new.svg'} />
      <p className={'empty d-flex'}>{translate('schoolabyApp.material.home.notFound')}</p>
    </div>
  );

  const mapRestMaterials = (materialsToMap: Material[]) => (
    <>
      {materialsToMap?.length ? (
        materialsToMap.map(material => (
          <MaterialWideCardNew key={`material-${material.getKey()}`} material={material} materialAction={() => addMaterial(material)} />
        ))
      ) : (
        <NoContent />
      )}
    </>
  );

  const mapPopularMaterials = (materialsToMap: Material[]) => (
    <>
      {materialsToMap?.length ? (
        materialsToMap.map(material => (
          <MaterialNarrowCardNew key={`material-${material.getKey()}`} material={material} materialAction={() => addMaterial(material)} />
        ))
      ) : (
        <NoContent />
      )}
    </>
  );

  const mapMaterials = (isFetching: boolean[], materialsType?: MaterialSource) => {
    const popularMaterials = materialsType === EKOOLIKOTT ? ekoolikottMaterials : opiqMaterials;
    const searchMaterialsToMap = !materialsType ? searchMaterials : popularMaterials;
    const popularSuggestedMaterials = materialsType === EKOOLIKOTT ? suggestedEkoolikottMaterials : suggestedOpiqMaterials;
    const suggestedMaterialsToMap = !materialsType ? suggestedMaterials : popularSuggestedMaterials;

    const mapFunction = (materials: Material[]) => (!materialsType ? mapRestMaterials(materials) : mapPopularMaterials(materials));

    const materialsToMap = search || isUserUkrainian ? searchMaterialsToMap : suggestedMaterialsToMap;

    return isFetching.some(fetching => !!fetching) ? (
      <Spinner containerClassName={'w-100 position-absolute'} />
    ) : (
      mapFunction(materialsToMap)
    );
  };

  const restMaterials = () =>
    mapMaterials([isFetchingSchoolabyMaterials, isFetchingExternalMaterials, isFetchingSuggestedExternal, isFetchingSuggestedSchoolaby]);

  const popularEKKMaterials = () => mapMaterials([isFetchingEkoolikottMaterials, isFetchingSuggestedEkoolikottMaterials], EKOOLIKOTT);

  const popularOpiqMaterials = () => mapMaterials([isFetchingSuggestedOpiqMaterials || isFetchingOpiqMaterials], OPIQ);

  return (
    <Card className={'materials-section'}>
      {!isUserUkrainian && (
        <>
          <div className={'sub-section popular'}>
            <h4>{translate('schoolabyApp.marketplace.popularOpiq')}</h4>
            <div className={'content'}>{popularOpiqMaterials()}</div>
          </div>
          <div className={'sub-section popular'}>
            <h4>{translate('schoolabyApp.marketplace.popularEKK')}</h4>
            <div className={'content'}>{popularEKKMaterials()}</div>
          </div>
        </>
      )}
      <div className={'sub-section rest'}>
        <h4>{translate('schoolabyApp.marketplace.suggestedMaterials')}</h4>
        <div className={'content'}>{restMaterials()}</div>
      </div>
    </Card>
  );
};

const mapStateToProps = ({ authentication: { account } }: IRootState) => ({
  country: account.country,
  isUserUkrainian: account.country === COUNTRY.UKRAINE,
});

type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(MaterialsSection);
