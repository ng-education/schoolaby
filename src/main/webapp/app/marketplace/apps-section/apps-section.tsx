import React, { FC, useState } from 'react';
import { Card, Row } from 'reactstrap';
import { translate } from 'react-jhipster';
import { useGetLtiConfigurations } from 'app/shared/services/lti-api';
import AppCardNew from 'app/shared/layout/lti/app-card-new/app-card-new';
import { ILtiResource } from 'app/shared/model/lti-resource.model';
import { isLtiAdvantage } from 'app/shared/util/lti-utils';
import { LTI_MODAL_ACTION_CREATE, LtiCreateModal, LtiCreateModalAction } from 'app/shared/layout/lti/lti-create-modal';
import { v4 as uuidv4 } from 'uuid';
import { createPortal } from 'react-dom';

interface AppsSectionProps {
  journeyId: number;
}
const AppsSection: FC<AppsSectionProps> = ({ journeyId }) => {
  const { data: ltiConfigs } = useGetLtiConfigurations({ journeyId }, !!journeyId);
  const [activeLtiResource, setActiveLtiResource] = useState<ILtiResource>();
  const [ltiModalAction, setLtiModalAction] = useState<LtiCreateModalAction>(LTI_MODAL_ACTION_CREATE);
  const [showLtiModal, setShowLtiModal] = useState(false);
  const toggleLtiModal = () => setShowLtiModal(!showLtiModal);

  const triggerConfiguration = index => {
    const app = ltiConfigs[index].ltiApp;
    const activeResource: ILtiResource = {
      ltiApp: app,
      title: '',
      description: '',
      resourceLinkId: isLtiAdvantage(app.version) ? null : uuidv4(),
    };

    setActiveLtiResource(activeResource);
    setLtiModalAction(LTI_MODAL_ACTION_CREATE);
    toggleLtiModal();
  };

  return (
    <div className="apps-section">
      <Card>
        <h4>{translate('schoolabyApp.marketplace.apps.associatedApps')}</h4>
        <Row>
          {ltiConfigs?.map((config, index) => (
            <AppCardNew
              md={6}
              lg={4}
              imageSrc={config?.ltiApp?.imageUrl}
              key={config?.id}
              title={config?.ltiApp?.name}
              description={config?.ltiApp?.description}
              consumerKey={config?.consumerKey}
              sharedSecret={config?.sharedSecret}
              ltiVersion={config?.ltiApp?.version}
              onClick={() => triggerConfiguration(index)}
              index={index}
              configurationWarningsEnabled
            />
          ))}
        </Row>
      </Card>
      {createPortal(
        <LtiCreateModal
          journeyId={+journeyId}
          ltiResource={activeLtiResource}
          showModal={showLtiModal}
          onClose={toggleLtiModal}
          title={translate('schoolabyApp.assignment.lti.edit')}
          action={ltiModalAction}
        />,
        document.getElementById('lti-form')
      )}
    </div>
  );
};

export default AppsSection;
