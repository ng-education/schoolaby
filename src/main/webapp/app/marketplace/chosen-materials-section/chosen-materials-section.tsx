import React, { FC } from 'react';

import './chosen-materials-section.scss';
import { Card } from 'reactstrap';
import MaterialWideCardNew from 'app/shared/layout/material-card-new/material-wide-card-new/material-wide-card-new';
import { useMaterialState } from 'app/shared/contexts/material-context';
import CustomButton from 'app/shared/layout/custom-button/custom-button';
import { ASSIGNMENT, MILESTONE } from 'app/shared/util/entity-utils';
import { translate } from 'react-jhipster';
import { useEntityToUpdateState } from 'app/shared/contexts/entity-update-context';
import { useHistory } from 'react-router-dom';
import { usePatchAssignment } from 'app/shared/services/assignment-api';
import { IAssignment } from 'app/shared/model/assignment.model';
import { HistoryStateProps } from 'app/marketplace/marketplace';
import { IMilestone } from 'app/shared/model/milestone.model';

interface ChosenMaterialsSectionProps {
  entityType: typeof ASSIGNMENT | typeof MILESTONE;
  back: Location;
}

const ChosenMaterialsSection: FC<ChosenMaterialsSectionProps> = ({ entityType, back }) => {
  const history = useHistory();
  const { entity } = useEntityToUpdateState();
  const { materials, removeMaterial } = useMaterialState();
  const patchAssignment = usePatchAssignment(entity?.id);
  const translationStringPrefix =
    entityType === ASSIGNMENT ? 'schoolabyApp.marketplace.assignment.' : 'schoolabyApp.marketplace.milestone.';

  const handleCancel = () => {
    history.replace({
      ...back,
      state: { ...entity },
    });
  };

  const handleAdd = () => {
    const ltiResources =
      entityType === ASSIGNMENT && materials.filter(material => material.isLti()).map(material => material.getLtiResource());
    const mats = materials.filter(material => !material.isLti()).map(material => material.getCopy());

    const historyState = history.location.state as HistoryStateProps<IAssignment | IMilestone>;
    if (historyState.patch) {
      if (entityType === ASSIGNMENT) {
        patchAssignment.mutateAsync({
          materials: mats,
          ltiResources,
        });
      }
      if (entityType === MILESTONE) {
        console.error('Milestone patch not implemented');
      }
    }

    history.replace({
      ...back,
      state: {
        ...entity,
        materials: mats,
        ltiResources,
      },
    });
  };

  return (
    <Card className={'chosen-materials-section'}>
      <h4>{translate(translationStringPrefix + 'chosen', { param: entity?.title })}</h4>
      {materials?.length ? (
        <div className={'chosen-materials-list d-flex flex-column mb-4'}>
          {materials?.map(material => (
            <MaterialWideCardNew
              key={`material-${material.getKey()}`}
              material={material}
              materialAction={() => removeMaterial(material)}
              deletable
            />
          ))}
        </div>
      ) : (
        <div className={'w-100 h-100 d-flex flex-column align-items-center justify-content-center mt-2 mb-2'}>
          <img alt={''} src={'content/images/happy_folder.svg'} />
          <p className={'empty d-flex'}>{translate('schoolabyApp.marketplace.noChosenMaterials')}</p>
        </div>
      )}
      <div className={'buttons-container d-flex flex-column flex-lg-row justify-content-end'}>
        <CustomButton title={translate('entity.action.cancel')} outline buttonType={'cancel'} onClick={handleCancel} />
        <CustomButton
          title={translate(translationStringPrefix + 'add')}
          buttonType={'primary'}
          onClick={handleAdd}
          disabled={!materials?.length}
        />
      </div>
    </Card>
  );
};

export default ChosenMaterialsSection;
