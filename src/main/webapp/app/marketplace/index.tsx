import React from 'react';
import ErrorBoundaryUpdateEntityRoute from 'app/shared/error/error-boundary-update-entity-route';
import Marketplace from 'app/marketplace/marketplace';
import { Switch } from 'react-router-dom';
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';
import PageNotFound from 'app/shared/error/page-not-found';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryUpdateEntityRoute exact path={`${match.url}/assignment/:entityId`} component={Marketplace} />
      <ErrorBoundaryUpdateEntityRoute exact path={`${match.url}/assignment/new`} component={Marketplace} />
      <ErrorBoundaryUpdateEntityRoute exact path={`${match.url}/milestone/:entityId`} component={Marketplace} />
      <ErrorBoundaryUpdateEntityRoute exact path={`${match.url}/milestone/new`} component={Marketplace} />
      <ErrorBoundaryRoute component={PageNotFound} />
    </Switch>
  </>
);

export default Routes;
