import React, { useEffect, useState } from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Table } from 'reactstrap';
import { getSortState, TextFormat, Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities, reset } from './grading-scheme-value.reducer';
import { APP_DATE_TIME_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';
import { overridePaginationStateWithQueryParams } from 'app/shared/util/entity-utils';

interface IGradingSchemeValueProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

const GradingSchemeValue = (props: IGradingSchemeValueProps) => {
  const [paginationState, setPaginationState] = useState(
    overridePaginationStateWithQueryParams(getSortState(props.location, ITEMS_PER_PAGE, 'id'), props.location.search)
  );
  const [sorting, setSorting] = useState(false);

  const getAllEntities = () => {
    props.getEntities(paginationState.activePage - 1, paginationState.itemsPerPage, `${paginationState.sort},${paginationState.order}`);
  };

  const resetAll = () => {
    props.reset();
    setPaginationState({
      ...paginationState,
      activePage: 1,
    });
    props.getEntities();
  };

  useEffect(() => {
    resetAll();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      resetAll();
    }
  }, [props.updateSuccess]);

  useEffect(() => {
    getAllEntities();
  }, [paginationState.activePage]);

  const handleLoadMore = () => {
    if ((window as any).pageYOffset > 0) {
      setPaginationState({
        ...paginationState,
        activePage: paginationState.activePage + 1,
      });
    }
  };

  useEffect(() => {
    if (sorting) {
      getAllEntities();
      setSorting(false);
    }
  }, [sorting]);

  const sort = p => () => {
    props.reset();
    setPaginationState({
      ...paginationState,
      activePage: 1,
      order: paginationState.order === 'asc' ? 'desc' : 'asc',
      sort: p,
    });
    setSorting(true);
  };

  const { gradingSchemeValueList, match, loading } = props;
  return (
    <div>
      <h2 id="grading-scheme-value-heading">
        <Translate contentKey="schoolabyApp.gradingSchemeValue.home.title">Grading Scheme Values</Translate>
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon={'plus'} />
          &nbsp;
          <Translate contentKey="schoolabyApp.gradingSchemeValue.home.createLabel">Create new Grading Scheme Value</Translate>
        </Link>
      </h2>
      <div className="table-responsive">
        <InfiniteScroll
          pageStart={paginationState.activePage}
          loadMore={handleLoadMore}
          hasMore={paginationState.activePage - 1 < props.links.next}
          loader={<div className="loader">Loading ...</div>}
          threshold={0}
          initialLoad={false}
        >
          {gradingSchemeValueList && gradingSchemeValueList.length > 0 ? (
            <Table responsive>
              <thead>
                <tr>
                  <th className="hand" onClick={sort('id')}>
                    <Translate contentKey="global.field.id">ID</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={sort('grade')}>
                    <Translate contentKey="schoolabyApp.gradingSchemeValue.grade">Grade</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={sort('percentageRange')}>
                    <Translate contentKey="schoolabyApp.gradingSchemeValue.percentageRange">Percentage Range</Translate>{' '}
                    <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={sort('deleted')}>
                    <Translate contentKey="schoolabyApp.gradingSchemeValue.deleted">Deleted</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th>
                    <Translate contentKey="schoolabyApp.gradingSchemeValue.gradingScheme">Grading Scheme</Translate>{' '}
                    <FontAwesomeIcon icon="sort" />
                  </th>
                  <th />
                </tr>
              </thead>
              <tbody>
                {gradingSchemeValueList.map((gradingSchemeValue, i) => (
                  <tr key={`entity-${i}`}>
                    <td>
                      <Button tag={Link} to={`${match.url}/${gradingSchemeValue.id}`} color="link" size="sm">
                        {gradingSchemeValue.id}
                      </Button>
                    </td>
                    <td>{gradingSchemeValue.grade}</td>
                    <td>{gradingSchemeValue.percentageRangeStart}</td>
                    <td>
                      {gradingSchemeValue.deleted ? (
                        <TextFormat type="date" value={gradingSchemeValue.deleted} format={APP_DATE_TIME_FORMAT} />
                      ) : null}
                    </td>
                    <td>
                      {gradingSchemeValue.gradingSchemeId ? (
                        <Link to={`grading-scheme/${gradingSchemeValue.gradingSchemeId}`}>{gradingSchemeValue.gradingSchemeId}</Link>
                      ) : (
                        ''
                      )}
                    </td>
                    <td className="text-right">
                      <div className="btn-group flex-btn-group-container">
                        <Button tag={Link} to={`${match.url}/${gradingSchemeValue.id}`} color="info" size="sm">
                          <FontAwesomeIcon icon="eye" />{' '}
                          <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.view">View</Translate>
                          </span>
                        </Button>
                        <Button tag={Link} to={`${match.url}/${gradingSchemeValue.id}/edit`} color="primary" size="sm">
                          <FontAwesomeIcon icon="pencil-alt" />{' '}
                          <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.edit">Edit</Translate>
                          </span>
                        </Button>
                        <Button tag={Link} to={`${match.url}/${gradingSchemeValue.id}/delete`} color="danger" size="sm">
                          <FontAwesomeIcon icon="trash" />{' '}
                          <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.delete">Delete</Translate>
                          </span>
                        </Button>
                      </div>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          ) : (
            !loading && (
              <div className="alert alert-warning">
                <Translate contentKey="schoolabyApp.gradingSchemeValue.home.notFound">No Grading Scheme Values found</Translate>
              </div>
            )
          )}
        </InfiniteScroll>
      </div>
    </div>
  );
};

const mapStateToProps = ({ gradingSchemeValue }: IRootState) => ({
  gradingSchemeValueList: gradingSchemeValue.entities,
  loading: gradingSchemeValue.loading,
  totalItems: gradingSchemeValue.totalItems,
  links: gradingSchemeValue.links,
  entity: gradingSchemeValue.entity,
  updateSuccess: gradingSchemeValue.updateSuccess,
});

const mapDispatchToProps = {
  getEntities,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(GradingSchemeValue);
