import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import GradingScheme from './grading-scheme';
import GradingSchemeDetail from './grading-scheme-detail';
import GradingSchemeUpdate from './grading-scheme-update';
import GradingSchemeDeleteDialog from './grading-scheme-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={GradingSchemeUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={GradingSchemeUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={GradingSchemeDetail} />
      <ErrorBoundaryRoute path={match.url} component={GradingScheme} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={GradingSchemeDeleteDialog} />
  </>
);

export default Routes;
