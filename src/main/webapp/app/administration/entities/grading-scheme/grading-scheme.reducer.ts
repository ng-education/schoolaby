import axios from 'axios';
import {
  ICrudDeleteAction,
  ICrudGetAction,
  ICrudGetAllAction,
  ICrudPutAction,
  loadMoreDataWhenScrolled,
  parseHeaderForLinks,
} from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { FAILURE, REQUEST, SUCCESS } from 'app/shared/reducers/action-type.util';

import { defaultValue, IGradingScheme } from 'app/shared/model/grading-scheme.model';

export const ACTION_TYPES = {
  FETCH_GRADINGSCHEME_LIST: 'gradingScheme/FETCH_GRADINGSCHEME_LIST',
  FETCH_GRADINGSCHEME: 'gradingScheme/FETCH_GRADINGSCHEME',
  CREATE_GRADINGSCHEME: 'gradingScheme/CREATE_GRADINGSCHEME',
  UPDATE_GRADINGSCHEME: 'gradingScheme/UPDATE_GRADINGSCHEME',
  DELETE_GRADINGSCHEME: 'gradingScheme/DELETE_GRADINGSCHEME',
  RESET: 'gradingScheme/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IGradingScheme>,
  entity: defaultValue,
  links: { next: 0 },
  updating: false,
  totalItems: 0,
  updateSuccess: false,
};

export type GradingSchemeState = Readonly<typeof initialState>;

// Reducer

export default (state: GradingSchemeState = initialState, action): GradingSchemeState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_GRADINGSCHEME_LIST):
    case REQUEST(ACTION_TYPES.FETCH_GRADINGSCHEME):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_GRADINGSCHEME):
    case REQUEST(ACTION_TYPES.UPDATE_GRADINGSCHEME):
    case REQUEST(ACTION_TYPES.DELETE_GRADINGSCHEME):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_GRADINGSCHEME_LIST):
    case FAILURE(ACTION_TYPES.FETCH_GRADINGSCHEME):
    case FAILURE(ACTION_TYPES.CREATE_GRADINGSCHEME):
    case FAILURE(ACTION_TYPES.UPDATE_GRADINGSCHEME):
    case FAILURE(ACTION_TYPES.DELETE_GRADINGSCHEME):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_GRADINGSCHEME_LIST): {
      const links = parseHeaderForLinks(action.payload.headers.link);

      return {
        ...state,
        loading: false,
        links,
        entities: loadMoreDataWhenScrolled(state.entities, action.payload.data, links),
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
      };
    }
    case SUCCESS(ACTION_TYPES.FETCH_GRADINGSCHEME):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_GRADINGSCHEME):
    case SUCCESS(ACTION_TYPES.UPDATE_GRADINGSCHEME):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_GRADINGSCHEME):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/grading-schemes';

// Actions

export const getEntities: ICrudGetAllAction<IGradingScheme> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_GRADINGSCHEME_LIST,
    payload: axios.get<IGradingScheme>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};

export const getEntity: ICrudGetAction<IGradingScheme> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_GRADINGSCHEME,
    payload: axios.get<IGradingScheme>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IGradingScheme> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_GRADINGSCHEME,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const updateEntity: ICrudPutAction<IGradingScheme> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_GRADINGSCHEME,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IGradingScheme> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_GRADINGSCHEME,
    payload: axios.delete(requestUrl),
  });
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
