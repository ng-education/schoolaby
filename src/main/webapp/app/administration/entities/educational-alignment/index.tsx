import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import EducationalAlignment from './educational-alignment';
import EducationalAlignmentDetail from './educational-alignment-detail';
import EducationalAlignmentUpdate from './educational-alignment-update';
import EducationalAlignmentDeleteDialog from './educational-alignment-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={EducationalAlignmentUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={EducationalAlignmentUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={EducationalAlignmentDetail} />
      <ErrorBoundaryRoute path={match.url} component={EducationalAlignment} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={EducationalAlignmentDeleteDialog} />
  </>
);

export default Routes;
