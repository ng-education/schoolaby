import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Label, Row } from 'reactstrap';
import { AvField, AvForm, AvGroup, AvInput } from 'availity-reactstrap-validation';
import { Translate, translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntities as getEducationalAlignments } from 'app/administration/entities/educational-alignment/educational-alignment.reducer';
import { createEntity, getEntity, reset, updateEntity } from './educational-alignment.reducer';
import { convertDateTimeFromServer, convertDateTimeToServer, getStartOfDay } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

interface IEducationalAlignmentUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

const EducationalAlignmentUpdate = (props: IEducationalAlignmentUpdateProps) => {
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { educationalAlignmentEntity, educationalAlignments, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/educational-alignment');
  };

  useEffect(() => {
    if (!isNew) {
      props.getEntity(props.match.params.id);
    }

    props.getEducationalAlignments();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    values.startDate = convertDateTimeToServer(values.startDate);
    values.endDate = convertDateTimeToServer(values.endDate);
    values.deleted = convertDateTimeToServer(values.deleted);

    if (errors.length === 0) {
      const entity = {
        ...educationalAlignmentEntity,
        ...values,
        children: mapIdList(values.children),
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="schoolabyApp.educationalAlignment.home.createOrEditLabel">
            <Translate contentKey="schoolabyApp.educationalAlignment.home.createOrEditLabel">
              Create or edit a EducationalAlignment
            </Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : educationalAlignmentEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="educational-alignment-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="educational-alignment-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="titleLabel" for="educational-alignment-title">
                  <Translate contentKey="schoolabyApp.educationalAlignment.title">Title</Translate>
                </Label>
                <AvField
                  id="educational-alignment-title"
                  type="text"
                  name="title"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="alignmentTypeLabel" for="educational-alignment-alignmentType">
                  <Translate contentKey="schoolabyApp.educationalAlignment.alignmentType">Alignment Type</Translate>
                </Label>
                <AvField
                  id="educational-alignment-alignmentType"
                  type="text"
                  name="alignmentType"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="educationalFrameworkLabel" for="educational-alignment-educationalFramework">
                  <Translate contentKey="schoolabyApp.educationalAlignment.educationalFramework">Educational Framework</Translate>
                </Label>
                <AvField
                  id="educational-alignment-educationalFramework"
                  type="text"
                  name="educationalFramework"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="targetNameLabel" for="educational-alignment-targetName">
                  <Translate contentKey="schoolabyApp.educationalAlignment.targetName">Target Name</Translate>
                </Label>
                <AvField
                  id="educational-alignment-targetName"
                  type="text"
                  name="targetName"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="targetUrlLabel" for="educational-alignment-targetUrl">
                  <Translate contentKey="schoolabyApp.educationalAlignment.targetUrl">Target Url</Translate>
                </Label>
                <AvField id="educational-alignment-targetUrl" type="text" name="targetUrl" />
              </AvGroup>
              <AvGroup>
                <Label id="startDateLabel" for="educational-alignment-startDate">
                  <Translate contentKey="schoolabyApp.educationalAlignment.startDate">Start Date</Translate>
                </Label>
                <AvInput
                  id="educational-alignment-startDate"
                  type="datetime-local"
                  className="form-control"
                  name="startDate"
                  placeholder={'YYYY-MM-DD HH:mm'}
                  value={isNew ? getStartOfDay() : convertDateTimeFromServer(props.educationalAlignmentEntity.startDate)}
                />
              </AvGroup>
              <AvGroup>
                <Label id="endDateLabel" for="educational-alignment-endDate">
                  <Translate contentKey="schoolabyApp.educationalAlignment.endDate">End Date</Translate>
                </Label>
                <AvInput
                  id="educational-alignment-endDate"
                  type="datetime-local"
                  className="form-control"
                  name="endDate"
                  placeholder={'YYYY-MM-DD HH:mm'}
                  value={isNew ? getStartOfDay() : convertDateTimeFromServer(props.educationalAlignmentEntity.endDate)}
                />
              </AvGroup>
              <AvGroup>
                <Label id="deletedLabel" for="educational-alignment-deleted">
                  <Translate contentKey="schoolabyApp.educationalAlignment.deleted">Deleted</Translate>
                </Label>
                <AvInput
                  id="educational-alignment-deleted"
                  type="datetime-local"
                  className="form-control"
                  name="deleted"
                  placeholder={'YYYY-MM-DD HH:mm'}
                  value={isNew ? getStartOfDay() : convertDateTimeFromServer(props.educationalAlignmentEntity.deleted)}
                />
              </AvGroup>
              <AvGroup>
                <Label for="educational-alignment-children">
                  <Translate contentKey="schoolabyApp.educationalAlignment.children">Children</Translate>
                </Label>
                <AvInput
                  id="educational-alignment-children"
                  type="select"
                  multiple
                  className="form-control"
                  name="children"
                  value={educationalAlignmentEntity.children && educationalAlignmentEntity.children.map(e => e.id)}
                >
                  <option value="" key="0" />
                  {educationalAlignments
                    ? educationalAlignments.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/educational-alignment" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  educationalAlignments: storeState.educationalAlignment.entities,
  milestones: storeState.milestone.entities,
  materials: storeState.material.entities,
  assignments: storeState.assignment.entities,
  educationalAlignmentEntity: storeState.educationalAlignment.entity,
  loading: storeState.educationalAlignment.loading,
  updating: storeState.educationalAlignment.updating,
  updateSuccess: storeState.educationalAlignment.updateSuccess,
});

const mapDispatchToProps = {
  getEducationalAlignments,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(EducationalAlignmentUpdate);
