import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row } from 'reactstrap';
import { TextFormat, Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './person-role.reducer';
import { APP_DATE_TIME_FORMAT } from 'app/config/constants';

interface IPersonRoleDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

const PersonRoleDetail = (props: IPersonRoleDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { personRoleEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="schoolabyApp.personRole.detail.title">PersonRole</Translate> [<b>{personRoleEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="marker">
              <Translate contentKey="schoolabyApp.personRole.marker">Marker</Translate>
            </span>
          </dt>
          <dd>{personRoleEntity.role}</dd>
          <dt>
            <span id="active">
              <Translate contentKey="schoolabyApp.personRole.active">Active</Translate>
            </span>
          </dt>
          <dd>{personRoleEntity?.active ? 'true' : 'false'}</dd>
          <dt>
            <span id="startDate">
              <Translate contentKey="schoolabyApp.personRole.startDate">Start Date</Translate>
            </span>
          </dt>
          <dd>
            {personRoleEntity.startDate ? (
              <TextFormat value={personRoleEntity.startDate} type="date" format={APP_DATE_TIME_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="endDate">
              <Translate contentKey="schoolabyApp.personRole.endDate">End Date</Translate>
            </span>
          </dt>
          <dd>
            {personRoleEntity.endDate ? <TextFormat value={personRoleEntity.endDate} type="date" format={APP_DATE_TIME_FORMAT} /> : null}
          </dd>
          <dt>
            <span id="providerEhisId">
              <Translate contentKey="schoolabyApp.personRole.providerEhisId">Provider Ehis Id</Translate>
            </span>
          </dt>
          <dd>{personRoleEntity.school.ehisId}</dd>
          <dt>
            <span id="providerRegNr">
              <Translate contentKey="schoolabyApp.personRole.providerRegNr">Provider Reg Nr</Translate>
            </span>
          </dt>
          <dd>{personRoleEntity.school.regNr}</dd>
          <dt>
            <span id="providerName">
              <Translate contentKey="schoolabyApp.personRole.providerName">Provider Name</Translate>
            </span>
          </dt>
          <dd>{personRoleEntity.school.name}</dd>
          <dt>
            <span id="grade">
              <Translate contentKey="schoolabyApp.personRole.grade">Grade</Translate>
            </span>
          </dt>
          <dd>{personRoleEntity.grade}</dd>
          <dt>
            <span id="parallel">
              <Translate contentKey="schoolabyApp.personRole.parallel">Parallel</Translate>
            </span>
          </dt>
          <dd>{personRoleEntity.parallel}</dd>
          <dt>
            <span id="deleted">
              <Translate contentKey="schoolabyApp.personRole.deleted">Deleted</Translate>
            </span>
          </dt>
          <dd>
            {personRoleEntity.deleted ? <TextFormat value={personRoleEntity.deleted} type="date" format={APP_DATE_TIME_FORMAT} /> : null}
          </dd>
          <dt>
            <Translate contentKey="schoolabyApp.personRole.person">Person</Translate>
          </dt>
          <dd>{personRoleEntity.personId ? personRoleEntity.personId : ''}</dd>
        </dl>
        <Button tag={Link} to="/person-role" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/person-role/${personRoleEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ personRole }: IRootState) => ({
  personRoleEntity: personRole.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PersonRoleDetail);
