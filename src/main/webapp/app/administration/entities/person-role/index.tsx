import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import PersonRole from './person-role';
import PersonRoleDetail from './person-role-detail';
import PersonRoleUpdate from './person-role-update';
import PersonRoleDeleteDialog from './person-role-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={PersonRoleUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={PersonRoleUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={PersonRoleDetail} />
      <ErrorBoundaryRoute path={match.url} component={PersonRole} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={PersonRoleDeleteDialog} />
  </>
);

export default Routes;
