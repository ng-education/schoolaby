import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Label, Row } from 'reactstrap';
import { AvField, AvForm, AvGroup, AvInput } from 'availity-reactstrap-validation';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { createEntity, getEntity, reset, updateEntity } from './person-role.reducer';
import { convertDateTimeFromServer, convertDateTimeToServer, getStartOfDay } from 'app/shared/util/date-utils';

interface IPersonRoleUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

const PersonRoleUpdate = (props: IPersonRoleUpdateProps) => {
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { personRoleEntity, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/person-role');
  };

  useEffect(() => {
    if (!isNew) {
      props.getEntity(props.match.params.id);
    }
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    values.startDate = convertDateTimeToServer(values.startDate);
    values.endDate = convertDateTimeToServer(values.endDate);
    values.deleted = convertDateTimeToServer(values.deleted);

    if (errors.length === 0) {
      const entity = {
        ...personRoleEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="schoolabyApp.personRole.home.createOrEditLabel">
            <Translate contentKey="schoolabyApp.personRole.home.createOrEditLabel">Create or edit a PersonRole</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : personRoleEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="person-role-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="person-role-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="markerLabel" for="person-role-marker">
                  <Translate contentKey="schoolabyApp.personRole.marker">Marker</Translate>
                </Label>
                <AvField id="person-role-marker" type="text" name="marker" />
              </AvGroup>
              <AvGroup check>
                <Label id="activeLabel">
                  <AvInput id="person-role-active" type="checkbox" className="form-check-input" name="active" />
                  <Translate contentKey="schoolabyApp.personRole.active">Active</Translate>
                </Label>
              </AvGroup>
              <AvGroup>
                <Label id="startDateLabel" for="person-role-startDate">
                  <Translate contentKey="schoolabyApp.personRole.startDate">Start Date</Translate>
                </Label>
                <AvInput
                  id="person-role-startDate"
                  type="datetime-local"
                  className="form-control"
                  name="startDate"
                  placeholder={'YYYY-MM-DD HH:mm'}
                  value={isNew ? getStartOfDay() : convertDateTimeFromServer(props.personRoleEntity.startDate)}
                />
              </AvGroup>
              <AvGroup>
                <Label id="endDateLabel" for="person-role-endDate">
                  <Translate contentKey="schoolabyApp.personRole.endDate">End Date</Translate>
                </Label>
                <AvInput
                  id="person-role-endDate"
                  type="datetime-local"
                  className="form-control"
                  name="endDate"
                  placeholder={'YYYY-MM-DD HH:mm'}
                  value={isNew ? getStartOfDay() : convertDateTimeFromServer(props.personRoleEntity.endDate)}
                />
              </AvGroup>
              <AvGroup>
                <Label id="providerEhisIdLabel" for="person-role-providerEhisId">
                  <Translate contentKey="schoolabyApp.personRole.providerEhisId">Provider Ehis Id</Translate>
                </Label>
                <AvField id="person-role-providerEhisId" type="text" name="providerEhisId" />
              </AvGroup>
              <AvGroup>
                <Label id="providerRegNrLabel" for="person-role-providerRegNr">
                  <Translate contentKey="schoolabyApp.personRole.providerRegNr">Provider Reg Nr</Translate>
                </Label>
                <AvField id="person-role-providerRegNr" type="text" name="providerRegNr" />
              </AvGroup>
              <AvGroup>
                <Label id="providerNameLabel" for="person-role-providerName">
                  <Translate contentKey="schoolabyApp.personRole.providerName">Provider Name</Translate>
                </Label>
                <AvField id="person-role-providerName" type="text" name="providerName" />
              </AvGroup>
              <AvGroup>
                <Label id="gradeLabel" for="person-role-grade">
                  <Translate contentKey="schoolabyApp.personRole.grade">Grade</Translate>
                </Label>
                <AvField id="person-role-grade" type="text" name="grade" />
              </AvGroup>
              <AvGroup>
                <Label id="parallelLabel" for="person-role-parallel">
                  <Translate contentKey="schoolabyApp.personRole.parallel">Parallel</Translate>
                </Label>
                <AvField id="person-role-parallel" type="text" name="parallel" />
              </AvGroup>
              <AvGroup>
                <Label id="deletedLabel" for="person-role-deleted">
                  <Translate contentKey="schoolabyApp.personRole.deleted">Deleted</Translate>
                </Label>
                <AvInput
                  id="person-role-deleted"
                  type="datetime-local"
                  className="form-control"
                  name="deleted"
                  placeholder={'YYYY-MM-DD HH:mm'}
                  value={isNew ? getStartOfDay() : convertDateTimeFromServer(props.personRoleEntity.deleted)}
                />
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/person-role" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  personRoleEntity: storeState.personRole.entity,
  loading: storeState.personRole.loading,
  updating: storeState.personRole.updating,
  updateSuccess: storeState.personRole.updateSuccess,
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PersonRoleUpdate);
