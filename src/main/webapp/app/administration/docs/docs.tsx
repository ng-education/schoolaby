import './docs.scss';
import React from 'react';

const DocsPage = () => (
  <div>
    <iframe
      src="../swagger-ui/index.html"
      width="100%"
      className={'swagger-iframe'}
      title="Swagger UI"
      seamless
      style={{ border: 'none' }}
      allowFullScreen
    />
  </div>
);

export default DocsPage;
