import React, { FC } from 'react';
import { Table } from 'reactstrap';
import { TextFormat, translate } from 'react-jhipster';
import Avatar from 'app/shared/layout/header/avatar';
import { getFullName } from 'app/shared/util/string-utils';
import Icon from 'app/shared/icons';
import { ICON_GREY } from 'app/shared/util/color-utils';
import { IJourneyTeacher } from 'app/shared/model/journey-teacher.model';
import { APP_LOCAL_DATE_TIME_FORMAT } from 'app/config/constants';
import { useDeleteTeacherFromJourney } from 'app/shared/services/journey-api';

interface CoTeachersListViewProps {
  journeyId: number;
  coTeachers: IJourneyTeacher[];
}

const CoTeachersListView: FC<CoTeachersListViewProps> = ({ journeyId, coTeachers }) => {
  const { mutate: deleteTeacherFromJourney } = useDeleteTeacherFromJourney();

  const deleteTeacher = (userId: number) => {
    deleteTeacherFromJourney({ journeyId, userId });
  };

  const TeacherRow: FC<{ journeyTeacher: IJourneyTeacher }> = ({ journeyTeacher }) => {
    return (
      <tr>
        <td>
          <Avatar fullName={getFullName(journeyTeacher.user)} showName />
        </td>
        <td className={'text-center'}>
          {journeyTeacher.joinedDate ? (
            <TextFormat type="date" format={APP_LOCAL_DATE_TIME_FORMAT} value={journeyTeacher.joinedDate} />
          ) : (
            '-'
          )}
        </td>
        <td className={'text-center delete'}>
          <div className={'icon-delete'} onClick={() => deleteTeacher(journeyTeacher.user.id)}>
            <Icon name={'trashNew'} className={'m-2'} width={'18px'} height={'18px'} stroke={ICON_GREY} />
          </div>
        </td>
      </tr>
    );
  };

  return (
    <Table striped bordered>
      <thead>
        <tr>
          <th>{translate('global.form.name')}</th>
          <th className={'text-center'}>{translate('schoolabyApp.journey.joinDate')}</th>
          <th className={'text-center'}>{translate('entity.action.delete')}</th>
        </tr>
      </thead>
      <tbody>
        {coTeachers?.map(teacher => (
          <TeacherRow key={teacher.user.id} journeyTeacher={teacher} />
        ))}
      </tbody>
    </Table>
  );
};

export default CoTeachersListView;
