import React, { FC, useEffect, useState } from 'react';
import { Card, Col, Progress, Row } from 'reactstrap';

import { IJourney } from 'app/shared/model/journey.model';
import Icon from 'app/shared/icons';
import { ICON_GREY } from 'app/shared/util/color-utils';
import Avatar from 'app/shared/layout/header/avatar';
import { Link } from 'react-router-dom';
import {
  filterUnreadTeacherNotificationsByJourneyId,
  getUniqueNotificationsByLink,
} from 'app/shared/layout/header/notification/notification-util';
import { useGetNotifications } from 'app/shared/services/notification-api';

import './journey-list-card.scss';
import NotificationCount from 'app/dashboard/journey-list-card/notification-counter/notification-count';
import ActiveTasksCard from 'app/dashboard/journey-list-card/inner-card/active-tasks-card';
import OverdueTasksCard from 'app/dashboard/journey-list-card/inner-card/overdue-tasks-card';
import StudentsCard from 'app/dashboard/journey-list-card/inner-card/students-card/students-card';
import QuestionsCard from 'app/dashboard/journey-list-card/inner-card/questions-card';
import { getSchoolImage } from 'app/shared/util/journey-utils';

interface JourneyListCardProps {
  journey: IJourney;
}

const JourneyListCard: FC<JourneyListCardProps> = ({ journey }) => {
  const { data: generalNotifications } = useGetNotifications({ type: 'GENERAL' }, false);
  const { data: messageNotifications } = useGetNotifications({ type: 'MESSAGE' }, false);
  const schoolImage = getSchoolImage(journey);

  const [unreadNotificationsCount, setUnreadNotificationsCount] = useState<number>(0);

  const SchoolIcon = () => {
    return schoolImage ? (
      <img className={'school-image mr-2'} src={schoolImage?.imageUrl} alt={schoolImage?.schoolName} />
    ) : (
      <Icon name={'home'} height={'24px'} width={'22px'} stroke={ICON_GREY} strokeWidth={'4px'} className={'mr-2'} />
    );
  };

  useEffect(() => {
    const uniqueMessagesByLink = getUniqueNotificationsByLink(messageNotifications || []);
    const allNotifications = [...(generalNotifications || []), ...uniqueMessagesByLink];
    setUnreadNotificationsCount(filterUnreadTeacherNotificationsByJourneyId(allNotifications, journey.id).length);
  }, [generalNotifications, messageNotifications]);

  return (
    <Card className={'journey-list-card'}>
      <Row className={'px-0 mx-0 h-100'}>
        <Col className={'my-0 base-info-column h-100'}>
          <Row className={'school d-flex align-items-center'}>
            <SchoolIcon />
            <p className={'ellipsis m-0'}>{schoolImage?.schoolName ? schoolImage?.schoolName : journey?.schools?.[0]?.name}</p>
          </Row>
          <Row className={'journey d-flex justify-content-between align-items-center'}>
            <Link to={`/journey/${journey.id}`}>
              <h3 className={'title ellipsis m-0'}>{journey?.title}</h3>
            </Link>
            <NotificationCount count={unreadNotificationsCount} />
          </Row>
          <Row>
            <Progress color={'success'} value={journey.progress} className={'w-100'}>
              <div className={`percent ${journey.progress === 0 && 'zero'}`}>{`${journey.progress}%`}</div>
            </Progress>
          </Row>
          <Row className={'teachers'}>
            <Avatar fullName={journey.teacherName} />
          </Row>
        </Col>
        <Col className={'my-0 h-100'}>
          <ActiveTasksCard journeyId={journey.id} journeyStudentCount={journey.studentCount} />
        </Col>
        <Col className={'h-100'}>
          <QuestionsCard journeyId={journey.id} />
        </Col>
        <Col className={'my-0 h-100'}>
          <OverdueTasksCard journeyId={journey.id} journeyStudentCount={journey.studentCount} />
        </Col>
        <Col className={'students-column h-100'}>
          <StudentsCard journeyId={journey.id} />
        </Col>
      </Row>
    </Card>
  );
};

export default JourneyListCard;
