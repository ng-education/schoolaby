import React, { FC } from 'react';
import './notification-count.scss';

interface NotificationCountProps {
  count: number;
  ariaLabel?: string;
}

const NotificationCount: FC<NotificationCountProps> = ({ count, ariaLabel }) => (
  <span aria-label={ariaLabel} className={`unread-notification-count ${!count ? 'no-unread' : ''}`}>
    {count}
  </span>
);

export default NotificationCount;
