import React, { FC } from 'react';
import { Card, CardBody, CardHeader, Col } from 'reactstrap';

import './inner-card.scss';
import NotificationCount from 'app/dashboard/journey-list-card/notification-counter/notification-count';

interface InnerCardProps {
  title: string;
  light?: boolean;
  emptyText?: string;
  showNotificationCount?: boolean;
  totalCount?: number;
  notificationCountLabel?: string;
  scroll?: boolean;
  children?: JSX.Element | JSX.Element[];
}

const InnerCard: FC<InnerCardProps> = ({
  title,
  totalCount,
  emptyText,
  notificationCountLabel,
  children,
  showNotificationCount = true,
  scroll = false,
  light = false,
}) => {
  return (
    <Card className={`inner-card h-100 ${light && 'light'} ${scroll && 'scrollable'}`}>
      <CardHeader className={'d-flex align-items-center justify-content-between'}>
        <h3 className={'m-0'}>{title}</h3>
        {showNotificationCount && <NotificationCount count={totalCount} ariaLabel={notificationCountLabel} />}
      </CardHeader>
      <CardBody>
        <Col className={'h-100'}>
          {children ? (
            children
          ) : (
            <div className={'w-100 h-100 d-flex flex-column align-items-center justify-content-center'}>
              <img alt={emptyText} src={'content/images/happy_folder.svg'} />
              <p className={'empty d-flex'}>{emptyText}</p>
            </div>
          )}
        </Col>
      </CardBody>
    </Card>
  );
};

export default InnerCard;
