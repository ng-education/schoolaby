import React, { FC } from 'react';
import { APP_DATE_FORMAT } from 'app/config/constants';
import Icon from 'app/shared/icons';
import { Row } from 'reactstrap';
import { Link } from 'react-router-dom';
import { TextFormat } from 'react-jhipster';

interface InnerCardRowProps {
  link: string;
  title: string;
  date: string;
  pupilSection: JSX.Element | string;
  shouldHaveHr: boolean;
}

const InnerCardRow: FC<InnerCardRowProps> = ({ link, title, date, pupilSection, shouldHaveHr }) => {
  return (
    <>
      <Row className="inner-card-row">
        <Link to={link}>
          <h4 className={'m-0 ellipsis'}>{title}</h4>
        </Link>
      </Row>
      <Row className={'d-flex align-items-center'}>
        <p className={'deadline'}>
          <TextFormat type="date" value={date} format={APP_DATE_FORMAT} />
        </p>
        <Icon name={'coloredUser'} width={'10'} height={'10'} viewBox={'0 0 8 12'} />
        <p data-testid={'submissions-count'}>{pupilSection}</p>
      </Row>
      {shouldHaveHr && <hr />}
    </>
  );
};

export default InnerCardRow;
