import React, { FC } from 'react';
import Avatar from 'app/shared/layout/header/avatar';
import Icon from 'app/shared/icons';
import { Row } from 'reactstrap/lib';

import './student-row.scss';
import { IJourneyStudent } from 'app/shared/model/journey-student.model';
import { getFullName } from 'app/shared/util/string-utils';
import { useToggleJourneyStudentPin } from 'app/shared/services/journey-api';
import { translate } from 'react-jhipster';

interface StudentRowProps {
  journeyId: number;
  journeyStudent: IJourneyStudent;
}

const StudentRow: FC<StudentRowProps> = ({ journeyId, journeyStudent }) => {
  const { mutate: toggleStudentPin } = useToggleJourneyStudentPin(journeyId);

  const onPinClicked = () => {
    toggleStudentPin(journeyStudent.user.id);
  };

  const getAvatarColor = () => {
    if (journeyStudent.averageGrade <= 50) {
      return 'bg-danger';
    } else if (journeyStudent.averageGrade <= 85) {
      return 'bg-warning';
    }

    return 'bg-success';
  };

  return (
    <Row className={'student-row d-flex align-items-center justify-content-between'}>
      <div className={'h-100'}>
        <Avatar fullName={getFullName(journeyStudent.user)} className="d-flex" avatarContainerClassName={getAvatarColor()} showName />
      </div>
      <div className={'h-100'}>
        <div onClick={onPinClicked} className="cursor-pointer">
          {journeyStudent.pinnedDate ? (
            <Icon width="15" height="15" name={'selectedPin'} ariaLabel={translate('schoolabyApp.journey.students.unpin')} />
          ) : (
            <Icon width="15" height="15" name={'pin'} ariaLabel={translate('schoolabyApp.journey.students.pin')} />
          )}
        </div>
      </div>
    </Row>
  );
};

export default StudentRow;
