import './student-dashboard.scss';
import React, { useEffect, useState } from 'react';
import { Storage, translate } from 'react-jhipster';
import { Col, Row } from 'reactstrap';
import EntityCard, { IEntityCardProps } from 'app/shared/layout/entity-card/entity-card';
import { AUTHORITIES, DASHBOARD_TOUR_SEEN, SIGN_UP_CODE } from 'app/config/constants';
import { ColorType, PRIMARY } from 'app/shared/util/color-utils';
import { IJourney, JourneyState } from 'app/shared/model/journey.model';
import DashboardMessages from 'app/dashboard/dashboard-messages';
import { Spinner } from 'app/shared/layout/spinner/spinner';
import { useHistory } from 'react-router-dom';
import ListFooterButton from 'app/shared/layout/list-footer-button/list-footer-button';
import { NavigationPath } from 'app/shared/util/navigation-utils';
import { EntityState } from 'app/shared/model/enumerations/entity-state';
import { DateTime } from 'luxon';
import { useGetJourneysPaginated, useGetUserJourneysPaginated } from 'app/shared/services/journey-api';
import { getAssignments } from 'app/shared/services/assignment-api';
import Tour from 'reactour';
import { IRootState } from 'app/shared/reducers';
import { hasAnyAuthority } from 'app/shared/auth/private-route';
import { connect } from 'react-redux';
import flatMap from 'lodash/flatMap';
import GreetingModal from 'app/shared/layout/greeting-modal/greeting-modal';
import HeadingNew, { SUB_HEADING } from 'app/shared/layout/heading/heading-new';
import AddButtonNew from 'app/shared/layout/add-button-new/add-button-new';

const StudentDashboard = ({ isAllowedToModify, isStudent, account }: StateProps) => {
  const history = useHistory();
  const [spinner, setSpinner] = useState(true);
  const [activeTasks, setActiveTasks] = useState(null);
  const [overdueTasks, setOverdueTasks] = useState(null);
  const [modal, setModal] = useState(false);
  const [isTourOpen, setIsTourOpen] = useState(false);
  const [activeJourneysExist, setActiveJourneysExist] = useState(false);

  const { isSuccess, data: allJourneys } = useGetJourneysPaginated();
  const { isSuccess: activeJourneysIsSuccess, data: activeJourneys } = useGetUserJourneysPaginated(JourneyState.IN_PROGRESS, account.id);
  const showCards = activeJourneysIsSuccess && overdueTasks && activeTasks;
  const tourSeen = !!Storage.local.get(DASHBOARD_TOUR_SEEN);
  const role = isStudent ? 'student' : 'teacher';

  useEffect(() => {
    setActiveJourneysExist(activeJourneys?.pages.some(page => page.data.length));
  }, [activeJourneys?.pages]);

  useEffect(() => {
    const showGreeting = isSuccess && !allJourneys?.pages?.some(page => page.data.length);

    (showGreeting || showCards) && setSpinner(false);
    showGreeting && tourSeen ? setModal(true) : setModal(false);
  }, [isSuccess, allJourneys?.pages, showCards]);

  useEffect(() => {
    const signUpCode = Storage.local.get(SIGN_UP_CODE);
    setIsTourOpen(!tourSeen);

    if (signUpCode) {
      Storage.local.remove(SIGN_UP_CODE);
      history.push(`/journey/join?signUpCode=${signUpCode}`);
    }

    const now = DateTime.local();
    const lastMonth = now.minus({ days: 30 }).toISODate();
    const overdueMinute = now.minus({ minutes: 1 }).toISODate();
    const today = now.toISODate();
    const nextWeek = now.plus({ days: 7 }).toISODate();

    getAssignments(lastMonth, overdueMinute, [EntityState.OVERDUE, EntityState.REJECTED]).then(res => setOverdueTasks(res.data));
    getAssignments(today, nextWeek, [EntityState.IN_PROGRESS, EntityState.URGENT, EntityState.NOT_STARTED]).then(res =>
      setActiveTasks(res.data)
    );
  }, []);

  const getAssignmentCards = (assignmentList, color) =>
    assignmentList?.length > 0 &&
    assignmentList.map(({ deadline, id, journeyId, teacherName, title, subject }) => {
      const entityCardProps: IEntityCardProps = {
        entityId: id,
        color,
        deadline,
        title,
        href: `/assignment/${id}?journeyId=${journeyId}`,
        teacherName,
        subject,
        isAssignment: true,
      };

      return (
        <Col key={id} xs={'12'} className={'px-0'}>
          <EntityCard key={id} {...entityCardProps} className={'spaced-top-1'} />
        </Col>
      );
    });

  const getJourneyCards = (journeysList, color: ColorType) =>
    journeysList?.length > 0 &&
    journeysList.map((journey: IJourney) => {
      const entityCardProps: IEntityCardProps = {
        entityId: journey.id,
        color,
        deadline: journey.endDate,
        subject: journey.subject,
        title: journey.title,
        href: `/journey/${journey.id}`,
        teacherName: journey.teacherName,
        studentCount: journey.studentCount,
        isAssignment: false,
      };

      return (
        <Col key={journey.id} xs={'12'} className={'px-0'}>
          <EntityCard key={journey.id} {...entityCardProps} className={'spaced-top-1'} />
        </Col>
      );
    });

  const getGreetingModal = () => <GreetingModal modal={modal} toggle={() => setModal(!modal)} />;

  const steps = [
    {
      selector: '[data-tour="active-journeys"]',
      content: translate(`schoolabyApp.dashboard.tour.${role}.activeJourneys`),
    },
    {
      selector: '[data-tour="tasks"]',
      content: translate(`schoolabyApp.dashboard.tour.${role}.tasks`),
    },
    {
      selector: '[data-tour="dashboard-messages"]',
      content: translate(`schoolabyApp.dashboard.tour.${role}.dashboardMessages`),
    },
    {
      selector: '[data-tour="messages"]',
      content: translate(`schoolabyApp.dashboard.tour.${role}.messages`),
    },
    {
      selector: '[data-tour="notifications"]',
      content: translate(`schoolabyApp.dashboard.tour.${role}.notifications`),
    },
    {
      selector: '[data-tour="logo"]',
      content: translate(`schoolabyApp.dashboard.tour.${role}.logo`),
    },
    {
      selector: '[data-tour="add-join-journey"]',
      content: translate(`schoolabyApp.dashboard.tour.${role}.addJoinJourney`),
    },
  ];

  const setTourSeen = () => Storage.local.set(DASHBOARD_TOUR_SEEN, true);

  return (
    <div className="dashboard-container">
      <Spinner loading={spinner} />
      <HeadingNew
        headingType={SUB_HEADING}
        isAllowedToModify={isAllowedToModify}
        title={translate('schoolabyApp.dashboard.title')}
        hideBreadcrumbs
      >
        <div className="ml-auto my-auto mr-3">
          <AddButtonNew
            onClick={() => history.push(NavigationPath.JOIN_JOURNEY)}
            title={translate('schoolabyApp.journey.home.joinButton')}
          />
        </div>
      </HeadingNew>
      {getGreetingModal()}
      {showCards && (
        <Row noGutters className="mt-3 dashboard">
          <Tour
            steps={steps}
            isOpen={isTourOpen}
            onRequestClose={() => setIsTourOpen(false)}
            onBeforeClose={setTourSeen}
            accentColor={PRIMARY}
          />
          <Col xs="12" lg="4" data-tour="active-journeys">
            <Col>
              <h3 className={'my-4 pr-3 pl-3'}>{translate('schoolabyApp.dashboard.activeJourneys')}</h3>
              {activeJourneysExist && (
                <div className={'dashboard-column p-1'}>{getJourneyCards(flatMap(activeJourneys.pages, 'data'), 'warning')}</div>
              )}
              {!activeJourneysExist && (
                <>
                  <div className={'text-center text-secondary'}>{translate('schoolabyApp.dashboard.noActiveJourneys')}</div>
                  <br />
                  {isAllowedToModify ? (
                    <Col key="add-new-journey" xs={'12'} className={'px-0'} data-tour="add-join-journey">
                      <ListFooterButton
                        onClick={() => history.push(NavigationPath.CREATE_JOURNEY)}
                        label={'entity.add.journey'}
                        className={'h-100'}
                      />
                    </Col>
                  ) : (
                    <Col key="join-journey" xs={'12'} className={'px-0'} data-tour="add-join-journey">
                      <ListFooterButton
                        link={NavigationPath.JOIN_JOURNEY}
                        label={'schoolabyApp.journey.home.joinButton'}
                        className={'h-100'}
                      />
                    </Col>
                  )}
                </>
              )}
            </Col>
          </Col>
          <Col xs="12" lg="4" data-tour="tasks">
            <Col>
              <h3 className={'my-4 pr-3 pl-3'}>{translate('schoolabyApp.dashboard.overdueTasks')}</h3>
              {!!overdueTasks && <div className={'dashboard-column p-1'}>{getAssignmentCards(overdueTasks, 'danger')}</div>}
              {!overdueTasks?.length && (
                <div className={'text-center text-secondary'}>{translate('schoolabyApp.dashboard.noOverdueTasks')}</div>
              )}
            </Col>
            <Col>
              <h3 className={'my-4 pr-3 pl-3'}>{translate('schoolabyApp.dashboard.activeTasks')}</h3>
              {!!activeTasks?.length && <div className={'dashboard-column p-1'}>{getAssignmentCards(activeTasks, 'warning')}</div>}
              {!activeTasks?.length && (
                <div className={'text-center text-secondary'}>{translate('schoolabyApp.dashboard.noActiveTasks')}</div>
              )}
            </Col>
          </Col>
          <Col xs="12" lg="4" data-tour="dashboard-messages">
            <Col className="mb-4 mt-4 dashboard-notification-column">
              <h3 className={'my-4 pr-3 pl-3'}>{translate('schoolabyApp.dashboard.messages')}</h3>
              <DashboardMessages />
            </Col>
          </Col>
          <Col lg="12" className="mb-5">
            {/* <hr />*/}
            {/* <h3 className={'my-4 pr-3 pl-3'}>{translate('schoolabyApp.dashboard.gradeSheet')}</h3>*/}
            {/* <GradeSheet />*/}
          </Col>
        </Row>
      )}
    </div>
  );
};

const mapStateToProps = ({ authentication }: IRootState) => ({
  isAllowedToModify: hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.TEACHER, AUTHORITIES.ADMIN]),
  isStudent: hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.STUDENT]),
  account: authentication.account,
});

type StateProps = ReturnType<typeof mapStateToProps>;
export default connect(mapStateToProps)(StudentDashboard);
