import React, { FC, SetStateAction } from 'react';
import Icon from 'app/shared/icons';
import { BLACK, EXTRA_LIGHT_GREY } from 'app/shared/util/color-utils';

import './display-mode-button.scss';

export const LIST = 'list';
export const GRID = 'grid';

export type ViewMode = typeof LIST | typeof GRID;

interface DisplayModeButtonProps {
  displayMode: ViewMode;
  setDisplayMode: React.Dispatch<SetStateAction<ViewMode>>;
}

const DisplayModeButton: FC<DisplayModeButtonProps> = ({ displayMode, setDisplayMode }) => {
  const renderButton = (iconName: 'listView' | 'gridView', mode: ViewMode) => (
    <div
      className={`list ${displayMode === mode && 'active'} d-flex align-items-center justify-content-center`}
      onClick={() => setDisplayMode(mode)}
    >
      <Icon name={iconName} fill={displayMode === mode ? EXTRA_LIGHT_GREY : BLACK} />
    </div>
  );

  return (
    <div className={'display-mode-btn cursor-pointer d-flex h-100'}>
      {renderButton('listView', LIST)}
      {renderButton('gridView', GRID)}
    </div>
  );
};

export default DisplayModeButton;
