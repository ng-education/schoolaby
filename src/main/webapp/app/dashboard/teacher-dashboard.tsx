import React, { useState } from 'react';
import JourneyOptionsModal from 'app/shared/layout/journey-options/journey-options-modal/journey-options-modal';
import HeadingNew, { MAIN_HEADING } from 'app/shared/layout/heading/heading-new';
import { translate } from 'react-jhipster';
import { useGetUserJourneysPaginated } from 'app/shared/services/journey-api';
import { JourneyState } from 'app/shared/model/journey.model';
import JourneyListCard from 'app/dashboard/journey-list-card/journey-list-card';
import { IRootState } from 'app/shared/reducers';
import { hasAnyAuthority } from 'app/shared/auth/private-route';
import { AUTHORITIES } from 'app/config/constants';
import { connect } from 'react-redux';
import flatMap from 'lodash/flatMap';
import JourneyTemplate from '../shared/layout/journey-template/journey-template';
import { useShowJourneyTemplatesViewState } from '../shared/contexts/journey-template-context';
import DisplayModeButton, { LIST, ViewMode } from 'app/dashboard/display-mode-button/display-mode-button';
import JourneyGridCard from './journey-grid-card/journey-grid-card';

import './teacher-dashboard.scss';
import ContentContainer from 'app/shared/layout/content-container/content-container';
import AddButtonNew from 'app/shared/layout/add-button-new/add-button-new';
import CustomButton from 'app/shared/layout/custom-button/custom-button';
import NoContentToDisplay from '../shared/layout/no-content-to-display/no-content-to-display';

const TeacherDashboard = ({ isAllowedToModify, userId }: StateProps) => {
  const [displayJourneyOptions, setDisplayJourneyOptions] = useState<boolean>(false);
  const { showTemplatesView } = useShowJourneyTemplatesViewState();
  const [displayMode, setDisplayMode] = useState<ViewMode>(LIST);
  const { data: activeJourneys, hasNextPage, fetchNextPage, isFetching, isFetchingNextPage } = useGetUserJourneysPaginated(
    JourneyState.IN_PROGRESS,
    userId
  );
  const activeJourneysFlatMap = flatMap(activeJourneys?.pages, 'data');

  const renderJourneys = () =>
    activeJourneysFlatMap?.length ? (
      <div className={`journey-list ${displayMode === LIST ? 'list' : 'grid'}`}>
        {displayMode === LIST
          ? activeJourneysFlatMap.map(journey => <JourneyListCard key={`journey-card-${journey.id}`} journey={journey} />)
          : activeJourneysFlatMap.map(journey => <JourneyGridCard key={`journey-card-${journey.id}`} journey={journey} />)}
      </div>
    ) : (
      <NoContentToDisplay
        action={() => setDisplayJourneyOptions(true)}
        actionTitle={translate('entity.add.journey')}
        message={translate('schoolabyApp.dashboard.noJourneysToDisplay')}
      />
    );

  return showTemplatesView ? (
    <JourneyTemplate />
  ) : (
    <ContentContainer spinner={isFetching && !isFetchingNextPage} className={'dashboard-new pb-5'}>
      <HeadingNew isAllowedToModify={isAllowedToModify} headingType={MAIN_HEADING} title={translate('schoolabyApp.dashboard.myJourneys')}>
        {isAllowedToModify && (
          <div className={'d-flex justify-content-end h-100'}>
            <DisplayModeButton displayMode={displayMode} setDisplayMode={setDisplayMode} />
            <AddButtonNew title={translate('entity.add.journey')} onClick={() => setDisplayJourneyOptions(true)} />
          </div>
        )}
      </HeadingNew>
      {(!isFetching || isFetchingNextPage) && renderJourneys()}
      {hasNextPage && (
        <CustomButton
          className={'mt-5 align-self-center'}
          buttonType={'primary'}
          outline
          onClick={() => fetchNextPage()}
          title={translate('schoolabyApp.journey.home.loadMore')}
        />
      )}
      <JourneyOptionsModal isModalOpen={displayJourneyOptions} closeModal={() => setDisplayJourneyOptions(false)} />
    </ContentContainer>
  );
};

const mapStateToProps = ({ authentication }: IRootState) => ({
  isAllowedToModify: hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.TEACHER, AUTHORITIES.ADMIN]),
  userId: authentication.account.id,
});

type StateProps = ReturnType<typeof mapStateToProps>;
export default connect(mapStateToProps)(TeacherDashboard);
