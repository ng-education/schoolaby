import React, { useEffect, useState } from 'react';
import InnerCard, { NOTIFICATIONS } from 'app/dashboard/journey-grid-card/inner-card/inner-card';
import { useGetNotifications } from 'app/shared/services/notification-api';
import { filterUnreadTeacherNotificationsByJourneyId } from 'app/shared/layout/header/notification/notification-util';
import { useUserNotificationDropdownState } from 'app/shared/contexts/user-notification-context';
import { translate } from 'react-jhipster';

const NotificationsCard = ({ journeyId }) => {
  const { data: generalNotifications } = useGetNotifications({ type: 'GENERAL' }, false);
  const { toggleDropdown, setJourneyId } = useUserNotificationDropdownState();

  const [unreadNotificationsCount, setUnreadNotificationsCount] = useState<number>(0);

  useEffect(() => {
    setUnreadNotificationsCount(filterUnreadTeacherNotificationsByJourneyId(generalNotifications ?? [], journeyId).length);
  }, [generalNotifications, generalNotifications]);

  const handleOpenNotifications = () => {
    setJourneyId(journeyId);
    toggleDropdown();
  };

  return (
    <InnerCard
      type={NOTIFICATIONS}
      newCount={unreadNotificationsCount}
      title={translate(
        unreadNotificationsCount === 1 ? 'schoolabyApp.dashboard.grid.notification' : 'schoolabyApp.dashboard.grid.notifications'
      )}
      iconName={'roundedBell'}
      onClick={handleOpenNotifications}
    />
  );
};

export default NotificationsCard;
