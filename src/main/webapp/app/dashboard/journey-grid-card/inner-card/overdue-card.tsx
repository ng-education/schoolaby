import React, { useState } from 'react';
import InnerCard, { OVERDUE } from 'app/dashboard/journey-grid-card/inner-card/inner-card';
import { useGetAssignments } from 'app/shared/services/assignment-api';
import { EntityState } from 'app/shared/model/enumerations/entity-state';
import { useHistory } from 'react-router-dom';
import { translate } from 'react-jhipster';

const OverdueCard = ({ journeyId }) => {
  const history = useHistory();
  const [totalCount, setTotalCount] = useState<number>(0);
  useGetAssignments([EntityState.OVERDUE], journeyId, null, null, setTotalCount);

  const navigateToJourneyKanbanView = () => history.push(`/journey/${journeyId}`);

  return (
    <InnerCard
      type={OVERDUE}
      newCount={totalCount}
      title={translate('schoolabyApp.dashboard.grid.overdue')}
      iconName={'stopwatch'}
      onClick={navigateToJourneyKanbanView}
    />
  );
};

export default OverdueCard;
