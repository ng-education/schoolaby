import React, { FC } from 'react';

import './inner-card.scss';
import { Col, Row } from 'reactstrap';
import Icon from 'app/shared/icons';

export const MESSAGES = 'messages';
export const OVERDUE = 'overdue';
export const NOTIFICATIONS = 'notifications';

type NotificationCardType = typeof MESSAGES | typeof OVERDUE | typeof NOTIFICATIONS;

interface NotificationCardProps {
  type: NotificationCardType;
  newCount: number;
  title: string;
  iconName: 'chat' | 'stopwatch' | 'roundedBell';
  onClick: () => void;
}

const InnerCard: FC<NotificationCardProps> = ({ type, newCount, title, iconName, onClick }) => (
  <Col className={`notification-card ${newCount > 0 ? 'active' : ''} ${type} d-flex flex-column cursor-pointer`} onClick={onClick}>
    <Row className={'align-items-center justify-content-between'}>
      <div className={'notification-count'}>{newCount}</div>
      <Icon name={iconName} />
    </Row>
    <Row className={'notification-card-title align-items-end'}>{title}</Row>
  </Col>
);

export default InnerCard;
