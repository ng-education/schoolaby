import React, { FC } from 'react';

import './journey-grid-card.scss';
import { IJourney } from 'app/shared/model/journey.model';
import { Card, Col, Row } from 'reactstrap';
import { getSubjectIcon } from 'app/shared/icons';
import { Link } from 'react-router-dom';
import MessagesCard from 'app/dashboard/journey-grid-card/inner-card/messages-card';
import OverdueCard from 'app/dashboard/journey-grid-card/inner-card/overdue-card';
import NotificationsCard from 'app/dashboard/journey-grid-card/inner-card/notifications-card';
import { getSchoolImage } from 'app/shared/util/journey-utils';

interface JourneyGridCardProps {
  journey: IJourney;
}

const JourneyGridCard: FC<JourneyGridCardProps> = ({ journey }) => {
  const schoolImage = getSchoolImage(journey);

  const SchoolIcon = () => {
    return schoolImage ? <img className={'school-image mr-2'} src={schoolImage?.imageUrl} alt={schoolImage?.schoolName} /> : null;
  };

  return (
    <Card className={'journey-grid-card'}>
      <Row>
        <SchoolIcon />
        <div className={'subject-icon'}>{getSubjectIcon(journey?.subject?.label ?? '')}</div>
      </Row>
      <Row className={'class'} />
      <Row className={'display-block'}>
        <Link to={`/journey/${journey.id}`}>
          <h3 className={'title ellipsis m-0'}>{journey?.title}</h3>
        </Link>
      </Row>
      <Row className={'school ellipsis display-block'}>
        {schoolImage?.schoolName ? schoolImage?.schoolName : journey?.schools?.[0]?.name}
      </Row>
      <Row className={'notifications-section'}>
        <Col xs={'4'}>
          <MessagesCard journeyId={journey.id} />
        </Col>
        <Col xs={'4'}>
          <OverdueCard journeyId={journey.id} />
        </Col>
        <Col xs={'4'}>
          <NotificationsCard journeyId={journey.id} />
        </Col>
      </Row>
    </Card>
  );
};

export default JourneyGridCard;
