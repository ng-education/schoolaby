import 'react-toastify/dist/ReactToastify.css';
import './app.scss';
import '@fontsource/montserrat/700.css';
import '@fontsource/montserrat/800.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';

import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Container } from 'reactstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { toast, ToastContainer } from 'react-toastify';
import { hot } from 'react-hot-loader';
import { IRootState } from 'app/shared/reducers';
import { getSession } from 'app/shared/reducers/authentication';
import { getProfile } from 'app/shared/reducers/application-profile';
import { setLocale } from 'app/shared/reducers/locale';
import Header from 'app/shared/layout/header/header';
import { hasAnyAuthority } from 'app/shared/auth/private-route';
import ErrorBoundary from 'app/shared/error/error-boundary';
import { AUTHORITIES } from 'app/config/constants';
import AppRoutes from 'app/routes';
import { RootProvider } from 'app/shared/contexts/root-context';
import { ThroughProvider } from 'react-through';
import { ChatProvider } from 'app/shared/contexts/chat-context';
import { hasValidJWT } from 'app/shared/util/auth-utils';
import Cookies from 'js-cookie';
import CookieConsentModal from './shared/cookies/cookie-consent-modal';
import TermsModal from 'app/account/common/terms-modal';
import { UserNotificationProvider } from 'app/shared/contexts/user-notification-context';
import DeleteModal, { IDeleteModal } from 'app/shared/layout/delete-modal/delete-modal';
import { daySW, monthSW } from 'app/shared/util/date-utils';
import { registerLocale } from 'react-datepicker';
import et from 'date-fns/locale/et';
import fi from 'date-fns/locale/fi';
import uk from 'date-fns/locale/uk';
import enUS from 'date-fns/locale/en-US';

const baseHref = document.querySelector('base')?.getAttribute('href').replace(/\/$/, '');

interface IAppProps extends StateProps, DispatchProps {}

export const AppContext = React.createContext(undefined);

registerLocale('et', et);
registerLocale('fi', fi);
registerLocale('uk', uk);
registerLocale('sw', {
  ...enUS,
  localize: {
    ...enUS.localize,
    month: n => monthSW[n],
    day: n => daySW[n],
  },
});

const App = (props: IAppProps) => {
  const [modal, setModal] = useState<IDeleteModal>(null);

  const injectGA = () => {
    if (Cookies.get('schoolabyCCv2') && Cookies.get('schoolabyCCv2').includes('analytics')) {
      // @ts-ignore
      window.injectGA();
    }
  };

  useEffect(() => {
    if (hasValidJWT()) {
      props.getSession();
    }
    props.getProfile();
    injectGA();
  }, []);

  const setIsOpen = () => {
    setModal({
      ...modal,
      isOpen: !modal.isOpen,
    });
  };

  return (
    <Router basename={baseHref}>
      <div className="app-container" style={{ paddingTop: props.isAuthenticated ? '50px' : '0px' }}>
        <AppContext.Provider
          value={{
            setModal,
          }}
        >
          <ToastContainer position={toast.POSITION.TOP_LEFT} className="toastify-container" toastClassName="toastify-toast" />
          <UserNotificationProvider>
            <ErrorBoundary>
              <Header
                hasSelectedRole={props.hasSelectedRole}
                isAdmin={props.isAdmin}
                ribbonEnv={props.ribbonEnv}
                isInProduction={props.isInProduction}
                isSwaggerEnabled={props.isSwaggerEnabled}
              />
            </ErrorBoundary>
            <Container fluid={true}>
              <div className="jh-card">
                <ErrorBoundary>
                  <RootProvider>
                    <ThroughProvider>
                      <ChatProvider>
                        <AppRoutes />
                      </ChatProvider>
                    </ThroughProvider>
                  </RootProvider>
                </ErrorBoundary>
              </div>
            </Container>
          </UserNotificationProvider>
          {modal?.isOpen && <DeleteModal {...modal} toggle={setIsOpen} />}
        </AppContext.Provider>
        <TermsModal />
        <CookieConsentModal onAccept={injectGA} />
        <span id={'lti-form'} />
      </div>
    </Router>
  );
};

const mapStateToProps = ({ authentication, applicationProfile, locale }: IRootState) => ({
  currentLocale: locale.currentLocale,
  isAuthenticated: authentication.isAuthenticated,
  hasSelectedRole: hasAnyAuthority(authentication.account.authorities, [
    AUTHORITIES.ADMIN,
    AUTHORITIES.TEACHER,
    AUTHORITIES.STUDENT,
    AUTHORITIES.PARENT,
    AUTHORITIES.ADMIN,
  ]),
  isAdmin: hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.ADMIN]),
  ribbonEnv: applicationProfile.ribbonEnv,
  isInProduction: applicationProfile.inProduction,
  isSwaggerEnabled: applicationProfile.isSwaggerEnabled,
});

const mapDispatchToProps = { setLocale, getSession, getProfile };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(hot(module)(App));
