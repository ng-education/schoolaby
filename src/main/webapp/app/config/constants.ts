const config = {
  VERSION: process.env.VERSION,
};

export default config;

export const SERVER_API_URL = process.env.SERVER_API_URL;
export const SIGN_UP_CODE = 'SIGN_UP_CODE';
export const DASHBOARD_TOUR_SEEN = 'DASHBOARD_TOUR_SEEN';
export const WEBM_INFO_MODAL_SEEN = 'WEBM_INFO_MODAL_SEEN';

export const AUTHORITIES = {
  ADMIN: 'ROLE_ADMIN',
  USER: 'ROLE_USER',
  STUDENT: 'ROLE_STUDENT',
  TEACHER: 'ROLE_TEACHER',
  PARENT: 'ROLE_PARENT',
};

export const ROLE = {
  STUDENT: 'STUDENT',
  TEACHER: 'TEACHER',
};

export const messages = {
  DATA_ERROR_ALERT: 'Internal Error',
};

export const APP_DATE_TIME_FORMAT = 'DD/MM/YY HH:mm';
export const APP_DATE_FORMAT = 'DD/MM/YYYY';
export const APP_TIMESTAMP_FORMAT = 'DD/MM/YY HH:mm:ss';
export const APP_LOCAL_DATE_FORMAT = 'DD.MM.YYYY';
export const APP_LOCAL_DATE_TIME_FORMAT = 'DD.MM.YYYY HH:mm';
export const APP_WHOLE_NUMBER_FORMAT = '0,0';
export const APP_TWO_DIGITS_AFTER_POINT_NUMBER_FORMAT = '0,0.[00]';

export const EKOOLIKOTT = 'EKOOLIKOTT';
export const OPIQ = 'OPIQ';

// Keyboard events
export const EVENT_CODE_ENTER = 'Enter';
export const EVENT_CODE_ARROW_UP = 'ArrowUp';
export const EVENT_CODE_ARROW_DOWN = 'ArrowDown';

export const COUNTRY = {
  ESTONIA: 'Estonia',
  TANZANIA: 'Tanzania',
  UNITED_STATES: 'United States',
  FINLAND: 'Finland',
  UKRAINE: 'Ukraine',
};
