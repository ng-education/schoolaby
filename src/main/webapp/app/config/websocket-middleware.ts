import SockJS from 'sockjs-client';
import Stomp from 'webstomp-client';
import { Observable } from 'rxjs';
import { Storage } from 'react-jhipster';
import { ACTION_TYPES as ADMIN_ACTIONS } from 'app/administration/administration.reducer';
import { ACTION_TYPES as AUTH_ACTIONS, getRolesFromJWT } from 'app/shared/reducers/authentication';
import { FAILURE, SUCCESS } from 'app/shared/reducers/action-type.util';
import { hasAnyAuthority } from 'app/shared/auth/private-route';
import { AUTHORITIES } from 'app/config/constants';

let stompClient = null;

let subscriber = null;
let ltiScoresSubscriber = null;
let connection: Promise<any>;
let connectedPromise: any = null;
let listener: Observable<any>;
let listenerObserver: any;
let alreadyConnectedOnce = false;

const createConnection = (): Promise<any> => new Promise((resolve, reject) => (connectedPromise = resolve));

const createListener = (): Observable<any> =>
  new Observable(observer => {
    listenerObserver = observer;
  });

const sendActivity = () => {
  connection.then(() => {
    stompClient.send(
      '/topic/activity', // destination
      JSON.stringify({ page: window.location.hash }), // body
      {} // header
    );
  });
};

const subscribe = () => {
  connection.then(() => {
    subscriber = stompClient.subscribe('/topic/tracker', data => {
      listenerObserver.next(JSON.parse(data.body));
    });
  });
};

export const subscribeToLtiScores = callback => {
  connection?.then(() => {
    if (ltiScoresSubscriber === null) {
      ltiScoresSubscriber = stompClient.subscribe('/user/queue/lti-scores', data => {
        const parsed = JSON.parse(data.body);
        callback(parsed);
        listenerObserver.next(parsed);
      });
    }
  });
};

export const unsubscribeFromLtiScores = () => {
  if (ltiScoresSubscriber !== null) {
    ltiScoresSubscriber.unsubscribe();
    ltiScoresSubscriber = null;
  }
};

const connect = () => {
  if (connectedPromise !== null || alreadyConnectedOnce) {
    // the connection is already being established
    return;
  }
  connection = createConnection();
  listener = createListener();

  // building absolute path so that websocket doesn't fail when deploying with a context path
  const loc = window.location;
  const baseHref = document.querySelector('base').getAttribute('href').replace(/\/$/, '');

  const headers = {};
  let url = '//' + loc.host + baseHref + '/websocket/tracker';
  const authToken = Storage.local.get('jhi-authenticationToken') || Storage.session.get('jhi-authenticationToken');
  if (authToken) {
    url += '?access_token=' + authToken;
  }
  const socket = new SockJS(url);
  stompClient = Stomp.over(socket);

  // Remove this to see debug logs
  stompClient.debug = () => {};

  stompClient.connect(headers, () => {
    connectedPromise('success');
    connectedPromise = null;
    const userRoles = getRolesFromJWT(authToken);
    if (hasAnyAuthority(userRoles, [AUTHORITIES.ADMIN])) {
      subscribe();
    }
    sendActivity();
    if (!alreadyConnectedOnce) {
      window.onhashchange = () => {
        sendActivity();
      };
      alreadyConnectedOnce = true;
    }
  });
};

const disconnect = () => {
  if (stompClient !== null) {
    stompClient.disconnect();
    stompClient = null;
  }
  window.onhashchange = () => {};
  alreadyConnectedOnce = false;
};

const receive = () => listener;

const unsubscribe = () => {
  if (subscriber !== null) {
    subscriber.unsubscribe();
  }
  unsubscribeFromLtiScores();
  listener = createListener();
};

export default store => next => action => {
  if (action.type === SUCCESS(AUTH_ACTIONS.GET_SESSION)) {
    connect();
    if (!alreadyConnectedOnce) {
      receive().subscribe(activity => {
        return store.dispatch({
          type: ADMIN_ACTIONS.WEBSOCKET_ACTIVITY_MESSAGE,
          payload: activity,
        });
      });
    }
  } else if (action.type === FAILURE(AUTH_ACTIONS.GET_SESSION)) {
    unsubscribe();
    disconnect();
  }
  return next(action);
};
