import './login.scss';
import React, { CSSProperties, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Link, Redirect, RouteComponentProps, useHistory } from 'react-router-dom';
import { IRootState } from 'app/shared/reducers';
import { login } from 'app/shared/reducers/authentication';
import { Alert, Button, Col, Row } from 'reactstrap';
import { Storage, translate } from 'react-jhipster';
import { useQueryParam } from 'app/shared/hooks/useQueryParam';
import { SIGN_UP_CODE } from 'app/config/constants';
import { createYupValidator } from 'app/shared/util/form-utils';
import { Field, Form } from 'react-final-form';
import { Input } from 'app/shared/form';
import { useLoginSchema } from 'app/shared/schema/login';
import { setLocale } from 'app/shared/reducers/locale';
import { parse, stringify } from 'query-string';
import { useStylesState } from 'app/shared/contexts/root-context';
import { useLogout } from 'app/shared/services/account-api';

export interface ILoginProps extends StateProps, DispatchProps, RouteComponentProps<{}> {}

export const Login = (props: ILoginProps) => {
  const { location, isAuthenticated, locale } = props;
  const history = useHistory();
  const [{ signUpCode, lang, oauth }] = useQueryParam<{ signUpCode: string; lang: string; oauth: string }>();
  const [passwordLoginEnabled, setPasswordLoginEnabled] = useState<boolean>(false);
  const { setStyles, initializeStyles } = useStylesState();
  const { mutate: backendLogout } = useLogout();

  const { from } = (location?.state as any) || { from: { pathname: '/', search: location.search } };
  signUpCode && Storage.local.set(SIGN_UP_CODE, signUpCode);

  const submit = ({ username, password }) => props.login(username, password);

  const loginStyles: CSSProperties = {
    display: 'flex',
    flexDirection: 'column',
  };

  useEffect(() => {
    setStyles(loginStyles);
    if (!isAuthenticated && !oauth) {
      backendLogout();
    }

    if (locale !== lang && lang) {
      props.setLocale(lang);
    } else {
      const queryParams = parse(location.search);
      const newQueries = { ...queryParams, lang: locale };
      history.push({ search: stringify(newQueries) });
    }

    return () => {
      initializeStyles();
    };
  }, []);

  const isRedirectFromLogout = () => {
    const searchParams = new URLSearchParams(location.search);
    return searchParams.get('logoutSuccessful');
  };

  const hasHarIdLoginFailed = () => {
    const searchParams = new URLSearchParams(location.search);
    return searchParams.get('harIdLoginFailed');
  };

  const getPasswordLoginFormProps = handleSubmit =>
    oauth
      ? {
          method: 'POST',
          action: 'api/authenticate/oauth',
        }
      : {
          onSubmit: handleSubmit,
        };

  const LoginText = () => <h4 className={'mt-5 font-weight-normal'}>{translate('login.loginMessage')}</h4>;

  const PasswordLogin = () => (
    <Form onSubmit={submit} validate={createYupValidator(useLoginSchema(locale))}>
      {({ handleSubmit }) => (
        <form {...getPasswordLoginFormProps(handleSubmit)} className="my-5 pw-login-container">
          <Field
            id="username"
            type="text"
            name="username"
            component={Input}
            autoFocus
            required
            placeholder={translate('global.form.username.placeholder')}
          />
          <Field name="password" type="password" placeholder={translate('login.form.password.placeholder')} component={Input} required />
          <Link to="/account/reset/request" className={'font-weight-normal float-right'}>
            {translate('login.password.forgot')}
          </Link>
          <Button color="primary" type="submit" className={'my-3 w-100'}>
            {translate('login.form.button')}
          </Button>
          <Link to="/account/register" className={'font-weight-normal float-right'}>
            {translate('login.registerAccount')}
          </Link>
          <Field name="rememberMe" type="checkbox">
            {({ input, name }) => (
              <>
                <input id={'remember-me'} name={name} checked={input.checked} type="checkbox" className="mr-2" onChange={input.onChange} />
                <label htmlFor={'remember-me'} className={'mb-0'}>
                  {translate('login.form.rememberMe')}
                </label>
              </>
            )}
          </Field>
        </form>
      )}
    </Form>
  );

  const OAuthLogin = () => (
    <div className={'oauth-buttons'}>
      <a
        href="oauth2/authorization/ekool"
        className="btn btn-lg btn-oauth d-flex justify-content-center align-items-center chat-bubble"
        aria-label={translate('login.oauth.ekoolLogin')}
      >
        <img src="content/images/ekool.png" alt="ekool-login" />
      </a>
      <a
        href="oauth2/authorization/google"
        className="btn btn-lg btn-oauth d-flex justify-content-center align-items-center mt-2 chat-bubble"
        aria-label={translate('login.oauth.googleLogin')}
      >
        <img src="content/images/google.png" alt="google-login" />
        {translate('login.google')}
      </a>
      <a
        href="oauth2/authorization/azure"
        className="btn btn-lg btn-oauth d-flex justify-content-center align-items-center mt-2 chat-bubble"
        aria-label={translate('login.oauth.microsoftLogin')}
      >
        <img src="content/images/microsoft.png" alt="microsoft-login" />
        {translate('login.microsoft')}
      </a>
      <a
        href="oauth2/authorization/yahoo"
        className="btn btn-lg btn-oauth yahoo d-flex justify-content-center align-items-center mt-2 chat-bubble"
        aria-label={translate('login.oauth.yahooLogin')}
      >
        {`${translate('login.yahoo')}!`}
      </a>
      {!oauth && (
        <a
          href="/api/harid"
          className="btn btn-lg btn-oauth d-flex justify-content-center align-items-center mt-2 chat-bubble"
          aria-label={translate('login.oauth.harIdLogin')}
        >
          <img alt="HarID" src={'content/images/harid.png'} />
        </a>
      )}
    </div>
  );

  const shouldShowAlerts = () => {
    return props.loginError || isRedirectFromLogout() || hasHarIdLoginFailed();
  };

  if (isAuthenticated) {
    return <Redirect to={from} />;
  }
  return (
    <>
      <div className="login-content-container d-flex justify-content-center">
        <div className="pt-lg-5 login-page">
          <Row className="px-md-5 flex-row-reverse justify-content-between px-sm-2">
            {shouldShowAlerts() && (
              <Col sm={12} className="login-alerts">
                {props.loginError ? (
                  <Alert color="danger" className="mt-4">
                    {translate('login.messages.error.authentication')}
                  </Alert>
                ) : null}
                {hasHarIdLoginFailed() && (
                  <Alert color="danger" className="mt-4">
                    {translate('login.messages.error.harIdLogin')}
                  </Alert>
                )}
                {isRedirectFromLogout() && !props.loginError ? (
                  <Alert color="success" className="mt-4">
                    {translate('logout.success')}
                  </Alert>
                ) : null}
              </Col>
            )}
            <Col lg={6} className="d-flex justify-content-lg-center login-filler">
              <img alt="Login filler" className="my-3" />
            </Col>
            <Col lg={5}>
              <h1 className="login-title bold">{translate('login.title')}</h1>
              <LoginText />
              <OAuthLogin />
              {passwordLoginEnabled ? (
                <PasswordLogin />
              ) : (
                <p className="my-5 enable-password-login d-inline-block" onClick={() => setPasswordLoginEnabled(true)}>
                  {translate('login.signInWithUsername')}
                </p>
              )}
            </Col>
          </Row>
        </div>
      </div>
      <div className="eas-logo d-flex justify-content-end p-3 bg-white">
        <img src="content/images/EAS.png" alt="EAS logo" />
      </div>
    </>
  );
};

const mapStateToProps = ({ authentication, locale }: IRootState) => ({
  isAuthenticated: authentication.isAuthenticated,
  loginError: authentication.loginError,
  locale: locale.currentLocale,
});

const mapDispatchToProps = { login, setLocale };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Login);
