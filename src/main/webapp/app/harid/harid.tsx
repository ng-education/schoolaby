import React, { useEffect, useState } from 'react';
import { Redirect } from 'react-router-dom';
import { authorizeHarId, getSession } from 'app/shared/reducers/authentication';
import { getUrlParameter } from 'react-jhipster';
import { IRootState } from 'app/shared/reducers';
import { connect } from 'react-redux';
import { Spinner } from 'app/shared/layout/spinner/spinner';
import { hasAnyAuthority } from 'app/shared/auth/private-route';
import { AUTHORITIES } from 'app/config/constants';

const HarIdTokenAuthenticationPage = props => {
  const [loading, setLoading] = useState(true);
  const { isAuthenticated, hasRole } = props;

  useEffect(() => {
    const code = getUrlParameter('code', props.location.search);
    const error = getUrlParameter('error', props.location.search);
    if (code?.length && !error?.length) {
      authorizeHarId(code).then(() => {
        props.getSession();
        setLoading(false);
      });
    } else {
      setLoading(false);
    }
  }, []);

  useEffect(() => {}, [loading, isAuthenticated]);

  if (!loading && isAuthenticated) {
    if (hasRole) {
      return <Redirect to={'/'} />;
    } else {
      return <Redirect to={'/account/settings'} />;
    }
  }

  if (!loading && !isAuthenticated) {
    return (
      <Redirect
        to={{
          pathname: '/login',
          search: 'harIdLoginFailed=true',
        }}
      />
    );
  }

  return <Spinner />;
};

const mapStateToProps = ({ authentication }: IRootState) => ({
  isAuthenticated: authentication.isAuthenticated,
  hasRole: hasAnyAuthority(authentication.account.authorities, [
    AUTHORITIES.TEACHER,
    AUTHORITIES.ADMIN,
    AUTHORITIES.STUDENT,
    AUTHORITIES.PARENT,
  ]),
});

const mapDispatchToProps = { getSession };

export default connect(mapStateToProps, mapDispatchToProps)(HarIdTokenAuthenticationPage);
