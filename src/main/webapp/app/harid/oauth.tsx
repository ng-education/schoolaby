import React, { useEffect, useState } from 'react';
import { Redirect } from 'react-router-dom';
import { AUTH_TOKEN_KEY } from 'app/shared/reducers/authentication';
import { getUrlParameter, Storage } from 'react-jhipster';
import { IRootState } from 'app/shared/reducers';
import { connect } from 'react-redux';
import { Spinner } from 'app/shared/layout/spinner/spinner';
import { hasAnyAuthority } from 'app/shared/auth/private-route';
import { AUTHORITIES } from 'app/config/constants';

const OauthAuthenticationPage = props => {
  document.cookie = 'JSESSIONID=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;';

  const [loading, setLoading] = useState(true);
  const { isAuthenticated, hasRole, firstLogin } = props;

  useEffect(() => {
    const jwt = getUrlParameter('token', props.location.search);
    Storage.local.set(AUTH_TOKEN_KEY, jwt);
    setLoading(false);
  }, []);

  useEffect(() => {}, [loading, isAuthenticated, firstLogin]);

  if (!loading && isAuthenticated) {
    if (!firstLogin && hasRole) {
      return <Redirect to={'/'} />;
    } else {
      return <Redirect to={'/account/settings'} />;
    }
  }

  return <Spinner />;
};

const mapStateToProps = ({ authentication }: IRootState) => ({
  isAuthenticated: authentication.isAuthenticated,
  firstLogin: authentication.account.firstLogin,
  hasRole: hasAnyAuthority(authentication.account.authorities, [
    AUTHORITIES.TEACHER,
    AUTHORITIES.ADMIN,
    AUTHORITIES.STUDENT,
    AUTHORITIES.PARENT,
  ]),
});

export default connect(mapStateToProps)(OauthAuthenticationPage);
