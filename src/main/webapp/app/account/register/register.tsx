import './register.scss';
import React, { useEffect, useState } from 'react';
import { translate } from 'react-jhipster';
import { connect } from 'react-redux';
import { Button, Col, FormFeedback, Row } from 'reactstrap';
import PasswordStrengthBar from 'app/shared/layout/password/password-strength-bar';
import { IRootState } from 'app/shared/reducers';
import { handleRegister, reset } from './register.reducer';
import { useHistory } from 'react-router-dom';
import { countries } from 'app/config/translation';
import { Field, Form } from 'react-final-form';
import { Input } from 'app/shared/form';
import { createYupValidator } from 'app/shared/util/form-utils';
import { useRegisterSchema } from 'app/shared/schema/account';
import CountryChoiceField from 'app/account/common/country-choice-field';
import RoleChoiceField from 'app/account/common/role-choice-field';
import { AUTHORITIES, COUNTRY } from 'app/config/constants';
import PersonRoleFieldArray from 'app/shared/layout/person-role-field-array/person-role-field-array';
import arrayMutators from 'final-form-arrays';
import { IUser } from 'app/shared/model/user.model';

interface IRegisterProps extends StateProps, DispatchProps {}

interface FormUser extends IUser {
  terms: boolean;
  authority: string;
}

export const RegisterPage = (props: IRegisterProps) => {
  const history = useHistory();
  const [selectedCountry, setSelectedCountry] = useState<string>(COUNTRY.ESTONIA);
  const initialValues = {
    country: COUNTRY.ESTONIA,
    authority: AUTHORITIES.STUDENT,
    terms: false,
    personRoles: [{ role: 'STUDENT', school: {}, active: true }],
  };
  const [currentAccount, setCurrentAccount] = useState<FormUser>(initialValues);

  useEffect(() => {
    props.reset();
  }, []);

  const handleValidSubmit = values => {
    props.handleRegister(
      [values.authority],
      values.username,
      values.firstName,
      values.lastName,
      values.email,
      values.firstPassword,
      countries.get(values.country)?.langKey,
      values.country,
      values.personRoles,
      values.termsAgreed,
      values.firstLogin
    );
    history.push('/');
  };

  return (
    <div className={'register-container'}>
      <Row className="justify-content-center">
        <Col md="8">
          <h1 id="register-title">{translate('register.title')}</h1>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          <Form
            onSubmit={handleValidSubmit}
            validate={createYupValidator(useRegisterSchema(props.currentLocale))}
            mutators={{ ...arrayMutators }}
            initialValues={currentAccount}
          >
            {({ values, handleSubmit }) => (
              <form onSubmit={handleSubmit}>
                <Field
                  name="username"
                  className="form-control"
                  placeholder={translate('global.form.username.placeholder')}
                  label={translate('global.form.username.label')}
                  component={Input}
                />
                <Field
                  name="firstName"
                  className="form-control"
                  placeholder={translate('global.form.firstName.placeholder')}
                  label={translate('global.form.firstName.label')}
                  component={Input}
                />
                <Field
                  name="lastName"
                  className="form-control"
                  placeholder={translate('global.form.lastName.placeholder')}
                  label={translate('global.form.lastName.label')}
                  component={Input}
                />
                <Field
                  name="email"
                  placeholder={translate('global.form.email.placeholder')}
                  label={translate('global.form.email.label')}
                  type="email"
                  component={Input}
                />

                <h3 className="my-4">{translate('global.form.role.selectYourLocale')}</h3>
                <CountryChoiceField setSelectedCountry={setSelectedCountry} />

                <h3 className="my-4">{translate('global.form.role.selectYourMainRole')}</h3>
                <RoleChoiceField />

                <h3 className="my-4">{translate('global.form.personRoles')}</h3>
                <PersonRoleFieldArray selectedCountry={selectedCountry} setCurrentAccount={setCurrentAccount} />
                <Field
                  name="firstPassword"
                  type="password"
                  className="form-control"
                  placeholder={translate('global.form.newpassword.placeholder')}
                  label={translate('global.form.newpassword.placeholder')}
                  component={Input}
                />
                <PasswordStrengthBar password={values?.firstPassword ? values.firstPassword : ''} />
                <Field
                  name="secondPassword"
                  label={translate('global.form.confirmpassword.label')}
                  placeholder={translate('global.form.confirmpassword.placeholder')}
                  type="password"
                  className="form-control"
                  component={Input}
                />
                <Field name="termsAgreed" type="checkbox">
                  {({ input, meta }) => {
                    return (
                      <div className={'form-group mt-4 form-control border-0 p-0 h-auto'}>
                        <input id="termsAgreed" name={input.name} type="checkbox" checked={input.checked} onChange={input.onChange} />
                        <label htmlFor="termsAgreed" className="ml-1 mb-0 terms">
                          {translate('userManagement.agreeToTermsAndConditions')}
                        </label>
                        {meta?.touched && meta?.error && (
                          <FormFeedback className={'d-block'}>{translate('entity.validation.required')}</FormFeedback>
                        )}
                      </div>
                    );
                  }}
                </Field>
                <br />
                <Button color="primary" type="submit">
                  {translate('register.form.button')}
                </Button>
              </form>
            )}
          </Form>
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = ({ locale }: IRootState) => ({
  currentLocale: locale.currentLocale,
});

const mapDispatchToProps = { handleRegister, reset };
type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(RegisterPage);
