const roleOptions = [
  {
    role: 'TEACHER',
    translationKey: 'global.teacher',
  },
  {
    role: 'STUDENT',
    translationKey: 'global.student',
  },
];

export default roleOptions;
