import React from 'react';
import authorityOptions from 'app/account/common/authority-options';
import HorizontalRadio from 'app/shared/layout/horizontal-radio-button/horizontal-radio-button';
import { Field, useForm } from 'react-final-form';
import { translate } from 'react-jhipster';
import { FormFeedback, Label } from 'reactstrap';

const RoleChoiceField = () => {
  const { submitFailed, errors } = useForm().getState();

  return (
    <HorizontalRadio className={`form-control border-0 p-0 h-auto ${submitFailed && errors?.authorities ? 'is-invalid text-danger' : ''}`}>
      {authorityOptions.map(({ role, translationKey }) => (
        <Field key={role} type={'radio'} value={role} name={'authority'}>
          {({ input }) => {
            return (
              <div className={'form-group'}>
                <input id={`role-${role}`} name={input.name} type="radio" value={role} checked={input.checked} onChange={input.onChange} />
                <Label for={`role-${role}`}>{translate(translationKey)}</Label>
              </div>
            );
          }}
        </Field>
      ))}
      {submitFailed && errors?.authority && <FormFeedback className="d-block">{errors.authority}</FormFeedback>}
    </HorizontalRadio>
  );
};

export default RoleChoiceField;
