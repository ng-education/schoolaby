import React, { FC, useEffect, useState } from 'react';
import { translate } from 'react-jhipster';
import { Button, Col, Label, Row } from 'reactstrap';
import { Field, Form } from 'react-final-form';
import { IRootState } from 'app/shared/reducers';
import { connect } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { hasAnyAuthority } from 'app/shared/auth/private-route';
import { AUTHORITIES } from 'app/config/constants';
import { useAgreeTerms } from 'app/shared/services/account-api';
import './terms-modal.scss';
import { getSession } from 'app/shared/reducers/authentication';
import CustomModal from 'app/shared/layout/custom-modal/custom-modal';

interface ITermsModal extends StateProps, DispatchProps {}

const TermsModal: FC<ITermsModal> = ({ termsAgreed, hasRole, getSess }) => {
  const history = useHistory();
  const [showModal, setShowModal] = useState<boolean>(false);
  const { mutate: agreeTerms } = useAgreeTerms();

  useEffect(() => {
    setShowModal(!termsAgreed && hasRole);
  }, [termsAgreed, hasRole]);

  const disagreeToTerms = () => {
    history.push('/logout');
  };

  const agreeToTerms = () => {
    agreeTerms(null, {
      onSuccess() {
        setShowModal(false);
        getSess();
      },
    });
  };

  return (
    <CustomModal className="terms-modal" isOpen={showModal} centered>
      <div className={'text-left'}>
        <div className={'d-block'}>
          <b>{translate('global.hello')}</b>
        </div>
        <span className={'mt-2 d-block terms'}>{translate('global.renewedTerms')}</span>
        <span className={'mt-2 d-block'}>{translate('global.agreeToTerms')}</span>
        <span className={'mt-2 d-block'}>{translate('global.schoolabyTeam')}</span>
        <Form onSubmit={agreeToTerms}>
          {({ values, handleSubmit }) => (
            <form onSubmit={handleSubmit} className={'my-1'}>
              <Row>
                <Col xs={12} className={'d-flex align-items-start mt-3'}>
                  <Field
                    id={'termsAgreed'}
                    type="checkbox"
                    name="termsAgreed"
                    component={'input'}
                    className={'d-inline-flex justify-content-start mt-1'}
                  />
                  <Label for={'termsAgreed'} className={'ml-2 d-block terms'}>
                    {translate('userManagement.agreeToTermsAndConditions')}
                  </Label>
                </Col>
                <Col xs={12} className={'d-flex align-items-start mt-2'}>
                  <Row noGutters className={'w-100'}>
                    <Col xs={5}>
                      <Button onClick={disagreeToTerms} color={'danger'} type={'button'}>
                        {translate('global.decline')}
                      </Button>
                    </Col>
                    <Col xs={{ size: 5, offset: 2 }}>
                      <Button disabled={!values.termsAgreed} color={'primary'} type={'submit'}>
                        {translate('global.accept')}
                      </Button>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </form>
          )}
        </Form>
      </div>
    </CustomModal>
  );
};

const mapStateToProps = ({ authentication }: IRootState) => ({
  termsAgreed: authentication?.account?.termsAgreed,
  hasRole: hasAnyAuthority(authentication?.account?.authorities, [
    AUTHORITIES.TEACHER,
    AUTHORITIES.ADMIN,
    AUTHORITIES.STUDENT,
    AUTHORITIES.PARENT,
  ]),
});

const mapDispatchToProps = { getSess: getSession };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(TermsModal);
