import React from 'react';
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';
import Settings from './settings/settings';
import Password from './password/password';
import RegisterPage from './register/register';
import Activate from './activate/activate';
import PasswordResetInit from './password-reset/init/password-reset-init';
import PasswordResetFinish from './password-reset/finish/password-reset-finish';
import { AUTHORITIES } from 'app/config/constants';
import PrivateRoute from 'app/shared/auth/private-route';

const Routes = ({ match }) => {
  const allRoles = [AUTHORITIES.ADMIN, AUTHORITIES.USER, AUTHORITIES.TEACHER, AUTHORITIES.STUDENT, AUTHORITIES.PARENT];

  return (
    <div>
      <PrivateRoute path={`${match.url}/settings`} component={Settings} hasAnyAuthorities={allRoles} />
      <PrivateRoute path={`${match.url}/password`} component={Password} hasAnyAuthorities={allRoles} />
      <ErrorBoundaryRoute path={`${match.url}/register`} component={RegisterPage} />
      <ErrorBoundaryRoute path={`${match.url}/activate/:key?`} component={Activate} />
      <ErrorBoundaryRoute path={`${match.url}/reset/request`} component={PasswordResetInit} />
      <ErrorBoundaryRoute path={`${match.url}/reset/finish/:key?`} component={PasswordResetFinish} />
    </div>
  );
};

export default Routes;
