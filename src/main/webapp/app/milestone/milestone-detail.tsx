import React, { useState } from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';
import { Col, CustomInput, Dropdown, DropdownToggle, Row } from 'reactstrap';
import { IRootState } from 'app/shared/reducers';
import Heading from 'app/shared/layout/heading/heading';
import MenuDropdown from 'app/shared/layout/menu-dropdown/menu-dropdown';
import LinesEllipsis from 'react-lines-ellipsis';
import { TextFormat, translate } from 'react-jhipster';
import { APP_LOCAL_DATE_FORMAT, AUTHORITIES } from 'app/config/constants';
import { useQueryParam } from 'app/shared/hooks/useQueryParam';
import { hasAnyAuthority } from 'app/shared/auth/private-route';
import { useGetMilestone } from 'app/shared/services/milestone-api';
import { MILESTONE } from 'app/shared/util/entity-utils';
import MyJourneysBreadcrumbItem from 'app/shared/layout/heading/breadcrumbs/my-journeys-breadcrumb-item';
import JourneyTitleBreadcrumbItem from 'app/shared/layout/heading/breadcrumbs/journey-title-breadcrumb-item';
import { useGetJourney } from 'app/shared/services/journey-api';
import MilestoneTitleBreadcrumbItem from 'app/shared/layout/heading/breadcrumbs/milestone-title-breadcrumb-item';
import { MilestoneMaterials } from 'app/milestone/milestone-materials';
import ReactMarkdown from 'react-markdown';
import { hasMaterials } from 'app/assignment/assignment-detail/assignment-util';
import { useDisplayEntityUpdateState } from 'app/shared/contexts/entity-update-context';
import EntityUpdate from 'app/shared/layout/entity-update/entity-update';
import HeadingNew, { SUB_HEADING } from 'app/shared/layout/heading/heading-new';
import { HomeBreadcrumbItem } from 'app/shared/layout/heading/breadcrumbs/home-breadcrumb-item';
import { getSubjectHeadingIcon } from 'app/shared/icons';
import CustomButton from 'app/shared/layout/custom-button/custom-button';

export interface IMilestoneDetailProps extends StateProps, RouteComponentProps<{ id: string }> {
  previewEnabled?: boolean;
  milestoneId?: number;
  closeMilestone?: () => void;
}

export const MilestoneDetail = ({
  match,
  isTeacherOrAdmin,
  account,
  previewEnabled,
  milestoneId,
  closeMilestone,
}: IMilestoneDetailProps) => {
  const [{ journeyId }] = useQueryParam<{ journeyId: string }>();
  const { data: journey } = useGetJourney(journeyId, !previewEnabled);
  const { data: milestone } = useGetMilestone(match?.params?.id || milestoneId, true, previewEnabled);
  const isAllowedToModify = isTeacherOrAdmin && !previewEnabled && journey?.teachers?.some(teacher => teacher.user.id === account.id);
  const { openMilestoneUpdate } = useDisplayEntityUpdateState();
  const [editDropdownOpen, setEditDropdownOpen] = useState(false);

  const toggleEditDropdown = () => setEditDropdownOpen(!editDropdownOpen);

  const MilestoneHeading = () =>
    previewEnabled ? (
      <Heading
        title={translate('schoolabyApp.milestone.detail.title')}
        journey={journey}
        previewEnabled={previewEnabled}
        onBackClicked={closeMilestone}
        dropdown={
          isAllowedToModify && (
            <MenuDropdown
              entityId={milestone.id}
              entityType={MILESTONE}
              entityTitle={milestone?.title}
              journeyId={+journeyId}
              onEditClicked={() => openMilestoneUpdate(milestone.id)}
            />
          )
        }
      />
    ) : (
      <HeadingNew
        isAllowedToModify={isAllowedToModify}
        headingType={SUB_HEADING}
        title={milestone.title}
        journey={journey}
        icon={getSubjectHeadingIcon(journey?.subject?.label)}
        keepRightNavVisible
      >
        {isAllowedToModify && (
          <div className={'heading-buttons d-flex justify-content-end h-100 ml-4'}>
            <Dropdown isOpen={editDropdownOpen} toggle={toggleEditDropdown}>
              <DropdownToggle tag={'div'} className={'h-100'}>
                <CustomButton
                  iconName={'editNew'}
                  title={translate('entity.dropdown.edit')}
                  buttonType={'secondary'}
                  className={'h-100'}
                  iconWidth={'13'}
                  iconHeight={'13'}
                  size={'md'}
                  outline
                />
              </DropdownToggle>
              <MenuDropdown
                entityId={milestone.id}
                entityType={MILESTONE}
                showDeleteButton={journey?.owner}
                onEditClicked={() => openMilestoneUpdate(milestone.id)}
                entityTitle={milestone?.title}
              />
            </Dropdown>
          </div>
        )}
      </HeadingNew>
    );

  return milestone ? (
    <>
      <Row noGutters={previewEnabled}>
        <Col>
          {!previewEnabled && <HomeBreadcrumbItem />}
          <MyJourneysBreadcrumbItem />
          <JourneyTitleBreadcrumbItem journeyId={journey?.id} journeyTitle={journey?.title} />
          <MilestoneTitleBreadcrumbItem journeyId={journey?.id} milestoneId={milestone.id} milestoneTitle={milestone.title} />
          <MilestoneHeading />
        </Col>
      </Row>
      <Row noGutters={previewEnabled} className={'justify-content-center'}>
        <Col md="8">
          <h3 className={'my-4'}>
            <LinesEllipsis text={milestone.title} maxLine={2} />
          </h3>
          <ReactMarkdown className={'text-secondary preserve-space text-break'} source={milestone.description} />
          <Row noGutters={previewEnabled}>
            <Col md={'5'}>
              <p className={'text-body'}>{translate('schoolabyApp.milestone.educationalAlignments')}</p>

              <p className={'text-secondary'}>
                {' '}
                {milestone?.educationalAlignments?.map((alignment, i) => (
                  <span key={alignment?.id}>
                    <span>{alignment?.title}</span>
                    {i === milestone?.educationalAlignments?.length - 1 ? '' : ', '}
                  </span>
                ))}
              </p>
            </Col>
            <Col md={'5'}>
              <p className={'text-body'}>{translate('schoolabyApp.milestone.endDate')}</p>
              <p className={'text-secondary'}>
                <TextFormat type="date" value={milestone.endDate} format={APP_LOCAL_DATE_FORMAT} />
              </p>
            </Col>
            <Col md={'2'}>
              {isAllowedToModify && (
                <>
                  <p className="text-body">{translate('entity.publishedState.hidden')}</p>
                  <CustomInput type="switch" id="hidden" disabled checked={milestone.hidden} className="mb-3" />
                </>
              )}
            </Col>
          </Row>

          <Row noGutters={previewEnabled}>
            <Col>
              <h5 className="font-weight-normal">{translate('schoolabyApp.milestone.materials')}</h5>
              <Row noGutters={!hasMaterials(milestone)} className="d-flex">
                {hasMaterials(milestone) ? (
                  <MilestoneMaterials milestone={milestone} />
                ) : (
                  <p>{translate('schoolabyApp.milestone.detail.noAssets')}</p>
                )}
              </Row>
            </Col>
          </Row>
        </Col>
      </Row>
      <EntityUpdate />
    </>
  ) : null;
};

const mapStateToProps = ({ authentication }: IRootState) => ({
  account: authentication.account,
  isTeacherOrAdmin: hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.TEACHER, AUTHORITIES.ADMIN]),
});

type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(MilestoneDetail);
