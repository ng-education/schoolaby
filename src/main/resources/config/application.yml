management:
  endpoints:
    web:
      base-path: /management
      exposure:
        include: [ 'configprops', 'env', 'health', 'info', 'jhimetrics', 'logfile', 'loggers', 'prometheus', 'threaddump' ]
  endpoint:
    health:
      show-details: when_authorized
      roles: 'ROLE_ADMIN'
    jhimetrics:
      enabled: true
  info:
    git:
      mode: full
  health:
    mail:
      enabled: false # When using the MailService, configure an SMTP server and set this to true
  metrics:
    export:
      # Prometheus is the default metrics backend
      prometheus:
        enabled: true
        step: 60
    enable:
      http: true
      jvm: true
      logback: true
      process: true
      system: true
    distribution:
      percentiles-histogram:
        all: true
      percentiles:
        all: 0, 0.5, 0.75, 0.95, 0.99, 1.0
    tags:
      application: ${spring.application.name}
    web:
      server:
        request:
          autotime:
            enabled: true
azure:
  activedirectory:
    session-stateless: true
    tenant-id:
    allow-telemetry: false

spring:
  application:
    name: Schoolaby
  jackson:
    default-property-inclusion: non_empty
  jmx:
    enabled: false
  data:
    jpa:
      repositories:
        bootstrap-mode: deferred
  flyway:
    out-of-order: true
  jpa:
    open-in-view: false
    properties:
      hibernate.jdbc.time_zone: UTC
      hibernate.id.new_generator_mappings: true
      hibernate.connection.provider_disables_autocommit: true
      hibernate.cache.use_second_level_cache: true
      hibernate.cache.use_query_cache: true
      hibernate.generate_statistics: false
      hibernate.jdbc.batch_size: 50
      hibernate.order_inserts: true
      hibernate.order_updates: true
      hibernate.query.fail_on_pagination_over_collection_fetch: true
      hibernate.query.in_clause_parameter_padding: true
      hibernate.session.events.log.LOG_QUERIES_SLOWER_THAN_MS: 20
    hibernate:
      ddl-auto: none
      naming:
        physical-strategy: org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy
        implicit-strategy: org.springframework.boot.orm.jpa.hibernate.SpringImplicitNamingStrategy
  messages:
    basename: i18n/messages
  main:
    allow-bean-definition-overriding: true
    allow-circular-references: true  # TODO: Remove this and fix circular references in mappers
  mvc:
    pathmatch:
      matching-strategy: ant_path_matcher  # TODO: Remove this and fix path matchers
  #    TODO: All this azure conf is because we can't upgrade azure dependency, because jhipster hasn't updated Spring Boot
  security.oauth2.client:
    provider:
      azure:
        authorization-uri: https://login.microsoftonline.com/common/oauth2/v2.0/authorize
        token-uri: https://login.microsoftonline.com/common/oauth2/v2.0/token
        user-info-uri: https://graph.microsoft.com/oidc/userinfo
        user-info-authentication-method: header
        jwk-set-uri: https://login.microsoftonline.com/common/discovery/v2.0/keys
        user-name-attribute: sub
      yahoo:
        authorization-uri: https://api.login.yahoo.com/oauth2/request_auth
        token-uri: https://api.login.yahoo.com/oauth2/get_token
        user-info-uri: https://api.login.yahoo.com/openid/v1/userinfo
        user-info-authentication-method: header
        user-name-attribute: sub
      ekool:
        authorization-uri: https://staging-login.ekool.eu/oauth/authorize
        token-uri: https://staging-login.ekool.eu/oauth/token
        user-info-uri: https://api-v2-staging.ekool.eu/generaldata
        user-info-authentication-method: header
        user-name-attribute: id
    registration:
      google:
        client-id: client-id
        client-secret: client-secret
      azure:
        client-id: client-id
        client-secret: client-secret
        client-authentication-method: post
        authorization-grant-type: authorization_code
        redirect-uri: "{baseUrl}/login/oauth2/code/{registrationId}"
        scope: openid, https://graph.microsoft.com/user.read, profile, email
        client-name: Azure
        provider: azure
      yahoo:
        client-id: client-id
        client-secret: client-secret
        authorization-grant-type: authorization_code
        redirect-uri: "{baseUrl}/login/oauth2/code/{registrationId}"
        provider: yahoo
      ekool:
        client-id: client-id
        client-secret: client-secret
        client-authentication-method: post
        authorization-grant-type: authorization_code
        redirect-uri: "{baseUrl}/login/oauth2/code/{registrationId}"
        provider: ekool
        scope:
          - users-basic
          - users-schools
          - users-roles
  task:
    execution:
      thread-name-prefix: schoolaby-task-
      pool:
        core-size: 2
        max-size: 50
        queue-capacity: 10000
    scheduling:
      thread-name-prefix: schoolaby-scheduling-
      pool:
        size: 2
  thymeleaf:
    mode: HTML
  output:
    ansi:
      console-available: true
  servlet:
    multipart:
      max-file-size: 800MB
      max-request-size: 800MB

# Properties to be exposed on the /info management endpoint
info:
  # Comma separated list of profiles that will trigger the ribbon to show
  display-ribbon-on-profiles: 'dev'

jhipster:
  cors:
    allowed-methods: '*'
    allowed-headers: '*'
    exposed-headers: 'Authorization,Link,X-Total-Count'
    allow-credentials: true
    max-age: 1800
  clientApp:
    name: 'schoolabyApp'
  mail:
    from: no-reply@schoolaby.com
  swagger:
    default-include-pattern: /api/.*
    title: Schoolaby API
    description: Schoolaby API documentation
    version: 0.0.1
    terms-of-service-url:
    contact-name:
    contact-url:
    contact-email:
    license: unlicensed
    license-url:
# ===================================================================
# Application specific properties
# Add your own application properties here, see the ApplicationProperties class
# to have type-safe configuration, like in the JHipsterProperties above
#
# More documentation is available at:
# https://www.jhipster.tech/common-application-properties/
# ===================================================================

# application:
application:
  mail:
    supportEmail: support@schoolaby.com
  monitoring:
    teams-hook: https://teamshook.localhost:5551

harid:
  logging: false
  client-id: client-id
  client-secret: client-secret
  authorize-url: https://test.harid.ee/et/authorizations/new
  token-url: https://test.harid.ee/et/access_tokens
  user-data-url: https://test.harid.ee/et/user_info
  callback-url: http://localhost/authenticate/harid
lti:
  callback-url:
  tool-consumer-info-product-family-code: schoolaby
  tool-consumer-info-version: 1.0
  tool-consumer-instance-name: Schoolaby
  tool-consumer-instance-contact-email:
  launch-presentation-document-target: iframe
lti-advantage:
  iss:
  token-validity-in-seconds: 43200

ekoolikott:
  host: e-koolikott.ee
ekool:
  user-schools-uri: https://api-v2-staging.ekool.eu/user/me/schools
  user-roles-uri: https://api-v2-staging.ekool.eu/user/me/roles
  user-basic-uri: https://api-v2-staging.ekool.eu/user/me/basic

proxy:
  embeddable-headers-check:
    timeout: 3000

cloud:
  aws:
    credentials:
      accessKey:
      secretKey:
      instanceProfile: true
      useDefaultAwsCredentialsChain: true
    stack:
      auto: false
    region:
      auto: false

server:
  forward-headers-strategy: NATIVE

oauth-server:
  iss: https://test-schoolaby-test.ml
