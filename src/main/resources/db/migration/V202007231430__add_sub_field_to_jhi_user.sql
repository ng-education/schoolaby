ALTER TABLE public.jhi_user
ADD sub VARCHAR(255);
ALTER TABLE public.jhi_user ALTER COLUMN login DROP NOT NULL;
ALTER TABLE public.jhi_user ALTER COLUMN password_hash DROP NOT NULL;
