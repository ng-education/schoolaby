ALTER TABLE public.school ADD COLUMN country TEXT;
UPDATE public.school SET country = 'Estonia' WHERE country IS NULL;
ALTER TABLE public.school ALTER COLUMN country SET NOT NULL;
