ALTER TABLE uploaded_file
    DROP COLUMN path;
ALTER TABLE uploaded_file
    RENAME COLUMN name TO original_name;
ALTER TABLE uploaded_file
    RENAME COLUMN upload_url TO unique_name;

UPDATE uploaded_file
SET unique_name = substring(unique_name FROM '[^/]+$')
