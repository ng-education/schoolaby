ALTER TABLE lti_score
    ALTER COLUMN score_given TYPE numeric(1000, 2),
    ALTER COLUMN score_maximum TYPE numeric(1000, 2)
