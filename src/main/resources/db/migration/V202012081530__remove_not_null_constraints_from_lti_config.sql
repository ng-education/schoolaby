ALTER TABLE lti_config
    ALTER COLUMN consumer_key DROP NOT NULL,
    ALTER COLUMN shared_secret DROP NOT NULL;
