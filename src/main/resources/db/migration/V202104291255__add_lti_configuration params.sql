ALTER TABLE lti_config
    ADD COLUMN launch_url           text,
    ADD COLUMN deep_linking_url     text,
    ADD COLUMN login_initiation_url text,
    ADD COLUMN redirect_host        text,
    ADD COLUMN jwks_url             text;;
