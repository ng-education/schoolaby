ALTER TABLE public.submission
ADD COLUMN re_submittable BOOLEAN DEFAULT FALSE,
ADD COLUMN feedback_date timestamp,
ADD COLUMN submitted timestamp;
