UPDATE journey
SET last_modified_by   = 'admin',
    last_modified_date = NOW()
WHERE last_modified_by IS NULL
   OR last_modified_date IS NULL;

UPDATE journey
SET educational_level_id = (SELECT id FROM educational_level WHERE name = 'I kooliaste')
WHERE educational_level_id IS NULL;

UPDATE journey
SET template = false
WHERE template IS NULL;

ALTER TABLE journey
    ALTER COLUMN creator_id SET NOT NULL,
    ALTER COLUMN created_by SET NOT NULL,
    ALTER COLUMN created_date SET NOT NULL,
    ALTER COLUMN last_modified_by SET NOT NULL,
    ALTER COLUMN last_modified_date SET NOT NULL,
    ALTER COLUMN template SET DEFAULT FALSE,
    ALTER COLUMN template SET NOT NULL,
    ALTER COLUMN educational_level_id SET NOT NULL;
