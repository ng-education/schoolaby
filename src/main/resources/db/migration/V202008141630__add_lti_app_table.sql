CREATE TABLE public.lti_app
(
    id                 bigint NOT NULL,
    name               text   NOT NULL,
    description        text   NOT NULL,
    version            text   NOT NULL,
    launch_url         text,
    image_url          text,
    created_by         text,
    created_date       timestamp without time zone,
    last_modified_by   text,
    last_modified_date timestamp without time zone,
    deleted            timestamp without time zone
);
ALTER TABLE ONLY public.lti_app
    ADD CONSTRAINT lti_app_pkey PRIMARY KEY (id);

ALTER TABLE public.lti_config
    DROP COLUMN name;
ALTER TABLE public.lti_config
    DROP COLUMN version;
ALTER TABLE public.lti_config
    ADD COLUMN lti_app_id bigint;

ALTER TABLE ONLY public.lti_config
    ADD CONSTRAINT fk_lti_config_lti_app_id FOREIGN KEY (lti_app_id) REFERENCES public.lti_app (id);

ALTER TABLE ONLY public.lti_config
    ADD COLUMN journey_id bigint;

DROP TABLE public.lti_link CASCADE;
