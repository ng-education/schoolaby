CREATE TABLE public.jhi_user_schools
(
    school_id    bigint NOT NULL REFERENCES public.school,
    user_id bigint NOT NULL REFERENCES public.jhi_user
);
