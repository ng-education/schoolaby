INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '57', '74000624', 'Eesti Kunstiakadeemia', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '112', '74001086', 'Eesti Maaülikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '60', '74000547', 'Eesti Muusika- ja Teatriakadeemia', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '75', '90013934', 'Estonian Business School', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '93', '74000323', 'Tallinna Tehnikaülikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '94', '74000122', 'Tallinna Ülikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '61', '74001073', 'Tartu Ülikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2322', '80208720', 'EELK Usuteaduse Instituut', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '71', '80196158', 'Eesti EKB Liit Kõrgem Usuteaduslik Seminar', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '138', '10588537', 'Eesti Ettevõtluskõrgkool Mainor', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '144', '70005699', 'Eesti Lennuakadeemia', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '74', '80196661', 'Eesti Metodisti Kiriku Teoloogiline Seminar', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '157', '70008641', 'Kaitseväe Akadeemia', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '143', '70005950', 'Kõrgem Kunstikool Pallas', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '56', '70004465', 'Sisekaitseakadeemia', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1019', '70003773', 'Tallinna Tehnikakõrgkool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2', '70003980', 'Tallinna Tervishoiu Kõrgkool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '11', '70005714', 'Tartu Tervishoiu Kõrgkool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '18571', '70007742', 'Ida- Virumaa Kutseharidusekeskus Narva filiaal', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '18591', '70007742', 'Ida- Virumaa Kutseharidusekeskus Sillamäe filiaal', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '18611', '70007742', 'Ida-Virumaa Kutseharidusekeskus Jõhvi filiaal', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2220', '70008641', 'KVÜÕA Lahingukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2232', '70004465', 'SKA Politsei- ja piirivalvekolledži Paikuse kool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2233', '70004465', 'SKA Päästekolledži päästekool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1059', '70003980', 'Tallinna Tervishoiu Kõrgkool - Kohtla-Järve filiaal', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '6711', '70003980', 'Tallinna Tervishoiu Kõrgkool Pärnus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
