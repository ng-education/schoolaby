INSERT INTO public.submission_feedback (id, grade, submission_id, student_id, feedback_date)
SELECT nextval('submission_feedback_sequence'), grade, id, authors_id, feedback_date
FROM submission
         left join submission_authors
                   on submission.id = submission_authors.submission_id
where grade is not null
  and deleted is null
  and group_id is null
  and authors_id is not null;

INSERT INTO public.submission_feedback (id, grade, submission_id, group_id, feedback_date)
SELECT nextval('submission_feedback_sequence'), grade, id, group_id, feedback_date
FROM submission
where grade is not null
  and deleted is null
  and group_id is not null;
