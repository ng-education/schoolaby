ALTER TABLE public.journey
    ADD COLUMN national_curriculum BOOLEAN DEFAULT TRUE;

INSERT INTO public.educational_level(id, name)
VALUES (5, 'Täiendkoolitus');
