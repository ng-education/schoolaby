ALTER TABLE public.submission_feedback
    ADD COLUMN creator_id BIGINT references public.jhi_user;

UPDATE public.submission_feedback sf SET creator_id = (SELECT a.creator_id FROM submission_feedback sf2 left join submission s on s.id = sf2.submission_id left join assignment a on a.id = s.assignment_id WHERE sf.id = sf2.id);

ALTER TABLE public.submission_feedback ALTER COLUMN creator_id SET NOT NULL;
