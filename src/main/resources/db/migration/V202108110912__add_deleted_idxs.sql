create index journey_end_date_idx on journey(end_date, deleted);
create index milestone_deleted_idx on milestone(deleted);
create index assignment_deleted_idx on assignment(deleted);
create index lti_resource_deleted_idx on lti_resource(deleted);
