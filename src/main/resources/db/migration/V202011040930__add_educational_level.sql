CREATE TABLE public.educational_level
(
    id                    bigint NOT NULL,
    name                  text   NOT NULL,
    created_by            text,
    created_date          timestamp without time zone,
    last_modified_by      text,
    last_modified_date    timestamp without time zone,
    deleted               timestamp without time zone
);

ALTER TABLE ONLY public.educational_level
    ADD CONSTRAINT educational_level_pkey PRIMARY KEY (id);

INSERT INTO public.educational_level(id, name)
VALUES (1, 'I kooliaste'),
       (2, 'II kooliaste'),
       (3, 'III kooliaste'),
       (4, 'Gümnaasium');

ALTER TABLE public.journey
    ADD COLUMN educational_level_id bigint;

ALTER TABLE ONLY public.journey
    ADD CONSTRAINT fk_educational_level_id FOREIGN KEY (educational_level_id) REFERENCES public.educational_level (id);
