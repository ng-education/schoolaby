UPDATE lti_config
SET journey_id = NULL
WHERE journey_id IS NOT NULL
  AND type = 'PLATFORM';

DELETE
FROM lti_config
WHERE id IN (SELECT lc.id
             FROM lti_config lc
             WHERE lc.type = 'JOURNEY'
               AND lc.deleted IS NULL
               AND exists(SELECT 1 FROM lti_config lc2 WHERE lc2.lti_app_id = lc.lti_app_id AND lc2.type = 'PLATFORM' AND lc2.deleted IS NULL)
               AND exists(SELECT 1 FROM lti_config lc2 WHERE lc2.lti_app_id = lc.lti_app_id AND lc2.type = 'JOURNEY' AND lc2.deleted IS NULL));

ALTER TABLE lti_config
    ADD CONSTRAINT type_must_be_journey_if_journey_id_not_null CHECK ( NOT (type = 'PLATFORM' AND journey_id IS NOT NULL OR type = 'JOURNEY' AND journey_id IS NULL) );


