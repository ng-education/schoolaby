CREATE TYPE AUTHENTICATION_TYPE AS ENUM
    (
        'HARID'
        );

CREATE TABLE external_authentication
(
    external_id        TEXT                NOT NULL,
    type               AUTHENTICATION_TYPE NOT NULL,
    data               JSONB,
    user_id            BIGINT              NOT NULL REFERENCES jhi_user,
    created_by         TEXT                NOT NULL,
    created_date       TIMESTAMP,
    last_modified_by   TEXT,
    last_modified_date TIMESTAMP,
    deleted            TIMESTAMP,
    PRIMARY KEY (external_id, type)
);

DO
$$
    DECLARE
        _user jhi_user%ROWTYPE;
    BEGIN
        FOR _user IN
            SELECT * FROM jhi_user
            LOOP
                IF (_user.sub IS NOT NULL AND _user.sub != '') THEN
                    INSERT INTO external_authentication (external_id, type, data, user_id, created_by)
                    VALUES (_user.sub, 'HARID', NULL, _user.id, 'migration');
                END IF;
            END LOOP;
    END ;
$$;

ALTER TABLE jhi_user
    DROP COLUMN sub;
