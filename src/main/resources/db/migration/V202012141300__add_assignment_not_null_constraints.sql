UPDATE assignment
SET flexible_deadline = FALSE
WHERE flexible_deadline IS NULL;

UPDATE assignment
SET grading_scheme_id = (SELECT id FROM grading_scheme WHERE code = 'NONE')
WHERE grading_scheme_id IS NULL;

DELETE
FROM assignment_educational_alignments
WHERE assignment_id IN (SELECT a.id FROM assignment a WHERE a.milestone_id IS NULL);

DELETE
FROM assignment
WHERE milestone_id IS NULL;

UPDATE assignment
SET created_by = 'admin'
WHERE created_by IS NULL;

UPDATE assignment a
SET creator_id = (SELECT u.id FROM jhi_user u WHERE u.login = a.created_by)
WHERE a.creator_id IS NULL;

-- last modified by user that is no longer in db
UPDATE assignment a
SET creator_id = (SELECT u.id FROM jhi_user u WHERE u.login = 'admin'),
    created_by = 'admin'
WHERE a.creator_id IS NULL;

UPDATE assignment
SET created_date = now()
WHERE assignment.created_date IS NULL;

UPDATE submission
SET deleted = now()
WHERE assignment_id IN (SELECT a.id FROM assignment a WHERE a.deleted IS NOT NULL)
  AND deleted IS NULL;

ALTER TABLE assignment
    DROP COLUMN assignment_type;

ALTER TABLE assignment
    ALTER COLUMN deadline SET NOT NULL,
    ALTER COLUMN flexible_deadline SET DEFAULT FALSE,
    ALTER COLUMN flexible_deadline SET NOT NULL,
    ALTER COLUMN grading_scheme_id SET NOT NULL,
    ALTER COLUMN milestone_id SET NOT NULL,
    ALTER COLUMN creator_id SET NOT NULL,
    ALTER COLUMN created_by SET NOT NULL,
    ALTER COLUMN created_date SET NOT NULL,
    ALTER COLUMN last_modified_by SET NOT NULL,
    ALTER COLUMN last_modified_date SET NOT NULL;
