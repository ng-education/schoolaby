UPDATE grading_scheme_value
SET percentage_range = 100, grade = 'PASS'
FROM grading_scheme
WHERE grading_scheme.code = 'NONE'
  AND grading_scheme_value.grading_scheme_id = grading_scheme.id
