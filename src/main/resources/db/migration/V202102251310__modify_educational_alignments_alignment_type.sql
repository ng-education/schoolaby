UPDATE public.educational_alignment
SET alignment_type = 'subject';

UPDATE public.educational_alignment
SET alignment_type = 'subjectArea'
WHERE title in ('Matemaatika', 'Keel ja kirjandus', 'Võõrkeeled', 'Loodusained', 'Sotsiaalained', 'Kunstiained', 'Tehnoloogia', 'Kehaline kasvatus', 'Valikõppeained');
