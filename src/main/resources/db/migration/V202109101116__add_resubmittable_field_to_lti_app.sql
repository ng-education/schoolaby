ALTER TABLE public.lti_app ADD COLUMN resubmittable boolean NOT NULL DEFAULT true;

UPDATE public.lti_app SET resubmittable = false WHERE name = 'Schoolaby LTI app';
