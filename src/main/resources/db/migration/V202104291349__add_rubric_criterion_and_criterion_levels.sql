CREATE TABLE rubric
(
    id    BIGINT NOT NULL PRIMARY KEY,
    title TEXT   NOT NULL,
    assignment_id  BIGINT NOT NULL
        CONSTRAINT fk_rubric_assignment_id REFERENCES assignment (id),
    created_by         TEXT NOT NULL,
    created_date       TIMESTAMP,
    last_modified_by   TEXT,
    last_modified_date TIMESTAMP,
    deleted            TIMESTAMP
);

CREATE TABLE criterion
(
    id          BIGINT NOT NULL PRIMARY KEY,
    title       TEXT   NOT NULL,
    description TEXT   NOT NULL,
    sequence_number BIGINT NOT NULL,
    rubric_id   BIGINT NOT NULL
        CONSTRAINT fk_criterion_rubric_id REFERENCES rubric (id),
    created_by         TEXT NOT NULL,
    created_date       TIMESTAMP,
    last_modified_by   TEXT,
    last_modified_date TIMESTAMP,
    deleted            TIMESTAMP
);

CREATE TABLE criterion_level
(
    id           BIGINT NOT NULL PRIMARY KEY,
    title        TEXT   NOT NULL,
    points       BIGINT NOT NULL,
    description  TEXT   NOT NULL,
    sequence_number BIGINT NOT NULL,
    criterion_id BIGINT NOT NULL
        CONSTRAINT fk_criterion_level_criterion_id REFERENCES criterion (id),
    created_by         TEXT NOT NULL,
    created_date       TIMESTAMP,
    last_modified_by   TEXT,
    last_modified_date TIMESTAMP,
    deleted            TIMESTAMP
);
