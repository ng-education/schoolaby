SELECT id INTO TEMPORARY submission_to_delete from public.submission
where id in (98007, 98006) AND last_modified_date = (SELECT MIN(last_modified_date)
                                                       from (SELECT *
                                                             FROM public.submission
                                                             WHERE id in (98007, 98006)) as ds);

UPDATE public.submission_feedback SET deleted=now()
WHERE submission_id in (98007, 98006) AND submission_id in (SELECT id from submission_to_delete);

UPDATE public.submission SET deleted=now()
WHERE id in (SELECT id from submission_to_delete);
