INSERT INTO chat_people (person_id, chat_id)
SELECT j.creator_id, c.id
FROM chat as c
         INNER JOIN journey as j on c.journey_id = j.id
WHERE NOT EXISTS(
        SELECT
        FROM journey_people as jp
        WHERE jp.people_id = j.creator_id
          and jp.journey_id = j.id
    );

INSERT INTO journey_people (people_id, journey_id)
SELECT j.creator_id, j.id
FROM journey as j
WHERE NOT EXISTS(
        SELECT
        FROM journey_people as jp
        WHERE jp.people_id = j.creator_id
          and jp.journey_id = j.id
    );
