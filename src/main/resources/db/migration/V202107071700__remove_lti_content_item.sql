DO
$$
    DECLARE
        record RECORD;
        line_item_id bigint;
    BEGIN
        FOR record IN
            SELECT (lci.properties->>'url')::TEXT as resource_launch_url,
                   (lci.properties->'lineItem'->'scoreConstraints'->>'totalMaximum')::BIGINT as score_maximum,
                   (lci.properties->>'title')::TEXT as label,
                   lr.id as lti_resource_id,
                   lr.created_by,
                   lr.created_date,
                   lr.last_modified_by,
                   lr.last_modified_date
            FROM public.lti_content_item lci
                     JOIN lti_resource lr on lci.id = lr.lti_content_item_id
            LOOP
                SELECT COALESCE(max(id), 0) + 1 FROM public.lti_line_item INTO line_item_id;

                INSERT INTO public.lti_line_item(id, score_maximum, label, lti_resource_id, created_date, created_by, last_modified_date, last_modified_by)
                VALUES (line_item_id, record.score_maximum, record.label, record.lti_resource_id, record.created_date, record.created_by, record.last_modified_date, record.last_modified_by);

                UPDATE public.lti_resource
                SET launch_url = record.resource_launch_url
                WHERE id = record.lti_resource_id;
            END LOOP;
    END ;
$$;

ALTER TABLE lti_resource
    DROP COLUMN lti_content_item_id;

DROP TABLE lti_content_item;
