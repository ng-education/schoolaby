ALTER TABLE material
    ADD COLUMN uploaded_file_id BIGINT REFERENCES uploaded_file;
