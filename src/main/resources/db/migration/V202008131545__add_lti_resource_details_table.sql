CREATE TABLE public.lti_link
(
    id                 bigint NOT NULL,
    type               text   NOT NULL,
    url                text   NOT NULL,
    config_id          bigint NOT NULL,
    created_by         text,
    created_date       timestamp without time zone,
    last_modified_by   text,
    last_modified_date timestamp without time zone,
    deleted            timestamp without time zone
);
ALTER TABLE ONLY public.lti_link
    ADD CONSTRAINT lti_link_pkey PRIMARY KEY (id);

CREATE TABLE public.lti_config
(
    id                 bigint NOT NULL,
    name               text   NOT NULL,
    consumer_key       text   NOT NULL,
    shared_secret      text   NOT NULL,
    version            text   NOT NULL,
    created_by         text,
    created_date       timestamp without time zone,
    last_modified_by   text,
    last_modified_date timestamp without time zone,
    deleted            timestamp without time zone
);
ALTER TABLE ONLY public.lti_config
    ADD CONSTRAINT lti_config_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.lti_link
    ADD CONSTRAINT fk_lti_link_config FOREIGN KEY (config_id) REFERENCES public.lti_config (id);

ALTER TABLE public.assignment
ADD COLUMN lti_link_id bigint;

ALTER TABLE public.assignment
    ADD CONSTRAINT fk_assignment_lti_link_id FOREIGN KEY (lti_link_id) REFERENCES public.lti_link (id);
