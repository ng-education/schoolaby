CREATE TABLE public.submission_feedback
(
    id                 BIGINT NOT NULL PRIMARY KEY,
    student_id         BIGINT REFERENCES public.jhi_user,
    group_id           BIGINT REFERENCES public.group,
    submission_id      BIGINT NOT NULL REFERENCES public.submission,
    grade              text,
    value              text,
    feedback_date      timestamp without time zone,
    created_by         text,
    created_date       timestamp without time zone,
    last_modified_by   text,
    last_modified_date timestamp without time zone,
    deleted            timestamp without time zone,
    CONSTRAINT student_id_or_group_id_must_not_be_null CHECK(student_id IS NOT NULL OR group_id IS NOT NULL)
);

CREATE SEQUENCE IF NOT EXISTS submission_feedback_sequence AS BIGINT;




