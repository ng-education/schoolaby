-- New general education schools
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '50716', '80593250', 'Hiiumaa Vabakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '51196', '77001406', 'Tammiste Lasteaed-Algkool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '50856', '77001240', 'Tabasalu Gümnaasium', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '50797', '80577216', 'Shanghai ja San-Francisco Püha Johannese nimeline Kool Tallinnas', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '50796', '80549036', 'Pärnu Kristlik Erakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');

-- Update general education schools
UPDATE public.school
SET name='Leie Kool', last_modified_date=NOW()::timestamp
WHERE  ehis_id='75012297';
UPDATE public.school
SET name='Risti Kool', last_modified_date=NOW()::timestamp
WHERE  ehis_id='75020233';
UPDATE public.school
SET name='Surju Kool', last_modified_date=NOW()::timestamp
WHERE  ehis_id='75000609';
UPDATE public.school
SET name='Melliste Kool', last_modified_date=NOW()::timestamp
WHERE  ehis_id='75015249';
UPDATE public.school
SET name='Tallinna Arte Gümnaasium', last_modified_date=NOW()::timestamp
WHERE  ehis_id='75018443';
UPDATE public.school
SET name='Rakke Kool', last_modified_date=NOW()::timestamp
WHERE  ehis_id='75010051';
UPDATE public.school
SET name='Kuressaare Hariduse Kool', last_modified_date=NOW()::timestamp
WHERE  ehis_id='75024538';
UPDATE public.school
SET name='Kambja Põhikool', last_modified_date=NOW()::timestamp
WHERE  ehis_id='75009562';
UPDATE public.school
SET name='Laekvere Kool', last_modified_date=NOW()::timestamp
WHERE  ehis_id='75010832';
UPDATE public.school
SET name='Carl Robert Jakobsoni nim Torma Põhikool', last_modified_date=NOW()::timestamp
WHERE  ehis_id='75023680';
UPDATE public.school
SET name='Tabasalu Kool', last_modified_date=NOW()::timestamp
WHERE  ehis_id='75014149';
UPDATE public.school
SET name='Tallinna Kivimäe Põhikool', last_modified_date=NOW()::timestamp
WHERE  ehis_id='75016645';
UPDATE public.school
SET name='VIVERE KOOL', last_modified_date=NOW()::timestamp
WHERE  ehis_id='80559247';
UPDATE public.school
SET name='Põlva Roosi Kool', last_modified_date=NOW()::timestamp
WHERE  ehis_id='75009266';
UPDATE public.school
SET name='Kuressaare Nooruse Kool', last_modified_date=NOW()::timestamp
WHERE  ehis_id='75028022';
UPDATE public.school
SET name='Tallinna Pae Gümnaasium', last_modified_date=NOW()::timestamp
WHERE  ehis_id='75016935';
UPDATE public.school
SET name='Tallinna Mustjõe Gümnaasium', last_modified_date=NOW()::timestamp
WHERE  ehis_id='75018992';
UPDATE public.school
SET name='Viimsi Kool', last_modified_date=NOW()::timestamp
WHERE  ehis_id='75021304';
UPDATE public.school
SET name='Tallinna Rahumäe Põhikool', last_modified_date=NOW()::timestamp
WHERE  ehis_id='75016705';
UPDATE public.school
SET name='Tallinna Laagna Gümnaasium', last_modified_date=NOW()::timestamp
WHERE  ehis_id='75016869';

-- Update hobby schools
-- name changes
UPDATE public.school
SET name='.Medoonity Huvikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='36378';
UPDATE public.school
SET name='EVE STUUDIO', last_modified_date=NOW()::timestamp
WHERE ehis_id='38631';
UPDATE public.school
SET name='Huvikool Keeltekoda', last_modified_date=NOW()::timestamp
WHERE ehis_id='2349';
UPDATE public.school
SET name='Huvikool Koduõpetaja', last_modified_date=NOW()::timestamp
WHERE ehis_id='21391';
UPDATE public.school
SET name='Joosep Aaviku nimeline Kuressaare Muusikakool', last_modified_date=NOW()::timestamp
WHERE ehis_id='1305';
UPDATE public.school
SET name='Lääne-Harju Muusika- ja Kunstide Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='1132';
UPDATE public.school
SET name='Muusikalikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='14256';
UPDATE public.school
SET name='Stuudio dotE Tantsukool', last_modified_date=NOW()::timestamp
WHERE ehis_id='40391';
-- ehis_id changes
UPDATE public.school
SET ehis_id='45776', last_modified_date=NOW()::timestamp
WHERE name='Huvikool Tulevikutähed';
UPDATE public.school
SET ehis_id='1286', last_modified_date=NOW()::timestamp
WHERE name='Jõelähtme Muusika- ja Kunstikool';

-- New hobby schools
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '49356', '80594338', 'Angela Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '49836', '80595243', 'Aruküla Sulgpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '48696', '80380726', 'Avatud Kooli Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '47157', '80366152', 'Dance Republic Tantsukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '46390', '80340715', 'Desiree Tantsustuudio', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '49456', '80213230', 'FC Kose Jalgpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '42911', '80568418', 'FV Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '48378', '14178210', 'Halliku Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '47076', '80565667', 'Huvikool Football Planet', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '49096', '11272782', 'Huvikool GAME', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '50578', '14817748', 'Huvikool Süsi ja Savi', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '50778', '80597584', 'Huvikool TANTSUTEATER FOCUS', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '50658', '16134889', 'Kantan Education huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '49316', '80068555', 'Karateklubi Falco Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '44217', '80141134', 'Keila Jalgpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '46896', '80277945', 'Kosejõe Käsitöökool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '48536', '80182274', 'Loodustarkuse Teadushuvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '48061', '16121125', 'Mina Tean eelkool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '44396', '80551441', 'ProDance Tantsukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '48656', '80593577', 'Spordiklubi Pärnu Vaprus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '47540', '80568080', 'Tantsukool eMotion', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
    id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '47879', '14880648', 'Tiigripuu Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');



