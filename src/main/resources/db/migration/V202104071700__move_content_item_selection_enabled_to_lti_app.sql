ALTER TABLE lti_config
    DROP COLUMN content_item_selection_enabled;
ALTER TABLE lti_app
    ADD COLUMN content_item_selection_enabled boolean DEFAULT false;

