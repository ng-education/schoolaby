DELETE
FROM chat_people cp
WHERE cp.person_id IN (SELECT cp2.person_id
                       FROM chat c
                                JOIN chat_people cp2 on c.id = cp2.chat_id
                       WHERE deleted IS NULL
                         AND cp.chat_id = c.id
                         AND submission_id IS NOT NULL);

INSERT INTO chat_people (chat_id, person_id)
SELECT chat_id, person_id
FROM (SELECT c.id as chat_id, sa.authors_id as person_id
      FROM chat c
               LEFT JOIN submission s on s.id = c.submission_id
               LEFT JOIN submission_authors sa on s.id = sa.submission_id
      WHERE c.submission_id IS NOT NULL
        AND c.deleted IS NULL
        AND s.deleted IS NULL) as people_from_submission_authors;


INSERT INTO chat_people (chat_id, person_id)
SELECT chat_id, person_id
FROM (SELECT c.id as chat_id, jt.user_id as person_id
      FROM chat c
               LEFT JOIN submission s on c.submission_id = s.id
               LEFT JOIN journey j on c.journey_id = j.id
               LEFT JOIN journey_teachers jt on j.id = jt.journey_id
      WHERE c.submission_id IS NOT NULL
        AND jt.user_id NOT IN (SELECT person_id FROM chat_people WHERE chat_id = c.id)
        AND jt.user_id IS NOT NULL
        AND s.deleted IS NULL
        AND j.deleted IS NULL
        AND c.deleted IS NULL) as people_from_journey_teachers;
