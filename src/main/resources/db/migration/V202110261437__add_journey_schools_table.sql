CREATE TABLE public.journey_schools
(
    journey_id BIGINT NOT NULL REFERENCES public.journey,
    school_id  BIGINT NOT NULL REFERENCES public.school
);

INSERT INTO public.journey_schools SELECT id as journey_id, school_id FROM public.journey j WHERE j.school_id IS NOT NULL;
