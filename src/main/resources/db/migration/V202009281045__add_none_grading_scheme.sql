--
-- Data for Name: grading_scheme; Type: TABLE DATA; Schema: public; Owner: Schoolaby
--
INSERT INTO public.grading_scheme (id, name, deleted, created_by, created_date, last_modified_by, last_modified_date, code)
VALUES (6, 'None', NULL, NULL, NULL, NULL, NULL, 'NONE');

--
-- Data for Name: grading_scheme_value; Type: TABLE DATA; Schema: public; Owner: Schoolaby
--
INSERT INTO public.grading_scheme_value (id, grade, percentage_range, deleted, grading_scheme_id, created_by,
                                                     created_date, last_modified_by, last_modified_date)
VALUES (16, '', '', NULL, 6, NULL, NULL, NULL, NULL);
