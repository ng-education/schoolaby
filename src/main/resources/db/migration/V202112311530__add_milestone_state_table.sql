CREATE TABLE public.milestone_state
(
    id             BIGSERIAL NOT NULL PRIMARY KEY,
    milestone_id   BIGINT NOT NULL REFERENCES public.milestone,
    user_id        BIGINT NOT NULL REFERENCES public.jhi_user,
    state          TEXT   NOT NULL,
    authority_name TEXT REFERENCES public.jhi_authority
);

CREATE SEQUENCE public.milestone_state_seq OWNED BY public.milestone_state.id;
