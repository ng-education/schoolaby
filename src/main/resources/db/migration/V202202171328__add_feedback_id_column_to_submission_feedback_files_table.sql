ALTER TABLE public.submission_feedback_files ADD COLUMN feedback_id BIGINT REFERENCES submission_feedback;
ALTER TABLE public.submission_feedback_files DROP CONSTRAINT submission_feedback_files_pkey, ALTER COLUMN submission_id DROP NOT NULL;

UPDATE public.submission_feedback_files sff SET feedback_id = (SELECT id FROM public.submission_feedback sf WHERE sf.submission_id = sff.submission_id LIMIT 1);

ALTER TABLE public.submission_feedback_files ADD PRIMARY KEY (feedback_id, feedback_file_id);
