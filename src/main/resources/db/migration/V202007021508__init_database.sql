SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: assignment; Type: TABLE; Schema: public;
--

CREATE TABLE public.assignment
(
    id                 bigint NOT NULL,
    title              text   NOT NULL,
    description        text,
    assignment_type    text,
    deadline           timestamp without time zone,
    flexible_deadline  boolean,
    deleted            timestamp without time zone,
    grading_scheme_id  bigint,
    milestone_id       bigint,
    creator_id         bigint,
    created_by         text,
    created_date       timestamp without time zone,
    last_modified_by   text,
    last_modified_date timestamp without time zone
);

--
-- Name: assignment_educational_alignments; Type: TABLE; Schema: public;
--

CREATE TABLE public.assignment_educational_alignments
(
    educational_alignments_id bigint NOT NULL,
    assignment_id             bigint NOT NULL
);


--
-- Name: assignment_materials; Type: TABLE; Schema: public;
--

CREATE TABLE public.assignment_materials
(
    materials_id  bigint NOT NULL,
    assignment_id bigint NOT NULL
);

--
-- Name: assignment_students; Type: TABLE; Schema: public;
--

CREATE TABLE public.assignment_students
(
    students_id   bigint NOT NULL,
    assignment_id bigint NOT NULL
);

--
-- Name: educational_alignment; Type: TABLE; Schema: public;
--

CREATE TABLE public.educational_alignment
(
    id                    bigint NOT NULL,
    title                 text   NOT NULL,
    alignment_type        text   NOT NULL,
    educational_framework text   NOT NULL,
    target_name           text   NOT NULL,
    target_url            text,
    start_date            timestamp without time zone,
    end_date              timestamp without time zone,
    deleted               timestamp without time zone,
    created_by            text,
    created_date          timestamp without time zone,
    last_modified_by      text,
    last_modified_date    timestamp without time zone
);

--
-- Name: TABLE educational_alignment; Type: COMMENT; Schema: public;
--

COMMENT ON TABLE public.educational_alignment IS 'Idea taken from https:\nAbovementioned schema is not final, therefor this is subject to change';


--
-- Name: educational_alignment_children; Type: TABLE; Schema: public;
--

CREATE TABLE public.educational_alignment_children
(
    children_id              bigint NOT NULL,
    educational_alignment_id bigint NOT NULL
);

--
-- Name: grading_scheme; Type: TABLE; Schema: public;
--

CREATE TABLE public.grading_scheme
(
    id                 bigint NOT NULL,
    name               text   NOT NULL,
    deleted            timestamp without time zone,
    created_by         text,
    created_date       timestamp without time zone,
    last_modified_by   text,
    last_modified_date timestamp without time zone
);

--
-- Name: grading_scheme_value; Type: TABLE; Schema: public;
--

CREATE TABLE public.grading_scheme_value
(
    id                 bigint NOT NULL,
    grade              text   NOT NULL,
    percentage_range   text   NOT NULL,
    deleted            timestamp without time zone,
    grading_scheme_id  bigint,
    created_by         text,
    created_date       timestamp without time zone,
    last_modified_by   text,
    last_modified_date timestamp without time zone
);

--
-- Name: jhi_authority; Type: TABLE; Schema: public;
--

CREATE TABLE public.jhi_authority
(
    name text NOT NULL
);

--
-- Name: jhi_persistent_audit_event; Type: TABLE; Schema: public;
--

CREATE TABLE public.jhi_persistent_audit_event
(
    event_id   bigint NOT NULL,
    principal  text   NOT NULL,
    event_date timestamp without time zone,
    event_type text
);


--
-- Name: jhi_persistent_audit_evt_data; Type: TABLE; Schema: public;
--

CREATE TABLE public.jhi_persistent_audit_evt_data
(
    event_id bigint NOT NULL,
    name     text   NOT NULL,
    value    text
);

--
-- Name: jhi_user; Type: TABLE; Schema: public;
--

CREATE TABLE public.jhi_user
(
    id                 bigint  NOT NULL,
    login              text    NOT NULL,
    password_hash      text    NOT NULL,
    first_name         text,
    last_name          text,
    email              text,
    phone_number       text,
    personal_code      text,
    image_url          text,
    activated          boolean NOT NULL,
    lang_key           text,
    activation_key     text,
    reset_key          text,
    created_by         text    NOT NULL,
    created_date       timestamp without time zone,
    reset_date         timestamp without time zone,
    last_modified_by   text,
    last_modified_date timestamp without time zone,
    deleted            timestamp without time zone
);


--
-- Name: jhi_user_authority; Type: TABLE; Schema: public;
--

CREATE TABLE public.jhi_user_authority
(
    user_id        bigint NOT NULL,
    authority_name text   NOT NULL
);

--
-- Name: journey; Type: TABLE; Schema: public;
--

CREATE TABLE public.journey
(
    id                   bigint                      NOT NULL,
    title                text                        NOT NULL,
    description          text,
    video_conference_url text,
    sign_up_code         text                        NOT NULL,
    start_date           timestamp without time zone NOT NULL,
    end_date             timestamp without time zone,
    deleted              timestamp without time zone,
    creator_id           bigint,
    created_by           text,
    created_date         timestamp without time zone,
    last_modified_by     text,
    last_modified_date   timestamp without time zone
);


--
-- Name: journey_educational_alignments; Type: TABLE; Schema: public;
--

CREATE TABLE public.journey_educational_alignments
(
    educational_alignments_id bigint NOT NULL,
    journey_id                bigint NOT NULL
);


--
-- Name: journey_people; Type: TABLE; Schema: public;
--

CREATE TABLE public.journey_people
(
    people_id  bigint NOT NULL,
    journey_id bigint NOT NULL
);


--
-- Name: material; Type: TABLE; Schema: public;
--

CREATE TABLE public.material
(
    id                 bigint NOT NULL,
    title              text   NOT NULL,
    type               text,
    description        text,
    external_id        text,
    url                text   NOT NULL,
    deleted            timestamp without time zone,
    created_by         text,
    created_date       timestamp without time zone,
    last_modified_by   text,
    last_modified_date timestamp without time zone
);


--
-- Name: material_educational_alignments; Type: TABLE; Schema: public;
--

CREATE TABLE public.material_educational_alignments
(
    educational_alignments_id bigint NOT NULL,
    material_id               bigint NOT NULL
);


--
-- Name: message; Type: TABLE; Schema: public;
--

CREATE TABLE public.message
(
    id                 bigint NOT NULL,
    value              text   NOT NULL,
    deleted            timestamp without time zone,
    creator_id         bigint,
    recipient_id       bigint,
    submission_id      bigint,
    step_id            bigint,
    journey_id         bigint,
    created_by         text,
    created_date       timestamp without time zone,
    last_modified_by   text,
    last_modified_date timestamp without time zone
);


--
-- Name: milestone; Type: TABLE; Schema: public;
--

CREATE TABLE public.milestone
(
    id                 bigint NOT NULL,
    title              text   NOT NULL,
    description        text,
    end_date           timestamp without time zone,
    deleted            timestamp without time zone,
    journey_id         bigint,
    creator_id         bigint,
    created_by         text,
    created_date       timestamp without time zone,
    last_modified_by   text,
    last_modified_date timestamp without time zone
);


--
-- Name: milestone_educational_alignments; Type: TABLE; Schema: public;
--

CREATE TABLE public.milestone_educational_alignments
(
    educational_alignments_id bigint NOT NULL,
    milestone_id              bigint NOT NULL
);


--
-- Name: milestone_materials; Type: TABLE; Schema: public;
--

CREATE TABLE public.milestone_materials
(
    materials_id bigint NOT NULL,
    milestone_id bigint NOT NULL
);


--

CREATE TABLE public.person_role
(
    id                 bigint NOT NULL,
    marker             text,
    active             boolean,
    start_date         timestamp without time zone,
    end_date           timestamp without time zone,
    provider_ehis_id   text,
    provider_reg_nr    text,
    provider_name      text,
    grade              text,
    parallel           text,
    deleted            timestamp without time zone,
    person_id          bigint,
    created_by         text,
    created_date       timestamp without time zone,
    last_modified_by   text,
    last_modified_date timestamp without time zone
);

--
-- Name: sequence_generator; Type: SEQUENCE; Schema: public;
--

CREATE SEQUENCE public.sequence_generator
    START WITH 1050
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: submission; Type: TABLE; Schema: public;
--

CREATE TABLE public.submission
(
    id                 bigint NOT NULL,
    value              text   NOT NULL,
    grade              text,
    grade_result       text,
    status             text,
    deleted            timestamp without time zone,
    assignment_id      bigint,
    created_by         text,
    created_date       timestamp without time zone,
    last_modified_by   text,
    last_modified_date timestamp without time zone
);

--
-- Name: COLUMN submission.grade_result; Type: COMMENT; Schema: public;
--

COMMENT ON COLUMN public.submission.grade_result IS 'For example how many points a student got';


--
-- Name: submission_authors; Type: TABLE; Schema: public;
--

CREATE TABLE public.submission_authors
(
    authors_id    bigint NOT NULL,
    submission_id bigint NOT NULL
);

--
-- Name: uploaded_file; Type: TABLE; Schema: public;
--

CREATE TABLE public.uploaded_file
(
    id                 bigint NOT NULL,
    name               text,
    type               text,
    extension          text,
    upload_url         text,
    deleted            timestamp without time zone,
    submission_id      bigint,
    created_by         text,
    created_date       timestamp without time zone,
    last_modified_by   text,
    last_modified_date timestamp without time zone
);

--
-- Data for Name: jhi_authority; Type: TABLE DATA; Schema: public;
--
INSERT INTO public.jhi_authority (name)
VALUES ('ROLE_ADMIN');
INSERT INTO public.jhi_authority (name)
VALUES ('ROLE_USER');
INSERT INTO public.jhi_authority (name)
VALUES ('ROLE_STUDENT');
INSERT INTO public.jhi_authority (name)
VALUES ('ROLE_TEACHER');


SELECT pg_catalog.setval('public.sequence_generator', 1050, true);


--
-- Name: assignment_educational_alignments assignment_educational_alignments_pkey; Type: CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.assignment_educational_alignments
    ADD CONSTRAINT assignment_educational_alignments_pkey PRIMARY KEY (assignment_id, educational_alignments_id);


--
-- Name: assignment_materials assignment_materials_pkey; Type: CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.assignment_materials
    ADD CONSTRAINT assignment_materials_pkey PRIMARY KEY (assignment_id, materials_id);


--
-- Name: assignment assignment_pkey; Type: CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.assignment
    ADD CONSTRAINT assignment_pkey PRIMARY KEY (id);


--
-- Name: assignment_students assignment_students_pkey; Type: CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.assignment_students
    ADD CONSTRAINT assignment_students_pkey PRIMARY KEY (assignment_id, students_id);


--
-- Name: educational_alignment_children educational_alignment_children_pkey; Type: CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.educational_alignment_children
    ADD CONSTRAINT educational_alignment_children_pkey PRIMARY KEY (educational_alignment_id, children_id);


--
-- Name: educational_alignment educational_alignment_pkey; Type: CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.educational_alignment
    ADD CONSTRAINT educational_alignment_pkey PRIMARY KEY (id);

--
-- Name: grading_scheme grading_scheme_pkey; Type: CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.grading_scheme
    ADD CONSTRAINT grading_scheme_pkey PRIMARY KEY (id);


--
-- Name: grading_scheme_value grading_scheme_value_pkey; Type: CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.grading_scheme_value
    ADD CONSTRAINT grading_scheme_value_pkey PRIMARY KEY (id);


--
-- Name: jhi_authority jhi_authority_pkey; Type: CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.jhi_authority
    ADD CONSTRAINT jhi_authority_pkey PRIMARY KEY (name);


--
-- Name: jhi_persistent_audit_event jhi_persistent_audit_event_pkey; Type: CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.jhi_persistent_audit_event
    ADD CONSTRAINT jhi_persistent_audit_event_pkey PRIMARY KEY (event_id);


--
-- Name: jhi_persistent_audit_evt_data jhi_persistent_audit_evt_data_pkey; Type: CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.jhi_persistent_audit_evt_data
    ADD CONSTRAINT jhi_persistent_audit_evt_data_pkey PRIMARY KEY (event_id, name);


--
-- Name: jhi_user_authority jhi_user_authority_pkey; Type: CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.jhi_user_authority
    ADD CONSTRAINT jhi_user_authority_pkey PRIMARY KEY (user_id, authority_name);


--
-- Name: jhi_user jhi_user_pkey; Type: CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.jhi_user
    ADD CONSTRAINT jhi_user_pkey PRIMARY KEY (id);


--
-- Name: journey_educational_alignments journey_educational_alignments_pkey; Type: CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.journey_educational_alignments
    ADD CONSTRAINT journey_educational_alignments_pkey PRIMARY KEY (journey_id, educational_alignments_id);


--
-- Name: journey_people journey_people_pkey; Type: CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.journey_people
    ADD CONSTRAINT journey_people_pkey PRIMARY KEY (journey_id, people_id);


--
-- Name: journey journey_pkey; Type: CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.journey
    ADD CONSTRAINT journey_pkey PRIMARY KEY (id);


--
-- Name: material_educational_alignments material_educational_alignments_pkey; Type: CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.material_educational_alignments
    ADD CONSTRAINT material_educational_alignments_pkey PRIMARY KEY (material_id, educational_alignments_id);


--
-- Name: material material_pkey; Type: CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.material
    ADD CONSTRAINT material_pkey PRIMARY KEY (id);


--
-- Name: message message_pkey; Type: CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.message
    ADD CONSTRAINT message_pkey PRIMARY KEY (id);


--
-- Name: milestone_educational_alignments milestone_educational_alignments_pkey; Type: CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.milestone_educational_alignments
    ADD CONSTRAINT milestone_educational_alignments_pkey PRIMARY KEY (milestone_id, educational_alignments_id);


--
-- Name: milestone_materials milestone_materials_pkey; Type: CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.milestone_materials
    ADD CONSTRAINT milestone_materials_pkey PRIMARY KEY (milestone_id, materials_id);


--
-- Name: milestone milestone_pkey; Type: CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.milestone
    ADD CONSTRAINT milestone_pkey PRIMARY KEY (id);

--
-- Name: person_role person_role_pkey; Type: CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.person_role
    ADD CONSTRAINT person_role_pkey PRIMARY KEY (id);


--
-- Name: submission_authors submission_authors_pkey; Type: CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.submission_authors
    ADD CONSTRAINT submission_authors_pkey PRIMARY KEY (submission_id, authors_id);


--
-- Name: submission submission_pkey; Type: CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.submission
    ADD CONSTRAINT submission_pkey PRIMARY KEY (id);


--
-- Name: uploaded_file uploaded_file_pkey; Type: CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.uploaded_file
    ADD CONSTRAINT uploaded_file_pkey PRIMARY KEY (id);


--
-- Name: jhi_user ux_user_email; Type: CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.jhi_user
    ADD CONSTRAINT ux_user_email UNIQUE (email);


--
-- Name: jhi_user ux_user_login; Type: CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.jhi_user
    ADD CONSTRAINT ux_user_login UNIQUE (login);

--
-- Name: idx_persistent_audit_event; Type: INDEX; Schema: public;
--

CREATE INDEX idx_persistent_audit_event ON public.jhi_persistent_audit_event USING btree (principal, event_date);


--
-- Name: idx_persistent_audit_evt_data; Type: INDEX; Schema: public;
--

CREATE INDEX idx_persistent_audit_evt_data ON public.jhi_persistent_audit_evt_data USING btree (event_id);


--
-- Name: assignment fk_assignment_creator_id; Type: FK CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.assignment
    ADD CONSTRAINT fk_assignment_creator_id FOREIGN KEY (creator_id) REFERENCES public.jhi_user (id);


--
-- Name: assignment_educational_alignments fk_assignment_educational_alignments_assignment_id; Type: FK CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.assignment_educational_alignments
    ADD CONSTRAINT fk_assignment_educational_alignments_assignment_id FOREIGN KEY (assignment_id) REFERENCES public.assignment (id);


--
-- Name: assignment_educational_alignments fk_assignment_educational_alignments_educational_alignments_id; Type: FK CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.assignment_educational_alignments
    ADD CONSTRAINT fk_assignment_educational_alignments_educational_alignments_id FOREIGN KEY (educational_alignments_id) REFERENCES public.educational_alignment (id);


--
-- Name: assignment fk_assignment_grading_scheme_id; Type: FK CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.assignment
    ADD CONSTRAINT fk_assignment_grading_scheme_id FOREIGN KEY (grading_scheme_id) REFERENCES public.grading_scheme (id);


--
-- Name: assignment_materials fk_assignment_materials_assignment_id; Type: FK CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.assignment_materials
    ADD CONSTRAINT fk_assignment_materials_assignment_id FOREIGN KEY (assignment_id) REFERENCES public.assignment (id);


--
-- Name: assignment_materials fk_assignment_materials_materials_id; Type: FK CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.assignment_materials
    ADD CONSTRAINT fk_assignment_materials_materials_id FOREIGN KEY (materials_id) REFERENCES public.material (id);


--
-- Name: assignment fk_assignment_milestone_id; Type: FK CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.assignment
    ADD CONSTRAINT fk_assignment_milestone_id FOREIGN KEY (milestone_id) REFERENCES public.milestone (id);


--
-- Name: assignment_students fk_assignment_students_assignment_id; Type: FK CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.assignment_students
    ADD CONSTRAINT fk_assignment_students_assignment_id FOREIGN KEY (assignment_id) REFERENCES public.assignment (id);


--
-- Name: assignment_students fk_assignment_students_students_id; Type: FK CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.assignment_students
    ADD CONSTRAINT fk_assignment_students_students_id FOREIGN KEY (students_id) REFERENCES public.jhi_user (id);


--
-- Name: jhi_user_authority fk_authority_name; Type: FK CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.jhi_user_authority
    ADD CONSTRAINT fk_authority_name FOREIGN KEY (authority_name) REFERENCES public.jhi_authority (name);


--
-- Name: educational_alignment_children fk_educational_alignment_children_children_id; Type: FK CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.educational_alignment_children
    ADD CONSTRAINT fk_educational_alignment_children_children_id FOREIGN KEY (children_id) REFERENCES public.educational_alignment (id);


--
-- Name: educational_alignment_children fk_educational_alignment_children_educational_alignment_id; Type: FK CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.educational_alignment_children
    ADD CONSTRAINT fk_educational_alignment_children_educational_alignment_id FOREIGN KEY (educational_alignment_id) REFERENCES public.educational_alignment (id);


--
-- Name: jhi_persistent_audit_evt_data fk_evt_pers_audit_evt_data; Type: FK CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.jhi_persistent_audit_evt_data
    ADD CONSTRAINT fk_evt_pers_audit_evt_data FOREIGN KEY (event_id) REFERENCES public.jhi_persistent_audit_event (event_id);


--
-- Name: grading_scheme_value fk_grading_scheme_value_grading_scheme_id; Type: FK CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.grading_scheme_value
    ADD CONSTRAINT fk_grading_scheme_value_grading_scheme_id FOREIGN KEY (grading_scheme_id) REFERENCES public.grading_scheme (id);


--
-- Name: journey fk_journey_creator_id; Type: FK CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.journey
    ADD CONSTRAINT fk_journey_creator_id FOREIGN KEY (creator_id) REFERENCES public.jhi_user (id);


--
-- Name: journey_educational_alignments fk_journey_educational_alignments_educational_alignments_id; Type: FK CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.journey_educational_alignments
    ADD CONSTRAINT fk_journey_educational_alignments_educational_alignments_id FOREIGN KEY (educational_alignments_id) REFERENCES public.educational_alignment (id);


--
-- Name: journey_educational_alignments fk_journey_educational_alignments_journey_id; Type: FK CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.journey_educational_alignments
    ADD CONSTRAINT fk_journey_educational_alignments_journey_id FOREIGN KEY (journey_id) REFERENCES public.journey (id);


--
-- Name: journey_people fk_journey_people_journey_id; Type: FK CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.journey_people
    ADD CONSTRAINT fk_journey_people_journey_id FOREIGN KEY (journey_id) REFERENCES public.journey (id);


--
-- Name: journey_people fk_journey_people_people_id; Type: FK CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.journey_people
    ADD CONSTRAINT fk_journey_people_people_id FOREIGN KEY (people_id) REFERENCES public.jhi_user (id);


--
-- Name: material_educational_alignments fk_material_educational_alignments_educational_alignments_id; Type: FK CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.material_educational_alignments
    ADD CONSTRAINT fk_material_educational_alignments_educational_alignments_id FOREIGN KEY (educational_alignments_id) REFERENCES public.educational_alignment (id);


--
-- Name: material_educational_alignments fk_material_educational_alignments_material_id; Type: FK CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.material_educational_alignments
    ADD CONSTRAINT fk_material_educational_alignments_material_id FOREIGN KEY (material_id) REFERENCES public.material (id);


--
-- Name: message fk_message_creator_id; Type: FK CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.message
    ADD CONSTRAINT fk_message_creator_id FOREIGN KEY (creator_id) REFERENCES public.jhi_user (id);


--
-- Name: message fk_message_journey_id; Type: FK CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.message
    ADD CONSTRAINT fk_message_journey_id FOREIGN KEY (journey_id) REFERENCES public.journey (id);


--
-- Name: message fk_message_recipient_id; Type: FK CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.message
    ADD CONSTRAINT fk_message_recipient_id FOREIGN KEY (recipient_id) REFERENCES public.jhi_user (id);


--
-- Name: message fk_message_step_id; Type: FK CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.message
    ADD CONSTRAINT fk_message_step_id FOREIGN KEY (step_id) REFERENCES public.milestone (id);


--
-- Name: message fk_message_submission_id; Type: FK CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.message
    ADD CONSTRAINT fk_message_submission_id FOREIGN KEY (submission_id) REFERENCES public.submission (id);


--
-- Name: milestone fk_milestone_creator_id; Type: FK CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.milestone
    ADD CONSTRAINT fk_milestone_creator_id FOREIGN KEY (creator_id) REFERENCES public.jhi_user (id);


--
-- Name: milestone_educational_alignments fk_milestone_educational_alignments_educational_alignments_id; Type: FK CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.milestone_educational_alignments
    ADD CONSTRAINT fk_milestone_educational_alignments_educational_alignments_id FOREIGN KEY (educational_alignments_id) REFERENCES public.educational_alignment (id);


--
-- Name: milestone_educational_alignments fk_milestone_educational_alignments_milestone_id; Type: FK CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.milestone_educational_alignments
    ADD CONSTRAINT fk_milestone_educational_alignments_milestone_id FOREIGN KEY (milestone_id) REFERENCES public.milestone (id);


--
-- Name: milestone fk_milestone_journey_id; Type: FK CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.milestone
    ADD CONSTRAINT fk_milestone_journey_id FOREIGN KEY (journey_id) REFERENCES public.journey (id);


--
-- Name: milestone_materials fk_milestone_materials_materials_id; Type: FK CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.milestone_materials
    ADD CONSTRAINT fk_milestone_materials_materials_id FOREIGN KEY (materials_id) REFERENCES public.material (id);


--
-- Name: milestone_materials fk_milestone_materials_milestone_id; Type: FK CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.milestone_materials
    ADD CONSTRAINT fk_milestone_materials_milestone_id FOREIGN KEY (milestone_id) REFERENCES public.milestone (id);


--
-- Name: person_role fk_person_role_person_id; Type: FK CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.person_role
    ADD CONSTRAINT fk_person_role_person_id FOREIGN KEY (person_id) REFERENCES public.jhi_user (id);


--
-- Name: submission fk_submission_assignment_id; Type: FK CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.submission
    ADD CONSTRAINT fk_submission_assignment_id FOREIGN KEY (assignment_id) REFERENCES public.assignment (id);


--
-- Name: submission_authors fk_submission_authors_authors_id; Type: FK CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.submission_authors
    ADD CONSTRAINT fk_submission_authors_authors_id FOREIGN KEY (authors_id) REFERENCES public.jhi_user (id);


--
-- Name: submission_authors fk_submission_authors_submission_id; Type: FK CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.submission_authors
    ADD CONSTRAINT fk_submission_authors_submission_id FOREIGN KEY (submission_id) REFERENCES public.submission (id);


--
-- Name: uploaded_file fk_uploaded_file_submission_id; Type: FK CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.uploaded_file
    ADD CONSTRAINT fk_uploaded_file_submission_id FOREIGN KEY (submission_id) REFERENCES public.submission (id);


--
-- Name: jhi_user_authority fk_user_id; Type: FK CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.jhi_user_authority
    ADD CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES public.jhi_user (id);


--
-- PostgreSQL database dump complete
--

