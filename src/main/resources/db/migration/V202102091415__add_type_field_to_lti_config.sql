ALTER TABLE public.lti_config
ADD COLUMN type TEXT;

CREATE TABLE public.lti_config_type
(
    name TEXT NOT NULL UNIQUE
);

INSERT INTO public.lti_config_type(name)
VALUES ('JOURNEY');
INSERT INTO public.lti_config_type(name)
VALUES ('PLATFORM');

UPDATE public.lti_config
SET type = 'JOURNEY';

ALTER TABLE ONLY public.lti_config
    ADD CONSTRAINT fk_lti_config_type FOREIGN KEY ("type") REFERENCES public.lti_config_type (name);
