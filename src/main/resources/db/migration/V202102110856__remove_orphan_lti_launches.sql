UPDATE lti_launch
SET deleted = lti_resource.deleted
FROM lti_resource
WHERE lti_launch.deleted IS NULL
  AND lti_resource.deleted IS NOT NULL
  AND lti_launch.lti_resource_id = lti_resource.id;
