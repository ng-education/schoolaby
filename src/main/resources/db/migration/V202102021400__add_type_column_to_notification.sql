CREATE TABLE public.notification_type
(
    name text NOT NULL UNIQUE
);

INSERT INTO public.notification_type(name)
VALUES ('GENERAL');
INSERT INTO public.notification_type(name)
VALUES ('MESSAGE');

ALTER TABLE public.notification
    ADD COLUMN "type" TEXT DEFAULT NULL;

ALTER TABLE ONLY public.notification
    ADD CONSTRAINT fk_notification_type FOREIGN KEY ("type") REFERENCES public.notification_type (name);

UPDATE public.notification
SET type = 'GENERAL';
