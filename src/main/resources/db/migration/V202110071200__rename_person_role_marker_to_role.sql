ALTER TABLE public.person_role
RENAME COLUMN marker to role;

UPDATE public.person_role
SET role = 'STUDENT'
WHERE role IN('student', 'Student');

UPDATE public.person_role
SET role = 'TEACHER'
WHERE role IN('faculty', 'Teacher');

DELETE FROM public.person_role
WHERE role NOT IN('STUDENT', 'TEACHER');
