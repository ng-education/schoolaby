ALTER TABLE public.assignment
RENAME COLUMN lti_link_id to lti_config_id;

ALTER TABLE ONLY public.assignment
    ADD CONSTRAINT fk_assignment_lti_config_id FOREIGN KEY (lti_config_id) REFERENCES public.lti_config (id);
