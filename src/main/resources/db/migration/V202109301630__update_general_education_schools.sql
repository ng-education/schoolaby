UPDATE public.school
SET name='Albu Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='214';
UPDATE public.school
SET name='Are Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='220';
UPDATE public.school
SET name='Halliste Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='330';
UPDATE public.school
SET name='Hargla Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='243';
UPDATE public.school
SET name='Muhu Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='544';
UPDATE public.school
SET name='Illuka Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='612';
UPDATE public.school
SET name='Juurikaru Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='347';
UPDATE public.school
SET name='Jõõpre Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='416';
UPDATE public.school
SET name='Kabala Lasteaed-Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='583';
UPDATE public.school
SET name='Kahtla Lasteaed-Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='543';
UPDATE public.school
SET name='Kalmetu Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='615';
UPDATE public.school
SET name='Kergu Lasteaed-Algkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='172';
UPDATE public.school
SET name='Kernu Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='17291';
UPDATE public.school
SET name='Kiltsi Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='655';
UPDATE public.school
SET name='Ruhnu Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='584';
UPDATE public.school
SET name='Kirivere Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='561';
UPDATE public.school
SET name='Lõpe Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='203';
UPDATE public.school
SET name='Mõniste Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='168';
UPDATE public.school
SET name='Vaimastvere Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='205';
UPDATE public.school
SET name='Kihnu Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='210';
UPDATE public.school
SET name='Ala Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='221';
UPDATE public.school
SET name='Emmaste Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='326';
UPDATE public.school
SET name='Leie Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='379';
UPDATE public.school
SET name='Jäneda Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='440';
UPDATE public.school
SET name='Metsküla Algkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='521';
UPDATE public.school
SET name='Tornimäe Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='538';
UPDATE public.school
SET name='Kaali Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='563';
UPDATE public.school
SET name='Pihtla Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='564';
UPDATE public.school
SET name='Vormsi Lasteaed-Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='565';
UPDATE public.school
SET name='Nõva Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='579';
UPDATE public.school
SET name='Kammeri Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='687';
UPDATE public.school
SET name='Urvaste Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='801';
UPDATE public.school
SET name='Kolga Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='632';
UPDATE public.school
SET name='Kauksi Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='409';
UPDATE public.school
SET name='Anna Haava nim Pala Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='428';
UPDATE public.school
SET name='Lümanda Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='639';
UPDATE public.school
SET name='Saaremaa Toetava Hariduse Keskus', last_modified_date=NOW()::timestamp
WHERE ehis_id='796';
UPDATE public.school
SET name='Varbola Lasteaed-Algkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='2133';
UPDATE public.school
SET name='Voore Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='233';
UPDATE public.school
SET name='Varbla Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='122';
UPDATE public.school
SET name='Kuldre Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='431';
UPDATE public.school
SET name='Kullamaa Keskkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='640';
UPDATE public.school
SET name='Hiiumaa Vabakool', last_modified_date=NOW()::timestamp
WHERE ehis_id='50716';
UPDATE public.school
SET name='Kõpu Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='425';
UPDATE public.school
SET name='Lahmuse Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='799';
UPDATE public.school
SET name='Vihasoo Lasteaed-Algkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='782';
UPDATE public.school
SET name='Laupa Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='629';
UPDATE public.school
SET name='J. V. Veski nimeline Maarja-Magdaleena Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='183';
UPDATE public.school
SET name='Martna Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='525';
UPDATE public.school
SET name='Metsapoole Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='128';
UPDATE public.school
SET name='Valgu Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='576';
UPDATE public.school
SET name='Maidla Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='237';
UPDATE public.school
SET name='Osula Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='171';
UPDATE public.school
SET name='Padise Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='533';
UPDATE public.school
SET name='Paistu Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='312';
UPDATE public.school
SET name='Palade Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='539';
UPDATE public.school
SET name='Palupera Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='283';
UPDATE public.school
SET name='Pikavere Mõisakool', last_modified_date=NOW()::timestamp
WHERE ehis_id='343';
UPDATE public.school
SET name='Prangli Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='556';
UPDATE public.school
SET name='Tilsi Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='315';
UPDATE public.school
SET name='Nurme Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='793';
UPDATE public.school
SET name='Raikküla Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='800';
UPDATE public.school
SET name='Riidaja Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='218';
UPDATE public.school
SET name='Risti Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='365';
UPDATE public.school
SET name='Ruila Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='358';
UPDATE public.school
SET name='Saarepeedi Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='637';
UPDATE public.school
SET name='Sauga Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='320';
UPDATE public.school
SET name='Sillaotsa Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='2227';
UPDATE public.school
SET name='Leiutajate Külakool', last_modified_date=NOW()::timestamp
WHERE ehis_id='5911';
UPDATE public.school
SET name='Unipiha Algkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='318';
UPDATE public.school
SET name='Surju Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='124';
UPDATE public.school
SET name='Pühajärve Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='659';
UPDATE public.school
SET name='Valguta Lasteaed-Algkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='776';
UPDATE public.school
SET name='Koonga Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='202';
UPDATE public.school
SET name='Vara Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='307';
UPDATE public.school
SET name='Vasalemma Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='180';
UPDATE public.school
SET name='Vasta Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='406';
UPDATE public.school
SET name='Viluste Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='535';
UPDATE public.school
SET name='Rocca al Mare Kooli Vodja Individuaalõppekeskus', last_modified_date=NOW()::timestamp
WHERE ehis_id='7259';
UPDATE public.school
SET name='Vohnja Lasteaed-Algkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='787';
UPDATE public.school
SET name='Ämmuste Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='792';
UPDATE public.school
SET name='Tallinna Mustamäe Humanitaargümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='504';
UPDATE public.school
SET name='Melliste Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='718';
UPDATE public.school
SET name='Mäepealse Erakool', last_modified_date=NOW()::timestamp
WHERE ehis_id='5971';
UPDATE public.school
SET name='Abja Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='437';
UPDATE public.school
SET name='Laeva Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='536';
UPDATE public.school
SET name='Käru Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='641';
UPDATE public.school
SET name='Jõgevamaa Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='3951';
UPDATE public.school
SET name='Paide Hammerbecki Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='188';
UPDATE public.school
SET name='Narva Kesklinna Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='293';
UPDATE public.school
SET name='Narva Paju Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='647';
UPDATE public.school
SET name='Kohtla-Järve Ahtme Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='303';
UPDATE public.school
SET name='Tartu Hansa Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='269';
UPDATE public.school
SET name='Tartu Descartes''i Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='272';
UPDATE public.school
SET name='Konguta Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='592';
UPDATE public.school
SET name='Pärnu Tammsaare Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='116';
UPDATE public.school
SET name='EBS Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='672';
UPDATE public.school
SET name='Kostivere Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='645';
UPDATE public.school
SET name='Gaia Kool (Tallinn)', last_modified_date=NOW()::timestamp
WHERE ehis_id='6071';
UPDATE public.school
SET name='Avatud Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='31651';
UPDATE public.school
SET name='Lohusuu Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='215';
UPDATE public.school
SET name='Tallinna Kadaka Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='516';
UPDATE public.school
SET name='Tallinna Arte Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='500';
UPDATE public.school
SET name='Tallinna Mustamäe Reaalgümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='505';
UPDATE public.school
SET name='Eurogümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='780';
UPDATE public.school
SET name='Keila Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='17891';
UPDATE public.school
SET name='Haapsalu Linna Algkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='387';
UPDATE public.school
SET name='Tallinna Heleni Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='476';
UPDATE public.school
SET name='Ehte Humanitaargümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='466';
UPDATE public.school
SET name='Rannu Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='280';
UPDATE public.school
SET name='Tallinna Inglise Kolledž', last_modified_date=NOW()::timestamp
WHERE ehis_id='487';
UPDATE public.school
SET name='Tallinna Reaalkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='493';
UPDATE public.school
SET name='Tallinna Kesklinna Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='4571';
UPDATE public.school
SET name='Läänemaa Ühisgümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='3551';
UPDATE public.school
SET name='Türi Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='2310';
UPDATE public.school
SET name='Tallinna Kesklinna Vene Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='491';
UPDATE public.school
SET name='Väike Werrone Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='32171';
UPDATE public.school
SET name='Tartu Waldorfgümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='771';
UPDATE public.school
SET name='Rakke Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='328';
UPDATE public.school
SET name='Kuressaare Täiskasvanute Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='604';
UPDATE public.school
SET name='Sillamäe Kannuka Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='369';
UPDATE public.school
SET name='Kadrioru Saksa Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='482';
UPDATE public.school
SET name='Randvere Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='5951';
UPDATE public.school
SET name='Taebla Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='355';
UPDATE public.school
SET name='Rõuge Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='179';
UPDATE public.school
SET name='Oru Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='361';
UPDATE public.school
SET name='Salu Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='1';
UPDATE public.school
SET name='Tabivere Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='182';
UPDATE public.school
SET name='Kõrveküla Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='338';
UPDATE public.school
SET name='Kuressaare Hariduse Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='605';
UPDATE public.school
SET name='Viljandi Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='2362';
UPDATE public.school
SET name='Tallinna Prantsuse Lütseum', last_modified_date=NOW()::timestamp
WHERE ehis_id='492';
UPDATE public.school
SET name='Narva Eesti Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='603';
UPDATE public.school
SET name='Vana-Vigala Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='599';
UPDATE public.school
SET name='Jõhvi Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='192';
UPDATE public.school
SET name='Jõhvi Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='6751';
UPDATE public.school
SET name='Merivälja Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='478';
UPDATE public.school
SET name='Vääna-Jõesuu Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='4811';
UPDATE public.school
SET name='Tartu Hiie Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='805';
UPDATE public.school
SET name='Käina Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='354';
UPDATE public.school
SET name='Nõmme Erakool', last_modified_date=NOW()::timestamp
WHERE ehis_id='723';
UPDATE public.school
SET name='Narva Õigeusu Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='722';
UPDATE public.school
SET name='Porkuni Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='815';
UPDATE public.school
SET name='Lihula Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='402';
UPDATE public.school
SET name='Lasnamäe Vene Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='453';
UPDATE public.school
SET name='Narva-Jõesuu Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='234';
UPDATE public.school
SET name='Miina Härma Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='259';
UPDATE public.school
SET name='Harkujärve Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='676';
UPDATE public.school
SET name='Tartu Katoliku Hariduskeskus', last_modified_date=NOW()::timestamp
WHERE ehis_id='769';
UPDATE public.school
SET name='Tartu Rahvusvaheline Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='1045';
UPDATE public.school
SET name='Tallinna Saksa Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='511';
UPDATE public.school
SET name='Tallinna 53. Keskkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='501';
UPDATE public.school
SET name='Eesti Rahvusvaheline Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='1032';
UPDATE public.school
SET name='Valga Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='224';
UPDATE public.school
SET name='Ferdinand von Wrangell'' i nimeline Roela Lasteaed-Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='299';
UPDATE public.school
SET name='Puiga Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='553';
UPDATE public.school
SET name='Tallinna Järveotsa Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='513';
UPDATE public.school
SET name='Siimusti Lasteaed-Algkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='664';
UPDATE public.school
SET name='Nõo Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='287';
UPDATE public.school
SET name='Nõo Reaalgümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='807';
UPDATE public.school
SET name='Tõstamaa Keskkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='396';
UPDATE public.school
SET name='Lindi Lasteaed-Algkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='417';
UPDATE public.school
SET name='Narva Keeltelütseum', last_modified_date=NOW()::timestamp
WHERE ehis_id='296';
UPDATE public.school
SET name='Karjamaa Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='469';
UPDATE public.school
SET name='Loova tuleviku kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='30171';
UPDATE public.school
SET name='Tallinna Juudi Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='489';
UPDATE public.school
SET name='Jaagu Lasteaed-Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='1386';
UPDATE public.school
SET name='Kunda Ühisgümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='700';
UPDATE public.school
SET name='Ääsmäe Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='380';
UPDATE public.school
SET name='Kohtla-Järve Järve Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='302';
UPDATE public.school
SET name='Tartu Annelinna Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='270';
UPDATE public.school
SET name='Tartu Kristjan Jaak Petersoni Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='5831';
UPDATE public.school
SET name='Tartu Kivilinna Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='273';
UPDATE public.school
SET name='Maarjamaa Hariduskolleegium', last_modified_date=NOW()::timestamp
WHERE ehis_id='811';
UPDATE public.school
SET name='Emili Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='17371';
UPDATE public.school
SET name='Tammiste Lasteaed-Algkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='51196';
UPDATE public.school
SET name='Sinimäe Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='438';
UPDATE public.school
SET name='Tamsalu Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='651';
UPDATE public.school
SET name='Tartu Luterlik Peetri Kool (Põlva)', last_modified_date=NOW()::timestamp
WHERE ehis_id='42011';
UPDATE public.school
SET name='Kambja Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='316';
UPDATE public.school
SET name='Põlva Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='16351';
UPDATE public.school
SET name='Ritsu Lasteaed-Algkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='740';
UPDATE public.school
SET name='Laekvere Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='345';
UPDATE public.school
SET name='Viljandi Kaare Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='227';
UPDATE public.school
SET name='Viljandi Täiskasvanute Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='231';
UPDATE public.school
SET name='Rapla Kesklinna Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='624';
UPDATE public.school
SET name='Peetri Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='2492';
UPDATE public.school
SET name='Vastse-Kuuste Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='337';
UPDATE public.school
SET name='Alu Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='642';
UPDATE public.school
SET name='Oru Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='360';
UPDATE public.school
SET name='Tallinna Mustamäe Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='503';
UPDATE public.school
SET name='Waldorfkool Läte', last_modified_date=NOW()::timestamp
WHERE ehis_id='1385';
UPDATE public.school
SET name='Tarvastu Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='384';
UPDATE public.school
SET name='Jakob Westholmi Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='481';
UPDATE public.school
SET name='Kiigemetsa Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='806';
UPDATE public.school
SET name='Imavere Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='222';
UPDATE public.school
SET name='Tallinna 32. Keskkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='498';
UPDATE public.school
SET name='Lüllemäe Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='177';
UPDATE public.school
SET name='Lüganuse Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='184';
UPDATE public.school
SET name='Laulasmaa Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='1049';
UPDATE public.school
SET name='Tallinna Humanitaargümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='486';
UPDATE public.school
SET name='Tarbja Lasteaed-Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='2318';
UPDATE public.school
SET name='Holstre Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='313';
UPDATE public.school
SET name='Antsla Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='335';
UPDATE public.school
SET name='Kuuste Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='317';
UPDATE public.school
SET name='Keeni Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='436';
UPDATE public.school
SET name='Carl Robert Jakobsoni nim Torma Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='594';
UPDATE public.school
SET name='Kääpa Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='130';
UPDATE public.school
SET name='Pärnjõe Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='346';
UPDATE public.school
SET name='Valjala Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='540';
UPDATE public.school
SET name='Püünsi Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='551';
UPDATE public.school
SET name='Ilmatsalu Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='430';
UPDATE public.school
SET name='Saverna Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='351';
UPDATE public.school
SET name='Raasiku Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='341';
UPDATE public.school
SET name='Varstu Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='173';
UPDATE public.school
SET name='Paide Hillar Hanssoo Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='189';
UPDATE public.school
SET name='Roosna-Alliku Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='216';
UPDATE public.school
SET name='Krootuse Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='291';
UPDATE public.school
SET name='Mustjala Lasteaed-Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='496';
UPDATE public.school
SET name='Turba Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='334';
UPDATE public.school
SET name='Kanepi Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='362';
UPDATE public.school
SET name='Väätsa Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='399';
UPDATE public.school
SET name='Juhan Liivi nim Alatskivi Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='252';
UPDATE public.school
SET name='August Kitzbergi nimeline Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='256';
UPDATE public.school
SET name='Kihelkonna Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='562';
UPDATE public.school
SET name='Kiili Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='595';
UPDATE public.school
SET name='Võsu Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='660';
UPDATE public.school
SET name='Hagudi Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='667';
UPDATE public.school
SET name='Kohila Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='695';
UPDATE public.school
SET name='Häädemeeste Keskkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='129';
UPDATE public.school
SET name='Pärnu Ühisgümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='119';
UPDATE public.school
SET name='Vändra Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='176';
UPDATE public.school
SET name='Lagedi Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='634';
UPDATE public.school
SET name='Aseri Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='242';
UPDATE public.school
SET name='Mehikoorma Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='717';
UPDATE public.school
SET name='Õru Lasteaed-Algkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='791';
UPDATE public.school
SET name='Oskar Lutsu Palamuse Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='196';
UPDATE public.school
SET name='Tallinna Vanalinna Täiskasvanute Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='765';
UPDATE public.school
SET name='Pärnu-Jaagupi Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='178';
UPDATE public.school
SET name='Puka Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='186';
UPDATE public.school
SET name='Kärla Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='507';
UPDATE public.school
SET name='Sõmeru Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='404';
UPDATE public.school
SET name='Ridala Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='568';
UPDATE public.school
SET name='Leisi Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='523';
UPDATE public.school
SET name='Räpina Ühisgümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='324';
UPDATE public.school
SET name='Mikitamäe Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='607';
UPDATE public.school
SET name='Kivi-Vigala Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='598';
UPDATE public.school
SET name='Kohtla-Nõmme Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='613';
UPDATE public.school
SET name='Mäetaguse Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='608';
UPDATE public.school
SET name='Tootsi Lasteaed-Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='125';
UPDATE public.school
SET name='Võru Kreutzwaldi Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='7131';
UPDATE public.school
SET name='Sadala Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='593';
UPDATE public.school
SET name='Rapla Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='32731';
UPDATE public.school
SET name='Tabasalu Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='414';
UPDATE public.school
SET name='Sindi Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='223';
UPDATE public.school
SET name='Otepää Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='658';
UPDATE public.school
SET name='Tallinna Kunstigümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='475';
UPDATE public.school
SET name='Alavere Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='609';
UPDATE public.school
SET name='Narva Vanalinna Riigikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='816';
UPDATE public.school
SET name='Tallinna Kuristiku Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='456';
UPDATE public.school
SET name='Tartu Kesklinna Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='275';
UPDATE public.school
SET name='Orissaare Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='541';
UPDATE public.school
SET name='Tallinna Lilleküla Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='443';
UPDATE public.school
SET name='Valga Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='322';
UPDATE public.school
SET name='Pärnu Kuninga Tänava Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='821';
UPDATE public.school
SET name='Laiuse Jaan Poska Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='204';
UPDATE public.school
SET name='Salme Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='554';
UPDATE public.school
SET name='Kurtna Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='705';
UPDATE public.school
SET name='Kõmsi Lasteaed-Algkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='706';
UPDATE public.school
SET name='Lauka Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='374';
UPDATE public.school
SET name='Pärnu Raeküla Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='117';
UPDATE public.school
SET name='Käo Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='2302';
UPDATE public.school
SET name='Simuna Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='385';
UPDATE public.school
SET name='Paide Täiskasvanute Keskkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='190';
UPDATE public.school
SET name='Jüri Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='636';
UPDATE public.school
SET name='Ardu Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='426';
UPDATE public.school
SET name='Kaiu Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='626';
UPDATE public.school
SET name='Muraste Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='7282';
UPDATE public.school
SET name='Tallinna Kivimäe Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='446';
UPDATE public.school
SET name='Sonda Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='419';
UPDATE public.school
SET name='Audru Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='415';
UPDATE public.school
SET name='Haapsalu Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='388';
UPDATE public.school
SET name='VIVERE KOOL', last_modified_date=NOW()::timestamp
WHERE ehis_id='42931';
UPDATE public.school
SET name='Võru Järve Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='526';
UPDATE public.school
SET name='Tallinna Südalinna Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='483';
UPDATE public.school
SET name='Mihkli Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='719';
UPDATE public.school
SET name='Põlva Roosi Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='309';
UPDATE public.school
SET name='Tartu Karlova Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='264';
UPDATE public.school
SET name='Tallinna Linnamäe Vene Lütseum', last_modified_date=NOW()::timestamp
WHERE ehis_id='458';
UPDATE public.school
SET name='Audentese Spordigümnaasiumi Otepää filiaal', last_modified_date=NOW()::timestamp
WHERE ehis_id='939';
UPDATE public.school
SET name='Uuemõisa Lasteaed-Algkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='566';
UPDATE public.school
SET name='Toila Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='175';
UPDATE public.school
SET name='Viljandi Vaba Waldorfkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='785';
UPDATE public.school
SET name='Pärnu Sütevaka Humanitaargümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='732';
UPDATE public.school
SET name='Risti Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='364';
UPDATE public.school
SET name='Palivere Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='356';
UPDATE public.school
SET name='Tallinna Mahtra Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='460';
UPDATE public.school
SET name='Pärnu Mai Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='5871';
UPDATE public.school
SET name='Kohtla-Järve Maleva Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='304';
UPDATE public.school
SET name='Erakool Garant', last_modified_date=NOW()::timestamp
WHERE ehis_id='2671';
UPDATE public.school
SET name='Tallinna Täiskasvanute Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='754';
UPDATE public.school
SET name='Pärnu Koidula Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='115';
UPDATE public.school
SET name='Pirita Majandusgümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='479';
UPDATE public.school
SET name='Pelgulinna Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='471';
UPDATE public.school
SET name='Hugo Treffneri Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='258';
UPDATE public.school
SET name='Aakre Lasteaed-Algkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='668';
UPDATE public.school
SET name='Koigi Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='435';
UPDATE public.school
SET name='Ruusa Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='742';
UPDATE public.school
SET name='Tartu Erakool (Elva linn)', last_modified_date=NOW()::timestamp
WHERE ehis_id='5271';
UPDATE public.school
SET name='Olustvere Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='245';
UPDATE public.school
SET name='Tartu Luterlik Peetri Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='4511';
UPDATE public.school
SET name='Jõhvi Vene Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='191';
UPDATE public.school
SET name='Peipsi Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='653';
UPDATE public.school
SET name='Neeme Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='1030';
UPDATE public.school
SET name='Pärnu Vanalinna Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='114';
UPDATE public.school
SET name='Nissi Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='333';
UPDATE public.school
SET name='Maardu Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='366';
UPDATE public.school
SET name='Kallavere Keskkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='367';
UPDATE public.school
SET name='Pärnu Täiskasvanute Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='120';
UPDATE public.school
SET name='Pärnu Vabakool', last_modified_date=NOW()::timestamp
WHERE ehis_id='733';
UPDATE public.school
SET name='Tsirguliina Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='185';
UPDATE public.school
SET name='Kuressaare Nooruse Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='704';
UPDATE public.school
SET name='Uhtna Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='382';
UPDATE public.school
SET name='Tapa Keelekümbluskool', last_modified_date=NOW()::timestamp
WHERE ehis_id='520';
UPDATE public.school
SET name='Tabasalu Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='50856';
UPDATE public.school
SET name='Tartu Tamme Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='262';
UPDATE public.school
SET name='Tartu Täiskasvanute Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='770';
UPDATE public.school
SET name='Saue Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='424';
UPDATE public.school
SET name='Järvakandi Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='571';
UPDATE public.school
SET name='Tallinna Kristiine Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='444';
UPDATE public.school
SET name='Näpi Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='813';
UPDATE public.school
SET name='Erakool Intellekt', last_modified_date=NOW()::timestamp
WHERE ehis_id='696';
UPDATE public.school
SET name='Viljandi Paalalinna Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='229';
UPDATE public.school
SET name='Tallinna Pae Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='461';
UPDATE public.school
SET name='Lasnamäe Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='452';
UPDATE public.school
SET name='Paikuse Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='167';
UPDATE public.school
SET name='Koeru Keskkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='694';
UPDATE public.school
SET name='Tallinna Mustjõe Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='514';
UPDATE public.school
SET name='Heimtali Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='236';
UPDATE public.school
SET name='Mooste Mõisakool', last_modified_date=NOW()::timestamp
WHERE ehis_id='408';
UPDATE public.school
SET name='Peetri Lasteaed-Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='2125';
UPDATE public.school
SET name='Tapa Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='519';
UPDATE public.school
SET name='Hummuli Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='169';
UPDATE public.school
SET name='Kuremaa Lasteaed-Algkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='702';
UPDATE public.school
SET name='Virtsu Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='2236';
UPDATE public.school
SET name='Kose-Uuemõisa Lasteaed-Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='699';
UPDATE public.school
SET name='Kiviõli Vene Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='306';
UPDATE public.school
SET name='Narva 6. Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='297';
UPDATE public.school
SET name='Narva Täiskasvanute Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='601';
UPDATE public.school
SET name='Tallinna Laagna Lasteaed-Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='756';
UPDATE public.school
SET name='Paldiski Vene Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='549';
UPDATE public.school
SET name='Meremäe Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='2239';
UPDATE public.school
SET name='Vastseliina Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='547';
UPDATE public.school
SET name='Aravete Keskkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='199';
UPDATE public.school
SET name='Tartu Variku Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='271';
UPDATE public.school
SET name='Põlva Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='14500';
UPDATE public.school
SET name='Jõgeva Täiskasvanute Keskkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='680';
UPDATE public.school
SET name='Väike-Maarja Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='656';
UPDATE public.school
SET name='Järva-Jaani Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='933';
UPDATE public.school
SET name='Kose Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='359';
UPDATE public.school
SET name='Värska Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='631';
UPDATE public.school
SET name='Rakvere Waldorfkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='2303';
UPDATE public.school
SET name='Kuressaare Vanalinna Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='606';
UPDATE public.school
SET name='Ambla-Aravete Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='200';
UPDATE public.school
SET name='Suuremõisa Lasteaed-Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='2497';
UPDATE public.school
SET name='Tõrva Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='238';
UPDATE public.school
SET name='Tartu Maarja Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='279';
UPDATE public.school
SET name='Rõngu Keskkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='284';
UPDATE public.school
SET name='Elva Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='288';
UPDATE public.school
SET name='Luunja Keskkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='207';
UPDATE public.school
SET name='Tartu Kroonuaia Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='276';
UPDATE public.school
SET name='Kohtla-Järve Tammiku Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='301';
UPDATE public.school
SET name='Aruküla Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='342';
UPDATE public.school
SET name='Narva Pähklimäe Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='602';
UPDATE public.school
SET name='Aegviidu Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='650';
UPDATE public.school
SET name='Kohtla-Järve Kesklinna Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='248';
UPDATE public.school
SET name='Kohtla-Järve Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='38091';
UPDATE public.school
SET name='Kohtla-Järve Slaavi Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='247';
UPDATE public.school
SET name='Tallinna Tõnismäe Reaalkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='494';
UPDATE public.school
SET name='Tallinna Ühisgümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='495';
UPDATE public.school
SET name='Mõisaküla Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='285';
UPDATE public.school
SET name='Tartu Raatuse Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='260';
UPDATE public.school
SET name='Pärnu Rääma Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='818';
UPDATE public.school
SET name='Paldiski Ühisgümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='4911';
UPDATE public.school
SET name='Haljala Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='393';
UPDATE public.school
SET name='Tudu Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='300';
UPDATE public.school
SET name='Kadrina Keskkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='286';
UPDATE public.school
SET name='Haabneeme Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='5931';
UPDATE public.school
SET name='Viimsi Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='550';
UPDATE public.school
SET name='Tallinna 21. Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='485';
UPDATE public.school
SET name='Tallinna Nõmme Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='448';
UPDATE public.school
SET name='Tallinna Nõmme Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='447';
UPDATE public.school
SET name='Järveküla Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='18631';
UPDATE public.school
SET name='Retla-Kabala Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='212';
UPDATE public.school
SET name='Misso Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='2306';
UPDATE public.school
SET name='Tahkuranna Lasteaed-Algkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='750';
UPDATE public.school
SET name='Viljandi Jakobsoni Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='228';
UPDATE public.school
SET name='Pärnu Waldorfkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='4831';
UPDATE public.school
SET name='Maardu Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='368';
UPDATE public.school
SET name='Ristiku Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='473';
UPDATE public.school
SET name='Kindluse Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='45896';
UPDATE public.school
SET name='Jõgeva Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='4431';
UPDATE public.school
SET name='Haanja Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='391';
UPDATE public.school
SET name='Pärnu Päikese Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='830';
UPDATE public.school
SET name='Lehtse Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='439';
UPDATE public.school
SET name='Loo Keskkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='643';
UPDATE public.school
SET name='Sakala Eragümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='2119';
UPDATE public.school
SET name='Kilingi-Nõmme Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='206';
UPDATE public.school
SET name='Võru Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='6571';
UPDATE public.school
SET name='Shanghai ja San-Francisco Püha Johannese nimeline Kool Tallinnas', last_modified_date=NOW()::timestamp
WHERE ehis_id='50797';
UPDATE public.school
SET name='Orava Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='652';
UPDATE public.school
SET name='Uulu Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='587';
UPDATE public.school
SET name='Kehra Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='392';
UPDATE public.school
SET name='Valtu Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='560';
UPDATE public.school
SET name='Kehtna Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='558';
UPDATE public.school
SET name='Kolkja Lasteaed-Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='348';
UPDATE public.school
SET name='Pärnu Kristlik Erakool', last_modified_date=NOW()::timestamp
WHERE ehis_id='50796';
UPDATE public.school
SET name='Gustav Adolfi Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='480';
UPDATE public.school
SET name='Haapsalu Viigi Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='814';
UPDATE public.school
SET name='Tallinna Rahvusvaheline Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='31411';
UPDATE public.school
SET name='Kool 21. sajandil', last_modified_date=NOW()::timestamp
WHERE ehis_id='497';
UPDATE public.school
SET name='Hilariuse Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='1040';
UPDATE public.school
SET name='Tallinna Tehnikagümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='512';
UPDATE public.school
SET name='Lilleoru Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='38272';
UPDATE public.school
SET name='Saku Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='524';
UPDATE public.school
SET name='Juuru Eduard Vilde Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='671';
UPDATE public.school
SET name='Eidapere Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='557';
UPDATE public.school
SET name='Aruküla Vaba Waldorfkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='670';
UPDATE public.school
SET name='Narva Soldino Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='649';
UPDATE public.school
SET name='Pärnu Ülejõe Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='118';
UPDATE public.school
SET name='Luce Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='31951';
UPDATE public.school
SET name='Suure-Jaani Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='241';
UPDATE public.school
SET name='Suure-Jaani Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='5972';
UPDATE public.school
SET name='Paide Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='33511';
UPDATE public.school
SET name='Loksa Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='573';
UPDATE public.school
SET name='Tartu Tamme Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='7281';
UPDATE public.school
SET name='Märjamaa Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='574';
UPDATE public.school
SET name='Viimsi Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='32231';
UPDATE public.school
SET name='Vinni-Pajusti Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='298';
UPDATE public.school
SET name='Puurmani Mõisakool', last_modified_date=NOW()::timestamp
WHERE ehis_id='627';
UPDATE public.school
SET name='Friedebert Tuglase nimeline Ahja Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='578';
UPDATE public.school
SET name='"Edu Valem" Erakool', last_modified_date=NOW()::timestamp
WHERE ehis_id='2309';
UPDATE public.school
SET name='Ülenurme Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='281';
UPDATE public.school
SET name='Iisaku Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='405';
UPDATE public.school
SET name='Mustvee Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='654';
UPDATE public.school
SET name='Rakvere Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='617';
UPDATE public.school
SET name='Võnnu Keskkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='353';
UPDATE public.school
SET name='Tallinna Euroopa Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='4491';
UPDATE public.school
SET name='Viiratsi Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='614';
UPDATE public.school
SET name='Türi Ühisgümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='2312';
UPDATE public.school
SET name='Tallinna Tondi Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='442';
UPDATE public.school
SET name='Audentese Erakool', last_modified_date=NOW()::timestamp
WHERE ehis_id='936';
UPDATE public.school
SET name='Audentese Spordigümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='2044';
UPDATE public.school
SET name='Audentese Rahvusvaheline Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='41691';
UPDATE public.school
SET name='Tallinna Balletikooli üldharidusklassid', last_modified_date=NOW()::timestamp
WHERE ehis_id='9';
UPDATE public.school
SET name='Tallinna Toomkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='2491';
UPDATE public.school
SET name='Kaarli Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='4611';
UPDATE public.school
SET name='Tallinna Vaba Waldorfkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='822';
UPDATE public.school
SET name='Kohtla-Järve Täiskasvanute Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='249';
UPDATE public.school
SET name='Kajamaa Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='1387';
UPDATE public.school
SET name='Tartu Forseliuse Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='265';
UPDATE public.school
SET name='Püha Johannese Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='4551';
UPDATE public.school
SET name='Tartu Erakool', last_modified_date=NOW()::timestamp
WHERE ehis_id='1054';
UPDATE public.school
SET name='Rakvere Eragümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='836';
UPDATE public.school
SET name='Kohila Mõisakool', last_modified_date=NOW()::timestamp
WHERE ehis_id='17991';
UPDATE public.school
SET name='Viljandi Kesklinna Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='232';
UPDATE public.school
SET name='Valga Kaugõppegümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='616';
UPDATE public.school
SET name='Kärdla Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='323';
UPDATE public.school
SET name='Hiiumaa Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='10811';
UPDATE public.school
SET name='Tartu Aleksander Puškini Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='263';
UPDATE public.school
SET name='Tallinna Muusikakeskkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='55';
UPDATE public.school
SET name='Tartu Emajõe Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='808';
UPDATE public.school
SET name='Tallinna Rahumäe Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='450';
UPDATE public.school
SET name='Rakvere Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='622';
UPDATE public.school
SET name='Võru Kesklinna Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='531';
UPDATE public.school
SET name='Valga Priimetsa Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='225';
UPDATE public.school
SET name='Tallinna Konstantin Pätsi Vabaõhukool', last_modified_date=NOW()::timestamp
WHERE ehis_id='803';
UPDATE public.school
SET name='Rocca al Mare Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='741';
UPDATE public.school
SET name='Türi Kevade Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='253';
UPDATE public.school
SET name='Kalamaja Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='467';
UPDATE public.school
SET name='Lasila Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='373';
UPDATE public.school
SET name='Sillamäe Eesti Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='371';
UPDATE public.school
SET name='Sillamäe Vanalinna Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='370';
UPDATE public.school
SET name='Kuusalu Keskkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='569';
UPDATE public.school
SET name='Vaida Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='633';
UPDATE public.school
SET name='Tartu Herbert Masingu Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='278';
UPDATE public.school
SET name='Tartu Jaan Poska Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='2293';
UPDATE public.school
SET name='Tartu Mart Reiniku Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='267';
UPDATE public.school
SET name='Tallinna Soome Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='2153';
UPDATE public.school
SET name='Narva Kreenholmi Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='292';
UPDATE public.school
SET name='Tartu Veeriku Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='274';
UPDATE public.school
SET name='Veltsi Lasteaed-Algkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='779';
UPDATE public.school
SET name='Vanalinna Hariduskolleegium', last_modified_date=NOW()::timestamp
WHERE ehis_id='764';
UPDATE public.school
SET name='Püha Miikaeli Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='1042';
UPDATE public.school
SET name='Võhma Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='570';
UPDATE public.school
SET name='Põltsamaa Ühisgümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='193';
UPDATE public.school
SET name='Laagri Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='1044';
UPDATE public.school
SET name='Tallinna Pääsküla Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='449';
UPDATE public.school
SET name='Tallinna Laagna Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='457';
UPDATE public.school
SET name='Kolga-Jaani Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='45936';
UPDATE public.school
SET name='Rapla Vesiroosi Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='600';
UPDATE public.school
SET name='Puhja Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='555';
UPDATE public.school
SET name='Valga Jaanikese Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='809';
UPDATE public.school
SET name='Sillamäe Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='2384';
UPDATE public.school
SET name='Kiviõli I Keskkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='226';
UPDATE public.school
SET name='Tori Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='434';
UPDATE public.school
SET name='Tallinna Läänemere Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='459';
UPDATE public.school
SET name='Aste Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='509';
UPDATE public.school
SET name='Avinurme Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='597';
UPDATE public.school
SET name='Rakvere Reaalgümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='619';
UPDATE public.school
SET name='Parksepa Keskkool', last_modified_date=NOW()::timestamp
WHERE ehis_id='552';
UPDATE public.school
SET name='Saaremaa Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='46080';
UPDATE public.school
SET name='Johannese Kool ja Lasteaed Rosmal', last_modified_date=NOW()::timestamp
WHERE ehis_id='679';
UPDATE public.school
SET name='Vääna Mõisakool', last_modified_date=NOW()::timestamp
WHERE ehis_id='790';
UPDATE public.school
SET name='Haabersti Vene Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='751';
UPDATE public.school
SET name='Tallinna Õismäe Vene Lütseum', last_modified_date=NOW()::timestamp
WHERE ehis_id='518';
UPDATE public.school
SET name='Tallinna Õismäe Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='517';
UPDATE public.school
SET name='Lähte Ühisgümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='372';
UPDATE public.school
SET name='Ahtme Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='810';
UPDATE public.school
SET name='Tartu Kristlik Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='768';
UPDATE public.school
SET name='Lasnamäe Põhikool', last_modified_date=NOW()::timestamp
WHERE ehis_id='490';
UPDATE public.school
SET name='Noarootsi Kool', last_modified_date=NOW()::timestamp
WHERE ehis_id='401';
UPDATE public.school
SET name='Noarootsi Gümnaasium', last_modified_date=NOW()::timestamp
WHERE ehis_id='804';
