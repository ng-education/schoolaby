package com.schoolaby.event;

import com.schoolaby.domain.Milestone;
import lombok.Getter;

@Getter
public class MilestoneStateCalculationEvent {
    private final Milestone milestone;

    public MilestoneStateCalculationEvent(Milestone milestone) {
        this.milestone = milestone;
    }
}
