package com.schoolaby.event;


import com.schoolaby.domain.Assignment;
import lombok.Getter;

@Getter
public class AssignmentStateCalculationEvent {
    private final Assignment assignment;

    public AssignmentStateCalculationEvent(Assignment assignment) {
        this.assignment = assignment;
    }
}
