package com.schoolaby.event;

import com.schoolaby.domain.Journey;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
public class JourneyStateCalculationEvent extends ApplicationEvent {
    private final Journey journey;

    public JourneyStateCalculationEvent(Journey journey) {
        super(journey);
        this.journey = journey;
    }
}
