package com.schoolaby.service;

import com.schoolaby.domain.UploadedFile;
import com.schoolaby.repository.UploadedFileRepository;
import com.schoolaby.service.storage.FileDetails;
import com.schoolaby.service.storage.StorageService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

import static java.lang.String.format;
import static java.util.UUID.randomUUID;
import static org.apache.commons.io.FilenameUtils.getExtension;

@Service
public class FileService {
    private final StorageService storageService;
    private final UploadedFileRepository uploadedFileRepository;

    public FileService(StorageService storageService, UploadedFileRepository uploadedFileRepository) {
        this.storageService = storageService;
        this.uploadedFileRepository = uploadedFileRepository;
    }

    @Transactional
    public UploadedFile upload(MultipartFile multipartFile) throws IOException {
        String extension = getExtension(multipartFile.getOriginalFilename());

        String uniqueName = generateFilename(multipartFile.getOriginalFilename());

        storageService.save(multipartFile, uniqueName);
        return uploadedFileRepository.save(
            new UploadedFile().originalName(multipartFile.getOriginalFilename()).type(extension).extension(extension).uniqueName(uniqueName));
    }

    public FileDetails getDetails(String uniqueFileName) throws IOException {
        return storageService.getDetails(uniqueFileName);
    }

    public void delete(String uniqueFileName) throws IOException {
        storageService.delete(uniqueFileName);
    }

    private static String generateFilename(String originalFileName) {
        return format("%s.%s", randomUUID().toString(), originalFileName).replace(" ", "_").replaceAll("[^A-Za-z0-9.\\-_]", "");
    }
}
