package com.schoolaby.service.storage;

import java.io.File;
import java.io.IOException;
import org.springframework.web.multipart.MultipartFile;

public interface StorageService {
    FileDetails getDetails(String uniqueFileName) throws IOException;

    void save(MultipartFile file, String uniqueFileName) throws IOException;
    void save(File file, String uniqueFileName, String contentType) throws IOException;

    void delete(String uniqueFileName) throws IOException;
}
