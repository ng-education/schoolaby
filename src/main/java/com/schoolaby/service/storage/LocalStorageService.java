package com.schoolaby.service.storage;

import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.activation.MimetypesFileTypeMap;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import static org.apache.commons.io.FileUtils.copyInputStreamToFile;

@Service
@Profile({ "!test & !prod" })
public class LocalStorageService implements StorageService {
    private static final String FOLDER = "uploadedFiles/uploads/";
    private static final MimetypesFileTypeMap FILE_TYPE_MAP = new MimetypesFileTypeMap();

    public LocalStorageService() {
        FILE_TYPE_MAP.addMimeTypes("type=image/svg+xml exts=svg");
    }

    @Override
    public void save(MultipartFile file, String uniqueFileName) throws IOException {
        File destination = new File(getFilePath(uniqueFileName));
        copyInputStreamToFile(file.getInputStream(), destination);
    }

    @Override
    public void save(File file, String uniqueFileName, String contentType) throws IOException {
        file.renameTo(new File(getFilePath(uniqueFileName)));
    }

    @Override
    public FileDetails getDetails(String uniqueFileName) throws IOException {
        FileInputStream inputStream = new FileInputStream(getFile(uniqueFileName));
        return new FileDetails(inputStream, getMediaType(uniqueFileName));
    }

    @Override
    public void delete(String uniqueFileName) throws IOException {
        File file = getFile(uniqueFileName);
        if (!file.delete()) {
            throw new IOException("Failed to delete file " + uniqueFileName);
        }
    }

    private MediaType getMediaType(String uniqueFileName) {
        return MediaType.valueOf(FILE_TYPE_MAP.getContentType(uniqueFileName));
    }

    private File getFile(String uniqueFileName) throws IOException {
        File file = new File(getFilePath(uniqueFileName));
        if (!file.exists()) {
            throw new FileNotFoundException("File " + uniqueFileName + " does not exist");
        }

        return file;
    }

    private String getFilePath(String uniqueFileName) {
        return FOLDER + uniqueFileName;
    }
}
