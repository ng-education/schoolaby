package com.schoolaby.service.storage;

import java.io.InputStream;
import org.springframework.http.MediaType;

public class FileDetails {
    private InputStream content;
    private MediaType contentType;

    public FileDetails(InputStream content, MediaType contentType) {
        this.content = content;
        this.contentType = contentType;
    }

    public InputStream getContent() {
        return content;
    }

    public void setContent(InputStream content) {
        this.content = content;
    }

    public MediaType getContentType() {
        return contentType;
    }

    public void setContentType(MediaType contentType) {
        this.contentType = contentType;
    }
}
