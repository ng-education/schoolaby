package com.schoolaby.service;

import com.schoolaby.domain.*;
import com.schoolaby.event.AssignmentStateCalculationEvent;
import com.schoolaby.repository.SubmissionFeedbackRepository;
import com.schoolaby.service.dto.submission.SubmissionDTO;
import com.schoolaby.service.dto.submission.SubmissionFeedbackDTO;
import com.schoolaby.service.dto.submission.SubmissionGradingPatchDTO;
import com.schoolaby.service.mapper.SubmissionFeedbackMapper;
import com.schoolaby.service.mapper.UploadedFileMapper;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;
import java.util.stream.Collectors;

import static com.schoolaby.common.JourneyUtils.validateIsUserATeacherInJourney;
import static com.schoolaby.domain.GradingScheme.SCHEMES_WITHOUT_GRADE;
import static com.schoolaby.security.SecurityUtils.IS_TEACHER_OR_ADMIN;
import static com.schoolaby.security.SecurityUtils.getCurrentUserId;
import static java.lang.String.format;
import static java.util.stream.Collectors.toSet;
import static org.springframework.http.HttpStatus.FORBIDDEN;

@Service
@Transactional
@RequiredArgsConstructor
public class SubmissionFeedbackService {
    private final Logger log = LoggerFactory.getLogger(SubmissionFeedbackService.class);

    private final SubmissionFeedbackRepository submissionFeedbackRepository;
    private final SubmissionFeedbackMapper submissionFeedbackMapper;
    private final UploadedFileMapper uploadedFileMapper;
    private final NotificationService notificationService;
    private final SubmissionService submissionService;
    private final AssignmentReadService assignmentReadService;
    private final GroupService groupService;
    private final UserService userService;
    private final JourneyStudentService journeyStudentService;
    private final ApplicationEventPublisher applicationEventPublisher;

    public Set<SubmissionFeedbackDTO> findAll(Long studentId, Long submissionId) {
        List<SubmissionFeedback> submissionFeedbacks = submissionFeedbackRepository.findAllByStudentIdAndSubmissionId(studentId, submissionId);

        if (!submissionFeedbacks.isEmpty() && !isMember(getCurrentUserId(), submissionFeedbacks)) {
            throw new ResponseStatusException(FORBIDDEN, "Current user is not in the journey of the given submission feedback");
        }

        return new LinkedHashSet<>(submissionFeedbackMapper.toDto(submissionFeedbacks));
    }

    private boolean isMember(Long userId, List<SubmissionFeedback> submissionFeedbacks) {
        SubmissionFeedback submissionFeedback = submissionFeedbacks.get(0);
        return submissionFeedback.isTeacher(userId) || submissionFeedback.isStudent(userId);
    }

    @PreAuthorize(IS_TEACHER_OR_ADMIN)
    public SubmissionFeedbackDTO gradeSubmission(Long id, SubmissionGradingPatchDTO submissionGradingPatchDTO) {
        validateSubmissionFeedback(id, submissionGradingPatchDTO);

        SubmissionFeedbackDTO submissionFeedbackDTO = submissionGradingPatchDTO.getSubmissionFeedback();

        Submission existingSubmission = submissionService.getOne(id);
        validateIsUserATeacherInJourney(existingSubmission.getAssignment().getMilestone().getJourney());

        SubmissionFeedback submissionFeedback = createSubmissionFeedback(submissionFeedbackDTO, id, submissionGradingPatchDTO);
        if (submissionFeedback.getGroup() != null) deleteIndividualFeedbacksFromSubmission(existingSubmission);
        submissionFeedback.setSelectedCriterionLevels(cleanSelectedCriterionLevelIds(submissionFeedback.getSelectedCriterionLevels()));

        Submission updatedSubmission = updateSubmissionValues(existingSubmission, submissionGradingPatchDTO);

        submissionFeedback.setSubmission(updatedSubmission);
        submissionFeedback.setUploadedFiles(new HashSet<>(uploadedFileMapper.toEntity(submissionGradingPatchDTO.getFeedbackFiles())));
        SubmissionFeedback savedFeedback = submissionFeedbackRepository.save(submissionFeedback);

        Journey journey = updatedSubmission
            .getAssignment()
            .getMilestone()
            .getJourney();
        applicationEventPublisher.publishEvent(new AssignmentStateCalculationEvent(updatedSubmission.getAssignment()));

        calculateJourneyStudentsAverageGrades(updatedSubmission.getAssignment(), journey, submissionFeedback);

        notificationService.createSubmissionFeedbackNotification(submissionFeedback);

        return submissionFeedbackMapper.toDto(savedFeedback);
    }

    @PreAuthorize(IS_TEACHER_OR_ADMIN)
    public SubmissionFeedbackDTO gradeNonExistingSubmission(SubmissionGradingPatchDTO submissionGradingPatchDTO) {
        validateReceivedFeedbackHasEitherStudentIdOrGroupId(submissionGradingPatchDTO);
        validateGradeValue(submissionGradingPatchDTO);
        Optional<SubmissionDTO> submissionDTO = findSubmissionIfExists(submissionGradingPatchDTO);
        if (submissionDTO.isPresent()) {
            Long submissionId = submissionDTO.get().getId();
            return gradeSubmission(submissionId, submissionGradingPatchDTO.setSubmissionId(submissionId));
        }
        SubmissionFeedback submissionFeedback = submissionFeedbackMapper.toNewEntity(submissionGradingPatchDTO.getSubmissionFeedback());

        Assignment assignment = assignmentReadService.getOne(submissionFeedback.getSubmission().getAssignment().getId(), false);
        validateIsUserATeacherInJourney(assignment.getMilestone().getJourney());

        if (submissionFeedback.getGroup() != null) {
            submissionFeedback.setGroup(groupService.findOne(submissionGradingPatchDTO.getGroupId()).orElseThrow());
        } else if (submissionFeedback.getStudent() != null) {
            submissionFeedback.setStudent(userService.getOneById(submissionGradingPatchDTO.getStudentId()));
        }

        if (submissionFeedback.getCreator() == null) {
            User creator = userService.getOneById(getCurrentUserId());
            submissionFeedback.setCreator(creator);
        }

        submissionFeedback.setUploadedFiles(new HashSet<>(uploadedFileMapper.toEntity(submissionGradingPatchDTO.getFeedbackFiles())));

        updateSubmissionValues(submissionFeedback.getSubmission(), submissionGradingPatchDTO);

        submissionFeedbackRepository.save(submissionFeedback);

        assignment.addSubmission(submissionFeedback.getSubmission());
        notificationService.createSubmissionFeedbackNotification(submissionFeedback);

        Journey journey = assignment
            .getMilestone()
            .getJourney();
        applicationEventPublisher.publishEvent(new AssignmentStateCalculationEvent(assignment));

        calculateJourneyStudentsAverageGrades(assignment, journey, submissionFeedback);

        return submissionFeedbackMapper.toDto(submissionFeedback);
    }

    private Set<SelectedCriterionLevel> cleanSelectedCriterionLevelIds(Set<SelectedCriterionLevel> selectedCriterionLevels) {
        selectedCriterionLevels.forEach(selectedCriterionLevel -> selectedCriterionLevel.setId(null));

        return selectedCriterionLevels;
    }

    private Optional<SubmissionDTO> findSubmissionIfExists(SubmissionGradingPatchDTO submissionGradingPatchDTO) {
        if (submissionGradingPatchDTO.getGroupId() != null || submissionGradingPatchDTO.getSubmissionGroupId() != null) {
            Long groupId = submissionGradingPatchDTO.getGroupId() != null ? submissionGradingPatchDTO.getGroupId() : submissionGradingPatchDTO.getSubmissionGroupId();
            return submissionService.findOneByAssignmentIdAndGroup(submissionGradingPatchDTO.getAssignmentId(), groupId);
        }

        return submissionService.findOneByAssignmentIdAndAuthor(submissionGradingPatchDTO.getAssignmentId(), submissionGradingPatchDTO.getStudentId());
    }

    private void calculateJourneyStudentsAverageGrades(Assignment assignment, Journey journey, SubmissionFeedback submissionFeedback) {
        if (!SCHEMES_WITHOUT_GRADE.contains(assignment.getGradingScheme().getCode())) {
            Set<Long> studentIds = submissionFeedback.getGroup() != null ?
                submissionFeedback.getGroup().getStudents().stream().map(User::getId).collect(toSet()) :
                Set.of(submissionFeedback.getStudent().getId());

            studentIds.forEach(id -> journeyStudentService.calculateJourneyStudentAverageGrade(journey.getId(), id, journey.getUserAverageGrade(id)));
        }
    }

    private void deleteIndividualFeedbacksFromSubmission(Submission existingSubmission) {
        Set<SubmissionFeedbackDTO> individualFeedbacks = findAllIndividualBySubmissionId(existingSubmission.getId());
        List<Long> ids = individualFeedbacks.stream().map(SubmissionFeedbackDTO::getId).collect(Collectors.toList());

        existingSubmission.getSubmissionFeedbacks().removeIf(feedback -> ids.contains(feedback.getId()));
    }

    private SubmissionFeedback createSubmissionFeedback(SubmissionFeedbackDTO submissionFeedbackDTO, Long submissionId, SubmissionGradingPatchDTO submissionGradingPatchDTO) {
        SubmissionFeedback submissionFeedback;

        if (submissionGradingPatchDTO.isValidateExistingFeedback()) {
            if (submissionFeedbackDTO.getStudentId() != null) {
                submissionFeedback = submissionFeedbackMapper.toEntity(submissionFeedbackDTO)
                    .setStudent(userService.getOneById(submissionFeedbackDTO.getStudentId())
                );
            } else if (submissionFeedbackDTO.getGroupId() != null) {
                submissionFeedback = submissionFeedbackMapper.toEntity(submissionFeedbackDTO)
                    .setGroup(groupService.findOne(submissionFeedbackDTO.getGroupId()).orElseThrow()
                );
            } else {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    format("Received submission feedback without student id and group id for submission '%s', at least one is required", submissionId)
                );
            }
        } else {
            submissionFeedback = submissionFeedbackMapper.toEntity(submissionFeedbackDTO)
                .setStudent(userService.getOneById(submissionFeedbackDTO.getStudentId()));
        }

        if (submissionFeedback.getCreator() == null) {
            User creator = userService.getOneById(getCurrentUserId());
            submissionFeedback.setCreator(creator);
        }

        submissionFeedback.setFeedbackDate(submissionFeedbackDTO.getFeedbackDate());
        submissionFeedback.setGrade(submissionFeedbackDTO.getGrade());
        submissionFeedback.setValue(submissionFeedbackDTO.getValue());
        submissionFeedback.setFeedbackFiles(new HashSet<>(uploadedFileMapper.toEntity(submissionFeedbackDTO.getUploadedFiles())));

        return submissionFeedback;
    }

    private Submission updateSubmissionValues(Submission submission, SubmissionGradingPatchDTO submissionGradingPatchDTO) {
        return submission
            .submittedForGradingDate(submissionGradingPatchDTO.getSubmittedForGradingDate())
            .resubmittable(submissionGradingPatchDTO.isResubmittable());
    }

    private void validateSubmissionFeedback(Long id, SubmissionGradingPatchDTO submissionGradingPatchDTO) {
        validateSubmissionExists(id);
        validateSubmissionIdMatchesFeedbackSubmissionId(id, submissionGradingPatchDTO);
        validateReceivedFeedbackHasEitherStudentIdOrGroupId(submissionGradingPatchDTO);
        validateStudentOrGroupIsAuthor(id, submissionGradingPatchDTO);
        if (submissionGradingPatchDTO.isValidateExistingFeedback()) {
            validateSubmissionSubmittedForGradingDate(id, submissionGradingPatchDTO);
        }
    }

    private void validateStudentOrGroupIsAuthor(Long submissionId, SubmissionGradingPatchDTO submissionGradingPatchDTO) {
        if (submissionGradingPatchDTO.getSubmissionFeedback().getStudentId() != null) {
            validateStudentIsAuthor(submissionId, submissionGradingPatchDTO.getStudentId());
        } else if (submissionGradingPatchDTO.getSubmissionFeedback().getGroupId() != null) {
            validateGroupIsAuthor(submissionId, submissionGradingPatchDTO.getGroupId());
        }
    }

    private void validateGroupIsAuthor(Long submissionId, Long groupId) {
        boolean groupIsAuthor = submissionService.getOne(submissionId).getGroup().getId().equals(groupId);
        if (!groupIsAuthor) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                format("Group with id '%s' is not author of the submission with id '%s'", groupId, submissionId)
            );
        }
    }

    private void validateStudentIsAuthor(Long submissionId, Long studentId) {
        boolean studentIsAuthor = submissionService.getOne(submissionId).getAuthors().stream().anyMatch(author -> author.getId().equals(studentId));
        if (!studentIsAuthor) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                format("Student with id '%s' is not author of the submission with id '%s'", studentId, submissionId)
            );
        }
    }

    private void validateSubmissionExists(Long id) {
        if (!submissionService.exists(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                format("Submission with id '%s' does not exist", id)
            );
        }
    }

    private void validateSubmissionIdMatchesFeedbackSubmissionId(Long id, SubmissionGradingPatchDTO submissionGradingPatchDTO) {
        if (!id.equals(submissionGradingPatchDTO.getSubmissionId())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                "Received submission feedback's submission id does not match submission id"
            );
        }
    }

    private void validateSubmissionSubmittedForGradingDate(Long id, SubmissionGradingPatchDTO submissionGradingPatchDTO) {
        Submission existingSubmission = submissionService.getOne(id);

        if (existingSubmission.getSubmittedForGradingDate() != null && !Objects.equals(existingSubmission.getSubmittedForGradingDate(), submissionGradingPatchDTO.getSubmittedForGradingDate())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                format("Received submittedForGradingDate is not equal to submission '%s' submittedForGradingDate", id)
            );
        }
    }

    private void validateReceivedFeedbackHasEitherStudentIdOrGroupId(SubmissionGradingPatchDTO submissionGradingPatchDTO) {
        if (submissionGradingPatchDTO.getStudentId() != null && submissionGradingPatchDTO.getGroupId() != null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                format("Received submission feedback with student id and group id for submission '%s', only one is allowed", submissionGradingPatchDTO.getSubmissionId())
            );
        }
    }

    private void validateGradeValue(SubmissionGradingPatchDTO submissionGradingPatchDTO) {
        String grade = submissionGradingPatchDTO.getSubmissionFeedback().getGrade();

        if (grade == null) {
            return;
        }

        Assignment assignment;
        SubmissionDTO submissionDTO = submissionGradingPatchDTO.getSubmissionFeedback().getSubmission();

        if (submissionDTO != null) {
            Long assignmentId = submissionDTO.getAssignmentId();
            assignment = assignmentReadService.getOne(assignmentId, false);
        } else {
            Submission submission = submissionService.getOne(submissionGradingPatchDTO.getSubmissionId());
            assignment = submission.getAssignment();
        }

        GradingScheme gradingScheme = assignment.getGradingScheme();

        if (!gradingScheme.isValidGrade(grade)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                format("Received grade '%s' is not valid for grading scheme '%s'", grade, gradingScheme.getName())
            );
        }
    }

    private Set<SubmissionFeedbackDTO> findAllIndividualBySubmissionId(Long submissionId) {
        return submissionFeedbackRepository.findAllIndividualBySubmissionId(submissionId).stream().map(submissionFeedbackMapper::toDto).collect(Collectors.toSet());
    }
}
