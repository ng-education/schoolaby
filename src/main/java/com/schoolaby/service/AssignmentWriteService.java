package com.schoolaby.service;

import com.schoolaby.domain.*;
import com.schoolaby.event.AssignmentStateCalculationEvent;
import com.schoolaby.repository.AssignmentRepository;
import com.schoolaby.service.dto.*;
import com.schoolaby.service.mapper.AssignmentMapper;
import com.schoolaby.service.mapper.AssignmentMaterialMapper;
import com.schoolaby.service.mapper.LtiResourceMapper;
import com.schoolaby.service.mapper.RubricMapper;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Predicate;

import static com.schoolaby.common.JourneyUtils.validateIsUserATeacherInJourney;
import static com.schoolaby.domain.NotificationType.GENERAL;
import static com.schoolaby.security.Role.Constants.STUDENT;
import static com.schoolaby.security.SecurityUtils.setInternalUserSecurityContext;
import static com.schoolaby.service.dto.states.EntityState.*;
import static java.lang.String.format;
import static java.time.LocalDate.now;
import static java.time.LocalTime.MAX;
import static java.time.ZoneOffset.UTC;
import static java.util.List.of;
import static java.util.stream.Collectors.*;
import static org.slf4j.LoggerFactory.getLogger;

@Service
@Transactional
@RequiredArgsConstructor
public class AssignmentWriteService {
    private final Logger log = getLogger(AssignmentWriteService.class);

    private final AssignmentReadService assignmentReadService;
    private final MilestoneService milestoneService;
    private final AssignmentMapper assignmentMapper;
    private final AssignmentMaterialMapper assignmentMaterialMapper;
    private final AssignmentRepository assignmentRepository;
    private final UserService userService;
    private final MaterialService materialService;
    private final RubricMapper rubricMapper;
    private final RubricService rubricService;
    private final LtiResourceService ltiResourceService;
    private final LtiResourceMapper ltiResourceMapper;
    private final NotificationService notificationService;
    private final ApplicationEventPublisher applicationEventPublisher;
    private final JourneyService journeyService;

    public AssignmentDTO update(AssignmentUpdateDTO assignmentUpdateDTO) {
        log.debug("Request to update Assignment : {}", assignmentUpdateDTO);

        Assignment existingAssignment = assignmentReadService.getOne(assignmentUpdateDTO.getId(), false);
        Assignment receivedAssignment = assignmentMapper.toEntity(assignmentUpdateDTO);
        Milestone receivedMilestone = milestoneService.getOne(receivedAssignment.getMilestone().getId());
        Milestone existingMilestone = milestoneService.getOne(existingAssignment.getMilestone().getId());

        validateIsUserATeacherInJourney(existingAssignment.getMilestone().getJourney());

        // We are only changing milestone of the assignment, no need to remap related entities to assignment
        if (!receivedAssignment.getMilestone().getId().equals(existingAssignment.getMilestone().getId())) {
            receivedMilestone.addAssignment(existingAssignment);
            existingMilestone.removeAssignment(existingAssignment);
        } else {
            Set<LtiResource> receivedLtiResources = receivedAssignment.getLtiResources();
            receivedLtiResources = fillResourcesLineItems(receivedLtiResources);
            receivedAssignment.setSubmissions(existingAssignment.getSubmissions());
            receivedAssignment.setStates(existingAssignment.getStates());
            receivedAssignment.setLtiResources(receivedLtiResources);
            receivedAssignment.setMilestone(existingMilestone);
            materialService.replaceNonRestrictedAssignmentMaterialsCreatedByOtherUsers(receivedAssignment.getAssignmentMaterials());
            Optional<Rubric> rubricOptional = rubricService.findByAssignmentId(assignmentUpdateDTO.getId());

            rubricOptional.ifPresent(rubric -> {
                if (assignmentUpdateDTO.getRubric() == null || assignmentUpdateDTO.getRubric().getId() == null) {
                    handleRubricChange(rubric);
                }
            });

            if (!receivedAssignment.getGroups().isEmpty() && !existingAssignment.getGroups().isEmpty()) {
                handleGroupCompositionChange(receivedAssignment, existingAssignment);
            }
        }

        Assignment savedAssignment = assignmentRepository.save(receivedAssignment);

        if (assignmentUpdateDTO.getRubric() != null) {
            Rubric rubric = rubricMapper.toEntity(assignmentUpdateDTO.getRubric()).setAssignment(savedAssignment);
            rubricService.save(rubric);
        }

        applicationEventPublisher.publishEvent(new AssignmentStateCalculationEvent(savedAssignment));

        return assignmentMapper.toDto(savedAssignment);
    }

    public AssignmentDTO patch(Long assignmentId, AssignmentPatchDTO assignmentPatchDTO) {
        Assignment existingAssignment = assignmentRepository.findOne(assignmentId, false).orElseThrow(EntityNotFoundException::new);
        Assignment receivedAssignment = new Assignment();
        BeanUtils.copyProperties(existingAssignment, receivedAssignment);

        if (assignmentPatchDTO.getTitle() != null) {
            receivedAssignment.setTitle(assignmentPatchDTO.getTitle());
        }
        if (assignmentPatchDTO.getDescription() != null) {
            receivedAssignment.setDescription(assignmentPatchDTO.getDescription());
        }
        if (assignmentPatchDTO.getDeadline() != null) {
            receivedAssignment.setDeadline(assignmentPatchDTO.getDeadline());
        }
        if (assignmentPatchDTO.getMaterials() != null) {
            Set<AssignmentMaterial> receivedMaterials = new HashSet<>(assignmentMaterialMapper.toEntity(assignmentPatchDTO.getMaterials()));
            receivedMaterials.forEach(receivedMaterial -> receivedMaterial.setAssignment(existingAssignment));
            receivedAssignment.setAssignmentMaterials(receivedMaterials);
        }
        if (assignmentPatchDTO.getLtiResources() != null) {
            Set<LtiResource> receivedLtiResources = new HashSet<>(ltiResourceMapper.toEntity(assignmentPatchDTO.getLtiResources()));
            receivedAssignment.setLtiResources(receivedLtiResources);
        }

        assignmentRepository.save(receivedAssignment);
        return assignmentMapper.toDto(receivedAssignment);
    }

    public AssignmentDTO create(AssignmentUpdateDTO assignmentUpdateDTO) {
        log.debug("Request to create Assignment : {}", assignmentUpdateDTO);

        User creator = userService.getUserWithAuthorities().orElseThrow();
        Long milestoneId = assignmentUpdateDTO.getMilestoneId();
        Journey journeyWithMilestone = journeyService.findByMilestoneId(milestoneId);

        validateIsUserATeacherInJourney(journeyWithMilestone);

        Milestone milestone = journeyWithMilestone.getMilestoneById(milestoneId).orElseThrow();
        Assignment assignment = assignmentMapper.toEntity(assignmentUpdateDTO);

        materialService.replaceDetachedWithManagedMaterials(assignment.getMaterials());
        assignment
            .creator(creator)
            .milestone(milestone);

        Assignment savedAssignment = assignmentRepository.save(assignment);
        if (assignmentUpdateDTO.getRubric() != null) {
            Rubric rubric = rubricMapper.toEntity(assignmentUpdateDTO.getRubric()).setAssignment(savedAssignment);
            rubricService.save(rubric);
        }

        applicationEventPublisher.publishEvent(new AssignmentStateCalculationEvent(savedAssignment));

        return assignmentMapper.toDto(savedAssignment);
    }

    private Set<LtiResource> fillResourcesLineItems(Set<LtiResource> receivedLtiResources) {
        List<Long> resourceIds = receivedLtiResources.stream().map(LtiResource::getId).filter(Objects::nonNull).collect(toList());
        List<LtiResource> existingLtiResources = ltiResourceService.getByIds(resourceIds).stream().map(ltiResourceMapper::toEntity).collect(toList());

        return receivedLtiResources.stream().map(ltiResource -> {
            if (ltiResource.getId() != null && ltiResource.getLineItems().isEmpty()) {
                LtiResource receivedLtiResource = existingLtiResources
                    .stream()
                    .filter(existingResource ->
                        existingResource
                            .getId()
                            .equals(ltiResource.getId()))
                    .findFirst()
                    .orElseThrow();

                Set<LtiLineItem> lineItems = receivedLtiResource.getLineItems();
                ltiResource.setLineItems(lineItems);
            }

            return ltiResource;
        }).collect(toSet());
    }

    private void handleGroupCompositionChange(Assignment receivedAssignment, Assignment existingAssignment) {
        Set<Group> existingGroups = existingAssignment.getGroups();
        Set<Group> receivedGroups = receivedAssignment.getGroups();

        existingGroups.forEach(existingGroup -> {
            if (!existingGroup.getSubmissions().isEmpty()) {
                Predicate<Group> correspondingReceivedGroup = receivedGroup -> existingGroup.getId().equals(receivedGroup.getId());
                Group receivedGroup = receivedGroups.stream().filter(correspondingReceivedGroup).findFirst().orElseThrow(() ->
                    new RuntimeException(format("Group '%s' has a submission but was deleted", existingGroup.getId())));
                boolean studentsChanged = !receivedGroup.getStudents().equals(existingGroup.getStudents());
                if (studentsChanged) {
                    existingGroup.getSubmissions().stream().findFirst().ifPresent(submission -> submission.setAuthors(receivedGroup.getStudents()));
                }
            }
        });
    }

    private void handleRubricChange(Rubric rubric) {
        if (rubric.getIsTemplate()) {
            rubricService.saveDTO(clearRubric(rubricMapper.toDto(rubric)));
        }
        rubricService.delete(rubric);
    }

    private RubricDTO clearRubric(RubricDTO rubric) {
        rubric.setId(null);
        rubric.setAssignmentId(null);
        rubric.setCriterions(clearCriterions(rubric.getCriterions()));
        return rubric;
    }

    private Set<CriterionDTO> clearCriterions(Set<CriterionDTO> criterions) {
        criterions.forEach(criterion -> {
            criterion.setId(null);
            criterion.setLevels(clearCriterionLevels(criterion.getLevels()));
        });
        return criterions;
    }

    private Set<CriterionLevelDTO> clearCriterionLevels(Set<CriterionLevelDTO> criterionLevels) {
        criterionLevels.forEach(level -> level.setId(null));
        return criterionLevels;
    }

    public void delete(Long id) {
        log.debug("Request to delete Assignment : {}", id);
        Assignment assignment = assignmentReadService.getOne(id, false);
        validateIsUserATeacherInJourney(assignment.getMilestone().getJourney());

        Optional<Rubric> rubricOptional = rubricService.findByAssignmentId(id);
        rubricOptional.ifPresent(rubric -> {
            if (rubric.getIsTemplate()) {
                rubricService.saveDTO(clearRubric(rubricMapper.toDto(rubric)));
            }
            rubricService.delete(rubric);
        });

        assignmentRepository.deleteById(id);
        applicationEventPublisher.publishEvent(new AssignmentStateCalculationEvent(assignment));
        assignment.getMilestone().removeAssignment(assignment);
    }

    /**
     * This is scheduled to get fired everyday, at 01:00 (am).
     */
    @Scheduled(cron = "0 0 1 * * ?")
    public void createDeadlineNotifications() {
        setInternalUserSecurityContext();
        LocalDateTime startOfDay = now().atStartOfDay();
        LocalDateTime endOfDay = now().atTime(MAX);

        List<Notification> sentNotifications = assignmentRepository.findAllByDeadlineBetween(startOfDay.toInstant(UTC), endOfDay.toInstant(UTC))
            .parallelStream()
            .map(assignment ->
                assignment.getMilestone().getJourney().getStudents()
                    .stream()
                    .filter(journeyStudent -> of(URGENT, NOT_STARTED, REJECTED)
                        .contains(assignment
                            .getState(journeyStudent.getUser().getId(), STUDENT)))
                    .map(journeyStudent -> createNotification(assignment, journeyStudent.getUser()))
                    .collect(toList())
            ).flatMap(List::stream)
            .collect(toList());

        log.info("Created {} assignment deadline notifications!", sentNotifications.size());
    }

    private Notification createNotification(Assignment assignment, User user) {
        Notification notification = new Notification();
        notification.message("{ASSIGNMENT_DEADLINE_TODAY}")
            .type(GENERAL)
            .link(format("/assignment/%s?journeyId=%s", assignment.getId(), assignment.getMilestone().getJourney().getId()))
            .assignment(assignment)
            .journey(assignment.getMilestone().getJourney())
            .recipient(user);
        return notificationService.save(notification);
    }

    @Async
    public void calculateStatesOfAssignmentsAsync(Set<Long> assignmentIds) {
        Set<Assignment> assignments = assignmentReadService.getAssignmentsByIds(assignmentIds);
        Map<Long, List<Milestone>> groupedMilestones = assignments
            .stream()
            .map(Assignment::getMilestone)
            .collect(groupingBy(Milestone::getId));

        Set<Milestone> milestones = groupedMilestones
            .values()
            .stream()
            .map(milestoneList -> milestoneList.get(0))
            .collect(toSet());

        Journey journey = milestones
            .stream()
            .findFirst()
            .orElseThrow()
            .getJourney();

        assignments.forEach(Assignment::calculateStates);
        milestones.forEach(Milestone::calculateStates);
        journey.calculateStates();
    }
}
