package com.schoolaby.service;

import com.schoolaby.domain.*;
import com.schoolaby.domain.enumeration.Country;
import com.schoolaby.event.AssignmentStateCalculationEvent;
import com.schoolaby.event.AssignmentsStatesCalculationEvent;
import com.schoolaby.event.JourneyStateCalculationEvent;
import com.schoolaby.event.MilestoneStateCalculationEvent;
import com.schoolaby.repository.JourneyRepository;
import com.schoolaby.service.dto.JoinJourneyResultDTO;
import com.schoolaby.service.dto.JourneyDTO;
import com.schoolaby.service.dto.JourneyStudentDTO;
import com.schoolaby.service.dto.states.EntityState;
import com.schoolaby.service.mapper.JourneyMapper;
import com.schoolaby.service.mapper.JourneyStudentMapper;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import java.util.*;

import static com.schoolaby.common.JourneyUtils.USER_NOT_A_TEACHER_IN_JOURNEY;
import static com.schoolaby.common.JourneyUtils.validateIsUserATeacherInJourney;
import static com.schoolaby.domain.NotificationType.GENERAL;
import static com.schoolaby.security.Role.Constants.*;
import static com.schoolaby.security.SecurityUtils.getCurrentUserId;
import static com.schoolaby.security.SecurityUtils.isCurrentUserInRole;
import static com.schoolaby.service.dto.states.JoinJourneyResult.*;
import static java.lang.String.format;
import static java.util.stream.Collectors.toSet;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.http.HttpStatus.*;

@Service
@Transactional
@RequiredArgsConstructor
public class JourneyService {
    private final Logger log = getLogger(JourneyService.class);

    private static final String GIVEN_AUTHORITY_IS_NOT_JOINABLE = "Given authority:%s is not joinable";
    public static final String SIGNUP_CODE_ALREADY_USED = "Given signup code has already been used";
    public static final String NO_JOURNEY_FOUND_WITH_SIGN_UP_CODE = "No journey found with sign-up code '%s'";

    private final JourneyRepository journeyRepository;
    private final UserService userService;
    private final JourneyStateService journeyStateService;
    private final JourneyMapper journeyMapper;
    private final JourneyStudentMapper journeyStudentMapper;
    private final GradeService gradeService;
    private final AssignmentReadService assignmentReadService;
    private final SubmissionService submissionService;
    private final NotificationService notificationService;
    private final JourneyStudentService journeyStudentService;
    private final JourneyTeacherService journeyTeacherService;
    private final ApplicationEventPublisher applicationEventPublisher;
    private final JourneySignupCodeService journeySignupCodeService;

    @Transactional(readOnly = true)
    public Page<JourneyDTO> findAll(Pageable pageable, EntityState state, boolean template) {
        log.debug("Request to get all Journeys");
        Page<Journey> journeys;

        // TODO: Remove the template check when new designs are being enabled in production
        if (template) {
            User user = userService.getUserWithAuthorities().orElseThrow();
            journeys = journeyRepository.findAllByTemplate(pageable, Country.getByValueOrDefault(user.getCountry()).getValue());
        } else if (state != null && (isCurrentUserInRole(STUDENT) || isCurrentUserInRole(TEACHER) || isCurrentUserInRole(ADMIN))) {
            journeys = getJourneysByRole(pageable, state);
        } else {
            journeys = journeyRepository.findAll(pageable);
        }
        return journeys.map(journeyMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<JourneyDTO> getJourneyTemplates(Pageable pageable, String search, Long subjectId, Long userId) {
        if (userId != null) {
            return findUserTemplatesBySearch(pageable, search, subjectId, userId);
        }
        return findAllTemplatesBySearch(pageable, search, subjectId);
    }

    private Page<JourneyDTO> findUserTemplatesBySearch(Pageable pageable, String search, Long subjectId, Long userId) {
        Page<Journey> journeys = journeyRepository.findUserTemplatesBySearchAndSubject(pageable, search, subjectId, userId);
        return journeys.map(journeyMapper::toDto);
    }

    private Page<JourneyDTO> findAllTemplatesBySearch(Pageable pageable, String search, Long subjectId) {
        User currentUser = userService.getUserWithAuthorities().orElseThrow();
        String country = Country.getByValueOrDefault(currentUser.getCountry()).getValue();
        Page<Journey> journeys = journeyRepository.findAllTemplatesBySearchAndSubjectAndCountry(pageable, search, subjectId, country);
        return journeys.map(journeyMapper::toDto);
    }

    private Page<Journey> getJourneysByRole(Pageable pageable, EntityState state) {
        if (isCurrentUserInRole(ADMIN)) {
            return journeyRepository.findAllJourneysByStateForAdmin(pageable, state.toString());
        } else {
            return journeyRepository.findAllByUserAndStateAndAuthority(
                pageable,
                getCurrentUserId(),
                state.toString(),
                isCurrentUserInRole(STUDENT) ? STUDENT : TEACHER
            );
        }
    }

    @Transactional(readOnly = true)
    public List<Journey> findAll() {
        return journeyRepository.findAll();
    }

    @Transactional(readOnly = true)
    public List<Journey> findAllOutOfSyncJourneys(Set<Long> excludedIds) {
        return journeyRepository.findAllOutOfSyncJourneys(excludedIds);
    }

    @Transactional(readOnly = true)
    public Optional<JourneyDTO> findOneDTO(Long id, boolean template) {
        log.debug("Request to get Journey : {}", id);
        return findOne(id, template).map(journeyMapper::toDto);
    }

    @Transactional(readOnly = true)
    Optional<Journey> findOne(Long id, boolean template) {
        return journeyRepository.findOne(id, template);
    }

    @Transactional(readOnly = true)
    Journey getOne(Long id, boolean template) {
        return journeyRepository.findOne(id, template).orElseThrow(EntityNotFoundException::new);
    }

    @Transactional(readOnly = true)
    public Journey findByMilestoneId(Long milestoneId) {
        return journeyRepository.findOneByMilestoneId(milestoneId).orElseThrow(EntityNotFoundException::new);
    }

    public void delete(Long id) {
        log.debug("Request to delete Journey : {}", id);
        Journey journey = getOne(id, false);
        if (!journey.getCreator().getId().equals(getCurrentUserId())) {
            throw new ResponseStatusException(FORBIDDEN, "User not the creator of the journey!");
        }
        applicationEventPublisher.publishEvent(new JourneyStateCalculationEvent(journey));
        journeyRepository.deleteById(id);
    }

    public Optional<JourneyDTO> findOneByMilestoneId(Long milestoneId) {
        Optional<Journey> journey = journeyRepository.findOneByMilestonesId(milestoneId);
        return journey.map(journeyMapper::toDto);
    }

    public Optional<JourneyDTO> findOneByAssignmentId(Long assignmentId) {
        Optional<Journey> journey = journeyRepository.findOneByMilestonesAssignmentsId(assignmentId);
        return journey.map(journeyMapper::toDto);
    }

    public Map<Long, Double> getJourneyStudentsAverageGrade(Long journeyId) {
        return gradeService.getUsersAverageGrade(journeyRepository.getOneWithSubmissionsById(journeyId).orElseThrow(EntityNotFoundException::new));
    }

    public JoinJourneyResultDTO join(JoinJourneyDTO joinDto) {
        JourneySignupCode signupCode = journeySignupCodeService
            .findOne(joinDto.getSignUpCode())
            .orElseThrow(() -> new ResponseStatusException(BAD_REQUEST, format(NO_JOURNEY_FOUND_WITH_SIGN_UP_CODE, joinDto.getSignUpCode())));
        Journey journey = signupCode.getJourney();
        JoinJourneyResultDTO joinJourneyResultDTO = new JoinJourneyResultDTO().setJourneyId(journey.getId());

        User user = userService.getUserWithAuthorities().orElseThrow();
        String authorityName = journey.getSignupCodeAuthority(joinDto.getSignUpCode()).getName();

        if (authorityName.equals(STUDENT) && journey.isStudentInJourney(user.getId())
            || authorityName.equals(TEACHER) && journey.isTeacherInJourney(user.getId())) {
            return joinJourneyResultDTO.setStatus(JOIN_RESULT_ALREADY_JOINED);
        }

        if (signupCode.isUsable()) {
            if (authorityName.equals(STUDENT)) {
                addUserToJourney(journey, user, STUDENT);
            } else if (authorityName.equals(TEACHER)) {
                addUserToJourney(journey, user, TEACHER);
            } else {
                throw new UnsupportedOperationException(String.format(GIVEN_AUTHORITY_IS_NOT_JOINABLE, authorityName));
            }
        } else {
            return joinJourneyResultDTO.setJourneyId(null).setStatus(JOIN_RESULT_SIGNUP_CODE_ALREADY_USED);
        }

        if (signupCode.isOneTime()) {
            signupCode.setUsed(true);
        }

        return joinJourneyResultDTO.setStatus(JOIN_RESULT_SUCCESS);
    }

    public Journey addUserToJourney(Journey journey, User user, String authorityName) {
        if (authorityName.equals(STUDENT)) {
            if (journey.isStudentInJourney(user.getId())) {
                return journey;
            }
            journey.addStudent(user);
        } else {
            if (journey.isTeacherInJourney(user.getId())) {
                return journey;
            }
            journey.addTeacher(user);
        }
        recalculateStates(journey);
        createNotification(user, journey);
        return journey;
    }

    private void recalculateStates(Journey journey) {
        Set<Assignment> assignments = journey.getAssignments();
        Set<Milestone> milestones = journey.getMilestones();

        if (!assignments.isEmpty()) {
            applicationEventPublisher.publishEvent(new AssignmentsStatesCalculationEvent(assignments));
        } else if (!milestones.isEmpty()) {
            milestones.forEach(milestone -> applicationEventPublisher.publishEvent(new MilestoneStateCalculationEvent(milestone)));
        } else {
            applicationEventPublisher.publishEvent(new JourneyStateCalculationEvent(journey));
        }
    }

    private void createNotification(User user, Journey journey) {
        Notification notification = new Notification();
        notification.message("{JOINED_JOURNEY}")
            .type(GENERAL)
            .link(format("/journey/%s", journey.getId()))
            .creator(user)
            .journey(journey)
            .recipient(journey.getCreator());
        notificationService.save(notification);
    }

    public JourneyDTO create(JourneyDTO journeyDTO) {
        log.debug("Request to create Journey : {}", journeyDTO);

        Journey journey = journeyMapper.toEntity(journeyDTO);
        User loggedUser = userService.getUserWithAuthorities().orElseThrow();
        journey.setCreator(loggedUser);
        journey.addTeacher(loggedUser);
        journey.setJourneySignupCodes(Set.of(
            new JourneySignupCode()
                .journey(journey)
                .signUpCode(randomAlphanumeric(4, 8).toLowerCase())
                .authority(new Authority().name(STUDENT)),
            new JourneySignupCode()
                .journey(journey)
                .signUpCode(randomAlphanumeric(4, 8).toLowerCase())
                .authority(new Authority().name(TEACHER))
        ));

        journey = journeyRepository.save(journey);

        if (isTemplate(journey)) {
            Set<Milestone> milestones = journey.getMilestones();
            Set<Assignment> assignments = milestones.stream().flatMap(milestone -> milestone.getAssignments().stream()).collect(toSet());
            assignments.forEach(assignment -> applicationEventPublisher.publishEvent(new AssignmentStateCalculationEvent(assignment)));
            milestones.forEach(milestone -> applicationEventPublisher.publishEvent(new MilestoneStateCalculationEvent(milestone)));
        } else {
            applicationEventPublisher.publishEvent(new JourneyStateCalculationEvent(journey));
        }

        return journeyMapper.toDto(journey);
    }

    public JourneyDTO update(JourneyDTO journeyDTO) {
        log.debug("Request to create Journey : {}", journeyDTO);
        Journey existingJourney = getOne(journeyDTO.getId(), false);
        if (existingJourney.getTeachers().stream().noneMatch(teacher -> teacher.getUser().getId().equals(getCurrentUserId()))) {
            throw new RuntimeException(USER_NOT_A_TEACHER_IN_JOURNEY);
        }
        boolean shouldRecalculateStates = shouldRecalculateStates(journeyDTO, existingJourney);

        Journey journey = journeyMapper.toEntity(journeyDTO);
        journey.setJourneySignupCodes(existingJourney.getJourneySignupCodes());

        journey.setStates(existingJourney.getStates());
        Journey savedJourney = journeyRepository.save(journey);
        if (shouldRecalculateStates) {
            Journey journeyToCalculate = findOne(savedJourney.getId(), false).orElse(savedJourney);
            applicationEventPublisher.publishEvent(new JourneyStateCalculationEvent(journeyToCalculate));
        }
        return journeyMapper.toDto(savedJourney);
    }

    public Set<Journey> getJourneysByCreatorId(Long creatorId) {
        return journeyRepository.findByCreatorId(creatorId);
    }

    private boolean shouldRecalculateStates(JourneyDTO newJourneyDTO, Journey existingJourney) {
        return !newJourneyDTO.getStartDate().equals(existingJourney.getStartDate()) ||
            !newJourneyDTO.getEndDate().equals(existingJourney.getEndDate());
    }

    public String removeStudent(Long journeyId, Long studentId) {
        log.debug("Request to remove student {} from journey {}", studentId, journeyId);
        Journey journey = getOne(journeyId, false);
        validateIsUserATeacherInJourney(journey);
        JourneyStudent removedStudent = journey.removeStudent(studentId);

        if (removedStudent == null) {
            throw new ResponseStatusException(NOT_FOUND);
        }

        journeyStudentService.delete(journeyId, studentId);
        Set<Assignment> assignments = journey.getAssignments();
        assignments.forEach(assignment -> {
            assignmentReadService.removeStudentFromAssignment(assignment, studentId);
            removeStudentFromSubmissionAuthors(studentId, assignment);
        });
        removeStudentJourneyState(journey, studentId);
        applicationEventPublisher.publishEvent(new JourneyStateCalculationEvent(journey));

        User student = removedStudent.getUser();

        return String.format("%s %s", student.getFirstName(), student.getLastName());
    }

    public String removeTeacher(Long journeyId, Long teacherId) {
        log.debug("Request to remove teacher {} from journey {}", teacherId, journeyId);
        Journey journey = getOne(journeyId, false);

        if (!journey.isOwner()) {
            throw new ResponseStatusException(FORBIDDEN);
        }

        JourneyTeacher removedTeacher = journey.removeTeacher(teacherId);

        if (removedTeacher == null) {
            throw new ResponseStatusException(NOT_FOUND);
        }

        journeyTeacherService.delete(journeyId, teacherId);
        removeTeacherJourneyState(journey, teacherId);

        User teacher = removedTeacher.getUser();

        return String.format("%s %s", teacher.getFirstName(), teacher.getLastName());
    }

    public JourneyStudent toggleStudentPin(Long journeyId, Long studentId) {
        log.debug("Request to toggle student {} pin in Journey : {}", studentId, journeyId);
        Journey journey = journeyRepository.findOne(journeyId, false).orElseThrow();
        validateIsUserATeacherInJourney(journey);

        return journeyStudentService.togglePin(journeyId, studentId);
    }

    private void removeStudentJourneyState(Journey journey, Long studentId) {
        Set<JourneyState> states = journey.getStates();
        states.removeIf(state ->
            state.getUser().getId().equals(studentId) &&
                state.getAuthority().getName().equals(STUDENT));
        journey.setStates(states);
        journeyStateService.deleteJourneyState(journey.getId(), studentId, STUDENT);
    }

    private void removeTeacherJourneyState(Journey journey, Long teacherId) {
        Set<JourneyState> states = journey.getStates();
        states.removeIf(state ->
            state.getUser().getId().equals(teacherId) &&
                state.getAuthority().getName().equals(TEACHER));
        journey.setStates(states);
        journeyStateService.deleteJourneyState(journey.getId(), teacherId, TEACHER);
    }

    private void removeStudentFromSubmissionAuthors(Long studentId, Assignment assignment) {
        Set<Submission> submissions = assignment.getSubmissions();
        Set<Submission> submissionsToDelete = new HashSet<>();

        submissions.forEach(submission -> {
            Set<User> authors = submission.getAuthors();

            if (authors.size() > 1) {
                authors.removeIf(p -> studentId.equals(p.getId()));
                Optional<SubmissionFeedback> feedback = submission.findFeedback(studentId, true);
                feedback.ifPresent(submission::removeSubmissionFeedback);
            } else if (authors.stream().anyMatch(author -> author.getId().equals(studentId))) {
                submissionsToDelete.add(submission);
            }
        });

        assignment.removeSubmissions(submissionsToDelete.stream().map(Submission::getId).collect(toSet()));
        submissionService.deleteAll(submissionsToDelete);
    }

    public List<JourneyStudentDTO> getStudents(Long journeyId) {
        Journey journey = journeyRepository.findOne(journeyId, false).orElseThrow();
        validateIsUserATeacherInJourney(journey);

        List<JourneyStudent> users = List.copyOf(journey.getStudents());
        return new ArrayList<>(journeyStudentMapper.toDto(users));
    }

    private boolean isTemplate(Journey journey) {
        return !journey.getMilestones().isEmpty();
    }
}
