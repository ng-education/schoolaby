package com.schoolaby.service;

import com.schoolaby.domain.AssignmentMaterial;
import com.schoolaby.domain.Material;
import com.schoolaby.repository.MaterialRepository;
import com.schoolaby.security.SecurityUtils;
import com.schoolaby.service.dto.MaterialDTO;
import com.schoolaby.service.mapper.EducationalAlignmentMapper;
import com.schoolaby.service.mapper.MaterialMapper;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import java.util.*;
import java.util.stream.Collectors;

import static java.lang.String.format;

@Service
@Transactional
@RequiredArgsConstructor
public class MaterialService {
    private final Logger log = LoggerFactory.getLogger(MaterialService.class);

    private final String USER_IS_NOT_THE_CREATOR_OF_THE_MATERIAL = "User is not the creator of the material!";

    private final MaterialRepository materialRepository;

    private final MaterialMapper materialMapper;

    private final EducationalAlignmentMapper educationalAlignmentMapper;

    /**
     * Save a material.
     *
     * @param materialDTO the entity to save.
     * @return the persisted entity.
     */
    public MaterialDTO save(MaterialDTO materialDTO) {
        log.debug("Request to save Material : {}", materialDTO);
        Material material = materialMapper.toEntity(materialDTO);
        if (material.getId() != null) {
            validateUserIsCreatorOfMaterial(material);
        }
        material = materialRepository.save(material);
        return materialMapper.toDto(material);
    }

    /**
     * Get all the materials.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<MaterialDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Materials");
        return materialRepository.findAll(pageable).map(materialMapper::toDto);
    }

    public List<Material> findAllById(List<Long> ids) {
        return materialRepository.findAllById(ids);
    }

    /**
     * Get all the materials with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<MaterialDTO> findAllWithEagerRelationships(Pageable pageable) {
        return materialRepository.findAllWithEagerRelationships(pageable).map(materialMapper::toDto);
    }

    /**
     * Get one material by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<MaterialDTO> findOne(Long id) {
        log.debug("Request to get Material : {}", id);
        return materialRepository.findOneWithEagerRelationships(id).map(materialMapper::toDto);
    }

    /**
     * Delete the material by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Material : {}", id);
        Material material = materialRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(format("Material with id %d not found", id)));
        validateUserIsCreatorOfMaterial(material);

        materialRepository.delete(material);
    }

    public Page<MaterialDTO> findAllNonRestrictedByEducationalAlignments(List<Long> educationalAlignmentIds, Pageable pageable) {
        Page<Material> suggestedMaterials = materialRepository.findAllByRestrictedFalseAndEducationalAlignmentsIdIn(educationalAlignmentIds, pageable);
        suggestedMaterials.forEach(material -> material.setSequenceNumber(null));
        return suggestedMaterials.map(materialMapper::toDto);
    }

    public Set<Material> findAllByIdInAndRestrictedFalse(List<Long> ids) {
        return new HashSet<>(materialRepository.findAllByIdInAndRestrictedFalseAndCreatedByNot(ids, SecurityUtils.getCurrentUserLogin().orElseThrow()));
    }

    public Page<MaterialDTO> findAllBySearchAndCountry(String searchString, String country, Pageable pageable) {
        if (country == null) {
            return findAllBySearchString(searchString, pageable);
        }
        return materialRepository
            .findAllBySearchAndCountry(searchString.toLowerCase(), SecurityUtils.getCurrentUserLogin().orElse(null), country, pageable)
            .map(materialMapper::toDto);
    }

    private Page<MaterialDTO> findAllBySearchString(String searchString, Pageable pageable) {
        return materialRepository
            .findAllBySearchString(searchString.toLowerCase(), SecurityUtils.getCurrentUserLogin().orElse(null), pageable)
            .map(materialMapper::toDto);
    }

    public void replaceDetachedWithManagedMaterials(Set<Material> materials) {
        List<Long> materialsIds = getMaterialsIds(materials);

        List<Material> existingMaterials = findAllById(materialsIds);
        existingMaterials.forEach(existingMaterial -> {
            if (existingMaterial.isCreatedByCurrentUser()) {
                Material receivedMaterial = materials.stream()
                    .filter(material -> existingMaterial.getId().equals(material.getId()))
                    .findAny().orElseThrow();
                BeanUtils.copyProperties(receivedMaterial, existingMaterial);
            }
            materials.remove(existingMaterial);
            materials.add(existingMaterial);
        });
    }

    public void replaceNonRestrictedMaterialsCreatedByOtherUsers(Set<Material> materials) {
        List<Long> materialsIds = getMaterialsIds(materials);
        Set<Material> nonRestrictedMaterials = findAllByIdInAndRestrictedFalse(materialsIds);
        nonRestrictedMaterials.forEach(nonRestrictedMaterial -> {
            materials.remove(nonRestrictedMaterial);
            materials.add(nonRestrictedMaterial);
        });
    }

    public void replaceNonRestrictedAssignmentMaterialsCreatedByOtherUsers(Set<AssignmentMaterial> assignmentMaterials) {
        Set<Material> materials = assignmentMaterials.stream().map(AssignmentMaterial::getMaterial).collect(Collectors.toSet());
        List<Long> materialsIds = getMaterialsIds(materials);
        Set<Material> nonRestrictedMaterials = findAllByIdInAndRestrictedFalse(materialsIds);
        nonRestrictedMaterials.forEach(nonRestrictedMaterial -> {
            AssignmentMaterial toReplace = assignmentMaterials.stream()
                .filter(am -> nonRestrictedMaterial.getId().equals(am.getMaterial().getId()))
                .findFirst().orElseThrow();
            toReplace.setMaterial(nonRestrictedMaterial);
        });
    }

    private void validateUserIsCreatorOfMaterial(Material material) {
        if (!material.isCreatedByCurrentUser()) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, USER_IS_NOT_THE_CREATOR_OF_THE_MATERIAL);
        }
    }

    private List<Long> getMaterialsIds(Set<Material> materials) {
        return materials.stream()
            .map(Material::getId)
            .filter(Objects::nonNull)
            .collect(Collectors.toList());
    }
}
