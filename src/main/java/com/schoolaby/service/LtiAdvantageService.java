package com.schoolaby.service;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.jwk.JWK;
import com.schoolaby.common.LtiContextHelper;
import com.schoolaby.config.lti.LtiAdvantageProperties;
import com.schoolaby.domain.LtiConfig;
import com.schoolaby.domain.LtiLaunch;
import com.schoolaby.domain.LtiResource;
import com.schoolaby.domain.User;
import com.schoolaby.domain.enumeration.LtiAdvantageRole;
import com.schoolaby.repository.LtiLaunchRepository;
import com.schoolaby.repository.filter.LtiConfigFilter;
import com.schoolaby.service.dto.lti.claims.*;
import io.jsonwebtoken.*;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.net.URI;
import java.util.Date;
import java.util.List;

import static com.schoolaby.common.LtiUtils.createPendingScore;
import static com.schoolaby.security.SecurityUtils.getAuthorities;
import static com.schoolaby.security.SecurityUtils.getCurrentUserId;
import static com.schoolaby.service.dto.lti.claims.LtiContextToken.LtiMessageType.DEEP_LINKING;
import static com.schoolaby.service.dto.lti.claims.LtiContextToken.LtiMessageType.RESOURCE_LINK;
import static com.schoolaby.service.mapper.UserDetailedMapper.userFromId;
import static io.jsonwebtoken.SignatureAlgorithm.RS256;
import static java.lang.Long.parseLong;
import static java.lang.String.format;
import static java.lang.System.currentTimeMillis;
import static java.util.Objects.requireNonNullElse;
import static org.apache.commons.lang3.StringUtils.isEmpty;

@Service
@Transactional
@RequiredArgsConstructor
public class LtiAdvantageService {
    public static final String CLAIM_MESSAGE_TYPE = "https://purl.imsglobal.org/spec/lti/claim/message_type";
    public static final String CLAIM_TARGET_LINK_URI = "https://purl.imsglobal.org/spec/lti/claim/target_link_uri";
    public static final String CLAIM_DEEP_LINKING_SETTINGS = "https://purl.imsglobal.org/spec/lti-dl/claim/deep_linking_settings";
    public static final String CLAIM_AGS_ENDPOINT = "https://purl.imsglobal.org/spec/lti-ags/claim/endpoint";
    public static final String CLAIM_CONTEXT = "https://purl.imsglobal.org/spec/lti/claim/context";
    public static final String CLAIM_RESOURCE_LINK = "https://purl.imsglobal.org/spec/lti/claim/resource_link";
    public static final String CLAIM_DEPLOYMENT_ID = "https://purl.imsglobal.org/spec/lti/claim/deployment_id";
    public static final String CLAIM_VERSION = "https://purl.imsglobal.org/spec/lti/claim/version";
    public static final String CLAIM_ROLES = "https://purl.imsglobal.org/spec/lti/claim/roles";
    public static final String CLAIM_CUSTOM = "https://purl.imsglobal.org/spec/lti/claim/custom";
    public static final String PARAM_ISSUER = "iss";
    public static final String PARAM_TARGET_LINK_URI = "target_link_uri";
    public static final String PARAM_CLIENT_ID = "client_id";
    public static final String PARAM_LTI_DEPLOYMENT_ID = "lti_deployment_id";
    public static final String PARAM_LOGIN_HINT = "login_hint";
    public static final String PARAM_LTI_MESSAGE_HINT = "lti_message_hint";


    private final LtiAdvantageProperties properties;
    private final JWK ltiJwk;
    private final LtiConfigService ltiConfigService;
    private final LtiResourceService ltiResourceService;
    private final LtiLaunchRepository ltiLaunchRepository;
    private final SigningKeyResolver toolKeyResolver;
    private final LtiContextHelper contextHelper;
    private final UserService userService;
    private final SimpMessagingTemplate simpMessagingTemplate;

    public RedirectView redirectDeepLinkingRequestToTool(Long appId, Long journeyId, RedirectAttributes attributes) throws JOSEException {
        LtiConfig config = ltiConfigService.getOne(LtiConfigFilter.builder()
            .appId(appId)
            .journeyId(journeyId)
            .build());
        attributes
            .addAttribute(PARAM_ISSUER, properties.getIss())
            .addAttribute(PARAM_TARGET_LINK_URI, config.getDeepLinkingUrl())
            .addAttribute(PARAM_CLIENT_ID, config.getLtiApp().getClientId())
            .addAttribute(PARAM_LTI_DEPLOYMENT_ID, config.getDeploymentId())
            .addAttribute(PARAM_LOGIN_HINT, getCurrentUserId().toString())
            .addAttribute(PARAM_LTI_MESSAGE_HINT,
                new LtiContextToken()
                    .messageType(DEEP_LINKING)
                    .userId(getCurrentUserId().toString())
                    .role(getRole())
                    .journeyId(journeyId != null ? journeyId.toString() : null)
                    .toToken(ltiJwk.toRSAKey().toPrivateKey(), properties.getTokenValidityInSeconds()));
        return new RedirectView(config.getLoginInitiationUrl());
    }

    public RedirectView redirectAppLaunchToTool(Long appId, Long journeyId, String resourceLinkId, String appClientId, String ltiConfigType, RedirectAttributes attributes) throws JOSEException {
        LtiConfig config = ltiConfigService.getOne(LtiConfigFilter.builder()
            .appId(appId)
            .journeyId(journeyId)
            .appClientId(appClientId)
            .ltiConfigType(ltiConfigType)
            .build());
        attributes
            .addAttribute(PARAM_ISSUER, properties.getIss())
            .addAttribute(PARAM_TARGET_LINK_URI, config.getLaunchUrl())
            .addAttribute(PARAM_CLIENT_ID, config.getLtiApp().getClientId())
            .addAttribute(PARAM_LTI_DEPLOYMENT_ID, config.getDeploymentId())
            .addAttribute(PARAM_LOGIN_HINT, getCurrentUserId().toString())
            .addAttribute(PARAM_LTI_MESSAGE_HINT,
                new LtiContextToken()
                    .messageType(RESOURCE_LINK)
                    .userId(getCurrentUserId().toString())
                    .role(getRole())
                    .journeyId(journeyId != null ? journeyId.toString() : null)
                    .resourceLink(new ResourceLink().id(resourceLinkId))
                    .appClientId(appClientId)
                    .ltiConfigType(ltiConfigType)
                    .toToken(ltiJwk.toRSAKey().toPrivateKey(), properties.getTokenValidityInSeconds()));
        return new RedirectView(config.getLoginInitiationUrl());
    }

    public RedirectView redirectResourceLaunchToTool(Long resourceId, String studentId, RedirectAttributes attributes) throws JOSEException {
        LtiResource resource = ltiResourceService.getOne(resourceId);

        Long journeyId = resource.getJourney() != null ? resource.getJourney().getId() : null;
        LtiConfig config = ltiConfigService.getOne(LtiConfigFilter.builder()
            .appId(resource.getLtiApp().getId())
            .journeyId(journeyId)
            .build());
        attributes
            .addAttribute(PARAM_ISSUER, properties.getIss())
            .addAttribute(PARAM_TARGET_LINK_URI, requireNonNullElse(resource.getLaunchUrl(), config.getLaunchUrl()))
            .addAttribute(PARAM_CLIENT_ID, resource.getLtiApp().getClientId())
            .addAttribute(PARAM_LTI_DEPLOYMENT_ID, config.getDeploymentId())
            .addAttribute(PARAM_LOGIN_HINT, getCurrentUserId().toString())
            .addAttribute(PARAM_LTI_MESSAGE_HINT,
                new LtiContextToken()
                    .messageType(RESOURCE_LINK)
                    .userId(getCurrentUserId().toString())
                    .role(getRole())
                    .studentId(studentId)
                    .journeyId(journeyId != null ? journeyId.toString() : null)
                    .resourceLink(new ResourceLink().id(resource.getResourceLinkId()))
                    .toToken(ltiJwk.toRSAKey().toPrivateKey(), properties.getTokenValidityInSeconds()));

        if(studentId == null && resource.getJourney().isStudentInJourney(getCurrentUserId())) {
            createNewLineItem(resource, getCurrentUserId());
        }

        return new RedirectView(config.getLoginInitiationUrl());
    }

    public String parseContextToken(String token) throws JOSEException {
        String subject = Jwts.parserBuilder()
            .setSigningKey(ltiJwk.toRSAKey().toPublicKey())
            .build()
            .parseClaimsJws(token)
            .getBody()
            .getSubject();
        if (isEmpty(subject)) {
            throw new BadCredentialsException("User id token does not contain subject!");
        }
        return subject;
    }

    public String createIdToken(String loginHint, String messageHint, String nonce, URI redirectUri) throws JOSEException {
        LtiContextToken contextToken = LtiContextToken.parse(messageHint, ltiJwk.toRSAKey().toPublicKey());
        if (!loginHint.equals(contextToken.getUserId())) {
            throw new BadCredentialsException(format("User id in loginHint '%s' !== user id in token '%s'", loginHint, contextToken.getUserId()));
        }

        JwtBuilder jwt = Jwts.builder();
        LtiConfig ltiConfig;
        switch (contextToken.getMessageType()) {
            case DEEP_LINKING:
                ltiConfig = ltiConfigService.getOne(LtiConfigFilter.builder()
                    .journeyId(parseLong(contextToken.getJourneyId()))
                    .redirectHost(redirectUri.getHost())
                    .build());
                jwt.claim(CLAIM_MESSAGE_TYPE, DEEP_LINKING.toString())
                    .claim(CLAIM_TARGET_LINK_URI, ltiConfig.getDeepLinkingUrl())
                    .claim(CLAIM_DEEP_LINKING_SETTINGS,
                        new DeepLinkingSettings()
                            .returnUrl(properties.getIss() + "/api/lti-advantage/deep-linking/response")
                            .acceptTypes("ltiResourceLink")
                            .acceptPresentationDocumentTargets("iframe")
                            .acceptMultiple(false));
                break;
            case RESOURCE_LINK:
                String journeyId = contextToken.getJourneyId();
                if (journeyId != null) {
                    LtiResource resource = ltiResourceService.getOneByResourceLinkId(contextToken.getResourceLink().getId());
                    ltiConfig = ltiConfigService.getOne(LtiConfigFilter.builder()
                        .journeyId(parseLong(journeyId))
                        .redirectHost(redirectUri.getHost())
                        .build());
                    jwt.claim(CLAIM_AGS_ENDPOINT, new GradingEndpoints(properties.getIss(), contextHelper.toBase64Url(parseLong(contextToken.getJourneyId()))))
                        .claim(CLAIM_CONTEXT, new LtiContext().id(contextHelper.toBase64Url(parseLong(contextToken.getJourneyId()))))
                        .claim(CLAIM_TARGET_LINK_URI, requireNonNullElse(resource.getLaunchUrl(), ltiConfig.getLaunchUrl()));
                } else {
                    ltiConfig = ltiConfigService.getOne(LtiConfigFilter.builder()
                        .appClientId(contextToken.getAppClientId())
                        .ltiConfigType(contextToken.getLtiConfigType())
                        .build());
                    jwt.claim(CLAIM_TARGET_LINK_URI, ltiConfig.getLaunchUrl());
                }
                jwt.claim(CLAIM_MESSAGE_TYPE, RESOURCE_LINK.toString())
                    .claim(CLAIM_RESOURCE_LINK, contextToken.getResourceLink());
                break;
            default:
                throw new UnsupportedOperationException(format("Unknown message type: %s", contextToken.getMessageType()));
        }
        User user = userService.getOneById(Long.parseLong(contextToken.getUserId()));

        return jwt.setSubject(contextToken.getUserId())
            .setIssuer(properties.getIss())
            .setIssuedAt(new Date())
            .setAudience(ltiConfig.getLtiApp().getClientId())
            .claim(CLAIM_DEPLOYMENT_ID, ltiConfig.getDeploymentId())
            .claim("nonce", nonce)
            .claim(CLAIM_VERSION, ltiConfig.getLtiApp().getVersion())
            .claim(CLAIM_CUSTOM,
                new CustomClaim()
                    .setStudentId(contextToken.getStudentId()))
            .claim("given_name", user.getFirstName())
            .claim("family_name", user.getLastName())
            .setHeaderParam("kid", ltiJwk.getKeyID())
            .signWith(ltiJwk.toRSAKey().toPrivateKey(), RS256)
            .setExpiration(new Date(currentTimeMillis() + properties.getTokenValidityInSeconds() * 1000L))
            .claim(CLAIM_ROLES, List.of(contextToken.getRole()))
            .compact();
    }

    private void createNewLineItem(LtiResource resource, Long studentId) {
        boolean studentHasScore = resource.getLineItems().stream().anyMatch(lineItem -> lineItem.userHasScore(studentId));
        boolean shouldCreateNewLineItem = (resource.isResubmittable() || !studentHasScore) && resource.getAssignment() != null && resource.isSyncGrade();

        if (shouldCreateNewLineItem) {
            LtiLaunch ltiLaunch = ltiLaunchRepository.save(new LtiLaunch()
                .user(userFromId(getCurrentUserId()))
                .ltiResource(resource));

            List<LtiLaunch> launches = ltiLaunchRepository.findAll(resource.getAssignment().getId(), getCurrentUserId(), null);
            simpMessagingTemplate.convertAndSendToUser(userService.getOneById(getCurrentUserId()).getLogin(), "/queue/lti-scores", createPendingScore(ltiLaunch, launches));
        }
    }

    private String getRole() {
        return LtiAdvantageRole.getRole(getAuthorities().findFirst().orElseThrow()).getRoleName();
    }

    public Jws<Claims> parseToolClaims(String toolJwt) {
        return Jwts.parserBuilder()
            .setSigningKeyResolver(toolKeyResolver)
            .build()
            .parseClaimsJws(toolJwt);
    }
}
