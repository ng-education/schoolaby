package com.schoolaby.service;

import com.schoolaby.domain.LtiLaunch;
import com.schoolaby.domain.LtiLineItem;
import com.schoolaby.domain.LtiScore;
import com.schoolaby.repository.LineItemRepository;
import com.schoolaby.repository.LtiLaunchRepository;
import com.schoolaby.repository.LtiScoreRepository;
import com.schoolaby.repository.UserRepository;
import com.schoolaby.service.dto.lti.LtiScoreDTO;
import com.schoolaby.service.dto.lti.external.LtiExtLineItemDTO;
import com.schoolaby.service.dto.lti.external.LtiExtScoreDTO;
import com.schoolaby.service.mapper.LtiExtLineItemMapper;
import com.schoolaby.service.mapper.LtiExtScoreMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@Slf4j
@Service
@RequiredArgsConstructor
public class LtiGradingService {
    private final LineItemRepository lineItemRepository;
    private final LtiScoreRepository scoreRepository;
    private final LtiExtLineItemMapper extLineItemMapper;
    private final LtiExtScoreMapper scoreMapper;
    private final SimpMessagingTemplate simpMessagingTemplate;
    private final UserRepository userRepository;
    private final LtiLaunchRepository ltiLaunchRepository;

    @Transactional
    public Collection<LtiExtLineItemDTO> findAllLineItems(Long journeyId, String resourceLinkId) {
        return extLineItemMapper.toDto(lineItemRepository.findAll(journeyId, resourceLinkId));
    }

    @Transactional
    public Long createScore(Long lineItemId, LtiExtScoreDTO scoreDto) {
        LtiLineItem ltiLineItem = lineItemRepository.getOne(lineItemId);
        LtiScore score = scoreMapper.toEntity(scoreDto);
        ltiLineItem.addScore(score);

        LtiScore savedScore = scoreRepository.save(score);
        log.info("Score with id {} saved", savedScore.getId());

        return savedScore.getId();
    }

    @Transactional
    public void sendScoreToWebSocket(Long scoreId) {
        LtiScore score = scoreRepository.getOne(scoreId);
        if (!score.getLtiResource().isSyncGrade()) {
            return;
        }

        String username = userRepository.findById(score.getUser().getId())
            .orElseThrow()
            .getLogin();

        this.simpMessagingTemplate.convertAndSendToUser(username, "/queue/lti-scores", getScoreDTO(score));
    }

    private LtiScoreDTO getScoreDTO(LtiScore score) {
        Long resourceId = score.getLtiResource().getId();
        Long userId = score.getUser().getId();

        log.info("Searching for the launches for resource with id {} and user with id {}", resourceId, userId);

        List<LtiLaunch> resourceLaunches = ltiLaunchRepository.findByResourceAndUser(resourceId, userId);

        log.info("Found {} launches", resourceLaunches.size());

        Set<Instant> launchDates = resourceLaunches.stream()
            .map(LtiLaunch::getCreatedDate)
            .collect(Collectors.toSet());

        return new LtiScoreDTO()
            .setTitle(score.getLtiResource().getTitle())
            .setResourceId(resourceId)
            .setUserId(userId)
            .setScore(String.valueOf(score.getScoreGiven()))
            .setScoreMax(String.valueOf(score.getScoreMaximum()))
            .setCreatedDate(score.getCreatedDate())
            .setLaunches(launchDates);
    }
}
