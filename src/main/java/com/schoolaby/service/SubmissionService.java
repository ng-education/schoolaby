package com.schoolaby.service;

import com.schoolaby.domain.*;
import com.schoolaby.event.AssignmentStateCalculationEvent;
import com.schoolaby.repository.SubmissionRepository;
import com.schoolaby.service.dto.submission.SubmissionDTO;
import com.schoolaby.service.dto.submission.SubmissionFeedbackDTO;
import com.schoolaby.service.dto.submission.SubmissionResubmitPatchDTO;
import com.schoolaby.service.mapper.SubmissionMapper;
import com.schoolaby.service.mapper.UploadedFileMapper;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.schoolaby.security.SecurityUtils.*;
import static java.lang.String.format;

@Service
@Transactional
@RequiredArgsConstructor
public class SubmissionService {
    private final Logger log = LoggerFactory.getLogger(SubmissionService.class);

    private final AssignmentReadService assignmentReadService;
    private final UploadedFileMapper uploadedFileMapper;
    private final SubmissionMapper submissionMapper;
    private final SubmissionRepository submissionRepository;
    private final NotificationService notificationService;
    private final GroupService groupService;
    private final ApplicationEventPublisher applicationEventPublisher;

    public SubmissionDTO create(SubmissionDTO submissionDTO) {
        log.debug("Request to create Submission : {}", submissionDTO);

        Long authorId = submissionDTO.getAuthors().stream().findFirst().orElseThrow().getId();

        if (submissionRepository.findOneByAssignmentIdAndAuthor(submissionDTO.getAssignmentId(), authorId).isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Submission already exists");
        }

        Submission createdSubmission = submissionMapper.toEntity(submissionDTO);
        Assignment assignment = assignmentReadService.getOne(submissionDTO.getAssignmentId(), false);

        Optional<Group> group = Optional.empty();
        if (submissionDTO.getGroupId() != null) {
            group = groupService.findOne(submissionDTO.getGroupId());
        } else if (!assignment.getGroups().isEmpty()) {
            group = assignment.getGroups()
                .stream()
                .filter(assignmentGroup -> assignmentGroup.getStudents()
                    .stream()
                    .anyMatch(student -> student
                        .getId()
                        .equals(authorId)))
                .findFirst();
        }

        group.ifPresent(submissionGroup -> {
            createdSubmission.getAuthors().forEach(submissionAuthor ->
                validateIsUserInGroup(submissionGroup, submissionAuthor.getId()));
            Set<User> students = submissionGroup.getStudents();
            createdSubmission.setAuthors(new HashSet<>(students));
            if (createdSubmission.getGroup() == null) {
                createdSubmission.setGroup(submissionGroup);
            }
        });

        validateSubmission(submissionDTO, assignment, group);

        Submission savedSubmission = submissionRepository.save(createdSubmission);
        assignment.addSubmission(savedSubmission);

        if (savedSubmission.getSubmittedForGradingDate() != null) {
            notificationService.createSolutionSubmittedNotifications(savedSubmission);
        }

        applicationEventPublisher.publishEvent(new AssignmentStateCalculationEvent(assignment));
        return submissionMapper.toDto(savedSubmission);
    }

    public boolean removeAuthorById(Submission submission, Long authorId) {
        Set<User> authors = submission.getAuthors();
        boolean removedAuthorOrDeleted = false;

        if (authors.size() > 1) {
            removedAuthorOrDeleted = authors.removeIf(p -> authorId.equals(p.getId()));
            Optional<SubmissionFeedback> feedback = submission.findFeedback(authorId, true);
            feedback.ifPresent(submission::removeSubmissionFeedback);
        } else if (authors.stream().anyMatch(author -> author.getId().equals(authorId))) {
            delete(submission.getId());
            removedAuthorOrDeleted = true;
        }

        return removedAuthorOrDeleted;
    }

    @PreAuthorize(IS_TEACHER_OR_ADMIN)
    public SubmissionDTO save(SubmissionDTO submissionDTO) {
        Submission savedSubmission = submissionRepository.save(submissionMapper.toEntity(submissionDTO));
        return submissionMapper.toDto(savedSubmission);
    }

    @PreAuthorize(IS_ADMIN)
    public SubmissionDTO update(SubmissionDTO submissionDTO) {
        log.debug("Request to update Submission : {}", submissionDTO);

        Submission existingSubmission = submissionRepository.findOne(submissionDTO.getId())
            .orElseThrow(() -> new RuntimeException(format("Submission '%s' not found!", submissionDTO.getId())));

        Optional<Group> groupOptional = groupService.findOne(submissionDTO.getGroupId());
        validateSubmission(submissionDTO, existingSubmission.getAssignment(), groupOptional);

        Submission submission = submissionRepository.save(submissionMapper.toEntity(submissionDTO));
        return submissionMapper.toDto(submission);
    }

    @Transactional(readOnly = true)
    public Page<SubmissionDTO> findAll(Pageable pageable, Long assignmentId, Long journeyId) {
        log.debug("Request to get all Submissions");

        Page<SubmissionDTO> allSubmissions = submissionRepository.findAll(pageable, journeyId, assignmentId).map(submissionMapper::toDto);

        if (!isCurrentUserTeacherOrAdmin()) {
            allSubmissions.forEach(this::filterSubmissionFeedbacksGivenToCurrentUser);
        }

        return allSubmissions;
    }

    @Transactional(readOnly = true)
    public Optional<SubmissionDTO> findOne(Long id) {
        log.debug("Request to get Submission : {}", id);

        Optional<SubmissionDTO> submission = submissionRepository.findOne(id).map(submissionMapper::toDto);

        if (!isCurrentUserTeacherOrAdmin() && submission.isPresent()) {
            submission = Optional.of(filterSubmissionFeedbacksGivenToCurrentUser(submission.get()));
        }

        return submission;
    }

    @Transactional(readOnly = true)
    public Optional<SubmissionDTO> findOneByAssignmentIdAndAuthor(Long assignmentId, Long authorId) {
        log.debug("Request to get Submission by assignment id: {} and author id: {}", assignmentId, authorId);

        return submissionRepository.findOneByAssignmentIdAndAuthor(assignmentId, authorId).map(submissionMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Optional<SubmissionDTO> findOneByAssignmentIdAndGroup(Long assignmentId, Long groupId) {
        log.debug("Request to get Submission by assignment id: {} and group id: {}", assignmentId, groupId);

        return submissionRepository.findOneByAssignmentIdAndGroup(assignmentId, groupId).map(submissionMapper::toDto);
    }

    public void delete(Long id) {
        log.debug("Request to delete Submission : {}", id);
        Submission submission = getOne(id);
        Assignment assignment = submission.getAssignment();
        assignment.removeSubmission(submission.getId());
        submissionRepository.deleteById(id);

        applicationEventPublisher.publishEvent(new AssignmentStateCalculationEvent(assignment));
    }

    public void deleteAll(Set<Submission> submissions) {
        submissionRepository.deleteAll(submissions);
    }


    @PreAuthorize(IS_STUDENT_OR_TEACHER_OR_ADMIN)
    public SubmissionDTO resubmit(Long id, SubmissionResubmitPatchDTO submissionResubmitPatchDTO) {
        Submission submission = getOne(id);

        if (submission.getGroup() != null) {
            validateIsUserInGroup(submission.getGroup(), getCurrentUserId());
        }

        if (submission.getGroup() == null && !submission.hasAuthor(getCurrentUserId())) {
            throw new AccessDeniedException(
                format("User '%s' is not the author of submission '%s'!", getCurrentUserId(), submission.getId())
            );
        }

        if (submission.isGraded() && !submission.isResubmittable()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                format("Submission '%s' is already graded!", submission.getId())
            );
        }

        validateDeadlineNotPassed(submission.getAssignment(), submission.isResubmittable());

        if (submission.getGroup() != null) {
            submission.setAuthors(new HashSet<>(submission.getGroup().getStudents()));
        }

        submission.setValue(submissionResubmitPatchDTO.getValue());
        submission.setSubmittedForGradingDate(submissionResubmitPatchDTO.getSubmittedForGradingDate());
        submission.setUploadedFiles(uploadedFileMapper.toEntity(submissionResubmitPatchDTO.getUploadedFiles()));

        if (submission.getSubmittedForGradingDate() != null) {
            notificationService.createSolutionSubmittedNotifications(submission);
        }

        applicationEventPublisher.publishEvent(new AssignmentStateCalculationEvent(submission.getAssignment()));

        return submissionMapper.toDto(submission);
    }

    public boolean exists(Long id) {
        return submissionRepository.existsById(id);
    }

    private SubmissionDTO filterSubmissionFeedbacksGivenToCurrentUser(SubmissionDTO submission) {
        Predicate<SubmissionFeedbackDTO> isUsersFeedback = feedback -> feedback.getStudentId() != null && feedback.getStudentId().equals(getCurrentUserId());
        Predicate<SubmissionFeedbackDTO> isGroupsFeedback = feedback -> feedback.getGroupId() != null;

        Set<SubmissionFeedbackDTO> submissionFeedbacks = submission.getSubmissionFeedbacks();
        Set<SubmissionFeedbackDTO> filteredFeedbacks = submissionFeedbacks.stream().filter(isUsersFeedback.or(isGroupsFeedback)).collect(Collectors.toCollection(LinkedHashSet::new));
        submission.setSubmissionFeedbacks(filteredFeedbacks);

        return submission;
    }

    private void validateIsUserInGroup(Group group, Long userId) {
        boolean isStudentInGroup = group.getStudents()
            .stream()
            .map(User::getId)
            .anyMatch(id -> id.equals(userId));

        if (!isStudentInGroup) {
            String errorMessage = format("User '%s' is not a group member of the submission!", userId);
            throw new AccessDeniedException(errorMessage);
        }
    }

    Submission getOne(Long id) {
        return submissionRepository.findOne(id).orElseThrow(EntityNotFoundException::new);
    }

    private void validateSubmission(SubmissionDTO submissionDTO, Assignment assignment, Optional<Group> groupOptional) {
        if (!isCurrentUserTeacherOrAdmin()) {
            validateSubmissionFeedbacksAreEmpty(submissionDTO);
            validateIsUserOneOfAuthors(submissionDTO, getCurrentUserId());
            groupOptional.ifPresent(group -> validateIsUserInGroup(group, getCurrentUserId()));
            validateDeadlineNotPassed(assignment, false);
        }
    }

    private void validateSubmissionFeedbacksAreEmpty(SubmissionDTO submissionDTO) {
        if (!submissionDTO.getSubmissionFeedbacks().isEmpty()) {
            throw new AccessDeniedException("Student can not create a submission with a feedback!");
        }
    }

    private void validateIsUserOneOfAuthors(SubmissionDTO submissionDTO, Long userId) {
        boolean isAuthor = submissionDTO
            .getAuthors()
            .stream()
            .anyMatch(author -> author.getId().equals(userId));
        if (!isAuthor) {
            throw new AccessDeniedException(format("Student '%s' is not an author of the submission!", userId));
        }
    }

    private void validateDeadlineNotPassed(Assignment assignment, boolean resubmittable) {
        if (assignment.deadlinePassed() && !assignment.isFlexibleDeadline() && !resubmittable) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                format("Assignment '%s' deadline has passed and can not be updated!", assignment.getId())
            );
        }
    }
}
