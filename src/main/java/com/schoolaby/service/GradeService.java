package com.schoolaby.service;

import static java.util.stream.Collectors.toMap;

import com.schoolaby.domain.Journey;
import com.schoolaby.domain.User;
import java.util.Map;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class GradeService {

    public Map<Long, Double> getUsersAverageGrade(Journey journey) {
        return journey
            .getJourneyStudentsUsers()
            .stream()
            .collect(toMap(User::getId, user -> journey.getUserAverageGrade(user.getId())));
    }
}
