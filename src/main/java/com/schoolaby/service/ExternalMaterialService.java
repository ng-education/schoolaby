package com.schoolaby.service;

import com.schoolaby.config.external_material.OpiqProperties;
import com.schoolaby.repository.ExternalMaterialRepository;
import com.schoolaby.service.dto.MaterialDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class ExternalMaterialService {

    private final ExternalMaterialRepository externalMaterialRepository;

    private final OpiqProperties opiqProperties;

    public List<MaterialDTO> getMaterialsBySearchString(NativeWebRequest request, String searchString, Integer limit) {
        List<MaterialDTO> responseMaterials = new ArrayList<>();
        Integer calculatedLimit = limit / 2;
        responseMaterials.addAll(getOpiqMaterialsBySearchString(searchString, calculatedLimit));
        responseMaterials.addAll(getEkoolikottMaterialsBySearchString(request, searchString, calculatedLimit));
        return responseMaterials;
    }

    public List<MaterialDTO> getEkoolikottMaterialsBySearchString(NativeWebRequest request, String searchString, Integer limit) {
        return externalMaterialRepository.getEkoolikottMaterials(request, searchString, limit, null);
    }

    public List<MaterialDTO> getOpiqMaterialsBySearchString(String searchString, Integer limit) {
        if (opiqProperties.getApiKeyValue() != null) {
            return externalMaterialRepository.getOpiqMaterials(searchString, null, limit);
        }
        return new ArrayList<>();
    }

    public List<MaterialDTO> getSuggestedMaterials(NativeWebRequest request, String searchString, String subjects, Integer limit, String taxon) {
        List<MaterialDTO> responseMaterials = new ArrayList<>();
        Integer calculatedLimit = limit / 2;
        responseMaterials.addAll(getSuggestedOpiqMaterials(searchString, subjects, calculatedLimit));
        responseMaterials.addAll(getSuggestedEkoolikottMaterials(request, calculatedLimit, taxon));
        return responseMaterials;
    }

    public List<MaterialDTO> getSuggestedEkoolikottMaterials(NativeWebRequest request, Integer limit, String taxon){
        return externalMaterialRepository.getEkoolikottMaterials(request, null, limit, taxon);
    }

    public List<MaterialDTO> getSuggestedOpiqMaterials(String searchString, String subjects, Integer limit) {
        List<MaterialDTO> responseMaterials = new ArrayList<>();

        if (opiqProperties.getApiKeyName() != null) {
            responseMaterials.addAll(externalMaterialRepository.getOpiqMaterials(searchString, subjects, limit));

            if (responseMaterials.isEmpty()) {
                responseMaterials.addAll(externalMaterialRepository.getOpiqMaterials(searchString, null, limit));
            }
        }

        return responseMaterials;
    }
}
