package com.schoolaby.service;

import com.schoolaby.domain.LtiResource;
import com.schoolaby.service.dto.lti.LtiResourceDTO;
import com.schoolaby.repository.LtiResourceRepository;
import com.schoolaby.service.mapper.LtiResourceMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@Transactional
@RequiredArgsConstructor
public class LtiResourceService {
    private final LtiResourceRepository ltiResourceRepository;
    private final LtiResourceMapper mapper;

    @Transactional(readOnly = true)
    public LtiResource getOne(Long id) {
        return ltiResourceRepository.getOne(id);
    }

    @Transactional(readOnly = true)
    public LtiResource getOneByResourceLinkId(String resourceLinkId) {
        return ltiResourceRepository.getOneByResourceLinkId(resourceLinkId);
    }

    @Transactional(readOnly = true)
    public List<LtiResourceDTO> getByIds(List<Long> ids) {
        return ltiResourceRepository.getByIds(ids).stream()
            .map(mapper::toDto)
            .collect(toList());
    }

    @Transactional(readOnly = true)
    public List<LtiResourceDTO> findAll(Long milestoneId, Long assignmentId) {
        return ltiResourceRepository.findAll(milestoneId, assignmentId).stream()
            .map(mapper::toDto)
            .collect(toList());
    }
}
