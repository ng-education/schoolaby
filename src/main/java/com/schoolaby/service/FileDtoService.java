package com.schoolaby.service;

import com.schoolaby.domain.UploadedFile;
import com.schoolaby.service.dto.UploadedFileDTO;
import com.schoolaby.service.mapper.UploadedFileMapper;
import com.schoolaby.service.storage.FileDetails;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ws.schild.jave.EncoderException;

import java.io.IOException;

import static com.schoolaby.common.FileUtil.validateIsUserTheCreatorOfTheFile;

@Service
@RequiredArgsConstructor
public class FileDtoService {
    private final FileService service;
    private final UploadedFileService uploadedFileService;
    private final UploadedFileMapper mapper;

    public FileDetails getDetails(String uniqueFileName) throws IOException {
        return service.getDetails(uniqueFileName);
    }

    public UploadedFileDTO upload(MultipartFile multipartFile) throws IOException, EncoderException {
        return mapper.toDto(service.upload(multipartFile));
    }

    public void delete(String uniqueFileName) throws IOException {
        UploadedFile uploadedFile = uploadedFileService.findByUniqueName(uniqueFileName);
        validateIsUserTheCreatorOfTheFile(uploadedFile);
        service.delete(uniqueFileName);
    }
}
