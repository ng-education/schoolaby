package com.schoolaby.service;

public class GradeAlreadyExistsException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public GradeAlreadyExistsException(String exception) {
        super(exception);
    }
}
