package com.schoolaby.service;

import com.schoolaby.repository.SubjectJpaRepository;
import com.schoolaby.service.dto.SubjectDTO;
import com.schoolaby.service.mapper.SubjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;

import static com.schoolaby.domain.enumeration.Country.*;

@Service
@RequiredArgsConstructor
@Transactional
public class SubjectService {
    private final SubjectMapper subjectMapper;
    private final SubjectJpaRepository subjectJpaRepository;

    public Collection<SubjectDTO> findAllByCountry(String country) {
        return subjectMapper.toDto(
            (country.equals(TANZANIA.getValue()) || country.equals(UKRAINE.getValue()))
                ? subjectJpaRepository.findAllByCountry(country)
                : subjectJpaRepository.findAllByCountry(DEFAULT_COUNTRY.getValue())
        );
    }
}
