package com.schoolaby.service;

import com.schoolaby.domain.UploadedFile;
import com.schoolaby.repository.UploadedFileRepository;
import com.schoolaby.service.dto.UploadedFileDTO;
import com.schoolaby.service.mapper.UploadedFileMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;

import static com.schoolaby.common.FileUtil.validateIsUserTheCreatorOfTheFile;

@Service
@Transactional
public class UploadedFileService {
    private final Logger log = LoggerFactory.getLogger(UploadedFileService.class);

    private final UploadedFileRepository uploadedFileRepository;

    private final UploadedFileMapper uploadedFileMapper;

    public UploadedFileService(UploadedFileRepository uploadedFileRepository, UploadedFileMapper uploadedFileMapper) {
        this.uploadedFileRepository = uploadedFileRepository;
        this.uploadedFileMapper = uploadedFileMapper;
    }

    public UploadedFileDTO save(UploadedFileDTO uploadedFileDTO) {
        log.debug("Request to save UploadedFile : {}", uploadedFileDTO);
        UploadedFile uploadedFile = uploadedFileMapper.toEntity(uploadedFileDTO);
        uploadedFile = uploadedFileRepository.save(uploadedFile);
        return uploadedFileMapper.toDto(uploadedFile);
    }

    @Transactional(readOnly = true)
    public Page<UploadedFileDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UploadedFiles");
        return uploadedFileRepository.findAll(pageable).map(uploadedFileMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Optional<UploadedFileDTO> findOne(Long id) {
        log.debug("Request to get UploadedFile : {}", id);
        return uploadedFileRepository.findById(id).map(uploadedFileMapper::toDto);
    }

    public UploadedFile findByUniqueName(String uniqueName) {
        log.debug("Request to get UploadedFile by uniqueName: {}", uniqueName);
        return uploadedFileRepository.findByUniqueName(uniqueName).orElseThrow(EntityNotFoundException::new);
    }

    public void delete(Long id) {
        log.debug("Request to delete UploadedFile : {}", id);
        UploadedFile uploadedFile = uploadedFileRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        validateIsUserTheCreatorOfTheFile(uploadedFile);
        uploadedFileRepository.deleteById(id);
    }
}
