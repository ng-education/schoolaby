package com.schoolaby.service.mapper;

import com.schoolaby.domain.Milestone;
import com.schoolaby.service.dto.MilestoneDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(uses = {MaterialMapper.class, EducationalAlignmentMapper.class, JourneyMapper.class, UserMapper.class, AssignmentMapper.class})
public interface MilestoneMapper extends EntityMapper<MilestoneDTO, Milestone> {
    @Mapping(source = "journey.id", target = "journeyId")
    @Mapping(source = "creator.id", target = "creatorId")
    MilestoneDTO toDto(Milestone milestone);

    @Mapping(source = "journeyId", target = "journey")
    @Mapping(source = "creatorId", target = "creator")
    Milestone toEntity(MilestoneDTO milestoneDTO);

    default Milestone fromId(Long id) {
        if (id == null) {
            return null;
        }
        return new Milestone().id(id);
    }
}
