package com.schoolaby.service.mapper;

import com.schoolaby.domain.UploadedFile;
import com.schoolaby.service.dto.UploadedFileDTO;
import org.mapstruct.Mapper;

@Mapper(uses = {SubmissionMapper.class})
public interface UploadedFileMapper extends EntityMapper<UploadedFileDTO, UploadedFile> {
    UploadedFileDTO toDto(UploadedFile uploadedFile);

    UploadedFile toEntity(UploadedFileDTO uploadedFileDTO);

    default UploadedFile fromId(Long id) {
        if (id == null) {
            return null;
        }
        UploadedFile uploadedFile = new UploadedFile();
        uploadedFile.setId(id);
        return uploadedFile;
    }
}
