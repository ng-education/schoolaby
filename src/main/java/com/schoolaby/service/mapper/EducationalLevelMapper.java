package com.schoolaby.service.mapper;

import com.schoolaby.domain.EducationalLevel;
import com.schoolaby.service.dto.EducationalLevelDTO;
import org.mapstruct.Mapper;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.TreeSet;
import java.util.stream.Collectors;

@Mapper
public interface EducationalLevelMapper extends EntityMapper<EducationalLevelDTO, EducationalLevel> {
    EducationalLevelDTO toDto(EducationalLevel educationalLevel);

    EducationalLevel toEntity(EducationalLevelDTO ltiConfigDTO);

    @Override
    default Collection<EducationalLevelDTO> toDto(Collection<EducationalLevel> entityCollection) {
        return entityCollection.stream()
            .map(this::toDto)
            .sorted(Comparator.comparing(EducationalLevelDTO::getId))
            .collect(Collectors.toList());
    };
}
