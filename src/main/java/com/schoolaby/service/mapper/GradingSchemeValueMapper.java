package com.schoolaby.service.mapper;

import com.schoolaby.domain.GradingSchemeValue;
import com.schoolaby.service.dto.GradingSchemeValueDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(uses = {GradingSchemeMapper.class})
public interface GradingSchemeValueMapper extends EntityMapper<GradingSchemeValueDTO, GradingSchemeValue> {
    @Mapping(source = "gradingScheme.id", target = "gradingSchemeId")
    @Mapping(source = "percentageRange", target = "percentageRangeStart")
    GradingSchemeValueDTO toDto(GradingSchemeValue gradingSchemeValue);

    @Mapping(source = "gradingSchemeId", target = "gradingScheme")
    @Mapping(source = "percentageRangeStart", target = "percentageRange")
    GradingSchemeValue toEntity(GradingSchemeValueDTO gradingSchemeValueDTO);

    default GradingSchemeValue fromId(Long id) {
        if (id == null) {
            return null;
        }
        GradingSchemeValue gradingSchemeValue = new GradingSchemeValue();
        gradingSchemeValue.setId(id);
        return gradingSchemeValue;
    }
}
