package com.schoolaby.service.mapper;

import com.schoolaby.domain.LtiLaunch;
import com.schoolaby.service.dto.LtiLaunchDTO;
import org.mapstruct.Mapper;

@Mapper
public interface LtiLaunchMapper extends EntityMapper<LtiLaunchDTO, LtiLaunch> {
    LtiLaunchDTO toDto(LtiLaunch ltiLaunch);
}
