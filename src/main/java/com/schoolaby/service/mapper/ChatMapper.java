package com.schoolaby.service.mapper;

import com.schoolaby.domain.Chat;
import com.schoolaby.service.dto.ChatDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(uses = {UserMapper.class, SubmissionMapper.class, MessageMapper.class, JourneyMapper.class})
public interface ChatMapper extends EntityMapper<ChatDTO, Chat> {
    @Mapping(source = "submission.id", target = "submissionId")
    @Mapping(source = "journey.id", target = "journeyId")
    ChatDTO toDto(Chat chat);

    @Mapping(source = "submissionId", target = "submission")
    @Mapping(source = "journeyId", target = "journey")
    Chat toEntity(ChatDTO chatDTO);

    default Chat fromId(Long id) {
        if (id == null) {
            return null;
        }
        Chat chat = new Chat();
        chat.setId(id);
        return chat;
    }
}
