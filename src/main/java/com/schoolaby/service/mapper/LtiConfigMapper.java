package com.schoolaby.service.mapper;

import com.schoolaby.domain.LtiConfig;
import com.schoolaby.service.dto.LtiConfigDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(uses = {JourneyMapper.class, LtiAppMapper.class})
public interface LtiConfigMapper extends EntityMapper<LtiConfigDTO, LtiConfig> {
    @Mapping(source = "journey.id", target = "journeyId")
    LtiConfigDTO toDto(LtiConfig ltiConfig);

    @Mapping(source = "journeyId", target = "journey")
    LtiConfig toEntity(LtiConfigDTO ltiConfigDTO);
}
