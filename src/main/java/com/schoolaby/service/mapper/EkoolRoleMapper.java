package com.schoolaby.service.mapper;

import com.schoolaby.domain.PersonRole;
import com.schoolaby.security.Role;
import com.schoolaby.service.dto.EkoolRolesDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper
public interface EkoolRoleMapper extends EntityMapper<EkoolRolesDTO.EkoolRoleDTO, PersonRole> {
    @Override
    @Mapping(source = "roleName", target = "role", qualifiedByName = "mapRole")
    @Mapping(constant = "false", target = "active")
    PersonRole toEntity(EkoolRolesDTO.EkoolRoleDTO dto);

    @Override
    @Mapping(source = "role", target = "roleName", qualifiedByName = "mapRoleName")
    EkoolRolesDTO.EkoolRoleDTO toDto(PersonRole entity);

    @Named("mapRole")
    default Role mapRole(String roleName) {
        return Role.findEkoolRole(roleName).orElse(null);
    }

    @Named("mapRoleName")
    default String mapRoleName(Role role) {
        return role.getEkoolMarker();
    }
}
