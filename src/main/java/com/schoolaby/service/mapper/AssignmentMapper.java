package com.schoolaby.service.mapper;

import com.schoolaby.domain.Assignment;
import com.schoolaby.domain.Submission;
import com.schoolaby.domain.User;
import com.schoolaby.service.dto.AssignmentDTO;
import com.schoolaby.service.dto.GroupAssignmentDTO;
import org.mapstruct.*;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(uses = {GroupMapper.class, AssignmentMaterialMapper.class, UserMapper.class, EducationalAlignmentMapper.class, GradingSchemeMapper.class, MilestoneMapper.class, JourneyMapper.class, LtiResourceMapper.class, RubricMapper.class, CriterionMapper.class, CriterionLevelMapper.class, SubjectMapper.class})
public interface AssignmentMapper extends EntityMapper<AssignmentDTO, Assignment> {
    @Mapping(source = "gradingScheme.id", target = "gradingSchemeId")
    @Mapping(source = "milestone.id", target = "milestoneId")
    @Mapping(source = "milestone.journey.id", target = "journeyId")
    @Mapping(source = "creator.id", target = "creatorId")
    @Mapping(source = "milestone.journey.creator", target = "teacherName", qualifiedByName = "mapAssignmentTeacherName")
    @Mapping(source = "milestone.journey.subject", target = "subject")
    @Mapping(source = "submissions", target = "hasGrades", qualifiedByName = "mapHasGrades")
    @Mapping(source = "assignmentMaterials", target = "materials")
    AssignmentDTO toDto(Assignment assignment);

    @Named("mapWithoutMaterials")
    @Mapping(source = "gradingScheme.id", target = "gradingSchemeId")
    @Mapping(source = "milestone.id", target = "milestoneId")
    @Mapping(source = "milestone.journey.id", target = "journeyId")
    @Mapping(source = "creator.id", target = "creatorId")
    @Mapping(source = "milestone.journey.creator", target = "teacherName", qualifiedByName = "mapAssignmentTeacherName")
    @Mapping(source = "milestone.journey.subject", target = "subject")
    @Mapping(source = "submissions", target = "hasGrades", qualifiedByName = "mapHasGrades")
    @Mapping(source = "assignmentMaterials", target = "materials", ignore = true)
    AssignmentDTO toDtoWithoutMaterials(Assignment assignment);

    GroupAssignmentDTO toGroupAssignmentDto(Assignment assignment);
    default Collection<GroupAssignmentDTO> toGroupAssignmentDto(Collection<Assignment> assignments) {
        return assignments.stream().map(this::toGroupAssignmentDto).collect(Collectors.toList());
    }

    @Mapping(source = "gradingSchemeId", target = "gradingScheme")
    @Mapping(source = "milestoneId", target = "milestone")
    @Mapping(source = "creatorId", target = "creator")
    @Mapping(source = "materials", target = "assignmentMaterials")
    Assignment toEntity(AssignmentDTO assignmentDTO);

    @AfterMapping
    default void mapAssignmentId(@MappingTarget Assignment assignment) {
        assignment.getAssignmentMaterials().forEach(material -> material.setAssignment(assignment));
    }

    @Named("mapAssignmentTeacherName")
    default String mapAssignmentTeacherName(User creator) {
        if (creator != null) {
            return String.format("%s %s", creator.getFirstName(), creator.getLastName());
        }
        return null;
    }

    @Named("mapHasGrades")
    default boolean mapHasGrades(Set<Submission> submissions) {
        return submissions.stream().anyMatch(Submission::hasGrades);
    }

    default Assignment fromId(Long id) {
        if (id == null) {
            return null;
        }
        return new Assignment().id(id);
    }
}
