package com.schoolaby.service.mapper;

import com.schoolaby.domain.Criterion;
import com.schoolaby.domain.Rubric;
import com.schoolaby.service.dto.CriterionDTO;
import com.schoolaby.service.dto.RubricDTO;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(uses = {CriterionMapper.class, CriterionLevelMapper.class, AssignmentMapper.class})
public interface RubricMapper extends EntityMapper<RubricDTO, Rubric> {
    CriterionMapper criterionMapper = Mappers.getMapper(CriterionMapper.class);

    @Mapping(source = "assignment.id", target = "assignmentId")
    RubricDTO toDto(Rubric rubric);

    default Set<CriterionDTO> setCriterions (Set<Criterion> criterions) {
        return criterions.stream().map(criterionMapper::toDto)
            .sorted(Comparator.comparingLong(CriterionDTO::getSequenceNumber))
            .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    @AfterMapping
    static void calculateMinPoints(@MappingTarget RubricDTO rubricDTO) {
        long minPoints = 0;
        long maxPoints = 0;

        for (CriterionDTO criterion : rubricDTO.getCriterions()) {
            maxPoints += criterion.getMaxPoints();
            minPoints += criterion.getMinPoints();
        }

        rubricDTO.setMinPoints(minPoints);
        rubricDTO.setMaxPoints(maxPoints);
    }
}
