package com.schoolaby.service.mapper;

import com.google.common.collect.Streams;
import com.schoolaby.domain.GradingScheme;
import com.schoolaby.domain.GradingSchemeValue;
import com.schoolaby.service.dto.GradingSchemeDTO;
import com.schoolaby.service.dto.GradingSchemeValueDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import static java.util.Comparator.comparingInt;
import static java.util.stream.Collectors.toList;

@Mapper(uses = GradingSchemeValueMapper.class)
public interface GradingSchemeMapper extends EntityMapper<GradingSchemeDTO, GradingScheme> {
    @Mapping(source = "values", target = "values", qualifiedByName = "mapGradingSchemeValues")
    GradingSchemeDTO toDto(GradingScheme gradingSchemeDTO);

    default GradingScheme fromId(Long id) {
        if (id == null) {
            return null;
        }
        GradingScheme gradingScheme = new GradingScheme();
        gradingScheme.setId(id);
        return gradingScheme;
    }

    @Named("mapGradingSchemeValues")
    default Set<GradingSchemeValueDTO> mapGradingSchemeValues(Set<GradingSchemeValue> values) {
        if (values != null && !values.isEmpty()) {
            GradingSchemeValueMapper gradingSchemeValueMapper = Mappers.getMapper(GradingSchemeValueMapper.class);

            List<GradingSchemeValue> sorted = values.stream()
                .sorted(comparingInt(GradingSchemeValue::getPercentageRange)).collect(toList());

            List<GradingSchemeValueDTO> mappedValues = Streams.zip(
                sorted.stream(),
                sorted.stream().skip(1),
                (current, next) -> gradingSchemeValueMapper.toDto(current)
                    .setPercentageRangeStart(current.getPercentageRange())
                    .setPercentageRangeEnd(next.getPercentageRange())
            ).collect(toList());

            GradingSchemeValue lastGrade = sorted.get(sorted.size() - 1);
            mappedValues.add(
                gradingSchemeValueMapper.toDto(lastGrade)
                    .setPercentageRangeStart(lastGrade.getPercentageRange() != null ? lastGrade.getPercentageRange() : 0)
                    .setPercentageRangeEnd(100)
            );

            return new LinkedHashSet<>(mappedValues);
        }

        return new LinkedHashSet<>();
    }
}
