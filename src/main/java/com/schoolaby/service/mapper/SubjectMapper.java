package com.schoolaby.service.mapper;

import com.schoolaby.domain.Subject;
import com.schoolaby.service.dto.SubjectDTO;
import org.mapstruct.Mapper;

@Mapper
public interface SubjectMapper extends EntityMapper<SubjectDTO, Subject> {
}
