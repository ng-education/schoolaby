package com.schoolaby.service.mapper;

import com.schoolaby.domain.PersonRole;
import com.schoolaby.security.Role;
import com.schoolaby.security.harid.HarIdUser;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Mapper
public interface HarIdRoleMapper extends EntityMapper<HarIdUser.Role, PersonRole> {

    @Mapping(target = "startDate", qualifiedByName = "localDate_to_localDateTime")
    @Mapping(target = "endDate", qualifiedByName = "localDate_to_localDateTime")
    @Mapping(source = "marker", target = "role", qualifiedByName = "mapRole")
    PersonRole toEntity(HarIdUser.Role dto);

    @Mapping(target = "startDate", qualifiedByName = "localDateTime_to_localDate")
    @Mapping(target = "endDate", qualifiedByName = "localDateTime_to_localDate")
    @Mapping(source = "role", target = "marker", qualifiedByName = "mapMarker")
    HarIdUser.Role toDto(PersonRole personRole);

    @Named("mapRole")
    default Role mapRole(String roleName) {
        return Role.findHaridRole(roleName).orElse(null);
    }

    @Named("mapMarker")
    default String mapMarker(Role role) {
        return role.getHarIdMarker();
    }

    @Named("localDate_to_localDateTime")
    static LocalDateTime localDateToLocalDateTime(LocalDate localDate) {
        return localDate == null ? null : LocalDateTime.of(localDate, LocalTime.MIN);
    }

    @Named("localDateTime_to_localDate")
    static LocalDate localDateTimeToLocalDate(LocalDateTime localDateTime) {
        return localDateTime == null ? null : localDateTime.toLocalDate();
    }
}
