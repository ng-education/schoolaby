package com.schoolaby.service.mapper;

import com.schoolaby.domain.JourneyStudent;
import com.schoolaby.service.dto.JourneyStudentDTO;
import org.mapstruct.Mapper;

@Mapper(uses = {UserMapper.class})
public interface JourneyStudentMapper extends EntityMapper<JourneyStudentDTO, JourneyStudent> {
}
