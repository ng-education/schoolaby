package com.schoolaby.service.mapper;

import com.schoolaby.domain.PersonRole;
import com.schoolaby.service.dto.PersonRoleDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(uses = {UserMapper.class, SchoolMapper.class})
public interface PersonRoleMapper extends EntityMapper<PersonRoleDTO, PersonRole> {
    @Mapping(source = "person.id", target = "personId")
    PersonRoleDTO toDto(PersonRole personRole);

    @Mapping(source = "personId", target = "person")
    PersonRole toEntity(PersonRoleDTO personRoleDTO);

    default PersonRole fromId(Long id) {
        if (id == null) {
            return null;
        }
        PersonRole personRole = new PersonRole();
        personRole.setId(id);
        return personRole;
    }
}
