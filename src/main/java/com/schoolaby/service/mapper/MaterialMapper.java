package com.schoolaby.service.mapper;

import com.schoolaby.domain.Material;
import com.schoolaby.service.dto.MaterialDTO;
import org.mapstruct.Mapper;

@Mapper(uses = {EducationalAlignmentMapper.class})
public interface MaterialMapper extends EntityMapper<MaterialDTO, Material> {
    Material toEntity(MaterialDTO materialDTO);

    default Material fromId(Long id) {
        if (id == null) {
            return null;
        }
        Material material = new Material();
        material.setId(id);
        return material;
    }
}
