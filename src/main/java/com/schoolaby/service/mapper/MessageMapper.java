package com.schoolaby.service.mapper;

import com.schoolaby.domain.Message;
import com.schoolaby.domain.MessageRecipient;
import com.schoolaby.domain.User;
import com.schoolaby.service.dto.MessageDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.time.Instant;
import java.util.Optional;
import java.util.Set;

import static com.schoolaby.security.SecurityUtils.getCurrentUserId;
import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.capitalize;

@Mapper(uses = {UserMapper.class, ChatMapper.class})
public interface MessageMapper extends EntityMapper<MessageDTO, Message> {
    @Mapping(source = "creator.id", target = "creatorId")
    @Mapping(source = "chat.id", target = "chatId")
    @Mapping(source = "messageRecipients", target = "read", qualifiedByName = "mapRead")
    @Mapping(source = "creator", target = "creator", qualifiedByName = "mapCreator")
    MessageDTO toDto(Message message);

    @Mapping(source = "creatorId", target = "creator")
    @Mapping(source = "chatId", target = "chat")
    Message toEntity(MessageDTO messageDTO);

    @Named("mapRead")
    default Instant mapRead(Set<MessageRecipient> messageRecipients) {
        if (messageRecipients != null && !messageRecipients.isEmpty()) {
            Optional<MessageRecipient> messageSeen = messageRecipients.stream().filter(m -> m.getPerson().getId().equals(getCurrentUserId())).findFirst();
            return messageSeen.map(MessageRecipient::getRead).orElse(null);
        } else {
            return null;
        }
    }

    @Named("mapCreator")
    default String mapCreator(User creator) {
        return creator != null ? format("%s %s", capitalize(creator.getFirstName()), capitalize(creator.getLastName())) : "";
    }

    default Message fromId(Long id) {
        if (id == null) {
            return null;
        }
        Message message = new Message();
        message.setId(id);
        return message;
    }
}
