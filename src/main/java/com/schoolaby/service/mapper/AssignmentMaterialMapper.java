package com.schoolaby.service.mapper;

import com.schoolaby.domain.AssignmentMaterial;
import com.schoolaby.service.dto.MaterialDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface AssignmentMaterialMapper extends EntityMapper<MaterialDTO, AssignmentMaterial>{
    @Mapping(source = "sequenceNumber", target = "sequenceNumber")
    @Mapping(source = ".", target = "material")
    AssignmentMaterial toEntity(MaterialDTO materialDTO);

    @Mapping(source = "sequenceNumber", target = "sequenceNumber")
    @Mapping(source = "material", target= ".")
    MaterialDTO toDto(AssignmentMaterial assignmentMaterial);
}
