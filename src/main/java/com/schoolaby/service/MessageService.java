package com.schoolaby.service;

import com.schoolaby.domain.Chat;
import com.schoolaby.domain.Journey;
import com.schoolaby.domain.Message;
import com.schoolaby.domain.Submission;
import com.schoolaby.repository.MessageRepository;
import com.schoolaby.service.dto.MessageDTO;
import com.schoolaby.service.dto.MessageViewDTO;
import com.schoolaby.service.mapper.MessageMapper;
import com.schoolaby.service.mapper.SubjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.schoolaby.security.SecurityUtils.getCurrentUserId;
import static com.schoolaby.web.rest.ChatController.URL_CHATS;

@Service
@Transactional
public class MessageService {
    private final Logger log = LoggerFactory.getLogger(MessageService.class);

    private final String USER_IS_NOT_THE_CREATOR_OF_THE_MESSAGE = "User is not the creator of the message!";

    private final MessageRepository messageRepository;
    private final MessageMapper messageMapper;
    private final NotificationService notificationService;
    private final ChatService chatService;
    private final SubjectMapper subjectMapper;

    public MessageService(MessageRepository messageRepository, MessageMapper messageMapper, NotificationService notificationService, ChatService chatService, SubjectMapper subjectMapper) {
        this.messageRepository = messageRepository;
        this.messageMapper = messageMapper;
        this.notificationService = notificationService;
        this.chatService = chatService;
        this.subjectMapper = subjectMapper;
    }

    public MessageDTO save(MessageDTO messageDTO) {
        log.debug("Request to save Message : {}", messageDTO);
        Message message = messageMapper.toEntity(messageDTO);
        message = messageRepository.save(message);
        Chat chat = chatService.getOne(message.getChat().getId());
        notificationService.createNotificationsForMessage(chat, message);
        return messageMapper.toDto(message);
    }

    public MessageDTO update(MessageDTO messageDTO) {
        log.debug("Request to update Message : {}", messageDTO);
        Message message = getOne(messageDTO.getId());
        validateCurrentUserIsMessageCreator(message);
        Chat chat = chatService.getOne(message.getChat().getId());
        notificationService.createNotificationsForMessage(chat, message);
        return messageMapper.toDto(messageRepository.save(messageMapper.toEntity(messageDTO)));
    }

    @Transactional(readOnly = true)
    public Page<MessageDTO> findAll(Pageable pageable, Long submissionId, Long journeyId, Boolean feedback) {
        log.debug("Request to get all Messages");
        Page<Message> messages;
        if (submissionId != null) {
            messages = messageRepository.findAllByChatSubmissionId(submissionId, pageable, feedback);
        } else if (journeyId != null) {
            messages = messageRepository.findAllByChatJourneyId(journeyId, pageable);
        } else {
            messages = messageRepository.findAll(pageable);
        }
        return messages.map(messageMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<MessageViewDTO> findAllMessageViews(Pageable pageable) {
        log.debug("Request to get all MessageViews");
        List<MessageViewDTO> messageViews = new ArrayList<>();
        messageRepository.findLatestMessagesInChats(pageable).forEach(message -> {
            Journey journey = null;
            MessageViewDTO messageViewDTO = new MessageViewDTO();
            if (message.getChat().getSubmission() != null) {
                Submission submission = message.getChat().getSubmission();
                journey = message.getChat().getJourney();
                messageViewDTO.urlToChat(URL_CHATS + "?journeyId=" + journey.getId() + "&submissionId=" + submission.getId())
                    .assignmentName(submission.getAssignment().getTitle());
            } else if (message.getChat().getJourney() != null) {
                journey = message.getChat().getJourney();
                messageViewDTO.urlToChat(URL_CHATS + "?journeyId=" + journey.getId());
            }
            if (journey != null) {
                messageViewDTO.messageCreatedDate(message.getCreatedDate())
                    .messageValue(message.getValue())
                    .authorName(message.getCreator().getFirstName() + " " + message.getCreator().getLastName())
                    .journeyName(journey.getTitle())
                    .subject(subjectMapper.toDto(journey.getSubject()))
                    .read(message.getReadTimestamp(getCurrentUserId()));
                messageViews.add(messageViewDTO);
            }
        });
        return new PageImpl<>(messageViews, pageable, messageViews.size());
    }

    @Transactional(readOnly = true)
    public Optional<MessageDTO> findOne(Long id) {
        log.debug("Request to get Message : {}", id);
        return messageRepository.findOne(id).map(messageMapper::toDto);
    }

    public void delete(Long id) {
        log.debug("Request to delete Message : {}", id);
        Message message = getOne(id);
        validateCurrentUserIsMessageCreator(message);
        messageRepository.deleteById(id);
    }

    Message getOne(Long id) {
        return messageRepository.findOne(id).orElseThrow(EntityNotFoundException::new);
    }

    private void validateCurrentUserIsMessageCreator(Message message) {
        if (!message.getCreator().getId().equals(getCurrentUserId())) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, USER_IS_NOT_THE_CREATOR_OF_THE_MESSAGE);
        }
    }
}
