package com.schoolaby.service;

import com.schoolaby.config.audit.AuditEventConverter;
import com.schoolaby.repository.AuditEventRepository;
import io.github.jhipster.config.JHipsterProperties;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Service for managing audit events.
 * <p>
 * This is the default implementation to support SpringBoot Actuator {@code AuditEventRepository}.
 */
@Service
@Transactional
@RequiredArgsConstructor
public class AuditEventService {
    private final Logger log = LoggerFactory.getLogger(AuditEventService.class);

    private final JHipsterProperties jHipsterProperties;
    private final AuditEventRepository auditEventRepository;
    private final AuditEventConverter auditEventConverter;
    private final JourneyService journeyService;

    /**
     * Old audit events should be automatically deleted after 180 days.
     *
     * This is scheduled to get fired at 12:00 (am).
     */
    @Scheduled(cron = "0 0 12 * * ?")
    public void removeOldAuditEvents() {
        auditEventRepository
            .findByAuditEventDateBefore(Instant.now().minus(jHipsterProperties.getAuditEvents().getRetentionPeriod(), ChronoUnit.DAYS))
            .forEach(
                auditEvent -> {
                    log.debug("Deleting audit data {}", auditEvent);
                    auditEventRepository.deleteById(auditEvent.getId());
                }
            );
    }

    @Transactional(readOnly = true)
    public Page<AuditEvent> findAll(Pageable pageable, Long journeyId) {
        if (journeyId == null) {
            return auditEventRepository.findAll(pageable).map(auditEventConverter::convertToAuditEvent);
        } else {
            Set<Long> studentIds = journeyService
                .getStudents(journeyId)
                .stream()
                .map(journeyStudentDTO -> journeyStudentDTO
                    .getUser()
                    .getId())
                .collect(Collectors.toSet());
            return auditEventRepository.findLastLoginsByIds(studentIds, pageable).map(auditEventConverter::convertToAuditEvent);
        }
    }

    @Transactional(readOnly = true)
    public Page<AuditEvent> findByDates(Instant fromDate, Instant toDate, Pageable pageable) {
        return auditEventRepository
            .findAllByAuditEventDateBetween(fromDate, toDate, pageable)
            .map(auditEventConverter::convertToAuditEvent);
    }

    @Transactional(readOnly = true)
    public Optional<AuditEvent> find(Long id) {
        return auditEventRepository.findById(id).map(auditEventConverter::convertToAuditEvent);
    }
}
