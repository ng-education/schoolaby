package com.schoolaby.service;

import com.schoolaby.repository.EkoolRepository;
import com.schoolaby.service.dto.EkoolRolesDTO.EkoolRoleDTO;
import com.schoolaby.service.dto.EkoolSchoolsDTO.EkoolSchoolDTO;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class EkoolService {
    public static final String EKOOL_CLIENT_REGISTRATION_ID = "ekool";

    private final EkoolRepository ekoolRepository;
    private final OAuth2AuthorizedClientService auth2AuthorizedClientService;

    public EkoolService(EkoolRepository ekoolRepository, OAuth2AuthorizedClientService auth2AuthorizedClientService) {
        this.ekoolRepository = ekoolRepository;
        this.auth2AuthorizedClientService = auth2AuthorizedClientService;
    }

    public Set<EkoolSchoolDTO> getSchools(String externalId) {
        OAuth2AuthorizedClient ekool = auth2AuthorizedClientService.loadAuthorizedClient(EKOOL_CLIENT_REGISTRATION_ID, externalId);
        return ekoolRepository.getUserSchools(ekool.getAccessToken()).getData();
    }

    public Set<EkoolRoleDTO> getRoles(String externalId) {
        OAuth2AuthorizedClient ekool = auth2AuthorizedClientService.loadAuthorizedClient(EKOOL_CLIENT_REGISTRATION_ID, externalId);
        return ekoolRepository.getUserRoles(ekool.getAccessToken()).getData();
    }

    public String getEmail(String externalId) {
        OAuth2AuthorizedClient ekool = auth2AuthorizedClientService.loadAuthorizedClient(EKOOL_CLIENT_REGISTRATION_ID, externalId);
        return ekoolRepository.getUserBasic(ekool.getAccessToken()).getData().getEmail();
    }
}
