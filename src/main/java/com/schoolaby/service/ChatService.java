package com.schoolaby.service;

import com.schoolaby.domain.Chat;
import com.schoolaby.domain.Journey;
import com.schoolaby.domain.enumeration.EntityType;
import com.schoolaby.repository.ChatRepository;
import com.schoolaby.service.dto.ChatDTO;
import com.schoolaby.service.mapper.ChatMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;

import static com.google.common.collect.Sets.union;
import static com.schoolaby.domain.enumeration.EntityType.JOURNEY;
import static com.schoolaby.domain.enumeration.EntityType.SUBMISSION;
import static org.springframework.data.domain.Page.empty;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class ChatService {
    private final ChatMapper chatMapper;
    private final ChatRepository chatRepository;
    private final JourneyService journeyService;


    private ChatDTO save(Chat chat) {
        log.debug("Request to save Chat : {}", chat);
        chat = chatRepository.save(chat);
        return chatMapper.toDto(chat);
    }

    public ChatDTO create(ChatDTO chatDTO) {
        Chat chat = chatMapper.toEntity(chatDTO);
        if (chatDTO.getPeople() == null || chatDTO.getPeople().isEmpty()) {
            Journey journey = journeyService.getOne(chatDTO.getJourneyId(), false);

            if (journey != null) {
                chat.setPeople(union(journey.getJourneyStudentsUsers(), journey.getJourneyTeachersUsers()));
            }
        }
        return save(chat);
    }

    @Transactional(readOnly = true)
    public Page<ChatDTO> getAllChats(Pageable pageable, EntityType entity, Long journeyId, Long submissionId) {
        if (entity != null && journeyId != null && submissionId != null) {
            throw new ResponseStatusException(BAD_REQUEST, "Cannot use all parameters at once");
        }
        if (entity != null && journeyId == null || entity == null && journeyId != null) {
            throw new ResponseStatusException(BAD_REQUEST, "JourneyId and Entity cannot be used separately");
        }

        Page<Chat> chats = empty();
        if (journeyId != null) {
            if (JOURNEY.equals(entity)) {
                chats = chatRepository.findAllJourneyChats(pageable, journeyId);
            } else if (SUBMISSION.equals(entity)) {
                chats = chatRepository.findAllSubmissionChatsByJourney(pageable, journeyId);
            }
        } else if (submissionId != null) {
            chats = chatRepository.findAllSubmissionChats(pageable, submissionId);
        }
        return chats.map(chatMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Chat getOne(Long id) {
        return chatRepository.findOne(id).orElseThrow(EntityNotFoundException::new);
    }
}
