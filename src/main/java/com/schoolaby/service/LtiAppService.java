package com.schoolaby.service;

import com.schoolaby.domain.LtiApp;
import com.schoolaby.repository.LtiAppRepository;
import com.schoolaby.service.dto.LtiAppDTO;
import com.schoolaby.service.mapper.LtiAppMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@Service
@Transactional
public class LtiAppService {
    private final LtiAppRepository repository;
    private final LtiAppMapper appMapper;

    public LtiAppService(LtiAppRepository repository, LtiAppMapper appMapper) {
        this.repository = repository;
        this.appMapper = appMapper;
    }

    public List<LtiAppDTO> findAll(Long journeyId) {
        if (journeyId != null) {
            return repository.findAllByConfigsJourneyId(journeyId)
                .stream()
                .map(appMapper::toDto)
                .collect(toList());
        }
        return repository.findAll().stream()
            .map(appMapper::toDto)
            .collect(toList());
    }

    public LtiApp get(long id) {
        return repository.getOne(id);
    }

    public Optional<LtiApp> findByDeepLinkingUri(String redirectUri) {
        return repository.findByDeepLinkingUrl(redirectUri);
    }

    public Optional<LtiApp> findByClientId(String clientId) {
        return repository.findByClientId(clientId);
    }
}
