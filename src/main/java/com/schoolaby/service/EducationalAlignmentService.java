package com.schoolaby.service;

import com.schoolaby.domain.EducationalAlignment;
import com.schoolaby.repository.EducationalAlignmentRepository;
import com.schoolaby.service.dto.EducationalAlignmentDTO;
import com.schoolaby.service.mapper.EducationalAlignmentMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static java.lang.String.format;

/**
 * Service Implementation for managing {@link EducationalAlignment}.
 */
@Service
@Transactional
public class EducationalAlignmentService {
    private final Logger log = LoggerFactory.getLogger(EducationalAlignmentService.class);

    private final EducationalAlignmentRepository repository;

    private final EducationalAlignmentMapper educationalAlignmentMapper;

    public EducationalAlignmentService(
        EducationalAlignmentMapper educationalAlignmentMapper,
        EducationalAlignmentRepository repository
    ) {
        this.educationalAlignmentMapper = educationalAlignmentMapper;
        this.repository = repository;
    }

    /**
     * Save a educationalAlignment.
     *
     * @param educationalAlignmentDTO the entity to save.
     * @return the persisted entity.
     */

    public EducationalAlignmentDTO create(EducationalAlignmentDTO educationalAlignmentDTO) {
        log.debug("Request to create EducationalAlignment : {}", educationalAlignmentDTO);

        return educationalAlignmentMapper.toDto(
            repository.save(educationalAlignmentMapper.toEntity(educationalAlignmentDTO))
        );
    }

    public EducationalAlignmentDTO update(EducationalAlignmentDTO educationalAlignmentDTO) {
        log.debug("Request to update EducationalAlignment : {}", educationalAlignmentDTO);

        if (!repository.existsById(educationalAlignmentDTO.getId())) {
            throw new RuntimeException(format("EducationalAlignment '%s' not found!", educationalAlignmentDTO.getId()));
        }

        return educationalAlignmentMapper.toDto(
            repository.save(educationalAlignmentMapper.toEntity(educationalAlignmentDTO))
        );
    }

    /**
     * Get all the educationalAlignments.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<EducationalAlignmentDTO> findAll(Pageable pageable, String alignmentType, String title, String country) {
        log.debug("Request to get all EducationalAlignments");
        return repository.findAll(pageable, alignmentType, title, country).map(educationalAlignmentMapper::toDto);
    }

    /**
     * Get one educationalAlignment by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<EducationalAlignmentDTO> findOne(Long id) {
        log.debug("Request to get EducationalAlignment : {}", id);
        return repository.findOne(id).map(educationalAlignmentMapper::toDto);
    }

    /**
     * Delete the educationalAlignment by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete EducationalAlignment : {}", id);
        repository.deleteById(id);
    }
}
