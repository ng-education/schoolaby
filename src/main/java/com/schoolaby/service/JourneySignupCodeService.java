package com.schoolaby.service;

import com.schoolaby.config.ApplicationProperties;
import com.schoolaby.domain.Authority;
import com.schoolaby.domain.Journey;
import com.schoolaby.domain.JourneySignupCode;
import com.schoolaby.repository.JourneySignupCodeRepository;
import com.schoolaby.service.dto.JourneySignupCodeDTO;
import com.schoolaby.service.mapper.JourneySignupCodeMapper;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

import static com.schoolaby.security.Role.Constants.STUDENT;
import static com.schoolaby.security.Role.Constants.TEACHER;
import static com.schoolaby.security.SecurityUtils.getCurrentUserLogin;
import static java.lang.String.format;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.http.HttpStatus.*;

@Service
@Transactional
@RequiredArgsConstructor
public class JourneySignupCodeService {
    private final Logger log = getLogger(JourneySignupCodeService.class);

    public static final String NO_SUCH_SIGN_UP_CODE = "No such sign-up code '%s'";
    public static final String NON_TEACHER_SIGNUP_CODE = "Non teacher signup code";
    public static final String DISALLOWED_USER = "This user is not allowed to make requests to this endpoint";

    private final ApplicationProperties applicationProperties;
    private final JourneySignupCodeRepository repository;
    private final JourneySignupCodeMapper mapper;

    @Transactional(readOnly = true)
    public Optional<JourneySignupCode> findOne(String signupCode) {
        log.debug("Request to get JourneySignupCode by teacherSignupCode: {}", signupCode);
        return repository.findBySignupCode(signupCode);
    }

    public JourneySignupCodeDTO createOneTimeSignupCode(JourneySignupCodeDTO signupCodeDTO) {
        String teacherSignupCode = signupCodeDTO.getSignUpCode();
        JourneySignupCode signupCode = findOne(teacherSignupCode)
            .orElseThrow(() -> new ResponseStatusException(NOT_FOUND, format(NO_SUCH_SIGN_UP_CODE, teacherSignupCode)));
        Journey journey = signupCode.getJourney();

        if (!signupCode.getAuthority().getName().equals(TEACHER)) {
            throw new ResponseStatusException(BAD_REQUEST, NON_TEACHER_SIGNUP_CODE);
        }
        if (!applicationProperties.getEndpoints().getJourneySignupCodeGeneration().getAllowedUsers().contains(getCurrentUserLogin().orElseThrow())) {
            throw new ResponseStatusException(FORBIDDEN, DISALLOWED_USER);
        }

        JourneySignupCode oneTimeSignupCode = new JourneySignupCode()
            .journey(journey)
            .signUpCode(randomAlphanumeric(4, 8).toLowerCase())
            .authority(new Authority().name(STUDENT))
            .setOneTime(true);

        oneTimeSignupCode = repository.save(oneTimeSignupCode);
        return mapper.toDto(oneTimeSignupCode);
    }
}
