package com.schoolaby.service;

import com.schoolaby.domain.LtiLaunch;
import com.schoolaby.domain.LtiResource;
import com.schoolaby.domain.LtiScore;
import com.schoolaby.repository.LtiLaunchRepository;
import com.schoolaby.repository.LtiScoreRepository;
import com.schoolaby.repository.filter.LtiScoreFilter;
import com.schoolaby.service.dto.lti.LtiScoreDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.schoolaby.common.LtiUtils.createPendingScore;
import static com.schoolaby.common.LtiUtils.getResourceLaunchesAsc;
import static com.schoolaby.domain.LtiScore.GRADING_PROGRESS_FULLY_GRADED;
import static io.jsonwebtoken.lang.Collections.isEmpty;
import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.*;

@Service
@Transactional
@RequiredArgsConstructor
public class LtiScoreService {
    private final LtiLaunchRepository ltiLaunchRepository;
    private final LtiScoreRepository ltiScoreRepository;

    public List<LtiScoreDTO> findAllOrderByCreatedDateDesc(Long assignmentId, Long userId) {
        Map<LtiResource, List<LtiLaunch>> launchesAscByResource = getResourceLaunchesAsc(ltiLaunchRepository.findAll(assignmentId, userId, true));
        Map<LtiResource, List<LtiScore>> scoresAscByResource = ltiScoreRepository.findAll(LtiScoreFilter.builder()
            .userId(userId)
            .assignmentId(assignmentId)
            .syncGrade(true)
            .gradingProgress(GRADING_PROGRESS_FULLY_GRADED)
            .build())
            .stream()
            .sorted(comparing(LtiScore::getTimestamp))
            .collect(groupingBy(LtiScore::getLtiResource));
        List<LtiScoreDTO> result = new ArrayList<>();

        launchesAscByResource.forEach((resource, launchesAsc) -> result.addAll(
            resource.isLtiAdvantage() ? getLtiAdvantageScores(resource, launchesAsc, scoresAscByResource.get(resource)) : getLti11Scores(launchesAsc))
        );

        return result.stream()
            .sorted(comparing(LtiScoreDTO::getCreatedDate).reversed())
            .collect(toList());
    }

    private List<LtiScoreDTO> getLti11Scores(List<LtiLaunch> resourceLaunchesAsc) {
        List<LtiScoreDTO> result = new ArrayList<>();

        for (int i = 0; i < resourceLaunchesAsc.size(); i++) {
            LtiLaunch launch = resourceLaunchesAsc.get(i);
            if (launch.getResult() != null || (resourceLaunchesAsc.size() == (i + 1))) {
                if (launch.getResult() != null) {
                    result.add(new LtiScoreDTO()
                        .setScore(launch.getResult())
                        .setTitle(launch.getLtiResource().getTitle())
                        .setResourceId(launch.getLtiResource().getId())
                        .setCreatedDate(launch.getCreatedDate())
                        .setUserId(launch.getUser().getId())
                        .setLaunches(resourceLaunchesAsc.subList(0, i + 1).stream()
                            .map(LtiLaunch::getCreatedDate)
                            .collect(toSet())));
                } else {
                    result.add(createPendingScore(launch, resourceLaunchesAsc.subList(0, i + 1)));
                }
            }
        }

        return result;
    }

    private List<LtiScoreDTO> getLtiAdvantageScores(LtiResource resource, List<LtiLaunch> resourceLaunchesAsc, List<LtiScore> resourceScoresAsc) {
        List<LtiScoreDTO> result = new ArrayList<>();

        if (!isEmpty(resourceScoresAsc)) {
            resourceScoresAsc.forEach(score -> result.add(
                new LtiScoreDTO()
                    .setTitle(resource.getTitle())
                    .setResourceId(score.getLineItem().getLtiResource().getId())
                    .setCreatedDate(score.getTimestamp())
                    .setUserId(score.getUser().getId())
                    .setScore(score.getScoreGiven().toString())
                    .setScoreMax(score.getScoreMaximum().toString())
                    .setLaunches(resourceLaunchesAsc.stream()
                        .map(LtiLaunch::getCreatedDate)
                        .filter(createdDate -> createdDate.compareTo(score.getTimestamp()) <= 0)
                        .collect(toSet()))));
        }
        LtiLaunch lastLaunch = resourceLaunchesAsc.get(resourceLaunchesAsc.size() - 1);
        if (isEmpty(resourceScoresAsc) || resourceScoresAsc.get(resourceScoresAsc.size() - 1).getTimestamp().isBefore(lastLaunch.getCreatedDate())) {
            result.add(createPendingScore(lastLaunch, resourceLaunchesAsc));
        }

        return result;
    }
}
