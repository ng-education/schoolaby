package com.schoolaby.service.dto;

import com.schoolaby.config.Constants;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A DTO representing a user, with their authorities.
 */
@Getter
@Setter
@NoArgsConstructor
public class UserDetailedDTO implements Serializable {
    private Long id;

    @NotBlank
    @Pattern(regexp = Constants.LOGIN_REGEX)
    @Size(min = 1, max = 100)
    private String login;

    @Size(max = 50)
    private String firstName;

    @Size(max = 50)
    private String lastName;

    @Email
    @Size(min = 5, max = 254)
    private String email;

    private String phoneNumber;

    private String personalCode;

    @Size(max = 256)
    private String imageUrl;

    private boolean activated = false;

    @Size(min = 2, max = 10)
    private String langKey;

    private String country;

    private Set<PersonRoleDTO> personRoles = new HashSet<>();

    private Set<String> authorities;

    private Set<String> externalAuthentications;

    private boolean termsAgreed;

    private boolean firstLogin;

    public Set<PersonRoleDTO> getPersonRoles() {
        return personRoles.stream().filter(personRoleDTO -> personRoleDTO.getRole() != null).collect(Collectors.toSet());
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UserDTO{" +
            "login='" + login + '\'' +
            ", firstName='" + firstName + '\'' +
            ", lastName='" + lastName + '\'' +
            ", email='" + email + '\'' +
            ", termsAgreed='" + termsAgreed + '\'' +
            "}";
    }
}
