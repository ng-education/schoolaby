package com.schoolaby.service.dto.submission;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.schoolaby.service.dto.SelectedCriterionLevelDTO;
import com.schoolaby.service.dto.UploadedFileDTO;
import lombok.Getter;

import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

public class SubmissionGradingPatchDTO implements SubmissionPatchDTO {

    @Getter
    private SubmissionFeedbackDTO submissionFeedback;
    private Boolean resubmittable;
    private Instant submittedForGradingDate;
    private Set<UploadedFileDTO> feedbackFiles = new HashSet<>();
    private boolean validateExistingFeedback = true;

    public Boolean isResubmittable() {
        return resubmittable;
    }

    public void setResubmittable(Boolean resubmittable) {
        this.resubmittable = resubmittable;
    }

    public Instant getSubmittedForGradingDate() {
        return submittedForGradingDate;
    }

    public void setSubmittedForGradingDate(Instant submittedForGradingDate) {
        this.submittedForGradingDate = submittedForGradingDate;
    }

    public Long getStudentId() {
        return submissionFeedback.getStudentId();
    }

    public Long getGroupId() {
        return submissionFeedback.getGroupId();
    }

    @JsonIgnore
    public Long getSubmissionGroupId() {
        return submissionFeedback.getSubmission().getGroupId();
    }

    public Long getSubmissionId() {
        return submissionFeedback.getSubmissionId();
    }

    public SubmissionGradingPatchDTO setSubmissionId(Long submissionId) {
        getSubmissionFeedback().setSubmissionId(submissionId);
        return this;
    }

    @JsonIgnore
    public Long getAssignmentId() { return submissionFeedback.getSubmission().getAssignmentId(); }

    public SubmissionGradingPatchDTO submissionFeedback(SubmissionFeedbackDTO submissionFeedback) {
        this.submissionFeedback = submissionFeedback;
        return this;
    }

    public SubmissionGradingPatchDTO resubmittable(Boolean resubmittable) {
        setResubmittable(resubmittable);
        return this;
    }

    public SubmissionGradingPatchDTO submittedForGradingDate(Instant submittedForGradingDate) {
        setSubmittedForGradingDate(submittedForGradingDate);
        return this;
    }

    public Set<UploadedFileDTO> getFeedbackFiles() {
        return feedbackFiles;
    }

    public void setFeedbackFiles(Set<UploadedFileDTO> feedbackFiles) {
        this.feedbackFiles = feedbackFiles;
    }

    @Override
    public String toString() {
        return "SubmissionGradingPatchDTO{" +
            "grade='" + submissionFeedback.getGrade() + '\'' +
            "feedbackDate=" + submissionFeedback.getFeedbackDate()  + '\'' +
            ", resubmittable=" + resubmittable +
            '}';
    }

    public boolean isValidateExistingFeedback() {
        return validateExistingFeedback;
    }

    public void setValidateExistingFeedback(boolean validateExistingFeedback) {
        this.validateExistingFeedback = validateExistingFeedback;
    }
}
