package com.schoolaby.service.dto.submission;

import com.schoolaby.service.dto.SelectedCriterionLevelDTO;
import com.schoolaby.service.dto.UploadedFileDTO;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
public class SubmissionFeedbackDTO implements Serializable {
    private Long id;

    private String grade;

    private String value;

    @NotNull
    private Instant feedbackDate;

    private Long studentId;

    private Long groupId;

    private Long submissionId;

    private Long creatorId;

    private SubmissionDTO submission;

    private Set<UploadedFileDTO> uploadedFiles = new HashSet<>();

    private Set<SelectedCriterionLevelDTO> selectedCriterionLevels = new HashSet<>();

    public SubmissionFeedbackDTO grade(String grade) {
        this.setGrade(grade);
        return this;
    }

    public SubmissionFeedbackDTO feedbackDate(Instant feedbackDate) {
        this.setFeedbackDate(feedbackDate);
        return this;
    }

    public SubmissionFeedbackDTO studentId(Long studentId) {
        this.setStudentId(studentId);
        return this;
    }

    public SubmissionFeedbackDTO groupId(Long groupId) {
        this.setGroupId(groupId);
        return this;
    }

    public SubmissionFeedbackDTO submissionId(Long submissionId) {
        this.setSubmissionId(submissionId);
        return this;
    }

    public SubmissionFeedbackDTO submission(SubmissionDTO submission) {
        this.submission = submission;
        return this;
    }
}
