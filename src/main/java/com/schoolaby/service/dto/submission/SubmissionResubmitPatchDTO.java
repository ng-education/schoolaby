package com.schoolaby.service.dto.submission;

import com.schoolaby.service.dto.UploadedFileDTO;

import java.time.Instant;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class SubmissionResubmitPatchDTO implements SubmissionPatchDTO {

    private String value;
    private Instant submittedForGradingDate;
    private Set<UploadedFileDTO> uploadedFiles = new HashSet<>();

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Instant getSubmittedForGradingDate() {
        return submittedForGradingDate;
    }

    public void setSubmittedForGradingDate(Instant submittedForGradingDate) {
        this.submittedForGradingDate = submittedForGradingDate;
    }

    public Collection<UploadedFileDTO> getUploadedFiles() {
        return uploadedFiles;
    }

    public void setUploadedFiles(Set<UploadedFileDTO> uploadedFiles) {
        this.uploadedFiles = uploadedFiles;
    }

    public SubmissionResubmitPatchDTO value(String value) {
        setValue(value);
        return this;
    }

    public SubmissionResubmitPatchDTO submittedForGradingDate(Instant submittedForGradingDate) {
        setSubmittedForGradingDate(submittedForGradingDate);
        return this;
    }

    public SubmissionResubmitPatchDTO uploadedFiles(Set<UploadedFileDTO> uploadedFiles) {
        setUploadedFiles(uploadedFiles);
        return this;
    }

    @Override
    public String toString() {
        return "SubmissionResubmitPatchDTO{" +
            "value='" + value + '\'' +
            ", submittedForGradingDate=" + submittedForGradingDate +
            ", uploadedFiles=" + uploadedFiles +
            '}';
    }
}
