package com.schoolaby.service.dto;

import lombok.Getter;

import java.util.List;

@Getter
public class OpiqResponseDTO {
    private Integer pageNumber;
    private Integer pageSize;
    private List<OpiqMaterialDTO> pageResult;

    @Getter
    public static class OpiqMaterialDTO {
        String url;
        Kit kit;
        Chapter chapter;
        Section section;
        List<Curriculum> curriculums;
        List<Class> classes;
        List<Subject> subjects;
        String thumbnailUrl;

        @Getter
        public static class Kit {
            Long id;
            String title;
        }

        @Getter
        public static class Chapter {
            Long id;
            String title;
        }


        public static class Section {
            Long id;
            String title;
        }


        public static class Curriculum {
            String code;
            String name;
        }


        public static class Class {
            String code;
            String name;
        }


        public static class Subject {
            String code;
            String name;
        }
    }
}
