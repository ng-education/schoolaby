package com.schoolaby.service.dto;

import lombok.Data;

import java.io.Serializable;
import java.time.Instant;

@Data
public class JourneyStudentDTO implements Serializable {
    private UserDTO user;
    private Instant pinnedDate;
    private Double averageGrade;
}
