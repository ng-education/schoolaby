package com.schoolaby.service.dto;

import com.schoolaby.service.dto.states.EntityState;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

import static java.time.Instant.now;

@Getter
@Setter
public class MilestoneDTO implements Serializable {
    private Long id;
    @NotNull
    private String title;
    private String description;
    private Instant endDate;
    private EntityState state;
    private Set<MaterialDTO> materials = new HashSet<>();
    private Set<AssignmentDTO> assignments = new HashSet<>();
    private Set<EducationalAlignmentDTO> educationalAlignments = new HashSet<>();
    private Long journeyId;
    private Long creatorId;
    private Instant published;
    private boolean hidden;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MilestoneDTO)) {
            return false;
        }

        return id != null && id.equals(((MilestoneDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MilestoneDTO{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", description='" + getDescription() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", materials='" + getMaterials() + "'" +
            ", educationalAlignments='" + getEducationalAlignments() + "'" +
            ", journeyId=" + getJourneyId() +
            ", creatorId=" + getCreatorId() +
            ", hidden=" + isHidden() +
            "}";
    }
}
