package com.schoolaby.service.dto.lti.claims;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;

import java.util.Set;

import static java.lang.String.format;

@JsonSerialize
@Getter
public class GradingEndpoints {
    @JsonProperty("scope")
    private static final Set<String> scope = Set.of(
        "https://purl.imsglobal.org/spec/lti-ags/scope/lineitem",
        "https://purl.imsglobal.org/spec/lti-ags/scope/result.readonly",
        "https://purl.imsglobal.org/spec/lti-ags/scope/score"
    );
    @JsonProperty("lineitems")
    private final String lineItems;

    public GradingEndpoints(String host, String context) {
        this.lineItems = format("%s/api/lti-advantage/%s/lineitems/", host, context);
    }
}
