package com.schoolaby.service.dto.lti.external;

import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

@Getter
@Setter
public class LtiExtScoreDTO {
    private Instant timestamp;
    private Double scoreGiven;
    private Double scoreMaximum;
    private String comment;
    private String activityProgress;
    private String gradingProgress;
    private String userId;
}
