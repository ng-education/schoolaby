package com.schoolaby.service.dto.lti;

import com.schoolaby.service.dto.LtiAppDTO;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
public class LtiResourceDTO implements Serializable {
    private Long id;
    private Long ltiContentItemId;
    private LtiAppDTO ltiApp;
    private String resourceLinkId = UUID.randomUUID().toString();
    private String launchUrl;
    private String toolResourceId;
    private String title;
    private String description;
    private boolean syncGrade;
    private List<LtiLineItemDTO> lineItems;
    private String createdBy;
    private Instant createdDate;
    private Integer sequenceNumber;
    private Long assignmentId;
}
