package com.schoolaby.service.dto.lti.claims;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
@JsonDeserialize
public class ResourceLink {
    @JsonProperty
    private String id;
    @JsonProperty
    private String description;
    @JsonProperty
    private String title;

    public String getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public String getTitle() {
        return title;
    }

    public ResourceLink id(String id) {
        this.id = id;
        return this;
    }

    public ResourceLink description(String description) {
        this.description = description;
        return this;
    }

    public ResourceLink title(String title) {
        this.title = title;
        return this;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
