package com.schoolaby.service.dto.lti.claims;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.jackson.io.JacksonDeserializer;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.BadCredentialsException;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Date;
import java.util.Map;

import static com.schoolaby.service.dto.lti.claims.LtiContextToken.LtiMessageType.RESOURCE_LINK;
import static io.jsonwebtoken.SignatureAlgorithm.RS256;
import static java.lang.System.currentTimeMillis;
import static org.apache.commons.lang3.StringUtils.isAnyEmpty;

/**
 * JWT token that we can use to authenticate the user in following redirects.
 */
public class LtiContextToken {
    public static final String CLAIM_JOURNEY_ID = "journeyId";
    public static final String CLAIM_MESSAGE_TYPE = "messageType";
    public static final String CLAIM_RESOURCE_LINK = "resourceLink";
    public static final String CLAIM_ROLE = "role";
    public static final String CLAIM_APP_CLIENT_ID = "appClientId";
    public static final String CLAIM_LTI_CONFIG_TYPE = "ltiConfigType";
    public static final String CLAIM_STUDENT_ID = "studentId";

    private String userId;
    private String journeyId;
    private LtiMessageType messageType;
    private ResourceLink resourceLink;
    private String role;
    private String appClientId;
    private String ltiConfigType;
    private String studentId;

    public LtiContextToken userId(String userId) {
        this.userId = userId;
        return this;
    }

    public LtiContextToken journeyId(String journeyId) {
        this.journeyId = journeyId;
        return this;
    }

    public LtiContextToken messageType(LtiMessageType messageType) {
        this.messageType = messageType;
        return this;
    }

    public LtiContextToken resourceLink(ResourceLink resourceLink) {
        this.resourceLink = resourceLink;
        return this;
    }

    public LtiContextToken role(String role) {
        this.role = role;
        return this;
    }

    public LtiContextToken appClientId(String appClientId) {
        this.appClientId = appClientId;
        return this;
    }

    public LtiContextToken ltiConfigType(String ltiConfigType) {
        this.ltiConfigType = ltiConfigType;
        return this;
    }

    public LtiContextToken studentId(String studentId) {
        this.studentId = studentId;
        return this;
    }


    public String getUserId() {
        return userId;
    }

    public String getJourneyId() {
        return journeyId;
    }

    public LtiMessageType getMessageType() {
        return messageType;
    }

    public ResourceLink getResourceLink() {
        return resourceLink;
    }

    public String getRole() {
        return role;
    }

    public String getAppClientId() {
        return appClientId;
    }

    public String getLtiConfigType() {
        return ltiConfigType;
    }

    public String getStudentId() {
        return studentId;
    }

    public String toToken(PrivateKey privateKey, int tokenValidityInSeconds) {
        return Jwts.builder()
            .setSubject(userId)
            .claim(CLAIM_JOURNEY_ID, journeyId)
            .claim(CLAIM_MESSAGE_TYPE, messageType)
            .claim(CLAIM_RESOURCE_LINK, resourceLink)
            .claim(CLAIM_ROLE, role)
            .claim(CLAIM_APP_CLIENT_ID, appClientId)
            .claim(CLAIM_LTI_CONFIG_TYPE, ltiConfigType)
            .claim(CLAIM_STUDENT_ID, studentId)
            .setExpiration(new Date(currentTimeMillis() + tokenValidityInSeconds * 1000L))
            .signWith(privateKey, RS256)
            .compact();
    }

    public static LtiContextToken parse(String token, PublicKey key) {
        Jws<Claims> claims = Jwts.parserBuilder()
            .deserializeJsonWith(new JacksonDeserializer(
                Map.of(CLAIM_RESOURCE_LINK, ResourceLink.class,
                    CLAIM_MESSAGE_TYPE, LtiMessageType.class)))
            .setSigningKey(key)
            .build()
            .parseClaimsJws(token);
        String subject = claims.getBody().getSubject();
        String journeyId = claims.getBody().get(CLAIM_JOURNEY_ID, String.class);
        LtiMessageType messageType = claims.getBody().get(CLAIM_MESSAGE_TYPE, LtiMessageType.class);
        ResourceLink resourceLink = claims.getBody().get(CLAIM_RESOURCE_LINK, ResourceLink.class);
        String role = claims.getBody().get(CLAIM_ROLE, String.class);
        String appClientId = claims.getBody().get(CLAIM_APP_CLIENT_ID, String.class);
        String ltiConfigType = claims.getBody().get(CLAIM_LTI_CONFIG_TYPE, String.class);
        String studentId = claims.getBody().get(CLAIM_STUDENT_ID, String.class);

        if (isAnyEmpty(subject, role) || messageType == null || (RESOURCE_LINK.equals(messageType) && resourceLink == null)) {
            throw new BadCredentialsException("Token does not contain required claims!");
        }

        return new LtiContextToken()
            .userId(subject)
            .journeyId(journeyId)
            .messageType(messageType)
            .resourceLink(resourceLink)
            .role(role)
            .appClientId(appClientId)
            .ltiConfigType(ltiConfigType)
            .studentId(studentId);
    }

    @RequiredArgsConstructor
    public enum LtiMessageType {
        RESOURCE_LINK("LtiResourceLinkRequest"), DEEP_LINKING("LtiDeepLinkingRequest");

        private final String value;

        @Override
        public String toString() {
            return this.value;
        }
    }
}
