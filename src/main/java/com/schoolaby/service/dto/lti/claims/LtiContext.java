package com.schoolaby.service.dto.lti.claims;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;


@JsonSerialize
@Getter
@Setter
@Accessors(fluent = true)
public class LtiContext {
    @JsonProperty("id")
    private String id;
    @JsonProperty("label")
    private String label;
    @JsonProperty("title")
    private String title;
}
