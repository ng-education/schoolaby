package com.schoolaby.service.dto.lti.claims;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomClaim {
    private String studentId;
}
