package com.schoolaby.service.dto.lti.external;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LtiExtLineItemDTO {
    private String id;
    private Long scoreMaximum;
    private String label;
    private String resourceId;
    private String tag;
    private String resourceLinkId;
}
