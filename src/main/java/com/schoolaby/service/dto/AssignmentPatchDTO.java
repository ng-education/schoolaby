package com.schoolaby.service.dto;

import com.schoolaby.service.dto.lti.LtiResourceDTO;
import lombok.Data;

import java.time.Instant;
import java.util.Set;

@Data
public class AssignmentPatchDTO {
    private String title;
    private String description;
    private Instant deadline;
    private Set<MaterialDTO> materials;
    private Set<LtiResourceDTO> ltiResources;
}
