package com.schoolaby.service.dto;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import static javax.xml.xpath.XPathConstants.NODE;
import static javax.xml.xpath.XPathConstants.STRING;

public class LtiRequest {
    private final String messageId;
    private final String operation;

    public LtiRequest(Document document) throws XPathExpressionException {
        XPathFactory xPathFactory = XPathFactory.newInstance();
        this.messageId = (String) xPathFactory.newXPath()
            .compile("/imsx_POXEnvelopeRequest/imsx_POXHeader/imsx_POXRequestHeaderInfo/imsx_messageIdentifier/text()")
            .evaluate(document, STRING);
        this.operation = ((Node) xPathFactory.newXPath()
            .compile("/imsx_POXEnvelopeRequest/imsx_POXBody/*[1]")
            .evaluate(document, NODE))
            .getNodeName()
            .replace("Request", "");
    }

    public String getOperation() {
        return operation;
    }

    public String getMessageId() {
        return messageId;
    }
}
