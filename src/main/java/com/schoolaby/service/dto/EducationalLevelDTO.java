package com.schoolaby.service.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

public class EducationalLevelDTO implements Serializable, Comparable<EducationalLevelDTO> {

    private Long id;

    @NotNull
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int compareTo(EducationalLevelDTO educationalLevelDTO) {
        return Integer.parseInt(String.valueOf(this.id - educationalLevelDTO.getId()));
    }
}
