package com.schoolaby.service.dto;

public enum LtiVersion {
    // LTI 1.1 is backwards compatible with 1.0, therefore both use 1.0
    LTI_1_1("LTI-1p0"), LTI_ADVANTAGE_VERSION("1.3.0");

    private final String value;

    LtiVersion(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
