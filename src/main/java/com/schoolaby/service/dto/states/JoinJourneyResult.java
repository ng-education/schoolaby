package com.schoolaby.service.dto.states;

public enum JoinJourneyResult {
    JOIN_RESULT_SUCCESS,
    JOIN_RESULT_ALREADY_JOINED,
    JOIN_RESULT_SIGNUP_CODE_ALREADY_USED,
}
