package com.schoolaby.service.dto.states;

public enum EntityState {
    OVERDUE(1),
    REJECTED(2),
    URGENT(3),
    IN_PROGRESS(4),
    FAILED(5),
    NOT_STARTED(6),
    COMPLETED(7),
    UNKNOWN(8);

    // lower number is higher priority
    private final int priority;

    EntityState(int priority) {
        this.priority = priority;
    }

    public int getPriority() {
        return priority;
    }
}
