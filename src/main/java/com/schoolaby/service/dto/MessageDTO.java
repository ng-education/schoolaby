package com.schoolaby.service.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;

@Getter
@Setter
public class MessageDTO implements Serializable {
    private Long id;

    @NotNull
    private String value;

    private String creator;

    private Long creatorId;

    @NotNull
    private Long chatId;

    private Instant createdDate;

    private Instant read;

    private boolean feedback;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MessageDTO)) {
            return false;
        }

        return id != null && id.equals(((MessageDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MessageDTO{" +
            "id=" + getId() +
            ", value='" + getValue() + "'" +
            ", creatorId=" + getCreatorId() +
            ", chatId=" + getChatId() +
            "}";
    }
}
