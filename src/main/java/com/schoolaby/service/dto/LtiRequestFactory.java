package com.schoolaby.service.dto;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.ByteArrayInputStream;
import java.io.IOException;

public class LtiRequestFactory {
    private static final String REPLACE_RESULT_REQUEST = "replaceResult";

    public static LtiRequest parseLtiRequest(String xml) throws IOException, SAXException, XPathExpressionException, ParserConfigurationException {
        Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder()
            .parse(new ByteArrayInputStream(xml.getBytes()));

        LtiRequest ltiRequest = new LtiRequest(document);
        if (REPLACE_RESULT_REQUEST.equals(ltiRequest.getOperation())) {
            return new ReplaceResultRequest(document);
        }

        return ltiRequest;
    }
}
