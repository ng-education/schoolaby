package com.schoolaby.service.dto;

import java.io.Serializable;

public class UploadedFileDTO implements Serializable {
    private Long id;
    private String uniqueName;
    private String originalName;
    private String type;
    private String extension;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOriginalName() {
        return originalName;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getUniqueName() {
        return uniqueName;
    }

    public void setUniqueName(String uniqueName) {
        this.uniqueName = uniqueName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UploadedFileDTO)) {
            return false;
        }

        return id != null && id.equals(((UploadedFileDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UploadedFileDTO{" +
            "id=" + getId() +
            ", uniqueName='" + getUniqueName() + "'" +
            ", originalName='" + getOriginalName() + "'" +
            ", type='" + getType() + "'" +
            ", extension='" + getExtension() + "'" +
            "}";
    }
}
