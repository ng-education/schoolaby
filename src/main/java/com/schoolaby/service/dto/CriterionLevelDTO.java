package com.schoolaby.service.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
@Setter
public class CriterionLevelDTO implements Serializable {
    @NotNull
    private Long id;
    @NotNull
    private String title;
    @NotNull
    private Long points;
    @NotNull
    private String description;
    @NotNull
    private Long sequenceNumber;
    @NotNull
    private Long criterionId;
}

