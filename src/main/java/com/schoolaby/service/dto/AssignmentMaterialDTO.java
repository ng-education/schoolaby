package com.schoolaby.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class AssignmentMaterialDTO implements Serializable {
    private AssignmentDTO assignment;
    private MaterialDTO material;
    private int sequenceNumber;
}
