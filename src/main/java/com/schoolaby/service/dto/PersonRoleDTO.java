package com.schoolaby.service.dto;

import com.schoolaby.security.Role;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
public class PersonRoleDTO implements Serializable {
    private Long id;
    private Boolean active;
    private LocalDateTime startDate = LocalDateTime.now();
    private LocalDateTime endDate;
    private SchoolDTO school;
    private String grade;
    private String parallel;
    private Long personId;
    private Role role;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PersonRoleDTO)) {
            return false;
        }

        return id != null && id.equals(((PersonRoleDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PersonRoleDTO{" +
            "id=" + getId() +
            ", role='" + getRole() + "'" +
            ", active='" + getActive() + "'" +
            ", startDate='" + getStartDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", school='" + (getSchool() != null ? getSchool().toString() : "null") + "'" +
            ", grade='" + getGrade() + "'" +
            ", parallel='" + getParallel() + "'" +
            ", personId=" + getPersonId() +
            "}";
    }
}
