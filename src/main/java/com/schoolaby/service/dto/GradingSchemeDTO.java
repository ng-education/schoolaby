package com.schoolaby.service.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
public class GradingSchemeDTO implements Serializable {
    private Long id;

    @NotNull
    private String name;

    private String code;

    private int sequenceNumber;

    private Set<GradingSchemeValueDTO> values = new HashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof GradingSchemeDTO)) {
            return false;
        }

        return id != null && id.equals(((GradingSchemeDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "GradingSchemeDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", code='" + getCode() + "'" +
            "}";
    }
}
