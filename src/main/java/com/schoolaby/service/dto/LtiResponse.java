package com.schoolaby.service.dto;

import java.util.UUID;

import static com.schoolaby.common.FileUtil.readFile;

public class LtiResponse {
    private static final String replaceResultTemplate = readFile("lti/replace-result-response-template.xml");
    private static final String unsupportedTemplate = readFile("lti/unsupported-response-template.xml");

    private LtiResponse() {
    }

    public static String unsupported(String messageRefId, String operation) {
        return unsupportedTemplate
            .replaceFirst("\\{messageIdentifier}", generateUUID())
            .replaceFirst("\\{messageRefId}", messageRefId)
            .replaceAll("\\{operation}", operation);
    }

    public static String replaceResult(String messageRefId, Long sourcedId, String result) {
        return replaceResultTemplate
            .replaceFirst("\\{messageIdentifier}", generateUUID())
            .replaceFirst("\\{sourcedId}", sourcedId.toString())
            .replaceFirst("\\{result}", result)
            .replaceFirst("\\{messageRefId}", messageRefId);
    }

    private static String generateUUID() {
        return UUID.randomUUID().toString();
    }
}
