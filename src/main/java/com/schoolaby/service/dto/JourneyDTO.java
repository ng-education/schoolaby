package com.schoolaby.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.schoolaby.service.dto.states.EntityState;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
public class JourneyDTO implements Serializable {
    private Long id;

    @NotNull
    private String title;

    private String description;

    private String videoConferenceUrl;

    private String signUpCode;

    @NotNull
    private Instant startDate;

    private Instant endDate;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY) // milestones are only here for creating from template
    private List<MilestoneDTO> milestones = new ArrayList<>();

    private List<JourneyTeacherDTO> teachers = new ArrayList<>();

    private Long progress = 0L;

    private Long creatorId;

    private int studentCount;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Set<JourneyStudentDTO> students = new HashSet<>();

    private Set<LtiConfigDTO> ltiConfigs = new HashSet<>();

    private EducationalLevelDTO educationalLevel;

    private Set<SchoolDTO> schools = new HashSet<>();

    private boolean template;

    private boolean nationalCurriculum;

    private String teacherName;

    private String imageUrl;

    private SubjectDTO subject;

    private CurriculumDTO curriculum;

    private int assignmentsCount;

    private int yearsCount;

    private int monthsCount;

    private int daysCount;

    private String teacherSignupCode;

    private String studentSignupCode;

    private boolean owner;

    private EntityState state;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof JourneyDTO)) {
            return false;
        }

        return id != null && id.equals(((JourneyDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "JourneyDTO{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            "}";
    }
}
