package com.schoolaby.service.dto;

import lombok.Data;

@Data
public class JourneySignupCodeDTO {
    private String signUpCode;
}
