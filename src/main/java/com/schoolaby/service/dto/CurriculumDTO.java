package com.schoolaby.service.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class CurriculumDTO implements Serializable {
    private Integer id;
    private String title;
    private String country;
    private Integer sequenceNumber;
    private boolean requiresSubject = true;
}
