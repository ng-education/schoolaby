package com.schoolaby.service.dto;

import lombok.Getter;

import java.util.List;

@Getter
public class EkoolikottResponseDTO {
    List<EkoolikottMaterialDTO> items;
    Integer totalResults;
    Integer start;
    Integer distinctIdCount;

    @Getter
    public static class EkoolikottMaterialDTO {
        String type;
        Long id;
        Creator creator;
        String visibility;
        String added;
        List<Taxon> taxons;
        Integer likes;
        Integer qualityWeight;
        Integer views;
        Boolean deleted;
        LicenseType licenseType;
        String title;
        String description;
        String thumbnailName;
        List<ResourceType> resourceTypes;

        public static class Creator {
            Long id;
            String username;
            String name;
            String surname;
        }

        public static class Taxon {
            String level;
            Long id;
            String name;
            String translationKey;
        }

        public static class LicenseType {
            Long id;
            String name;
        }

        public static class ResourceType {
            Long id;
            String name;
            String icon;
            Integer displayOrder;
        }
    }
}
