package com.schoolaby.service.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class EkoolRolesDTO implements Serializable {
    @JsonProperty("data")
    private Set<EkoolRoleDTO> data = new HashSet<>();

    @Getter
    @Setter
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class EkoolRoleDTO implements Serializable {
        @JsonProperty("role_name")
        private String roleName;
        @JsonProperty("school_id")
        private Long schoolId;
    }
}

