package com.schoolaby.service.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class SupportEmailDTO implements Serializable {
    private String subject;
    private String content;
}
