package com.schoolaby.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.w3c.dom.Document;

import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import static javax.xml.xpath.XPathConstants.STRING;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReplaceResultRequest extends LtiRequest {
    private final String sourcedId;
    private final String result;

    public ReplaceResultRequest(Document document) throws XPathExpressionException {
        super(document);
        XPathFactory xPathFactory = XPathFactory.newInstance();
        this.sourcedId = (String) xPathFactory.newXPath()
            .compile("/imsx_POXEnvelopeRequest/imsx_POXBody/replaceResultRequest/resultRecord/sourcedGUID/sourcedId/text()")
            .evaluate(document, STRING);
        this.result = ((String) xPathFactory.newXPath()
            .compile("/imsx_POXEnvelopeRequest/imsx_POXBody/replaceResultRequest/resultRecord/result/resultScore/textString/text()")
            .evaluate(document, STRING));
    }

    public String getSourcedId() {
        return sourcedId;
    }

    public String getResultScore() {
        return result;
    }

}
