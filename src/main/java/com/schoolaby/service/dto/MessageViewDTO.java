package com.schoolaby.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Set;

public class MessageViewDTO implements Serializable {
    private String authorName;
    private String messageValue;
    private Instant messageCreatedDate;
    private String journeyName;
    private String assignmentName;
    private SubjectDTO subject;
    private String urlToChat;
    private Instant read;

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getMessageValue() {
        return messageValue;
    }

    public void setMessageValue(String messageValue) {
        this.messageValue = messageValue;
    }

    public Instant getMessageCreatedDate() {
        return messageCreatedDate;
    }

    public void setMessageCreatedDate(Instant messageCreatedDate) {
        this.messageCreatedDate = messageCreatedDate;
    }

    public String getJourneyName() {
        return journeyName;
    }

    public void setJourneyName(String journeyName) {
        this.journeyName = journeyName;
    }

    public String getUrlToChat() {
        return urlToChat;
    }

    public void setUrlToChat(String urlToChat) {
        this.urlToChat = urlToChat;
    }

    public String getAssignmentName() {
        return assignmentName;
    }

    public void setAssignmentName(String assignmentName) {
        this.assignmentName = assignmentName;
    }

    public Instant getRead() {
        return read;
    }

    public void setRead(Instant read) {
        this.read = read;
    }

    public MessageViewDTO authorName(String authorName) {
        setAuthorName(authorName);
        return this;
    }

    public MessageViewDTO messageValue(String messageValue) {
        setMessageValue(messageValue);
        return this;
    }

    public MessageViewDTO messageCreatedDate(Instant messageCreatedDate) {
        setMessageCreatedDate(messageCreatedDate);
        return this;
    }

    public MessageViewDTO journeyName(String journeyName) {
        setJourneyName(journeyName);
        return this;
    }

    public MessageViewDTO urlToChat(final String urlToChat) {
        setUrlToChat(urlToChat);
        return this;
    }

    public MessageViewDTO assignmentName(final String assignmentName) {
        setAssignmentName(assignmentName);
        return this;
    }

    public MessageViewDTO read(Instant read) {
        setRead(read);
        return this;
    }

    public SubjectDTO getSubject() {
        return subject;
    }

    public void setSubject(SubjectDTO subject) {
        this.subject = subject;
    }

    public MessageViewDTO subject(SubjectDTO subject) {
        setSubject(subject);
        return this;
    }
}
