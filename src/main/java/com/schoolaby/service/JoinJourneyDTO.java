package com.schoolaby.service;

import javax.validation.constraints.NotEmpty;

public class JoinJourneyDTO {
    @NotEmpty
    private String signUpCode;

    public String getSignUpCode() {
        return signUpCode;
    }

    public void setSignUpCode(String signUpCode) {
        this.signUpCode = signUpCode;
    }

    public JoinJourneyDTO signUpCode(String signUpCode) {
        setSignUpCode(signUpCode);
        return this;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "JoinJourneyDTO{" +
            "signUpCode=" + getSignUpCode() +
            "}";
    }
}
