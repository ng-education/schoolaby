package com.schoolaby.security.harid;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;
import java.util.Set;

@JsonIgnoreProperties(ignoreUnknown = true)
public class HarIdUser {
    @JsonProperty("personal_code")
    private String idCode;
    @JsonProperty("given_name")
    private String firstName;
    @JsonProperty("family_name")
    private String lastName;
    @JsonProperty("profile")
    private String profile;
    @JsonProperty("strong_session")
    private String strongSession;
    @JsonProperty("email")
    private String email;
    @JsonProperty("email_verified")
    private boolean emailVerified;
    @JsonProperty("phone_number")
    private String phoneNumber;
    @JsonProperty("sub")
    private String sub;
    @JsonProperty("ui_locales")
    private String locale;
    @JsonProperty("roles")
    private Set<Role> roles;

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getIdCode() {
        return idCode;
    }

    public String getIdCodeNumbers() {
        if (idCode.toUpperCase().charAt(0) >= 'A') {
            idCode = idCode.substring(idCode.lastIndexOf(':') + 1);
        }
        return idCode;
    }

    public void setIdCode(String idCode) {
        this.idCode = idCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getStrongSession() {
        return strongSession;
    }

    public void setStrongSession(String strongSession) {
        this.strongSession = strongSession;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public static class Role {
        @JsonProperty("marker")
        private String marker;
        @JsonProperty("active")
        private Boolean active;
        @JsonProperty("start_date")
        private LocalDate startDate;
        @JsonProperty("end_date")
        private LocalDate endDate;
        @JsonProperty("provider_ehis_id")
        private String providerEhisId;
        @JsonProperty("provider_reg_nr")
        private String providerRegNr;
        @JsonProperty("provider_name")
        private String providerName;
        @JsonProperty("grade")
        private String grade;
        @JsonProperty("parallel")
        private String parallel;

        public String getMarker() {
            return marker;
        }

        public void setMarker(String marker) {
            this.marker = marker;
        }

        public Role marker(String marker) {
            setMarker(marker);
            return this;
        }

        public Boolean getActive() {
            return active;
        }

        public void setActive(Boolean active) {
            this.active = active;
        }

        public LocalDate getStartDate() {
            return startDate;
        }

        public void setStartDate(LocalDate startDate) {
            this.startDate = startDate;
        }

        public LocalDate getEndDate() {
            return endDate;
        }

        public void setEndDate(LocalDate endDate) {
            this.endDate = endDate;
        }

        public String getProviderEhisId() {
            return providerEhisId;
        }

        public void setProviderEhisId(String providerEhisId) {
            this.providerEhisId = providerEhisId;
        }

        public String getProviderRegNr() {
            return providerRegNr;
        }

        public void setProviderRegNr(String providerRegNr) {
            this.providerRegNr = providerRegNr;
        }

        public String getProviderName() {
            return providerName;
        }

        public void setProviderName(String providerName) {
            this.providerName = providerName;
        }

        public String getGrade() {
            return grade;
        }

        public void setGrade(String grade) {
            this.grade = grade;
        }

        public String getParallel() {
            return parallel;
        }

        public void setParallel(String parallel) {
            this.parallel = parallel;
        }
    }
}
