package com.schoolaby.security.harid;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.List;

import static java.lang.String.format;

@Repository
public class HarIdRepository {
    private final RestTemplate restTemplate;
    private final HarIdProperties configuration;

    public HarIdRepository(HarIdProperties configuration, RestTemplate harIdRestTemplate) {
        this.restTemplate = harIdRestTemplate;
        this.configuration = configuration;
    }

    public HarIdCode getHarIdCode(String code) {
        MultiValueMap<String, Object> params = new LinkedMultiValueMap<>();
        params.add("grant_type", "authorization_code");
        params.add("redirect_uri", configuration.getCallbackUrl());
        params.add("code", code);

        HttpHeaders headers = new HttpHeaders();
        headers.setBasicAuth(generateAuthHeaderHash());
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.setAccept(List.of(MediaType.APPLICATION_JSON));

        final HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<>(params, headers);

        final ResponseEntity<HarIdCode> response = restTemplate.postForEntity(configuration.getTokenUrl(), entity, HarIdCode.class);
        return response.getBody();
    }

    public HarIdUser getHarIdUser(HarIdCode code) {
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(code.getAccessToken());
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.setAccept(List.of(MediaType.APPLICATION_JSON));

        final HttpEntity<Object> entity = new HttpEntity<>(headers);

        final ResponseEntity<HarIdUser> response = restTemplate.exchange(
            configuration.getUserDataUrl(),
            HttpMethod.GET,
            entity,
            HarIdUser.class
        );
        return response.getBody();
    }

    private String generateAuthHeaderHash() {
        String authHeader = format("%s:%s", configuration.getClientId(), configuration.getClientSecret());
        return Base64.getEncoder().encodeToString(authHeader.getBytes(StandardCharsets.UTF_8));
    }
}
