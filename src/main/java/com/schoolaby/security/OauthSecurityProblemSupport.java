package com.schoolaby.security;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.zalando.problem.spring.web.advice.security.SecurityProblemSupport;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class OauthSecurityProblemSupport extends SecurityProblemSupport {
    private static final String LOGIN_URI = "/login?oauth=true";
    private static final String OAUTH_AUTHORIZATION_URI = "/oauth2/authorize";
    public static final String OAUTH_SERVER_REQUEST = "oauthServerRequest";

    public OauthSecurityProblemSupport(@Qualifier("handlerExceptionResolver") final HandlerExceptionResolver resolver) {
        super(resolver);
    }

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException exception) {
        super.handle(request, response, exception);
    }

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) {
        // If we receive an unauthenticated request to oauth2 authorization endpoint then we need to redirect the client to login view
        if (request.getRequestURI().contains(OAUTH_AUTHORIZATION_URI)) {
            try {
                request.getSession().setAttribute(OAUTH_SERVER_REQUEST, true);
                response.sendRedirect(LOGIN_URI);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            super.commence(request, response, exception);
        }
    }
}
