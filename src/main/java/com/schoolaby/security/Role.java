package com.schoolaby.security;

import java.util.Arrays;
import java.util.Optional;

public enum Role {
    // schoolaby application admin
    ADMIN(Constants.ADMIN, null, null),
    STUDENT(Constants.STUDENT, "student", "Student"),
    TEACHER(Constants.TEACHER, "faculty", "Teacher"),
    ANONYMOUS(Constants.ANONYMOUS, null, null);

    private final String name;
    private final String harIdMarker;
    private final String ekoolMarker;

    Role(String name, String harIdMarker, String ekoolMarker) {
        this.name = name;
        this.harIdMarker = harIdMarker;
        this.ekoolMarker = ekoolMarker;
    }

    public static Optional<Role> findHaridRole(String harIdMarker) {
        return Arrays.stream(Role.values())
            .filter(role -> role.harIdMarker != null && role.harIdMarker.equals(harIdMarker))
            .findFirst();
    }

    public static Optional<Role> findEkoolRole(String ekoolMarker) {
        return Arrays.stream(Role.values())
            .filter(role -> role.ekoolMarker != null && role.ekoolMarker.equals(ekoolMarker))
            .findFirst();
    }

    public String getName() {
        return name;
    }

    public String getEkoolMarker() {
        return ekoolMarker;
    }

    public String getHarIdMarker() {
        return harIdMarker;
    }

    public static class Constants {
        public static final String ADMIN = "ROLE_ADMIN";
        public static final String STUDENT = "ROLE_STUDENT";
        public static final String TEACHER = "ROLE_TEACHER";
        public static final String ANONYMOUS = "ROLE_ANONYMOUS";

        private Constants() {
        }
    }
}
