package com.schoolaby.logging.teams;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.zalando.problem.Problem;

@Service
@Profile({"!prod"})
public class NOPTeamsNotifierImpl implements TeamsNotifier {

    @Override
    public void send(Problem problem) {
        // NOP
    }

    @Override
    public void send(RuntimeException exception) {
        // NOP
    }

}
