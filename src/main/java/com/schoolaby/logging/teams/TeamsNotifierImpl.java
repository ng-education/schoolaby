package com.schoolaby.logging.teams;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.zalando.problem.DefaultProblem;
import org.zalando.problem.Problem;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Map;

import static java.lang.String.format;
import static java.util.Collections.list;
import static java.util.stream.Collectors.joining;
import static org.apache.commons.lang3.exception.ExceptionUtils.getRootCauseMessage;
import static org.apache.commons.lang3.exception.ExceptionUtils.getRootCauseStackTrace;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.context.request.RequestContextHolder.getRequestAttributes;

@Service
@Profile({"prod"})
@Slf4j
public class TeamsNotifierImpl implements TeamsNotifier {
    private final RestTemplate restTemplate;
    @Value("${application.monitoring.teams-hook}")
    private String teamsHook;

    public TeamsNotifierImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public void send(Problem problem) {
        log.debug("Sending error report to teams");

        JSONObject message = new JSONObject()
            .put("@context", "https://schema.org/extensions")
            .put("@type", "MessageCard")
            .put("themeColor", "FF0000")
            .put("title", problem.getTitle());

        if (problem instanceof Throwable) {
            Throwable defaultProblem = (Throwable) problem;
            message.put("text", getRootCauseMessage(defaultProblem));

            addAdditionalInfo(message, defaultProblem);

        } else {
            log.error("Problem type: {}", problem.getClass());
            message.put("text", problem.getDetail());
        }

        send(message.toString());
    }

    @Override
    public void send(RuntimeException exception) {
        log.debug("Sending error report to teams");
        Problem problem = Problem.builder()
            .withTitle(exception.getMessage())
            .withDetail(ExceptionUtils.getStackTrace(exception))
            .build();

        JSONObject message = new JSONObject()
            .put("@context", "https://schema.org/extensions")
            .put("@type", "MessageCard")
            .put("themeColor", "FF0000")
            .put("title", problem.getTitle());

        Throwable defaultProblem = (Throwable) problem;
        message.put("text", getRootCauseMessage(defaultProblem));

        addAdditionalInfo(message, defaultProblem);

        send(message.toString());
    }

    private void addAdditionalInfo(JSONObject message, Throwable defaultProblem) {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) getRequestAttributes();
        if (requestAttributes != null) {
            HttpServletRequest request = requestAttributes.getRequest();

            JSONArray facts = new JSONArray()
                .put(new JSONObject().put("name", "URL").put("value", request.getMethod() + " " + new String(request.getRequestURL())))
                .put(new JSONObject().put("name", "Query string").put("value", request.getQueryString()))
                .put(new JSONObject().put("name", "Remote user").put("value", request.getRemoteUser()))
                .put(new JSONObject().put("name", "Remote IP").put("value", request.getRemoteAddr()))
                .put(new JSONObject().put("name", "Headers").put("value", getHeaders(request)));

            if (defaultProblem instanceof DefaultProblem) {
                Map<String, Object> parameters = ((DefaultProblem) defaultProblem).getParameters();
                if (!parameters.isEmpty()) {
                    JSONArray additionalInfo = new JSONArray();
                    parameters.forEach((key, value) -> additionalInfo.put(new JSONObject().put(key, value)));
                    facts.put(new JSONObject().put("name", "Additional info").put("value", additionalInfo));
                }
            }

            message.put("sections", new JSONArray().put(
                    new JSONObject()
                        .put("text", Arrays.toString(getRootCauseStackTrace(defaultProblem)).replace("\t", "\t\n\t"))
                        .put("facts", facts)
                )
            );
        }
    }

    private String getHeaders(HttpServletRequest request) {
        return list(request.getHeaderNames()).stream()
            .filter(s -> !s.equals("cookie") && !s.equals("authorization"))
            .map(s -> format("%s: %s", s, request.getHeader(s)))
            .collect(joining("\n"));
    }


    private void send(String message) {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(APPLICATION_JSON);

            restTemplate.postForLocation(teamsHook, new HttpEntity<>(message, headers));
        } catch (Exception e) {
            log.error("Sending message to Teams failed! {}", e.getMessage());
        }
    }
}
