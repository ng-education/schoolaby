package com.schoolaby.common;

import java.time.ZoneId;

public class DateUtils {
    public static final ZoneId EST_TZ = ZoneId.of("Europe/Tallinn");

    private DateUtils() {
    }
}
