package com.schoolaby.common;

import com.schoolaby.domain.LtiLaunch;
import com.schoolaby.domain.LtiResource;
import com.schoolaby.service.dto.lti.LtiScoreDTO;

import java.util.List;
import java.util.Map;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toSet;

public class LtiUtils {
    public static LtiScoreDTO createPendingScore(LtiLaunch lastLaunch, List<LtiLaunch> allLaunches) {
        return new LtiScoreDTO()
            .setTitle(lastLaunch.getLtiResource().getTitle())
            .setResourceId(lastLaunch.getLtiResource().getId())
            .setCreatedDate(lastLaunch.getCreatedDate())
            .setUserId(lastLaunch.getUser().getId())
            .setLaunches(getResourceLaunchesAsc(allLaunches).get(lastLaunch.getLtiResource()).stream()
                .map(LtiLaunch::getCreatedDate)
                .collect(toSet()));
    }

    public static Map<LtiResource, List<LtiLaunch>> getResourceLaunchesAsc(List<LtiLaunch> launches) {
        return launches.stream()
            .sorted(comparing(LtiLaunch::getCreatedDate))
            .collect(groupingBy(LtiLaunch::getLtiResource));
    }
}
