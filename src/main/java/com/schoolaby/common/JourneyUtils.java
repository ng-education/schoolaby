package com.schoolaby.common;

import com.schoolaby.domain.Journey;
import org.springframework.web.server.ResponseStatusException;

import static com.schoolaby.security.SecurityUtils.getCurrentUserId;
import static org.springframework.http.HttpStatus.FORBIDDEN;

public class JourneyUtils {
    public static final String USER_NOT_A_TEACHER_IN_JOURNEY = "Current user is not a teacher in the journey!";

    public static void validateIsUserATeacherInJourney(Journey journey) {
        if (!journey.isTeacherInJourney(getCurrentUserId())) {
            throw new ResponseStatusException(FORBIDDEN, USER_NOT_A_TEACHER_IN_JOURNEY);
        }
    }
}
