package com.schoolaby.web.rest.upload;

import com.schoolaby.service.FileDtoService;
import com.schoolaby.service.dto.UploadedFileDTO;
import com.schoolaby.service.storage.FileDetails;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ws.schild.jave.EncoderException;

import java.io.IOException;
import java.net.URLEncoder;

import static java.nio.charset.StandardCharsets.UTF_8;

@RestController
@RequestMapping("/api/files")
public class FileController {
    private final FileDtoService service;

    public FileController(FileDtoService service) {
        this.service = service;
    }

    @PostMapping
    public UploadedFileDTO upload(@RequestPart(value = "file") MultipartFile file) throws IOException, EncoderException {
        return service.upload(file);
    }

    @DeleteMapping("/{uniqueFileName}")
    public void delete(@PathVariable String uniqueFileName) throws IOException {
        service.delete(uniqueFileName);
    }

    // TODO: Implement some sort of business logic based checks
    @GetMapping("/{uniqueFileName}")
    public ResponseEntity<InputStreamResource> download(@PathVariable String uniqueFileName) throws IOException {
        FileDetails file = service.getDetails(uniqueFileName);

        return ResponseEntity
            .ok()
            .contentType(file.getContentType())
            .header("Content-Disposition", "attachment; filename=\"" + URLEncoder.encode(uniqueFileName, UTF_8) + "\"")
            .body(new InputStreamResource(file.getContent()));
    }
}
