package com.schoolaby.web.rest;

import com.schoolaby.domain.User;
import com.schoolaby.security.SecurityUtils;
import com.schoolaby.security.jwt.JWTFilter;
import com.schoolaby.security.jwt.TokenProvider;
import com.schoolaby.service.AccountResourceException;
import com.schoolaby.service.MailService;
import com.schoolaby.service.UserService;
import com.schoolaby.service.dto.PasswordChangeDTO;
import com.schoolaby.service.dto.UserDetailedDTO;
import com.schoolaby.web.rest.errors.EmailAlreadyUsedException;
import com.schoolaby.web.rest.errors.InvalidPasswordException;
import com.schoolaby.web.rest.vm.JWTToken;
import com.schoolaby.web.rest.vm.KeyAndPasswordVM;
import com.schoolaby.web.rest.vm.ManagedUserVM;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

import static com.schoolaby.security.Role.Constants.ADMIN;
import static com.schoolaby.security.SecurityUtils.*;
import static io.github.jhipster.web.util.HeaderUtil.createAlert;
import static io.github.jhipster.web.util.HeaderUtil.createEntityUpdateAlert;
import static org.springframework.http.ResponseEntity.badRequest;

@RestController
@RequestMapping("/api")
@Slf4j
public class AccountController {
    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private static final String ENTITY_NAME = "user";
    private final UserService userService;
    private final MailService mailService;
    private final TokenProvider tokenProvider;

    public AccountController(UserService userService, MailService mailService, TokenProvider tokenProvider) {
        this.userService = userService;
        this.mailService = mailService;
        this.tokenProvider = tokenProvider;
    }

    @PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<JWTToken> registerAccount(@Valid @RequestBody ManagedUserVM managedUserVM) throws URISyntaxException {
        if (!checkPasswordLength(managedUserVM.getPassword())) {
            throw new InvalidPasswordException();
        }
        String jwt = tokenProvider.createToken(userService.registerUser(managedUserVM, managedUserVM.getPassword()));

        HttpHeaders httpHeaders = createAlert(applicationName, "register.messages.success", "");
        httpHeaders.add(JWTFilter.AUTHORIZATION_HEADER, "Bearer " + jwt);

        return ResponseEntity
            .created(new URI("/"))
            .headers(httpHeaders)
            .body(new JWTToken(jwt));
    }

    @GetMapping("/activate")
    public void activateAccount(@RequestParam(value = "key") String key) {
        Optional<User> user = userService.activateRegistration(key);
        if (user.isEmpty()) {
            throw new AccountResourceException("No user was found for this activation key");
        }
    }

    @GetMapping("/authenticate")
    public String isAuthenticated(HttpServletRequest request) {
        log.debug("REST request to check if the current user is authenticated");
        return request.getRemoteUser();
    }

    @GetMapping("/account")
    public UserDetailedDTO getAccount() {
        return userService.getAccountDto();
    }

    @PostMapping("/account")
    public ResponseEntity<Void> saveAccount(@Valid @RequestBody UserDetailedDTO userDTO) {
        String userLogin = SecurityUtils
            .getCurrentUserLogin()
            .orElseThrow(() -> new AccountResourceException("Current user login not found"));

        if (!isCurrentUserInRole(ADMIN) && !getCurrentUserId().equals(userDTO.getId())) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Current user is not the user sent for updating!");
        }

        Optional<User> existingUser = userService.findOneByEmailIgnoreCase(userDTO.getEmail());
        if (existingUser.isPresent() && (!existingUser.get().getLogin().equalsIgnoreCase(userLogin))) {
            throw new EmailAlreadyUsedException();
        }
        Optional<User> user = userService.findOneByLogin(userLogin);
        if (user.isEmpty()) {
            throw new AccountResourceException("User could not be found");
        }
        userService.updateUser(
            userDTO.getFirstName(),
            userDTO.getLastName(),
            userDTO.getEmail(),
            userDTO.getLangKey(),
            userDTO.getImageUrl(),
            userDTO.getAuthorities(),
            userDTO.getCountry(),
            userDTO.getPersonRoles(),
            userDTO.isTermsAgreed()
        );
        return ResponseEntity
            .ok()
            .headers(createEntityUpdateAlert(applicationName, true, ENTITY_NAME, ""))
            .build();
    }

    @PostMapping(path = "/account/change-password")
    public void changePassword(@RequestBody PasswordChangeDTO passwordChangeDto) {
        if (!checkPasswordLength(passwordChangeDto.getNewPassword())) {
            throw new InvalidPasswordException();
        }
        userService.changePassword(passwordChangeDto.getCurrentPassword(), passwordChangeDto.getNewPassword());
    }

    @PostMapping("/account/logout")
    public ResponseEntity<Void> logout(HttpServletRequest request) {
        try {
            request.getSession().invalidate();
        } catch (IllegalStateException ignored) {

        }

        return ResponseEntity.ok().build();
    }

    @PostMapping(path = "/account/reset-password/init")
    public ResponseEntity<Void> requestPasswordReset(@RequestBody String mail) {
        Optional<User> user = userService.requestPasswordReset(mail);
        if (user.isPresent()) {
            mailService.sendPasswordResetMail(user.get());

            return ResponseEntity
                .ok()
                .headers(createAlert(applicationName, "reset.request.messages.success", ""))
                .build();
        } else {
            log.warn("Password reset requested for non existing mail");
            return badRequest().build();
        }
    }

    @PostMapping(path = "/account/reset-password/finish")
    public ResponseEntity<Void> finishPasswordReset(@RequestBody KeyAndPasswordVM keyAndPassword) {
        if (!checkPasswordLength(keyAndPassword.getNewPassword())) {
            throw new InvalidPasswordException();
        }
        Optional<User> user = userService.completePasswordReset(keyAndPassword.getNewPassword(), keyAndPassword.getKey());

        if (user.isEmpty()) {
            throw new AccountResourceException("No user was found for this reset key");
        }
        return ResponseEntity
            .ok()
            .headers(createAlert(applicationName, "reset.finish.messages.success", ""))
            .build();
    }

    @PostMapping(path = "/account/terms-agreed")
    public ResponseEntity<Void> termsAgreed() {
        userService.agreeToTerms();
        return ResponseEntity
            .ok()
            .build();
    }

    @PostMapping(path = "/account/first-login")
    public ResponseEntity<Void> firstLogin() {
        userService.firstLogin();
        return ResponseEntity
            .ok()
            .build();
    }

    private static boolean checkPasswordLength(String password) {
        return (
            !StringUtils.isEmpty(password) &&
                password.length() >= ManagedUserVM.PASSWORD_MIN_LENGTH &&
                password.length() <= ManagedUserVM.PASSWORD_MAX_LENGTH
        );
    }
}
