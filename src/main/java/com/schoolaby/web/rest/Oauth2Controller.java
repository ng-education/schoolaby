package com.schoolaby.web.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.core.oidc.OidcIdToken;
import org.springframework.security.oauth2.server.authorization.OAuth2Authorization;
import org.springframework.security.oauth2.server.authorization.OAuth2AuthorizationService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.security.oauth2.core.OAuth2TokenType.ACCESS_TOKEN;

@Controller
@RequestMapping("/oauth2")
public class Oauth2Controller {

    private static final String OAUTH2_BEARER_TOKEN_IS_NOT_PRESENT_IN_HEADERS = "Oauth2 Bearer token is not present in headers";
    private static final String OAUTH2_ACCESS_TOKEN_IS_INVALID_OR_HAS_EXPIRED = "Oauth2 Access token is invalid or has expired";

    private final OAuth2AuthorizationService oAuth2AuthorizationService;

    public Oauth2Controller(OAuth2AuthorizationService oAuth2AuthorizationService) {
        this.oAuth2AuthorizationService = oAuth2AuthorizationService;
    }

    @GetMapping("/userinfo")
    public ResponseEntity<?> userInfo(@RequestHeader(value = AUTHORIZATION, required = false) String bearerToken) {
        if (bearerToken == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(OAUTH2_BEARER_TOKEN_IS_NOT_PRESENT_IN_HEADERS);
        }

        OAuth2Authorization authorization = oAuth2AuthorizationService.findByToken(bearerToken.replace("Bearer", "").trim(), ACCESS_TOKEN);

        if (authorization == null) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(OAUTH2_ACCESS_TOKEN_IS_INVALID_OR_HAS_EXPIRED);
        }

        return ResponseEntity.ok(authorization.getToken(OidcIdToken.class).getClaims());
    }
}
