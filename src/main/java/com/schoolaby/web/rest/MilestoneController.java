package com.schoolaby.web.rest;

import com.schoolaby.service.MilestoneService;
import com.schoolaby.service.dto.MilestoneDTO;
import com.schoolaby.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import static com.schoolaby.security.SecurityUtils.IS_TEACHER_OR_ADMIN;
import static io.github.jhipster.web.util.HeaderUtil.*;

@RestController
@RequestMapping("/api")
public class MilestoneController {
    private final Logger log = LoggerFactory.getLogger(MilestoneController.class);

    private static final String ENTITY_NAME = "milestone";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MilestoneService milestoneService;

    public MilestoneController(MilestoneService milestoneService) {
        this.milestoneService = milestoneService;
    }

    @PostMapping("/milestones")
    @PreAuthorize(IS_TEACHER_OR_ADMIN)
    public ResponseEntity<MilestoneDTO> createMilestone(@Valid @RequestBody MilestoneDTO milestoneDTO) throws URISyntaxException {
        log.debug("REST request to save Milestone : {}", milestoneDTO);
        if (milestoneDTO.getId() != null) {
            throw new BadRequestAlertException("A new milestone cannot already have an ID", ENTITY_NAME, "idExists");
        }
        MilestoneDTO result = milestoneService.create(milestoneDTO);
        return ResponseEntity
            .created(new URI("/api/milestones/" + result.getId()))
            .headers(createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getTitle()))
            .body(result);
    }

    @PutMapping("/milestones")
    @PreAuthorize(IS_TEACHER_OR_ADMIN)
    public ResponseEntity<MilestoneDTO> updateMilestone(@Valid @RequestBody MilestoneDTO milestoneDTO) throws URISyntaxException {
        log.debug("REST request to update Milestone : {}", milestoneDTO);
        if (milestoneDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idNull");
        }
        MilestoneDTO result = milestoneService.update(milestoneDTO);
        return ResponseEntity
            .ok()
            .headers(createEntityUpdateAlert(applicationName, true, ENTITY_NAME, milestoneDTO.getTitle()))
            .body(result);
    }

    @GetMapping("/milestones")
    public ResponseEntity<List<MilestoneDTO>> getAllMilestones(
        @RequestParam(required = false) Long journeyId,
        @RequestParam(required = false, defaultValue = "false") Boolean template
    ) {
        log.debug("REST request to get a list of Milestones");
        return ResponseEntity.ok(milestoneService.findAll(journeyId, template));
    }

    @GetMapping("/milestones/{id}")
    public ResponseEntity<MilestoneDTO> getMilestone(
        @PathVariable Long id,
        @RequestParam(required = false, defaultValue = "false") Boolean template
        ) {
        log.debug("REST request to get Milestone : {}", id);
        Optional<MilestoneDTO> milestoneDTO = milestoneService.findOne(id, template);
        return ResponseUtil.wrapOrNotFound(milestoneDTO);
    }

    @DeleteMapping("/milestones/{id}")
    @PreAuthorize(IS_TEACHER_OR_ADMIN)
    public ResponseEntity<Void> deleteMilestone(@PathVariable Long id) {
        log.debug("REST request to delete Milestone : {}", id);
        milestoneService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
