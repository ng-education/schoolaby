package com.schoolaby.web.rest;

import com.schoolaby.service.LtiScoreService;
import com.schoolaby.service.dto.lti.LtiScoreDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/api/lti-scores")
@RequiredArgsConstructor
@Slf4j
public class LtiScoreController {
    private final LtiScoreService ltiScoreService;

    @GetMapping
    public ResponseEntity<List<LtiScoreDTO>> findAllOrderByCreatedDateDesc(@RequestParam Long assignmentId,
                                                     @RequestParam Long userId) {
        return ok(ltiScoreService.findAllOrderByCreatedDateDesc(assignmentId, userId));
    }
}
