package com.schoolaby.web.rest;

import com.schoolaby.service.JoinJourneyDTO;
import com.schoolaby.service.JourneyService;
import com.schoolaby.service.dto.*;
import com.schoolaby.service.dto.states.EntityState;
import com.schoolaby.web.rest.errors.BadRequestAlertException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.schoolaby.security.SecurityUtils.*;
import static com.schoolaby.service.JourneyService.SIGNUP_CODE_ALREADY_USED;
import static com.schoolaby.service.dto.states.JoinJourneyResult.JOIN_RESULT_ALREADY_JOINED;
import static com.schoolaby.service.dto.states.JoinJourneyResult.JOIN_RESULT_SIGNUP_CODE_ALREADY_USED;
import static io.github.jhipster.web.util.HeaderUtil.*;
import static io.github.jhipster.web.util.PaginationUtil.generatePaginationHttpHeaders;
import static io.github.jhipster.web.util.ResponseUtil.wrapOrNotFound;
import static java.lang.String.format;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.ResponseEntity.*;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/journeys")
public class JourneyController {
    private static final String ENTITY_NAME = "journey";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final JourneyService journeyService;

    @PostMapping
    @PreAuthorize(IS_TEACHER_OR_ADMIN)
    public ResponseEntity<JourneyDTO> createJourney(@Valid @RequestBody JourneyDTO journeyDTO) throws URISyntaxException {
        log.debug("REST request to save Journey : {}", journeyDTO);
        if (journeyDTO.getId() != null) {
            throw new BadRequestAlertException("A new journey cannot already have an ID", ENTITY_NAME, "idExists");
        }
        JourneyDTO result = journeyService.create(journeyDTO);
        return created(new URI("/api/journeys/" + result.getId()))
            .headers(createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getTitle()))
            .body(result);
    }

    @PutMapping
    @PreAuthorize(IS_TEACHER_OR_ADMIN)
    public ResponseEntity<JourneyDTO> updateJourney(@Valid @RequestBody JourneyDTO journeyDTO) {
        log.debug("REST request to update Journey : {}", journeyDTO);
        if (journeyDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idNull");
        }
        JourneyDTO result = journeyService.update(journeyDTO);
        return ok().headers(createEntityUpdateAlert(applicationName, true, ENTITY_NAME, journeyDTO.getTitle())).body(result);
    }

    // TODO: Remove this method when dashboard-old is removed in front end
    @GetMapping(headers = {"JOURNEY-JSON-VERSION=1"})
    @PreAuthorize(IS_STUDENT_OR_TEACHER_OR_ADMIN)
    public ResponseEntity<List<JourneyDTO>> getAllJourneys(
        Pageable pageable,
        @RequestParam(required = false) EntityState state,
        @RequestParam(required = false) boolean template
    ) {
        log.debug("REST request to get a page of Journeys");

        Page<JourneyDTO> page = journeyService.findAll(pageable, state, template);

        HttpHeaders headers = generatePaginationHttpHeaders(fromCurrentRequest(), page);
        return ok().headers(headers).body(page.getContent());
    }

    @GetMapping(headers = {"JOURNEY-JSON-VERSION=2"})
    @PreAuthorize(IS_STUDENT_OR_TEACHER_OR_ADMIN)
    public ResponseEntity<List<JourneyDTO>> getAllJourneysV2(
        Pageable pageable,
        @RequestParam(required = false) EntityState state,
        @RequestParam(required = false) boolean template,
        @RequestParam(required = false, defaultValue = "") String search,
        @RequestParam(required = false) Long subjectId,
        @RequestParam(required = false) Long userId
    ) {
        log.debug("REST request to get all Journeys");

        validateJourneyRequest(state, search);

        Page<JourneyDTO> page;
        if (template) {
            page = journeyService.getJourneyTemplates(pageable, search, subjectId, userId);
        } else {
            page = journeyService.findAll(pageable, state, false);
        }

        HttpHeaders headers = generatePaginationHttpHeaders(fromCurrentRequest(), page);
        return ok().headers(headers).body(page.getContent());
    }

    private void validateJourneyRequest(EntityState state, String search) {
        if (state != null && search != null) {
            throw new BadRequestAlertException("Journey state and search should not co-exist", "Journey", "journey.stateAndSearchExist");
        }
    }

    @GetMapping("/{id}")
    @PreAuthorize(IS_STUDENT_OR_TEACHER_OR_ADMIN)
    public ResponseEntity<JourneyDTO> getJourney(
        @PathVariable Long id,
        @RequestParam(required = false, defaultValue = "false") Boolean template
    ) {
        log.debug("REST request to get Journey : {}", id);
        Optional<JourneyDTO> journeyDTO = journeyService.findOneDTO(id, template);
        return wrapOrNotFound(journeyDTO);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize(IS_TEACHER_OR_ADMIN)
    public ResponseEntity<Void> deleteJourney(@PathVariable Long id) {
        log.debug("REST request to delete Journey : {}", id);
        journeyService.delete(id);
        return noContent().headers(createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    @PreAuthorize(IS_TEACHER_OR_ADMIN)
    @GetMapping("/{journeyId}/average-grade")
    public ResponseEntity<Map<Long, Double>> getStudentsAverageGradesByJourneyId(@PathVariable Long journeyId) {
        log.debug("REST request to get students average grades by journey id : {}", journeyId);
        return ok(journeyService.getJourneyStudentsAverageGrade(journeyId));
    }

    @PreAuthorize(IS_TEACHER_OR_ADMIN)
    @GetMapping("/{journeyId}/students")
    public ResponseEntity<List<JourneyStudentDTO>> getStudents(@PathVariable Long journeyId) {
        log.debug("REST request to get students for journey id : {}", journeyId);
        return ok(journeyService.getStudents(journeyId));
    }

    @PreAuthorize(IS_AUTHENTICATED)
    @PostMapping("/join")
    public ResponseEntity<JoinJourneyResultDTO> join(@Valid @RequestBody JoinJourneyDTO request) {
        log.debug("REST request for user {} to join journey with sign up code : {}", getCurrentUserId(), request.getSignUpCode());
        JoinJourneyResultDTO joinJourneyResultDTO = journeyService.join(request);

        if (JOIN_RESULT_ALREADY_JOINED.equals(joinJourneyResultDTO.getStatus())) {
            return ok().body(joinJourneyResultDTO);
        } else if (JOIN_RESULT_SIGNUP_CODE_ALREADY_USED.equals(joinJourneyResultDTO.getStatus())) {
            throw new BadRequestAlertException(SIGNUP_CODE_ALREADY_USED, ENTITY_NAME, "signupCodeAlreadyUsed");
        }

        return ok()
            .headers(createEntityCreationAlert(applicationName, true, format("%s.students", ENTITY_NAME), ""))
            .body(joinJourneyResultDTO);
    }

    @PreAuthorize(IS_TEACHER_OR_ADMIN)
    @DeleteMapping("/{journeyId}/students/{studentId}")
    public ResponseEntity<Void> removeStudent(@PathVariable Long journeyId, @PathVariable Long studentId) {
        log.debug("REST request to delete student {} from journey {}", studentId, journeyId);
        String studentName = journeyService.removeStudent(journeyId, studentId);

        return noContent()
            .headers(createEntityDeletionAlert(applicationName, true, format("%s.students", ENTITY_NAME), studentName))
            .build();
    }

    @PreAuthorize(IS_TEACHER_OR_ADMIN)
    @DeleteMapping("/{journeyId}/teachers/{teacherId}")
    public ResponseEntity<Void> removeTeacher(@PathVariable Long journeyId, @PathVariable Long teacherId) {
        log.debug("REST request to delete teacher {} from journey {}", teacherId, journeyId);
        String teacherName = journeyService.removeTeacher(journeyId, teacherId);

        return noContent()
            .headers(createEntityDeletionAlert(applicationName, true, format("%s.teachers", ENTITY_NAME), teacherName))
            .build();
    }

    @PreAuthorize(IS_TEACHER_OR_ADMIN)
    @PutMapping("/{journeyId}/students/{studentId}/pin")
    public ResponseEntity<Void> toggleStudentPin(@PathVariable Long journeyId, @PathVariable Long studentId) {
        log.debug("REST request to toggle Journey: {} Student : {} pin", journeyId, studentId);
        if (journeyService.toggleStudentPin(journeyId, studentId) == null) {
            throw new ResponseStatusException(NOT_FOUND);
        }
        return noContent().build();
    }
}
