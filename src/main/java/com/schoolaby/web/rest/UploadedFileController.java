package com.schoolaby.web.rest;

import com.schoolaby.service.UploadedFileService;
import com.schoolaby.service.dto.UploadedFileDTO;
import com.schoolaby.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import static io.github.jhipster.web.util.HeaderUtil.*;
import static org.slf4j.LoggerFactory.getLogger;

@RestController
@RequestMapping("/api")
public class UploadedFileController {
    private final Logger log = getLogger(UploadedFileController.class);

    private static final String ENTITY_NAME = "uploadedFile";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UploadedFileService uploadedFileService;

    public UploadedFileController(UploadedFileService uploadedFileService) {
        this.uploadedFileService = uploadedFileService;
    }

    /**
     * {@code POST  /uploaded-files} : Create a new uploadedFile.
     *
     * @param uploadedFileDTO the uploadedFileDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new uploadedFileDTO, or with status {@code 400 (Bad Request)} if the uploadedFile has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/uploaded-files")
    public ResponseEntity<UploadedFileDTO> createUploadedFile(@RequestBody UploadedFileDTO uploadedFileDTO) throws URISyntaxException {
        log.debug("REST request to save UploadedFile : {}", uploadedFileDTO);
        if (uploadedFileDTO.getId() != null) {
            throw new BadRequestAlertException("A new uploadedFile cannot already have an ID", ENTITY_NAME, "idExists");
        }
        UploadedFileDTO result = uploadedFileService.save(uploadedFileDTO);
        return ResponseEntity
            .created(new URI("/api/uploaded-files/" + result.getId()))
            .headers(createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getOriginalName()))
            .body(result);
    }

    /**
     * {@code GET  /uploaded-files} : get all the uploadedFiles.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of uploadedFiles in body.
     */
    @GetMapping("/uploaded-files")
    public ResponseEntity<List<UploadedFileDTO>> getAllUploadedFiles(Pageable pageable) {
        log.debug("REST request to get a page of UploadedFiles");
        Page<UploadedFileDTO> page = uploadedFileService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /uploaded-files/:id} : get the "id" uploadedFile.
     *
     * @param id the id of the uploadedFileDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the uploadedFileDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/uploaded-files/{id}")
    public ResponseEntity<UploadedFileDTO> getUploadedFile(@PathVariable Long id) {
        log.debug("REST request to get UploadedFile : {}", id);
        Optional<UploadedFileDTO> uploadedFileDTO = uploadedFileService.findOne(id);
        return ResponseUtil.wrapOrNotFound(uploadedFileDTO);
    }

    /**
     * {@code DELETE  /uploaded-files/:id} : delete the "id" uploadedFile.
     *
     * @param id the id of the uploadedFileDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/uploaded-files/{id}")
    public ResponseEntity<Void> deleteUploadedFile(@PathVariable Long id) {
        log.debug("REST request to delete UploadedFile : {}", id);
        uploadedFileService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
