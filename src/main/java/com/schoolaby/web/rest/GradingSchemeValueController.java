package com.schoolaby.web.rest;

import com.schoolaby.service.GradingSchemeValueService;
import com.schoolaby.service.dto.GradingSchemeValueDTO;
import com.schoolaby.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import static com.schoolaby.security.SecurityUtils.IS_ADMIN;
import static io.github.jhipster.web.util.HeaderUtil.*;

@RestController
@RequestMapping("/api")
@PreAuthorize(IS_ADMIN)
public class GradingSchemeValueController {
    private final Logger log = LoggerFactory.getLogger(GradingSchemeValueController.class);

    private static final String ENTITY_NAME = "gradingSchemeValue";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final GradingSchemeValueService gradingSchemeValueService;

    public GradingSchemeValueController(GradingSchemeValueService gradingSchemeValueService) {
        this.gradingSchemeValueService = gradingSchemeValueService;
    }

    /**
     * {@code POST  /grading-scheme-values} : Create a new gradingSchemeValue.
     *
     * @param gradingSchemeValueDTO the gradingSchemeValueDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new gradingSchemeValueDTO, or with status {@code 400 (Bad Request)} if the gradingSchemeValue has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/grading-scheme-values")
    public ResponseEntity<GradingSchemeValueDTO> createGradingSchemeValue(@Valid @RequestBody GradingSchemeValueDTO gradingSchemeValueDTO)
        throws URISyntaxException {
        log.debug("REST request to save GradingSchemeValue : {}", gradingSchemeValueDTO);
        if (gradingSchemeValueDTO.getId() != null) {
            throw new BadRequestAlertException("A new gradingSchemeValue cannot already have an ID", ENTITY_NAME, "idExists");
        }
        GradingSchemeValueDTO result = gradingSchemeValueService.create(gradingSchemeValueDTO);
        return ResponseEntity
            .created(new URI("/api/grading-scheme-values/" + result.getId()))
            .headers(createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getGrade()))
            .body(result);
    }

    /**
     * {@code PUT  /grading-scheme-values} : Updates an existing gradingSchemeValue.
     *
     * @param gradingSchemeValueDTO the gradingSchemeValueDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated gradingSchemeValueDTO,
     * or with status {@code 400 (Bad Request)} if the gradingSchemeValueDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the gradingSchemeValueDTO couldn't be updated.
     */
    @PutMapping("/grading-scheme-values")
    public ResponseEntity<GradingSchemeValueDTO> updateGradingSchemeValue(@Valid @RequestBody GradingSchemeValueDTO gradingSchemeValueDTO) {
        log.debug("REST request to update GradingSchemeValue : {}", gradingSchemeValueDTO);
        if (gradingSchemeValueDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idNull");
        }
        GradingSchemeValueDTO result = gradingSchemeValueService.update(gradingSchemeValueDTO);
        return ResponseEntity
            .ok()
            .headers(createEntityUpdateAlert(applicationName, true, ENTITY_NAME, gradingSchemeValueDTO.getGrade()))
            .body(result);
    }

    /**
     * {@code GET  /grading-scheme-values} : get all the gradingSchemeValues.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of gradingSchemeValues in body.
     */
    @GetMapping("/grading-scheme-values")
    public ResponseEntity<List<GradingSchemeValueDTO>> getAllGradingSchemeValues(Pageable pageable) {
        log.debug("REST request to get a page of GradingSchemeValues");
        Page<GradingSchemeValueDTO> page = gradingSchemeValueService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /grading-scheme-values/:id} : get the "id" gradingSchemeValue.
     *
     * @param id the id of the gradingSchemeValueDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the gradingSchemeValueDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/grading-scheme-values/{id}")
    public ResponseEntity<GradingSchemeValueDTO> getGradingSchemeValue(@PathVariable Long id) {
        log.debug("REST request to get GradingSchemeValue : {}", id);
        Optional<GradingSchemeValueDTO> gradingSchemeValueDTO = gradingSchemeValueService.findOne(id);
        return ResponseUtil.wrapOrNotFound(gradingSchemeValueDTO);
    }

    /**
     * {@code DELETE  /grading-scheme-values/:id} : delete the "id" gradingSchemeValue.
     *
     * @param id the id of the gradingSchemeValueDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/grading-scheme-values/{id}")
    public ResponseEntity<Void> deleteGradingSchemeValue(@PathVariable Long id) {
        log.debug("REST request to delete GradingSchemeValue : {}", id);
        gradingSchemeValueService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
