package com.schoolaby.web.rest;

import com.schoolaby.config.HarIdAuthenticationToken;
import com.schoolaby.domain.User;
import com.schoolaby.security.harid.HarIdProperties;
import com.schoolaby.security.jwt.JWTFilter;
import com.schoolaby.security.jwt.TokenProvider;
import com.schoolaby.service.UserService;
import com.schoolaby.web.rest.vm.JWTToken;
import com.schoolaby.web.rest.vm.LoginVM;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;

import static com.schoolaby.security.OauthSecurityProblemSupport.OAUTH_SERVER_REQUEST;
import static com.schoolaby.security.SecurityUtils.IS_AUTHENTICATED;

@RestController
@RequestMapping("/api")
public class AuthenticationController {
    private static final String HARID_AUTHENTICATION_URL =
        "%s?client_id=%s&redirect_uri=%s&scope=openid+profile+personal_code+roles+phone+email&response_type=code";

    private final TokenProvider tokenProvider;
    private final AuthenticationManagerBuilder authenticationManagerBuilder;
    private final HarIdProperties configuration;
    private final UserService userService;
    private final SavedRequestAwareAuthenticationSuccessHandler handler = new SavedRequestAwareAuthenticationSuccessHandler();

    public AuthenticationController(
        TokenProvider tokenProvider,
        AuthenticationManagerBuilder authenticationManagerBuilder,
        HarIdProperties configuration,
        UserService userService
    ) {
        this.tokenProvider = tokenProvider;
        this.authenticationManagerBuilder = authenticationManagerBuilder;
        this.configuration = configuration;
        this.userService = userService;
    }

    @GetMapping("/harid")
    public void redirectToHarid(HttpServletResponse httpServletResponse) throws IOException {
        httpServletResponse.sendRedirect(getHarIdAuthenticationUrl());
    }

    @GetMapping("/harid/success")
    public ResponseEntity<JWTToken> haridSuccess(@RequestParam(value = "code") String code) {
        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(new HarIdAuthenticationToken(code, code));
        return authorize(authentication);
    }

    @PostMapping("/authenticate")
    public void authorize(@Valid @RequestBody LoginVM loginVM, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
            loginVM.getUsername(),
            loginVM.getPassword()
        );

        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        redirectOrReturnJWT(request, response, authentication);
    }

    @PostMapping("/authenticate/oauth")
    public void authorizeOauth(@Valid LoginVM loginVM, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        authorize(loginVM, request, response);
    }

    @PreAuthorize(IS_AUTHENTICATED)
    @GetMapping("/authenticate/refresh")
    public ResponseEntity<JWTToken> refresh() {
        User currentUser = userService.getUserWithAuthorities().orElseThrow(() -> new RuntimeException("Requesting user not found"));
        String jwt = tokenProvider.createToken(currentUser);

        return addJwtToHeaders(jwt);
    }

    private void redirectOrReturnJWT(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws ServletException, IOException {
        if (request.getSession().getAttribute(OAUTH_SERVER_REQUEST) != null) {
            request.getSession().removeAttribute(OAUTH_SERVER_REQUEST);
            com.schoolaby.security.User usr = (com.schoolaby.security.User) authentication.getPrincipal();
            String jwt = tokenProvider.createToken(authentication, usr.getId());
            Cookie cookie = new Cookie("jhi-authenticationToken", jwt);
            cookie.setPath("/");
            response.addCookie(cookie);
            handler.onAuthenticationSuccess(request, response, SecurityContextHolder.getContext().getAuthentication());
        } else {
            writeJwtResponse(authentication, response);
        }
    }

    private void writeJwtResponse(Authentication authentication, HttpServletResponse response) throws IOException {
        com.schoolaby.security.User user = (com.schoolaby.security.User) authentication.getPrincipal();
        String jwt = tokenProvider.createToken(authentication, user.getId());
        response.setHeader(JWTFilter.AUTHORIZATION_HEADER, "Bearer " + jwt);
        response.setStatus(HttpServletResponse.SC_OK);
        response.getWriter().write("{\"id_token\":\"" + jwt + "\"}");
    }

    private ResponseEntity<JWTToken> authorize(Authentication authentication) {
        SecurityContextHolder.getContext().setAuthentication(authentication);
        com.schoolaby.security.User user = (com.schoolaby.security.User) authentication.getPrincipal();
        String jwt = tokenProvider.createToken(authentication, user.getId());

        return addJwtToHeaders(jwt);
    }

    private ResponseEntity<JWTToken> addJwtToHeaders(String jwt) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(JWTFilter.AUTHORIZATION_HEADER, "Bearer " + jwt);

        return new ResponseEntity<>(new JWTToken(jwt), httpHeaders, HttpStatus.OK);
    }

    private String getHarIdAuthenticationUrl() {
        return String.format(
            HARID_AUTHENTICATION_URL,
            configuration.getAuthorizeUrl(),
            configuration.getClientId(),
            configuration.getCallbackUrl()
        );
    }
}
