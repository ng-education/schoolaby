package com.schoolaby.web.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.jwk.JWK;
import com.schoolaby.common.AutoPostingForm;
import com.schoolaby.common.LtiContextHelper;
import com.schoolaby.domain.LtiScore;
import com.schoolaby.service.LtiAdvantageService;
import com.schoolaby.service.LtiGradingService;
import com.schoolaby.service.dto.lti.external.LtiExtLineItemDTO;
import com.schoolaby.service.dto.lti.external.LtiExtScoreDTO;
import com.schoolaby.service.dto.lti.security.AccessTokenResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.net.URI;
import java.util.Collection;

import static com.schoolaby.security.SecurityUtils.*;
import static com.schoolaby.web.rest.LtiController.DEEP_LINKING_RESPONSE_TEMPLATE;
import static java.lang.Long.parseLong;
import static java.lang.String.format;
import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/api/lti-advantage")
@RequiredArgsConstructor
public class LtiAdvantageController {
    public static final String APPLICATION_VND_IMS_LIS_V_1_SCORE_JSON = "application/vnd.ims.lis.v1.score+json";
    public static final String APPLICATION_VND_IMS_LIS_V_2_LINEITEMCONTAINER_JSON = "application/vnd.ims.lis.v2.lineitemcontainer+json";
    private final LtiAdvantageService service;
    private final JWK ltiJwk;
    private final ObjectMapper objectMapper;
    private final LtiGradingService ltiGradingService;
    private final LtiContextHelper contextHelper;

    @PostMapping("deep-linking")
    @PreAuthorize(IS_TEACHER_OR_ADMIN)
    public RedirectView deepLinking(@RequestParam Long appId, @RequestParam Long journeyId, RedirectAttributes attributes) throws JOSEException {
        return service.redirectDeepLinkingRequestToTool(appId, journeyId, attributes);
    }

    @PostMapping("launch")
    @PreAuthorize(IS_STUDENT_OR_TEACHER_OR_ADMIN)
    public RedirectView launch(@RequestParam(required = false) Long resourceId,
                               @RequestParam(required = false) Long appId,
                               @RequestParam(required = false) Long journeyId,
                               @RequestParam(required = false) String resourceLinkId,
                               @RequestParam(required = false) String appClientId,
                               @RequestParam(required = false) String ltiConfigType,
                               @RequestParam(required = false) String studentId,
                               RedirectAttributes attributes) throws JOSEException {
        if (resourceId != null) {
            return service.redirectResourceLaunchToTool(resourceId, studentId, attributes);
        } else if (!isCurrentUserTeacherOrAdmin()) {
            throw new AccessDeniedException("Not authorized to create new LTI resources in LTI tool!");
        } else {
            return service.redirectAppLaunchToTool(appId, journeyId, resourceLinkId, appClientId, ltiConfigType, attributes);
        }
    }

    @GetMapping(value = "authorize", produces = "text/html")
    public ResponseEntity<String> authorize(@RequestParam(name = "login_hint") String loginHint,
                                            @RequestParam(name = "lti_message_hint") String messageHint,
                                            @RequestParam(name = "redirect_uri", required = false) String redirectUri,
                                            @RequestParam(required = false) String state,
                                            @RequestParam(required = false) String nonce) throws JOSEException {
        String idToken = service.createIdToken(loginHint, messageHint, nonce, URI.create(redirectUri));
        return ok(new AutoPostingForm(redirectUri)
            .addParam("state", state)
            .addParam("id_token", idToken)
            .toString()
        );
    }

    @PostMapping(value = "deep-linking/response", produces = "text/html")
    public ResponseEntity<Object> deepLinkingResponse(@ModelAttribute("JWT") String jwtParam) throws JsonProcessingException {
        String response = objectMapper.writeValueAsString(service.parseToolClaims(jwtParam)
            .getBody()
            .get("https://purl.imsglobal.org/spec/lti-dl/claim/content_items"));
        return ok(format(DEEP_LINKING_RESPONSE_TEMPLATE, response));
    }

    @GetMapping("keys")
    public ResponseEntity<String> keys() {
        return ok(format("{\"keys\": [%s]}", ltiJwk.toPublicJWK().toString()));
    }

    @PostMapping("token")
    public ResponseEntity<AccessTokenResponse> token(@RequestParam(name = "client_assertion") String toolJwt, @RequestParam String scope) {
        service.parseToolClaims(toolJwt);
        // SCHOOL-123
        return ok(new AccessTokenResponse("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c", scope));
    }

    @GetMapping(value = "{context}/lineitems", produces = APPLICATION_VND_IMS_LIS_V_2_LINEITEMCONTAINER_JSON)
    public ResponseEntity<Collection<LtiExtLineItemDTO>> getLineItems(@PathVariable String context,
                                                                      @RequestParam(value = "resource_link_id", required = false) String resourceLinkId) {
        return ok(ltiGradingService.findAllLineItems(contextHelper.parse(context).getJourneyId(), resourceLinkId));
    }

    @PostMapping(value = "{context}/lineitems/{lineItemId}/scores", consumes = APPLICATION_VND_IMS_LIS_V_1_SCORE_JSON)
    public void postScores(@PathVariable String context, @PathVariable String lineItemId, @RequestBody LtiExtScoreDTO score) {
        Long scoreId = ltiGradingService.createScore(parseLong(lineItemId), score);
        ltiGradingService.sendScoreToWebSocket(scoreId);
    }

}
