package com.schoolaby.web.rest;

import com.schoolaby.service.EducationalAlignmentService;
import com.schoolaby.service.dto.EducationalAlignmentDTO;
import com.schoolaby.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import static com.schoolaby.security.SecurityUtils.IS_ADMIN;
import static io.github.jhipster.web.util.HeaderUtil.*;

@RestController
@RequestMapping("/api")
public class EducationalAlignmentController {
    private final Logger log = LoggerFactory.getLogger(EducationalAlignmentController.class);

    private static final String ENTITY_NAME = "educationalAlignment";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EducationalAlignmentService educationalAlignmentService;

    public EducationalAlignmentController(EducationalAlignmentService educationalAlignmentService) {
        this.educationalAlignmentService = educationalAlignmentService;
    }

    /**
     * {@code POST  /educational-alignments} : Create a new educationalAlignment.
     *
     * @param educationalAlignmentDTO the educationalAlignmentDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new educationalAlignmentDTO, or with status {@code 400 (Bad Request)} if the educationalAlignment has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/educational-alignments")
    @PreAuthorize(IS_ADMIN)
    public ResponseEntity<EducationalAlignmentDTO> createEducationalAlignment(
        @Valid @RequestBody EducationalAlignmentDTO educationalAlignmentDTO
    )
        throws URISyntaxException {
        log.debug("REST request to save EducationalAlignment : {}", educationalAlignmentDTO);
        if (educationalAlignmentDTO.getId() != null) {
            throw new BadRequestAlertException("A new educationalAlignment cannot already have an ID", ENTITY_NAME, "idExists");
        }
        EducationalAlignmentDTO result = educationalAlignmentService.create(educationalAlignmentDTO);
        return ResponseEntity
            .created(new URI("/api/educational-alignments/" + result.getId()))
            .headers(createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getTargetName()))
            .body(result);
    }

    /**
     * {@code PUT  /educational-alignments} : Updates an existing educationalAlignment.
     *
     * @param educationalAlignmentDTO the educationalAlignmentDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated educationalAlignmentDTO,
     * or with status {@code 400 (Bad Request)} if the educationalAlignmentDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the educationalAlignmentDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/educational-alignments")
    @PreAuthorize(IS_ADMIN)
    public ResponseEntity<EducationalAlignmentDTO> updateEducationalAlignment(
        @Valid @RequestBody EducationalAlignmentDTO educationalAlignmentDTO
    )
        throws URISyntaxException {
        log.debug("REST request to update EducationalAlignment : {}", educationalAlignmentDTO);
        if (educationalAlignmentDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idNull");
        }
        EducationalAlignmentDTO result = educationalAlignmentService.update(educationalAlignmentDTO);
        return ResponseEntity
            .ok()
            .headers(createEntityUpdateAlert(applicationName, true, ENTITY_NAME, educationalAlignmentDTO.getTargetName()))
            .body(result);
    }

    @GetMapping("/educational-alignments")
    public ResponseEntity<List<EducationalAlignmentDTO>> getAllEducationalAlignments(
        Pageable pageable,
        @RequestParam(required = false) String title,
        @RequestParam(required = false) String alignmentType,
        @RequestParam(defaultValue = "Estonia") String country) {
        log.debug("REST request to get a page of EducationalAlignments");
        Page<EducationalAlignmentDTO> page;
        page = educationalAlignmentService.findAll(pageable, alignmentType, title, country);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /educational-alignments/:id} : get the "id" educationalAlignment.
     *
     * @param id the id of the educationalAlignmentDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the educationalAlignmentDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/educational-alignments/{id}")
    public ResponseEntity<EducationalAlignmentDTO> getEducationalAlignment(@PathVariable Long id) {
        log.debug("REST request to get EducationalAlignment : {}", id);
        Optional<EducationalAlignmentDTO> educationalAlignmentDTO = educationalAlignmentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(educationalAlignmentDTO);
    }

    /**
     * {@code DELETE  /educational-alignments/:id} : delete the "id" educationalAlignment.
     *
     * @param id the id of the educationalAlignmentDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/educational-alignments/{id}")
    @PreAuthorize(IS_ADMIN)
    public ResponseEntity<Void> deleteEducationalAlignment(@PathVariable Long id) {
        log.debug("REST request to delete EducationalAlignment : {}", id);
        educationalAlignmentService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
