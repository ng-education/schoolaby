package com.schoolaby.web.rest;

import com.schoolaby.service.SchoolService;
import com.schoolaby.service.dto.SchoolDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/api/schools")
@RequiredArgsConstructor
public class SchoolController {
    private final SchoolService schoolService;

    @GetMapping
    public ResponseEntity<Set<SchoolDTO>> getSchoolsBySearch(@RequestParam String search, @RequestParam String country) {
        return ok(schoolService.findSchoolsBySearch(search, country));
    }
}
