package com.schoolaby.web.rest;

import com.schoolaby.service.RubricService;
import com.schoolaby.service.dto.RubricDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Set;

import static com.schoolaby.security.SecurityUtils.IS_STUDENT_OR_TEACHER_OR_ADMIN;
import static com.schoolaby.security.SecurityUtils.IS_TEACHER_OR_ADMIN;
import static org.springframework.http.ResponseEntity.*;

@RestController
@RequestMapping("api/rubrics")
@PreAuthorize(IS_TEACHER_OR_ADMIN)
public class RubricController {
    private final Logger log = LoggerFactory.getLogger(RubricController.class);

    private final RubricService rubricService;

    public RubricController(RubricService rubricService) {
        this.rubricService = rubricService;
    }

    @GetMapping("/templates")
    public ResponseEntity<Set<RubricDTO>> findRubricTemplatesBySearch(@RequestParam(required = false, defaultValue = "") String search) {
        log.debug("REST request to search Rubrics : {}", search);
        Set<RubricDTO> rubrics = rubricService.findAllRubricTemplatesBySearch(search);
        return ok(rubrics);
    }

    @PreAuthorize(IS_STUDENT_OR_TEACHER_OR_ADMIN)
    @GetMapping
    public ResponseEntity<RubricDTO> findRubricByAssignmentId(@RequestParam Long assignmentId) {
        log.debug("REST request to get Rubric by Assignment id : {}", assignmentId);
        return ok(rubricService.findDTOByAssignmentId(assignmentId));
    }

    @PostMapping
    public ResponseEntity<RubricDTO> createRubric(@RequestBody RubricDTO rubric) throws URISyntaxException {
        log.debug("REST request to save Rubric : {}", rubric);
        RubricDTO savedRubric = rubricService.saveDTO(rubric);
        return created(new URI("/api/assignments/" + savedRubric.getId()))
            .body(savedRubric);
    }

    @PutMapping
    public ResponseEntity<RubricDTO> updateRubric(@RequestBody RubricDTO rubric) {
        log.debug("REST request to update Rubric : {}", rubric);
        return ok(rubricService.updateRubric(rubric));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteRubric(@PathVariable Long id) {
        log.debug("REST request to delete Rubric : {}", id);
        rubricService.deleteById(id);
        return noContent().build();
    }
}
