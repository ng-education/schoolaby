package com.schoolaby.web.rest;

import com.schoolaby.domain.enumeration.EntityType;
import com.schoolaby.service.ChatService;
import com.schoolaby.service.dto.ChatDTO;
import com.schoolaby.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import static com.schoolaby.security.SecurityUtils.IS_AUTHENTICATED;
import static com.schoolaby.security.SecurityUtils.IS_STUDENT_OR_TEACHER_OR_ADMIN;
import static io.github.jhipster.web.util.HeaderUtil.createEntityCreationAlert;
import static io.github.jhipster.web.util.PaginationUtil.generatePaginationHttpHeaders;
import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

@RestController
@RequestMapping("/api/chats")
public class ChatController {
    private final Logger log = getLogger(ChatController.class);

    private static final String ENTITY_NAME = "chat";
    public static final String URL_CHATS = "/chats";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ChatService chatService;

    public ChatController(ChatService chatService) {
        this.chatService = chatService;
    }

    @GetMapping
    @PreAuthorize(IS_AUTHENTICATED)
    public ResponseEntity<List<ChatDTO>> getAllChats(
        Pageable pageable,
        @RequestParam(required = false) EntityType entity,
        @RequestParam(required = false) Long journeyId,
        @RequestParam(required = false) Long submissionId
    ) {
        log.debug("REST request to get a Set of Chats");
        Page<ChatDTO> page = chatService.getAllChats(pageable, entity, journeyId, submissionId);

        HttpHeaders headers = generatePaginationHttpHeaders(fromCurrentRequest(), page);
        return ok().headers(headers).body(page.getContent());
    }

    @PostMapping
    @PreAuthorize(IS_STUDENT_OR_TEACHER_OR_ADMIN)
    public ResponseEntity<ChatDTO> createChat(@Valid @RequestBody ChatDTO chatDTO) throws URISyntaxException {
        log.debug("REST request to save Chat : {}", chatDTO);
        if (chatDTO.getId() != null) {
            throw new BadRequestAlertException("A new chat cannot already have an ID", ENTITY_NAME, "idExists");
        }
        ChatDTO result = chatService.create(chatDTO);
        return created(new URI("/api/chats/" + result.getId()))
            .headers(createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getTitle()))
            .body(result);
    }
}
