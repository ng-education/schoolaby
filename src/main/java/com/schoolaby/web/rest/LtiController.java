package com.schoolaby.web.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.schoolaby.domain.LtiContentItemDTO;
import com.schoolaby.service.*;
import com.schoolaby.service.dto.*;
import com.schoolaby.service.dto.lti.LtiResourceDTO;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.schoolaby.common.FileUtil.readFile;
import static com.schoolaby.security.SecurityUtils.IS_STUDENT_OR_TEACHER_OR_ADMIN;
import static com.schoolaby.security.SecurityUtils.IS_TEACHER_OR_ADMIN;
import static com.schoolaby.service.dto.LtiResponse.replaceResult;
import static com.schoolaby.service.dto.LtiResponse.unsupported;
import static io.github.jhipster.web.util.HeaderUtil.*;
import static java.lang.String.format;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED_VALUE;
import static org.springframework.http.MediaType.APPLICATION_XML_VALUE;
import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/api/lti")
@RequiredArgsConstructor
public class LtiController {
    private static final String ENTITY_NAME = "ltiConfig";
    private final Logger log = LoggerFactory.getLogger(LtiController.class);
    public static final String APPLICATION_XML_UTF_8 = APPLICATION_XML_VALUE + ";charset=UTF-8";
    public static final String DEEP_LINKING_RESPONSE_TEMPLATE = readFile("templates/lti-deep-linking-response.html");

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final LtiLaunchService launchService;
    private final LtiConfigService configService;
    private final LtiAppService appService;
    private final LtiResourceService resourceService;
    private final ObjectMapper objectMapper;

    @GetMapping("resource/launch/parameters")
    @PreAuthorize(IS_STUDENT_OR_TEACHER_OR_ADMIN)
    public ResponseEntity<Map<String, Object>> getLtiLaunchParametersForResource(@RequestParam(required = false) Long ltiResourceId, @RequestParam String returnUrl) {
        return new ResponseEntity<>(launchService.launchResource(ltiResourceId, returnUrl), OK);
    }

    @GetMapping("app/launch/parameters")
    @PreAuthorize(IS_TEACHER_OR_ADMIN)
    public ResponseEntity<Map<String, Object>> getLtiLaunchParametersForApp(
        @RequestParam Long ltiAppId,
        @RequestParam Long journeyId,
        @RequestParam String ltiResourceLinkId,
        @RequestParam String returnUrl
    ) {
        return new ResponseEntity<>(launchService.launchApp(ltiAppId, journeyId, ltiResourceLinkId, returnUrl), OK);
    }

    @PostMapping(value = "results", produces = APPLICATION_XML_UTF_8, consumes = APPLICATION_XML_VALUE)
    public ResponseEntity<String> results(@RequestBody String request) {
        log.info(String.format("Got LTI result payload: %s", request));
        LtiRequest ltiRequest;
        try {
            ltiRequest = LtiRequestFactory.parseLtiRequest(request);
        } catch (Exception e) {
            log.error("Failed to parse request!", e);
            return ResponseEntity.badRequest().build();
        }

        if (ltiRequest instanceof ReplaceResultRequest) {
            ReplaceResultRequest replaceResultRequest = (ReplaceResultRequest) ltiRequest;
            LtiLaunchDTO launch = launchService.replaceResult(replaceResultRequest.getSourcedId(), replaceResultRequest.getResultScore());
            return ok(replaceResult(replaceResultRequest.getMessageId(), launch.getId(), launch.getResult()));
        }

        return ok(unsupported(ltiRequest.getMessageId(), ltiRequest.getOperation()));
    }

    @PostMapping("config")
    @PreAuthorize(IS_TEACHER_OR_ADMIN)
    public ResponseEntity<LtiConfigDTO> saveLtiConfig(@RequestBody LtiConfigDTO ltiConfigDTO) {
        final LtiConfigDTO ltiConfig = configService.create(ltiConfigDTO);
        return new ResponseEntity<>(
            ltiConfig,
            createEntityCreationAlert(applicationName, true, ENTITY_NAME, ltiConfig.getLtiApp().getName()),
            HttpStatus.CREATED
        );
    }

    @PutMapping("config")
    @PreAuthorize(IS_TEACHER_OR_ADMIN)
    public ResponseEntity<LtiConfigDTO> updateLtiConfig(@RequestBody LtiConfigDTO ltiConfigDTO) {
        final LtiConfigDTO ltiConfig = configService.update(ltiConfigDTO);
        return new ResponseEntity<>(
            ltiConfig,
            createEntityUpdateAlert(applicationName, true, ENTITY_NAME, ltiConfig.getLtiApp().getName()),
            OK
        );
    }

    @GetMapping("config")
    @PreAuthorize(IS_TEACHER_OR_ADMIN)
    public ResponseEntity<List<LtiConfigDTO>> getLtiConfig(@RequestParam Long journeyId) {
        final List<LtiConfigDTO> apps = configService.findAll(journeyId);
        return new ResponseEntity<>(apps, OK);
    }

    @DeleteMapping("config/{id}")
    @PreAuthorize(IS_TEACHER_OR_ADMIN)
    public ResponseEntity<Void> deleteLtiConfig(@PathVariable Long id) {
        configService.deleteLtiConfig(id);
        return ResponseEntity
            .noContent()
            .headers(createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("app")
    @PreAuthorize(IS_TEACHER_OR_ADMIN)
    public ResponseEntity<List<LtiAppDTO>> getLtiApps(@RequestParam(required = false) Long journeyId) {
        final List<LtiAppDTO> apps = appService.findAll(journeyId);
        return new ResponseEntity<>(apps, OK);
    }

    @GetMapping("resource")
    public ResponseEntity<List<LtiResourceDTO>> getLtiResources(@RequestParam(required = false) Long milestoneId, @RequestParam(required = false) Long assignmentId) {
        final List<LtiResourceDTO> resources = resourceService.findAll(milestoneId, assignmentId);
        return new ResponseEntity<>(resources, OK);
    }

    @GetMapping("launch")
    public ResponseEntity<List<LtiLaunchDTO>> getLtiLaunches(@RequestParam Long assignmentId, @RequestParam(required = false) Long userId) {
        final List<LtiLaunchDTO> launches = launchService.findLtiLaunches(assignmentId, userId);
        return new ResponseEntity<>(launches, OK);
    }

    @PostMapping(value = "deep-linking/response", consumes = {APPLICATION_FORM_URLENCODED_VALUE}, produces = "text/html")
    public ResponseEntity<Object> saveContentItem(@RequestBody MultiValueMap<String, String> formParams) throws JsonProcessingException {
        String response = objectMapper.readTree(formParams.getFirst("content_items")).get("@graph").toPrettyString();
        return ok(format(DEEP_LINKING_RESPONSE_TEMPLATE, response));
    }
}
