package com.schoolaby.web.rest;

import com.schoolaby.service.SubmissionService;
import com.schoolaby.service.dto.submission.SubmissionDTO;
import com.schoolaby.service.dto.submission.SubmissionPatchDTO;
import com.schoolaby.service.dto.submission.SubmissionResubmitPatchDTO;
import com.schoolaby.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import static com.schoolaby.security.SecurityUtils.IS_ADMIN;
import static com.schoolaby.security.SecurityUtils.IS_STUDENT_OR_TEACHER_OR_ADMIN;
import static io.github.jhipster.web.util.HeaderUtil.*;
import static io.github.jhipster.web.util.PaginationUtil.generatePaginationHttpHeaders;
import static io.github.jhipster.web.util.ResponseUtil.wrapOrNotFound;
import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.http.ResponseEntity.noContent;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

@RestController
@RequestMapping("/api")
public class SubmissionController {
    private static final String ENTITY_NAME = "submission";
    private final Logger log = getLogger(SubmissionController.class);

    @Value("${jhipster.clientApp.name}")
    private String applicationName;
    private final SubmissionService submissionService;

    public SubmissionController(SubmissionService submissionService) {
        this.submissionService = submissionService;
    }

    @PostMapping("/submissions")
    @PreAuthorize(IS_STUDENT_OR_TEACHER_OR_ADMIN)
    public ResponseEntity<SubmissionDTO> create(@Valid @RequestBody SubmissionDTO submissionDTO) throws URISyntaxException {
        log.debug("REST request to save Submission : {}", submissionDTO);
        if (submissionDTO.getSubmissionFeedbacks() != null && !submissionDTO.getSubmissionFeedbacks().isEmpty()) {
            throw new BadRequestAlertException("Can not create submission with feedbacks", ENTITY_NAME, "submissionwithfeedbacks");
        }
        if (submissionDTO.getId() != null) {
            throw new BadRequestAlertException("A new submission cannot already have an ID", ENTITY_NAME, "idExists");
        }

        SubmissionDTO result = submissionService.create(submissionDTO);
        return ResponseEntity
            .created(new URI("/api/submissions/" + result.getId()))
            .headers(createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getAssignmentTitle()))
            .body(result);
    }

    @PatchMapping("/submissions/{id}")
    @PreAuthorize(IS_STUDENT_OR_TEACHER_OR_ADMIN)
    public ResponseEntity<SubmissionDTO> patch(@PathVariable("id") Long id,
                                               @Valid @RequestBody SubmissionPatchDTO submissionPatchDTO) {
        log.debug("REST request to partially update Submission : {}", submissionPatchDTO);

        SubmissionDTO submission;
        if (submissionPatchDTO instanceof SubmissionResubmitPatchDTO) {
            submission = submissionService.resubmit(id, (SubmissionResubmitPatchDTO) submissionPatchDTO);
        } else {
            throw new RuntimeException("Invalid submissionPatchDTO");
        }

        return ok()
            .headers(createEntityUpdateAlert(applicationName, true, ENTITY_NAME, submission.getAssignmentTitle()))
            .body(submission);
    }

    @PutMapping("/submissions")
    @PreAuthorize(IS_ADMIN)
    public ResponseEntity<SubmissionDTO> update(@Valid @RequestBody SubmissionDTO submissionDTO) {
        log.debug("REST request to update Submission : {}", submissionDTO);
        if (submissionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idNull");
        }
        SubmissionDTO result = submissionService.update(submissionDTO);
        return ok()
            .headers(createEntityUpdateAlert(applicationName, true, ENTITY_NAME, submissionDTO.getAssignmentTitle()))
            .body(result);
    }

    @GetMapping("/submissions/{id}")
    @PreAuthorize(IS_STUDENT_OR_TEACHER_OR_ADMIN)
    public ResponseEntity<SubmissionDTO> get(@PathVariable Long id) {
        log.debug("REST request to get Submission : {}", id);
        Optional<SubmissionDTO> submissionDTO = submissionService.findOne(id);
        return wrapOrNotFound(submissionDTO);
    }

    @GetMapping("/submissions")
    @PreAuthorize(IS_STUDENT_OR_TEACHER_OR_ADMIN)
    public ResponseEntity<List<SubmissionDTO>> findAll(Pageable pageable, @RequestParam(required = false) Long assignmentId, @RequestParam(required = false) Long journeyId, @RequestParam(required = false) boolean unPaged) {
        log.debug("REST request to get Submissions page with params assignmentId : {}", assignmentId);
        Pageable page = pageable;
        if (unPaged) {
            page = Pageable.unpaged();
        }
        Page<SubmissionDTO> result = submissionService.findAll(page, assignmentId, journeyId);

        return ok()
            .headers(generatePaginationHttpHeaders(fromCurrentRequest(), result))
            .body(result.getContent());
    }

    @DeleteMapping("/submissions/{id}")
    @PreAuthorize(IS_ADMIN)
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        log.debug("REST request to delete Submission : {}", id);
        submissionService.delete(id);
        return noContent()
            .headers(createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
