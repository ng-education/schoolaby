package com.schoolaby.web.rest;

import com.schoolaby.service.PersonRoleService;
import com.schoolaby.service.dto.PersonRoleDTO;
import com.schoolaby.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import static com.schoolaby.security.SecurityUtils.IS_ADMIN;
import static io.github.jhipster.web.util.HeaderUtil.*;

@RestController
@RequestMapping("/api")
@PreAuthorize(IS_ADMIN)
public class PersonRoleController {
    private final Logger log = LoggerFactory.getLogger(PersonRoleController.class);

    private static final String ENTITY_NAME = "personRole";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PersonRoleService personRoleService;

    public PersonRoleController(PersonRoleService personRoleService) {
        this.personRoleService = personRoleService;
    }

    /**
     * {@code POST  /person-roles} : Create a new personRole.
     *
     * @param personRoleDTO the personRoleDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new personRoleDTO, or with status {@code 400 (Bad Request)} if the personRole has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/person-roles")
    public ResponseEntity<PersonRoleDTO> createPersonRole(@RequestBody PersonRoleDTO personRoleDTO) throws URISyntaxException {
        log.debug("REST request to save PersonRole : {}", personRoleDTO);
        if (personRoleDTO.getId() != null) {
            throw new BadRequestAlertException("A new personRole cannot already have an ID", ENTITY_NAME, "idExists");
        }
        PersonRoleDTO result = personRoleService.save(personRoleDTO);
        return ResponseEntity
            .created(new URI("/api/person-roles/" + result.getId()))
            .headers(createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getRole().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /person-roles} : Updates an existing personRole.
     *
     * @param personRoleDTO the personRoleDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated personRoleDTO,
     * or with status {@code 400 (Bad Request)} if the personRoleDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the personRoleDTO couldn't be updated.
     */
    @PutMapping("/person-roles")
    public ResponseEntity<PersonRoleDTO> updatePersonRole(@RequestBody PersonRoleDTO personRoleDTO) {
        log.debug("REST request to update PersonRole : {}", personRoleDTO);
        if (personRoleDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idNull");
        }
        PersonRoleDTO result = personRoleService.save(personRoleDTO);
        return ResponseEntity
            .ok()
            .headers(createEntityUpdateAlert(applicationName, true, ENTITY_NAME, personRoleDTO.getRole().toString()))
            .body(result);
    }

    /**
     * {@code GET  /person-roles} : get all the personRoles.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of personRoles in body.
     */
    @GetMapping("/person-roles")
    public ResponseEntity<List<PersonRoleDTO>> getAllPersonRoles(Pageable pageable) {
        log.debug("REST request to get a page of PersonRoles");
        Page<PersonRoleDTO> page = personRoleService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /person-roles/:id} : get the "id" personRole.
     *
     * @param id the id of the personRoleDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the personRoleDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/person-roles/{id}")
    public ResponseEntity<PersonRoleDTO> getPersonRole(@PathVariable Long id) {
        log.debug("REST request to get PersonRole : {}", id);
        Optional<PersonRoleDTO> personRoleDTO = personRoleService.findOne(id);
        return ResponseUtil.wrapOrNotFound(personRoleDTO);
    }

    /**
     * {@code DELETE  /person-roles/:id} : delete the "id" personRole.
     *
     * @param id the id of the personRoleDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/person-roles/{id}")
    public ResponseEntity<Void> deletePersonRole(@PathVariable Long id) {
        log.debug("REST request to delete PersonRole : {}", id);
        personRoleService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
