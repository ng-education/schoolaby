package com.schoolaby.web.rest;

import com.schoolaby.service.SubjectService;
import com.schoolaby.service.dto.SubjectDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping("api/subjects")
@RequiredArgsConstructor
public class SubjectController {
    private final SubjectService subjectService;

    @GetMapping
    public Collection<SubjectDTO> findAllByCountry(@RequestParam(defaultValue = "Estonia") String country) {
        return subjectService.findAllByCountry(country);
    }
}
