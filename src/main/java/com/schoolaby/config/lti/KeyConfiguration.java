package com.schoolaby.config.lti;

import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.RSAKey;
import com.schoolaby.domain.LtiApp;
import com.schoolaby.repository.LtiAdvantageRepository;
import com.schoolaby.service.LtiAppService;
import io.jsonwebtoken.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.Optional;

import static com.nimbusds.jose.jwk.KeyUse.SIGNATURE;
import static java.lang.String.format;
import static java.util.UUID.randomUUID;
import static org.apache.commons.lang3.StringUtils.isEmpty;

@Configuration
public class KeyConfiguration {

    @Bean
    public JWK ltiJwk() throws NoSuchAlgorithmException {
        KeyPair keyPair = KeyPairGenerator.getInstance("RSA")
            .genKeyPair();
        return new RSAKey.Builder((RSAPublicKey) keyPair.getPublic())
            .privateKey((RSAPrivateKey) keyPair.getPrivate())
            .keyUse(SIGNATURE)
            .keyID(randomUUID().toString())
            .build();
    }

    @Bean
    public SigningKeyResolver toolKeyResolver(LtiAdvantageRepository repository, LtiAppService ltiAppService) {
        return new SigningKeyResolverAdapter() {
            @Override
            public Key resolveSigningKey(JwsHeader header, Claims claims) {
                String kid = header.getKeyId();
                if (isEmpty(kid)) {
                    throw new JwtException("Missing required 'kid' header param in JWT with claims: " + claims);
                }
                String clientId = Optional.ofNullable(claims.getSubject()).orElse(claims.getIssuer());
                LtiApp app = ltiAppService.findByClientId(clientId)
                    .orElseThrow(() -> new JwtException(format("Client '%s' not registered", clientId)));
                Key key = repository.getToolPublicKey(app.getJwksUrl(), kid);
                if (key == null) {
                    throw new JwtException("No public key registered for kid: " + kid + ". JWT claims: " + claims);
                }
                return key;
            }
        };
    }
}
