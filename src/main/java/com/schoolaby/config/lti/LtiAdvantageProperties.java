package com.schoolaby.config.lti;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "lti-advantage")
@Getter
@Setter
public class LtiAdvantageProperties {
    private String iss;
    private int tokenValidityInSeconds;
}
