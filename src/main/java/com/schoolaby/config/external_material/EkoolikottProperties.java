package com.schoolaby.config.external_material;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "ekoolikott")
public class EkoolikottProperties {
    private String host;
    private int port = -1;
    private String scheme = "https";
}
