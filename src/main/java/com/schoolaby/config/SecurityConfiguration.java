package com.schoolaby.config;

import com.schoolaby.config.oauth2.OAuth2AuthenticationFailureHandler;
import com.schoolaby.config.oauth2.OAuth2AuthenticationSuccessHandler;
import com.schoolaby.config.oauth2.OauthAuthorizationRequestResolver;
import com.schoolaby.config.oauth2.OauthServerProperties;
import com.schoolaby.security.OauthSecurityProblemSupport;
import com.schoolaby.security.jwt.JWTConfigurer;
import com.schoolaby.security.jwt.TokenProvider;
import com.schoolaby.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.oauth2.server.authorization.OAuth2AuthorizationServerConfigurer;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.client.registration.InMemoryClientRegistrationRepository;
import org.springframework.security.oauth2.server.authorization.JwtEncodingContext;
import org.springframework.security.oauth2.server.authorization.OAuth2TokenCustomizer;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClientRepository;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.filter.CorsFilter;

import static com.schoolaby.security.Role.Constants.ADMIN;
import static com.schoolaby.security.Role.Constants.TEACHER;
import static org.springframework.security.oauth2.core.oidc.endpoint.OidcParameterNames.ID_TOKEN;
import static org.springframework.security.web.header.writers.ReferrerPolicyHeaderWriter.ReferrerPolicy.STRICT_ORIGIN_WHEN_CROSS_ORIGIN;

@EnableWebSecurity
@RequiredArgsConstructor
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
@Import(OauthSecurityProblemSupport.class)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    private final TokenProvider tokenProvider;
    private final CorsFilter corsFilter;
    private final OauthSecurityProblemSupport problemSupport;
    private final HarIdAuthenticationProvider harIdAuthenticationProvider;
    private final UserDetailsService userDetailsService;
    private final OAuth2AuthenticationSuccessHandler oAuth2AuthenticationSuccessHandler;
    private final OAuth2AuthenticationFailureHandler oAuth2AuthenticationFailureHandler;
    private final InMemoryClientRegistrationRepository inMemoryClientRegistrationRepository;
    private final RegisteredClientRepository jdbcRegisteredClientRepository;
    private final UserService userService;
    private final OauthServerProperties oauthServerProperties;

    @Override
    @Autowired
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
        auth.authenticationProvider(harIdAuthenticationProvider);
    }

    @Override
    public void configure(WebSecurity web) {
        web
            .ignoring()
            .antMatchers(HttpMethod.OPTIONS, "/**")
            .antMatchers("/app/**/*.{js,html}")
            .antMatchers("/i18n/**")
            .antMatchers("/content/**")
            .antMatchers("/swagger-ui/index.html")
            .antMatchers("/test/**");
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        OAuth2AuthorizationServerConfigurer<HttpSecurity> authorizationServerConfigurer =
            new OAuth2AuthorizationServerConfigurer<>();

        http
            .csrf()
            .disable()
            .addFilterBefore(corsFilter, UsernamePasswordAuthenticationFilter.class)
            .exceptionHandling()
            .authenticationEntryPoint(problemSupport)
            .accessDeniedHandler(problemSupport)
            .and().headers().contentSecurityPolicy("connect-src * 'unsafe-inline'; frame-src *;")
            .and().referrerPolicy(STRICT_ORIGIN_WHEN_CROSS_ORIGIN)
            .and().permissionsPolicy(permissions -> permissions.policy("geolocation 'none'; midi 'none'; sync-xhr 'none'; microphone 'self'; camera 'self'; magnetometer 'none'; gyroscope 'none'; speaker-selection 'none'; fullscreen 'self' *; payment 'none'"))
            .and().frameOptions().sameOrigin()
            .and().apply(authorizationServerConfigurer)
            .and().authorizeRequests()
            .requestMatchers(authorizationServerConfigurer.getEndpointsMatcher()).authenticated()
            .antMatchers("/api/authenticate").permitAll()
            .antMatchers("/api/authenticate/oauth").permitAll()
            .antMatchers("/api/harid").permitAll()
            .antMatchers("/api/educational-alignments").permitAll()
            .antMatchers("/api/lti/results").permitAll()
            .antMatchers("/api/lti/deep-linking/response").permitAll()
            .antMatchers("/api/lti-advantage/authorize").permitAll()
            .antMatchers("/api/lti-advantage/keys").permitAll()
            .antMatchers("/api/lti-advantage/token").permitAll()
            .antMatchers("/api/lti-advantage/**/lineitems").permitAll()
            .antMatchers("/api/lti-advantage/**/lineitems/**/scores").permitAll()
            .antMatchers("/api/lti-advantage/deep-linking/response").permitAll()
            .antMatchers("/api/harid/success").permitAll()
            .antMatchers("/api/register").permitAll()
            .antMatchers("/api/activate").permitAll()
            .antMatchers("/api/account/reset-password/init").permitAll()
            .antMatchers("/api/account/logout").permitAll()
            .antMatchers("/api/account/reset-password/finish").permitAll()
            .antMatchers("/api/schools").permitAll()
            .antMatchers("/api/suggestions/**").permitAll()
            .antMatchers("/api/proxy/**").permitAll()
            .antMatchers("/api/files/**").permitAll()
            .antMatchers("/websocket/**").permitAll()
            .antMatchers("/management/health").permitAll()
            .antMatchers("/management/info").permitAll()
            .antMatchers("/management/audits").hasAnyAuthority(ADMIN, TEACHER)
            .antMatchers("/management/prometheus").hasAuthority(ADMIN)
            .antMatchers("/websocket/tracker").hasAuthority(ADMIN)
            .antMatchers("/management/**").hasAuthority(ADMIN)
            .antMatchers("/api/**").authenticated()
            .and().httpBasic()
            .and().oauth2Login()
            .successHandler(oAuth2AuthenticationSuccessHandler).failureHandler(oAuth2AuthenticationFailureHandler)
            .authorizationEndpoint().authorizationRequestResolver(new OauthAuthorizationRequestResolver(inMemoryClientRegistrationRepository))
            .and()
            .and().apply(securityConfigurerAdapter())
        ;
    }

    @Bean
    public OAuth2TokenCustomizer<JwtEncodingContext> buildCustomizer() {
        OAuth2TokenCustomizer<JwtEncodingContext> customizer = (context) -> {
            if (context.getTokenType().getValue().equals(ID_TOKEN) && context.getPrincipal().isAuthenticated()) {
                com.schoolaby.domain.User user = null;

                if (context.getPrincipal() instanceof UsernamePasswordAuthenticationToken) {
                    user = userService.findOneByLogin(context.getPrincipal().getName()).orElse(null);
                }

                if (context.getPrincipal() instanceof OAuth2AuthenticationToken) {
                    user = userService.findUserByExternalId(context.getPrincipal().getName());
                }

                if (user != null) {
                    context.getClaims().claim("sub", user.getId());
                    context.getClaims().claim("profile", user.getClaims());
                }

                context.getClaims().claim("iss", oauthServerProperties.getIss());
            }
        };
        return customizer;
    }

    private JWTConfigurer securityConfigurerAdapter() {
        return new JWTConfigurer(tokenProvider);
    }
}
