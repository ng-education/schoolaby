package com.schoolaby.config;

import com.schoolaby.domain.User;
import com.schoolaby.security.harid.HarIdService;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import static com.schoolaby.security.SecurityUtils.createSpringSecurityUser;

@Service
public class HarIdAuthenticationProvider implements AuthenticationProvider {
    private final HarIdService harIdService;

    public HarIdAuthenticationProvider(HarIdService harIdService) {
        this.harIdService = harIdService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) {
        User user = harIdService.saveOrUpdateHarIdData(authentication.getCredentials().toString());
        com.schoolaby.security.User springSecurityUser = createSpringSecurityUser(user);
        return new HarIdAuthenticationToken(springSecurityUser, authentication.getCredentials(), springSecurityUser.getAuthorities());
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(HarIdAuthenticationToken.class);
    }
}
