package com.schoolaby.config.oauth2;

import org.springframework.security.crypto.keygen.Base64StringKeyGenerator;
import org.springframework.security.crypto.keygen.StringKeyGenerator;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.web.DefaultOAuth2AuthorizationRequestResolver;
import org.springframework.security.oauth2.client.web.OAuth2AuthorizationRequestResolver;
import org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.Base64;

public class OauthAuthorizationRequestResolver implements OAuth2AuthorizationRequestResolver {
    private final StringKeyGenerator stateGenerator = new Base64StringKeyGenerator(Base64.getUrlEncoder());
    private final OAuth2AuthorizationRequestResolver defaultResolver;

    public OauthAuthorizationRequestResolver(ClientRegistrationRepository repo) {
        defaultResolver = new DefaultOAuth2AuthorizationRequestResolver(repo, "/oauth2/authorization");
    }

    @Override
    public OAuth2AuthorizationRequest resolve(HttpServletRequest request) {
        OAuth2AuthorizationRequest req = defaultResolver.resolve(request);

        if (req != null) {
            String userId = request.getParameter("id");
            req = customizeAuthorizationRequest(req, userId);
        }
        return req;
    }

    @Override
    public OAuth2AuthorizationRequest resolve(HttpServletRequest request, String clientRegistrationId) {
        OAuth2AuthorizationRequest req = defaultResolver.resolve(request, clientRegistrationId);
        if (req != null) {
            String userId = request.getParameter("id");
            req = customizeAuthorizationRequest(req, userId);
        }
        return req;
    }

    private OAuth2AuthorizationRequest customizeAuthorizationRequest(OAuth2AuthorizationRequest req, String userId) {
        String state = this.stateGenerator.generateKey();
        if (userId != null) {
            state += "," + userId;
        }
        return OAuth2AuthorizationRequest
            .from(req)
            .state(state)
            .build();
    }
}
