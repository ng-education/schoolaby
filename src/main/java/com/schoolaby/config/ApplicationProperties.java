package com.schoolaby.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {
    private final Storage storage = new Storage();
    private final Mail mail = new Mail();
    private final Endpoints endpoints = new Endpoints();
    private final Monitoring monitoring = new Monitoring();

    public Storage getStorage() {
        return storage;
    }

    public Mail getMail() {
        return mail;
    }

    public Endpoints getEndpoints() {
        return endpoints;
    }

    public Monitoring getMonitoring() {
        return monitoring;
    }

    public static class Storage {
        private String accessKey;
        private String accessSecret;
        private String endpoint;
        private String region;

        public String getAccessKey() {
            return accessKey;
        }

        public void setAccessKey(String accessKey) {
            this.accessKey = accessKey;
        }

        public String getAccessSecret() {
            return accessSecret;
        }

        public void setAccessSecret(String accessSecret) {
            this.accessSecret = accessSecret;
        }

        public String getEndpoint() {
            return endpoint;
        }

        public void setEndpoint(String endpoint) {
            this.endpoint = endpoint;
        }

        public String getRegion() {
            return region;
        }

        public void setRegion(String region) {
            this.region = region;
        }
    }

    public static class Mail {
        private String supportEmail;

        public String getSupportEmail() {
            return supportEmail;
        }

        public void setSupportEmail(String supportEmail) {
            this.supportEmail = supportEmail;
        }
    }

    public static class Monitoring {
        private String teamsHook;

        public String getTeamsHook() {
            return teamsHook;
        }

        public void setTeamsHook(String teamsHook) {
            this.teamsHook = teamsHook;
        }
    }

    public static class Endpoints {
        JourneySignupCodeGeneration journeySignupCodeGeneration = new JourneySignupCodeGeneration();

        public JourneySignupCodeGeneration getJourneySignupCodeGeneration() {
            return journeySignupCodeGeneration;
        }

        public static class JourneySignupCodeGeneration {
            private List<String> allowedUsers;

            public List<String> getAllowedUsers() {
                return allowedUsers;
            }

            public void setAllowedUsers(List<String> allowedUsers) {
                this.allowedUsers = allowedUsers;
            }
        }
    }
}
