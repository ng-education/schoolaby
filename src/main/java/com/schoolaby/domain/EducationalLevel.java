package com.schoolaby.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

import static javax.persistence.GenerationType.SEQUENCE;

@Entity
@Table(name = "educational_level")
public class EducationalLevel implements Serializable {
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "sequenceGenerator")
    private Long id;
    @NotNull
    private String name;

    private String country;

    public EducationalLevel setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public EducationalLevel setName(String name) {
        this.name = name;
        return this;
    }

    public Long getId() {
        return id;
    }

    public EducationalLevel id(Long id) {
        setId(id);
        return this;
    }

    public EducationalLevel name(String name) {
        setName(name);
        return this;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public EducationalLevel country(String country) {
        setCountry(country);
        return this;
    }
}
