package com.schoolaby.domain;

import lombok.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.Loader;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static java.lang.String.format;
import static java.util.Comparator.naturalOrder;
import static javax.persistence.FetchType.EAGER;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.hibernate.annotations.CacheConcurrencyStrategy.READ_WRITE;

@Entity
@Table(name = "grading_scheme")
@Cache(usage = READ_WRITE)
@SQLDelete(sql = "UPDATE grading_scheme SET deleted = now() WHERE id = ?")
@Loader(namedQuery = "findGradingSchemeById")
@NamedQuery(name = "findGradingSchemeById", query = "SELECT gs FROM GradingScheme gs WHERE gs.id = ?1 AND gs.deleted IS NULL")
@Where(clause = "deleted IS NULL")
@Getter
@Setter
public class GradingScheme extends AbstractAuditingEntity implements Serializable {
    public static final String NARRATIVE = "NARRATIVE";
    public static final String NONE = "NONE";
    public static final List<String> SCHEMES_WITHOUT_GRADE = List.of(NARRATIVE, NONE);

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String code;

    @Column(nullable = false)
    private int sequenceNumber;

    @OneToMany(mappedBy = "gradingScheme", fetch = EAGER)
    @Cache(usage = READ_WRITE)
    private Set<GradingSchemeValue> values = new HashSet<>();

    public GradingScheme name(String name) {
        this.name = name;
        return this;
    }

    public GradingScheme code(final String code) {
        setCode(code);
        return this;
    }

    public boolean isValidGrade(String grade) {
        return getValues().stream().anyMatch(gsv -> gsv.getGrade().equals(grade.toUpperCase()));
    }

    public GradingScheme addValue(GradingSchemeValue gradingSchemeValue) {
        this.values.add(gradingSchemeValue);
        gradingSchemeValue.setGradingScheme(this);
        return this;
    }

    public GradingScheme removeValue(GradingSchemeValue gradingSchemeValue) {
        this.values.remove(gradingSchemeValue);
        gradingSchemeValue.setGradingScheme(null);
        return this;
    }

    public GradingScheme values(Set<GradingSchemeValue> gradingSchemeValues) {
        this.values = gradingSchemeValues;
        return this;
    }

    public boolean isPassingGrade(String grade) {
        Optional<GradingSchemeValue> matchingGradingSchemeValue = values
            .stream()
            .filter(gradingSchemeValue -> gradingSchemeValue.getGrade().equals(grade))
            .findFirst();

        return matchingGradingSchemeValue.isPresent() && matchingGradingSchemeValue.get().getPercentageRange() >= 50;
    }

    public boolean isHighestGrade(String grade) {
        int gradePercentage = getPercentage(grade);
        int highestPercentage = getValues()
            .stream()
            .map(GradingSchemeValue::getPercentageRange)
            .max(naturalOrder()).orElseThrow();

        return gradePercentage == highestPercentage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof GradingScheme)) {
            return false;
        }
        return id != null && id.equals(((GradingScheme) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "GradingScheme{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }

    public int getPercentage(String grade) {
        if (isEmpty(grade)) {
            return 0;
        }

        return getValues()
            .stream()
            .filter(gradingSchemeValue -> gradingSchemeValue.getGrade().equals(grade))
            .map(GradingSchemeValue::getPercentageRange)
            .findFirst()
            .orElseThrow(() -> new RuntimeException(format("Unable to find grade %s in grading scheme %s", grade, this)));
    }
}
