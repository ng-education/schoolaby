package com.schoolaby.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.querydsl.core.annotations.QueryInit;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.Loader;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

import static javax.persistence.CascadeType.*;
import static org.hibernate.annotations.CacheConcurrencyStrategy.READ_WRITE;

@Entity
@Table(name = "submission")
@Cache(usage = READ_WRITE)
@SQLDelete(sql = "UPDATE submission SET deleted = now() WHERE id = ?")
@Loader(namedQuery = "findSubmissionById")
@NamedQuery(name = "findSubmissionById", query = "SELECT pr FROM Submission pr WHERE pr.id = ?1 AND pr.deleted IS NULL")
@Where(clause = "deleted IS NULL")
public class Submission extends AbstractAuditingEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "value", nullable = false)
    private String value;

    @Getter
    @Setter
    @OneToMany(mappedBy = "submission", cascade = REMOVE, orphanRemoval = true)
    @OrderBy("feedbackDate DESC")
    private Set<SubmissionFeedback> submissionFeedbacks = new LinkedHashSet<>();

    @Column(name = "resubmittable")
    private boolean resubmittable;

    @Column(name = "submitted_for_grading_date")
    private Instant submittedForGradingDate;

    @OneToMany(mappedBy = "submission", cascade = REMOVE)
    private Set<Chat> chats = new HashSet<>();

    @OneToMany
    @Cache(usage = READ_WRITE)
    @JoinTable(
        name = "submission_uploaded_files",
        joinColumns = @JoinColumn(name = "submission_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "uploaded_file_id", referencedColumnName = "id")
    )
    private Set<UploadedFile> uploadedFiles = new HashSet<>();

    @ManyToMany
    @JoinTable(
        name = "submission_authors",
        joinColumns = @JoinColumn(name = "submission_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "authors_id", referencedColumnName = "id")
    )
    private Set<User> authors = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "submissions", allowSetters = true)
    @QueryInit("milestone.journey")
    private Assignment assignment;

    @ManyToOne
    @JsonIgnoreProperties(value = "submissions", allowSetters = true)
    private Group group;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Instant getSubmittedForGradingDate() {
        return submittedForGradingDate;
    }

    public void setSubmittedForGradingDate(Instant submitted) {
        this.submittedForGradingDate = submitted;
    }

    public boolean isResubmittable() {
        return resubmittable;
    }

    public void setResubmittable(boolean resubmittable) {
        this.resubmittable = resubmittable;
    }

    public Set<UploadedFile> getUploadedFiles() {
        return uploadedFiles;
    }

    public Submission addSubmissionFeedback(SubmissionFeedback submissionFeedback) {
        this.submissionFeedbacks.add(submissionFeedback);
        return this;
    }

    public Submission addUploadedFile(UploadedFile uploadedFile) {
        this.uploadedFiles.add(uploadedFile);
        return this;
    }

    public Submission removeUploadedFile(UploadedFile uploadedFile) {
        this.uploadedFiles.remove(uploadedFile);
        return this;
    }

    public void removeSubmissionFeedback(SubmissionFeedback feedback) {
        this.submissionFeedbacks.remove(feedback);
    }

    public void setUploadedFiles(Collection<UploadedFile> uploadedFiles) {
        this.uploadedFiles.clear();
        this.uploadedFiles.addAll(uploadedFiles);
    }

    public Set<User> getAuthors() {
        return authors;
    }

    public Submission addAuthor(User person) {
        this.authors.add(person);
        person.getSubmissions().add(this);
        return this;
    }

    public void setAuthors(Set<User> people) {
        this.authors = people;
    }

    public Assignment getAssignment() {
        return assignment;
    }

    public void setAssignment(Assignment assignment) {
        this.assignment = assignment;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Submission group(Group group) {
        setGroup(group);
        return this;
    }

    public Submission id(Long id) {
        setId(id);
        return this;
    }

    public Submission value(String value) {
        setValue(value);
        return this;
    }

    public Submission submittedForGradingDate(Instant submittedForGradingDate) {
        setSubmittedForGradingDate(submittedForGradingDate);
        return this;
    }

    public Submission resubmittable(boolean resubmittable) {
        setResubmittable(resubmittable);
        return this;
    }

    public Submission uploadedFiles(Set<UploadedFile> uploadedFiles) {
        setUploadedFiles(uploadedFiles);
        return this;
    }

    public Submission authors(Set<User> authors) {
        setAuthors(authors);
        return this;
    }

    public Submission assignment(Assignment assignment) {
        setAssignment(assignment);
        return this;
    }

    public Set<Chat> getChats() {
        return chats;
    }

    public void setChats(Set<Chat> chats) {
        this.chats = chats;
    }

    public Submission chats(final Set<Chat> chats) {
        setChats(chats);
        return this;
    }

    public boolean isCompleted() {
        return isGraded() && hasPassingGrade();
    }

    public boolean hasStudentCompleted(Long studentId) {
        SubmissionFeedback feedback = getStudentFeedbackOrElseGroupFeedback(studentId);
        if (feedback != null) {
            return feedback.isGraded() && feedback.hasPassingGrade();
        }
        return false;
    }

    public boolean isFailed() {
        return isGraded() && !hasPassingGrade() && !isResubmittable();
    }

    public boolean hasStudentFailed(Long studentId) {
        SubmissionFeedback feedback = getStudentFeedbackOrElseGroupFeedback(studentId);
        if (feedback != null) {
            boolean hasJourneyEnded = getAssignment().getMilestone().getJourney().hasEndDatePassed();
            return hasJourneyEnded ?
                feedback.isGraded() && !feedback.hasPassingGrade() :
                feedback.isGraded() && !feedback.hasPassingGrade() && !isResubmittable();
        }

        return false;
    }

    public boolean isRejected() {
        return isGraded() && !hasPassingGrade() && isResubmittable();
    }

    public boolean isStudentSubmissionRejected(Long studentId) {
        SubmissionFeedback feedback = getStudentFeedbackOrElseGroupFeedback(studentId);
        if (feedback != null) {
            return feedback.isGraded() && !feedback.hasPassingGrade() && isResubmittable();
        }

        return false;
    }

    public boolean isStudentSubmissionInProgress(Long studentId) {
        SubmissionFeedback feedback = getStudentFeedbackOrElseGroupFeedback(studentId);
        if (feedback != null) {
            return (!feedback.isGraded() && submittedForGradingDate == null) || isResubmittable();
        }

        return submittedForGradingDate == null || isResubmittable();
    }

    public boolean isSubmitted() {
        return !isGraded() && submittedForGradingDate != null;
    }

    public boolean hasStudentSubmitted(Long studentId) {
        SubmissionFeedback feedback = getStudentFeedbackOrElseGroupFeedback(studentId);
        if (feedback != null) {
            return (!feedback.isGraded() && submittedForGradingDate != null);
        }

        return submittedForGradingDate != null;
    }

    public boolean hasGrades() {
        return submissionFeedbacks.stream().anyMatch(SubmissionFeedback::isGraded);
    }

    public boolean isGraded() {
        return isGroupSubmissionGraded() || isIndividualSubmissionGraded();
    }

    private SubmissionFeedback getStudentFeedbackOrElseGroupFeedback(Long studentId) {
        SubmissionFeedback studentFeedback = submissionFeedbacks.stream().filter(feedback -> feedback.getStudent() != null && feedback.getStudent().getId().equals(studentId)).findFirst().orElse(null);
        SubmissionFeedback groupFeedback = submissionFeedbacks.stream().filter(feedback -> feedback.getGroup() != null).findFirst().orElse(null);
        return studentFeedback != null ? studentFeedback : groupFeedback;
    }

    private boolean hasPassingGrade() {
        return submissionFeedbacks.stream().allMatch(feedback -> assignment.getGradingScheme().isPassingGrade(feedback.getGrade()));
    }

    public boolean hasAuthor(Long userId) {
        return getAuthors().stream().anyMatch(user -> user.getId().equals(userId));
    }

    public boolean isHighestGrade() {
        return submissionFeedbacks.stream().allMatch(feedback -> feedback.isGraded() && assignment.getGradingScheme().isHighestGrade(feedback.getGrade()));
    }

    public Optional<SubmissionFeedback> findFeedback(Long studentId, boolean findIndividualOnly) {
        Optional<SubmissionFeedback> studentFeedback = submissionFeedbacks.stream().filter(feedback -> feedback.getStudent() != null && feedback.getStudent().getId().equals(studentId)).findFirst();
        Optional<SubmissionFeedback> groupFeedback = submissionFeedbacks.stream().filter(feedback -> feedback.getGroup() != null).findFirst();

        if (studentFeedback.isPresent() || findIndividualOnly) {
            return studentFeedback;
        }

        return groupFeedback;
    }

    public Optional<SubmissionFeedback> findFeedback(Long studentId) {
        return findFeedback(studentId, false);
    }

    private boolean isGroupSubmissionGraded() {
        if (assignment.isGroupAssignment()) {
            Optional<SubmissionFeedback> groupFeedback = submissionFeedbacks.stream().filter(feedback -> feedback.getGroup() != null).findFirst();
            Set<SubmissionFeedback> individualFeedbacks = submissionFeedbacks.stream().filter(feedback -> feedback.getGroup() == null).collect(Collectors.toSet());
            Set<User> studentsWithFeedback = individualFeedbacks.stream().map(SubmissionFeedback::getStudent).collect(Collectors.toSet());
            boolean allAuthorsHaveFeedback = studentsWithFeedback.containsAll(getAuthors());

            boolean groupGraded = groupFeedback.map(SubmissionFeedback::isGraded).orElse(false);
            boolean allIndividuallyGraded = individualFeedbacks.stream().allMatch(SubmissionFeedback::isGraded) && allAuthorsHaveFeedback;

            return groupGraded || allIndividuallyGraded;
        }

        return false;
    }

    private boolean isIndividualSubmissionGraded() {
        if(!assignment.isGroupAssignment()) {
            Optional<SubmissionFeedback> individualFeedback = submissionFeedbacks.stream().filter(feedback -> feedback.getGroup() == null).findFirst();
            return individualFeedback.map(SubmissionFeedback::isGraded).orElse(false);
        }

        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Submission)) {
            return false;
        }
        return id != null && id.equals(((Submission) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Submission{" +
            "id=" + id +
            ", value='" + value + '\'' +
            ", resubmittable=" + resubmittable +
            ", submittedForGradingDate=" + submittedForGradingDate +
            ", uploadedFiles=" + uploadedFiles +
            ", authors=" + authors +
            ", assignment=" + assignment +
            '}';
    }
}
