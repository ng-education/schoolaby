package com.schoolaby.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.schoolaby.service.dto.states.EntityState;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.*;

import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

import static com.schoolaby.config.filter.TenantServiceAspect.FILTER_HIDDEN;
import static com.schoolaby.security.Role.Constants.STUDENT;
import static com.schoolaby.security.Role.Constants.TEACHER;
import static com.schoolaby.security.SecurityUtils.getCurrentUserId;
import static com.schoolaby.service.dto.states.EntityState.COMPLETED;
import static java.time.Instant.now;
import static java.util.stream.Collectors.toSet;
import static java.util.stream.Collectors.toUnmodifiableSet;
import static javax.persistence.CascadeType.*;
import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.SEQUENCE;
import static org.hibernate.annotations.CacheConcurrencyStrategy.READ_WRITE;

@Entity
@Table(name = "journey")
@Cache(usage = READ_WRITE)
@SQLDelete(sql = "UPDATE journey SET deleted = now() WHERE id = ?")
@Loader(namedQuery = "findJourneyById")
@NamedQuery(name = "findJourneyById", query = "SELECT j FROM Journey j WHERE j.id = ?1 AND j.deleted IS NULL")
@Where(clause = "deleted IS NULL")
@Getter
@Setter
public class Journey extends AbstractAuditingEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "video_conference_url")
    private String videoConferenceUrl;

    @NotNull
    @Column(name = "start_date", nullable = false)
    private Instant startDate;

    @Column(name = "end_date")
    private Instant endDate;

    @OneToMany(mappedBy = "journey", cascade = {REFRESH, DETACH, PERSIST, REMOVE})
    @OrderBy("endDate, id")
    @Filter(name = FILTER_HIDDEN, condition = "published IS NOT NULL AND published < NOW()")
    @Cache(usage = READ_WRITE)
    private Set<Milestone> milestones = new HashSet<>();

    @OneToMany(mappedBy = "journey", fetch = EAGER, cascade = {PERSIST, MERGE, REMOVE})
    @Cache(usage = READ_WRITE)
    @OrderBy("pinnedDate")
    private Set<JourneyStudent> students = new HashSet<>();

    @OneToMany(mappedBy = "journey", fetch = EAGER, cascade = {PERSIST, MERGE, REMOVE})
    private Set<JourneySignupCode> journeySignupCodes = new HashSet<>();

    @OneToMany(mappedBy = "journey", fetch = EAGER, cascade = {PERSIST, MERGE, REMOVE})
    @Cache(usage = READ_WRITE)
    private Set<JourneyTeacher> teachers = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "createdJourneys", allowSetters = true)
    private User creator;

    @OneToMany(mappedBy = "journey", cascade = {PERSIST, REMOVE}, fetch = EAGER)
    private Set<LtiConfig> ltiConfigs = new HashSet<>();

    @ManyToOne(fetch = EAGER)
    private EducationalLevel educationalLevel;

    @ManyToMany(fetch = EAGER)
    @Cache(usage = READ_WRITE)
    @JoinTable(
        name = "journey_schools",
        joinColumns = @JoinColumn(name = "journey_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "school_id", referencedColumnName = "id")
    )
    @OrderBy("name,id")
    private Set<School> schools = new HashSet<>();

    @Column(name = "template")
    private Boolean template;

    @Column(name = "national_curriculum")
    private Boolean nationalCurriculum;

    @OneToMany(mappedBy = "journey", cascade = {REMOVE})
    private Set<Chat> chats = new HashSet<>();

    @OneToMany(mappedBy = "journey", cascade = {REMOVE})
    private Set<Notification> notifications = new HashSet<>();

    @Column(name = "image_url")
    private String imageUrl;

    @ManyToOne(fetch = EAGER)
    private Subject subject;

    @OneToMany(mappedBy = "journey", cascade = {PERSIST, MERGE, REMOVE})
    private Set<JourneyState> states = new HashSet<>();

    @ManyToOne
    private Curriculum curriculum;

    public Long getJourneyProgress() {
        Set<Assignment> assignments = new HashSet<>();
        this.milestones.forEach(milestone -> assignments.addAll(milestone.getAssignments()));
        List<EntityState> assignmentsStates = assignments.stream().map(Assignment::getState).filter(Objects::nonNull).collect(Collectors.toList());

        if (assignments.isEmpty() || assignmentsStates.isEmpty()) {
            return 0L;
        }

        double allCount = assignments.size();
        double completedCount = assignmentsStates.stream().filter(state -> state.equals(COMPLETED)).count();

        double progress = completedCount / allCount * 100;
        return Math.round(progress);
    }

    public Journey id(Long id) {
        setId(id);
        return this;
    }

    public Journey title(String title) {
        setTitle(title);
        return this;
    }

    public Journey description(String description) {
        setDescription(description);
        return this;
    }

    public Journey videoConferenceUrl(String videoConferenceUrl) {
        setVideoConferenceUrl(videoConferenceUrl);
        return this;
    }

    public Journey startDate(Instant startDate) {
        setStartDate(startDate);
        return this;
    }

    public Journey endDate(Instant endDate) {
        setEndDate(endDate);
        return this;
    }

    public Optional<Milestone> getMilestoneById(Long id) {
        return getMilestones().stream().filter(milestone -> milestone.getId().equals(id)).findFirst();
    }

    public Journey milestones(Set<Milestone> milestones) {
        setMilestones(milestones);
        return this;
    }

    public Journey addMilestone(Milestone milestone) {
        this.milestones.add(milestone);
        milestone.setJourney(this);
        return this;
    }

    public Journey removeMilestone(Milestone milestone) {
        this.milestones.remove(milestone);
        milestone.setJourney(null);
        return this;
    }

    public void setMilestones(Set<Milestone> milestones) {
        milestones.forEach(milestone -> milestone.setJourney(this));
        this.milestones = milestones;
    }

    public Set<User> getJourneyStudentsUsers() {
        return getStudents()
            .stream()
            .map(JourneyStudent::getUser)
            .collect(toSet());
    }

    public Journey setStudents(Set<JourneyStudent> journeyStudents) {
        if (journeyStudents != null) {
            journeyStudents.forEach(journeyStudent -> journeyStudent.setJourney(this));
        }
        this.students = journeyStudents;
        return this;
    }

    public Journey students(Set<JourneyStudent> students) {
        setStudents(students);
        return this;
    }

    public Journey addStudent(User student) {
        JourneyStudent journeyStudent = new JourneyStudent()
            .user(student)
            .journey(this);
        return addStudent(journeyStudent);
    }

    public Journey addStudent(JourneyStudent journeyStudent) {
        getStudents().add(journeyStudent);
        journeyStudent.journey(this);
        return this;
    }

    public boolean isStudentInJourney(Long userId) {
        return students.stream().anyMatch(student -> student.getUser().getId().equals(userId));
    }

    public boolean isTeacherInJourney(Long userId) {
        return teachers.stream().anyMatch(teacher -> teacher.getUser().getId().equals(userId));
    }

    public Set<User> getJourneyTeachersUsers() {
        return getTeachers()
            .stream()
            .map(JourneyTeacher::getUser)
            .collect(toSet());
    }

    public Journey setTeachers(Set<JourneyTeacher> journeyTeachers) {
        if (journeyTeachers != null) {
            journeyTeachers.forEach(journeyStudent -> journeyStudent.setJourney(this));
        }
        this.teachers = journeyTeachers;
        return this;
    }

    public Journey teachers(Set<JourneyTeacher> teachers) {
        setTeachers(teachers);
        return this;
    }

    public Journey addTeacher(User teacher) {
        JourneyTeacher journeyTeacher = new JourneyTeacher()
            .user(teacher)
            .journey(this)
            .joinedDate(Instant.now());
        return addTeacher(journeyTeacher);
    }

    public Journey addTeacher(JourneyTeacher journeyTeacher) {
        getTeachers().add(journeyTeacher);
        journeyTeacher.setJourney(this);
        return this;
    }

    public Journey creator(User person) {
        setCreator(person);
        return this;
    }

    public Journey template(Boolean template) {
        return this.setTemplate(template);
    }

    public Set<Assignment> getAssignments() {
        return getMilestones().stream().map(Milestone::getAssignments).flatMap(Set::stream).collect(toUnmodifiableSet());
    }

    public Double getUserAverageGrade(Long studentId) {
        return getAssignments()
            .stream()
            .map(assignment -> assignment.getAverageScore(studentId))
            .filter(Objects::nonNull)
            .mapToInt(Integer::intValue)
            .average()
            .orElse(0d);
    }

    public Journey addLtiConfig(LtiConfig ltiConfig) {
        this.ltiConfigs.add(ltiConfig);
        ltiConfig.setJourney(this);
        return this;
    }

    public void removeLtiConfig(LtiConfig ltiConfig) {
        getLtiConfigs().remove(ltiConfig);
    }

    public Journey educationalLevel(EducationalLevel educationalLevel) {
        setEducationalLevel(educationalLevel);
        return this;
    }

    public void setChats(Set<Chat> chats) {
        chats.forEach(chat -> chat.journey(this));
        this.chats = chats;
    }

    public Journey addChat(Chat chat) {
        this.chats.add(chat);
        chat.journey(this);
        return this;
    }

    public Journey chats(Set<Chat> chats) {
        setChats(chats);
        return this;
    }

    public Journey notifications(Set<Notification> notifications) {
        setNotifications(notifications);
        return this;
    }

    public Journey addNotification(Notification notification) {
        this.notifications.add(notification);
        notification.journey(this);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Journey)) {
            return false;
        }
        return id != null && id.equals(((Journey) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    public Set<LtiConfig> getLtiConfigs() {
        return ltiConfigs;
    }

    public void setLtiConfigs(Set<LtiConfig> ltiConfigs) {
        ltiConfigs.forEach(ltiConfig -> ltiConfig.setJourney(this));
        this.ltiConfigs = ltiConfigs;
    }

    public Journey ltiConfigs(final Set<LtiConfig> ltiConfigs) {
        setLtiConfigs(ltiConfigs);
        return this;
    }

    public Journey addState(JourneyState state) {
        getStates().add(state);
        return this;
    }

    private void calculateAndSaveState(JourneyState state) {
        getExistingJourneyState(state).ifPresentOrElse(
            JourneyState::calculate,
            () -> addState(state.calculate())
        );
    }

    public Journey states(Set<JourneyState> states) {
        this.setStates(states);
        return this;
    }

    public JourneyStudent removeStudent(Long studentId) {
        JourneyStudent student = getStudents().stream()
            .filter(journeyStudent -> journeyStudent
                .getUser()
                .getId()
                .equals(studentId))
            .findFirst()
            .orElse(null);
        if (student == null) {
            return null;
        }
        getStudents().remove(student);
        return student;
    }

    public JourneyTeacher removeTeacher(Long teacherId) {
        JourneyTeacher teacher = getTeachers().stream()
            .filter(journeyTeacher -> journeyTeacher
                .getUser()
                .getId()
                .equals(teacherId))
            .findFirst()
            .orElse(null);
        if (teacher == null) {
            return null;
        }

        getTeachers().remove(teacher);
        return teacher;
    }

    public Boolean getNationalCurriculum() {
        return nationalCurriculum;
    }

    public void setNationalCurriculum(Boolean nationalCurriculum) {
        this.nationalCurriculum = nationalCurriculum;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Subject getSubject() {
        return subject;
    }

    public Journey setSubject(Subject subject) {
        this.subject = subject;
        return this;
    }


    public Journey addSchool(School school) {
        getSchools().add(school);
        return this;
    }

    public Journey removeSchool(School school) {
        getSchools().remove(school);
        return this;
    }

    public Journey schools(Set<School> schools) {
        setSchools(schools);
        return this;
    }

    public Journey curriculum(Curriculum curriculum) {
        setCurriculum(curriculum);
        return this;
    }

    public boolean hasEndDatePassed() {
        return getEndDate().plus(1, ChronoUnit.DAYS).truncatedTo(ChronoUnit.DAYS).isBefore(now());
    }

    public boolean isStartDateInFuture() {
        return getStartDate().truncatedTo(ChronoUnit.DAYS).isAfter(now());
    }

    public void calculateStates() {
        calculateJourneyStateByAuthority(getJourneyStudentsUsers(), STUDENT);
        calculateJourneyStateByAuthority(getJourneyTeachersUsers(), TEACHER);
    }

    private void calculateJourneyStateByAuthority(Set<User> users, String authorityName) {
        users.forEach(user -> calculateAndSaveState(new JourneyState()
            .journey(this)
            .user(user)
            .authority(new Authority().name(authorityName))));
    }

    private Optional<JourneyState> getExistingJourneyState(JourneyState journeyState) {
        return getStates()
            .stream()
            .filter(state -> state.getUser()
                .equals(journeyState.getUser()) &&
                state.getAuthority()
                    .equals(journeyState.getAuthority()))
            .findFirst();
    }

    public String getSignupCode(String authority) {
        Optional<JourneySignupCode> signupCode = journeySignupCodes.stream().filter(journeySignupCode -> !journeySignupCode.isOneTime() && authority.equals(journeySignupCode.getAuthority().getName())).findFirst();
        return signupCode.map(JourneySignupCode::getSignUpCode).orElse(null);
    }

    public Authority getSignupCodeAuthority(String signUpCode) {
        return getJourneySignupCodes().stream().filter(journeySignupCode -> journeySignupCode.getSignUpCode().equals(signUpCode)).findFirst().orElseThrow().getAuthority();
    }

    public boolean isOwner() {
        return getCurrentUserId() != null && getCurrentUserId().equals(getCreator().getId());
    }

    public EntityState getState() {
        Long userId = getCurrentUserId();
        boolean isStudent = isStudentInJourney(userId);
        boolean isTeacher = isTeacherInJourney(userId);

        if (!isStudent && !isTeacher) {
            return null;
        }
        String authorityName = isTeacher ? TEACHER : STUDENT;
        return getState(getCurrentUserId(), authorityName);
    }

    private EntityState getState(Long userId, String authorityName) {
        Optional<JourneyState> state = getStates()
            .stream().filter(journeyState -> journeyState.getUser().getId().equals(userId) && journeyState.getAuthority().getName().equals(authorityName)).findFirst();
        return state.map(JourneyState::getState).orElse(null);
    }


    @Override
    public String toString() {
        return "Journey{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            "}";
    }
}
