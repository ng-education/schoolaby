package com.schoolaby.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.schoolaby.security.Role;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Loader;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashMap;

@Entity
@Table(name = "person_role")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SQLDelete(sql = "UPDATE person_role SET deleted = now() WHERE id = ?")
@Loader(namedQuery = "findPersonRoleById")
@NamedQuery(name = "findPersonRoleById", query = "SELECT pr FROM PersonRole pr WHERE pr.id = ?1 AND pr.deleted IS NULL")
@Where(clause = "deleted IS NULL")
public class PersonRole extends AbstractAuditingEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;
    @Enumerated(EnumType.STRING)
    private Role role;
    private Boolean active;
    private LocalDateTime startDate = LocalDateTime.now();
    private LocalDateTime endDate;
    private String grade;
    private String parallel;
    @ManyToOne
    @JsonIgnoreProperties(value = "personRoles", allowSetters = true)
    private User person;
    @ManyToOne
    private School school;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PersonRole id(Long id) {
        setId(id);
        return this;
    }

    public PersonRole role(Role role) {
        setRole(role);
        return this;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Boolean isActive() {
        return active;
    }

    public PersonRole active(Boolean active) {
        this.active = active;
        return this;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public PersonRole startDate(LocalDateTime startDate) {
        this.startDate = startDate;
        return this;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public PersonRole endDate(LocalDateTime endDate) {
        this.endDate = endDate;
        return this;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public String getGrade() {
        return grade;
    }

    public PersonRole grade(String grade) {
        this.grade = grade;
        return this;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getParallel() {
        return parallel;
    }

    public PersonRole parallel(String parallel) {
        this.parallel = parallel;
        return this;
    }

    public void setParallel(String parallel) {
        this.parallel = parallel;
    }

    public User getPerson() {
        return person;
    }

    public PersonRole person(User person) {
        this.person = person;
        return this;
    }

    public void setPerson(User person) {
        this.person = person;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public PersonRole school(School school) {
        setSchool(school);
        return this;
    }
    public HashMap<String, Object> getClaims() {
        HashMap<String, Object> schoolClaim = new HashMap<>();
        schoolClaim.put("ehis_id", getSchool().getEhisId());
        schoolClaim.put("name", getSchool().getName());
        schoolClaim.put("role", getRole().getName());
        return schoolClaim;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PersonRole)) {
            return false;
        }
        return id != null && id.equals(((PersonRole) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "PersonRole{" +
            "id=" + getId() +
            ", role='" + getRole() + "'" +
            ", active='" + isActive() + "'" +
            ", startDate='" + getStartDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", grade='" + getGrade() + "'" +
            ", parallel='" + getParallel() + "'" +
            "}";
    }
}
