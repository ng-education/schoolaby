package com.schoolaby.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.Loader;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;

import static javax.persistence.EnumType.STRING;
import static org.hibernate.annotations.CacheConcurrencyStrategy.READ_WRITE;

@Entity
@Table(name = "notification")
@Cache(usage = READ_WRITE)
@SQLDelete(sql = "UPDATE notification SET deleted = now() WHERE id = ?")
@Loader(namedQuery = "findNotificationById")
@NamedQuery(name = "findNotificationById", query = "SELECT n FROM Notification n WHERE n.id = ?1 AND n.deleted IS NULL")
@Where(clause = "deleted IS NULL")
public class Notification extends AbstractAuditingEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "message", nullable = false)
    private String message;

    @NotNull
    @Column(name = "link", nullable = false)
    private String link;

    @ManyToOne
    private User creator;

    @ManyToOne
    private User recipient;

    @ManyToOne
    private Journey journey;

    @ManyToOne
    private Assignment assignment;

    @Column
    private Instant read;

    @Column
    @Enumerated(STRING)
    private NotificationType type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Notification id(Long id) {
        this.id = id;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Notification message(String message) {
        this.message = message;
        return this;
    }

    public Notification link(String link) {
        this.link = link;
        return this;
    }

    public Journey getJourney() {
        return journey;
    }

    public void setJourney(Journey journey) {
        this.journey = journey;
    }

    public Assignment getAssignment() {
        return assignment;
    }

    public void setAssignment(Assignment assignment) {
        this.assignment = assignment;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public User getRecipient() {
        return recipient;
    }

    public void setRecipient(User recipient) {
        this.recipient = recipient;
    }

    public Notification creator(final User creator) {
        setCreator(creator);
        return this;
    }

    public Notification recipient(final User recipient) {
        setRecipient(recipient);
        return this;
    }

    public Notification journey(final Journey journey) {
        setJourney(journey);
        return this;
    }

    public Notification assignment(final Assignment assignment) {
        setAssignment(assignment);
        return this;
    }

    public Instant getRead() {
        return read;
    }

    public void setRead(Instant read) {
        this.read = read;
    }

    public Notification read(Instant read) {
        this.read = read;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Notification)) {
            return false;
        }
        return id != null && id.equals(((Notification) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Notification{" +
            "id=" + id +
            ", message='" + message + '\'' +
            ", link='" + link + '\'' +
            ", journey=" + journey +
            ", assignment=" + assignment +
            ", creator=" + creator +
            ", recipient=" + recipient +
            '}';
    }

    public NotificationType getType() {
        return type;
    }

    public void setType(NotificationType type) {
        this.type = type;
    }

    public Notification type(NotificationType type) {
        this.type = type;
        return this;
    }
}
