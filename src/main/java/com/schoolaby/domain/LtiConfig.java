package com.schoolaby.domain;

import com.querydsl.core.annotations.QueryInit;
import com.schoolaby.domain.enumeration.LtiConfigType;
import lombok.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.Loader;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;

import static com.schoolaby.domain.enumeration.LtiConfigType.JOURNEY;
import static javax.persistence.EnumType.STRING;
import static javax.persistence.GenerationType.SEQUENCE;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.hibernate.annotations.CacheConcurrencyStrategy.NONSTRICT_READ_WRITE;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "lti_config")
@Cache(usage = NONSTRICT_READ_WRITE)
@SQLDelete(sql = "UPDATE lti_config SET deleted = now() WHERE id = ?")
@Loader(namedQuery = "findLtiConfigById")
@NamedQuery(name = "findLtiConfigById", query = "SELECT lc FROM LtiConfig lc WHERE lc.id = ?1 AND lc.deleted IS NULL")
@Where(clause = "deleted IS NULL")
public class LtiConfig extends AbstractAuditingEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "sequenceGenerator")
    private Long id;
    private String consumerKey;
    private String sharedSecret;
    private String deploymentId;

    private String launchUrl;
    private String deepLinkingUrl;
    private String loginInitiationUrl;
    private String redirectHost;
    private String jwksUrl;

    @ManyToOne
    private LtiApp ltiApp;

    @ManyToOne
    @QueryInit("milestones.assignments")
    private Journey journey;

    @Builder.Default
    @Enumerated(STRING)
    private LtiConfigType type = JOURNEY;

    public String getLaunchUrl() {
        return isBlank(launchUrl) ? ltiApp.getLaunchUrl() : launchUrl;
    }

    public String getDeepLinkingUrl() {
        return isBlank(deepLinkingUrl) ? ltiApp.getDeepLinkingUrl() : deepLinkingUrl;
    }

    public String getLoginInitiationUrl() {
        return isBlank(loginInitiationUrl) ? ltiApp.getLoginInitiationUrl() : loginInitiationUrl;
    }

    public String getRedirectHost() {
        return isBlank(redirectHost) ? ltiApp.getRedirectHost() : redirectHost;
    }

    public String getJwksUrl() {
        return isBlank(jwksUrl) ? ltiApp.getJwksUrl() : jwksUrl;
    }
}
