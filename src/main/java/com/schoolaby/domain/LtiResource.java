package com.schoolaby.domain;

import com.querydsl.core.annotations.QueryInit;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.Loader;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import static java.util.UUID.randomUUID;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.CascadeType.REMOVE;
import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.SEQUENCE;
import static org.hibernate.annotations.CacheConcurrencyStrategy.NONSTRICT_READ_WRITE;

@Entity
@Table(name = "lti_resource")
@Cache(usage = NONSTRICT_READ_WRITE)
@SQLDelete(sql = "UPDATE lti_resource SET deleted = now() WHERE id = ?")
@Loader(namedQuery = "findLtiResourceById")
@NamedQuery(name = "findLtiResourceById", query = "SELECT lr FROM LtiResource lr WHERE lr.id = ?1 AND lr.deleted IS NULL")
@Where(clause = "deleted IS NULL")
@Getter
@Setter
public class LtiResource extends AbstractAuditingEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "sequenceGenerator")
    private Long id;
    @NotNull
    private String title;
    private String description;
    private String resourceLinkId = randomUUID().toString();
    private String launchUrl;
    private String toolResourceId;
    private boolean syncGrade;
    private Integer sequenceNumber;
    @ManyToOne
    private LtiApp ltiApp;
    @ManyToOne(fetch = LAZY)
    @QueryInit("journey.people")
    private Milestone milestone;
    @ManyToOne
    @QueryInit("milestone.journey.people")
    private Assignment assignment;
    @OneToMany(mappedBy = "ltiResource", cascade = REMOVE)
    private Set<LtiLaunch> ltiLaunches = new HashSet<>();
    @OneToMany(mappedBy = "ltiResource", cascade = ALL, orphanRemoval = true)
    private Set<LtiLineItem> lineItems = new HashSet<>();

    public boolean isResubmittable() {
        return ltiApp.getResubmittable();
    }

    public LtiResource addLtiLaunch(LtiLaunch ltiLaunch) {
        ltiLaunches.add(ltiLaunch);
        ltiLaunch.setLtiResource(this);
        return this;
    }

    public LtiResource addLtiLineItem(LtiLineItem lineItem) {
        this.lineItems.add(lineItem);
        lineItem.setLtiResource(this);
        return this;
    }

    public void setLineItems(Set<LtiLineItem> lineItems) {
        this.lineItems.clear();
        if (lineItems != null) {
            lineItems.forEach(this::addLtiLineItem);
        }
    }

    public Journey getJourney() {
        if (milestone != null) {
            return milestone.getJourney();
        } else if (assignment != null) {
            return assignment.getMilestone().getJourney();
        }
        return null;
    }

    public boolean isLtiAdvantage() {
        return this.getLtiApp().isLtiAdvantage();
    }
}
