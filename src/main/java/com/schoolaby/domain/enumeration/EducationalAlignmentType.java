package com.schoolaby.domain.enumeration;

public enum EducationalAlignmentType {

    SUBJECT("subject"),
    SUBJECT_AREA("subjectArea");

    private final String alignmentType;

    EducationalAlignmentType(String alignmentType) {
        this.alignmentType = alignmentType;
    }

    public String getAlignmentType() {
        return alignmentType;
    }
}
