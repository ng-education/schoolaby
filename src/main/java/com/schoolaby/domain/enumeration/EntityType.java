package com.schoolaby.domain.enumeration;

public enum EntityType {
    ASSIGNMENT,
    MILESTONE,
    JOURNEY,
    SUBMISSION,
}
