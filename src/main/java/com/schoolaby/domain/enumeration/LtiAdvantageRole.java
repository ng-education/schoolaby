// CHECKSTYLE:OFF
package com.schoolaby.domain.enumeration;

import com.schoolaby.security.Role;

import java.util.List;

public enum LtiAdvantageRole {
    STUDENT("http://purl.imsglobal.org/vocab/lis/v2/membership#Learner", Role.Constants.STUDENT),
    INSTRUCTOR("http://purl.imsglobal.org/vocab/lis/v2/membership#Instructor", Role.Constants.TEACHER),
    ADMIN("http://purl.imsglobal.org/vocab/lis/v2/membership#Administrator", Role.Constants.ADMIN);

    private final String roleName;
    private final String authorityConstant;

    LtiAdvantageRole(String roleName, String authorityConstant) {
        this.roleName = roleName;
        this.authorityConstant = authorityConstant;
    }

    public String getRoleName() {
        return roleName;
    }

    public String getAuthorityConstant() {
        return authorityConstant;
    }

    public static LtiAdvantageRole getRole(String authorityConstant) {
        return List
            .of(LtiAdvantageRole.values())
            .stream()
            .filter(ltiRole -> ltiRole.getAuthorityConstant().equals(authorityConstant))
            .findFirst()
            .orElseThrow();
    }
}
// CHECKSTYLE:ON
