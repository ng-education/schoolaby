package com.schoolaby.domain.enumeration;

public enum LtiConfigType {
    JOURNEY,
    PLATFORM
}
