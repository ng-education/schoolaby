package com.schoolaby.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.*;

import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.GenerationType.*;
import static org.hibernate.annotations.CacheConcurrencyStrategy.*;

@Entity
@Table(name = "chat")
@Cache(usage = READ_WRITE)
@SQLDelete(sql = "UPDATE chat SET deleted = now() WHERE id = ?")
@Loader(namedQuery = "findChatById")
@NamedQuery(name = "findChatById", query = "SELECT c FROM Chat c WHERE c.id = ?1 AND c.deleted IS NULL")
@Where(clause = "deleted IS NULL")
public class Chat extends AbstractAuditingEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "title", nullable = false)
    private String title;

    @ManyToOne
    private Submission submission;

    @ManyToOne
    private Journey journey;

    @OneToMany(mappedBy = "chat")
    @Cache(usage = READ_WRITE)
    @OrderBy("createdDate,id")
    private Set<Message> messages = new HashSet<>();

    @ManyToMany
    @Cache(usage = READ_WRITE)
    @JoinTable(
        name = "chat_people",
        joinColumns = @JoinColumn(name = "chat_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "person_id", referencedColumnName = "id")
    )
    @OrderBy("firstName,id")
    private Set<User> people = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Chat title(String title) {
        this.title = title;
        return this;
    }

    public Submission getSubmission() {
        return submission;
    }

    public void setSubmission(Submission submission) {
        this.submission = submission;
    }

    public Chat submission(final Submission submission) {
        setSubmission(submission);
        return this;
    }

    public Journey getJourney() {
        return journey;
    }

    public Chat journey(Journey journey) {
        setJourney(journey);
        return this;
    }

    public void setJourney(Journey journey) {
        this.journey = journey;
    }

    public Set<Message> getMessages() {
        return messages;
    }

    public Chat addMessage(Message message) {
        this.messages.add(message);
        message.setChat(this);
        return this;
    }

    public Chat removeMessage(Message message) {
        this.messages.remove(message);
        message.setChat(null);
        return this;
    }

    public void setMessages(Set<Message> messages) {
        this.messages = messages;
    }

    public Set<User> getPeople() {
        return people;
    }

    public Chat people(Set<User> people) {
        this.people = people;
        return this;
    }

    public Chat addPerson(User person) {
        this.people.add(person);
        person.getChats().add(this);
        return this;
    }

    public Chat removePeople(User person) {
        this.people.remove(person);
        person.getChats().remove(this);
        return this;
    }

    public void setPeople(Set<User> people) {
        this.people = people;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Chat)) {
            return false;
        }
        return id != null && id.equals(((Chat) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Chat{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            "}";
    }
}
