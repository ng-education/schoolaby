package com.schoolaby.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.schoolaby.service.dto.states.EntityState;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.*;

import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static com.schoolaby.config.filter.TenantServiceAspect.FILTER_HIDDEN;
import static com.schoolaby.config.filter.TenantServiceAspect.STUDENT_FILTER;
import static com.schoolaby.domain.Assignment.STUDENT_IN_ASSIGNMENT_STUDENTS_OR_ASSIGNMENT_STUDENTS_EMPTY;
import static com.schoolaby.security.Role.Constants.STUDENT;
import static com.schoolaby.security.Role.Constants.TEACHER;
import static com.schoolaby.security.SecurityUtils.getCurrentUserId;
import static java.time.Instant.now;
import static java.util.stream.Collectors.toSet;
import static javax.persistence.CascadeType.*;
import static org.hibernate.annotations.CacheConcurrencyStrategy.READ_WRITE;

@Entity
@Getter
@Setter
@Table(name = "milestone")
@Cache(usage = READ_WRITE)
@SQLDelete(sql = "UPDATE milestone SET deleted = now() WHERE id = ?")
@Loader(namedQuery = "findMilestoneById")
@NamedQuery(name = "findMilestoneById", query = "SELECT m FROM Milestone m WHERE m.id = ?1 AND m.deleted IS NULL")
@Where(clause = "deleted IS NULL")
@FilterDef(name = FILTER_HIDDEN)
@FilterDef(name = STUDENT_FILTER,
    parameters = @ParamDef(name = "currentUserId", type = "long"))
@Filter(name = FILTER_HIDDEN, condition = "published IS NOT NULL AND published < NOW()")
public class Milestone extends AbstractAuditingEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "end_date")
    private Instant endDate;

    @OneToMany(mappedBy = "milestone", cascade = {REFRESH, DETACH, PERSIST, REMOVE})
    @OrderBy("deadline, id")
    @Filter(name = STUDENT_FILTER, condition = STUDENT_IN_ASSIGNMENT_STUDENTS_OR_ASSIGNMENT_STUDENTS_EMPTY)
    @Filter(name = FILTER_HIDDEN, condition = "published IS NOT NULL AND published < NOW()")
    @Cache(usage = READ_WRITE)
    private Set<Assignment> assignments = new HashSet<>();

    @ManyToMany(cascade = {PERSIST, MERGE})
    @Cache(usage = READ_WRITE)
    @JoinTable(
        name = "milestone_materials",
        joinColumns = @JoinColumn(name = "milestone_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "materials_id", referencedColumnName = "id")
    )
    private Set<Material> materials = new HashSet<>();

    @ManyToMany
    @Cache(usage = READ_WRITE)
    @JoinTable(
        name = "milestone_educational_alignments",
        joinColumns = @JoinColumn(name = "milestone_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "educational_alignments_id", referencedColumnName = "id")
    )
    private Set<EducationalAlignment> educationalAlignments = new HashSet<>();

    @ManyToOne(cascade = {REFRESH})
    @JsonIgnoreProperties(value = "milestones", allowSetters = true)
    private Journey journey;

    @ManyToOne
    @JsonIgnoreProperties(value = "milestones", allowSetters = true)
    private User creator;

    @OneToMany(mappedBy = "milestone", cascade = ALL, orphanRemoval = true)
    private Set<LtiResource> ltiResources = new HashSet<>();

    @Column(name = "published")
    private Instant published;

    @OneToMany(mappedBy = "milestone", cascade = {PERSIST, MERGE, REMOVE})
    private Set<MilestoneState> states = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Milestone id(Long id) {
        setId(id);
        return this;
    }

    public Milestone title(String title) {
        setTitle(title);
        return this;
    }

    public Milestone description(String description) {
        setDescription(description);
        return this;
    }

    public Milestone endDate(Instant endDate) {
        setEndDate(endDate);
        return this;
    }

    public Milestone assignments(Set<Assignment> assignments) {
        setAssignments(assignments);
        return this;
    }

    public Milestone addAssignment(Assignment assignment) {
        assignment.setMilestone(this);
        Set<Assignment> newAssignments = new HashSet<>(getAssignments());
        newAssignments.add(assignment);
        setAssignments(newAssignments);
        return this;
    }

    public Milestone removeAssignment(Assignment assignment) {
        Set<Assignment> newAssignments = new HashSet<>(getAssignments());
        newAssignments.remove(assignment);
        assignment.setMilestone(null);
        setAssignments(newAssignments);
        return this;
    }

    public void setAssignments(Set<Assignment> assignments) {
        assignments.forEach(assignment -> assignment.setMilestone(this));
        this.assignments = assignments;
    }

    public Set<Material> getMaterials() {
        return materials;
    }

    public Milestone materials(Set<Material> materials) {
        setMaterials(materials);
        return this;
    }

    public Milestone addMaterial(Material material) {
        Set<Material> newMaterials = new HashSet<>(getMaterials());
        material.addMilestone(this);
        newMaterials.add(material);
        setMaterials(newMaterials);
        return this;
    }

    public Milestone removeMaterials(Material material) {
        Set<Material> newMaterials = new HashSet<>(getMaterials());
        newMaterials.remove(material);
        material.removeMilestone(this);
        setMaterials(newMaterials);
        return this;
    }

    public void setMaterials(Set<Material> materials) {
        materials.forEach(material -> material.addMilestone(this));
        this.materials = materials;
    }

    public Set<EducationalAlignment> getEducationalAlignments() {
        return educationalAlignments;
    }

    public Milestone educationalAlignments(Set<EducationalAlignment> educationalAlignments) {
        setEducationalAlignments(educationalAlignments);
        return this;
    }

    public Milestone addEducationalAlignment(EducationalAlignment educationalAlignment) {
        Set<EducationalAlignment> newEducationAlignments = new HashSet<>(getEducationalAlignments());
        educationalAlignment.addMilestones(this);
        newEducationAlignments.add(educationalAlignment);
        setEducationalAlignments(newEducationAlignments);
        return this;
    }

    public Milestone removeEducationalAlignments(EducationalAlignment educationalAlignment) {
        Set<EducationalAlignment> newEducationAlignments = new HashSet<>(getEducationalAlignments());
        newEducationAlignments.remove(educationalAlignment);
        educationalAlignment.removeMilestones(this);
        setEducationalAlignments(newEducationAlignments);
        return this;
    }

    public void setEducationalAlignments(Set<EducationalAlignment> educationalAlignments) {
        this.educationalAlignments = educationalAlignments;
    }

    public Milestone journey(Journey journey) {
        this.setJourney(journey);
        return this;
    }

    public Milestone creator(User person) {
        setCreator(person);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Milestone)) {
            return false;
        }
        return id != null && id.equals(((Milestone) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Milestone{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", description='" + getDescription() + "'" +
            ", endDate='" + getEndDate() + "'" +
            "}";
    }

    public void setLtiResources(Set<LtiResource> ltiResources) {
        ltiResources.forEach(ltiResource -> ltiResource.setMilestone(this));
        this.ltiResources = ltiResources;
    }

    public boolean isHidden() {
        return published == null || published.isAfter(now());
    }

    public Milestone published(Instant published) {
        setPublished(published);
        return this;
    }

    public Milestone addLtiResource(LtiResource ltiResource) {
        this.ltiResources.add(ltiResource);
        ltiResource.setMilestone(this);
        return this;
    }

    public void addState(MilestoneState state) {
        getStates().add(state);
    }

    public Milestone states(Set<MilestoneState> states) {
        setStates(states);
        return this;
    }

    private Optional<MilestoneState> getExistingState(MilestoneState state) {
        return getStates()
            .stream()
            .filter(milestoneState -> milestoneState.equals(state))
            .findFirst();
    }

    public void calculateStates() {
        calculateStateByAuthority(getJourney().getJourneyStudentsUsers(), STUDENT);
        calculateStateByAuthority(getJourney().getJourneyTeachersUsers(), TEACHER);
    }

    private void calculateStateByAuthority(Set<User> users, String authorityName) {
        users.forEach(user -> calculateAndSaveState(new MilestoneState()
            .milestone(this)
            .user(user)
            .authority(new Authority().name(authorityName))));
    }

    private void calculateAndSaveState(MilestoneState state) {
        getExistingState(state).ifPresentOrElse(MilestoneState::calculate,
            () -> addState(state.calculate())
        );
    }

    public EntityState getState() {
        Long userId = getCurrentUserId();
        boolean isStudent = getJourney().isStudentInJourney(userId);
        boolean isTeacher = getJourney().isTeacherInJourney(userId);

        if (!isStudent && !isTeacher) {
            return null;
        }
        String authorityName = isTeacher ? TEACHER : STUDENT;
        return getState(getCurrentUserId(), authorityName);
    }

    public EntityState getState(Long userId, String authorityName) {
        Optional<MilestoneState> state = getStates()
            .stream().filter(milestoneState -> milestoneState.getUser().getId().equals(userId) && milestoneState.getAuthority().getName().equals(authorityName)).findFirst();
        return state.map(MilestoneState::getState).orElse(null);
    }
}
