package com.schoolaby.domain;

import com.schoolaby.service.dto.states.EntityState;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.slf4j.Logger;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import static com.schoolaby.security.Role.Constants.TEACHER;
import static com.schoolaby.service.dto.states.EntityState.*;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.hibernate.annotations.CacheConcurrencyStrategy.READ_WRITE;
import static org.slf4j.LoggerFactory.getLogger;

@Entity
@Cache(usage = READ_WRITE)
@Getter
@Setter
public class AssignmentState implements Serializable {
    private static final Logger log = getLogger(AssignmentState.class);

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "assignmentStateGenerator")
    @SequenceGenerator(name = "assignmentStateGenerator", sequenceName = "assignment_state_seq", allocationSize = 1)
    private Long id;

    @ManyToOne
    private Assignment assignment;

    @ManyToOne
    private User user;

    @ManyToOne
    @JoinColumn(name = "authority_name", referencedColumnName = "name")
    private Authority authority;

    private String state;

    public EntityState getState() {
        return state != null ? EntityState.valueOf(state) : null;
    }

    public AssignmentState setState(EntityState state) {
        if (state != null) {
            this.state = state.toString();
        }
        return this;
    }

    public AssignmentState assignment(Assignment assignment) {
        setAssignment(assignment);
        return this;
    }

    public AssignmentState user(User user) {
        setUser(user);
        return this;
    }

    public AssignmentState authority(Authority authority) {
        setAuthority(authority);
        return this;
    }

    public AssignmentState calculate() {
        validateFieldsForCalculation();

        if (getAuthority().getName().equals(TEACHER)) {
            EntityState stateByTeacherRole = getStateByTeacherRole();
            if (stateByTeacherRole == UNKNOWN) {
                log.warn("Assignment state could not be calculated for teacher:{}, assignment:{}", getUser().getId(), getId());
            }
            setState(stateByTeacherRole);
        } else {
            EntityState stateByStudentRole = getStateByStudentRole();
            if (stateByStudentRole == UNKNOWN) {
                log.warn("Assignment state could not be calculated for student:{}, assignment:{}", getUser().getId(), getId());
            }
            setState(stateByStudentRole);
        }
        return this;
    }

    private void validateFieldsForCalculation() {
        if (getUser() == null) {
            throw new IllegalStateException("User field is required to calculate state");
        } else if (getAssignment() == null) {
            throw new IllegalStateException("Assignment field is required to calculate state");
        } else if (getAuthority() == null) {
            throw new IllegalStateException("Authority field is required to calculate state");
        }
    }

    private EntityState getStateByTeacherRole() {
        Set<Submission> assignedStudentsSubmissions = getAssignment().getAssignedStudentsSubmissions();
        Optional<EntityState> allSubmissionsGraded = allSubmissionsGraded(assignedStudentsSubmissions);
        return Stream.of(
                allSubmissionsGraded,
                urgentAndAnySubmissionMissingOrNotGraded(allSubmissionsGraded),
                overdueAndAnySubmissionMissingOrNotGraded(allSubmissionsGraded),
                deadlineInFutureAndHasNoStudents()
            )
            .flatMap(Optional::stream)
            .findFirst()
            .orElse(UNKNOWN);
    }

    public EntityState getStateByStudentRole() {
        Submission studentSubmission = getAssignment().findSubmission(getUser().getId()).orElse(null);

        return Stream.of(
                studentSubmissionUrgent(studentSubmission, getUser().getId()),
                studentNotStarted(studentSubmission, getUser().getId()),
                hasStudentFailed(studentSubmission, getUser().getId()),
                hasStudentCompletedAssignment(studentSubmission, getUser().getId()),
                isStudentSubmissionOverdue(studentSubmission, getUser().getId()),
                isStudentSubmissionInProgress(studentSubmission, getUser().getId()),
                isStudentSubmissionRejected(studentSubmission, getUser().getId())
            )
            .flatMap(Optional::stream)
            .findFirst()
            .orElse(UNKNOWN);
    }

    private Optional<EntityState> allSubmissionsGraded(Set<Submission> assignedStudentsSubmissions) {
        if (!getAssignment().isRequiresSubmission() && getAssignment().deadlinePassed()) {
            return of(COMPLETED);
        }

        boolean areExistingSubmissionsGraded = assignedStudentsSubmissions.stream()
            .allMatch(submission -> submission.isCompleted() || submission.isRejected() || submission.isFailed());
        if (areExistingSubmissionsGraded && getAssignment().isRequiresSubmission()
            && (!getAssignment().isGroupAssignment() && assignedStudentsSubmissions.size() == getAssignment().getAssignedStudents().size()
            || getAssignment().isGroupAssignment() && assignedStudentsSubmissions.size() >= getAssignment().getGroups().size()
        )
            && (journeyHasStudents() || getAssignment().deadlinePassed())) {
            return of(COMPLETED);
        }
        return Optional.empty();
    }

    private Optional<EntityState> urgentAndAnySubmissionMissingOrNotGraded(Optional<EntityState> allSubmissionsGraded) {
        if (getAssignment().dueByTomorrow() &&
            journeyHasStudents() &&
            (!getAssignment().isRequiresSubmission() || atLeastOneSubmissionMissing() || allSubmissionsGraded.isEmpty())
        ) {
            return of(URGENT);
        }
        return Optional.empty();
    }

    private Optional<EntityState> overdueAndAnySubmissionMissingOrNotGraded(Optional<EntityState> allSubmissionsGraded) {
        if (getAssignment().deadlinePassed() && journeyHasStudents() && (atLeastOneSubmissionMissing() || allSubmissionsGraded.isEmpty())) {
            return of(OVERDUE);
        }
        return Optional.empty();
    }

    private Optional<EntityState> deadlineInFutureAndHasNoStudents() {
        if (!getAssignment().dueByTomorrow() || !journeyHasStudents()) {
            return of(NOT_STARTED);
        }
        return Optional.empty();
    }

    private boolean journeyHasStudents() {
        return !getAssignment().getMilestone().getJourney().getStudents().isEmpty();
    }

    private boolean atLeastOneSubmissionMissing() {
        int expectedSubmissionsCount = getAssignment().isGroupAssignment() ? getAssignment().getGroups().size() : getAssignment().getAssignedStudents().size();
        return getAssignment().getSubmittedSubmissions().size() < expectedSubmissionsCount;
    }

    private Optional<EntityState> studentSubmissionUrgent(Submission submission, Long studentId) {
        if (getAssignment().dueByTomorrow() &&
            (!getAssignment().isRequiresSubmission() ||
                submission == null ||
                submission.isStudentSubmissionInProgress(studentId))) {
            return of(URGENT);
        }
        return empty();
    }

    private Optional<EntityState> studentNotStarted(Submission submission, Long studentId) {
        if (!getAssignment().isRequiresSubmission() && !getAssignment().deadlinePassed()) {
            return of(NOT_STARTED);
        } else if (getAssignment().isRequiresSubmission() && !getAssignment().deadlinePassed() && (submission == null || submission.isStudentSubmissionInProgress(studentId))) {
            return of(NOT_STARTED);
        }
        return empty();
    }

    private Optional<EntityState> hasStudentFailed(Submission submission, Long studentId) {
        if (!getAssignment().isRequiresSubmission()) {
            return empty();
        }
        if (submission != null && submission.hasStudentFailed(studentId)) {
            return of(FAILED);
        } else if (!getAssignment().isFlexibleDeadline() && getAssignment().deadlinePassed()
            && (submission == null || submission.hasStudentFailed(studentId))) {
            return of(FAILED);
        } else {
            return empty();
        }
    }

    private Optional<EntityState> hasStudentCompletedAssignment(Submission submission, Long studentId) {
        return (getAssignment().isRequiresSubmission() && submission != null && submission.hasStudentCompleted(studentId))
            || (!getAssignment().isRequiresSubmission() && getAssignment().deadlinePassed())
            ? of(COMPLETED) : empty();
    }

    private Optional<EntityState> isStudentSubmissionInProgress(Submission submission, Long studentId) {
        if ((submission != null && (submission.isStudentSubmissionInProgress(studentId) || submission.hasStudentSubmitted(studentId)))) {
            return of(IN_PROGRESS);
        }
        return empty();
    }

    private Optional<EntityState> isStudentSubmissionOverdue(Submission submission, Long studentId) {
        if ((submission == null || submission.isStudentSubmissionInProgress(studentId)) && getAssignment().deadlinePassed() && getAssignment().isFlexibleDeadline()) {
            return of(OVERDUE);
        }
        return empty();
    }

    private Optional<EntityState> isStudentSubmissionRejected(Submission submission, Long studentId) {
        if (submission != null && submission.isStudentSubmissionRejected(studentId)) {
            return of(REJECTED);
        }
        return empty();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof AssignmentState) {
            AssignmentState assignmentState = (AssignmentState) obj;

            if (assignmentState.getAssignment().getId() == null) {
                return false;
            }

            return assignmentState.getAssignment().getId().equals(getAssignment().getId()) &&
                assignmentState.getUser().getId().equals(getUser().getId()) &&
                assignmentState.getAuthority().getName().equals(getAuthority().getName());
        }
        return false;
    }

}
