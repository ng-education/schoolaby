package com.schoolaby.repository.common;

import com.querydsl.core.types.Predicate;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import javax.annotation.Nonnull;
import java.util.List;

public interface CollectionsQdslPredicateExecutor<T> extends QuerydslPredicateExecutor<T> {
    @Override
    @Nonnull
    List<T> findAll(@Nonnull Predicate predicate);
}
