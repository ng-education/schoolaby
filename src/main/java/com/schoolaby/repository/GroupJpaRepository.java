package com.schoolaby.repository;

import com.schoolaby.domain.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface GroupJpaRepository extends JpaRepository<Group, Long> {

    Optional<Group> findFirstById(Long id);
}
