package com.schoolaby.repository;

import com.schoolaby.config.EkoolProperties;
import com.schoolaby.service.dto.EkoolPersonalDatasDTO;
import com.schoolaby.service.dto.EkoolRolesDTO;
import com.schoolaby.service.dto.EkoolSchoolsDTO;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@Repository
public class EkoolRepository {
    private final EkoolProperties ekoolProperties;
    private final RestTemplate ekoolRestTemplate;

    public EkoolRepository(EkoolProperties ekoolProperties, RestTemplate ekoolRestTemplate) {
        this.ekoolProperties = ekoolProperties;
        this.ekoolRestTemplate = ekoolRestTemplate;
    }

    public HttpEntity<String> createEntity(OAuth2AccessToken oAuth2AccessToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.set(AUTHORIZATION, "Bearer " + oAuth2AccessToken.getTokenValue());
        return new HttpEntity<>(headers);
    }

    public EkoolSchoolsDTO getUserSchools(OAuth2AccessToken oAuth2AccessToken) {
        String userSchoolsUri = ekoolProperties.getUserSchoolsUri();
        return ekoolRestTemplate.exchange(
            userSchoolsUri,
            HttpMethod.GET,
            createEntity(oAuth2AccessToken),
            EkoolSchoolsDTO.class
        ).getBody();
    }

    public EkoolRolesDTO getUserRoles(OAuth2AccessToken oAuth2AccessToken) {
        String userSchoolsUri = ekoolProperties.getUserRolesUri();
        return ekoolRestTemplate.exchange(
            userSchoolsUri,
            HttpMethod.GET,
            createEntity(oAuth2AccessToken),
            EkoolRolesDTO.class
        ).getBody();
    }

    public EkoolPersonalDatasDTO getUserBasic(OAuth2AccessToken oAuth2AccessToken) {
        String userSchoolsUri = ekoolProperties.getUserBasicUri();
        return ekoolRestTemplate.exchange(
            userSchoolsUri,
            HttpMethod.GET,
            createEntity(oAuth2AccessToken),
            EkoolPersonalDatasDTO.class
        ).getBody();
    }
}
