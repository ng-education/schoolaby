package com.schoolaby.repository;

import com.schoolaby.domain.Journey;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.query.Param;

public interface JourneyJpaRepository extends JpaRepository<Journey, Long>, QuerydslPredicateExecutor<Journey> {

    @Query(value = "SELECT j " +
        "FROM Journey j " +
        "   JOIN JourneyState js ON js.journey.id = j.id " +
        "WHERE js.user.id = :userId " +
        "  AND js.state = :state " +
        "  AND js.authority.name = :authorityName " +
        "  AND j.deleted IS NULL")
    Page<Journey> findAllJourneysByStateAndAuthorityName(@Param("userId") Long userId, @Param("state") String state, @Param("authorityName") String authorityName, Pageable pageable);

    @Query(value = "SELECT j " +
        "FROM Journey j " +
        "   JOIN JourneyState js ON js.journey.id = j.id " +
        "WHERE js.user.id = j.creator.id " +
        "  AND js.state = :state " +
        "  AND js.authority.name = 'ROLE_TEACHER' " +
        "  AND j.deleted IS NULL")
    Page<Journey> findAllJourneysByStateForAdmin(@Param("state") String state, Pageable pageable);

}
