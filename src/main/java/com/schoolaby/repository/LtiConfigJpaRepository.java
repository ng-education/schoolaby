package com.schoolaby.repository;

import com.schoolaby.domain.LtiConfig;
import com.schoolaby.repository.common.CollectionsQdslPredicateExecutor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LtiConfigJpaRepository extends JpaRepository<LtiConfig, Long>, CollectionsQdslPredicateExecutor<LtiConfig> {
}

