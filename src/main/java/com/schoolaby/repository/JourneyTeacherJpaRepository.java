package com.schoolaby.repository;

import com.schoolaby.domain.JourneyTeacher;
import com.schoolaby.domain.keys.JourneyUserKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JourneyTeacherJpaRepository extends JpaRepository<JourneyTeacher, JourneyUserKey> {
    void deleteByJourneyIdAndUserId(Long journeyId, Long userId);
}
