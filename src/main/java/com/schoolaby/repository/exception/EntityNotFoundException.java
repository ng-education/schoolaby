package com.schoolaby.repository.exception;

import static java.lang.String.format;

public class EntityNotFoundException extends javax.persistence.EntityNotFoundException {
    public EntityNotFoundException(Class clazz, Long id) {
        super(format("%s with id '%s' not found!", clazz, id));
    }

    public EntityNotFoundException(Class clazz, String fieldName, String fieldValue) {
        super(format("%s with %s '%s' not found!", clazz, fieldName, fieldValue));
    }
}
