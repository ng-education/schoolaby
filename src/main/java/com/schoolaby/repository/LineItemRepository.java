package com.schoolaby.repository;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.schoolaby.domain.LtiLineItem;
import com.schoolaby.repository.exception.EntityNotFoundException;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

import static com.schoolaby.domain.QLtiLineItem.ltiLineItem;

@Repository
public class LineItemRepository extends BaseQuerydslRepository {
    private final LineItemJpaRepository jpaRepository;

    public LineItemRepository(LineItemJpaRepository jpaRepository, JPAQueryFactory queryFactory) {
        super(LtiLineItem.class, queryFactory);
        this.jpaRepository = jpaRepository;
    }

    private BooleanBuilder predicate() {
        return new BooleanBuilder();
    }

    public List<LtiLineItem> findAll(Long journeyId, String resourceLinkId) {
        BooleanBuilder predicate = predicate();
        if (journeyId != null) {
            predicate.and(ltiLineItem.ltiResource.milestone.journey.id.eq(journeyId)
                .or(ltiLineItem.ltiResource.assignment.milestone.journey.id.eq(journeyId)));
        }
        if (resourceLinkId != null) {
            predicate.and(ltiLineItem.ltiResource.resourceLinkId.eq(resourceLinkId));
        }
        return queryFactory.selectFrom(ltiLineItem)
            .leftJoin(ltiLineItem.ltiResource.milestone)
            .leftJoin(ltiLineItem.ltiResource.assignment)
            .where(predicate)
            .fetch();
    }

    public Optional<LtiLineItem> findOne(Long id) {
        return jpaRepository.findOne(ltiLineItem.id.eq(id));
    }

    public LtiLineItem getOne(Long id) {
        return findOne(id).orElseThrow(() -> new EntityNotFoundException(LtiLineItem.class, id));
    }
}
