package com.schoolaby.repository;

import com.schoolaby.domain.EducationalAlignment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface EducationalAlignmentJpaRepository extends JpaRepository<EducationalAlignment, Long>, QuerydslPredicateExecutor<EducationalAlignment> {
    EducationalAlignment getByTaxonId(Long taxonId);
}
