package com.schoolaby.repository.filter;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class LtiConfigFilter {
    private final Long appId;
    private final Long journeyId;
    private final String deepLinkingUrl;
    private final String launchUrl;
    private final String appClientId;
    private final String ltiConfigType;
    private final String redirectHost;
}
