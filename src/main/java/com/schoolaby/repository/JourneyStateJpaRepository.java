package com.schoolaby.repository;

import com.schoolaby.domain.JourneyState;
import com.schoolaby.domain.keys.JourneyUserKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JourneyStateJpaRepository extends JpaRepository<JourneyState, JourneyUserKey> {
    void deleteByJourneyIdAndUserIdAndAuthorityName(Long journeyId, Long userId, String authorityName);
}
