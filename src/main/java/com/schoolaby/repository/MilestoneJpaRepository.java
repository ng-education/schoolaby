package com.schoolaby.repository;

import com.schoolaby.domain.Milestone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface MilestoneJpaRepository extends JpaRepository<Milestone, Long>, QuerydslPredicateExecutor<Milestone> {
}
