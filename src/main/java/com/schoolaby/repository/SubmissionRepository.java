package com.schoolaby.repository;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.schoolaby.domain.Submission;
import com.schoolaby.security.Role;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityNotFoundException;
import java.time.Instant;
import java.util.Optional;
import java.util.Set;

import static com.schoolaby.domain.QSubmission.submission;
import static com.schoolaby.security.SecurityUtils.getCurrentUserId;
import static com.schoolaby.security.SecurityUtils.isCurrentUserInRole;

@Repository
public class SubmissionRepository extends QuerydslRepositorySupport {
    private final SubmissionJpaRepository jpaRepository;

    public SubmissionRepository(SubmissionJpaRepository jpaRepository) {
        super(Submission.class);
        this.jpaRepository = jpaRepository;
    }

    private BooleanBuilder predicate() {
        BooleanExpression isInStudents = submission.assignment.milestone.journey.students.any().user.id.eq(getCurrentUserId());
        BooleanBuilder predicate = new BooleanBuilder();
        if (isCurrentUserInRole(Role.Constants.TEACHER)) {
            predicate.and(
                (submission.assignment.milestone.journey.teachers.any().user.id.eq(getCurrentUserId()))
                    .or(isInStudents.and(submission.authors.any().id.eq(getCurrentUserId())))
            );
        } else if (!isCurrentUserInRole(Role.Constants.ADMIN)) {
            predicate.and(submission.assignment.published.before(Instant.now()))
                .and(submission.assignment.milestone.published.before(Instant.now()))
                .and(submission.authors.any().id.eq(getCurrentUserId())
                    .or(submission.group.isNotNull().and(submission.group.students.any().id.eq(getCurrentUserId()))))
                .and(isInStudents);
        }
        return predicate;
    }

    public Page<Submission> findAll(Pageable pageable, Long journeyId, Long assignmentId) {
        BooleanBuilder predicate = predicate();
        if (assignmentId != null) {
            predicate.and(submission.assignment.id.eq(assignmentId));
        }
        if (journeyId != null) {
            predicate.and(submission.assignment.milestone.journey.id.eq(journeyId));
        }
        return jpaRepository.findAll(predicate, pageable);
    }

    public Optional<Submission> findOne(Long id) {
        return jpaRepository.findOne(predicate().and(submission.id.eq(id)));
    }

    public Optional<Submission> findOneByAssignmentIdAndAuthor(Long assignmentId, Long authorId) {
        return jpaRepository.findOne(predicate()
            .and(submission.assignment.id.eq(assignmentId)
                .and(submission.authors.any().id.eq(authorId))));
    }

    public Optional<Submission> findOneByAssignmentIdAndGroup(Long assignmentId, Long groupId) {
        return jpaRepository.findOne(predicate()
            .and(submission.assignment.id.eq(assignmentId)
                .and(submission.group.id.eq(groupId))));
    }

    public Submission save(Submission submission) {
        return jpaRepository.save(submission);
    }

    public boolean existsById(Long id) {
        return jpaRepository.exists(predicate().and(submission.id.eq(id)));
    }

    public void deleteById(Long id) {
        if (existsById(id)) {
            jpaRepository.deleteById(id);
        } else {
            throw new EntityNotFoundException();
        }
    }

    public void deleteAll(Set<Submission> submissions) {
        jpaRepository.deleteAll(submissions);
    }
}
