package com.schoolaby.repository;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.schoolaby.domain.Chat;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.Optional;

import static com.schoolaby.domain.QChat.chat;
import static com.schoolaby.security.Role.Constants.ADMIN;
import static com.schoolaby.security.Role.Constants.TEACHER;
import static com.schoolaby.security.SecurityUtils.getCurrentUserId;
import static com.schoolaby.security.SecurityUtils.isCurrentUserInRole;
import static java.util.stream.Collectors.toList;

@Repository
public class ChatRepository extends BaseQuerydslRepository {
    private final ChatJpaRepository jpaRepository;

    public ChatRepository(ChatJpaRepository jpaRepository, JPAQueryFactory queryFactory) {
        super(Chat.class, queryFactory);
        this.jpaRepository = jpaRepository;
    }

    private BooleanBuilder predicate() {
        BooleanExpression isInStudents = chat.journey.students.any().user.id.eq(getCurrentUserId());
        BooleanBuilder predicate = new BooleanBuilder();
        predicate.and(chat.people.any().id.eq(getCurrentUserId()));
        if (isCurrentUserInRole(TEACHER)) {
            predicate.and(
                (chat.journey.teachers.any().user.id.eq(getCurrentUserId()))
                    .or(isInStudents)
            );
        } else if (!isCurrentUserInRole(ADMIN)) {
            predicate.and(isInStudents);
        }
        return predicate;
    }

    public Page<Chat> findAllJourneyChats(Pageable pageable, Long journeyId) {
        Page<Chat> chats = jpaRepository.findAll(predicate()
            .and(chat.journey.id.eq(journeyId))
            .and(chat.submission.id.isNull()), pageable);

        return new PageImpl<>(chats.getContent().stream().distinct().collect(toList()), pageable, chats.getTotalElements());
    }

    public Page<Chat> findAllSubmissionChats(Pageable pageable, Long submissionId) {
        Page<Chat> chats = jpaRepository.findAll(predicate()
            .and(chat.submission.id.eq(submissionId)), pageable);

        return new PageImpl<>(chats.getContent().stream().distinct().collect(toList()), pageable, chats.getTotalElements());
    }

    public Optional<Chat> findOne(Long id) {
        return jpaRepository.findOne(predicate().and(chat.id.eq(id)));
    }

    public Page<Chat> findAllSubmissionChatsByJourney(Pageable pageable, Long journeyId) {
        Page<Chat> chats = jpaRepository.findAll(predicate()
            .and(chat.journey.id.eq(journeyId))
            .and(chat.submission.isNotNull()), pageable);

        return new PageImpl<>(chats.getContent().stream().distinct().collect(toList()), pageable, chats.getTotalElements());
    }

    public Chat save(Chat chat) {
        return jpaRepository.save(chat);
    }
}
