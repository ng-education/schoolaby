package com.schoolaby.repository;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.schoolaby.domain.LtiConfig;
import com.schoolaby.domain.enumeration.LtiConfigType;
import com.schoolaby.repository.filter.LtiConfigFilter;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

import static com.querydsl.core.types.dsl.Expressions.asBoolean;
import static com.schoolaby.domain.QLtiConfig.ltiConfig;
import static com.schoolaby.domain.enumeration.LtiConfigType.JOURNEY;
import static com.schoolaby.domain.enumeration.LtiConfigType.PLATFORM;
import static com.schoolaby.security.SecurityUtils.isCurrentUserTeacherOrAdmin;

@Repository
public class LtiConfigRepository extends BaseQuerydslRepository {
    private final LtiConfigJpaRepository ltiConfigJpaRepository;

    public LtiConfigRepository(JPAQueryFactory queryFactory, LtiConfigJpaRepository ltiConfigJpaRepository) {
        super(LtiConfig.class, queryFactory);
        this.ltiConfigJpaRepository = ltiConfigJpaRepository;
    }

    private BooleanExpression platformPredicate() {
        if (isCurrentUserTeacherOrAdmin()) {
            return ltiConfig.type.eq(PLATFORM);
        }
        return asBoolean(true);
    }

    private BooleanExpression journeyPredicate(Long journeyId) {
        return ltiConfig.type.eq(JOURNEY).and(ltiConfig.journey.id.eq(journeyId));
    }

    public Optional<LtiConfig> findOne(LtiConfigFilter ltiConfigFilter) {
        BooleanBuilder predicate = new BooleanBuilder();
        if (ltiConfigFilter.getAppId() != null) {
            predicate.and(ltiConfig.ltiApp.id.eq(ltiConfigFilter.getAppId()));
        }
        if (ltiConfigFilter.getJourneyId() != null) {
            predicate.and(ltiConfig.journey.id.eq(ltiConfigFilter.getJourneyId()).and(ltiConfig.type.eq(JOURNEY))
                .or(ltiConfig.journey.id.isNull().and(ltiConfig.type.eq(PLATFORM))));
        }
        if (ltiConfigFilter.getDeepLinkingUrl() != null) {
            predicate.and(ltiConfig.ltiApp.deepLinkingUrl.eq(ltiConfigFilter.getDeepLinkingUrl()).or(ltiConfig.deepLinkingUrl.eq(ltiConfigFilter.getDeepLinkingUrl())));
        }
        if (ltiConfigFilter.getLaunchUrl() != null) {
            predicate.and(ltiConfig.ltiApp.launchUrl.eq(ltiConfigFilter.getLaunchUrl()).or(ltiConfig.launchUrl.eq(ltiConfigFilter.getLaunchUrl())));
        }
        if (ltiConfigFilter.getRedirectHost() != null) {
            predicate.and(ltiConfig.ltiApp.redirectHost.eq(ltiConfigFilter.getRedirectHost()).or(ltiConfig.redirectHost.eq(ltiConfigFilter.getRedirectHost())));
        }
        if (ltiConfigFilter.getAppClientId() != null && ltiConfigFilter.getLtiConfigType() != null) {
            predicate.and(ltiConfig.ltiApp.clientId.eq(ltiConfigFilter.getAppClientId())
                .and(ltiConfig.type.eq(LtiConfigType.valueOf(ltiConfigFilter.getLtiConfigType()))));
        }
        return ltiConfigJpaRepository.findOne(predicate);
    }

    public List<LtiConfig> findAllByJourneyId(Long journeyId) {
        BooleanExpression predicate = platformPredicate().or(ltiConfig.journey.id.eq(journeyId));
        return ltiConfigJpaRepository.findAll(predicate);
    }

    public List<LtiConfig> findAllByJourneyIdAndJourneyTeacherId(Long journeyId, Long teacherId) {
        BooleanExpression predicate = platformPredicate().or(
            journeyPredicate(journeyId).and(ltiConfig.journey.teachers.any().user.id.eq(teacherId))
        );
        return ltiConfigJpaRepository.findAll(predicate);
    }

    public LtiConfig save(LtiConfig ltiConfig) {
        return ltiConfigJpaRepository.save(ltiConfig);
    }

    public boolean existsById(Long id) {
        return ltiConfigJpaRepository.exists(platformPredicate().or(ltiConfig.id.eq(id)));
    }

    public void deleteById(Long id) {
        if (existsById(id)) {
            ltiConfigJpaRepository.deleteById(id);
        } else {
            throw new EntityNotFoundException();
        }
    }

    public Optional<LtiConfig> findById(Long id) {
        return ltiConfigJpaRepository.findById(id);
    }
}

