package com.schoolaby.repository;

import com.schoolaby.domain.Rubric;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public interface RubricJpaRepository extends JpaRepository<Rubric, Long> {
    Set<Rubric> findAllByIsTemplateTrueAndTitleContainingIgnoreCase(String search);
    Optional<Rubric> findByAssignmentId(Long assignmentId);
}
