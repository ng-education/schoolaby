package com.schoolaby.repository;

import com.querydsl.core.BooleanBuilder;
import com.schoolaby.domain.LtiResource;
import com.schoolaby.repository.exception.EntityNotFoundException;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

import static com.schoolaby.domain.QLtiResource.ltiResource;
import static com.schoolaby.security.SecurityUtils.getCurrentUserId;
import static com.schoolaby.security.SecurityUtils.isCurrentUserTeacherOrAdmin;

@Repository
public class LtiResourceRepository extends QuerydslRepositorySupport {
    private final LtiResourceJpaRepository jpaRepository;

    public LtiResourceRepository(LtiResourceJpaRepository jpaRepository) {
        super(LtiResource.class);
        this.jpaRepository = jpaRepository;
    }

    private BooleanBuilder predicate() {
        BooleanBuilder predicate = new BooleanBuilder();
        if (!isCurrentUserTeacherOrAdmin()) {
            predicate.and(ltiResource.assignment.isNull().or(ltiResource.assignment.milestone.journey.students.any().user.id.eq(getCurrentUserId())))
                .and(ltiResource.milestone.isNull().or(ltiResource.milestone.journey.students.any().user.id.eq(getCurrentUserId())));
        }
        return predicate;
    }

    public List<LtiResource> findAll(Long milestoneId, Long assignmentId) {
        BooleanBuilder predicate = predicate();
        if (milestoneId != null) {
            predicate.and(ltiResource.milestone.id.eq(milestoneId));
        }
        if (assignmentId != null) {
            predicate.and(ltiResource.assignment.id.eq(assignmentId));
        }
        return jpaRepository.findAll(predicate);
    }

    public LtiResource save(LtiResource ltiResource) {
        return jpaRepository.save(ltiResource);
    }

    public boolean existsById(Long id) {
        return jpaRepository.exists(predicate().and(ltiResource.id.eq(id)));
    }

    public void deleteById(Long id) {
        if (existsById(id)) {
            jpaRepository.deleteById(id);
        }
    }

    public Optional<LtiResource> findOne(Long id) {
        return jpaRepository.findOne(predicate().and(ltiResource.id.eq(id)));
    }

    public LtiResource getOne(Long id) {
        return findOne(id).orElseThrow(() -> new EntityNotFoundException(LtiResource.class, id));
    }

    public LtiResource getOneByResourceLinkId(String resourceLinkId) {
        return jpaRepository.findOne(ltiResource.resourceLinkId.eq(resourceLinkId))
            .orElseThrow(() -> new EntityNotFoundException(LtiResource.class, "resourceLinkId", resourceLinkId));
    }

    public List<LtiResource> getByIds(List<Long> ids) {
        BooleanBuilder predicate = predicate();
        predicate.and(ltiResource.id.in(ids));
        return jpaRepository.findAll(predicate);
    }
}
