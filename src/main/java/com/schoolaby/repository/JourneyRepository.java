package com.schoolaby.repository;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.schoolaby.domain.Journey;
import com.schoolaby.security.Role;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static com.schoolaby.domain.QJourney.journey;
import static com.schoolaby.security.SecurityUtils.getCurrentUserId;
import static com.schoolaby.security.SecurityUtils.isCurrentUserInRole;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

@Repository
public class JourneyRepository extends QuerydslRepositorySupport {

    private final JourneyJpaRepository jpaRepository;

    public JourneyRepository(JourneyJpaRepository jpaRepository) {
        super(Journey.class);
        this.jpaRepository = jpaRepository;
    }

    private BooleanBuilder predicate() {
        return predicate(false);
    }

    private BooleanBuilder predicate(boolean template) {
        BooleanExpression isInStudents = journey.students.any().user.id.eq(getCurrentUserId());
        BooleanExpression isInTeachers = journey.teachers.any().user.id.eq(getCurrentUserId());

        BooleanBuilder predicate = new BooleanBuilder();
        if (isCurrentUserInRole(Role.Constants.TEACHER) && !template) {
            predicate.and(isInTeachers
                    .or(isInStudents)); // This is for "Kiida digiõpetajat", where teacher might join the journey in role "teacher". Might also be useful to collaborate on journeys.
        } else if (isCurrentUserInRole(Role.Constants.TEACHER) && template) {
            predicate.and(isInTeachers.or(journey.template.isTrue()));
        } else if (!isCurrentUserInRole(Role.Constants.ADMIN)) {
            predicate.and(isInStudents);
        }
        return predicate;
    }

    public Page<Journey> findAll(Pageable pageable) {
        return jpaRepository.findAll(predicate(), pageable);
    }

    public Page<Journey> findAllByUserAndStateAndAuthority(Pageable pageable, Long userId, String state, String authority) {
        return jpaRepository.findAllJourneysByStateAndAuthorityName(userId, state, authority, pageable);
    }

    public Page<Journey> findAllJourneysByStateForAdmin(Pageable pageable, String state) {
        return jpaRepository.findAllJourneysByStateForAdmin(state, pageable);
    }

    public List<Journey> findAll() {
        return jpaRepository.findAll();
    }

    public List<Journey> findAllOutOfSyncJourneys(Set<Long> excludedIds) {
        BooleanBuilder predicate = new BooleanBuilder()
            .and(journey.endDate.after(journey.lastModifiedDate))
            .and(journey.id.notIn(excludedIds));
        return findAllAndGetStream(predicate).collect(toList());
    }

    public Page<Journey> findUserTemplatesBySearchAndSubject(Pageable pageable, String search, Long subjectId, Long userId) {
        BooleanBuilder predicate = new BooleanBuilder()
            .and(journey.creator.id.eq(userId))
            .and(journey.title.containsIgnoreCase(search));

        if (subjectId != null) {
            predicate.and(journey.subject.id.eq(subjectId));
        } else {
            predicate.and(journey.subject.isNull());
        }

        return jpaRepository.findAll(predicate, pageable);
    }

    public Page<Journey> findAllTemplatesBySearchAndSubjectAndCountry(Pageable pageable, String search, Long subjectId, String country) {
        BooleanBuilder predicate = new BooleanBuilder()
            .and(journey.title.containsIgnoreCase(search))
            .and(journey.creator.country.eq(country))
            .and(journey.template.isTrue()
                .or(journey.creator.id.eq(getCurrentUserId())));

        if (subjectId != null) {
            predicate.and(journey.subject.id.eq(subjectId));
        } else {
            predicate.and(journey.subject.isNull());
        }

        return jpaRepository.findAll(predicate, pageable);
    }

    public Set<Journey> findByCreatorId(Long creatorId) {
        return findAllAndGetStream(predicate().and(journey.creator.id.eq(creatorId))).collect(toSet());
    }

    public Optional<Journey> findOne(@Param("id") Long id, boolean template) {
        return jpaRepository.findOne(predicate(template).and(journey.id.eq(id)));
    }

    public Optional<Journey> findOneByMilestoneId(Long milestoneId) {
        return jpaRepository.findOne(predicate().and(journey.milestones.any().id.eq(milestoneId)));
    }

    public Journey save(Journey journey) {
        return jpaRepository.save(journey);
    }

    public boolean existsById(Long id) {
        return jpaRepository.exists(predicate().and(journey.id.eq(id)));
    }

    public void deleteById(Long id) {
        if (existsById(id)) {
            jpaRepository.deleteById(id);
        } else {
            throw new EntityNotFoundException();
        }
    }

    public Page<Journey> findAllByTemplate(Pageable pageable, String country) {
        BooleanBuilder booleanBuilder = new BooleanBuilder()
            .and(journey.creator.country.eq(country))
            .and(journey.template.isTrue()
                .or(journey.creator.id.eq(getCurrentUserId())));

        return jpaRepository.findAll(booleanBuilder, pageable);
    }

    public Optional<Journey> findFirstBySignUpCode(String signUpCode) {
        return jpaRepository.findOne(new BooleanBuilder(journey.journeySignupCodes.any().signUpCode.contains(signUpCode)));
    }

    public Optional<Journey> findOneByMilestonesId(Long milestoneId) {
        return jpaRepository.findOne(predicate().and(journey.milestones.any().id.eq(milestoneId)));
    }

    public Optional<Journey> findOneByMilestonesAssignmentsId(Long assignmentId) {
        return jpaRepository.findOne(predicate().and(journey.milestones.any().assignments.any().id.eq(assignmentId)));
    }

    // For internal queries only, does not check teachers or students array
    public Optional<Journey> findOne(Long journeyId) {
        return jpaRepository.findOne(journey.id.eq(journeyId));
    }

    @EntityGraph(attributePaths = {"milestones.assignments.submissions.authors"})
    public Optional<Journey> getOneWithSubmissionsById(Long id) {
        return jpaRepository.findOne(predicate().and(journey.id.eq(id)));
    }

    public Stream<Journey> findAllAndGetStream(BooleanBuilder predicate) {
        return StreamSupport
            .stream(jpaRepository
                .findAll(predicate)
                .spliterator(), false);
    }
}
