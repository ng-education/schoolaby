package com.schoolaby.repository;

import com.schoolaby.domain.Assignment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface AssignmentJpaRepository extends JpaRepository<Assignment, Long>, QuerydslPredicateExecutor<Assignment> {
}
