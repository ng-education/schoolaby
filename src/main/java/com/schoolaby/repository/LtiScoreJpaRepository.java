package com.schoolaby.repository;

import com.schoolaby.domain.LtiScore;
import com.schoolaby.repository.common.CollectionsQdslPredicateExecutor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface LtiScoreJpaRepository extends JpaRepository<LtiScore, Long>, QuerydslPredicateExecutor<LtiScore>, CollectionsQdslPredicateExecutor<LtiScore> {
}
