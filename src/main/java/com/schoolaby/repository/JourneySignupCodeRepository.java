package com.schoolaby.repository;

import com.schoolaby.domain.Journey;
import com.schoolaby.domain.JourneySignupCode;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import java.util.Optional;

import static com.schoolaby.domain.QJourneySignupCode.journeySignupCode;

@Repository
public class JourneySignupCodeRepository extends QuerydslRepositorySupport {

    private final JourneySignupCodeJpaRepository jpaRepository;

    public JourneySignupCodeRepository(JourneySignupCodeJpaRepository jpaRepository) {
        super(Journey.class);
        this.jpaRepository = jpaRepository;
    }

    public Optional<JourneySignupCode> findBySignupCode(String signupCode) {
        return jpaRepository.findOne(journeySignupCode.signUpCode.eq(signupCode));
    }

    public JourneySignupCode save(JourneySignupCode journeySignupCode) {
        return jpaRepository.save(journeySignupCode);
    }
}
