package com.schoolaby.repository;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.schoolaby.domain.PersistentAuditEvent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityNotFoundException;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.schoolaby.domain.QPersistentAuditEvent.persistentAuditEvent;
import static org.springframework.boot.actuate.security.AuthenticationAuditListener.AUTHENTICATION_SUCCESS;

@Repository
public class AuditEventRepository extends BaseQuerydslRepository {
    private final PersistenceAuditEventRepository jpaRepository;

    public AuditEventRepository(PersistenceAuditEventRepository jpaRepository, JPAQueryFactory queryFactory) {
        super(PersistentAuditEvent.class, queryFactory);
        this.jpaRepository = jpaRepository;
    }

    private BooleanBuilder predicate() {
        return new BooleanBuilder();
    }

    public Page<PersistentAuditEvent> findLastLoginsByIds(Set<Long> ids, Pageable pageable) {
        BooleanBuilder predicate = predicate().and(persistentAuditEvent.auditEventType.eq(AUTHENTICATION_SUCCESS))
            .and(persistentAuditEvent.id.in(queryFactory.select(persistentAuditEvent.id.max()).from(persistentAuditEvent).where(persistentAuditEvent.userId.in(ids)).groupBy(persistentAuditEvent.userId)));
        return jpaRepository.findAll(predicate, pageable);
    }

    public Page<PersistentAuditEvent> findAll(Pageable pageable) {
        return jpaRepository.findAll(predicate(), pageable);
    }

    public Page<PersistentAuditEvent> findAllByAuditEventDateBetween(Instant fromDate, Instant toDate, Pageable pageable) {
        return jpaRepository.findAll(predicate().and(persistentAuditEvent.auditEventDate.between(fromDate, toDate)), pageable);
    }

    public List<PersistentAuditEvent> findByAuditEventDateBefore(Instant before) {
        return jpaRepository.findAll(predicate().and(persistentAuditEvent.auditEventDate.before(before)));
    }

    public Optional<PersistentAuditEvent> findById(Long id) {
        return jpaRepository.findOne(predicate().and(persistentAuditEvent.id.eq(id)));
    }

    public boolean existsById(Long id) {
        return jpaRepository.exists(predicate().and(persistentAuditEvent.id.eq(id)));
    }

    public void deleteById(Long id) {
        if (existsById(id)) {
            jpaRepository.deleteById(id);
        } else {
            throw new EntityNotFoundException();
        }
    }
}
