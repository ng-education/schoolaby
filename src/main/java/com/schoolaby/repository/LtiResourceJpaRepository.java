package com.schoolaby.repository;

import com.schoolaby.domain.LtiResource;
import com.schoolaby.repository.common.CollectionsQdslPredicateExecutor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LtiResourceJpaRepository extends JpaRepository<LtiResource, Long>, CollectionsQdslPredicateExecutor<LtiResource> {
}
