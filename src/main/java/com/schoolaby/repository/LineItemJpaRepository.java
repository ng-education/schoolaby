package com.schoolaby.repository;

import com.schoolaby.domain.LtiLineItem;
import com.schoolaby.repository.common.CollectionsQdslPredicateExecutor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface LineItemJpaRepository extends JpaRepository<LtiLineItem, Long>, QuerydslPredicateExecutor<LtiLineItem>, CollectionsQdslPredicateExecutor<LtiLineItem> {
}
