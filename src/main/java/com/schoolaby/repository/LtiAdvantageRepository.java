package com.schoolaby.repository;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.jwk.JWKSet;
import io.jsonwebtoken.JwtException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.security.Key;
import java.text.ParseException;

import static java.lang.String.format;

@Service
@Transactional
public class LtiAdvantageRepository {

    public Key getToolPublicKey(String url, String kid) {
        RestTemplate restTemplate = new RestTemplate();
        String jwkString = restTemplate.getForObject(url, String.class);
        try {
            return JWKSet.parse(jwkString).getKeyByKeyId(kid).toRSAKey().toRSAPublicKey();
        } catch (ParseException | JOSEException e) {
            throw new JwtException(format("Unable to parse jwks for url: %s, kid: %s", url, kid), e);
        }
    }
}
