package com.schoolaby.repository;

import com.schoolaby.domain.PersonRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface PersonRoleRepository extends JpaRepository<PersonRole, Long> {
    Set<PersonRole> findAllByIdIn(Set<Long> ids);
}
