const tsconfig = require('../../../tsconfig.json');

module.exports = {
  transform: {
    '^.+\\.tsx?$': 'ts-jest',
  },
  rootDir: '../../../',
  testURL: 'http://localhost/',
  cacheDirectory: '<rootDir>/build/jest-cache',
  coverageDirectory: '<rootDir>/build/test-results/',
  testMatch: ['<rootDir>/src/test/javascript/spec/**/@(*.)@(spec.ts?(x))'],
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
  coveragePathIgnorePatterns: [
    '<rootDir>/src/test/javascript',
    '<rootDir>/src/main/webapp/app/shared/icons',
    '<rootDir>/src/main/webapp/app/config/websocket-middleware.ts',
    '<rootDir>/src/main/webapp/app/shared/layout/webcams/video-webcam.tsx',
    '<rootDir>/src/main/webapp/app/journey/journey-detail/journey-board/milestone/milestone-with-assignments-old.tsx',
    '<rootDir>/src/main/webapp/app/assignment/assessment-rubric/rubric-update',
  ],
  moduleNameMapper: mapTypescriptAliasToJestAlias({
    '\\.(css|scss)$': 'identity-obj-proxy',
  }),
  reporters: [
    'default',
    [
      'jest-junit',
      {
        outputDirectory: './build/test-results/',
        outputName: 'TESTS-results-jest.xml',
      },
    ],
  ],
  testResultsProcessor: 'jest-sonar-reporter',
  testEnvironment: 'jsdom',
  testPathIgnorePatterns: ['<rootDir>/node_modules/', '<rootDir>/testcafe/', '<rootDir>/build/', '<rootDir>/out/'],
  setupFiles: ['<rootDir>/src/test/javascript/spec/storage-mock.ts'],
  setupFilesAfterEnv: ['<rootDir>/src/test/javascript/spec/rtl-setup.ts', '<rootDir>/src/test/javascript/spec/helper/test-utils.tsx'],
  globals: {
    'ts-jest': {
      tsConfig: './tsconfig.test.json',
      diagnostics: false,
    },
  },
  coverageThreshold: {
    global: {
      statements: 81.0,
      branches: 65.3,
      functions: 66.5,
      lines: 82.1,
    },
  },
};

function mapTypescriptAliasToJestAlias(alias = {}) {
  const jestAliases = { ...alias };
  if (!tsconfig.compilerOptions.paths) {
    return jestAliases;
  }
  Object.entries(tsconfig.compilerOptions.paths)
    .filter(([key, value]) => {
      // use Typescript alias in Jest only if this has value
      return !!value.length;
    })
    .map(([key, value]) => {
      // if Typescript alias ends with /* then in Jest:
      // - alias key must end with /(.*)
      // - alias value must end with /$1
      const regexToReplace = /(.*)\/\*$/;
      const aliasKey = key.replace(regexToReplace, '$1/(.*)');
      const aliasValue = value[0].replace(regexToReplace, '$1/$$1');
      return [aliasKey, `<rootDir>/${aliasValue}`];
    })
    .reduce((aliases, [key, value]) => {
      aliases[key] = value;
      return aliases;
    }, jestAliases);
  return jestAliases;
}
