import React, { FC } from 'react';
import { Router, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import { createBrowserHistory } from 'history';
import { Store } from 'redux';

interface IRouteProps {
  path?: string;
  location?: any;
  history?: any;
}

interface IContextProvider {
  store?: Store<any>;
  contextProvider: React.FC;
  props?: any;
  routeProps?: IRouteProps;
}

const ContextProvider: FC<IContextProvider> = ({ children, store, props, routeProps, contextProvider: ContextWrapper }) => {
  const createHistory = createBrowserHistory();

  return (
    <Provider store={store}>
      <Router history={routeProps?.history ? routeProps.history : createHistory}>
        <Route path={routeProps?.path}>
          <ContextWrapper {...props}>{children}</ContextWrapper>
        </Route>
      </Router>
    </Provider>
  );
};

export default ContextProvider;
