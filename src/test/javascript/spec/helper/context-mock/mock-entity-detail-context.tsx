import React, { FC, useEffect } from 'react';
import {
  useAssignmentState,
  useDeadlineTargetState,
  useGradingSchemeState,
  useSelectedUserState,
  useSubmissionsState,
} from 'app/shared/contexts/entity-detail-context';
import { IMockEntityDetailProvider } from './mock-entity-detail-provider';

const MockEntityDetailContext: FC<IMockEntityDetailProvider> = ({
  children,
  entity,
  submissions,
  gradingSchemeEntity,
  selectedUser,
  deadlineTarget,
}) => {
  const { setEntity } = useAssignmentState();
  const { setSubmissions } = useSubmissionsState();
  const { setGradingScheme } = useGradingSchemeState();
  const { setSelectedUser } = useSelectedUserState();
  const { setDeadlineTarget } = useDeadlineTargetState();

  useEffect(() => {
    setEntity(entity);
    setSubmissions(submissions || []);
    setGradingScheme(gradingSchemeEntity);
    setSelectedUser(selectedUser);
    setDeadlineTarget(deadlineTarget);
  }, []);

  return <>{children}</>;
};

export default MockEntityDetailContext;
