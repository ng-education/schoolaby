import React, { FC, useEffect } from 'react';
import {
  useEntityUpdateIdState,
  useParentEducationalAlignmentsState,
  useSelectedEducationalAlignmentsState,
  useSelectedGroupsState,
  useSelectedRubricState,
  useSelectedStudentsState,
} from 'app/shared/contexts/entity-update-context';
import { IMockEntityUpdateProvider } from './mock-entity-update-provider';

const MockEntityUpdateContext: FC<IMockEntityUpdateProvider> = ({
  children,
  submissions,
  selectedRubric,
  selectedStudents,
  selectedGroups,
  milestoneUpdateId,
  assignmentUpdateId,
  selectedEducationalAlignments,
}) => {
  const { setSelectedEducationalAlignments } = useSelectedEducationalAlignmentsState();
  const { setParentEducationalAlignments } = useParentEducationalAlignmentsState();
  const { setSelectedRubric } = useSelectedRubricState();
  const { setSelectedStudents } = useSelectedStudentsState();
  const { setSelectedGroups } = useSelectedGroupsState();
  const { setMilestoneUpdateId } = useEntityUpdateIdState();
  const { setAssignmentUpdateId } = useEntityUpdateIdState();

  useEffect(() => {
    setSelectedEducationalAlignments(selectedEducationalAlignments);
    setParentEducationalAlignments(submissions || []);
    setSelectedRubric(selectedRubric);
    setSelectedStudents(selectedStudents);
    setSelectedGroups(selectedGroups || []);
    setMilestoneUpdateId(milestoneUpdateId);
    setAssignmentUpdateId(assignmentUpdateId);
  }, []);

  return <>{children}</>;
};

export default MockEntityUpdateContext;
