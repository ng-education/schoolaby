import React, { FC } from 'react';
import { ISubmission } from 'app/shared/model/submission/submission.model';
import MockEntityUpdateContext from './mock-entity-update-context';
import { EntityUpdateProvider } from 'app/shared/contexts/entity-update-context';
import { IRubric } from 'app/shared/model/rubric/rubric.model';
import { IPerson } from 'app/shared/model/person.model';
import { IGroup } from 'app/shared/model/group.model';
import { IEducationalAlignment } from 'app/shared/model/educational-alignment.model';

export interface IMockEntityUpdateProvider {
  entity?: any;
  grade?: string;
  submissions?: ISubmission[];
  selectedRubric?: IRubric;
  selectedStudents?: IPerson[];
  selectedGroups?: IGroup[];
  milestoneUpdateId?: number;
  assignmentUpdateId?: number;
  selectedEducationalAlignments?: IEducationalAlignment[];
}

const MockEntityUpdateProvider: FC<IMockEntityUpdateProvider> = ({
  children,
  entity,
  grade,
  submissions,
  selectedRubric,
  selectedStudents,
  selectedGroups,
  milestoneUpdateId,
  selectedEducationalAlignments,
  assignmentUpdateId,
}) => {
  return (
    <>
      <EntityUpdateProvider>
        <MockEntityUpdateContext
          entity={entity}
          grade={grade}
          submissions={submissions}
          selectedRubric={selectedRubric}
          selectedStudents={selectedStudents}
          selectedGroups={selectedGroups}
          milestoneUpdateId={milestoneUpdateId}
          assignmentUpdateId={assignmentUpdateId}
          selectedEducationalAlignments={selectedEducationalAlignments}
        >
          {children}
        </MockEntityUpdateContext>
      </EntityUpdateProvider>
    </>
  );
};

export default MockEntityUpdateProvider;
