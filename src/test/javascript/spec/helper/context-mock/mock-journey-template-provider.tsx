import React, { FC } from 'react';
import ISubject from 'app/shared/model/subject.model';
import { JourneyTemplateProvider } from 'app/shared/contexts/journey-template-context';
import MockJourneyTemplateContext from './mock-journey-template-context';

export interface IMockJourneyTemplateProvider {
  templateSubject?: ISubject;
  showTemplatesView?: boolean;
  selectedTemplateId?: number;
}

const MockJourneyTemplateProvider: FC<IMockJourneyTemplateProvider> = ({
  children,
  templateSubject,
  showTemplatesView,
  selectedTemplateId,
}) => {
  return (
    <>
      <JourneyTemplateProvider>
        <MockJourneyTemplateContext
          templateSubject={templateSubject}
          showTemplatesView={showTemplatesView}
          selectedTemplateId={selectedTemplateId}
        >
          {children}
        </MockJourneyTemplateContext>
      </JourneyTemplateProvider>
    </>
  );
};

export default MockJourneyTemplateProvider;
