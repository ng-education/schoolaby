import React, { FC } from 'react';
import { ISubmission } from 'app/shared/model/submission/submission.model';
import { ILtiResource } from 'app/shared/model/lti-resource.model';
import { IUploadedFile } from 'app/shared/model/uploaded-file.model';
import { IMessage } from 'app/shared/model/message.model';
import { SubmissionGradingProvider } from 'app/shared/contexts/submission-grading-context';
import MockSubmissionGradingContext from './mock-submission-grading-context';
import { ISelectedCritreionLevel } from 'app/shared/model/rubric/selected-critreion-level.model';
import { ISubmissionFeedback } from 'app/shared/model/submission/submission-feedback.model';

export interface IMockSubmissionGradingProvider {
  ltiResources?: ILtiResource[];
  commentFromEditor?: string;
  submissionFeedback?: IMessage;
  modifiedSubmission?: ISubmission;
  feedbackFiles?: IUploadedFile[];
  resubmittable?: boolean;
  submittedSubmissions?: ISubmission[];
  grade?: string;
  selectedCriterionLevels?: ISelectedCritreionLevel[];
  selectedFeedback?: ISubmissionFeedback;
}

const MockSubmissionGradingProvider: FC<IMockSubmissionGradingProvider> = ({
  children,
  ltiResources,
  commentFromEditor,
  submissionFeedback,
  modifiedSubmission,
  feedbackFiles,
  resubmittable,
  submittedSubmissions,
  grade,
  selectedCriterionLevels,
  selectedFeedback,
}) => {
  return (
    <>
      <SubmissionGradingProvider>
        <MockSubmissionGradingContext
          ltiResources={ltiResources}
          commentFromEditor={commentFromEditor}
          submissionFeedback={submissionFeedback}
          modifiedSubmission={modifiedSubmission}
          feedbackFiles={feedbackFiles}
          resubmittable={resubmittable}
          submittedSubmissions={submittedSubmissions}
          grade={grade}
          selectedCriterionLevels={selectedCriterionLevels}
          selectedFeedback={selectedFeedback}
        >
          {children}
        </MockSubmissionGradingContext>
      </SubmissionGradingProvider>
    </>
  );
};

export default MockSubmissionGradingProvider;
