import React, { FC, useEffect } from 'react';
import { IMockSubmissionGradingProvider } from './mock-submission-grading-provider';
import {
  useCommentFromEditorState,
  useFeedbackFilesState,
  useGradeState,
  useLtiResourcesState,
  useModifiedSubmissionState,
  useResubmittableState,
  useSelectedCriterionLevelsState,
  useSelectedFeedbackState,
  useSubmissionFeedbackState,
  useSubmittedSubmissionsState,
} from 'app/shared/contexts/submission-grading-context';

const MockSubmissionGradingContext: FC<IMockSubmissionGradingProvider> = ({
  children,
  ltiResources,
  commentFromEditor,
  submissionFeedback,
  modifiedSubmission,
  feedbackFiles,
  resubmittable,
  submittedSubmissions,
  grade,
  selectedCriterionLevels,
  selectedFeedback,
}) => {
  const { setLtiResources } = useLtiResourcesState();
  const { setCommentFromEditor } = useCommentFromEditorState();
  const { setSubmissionFeedback } = useSubmissionFeedbackState();
  const { setModifiedSubmission } = useModifiedSubmissionState();
  const { setFeedbackFiles } = useFeedbackFilesState();
  const { setIsResubmittable } = useResubmittableState();
  const { setSubmittedSubmissions } = useSubmittedSubmissionsState();
  const { setGrade } = useGradeState();
  const { setSelectedCriterionLevels } = useSelectedCriterionLevelsState();
  const { setSelectedFeedback } = useSelectedFeedbackState();

  useEffect(() => {
    setLtiResources(ltiResources);
    setCommentFromEditor(commentFromEditor);
    setSubmissionFeedback(submissionFeedback);
    setModifiedSubmission(modifiedSubmission);
    setFeedbackFiles(feedbackFiles);
    setIsResubmittable(resubmittable);
    setSubmittedSubmissions(submittedSubmissions);
    setGrade(grade);
    setSelectedCriterionLevels(selectedCriterionLevels);
    setSelectedFeedback(selectedFeedback);
  }, []);

  return <>{children}</>;
};
export default MockSubmissionGradingContext;
