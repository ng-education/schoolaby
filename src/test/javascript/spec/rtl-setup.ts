import '@testing-library/jest-dom';
import { setupServer } from 'msw/node';
import { rest } from 'msw';
import { ILtiResourceParameters } from 'app/shared/layout/lti/lti-modal';
import { ILtiResource } from 'app/shared/model/lti-resource.model';
import { AUTHORITIES, EKOOLIKOTT, OPIQ } from 'app/config/constants';
import { JOURNEY, SUBMISSION } from 'app/shared/util/entity-utils';
import { ILtiScore } from 'app/shared/model/lti-score.model';
import { createRubric } from 'app/shared/util/rubric';
import { DateTime } from 'luxon';
import { EntityState } from 'app/shared/model/enumerations/entity-state';

// TODO: Create separate handler files for each domain

export const JOIN_LIVE_HREF = 'www.joinlive.ee/math';

const scoreGradable: ILtiScore = {
  title: 'LTI Test title (launch)',
  resourceId: 2,
  score: '0.5',
  scoreMax: null,
  createdDate: '2020-11-05T12:44:11.869627Z',
  launches: ['2020-11-05T12:44:11.869627Z'],
};

/* eslint-disable @typescript-eslint/camelcase */
const ltiParameters: ILtiResourceParameters = {
  lti_version: 'test-version',
  launch_url: 'test-url',
  oauth_nonce: 'test-none',
  oauth_signature: 'test-signature',
  user_id: '1',
  lti_message_type: 'lti_launch',
  roles: 'test-roles',
  oauth_consumer_key: 'test-conusmer-key',
  resource_link_id: 'test-resource-link-id',
  oauth_timestamp: '15800324320',
  oauth_signature_method: 'HMAC-SHA1',
  oauth_version: '1.0',
};

const ltiResource: ILtiResource = {
  id: 2,
  description: 'LTI Test description',
  resourceLinkId: '1',
  assignmentId: 1234,
  title: 'LTI Test title',
  ltiApp: {
    imageUrl: 'http://0.0.0.0/img',
    id: 1,
    launchUrl: 'http://0.0.0.0/launch',
    version: 'v1p0',
    name: 'Test App',
    description: 'Test Description',
    clientId: null,
  },
};

const journey = {
  creatorId: 6,
  videoConferenceUrl: JOIN_LIVE_HREF,
  description: 'Matemaatika 8. klassile',
  title: 'Journey title',
  owner: true,
  educationalLevel: {
    id: 1,
    name: 'I kooliaste',
  },
  schools: [
    {
      id: 3,
      name: 'Tallinna Kadrioru Saksa Gümnaasium',
    },
  ],
  endDate: '2021-03-26T09:04:11.448932Z',
  id: 1,
  milestones: [
    {
      id: 1,
      title: 'Milestone title',
      journeyId: 1,
      creatorId: 6,
      assignments: [
        {
          id: 1234,
          gradingSchemeId: 1,
          state: EntityState.NOT_STARTED,
        },
      ],
    },
  ],
  teachers: [
    {
      user: {
        authorities: ['ROLE_TEACHER'],
        email: 'student@localhost.com',
        firstName: 'teacher',
        id: 7,
        lastName: 'teacher',
      },
    },
    {
      user: {
        authorities: ['ROLE_TEACHER'],
        email: 'coTeacher@localhost.com',
        firstName: 'Co',
        id: 8,
        lastName: 'Teacher',
      },
      joinedDate: '2022-04-29T13:33:36.259260Z',
    },
  ],
  signUpCode: '12341234',
  startDate: '2020-10-15T09:04:11.448932Z',
  nationalCurriculum: true,
  curriculum: {
    title: 'NATIONAL_SECONDARY_EDUCATION',
    requiresSubject: true,
  },
};

const handlers = [
  rest.get('/api/assignments/1', (req, res, ctx) => {
    return res(
      ctx.json({
        id: '1',
        title: 'Previous assignment',
        milestoneId: 1,
        journeyId: 1,
        state: EntityState.COMPLETED,
      })
    );
  }),
  rest.get('/api/assignments/2', (req, res, ctx) => {
    return res(
      ctx.json({
        id: '2',
        title: 'Next assignment',
        milestoneId: 1,
        journeyId: 1,
        state: EntityState.COMPLETED,
      })
    );
  }),
  rest.get('/api/assignments/1234', (req, res, ctx) => {
    return res(
      ctx.json({
        id: 1234,
        title: 'Assignment title',
        description: 'Description text',
        gradingSchemeId: 10,
        hasGrades: false,
        deadline: DateTime.fromISO('2020-09-07T09:24:00', { zone: 'local' }),
        materials: [
          {
            id: 1,
            title: 'Test material',
          },
        ],
        milestoneId: 1,
        journeyId: 1,
        state: EntityState.COMPLETED,
        previousAssignmentId: 1,
        nextAssignmentId: 2,
        students: [
          {
            authorities: ['ROLE_STUDENT'],
            email: 'student@localhost.com',
            firstName: 'student1',
            login: 'student1',
            id: 5,
            lastName: 'student1',
          },
        ],
        educationalAlignments: [
          {
            id: 529,
            title: 'Teadustekst (Eesti keel)',
            alignmentType: 'subject',
            educationalFramework: 'Estonian National Curriculum',
            targetName: 'Teadustekst (Eesti keel)',
            targetUrl: null,
            startDate: '2020-07-01T05:00:12Z',
            endDate: null,
            taxonId: 60021,
            children: [],
            subject: {
              id: 1,
              label: 'schoolabyApp.subject.estonian',
            },
          },
        ],
      })
    );
  }),
  rest.get('/api/assignments/12345', (req, res, ctx) => {
    return res(
      ctx.json({
        id: 12345,
        milestoneId: 1,
        title: 'Assignment title',
        hasGrades: true,
        students: [],
        journeyId: 1,
        materials: [
          {
            id: 1,
            title: 'Test material',
          },
        ],
        state: EntityState.COMPLETED,
        educationalAlignments: [
          {
            id: 529,
            title: 'Teadustekst (Eesti keel)',
            alignmentType: 'subject',
            educationalFramework: 'Estonian National Curriculum',
            targetName: 'Teadustekst (Eesti keel)',
            targetUrl: null,
            startDate: '2020-07-01T05:00:12Z',
            endDate: null,
            taxonId: 60021,
            children: [],
            subject: {
              id: 1,
              label: 'schoolabyApp.subject.estonian',
            },
          },
        ],
      })
    );
  }),
  rest.get('/api/assignments/123456', (req, res, ctx) => {
    return res(
      ctx.json({
        id: 123456,
        milestoneId: 1,
        title: 'Assignment title',
        hasGrades: true,
        students: [],
        groups: [
          {
            id: 1,
            name: 'Group 1',
            students: [
              {
                authorities: ['ROLE_STUDENT'],
                email: 'student@localhost.com',
                firstName: 'student1',
                id: 5,
                lastName: 'student1',
              },
              {
                authorities: ['ROLE_STUDENT'],
                email: 'student@localhost.com',
                firstName: 'student2',
                id: 6,
                lastName: 'student2',
              },
            ],
            submissions: [],
          },
        ],
        educationalAlignments: [
          {
            id: 529,
            title: 'Teadustekst (Eesti keel)',
            alignmentType: 'subject',
            educationalFramework: 'Estonian National Curriculum',
            targetName: 'Teadustekst (Eesti keel)',
            targetUrl: null,
            startDate: '2020-07-01T05:00:12Z',
            endDate: null,
            taxonId: 60021,
            children: [],
            subject: {
              id: 1,
              label: 'schoolabyApp.subject.estonian',
            },
          },
        ],
      })
    );
  }),
  rest.get('/api/assignments?', (req, res, ctx) => {
    if (req.url.searchParams.get('states').includes('OVERDUE')) {
      return res(
        ctx.json([
          {
            title: 'Overdue assignment',
            deadline: '2021-05-09T13:17:49.929545Z',
            students: [
              {
                id: 1,
              },
              {
                id: 2,
              },
            ],
            submissionsCount: 1,
          },
        ])
      );
    } else {
      return res(
        ctx.json([
          {
            title: 'Active assignment',
            deadline: '2021-05-09T13:17:49.929545Z',
            students: [
              {
                id: 1,
              },
              {
                id: 2,
              },
            ],
            submissionsCount: 1,
          },
        ])
      );
    }
  }),

  rest.get('/api/uploaded-files?submissionId=6', (req, res, ctx) => {
    return res(ctx.json([]));
  }),

  rest.get('/api/grading-schemes/10', (req, res, ctx) => {
    return res(
      ctx.json({
        id: 10,
        name: 'Alphabetical (A-F)',
        code: 'ALPHABETICAL_A_F',
      })
    );
  }),

  rest.get('/api/grading-schemes/7', (req, res, ctx) => {
    return res(
      ctx.json({
        id: 7,
        name: 'Numerical 1-12',
        code: 'NUMERICAL_1_12',
        sequenceNumber: 2,
      })
    );
  }),

  rest.get('/api/grading-schemes', (req, res, ctx) => {
    return res(
      ctx.json([
        {
          id: 1,
          code: 'NUMERICAL_1_5',
          values: [
            {
              id: 5,
              grade: '1',
              percentageRangeStart: 0,
              percentageRangeEnd: 20,
              gradingSchemeId: 1,
            },
            {
              id: 4,
              grade: '2',
              percentageRangeStart: 20,
              percentageRangeEnd: 50,
              gradingSchemeId: 1,
            },
            {
              id: 3,
              grade: '3',
              percentageRangeStart: 50,
              percentageRangeEnd: 75,
              gradingSchemeId: 1,
            },
            {
              id: 2,
              grade: '4',
              percentageRangeStart: 75,
              percentageRangeEnd: 90,
              gradingSchemeId: 1,
            },
            {
              id: 1,
              grade: '5',
              percentageRangeStart: 90,
              percentageRangeEnd: 100,
              gradingSchemeId: 1,
            },
          ],
        },
        {
          id: 2,
          code: 'NONE',
          values: [],
        },
      ])
    );
  }),

  rest.get('/api/submissions', (req, res, ctx) => {
    const assignmentId = req.url.searchParams.get('assignmentId');
    const journeyId = req.url.searchParams.get('journeyId');

    if (assignmentId === '1234' || journeyId === '1') {
      return res(
        ctx.json([
          {
            id: 6,
            value: 'value',
            gradeResult: 'Teacher feedback',
            status: 'COMPLETED',
            submittedForGradingDate: '2022-02-03T13:00:57.801Z',
            resubmittable: true,
            submissionFeedbacks: [
              {
                id: 1,
                grade: 'A',
                value: 'Feedback value',
                studentId: 5,
                feedbackDate: '2022-02-04T14:00:57.801Z',
                feedbackFiles: [
                  {
                    id: 1,
                    uniqueName: 'Feedback file 1',
                    originalName: 'Feedback file 1',
                  },
                ],
              },
              {
                id: 2,
                grade: 'B',
                value: 'Feedback value 2',
                studentId: 5,
                feedbackDate: '2022-02-05T15:00:57.801Z',
                feedbackFiles: [
                  {
                    id: 2,
                    uniqueName: 'Feedback file 2',
                    originalName: 'Feedback file 2',
                  },
                ],
              },
            ],
            authors: [
              {
                id: 5,
                login: 'student',
                firstName: 'student',
                lastName: 'student',
                email: 'student@localhost.com',
                personRoles: [
                  {
                    id: 2,
                    role: 'STUDENT',
                    active: true,
                    startDate: '2020-06-30T10:52:03',
                    endDate: null,
                    grade: '11',
                    parallel: 'B',
                    person: {
                      id: 5,
                      login: 'student',
                      firstName: 'student',
                      lastName: 'student',
                      email: 'student@localhost.com',
                    },
                    schoolId: -1,
                  },
                ],
                authorities: ['ROLE_STUDENT'],
              },
            ],
            uploadedFiles: [
              {
                id: 1,
                originalName: 'test.png',
                uniqueName: 'test.png',
                extension: 'PNG',
              },
              {
                id: 2,
                originalName: 'test.doc',
                uniqueName: 'test.doc',
                extension: 'DOC',
              },
              {
                id: 3,
                originalName: 'test.xls',
                uniqueName: 'test.xls',
                extension: 'XLS',
              },
              {
                id: 4,
                originalName: 'test.pdf',
                uniqueName: 'test.pdf',
                extension: 'PDF',
              },
              {
                id: 5,
                originalName: 'test.mp4',
                uniqueName: 'test.mp4',
                extension: 'MP4',
              },
              {
                id: 6,
                originalName: 'generic.sh',
                uniqueName: 'generic.sh',
                extension: 'SH',
              },
            ],
            assignmentId: 1234,
          },
        ])
      );
    } else if (assignmentId === '12345') {
      return res(
        ctx.json([
          {
            id: 6,
            value: 'value',
            gradeResult: 'Teacher feedback',
            status: 'COMPLETED',
            submittedForGradingDate: '2022-02-03T13:00:57.801Z',
            authors: [
              {
                id: 5,
                login: 'student',
                firstName: 'student',
                lastName: 'student',
                email: 'student@localhost.com',
                personRoles: [
                  {
                    id: 2,
                    role: 'STUDENT',
                    active: true,
                    startDate: '2020-06-30T10:52:03',
                    endDate: null,
                    grade: '11',
                    parallel: 'B',
                    person: {
                      id: 5,
                      login: 'student',
                      firstName: 'student',
                      lastName: 'student',
                      email: 'student@localhost.com',
                    },
                    schoolId: -1,
                  },
                ],
                authorities: ['ROLE_STUDENT'],
              },
            ],
            assignmentId: 12345,
          },
        ])
      );
    } else {
      return res(ctx.json([]));
    }
  }),

  rest.get('/api/submissions/6', (req, res, ctx) => {
    return res(
      ctx.json({
        id: 6,
        value: 'value',
        gradeResult: 'Teacher feedback',
        status: 'COMPLETED',
        submittedForGradingDate: '2022-02-03T13:00:57.801Z',
        authors: [
          {
            id: 5,
            login: 'student',
            firstName: 'student',
            lastName: 'student',
            email: 'student@localhost.com',
            personRoles: [
              {
                id: 2,
                role: 'STUDENT',
                active: true,
                startDate: '2020-06-30T10:52:03',
                endDate: null,
                grade: '11',
                parallel: 'B',
                person: {
                  id: 5,
                  login: 'student',
                  firstName: 'student',
                  lastName: 'student',
                  email: 'student@localhost.com',
                },
                schoolId: -1,
              },
            ],
            authorities: ['ROLE_STUDENT'],
          },
        ],
        assignmentId: 1234,
        assignmentTitle: 'Assignment title',
      })
    );
  }),

  rest.get(`/api/lti/resource?entityType=ASSIGNMENT&entityId=1234`, (req, res, ctx) => {
    const assignmentId = req.url.searchParams.get('assignmentId');

    if (assignmentId === '12345') {
      return res(ctx.json([]));
    }

    return res(ctx.json([ltiResource]));
  }),
  rest.get(`/api/lti/resource/launch/parameters?ltiResourceId=${ltiResource.id}`, (req, res, ctx) => {
    return res(ctx.json(ltiParameters));
  }),
  rest.get(`/api/lti-scores?userId=${ltiParameters.user_id}&assignmentId=${ltiResource.assignmentId}`, (req, res, ctx) => {
    return res(ctx.json([scoreGradable]));
  }),
  rest.get(`/api/messages?submissionId=1`, (req, res, ctx) => {
    return res(
      ctx.json([
        {
          value: 'message value',
          recipientId: 1020,
          creatorId: 123,
        },
      ])
    );
  }),
  rest.get(`/api/messages/views`, (req, res, ctx) => {
    return res(
      ctx.json([
        {
          messageValue: 'message value',
          messageCreatedDate: '2020-06-14T08:41:46.000Z',
          authorName: 'author',
          journeyName: 'journey title',
          educationalAlignmentTargetNames: ['matemaatika'],
          seen: null,
        },
      ])
    );
  }),
  rest.get('/api/educational-levels', (req, res, ctx) => {
    return res(
      ctx.json([
        {
          id: 1,
          name: 'I kooliaste',
        },
        {
          id: 2,
          name: 'II kooliaste',
        },
        {
          id: 3,
          name: 'III kooliaste',
        },
        {
          id: 4,
          name: 'Gümnaasium',
        },
        {
          id: 5,
          name: 'Täiendkoolitus',
        },
      ])
    );
  }),
  rest.get('/api/uploaded-files?submissionId=6', (req, res, ctx) => {
    return res(ctx.json([]));
  }),
  rest.get('https://noembed.com/embed', (req, res, ctx) => {
    return res(ctx.json({ error: 'no matching providers found', url: 'https://www.google.com' }));
  }),
  rest.get('/api/suggestions/rest/v2/learningObject?id=1102', (req, res, ctx) => {
    return res(
      ctx.json({
        type: '.Material',
        id: 1102,
        added: '2020-06-14T08:41:46.000Z',
        titles: [
          {
            language: 'est',
            text: 'title text',
          },
        ],
        descriptions: [
          {
            language: 'est',
            text: '<div>Teoreetiline materjal õpilastele jagamiseks.</div>',
          },
        ],
        source: 'https://e-koolikott.ee/rest/uploadedFile/4683/Võrdeline+seos.pdf',
        titlesForUrl: [
          {
            language: 'est',
            text: 'Vordeline-seos',
          },
        ],
      })
    );
  }),
  rest.get('/api/suggestions/rest/v2/learningObject?id=20880', (req, res, ctx) => {
    return res(
      ctx.json({
        type: '.Material',
        id: 20880,
        added: '2020-06-14T08:41:46.000Z',
        titles: [
          {
            language: 'est',
            text: 'title text',
          },
        ],
        descriptions: [
          {
            language: 'est',
            text: '<div>Massiühikud. Massiühikute teisendamine 4. klass</div>',
          },
        ],
        source: 'https://eesti.ee',
        titlesForUrl: [
          {
            language: 'est',
            text: 'Massiühikud. Massiühikute teisendamine 4. klass',
          },
        ],
      })
    );
  }),
  rest.get('/api/uploaded-files?submissionId=6', (req, res, ctx) => {
    return res(ctx.json([]));
  }),
  rest.get('/api/chats?journeyId=1&entity=SUBMISSION', (req, res, ctx) => {
    const searchType = req.url.searchParams.get('entity');
    if (searchType === SUBMISSION) {
      return res(
        ctx.json([
          {
            id: 1,
            journeyId: 1,
            submissionId: 6,
            title: 'assignment title',
            people: [
              {
                firstName: 'firstName',
                lastName: 'lastName',
                id: 123,
                authorities: [AUTHORITIES.STUDENT],
              },
              {
                firstName: 'firstName2',
                lastName: 'lastName2',
                id: 1234,
                authorities: [AUTHORITIES.TEACHER],
              },
            ],
          },
        ]),
        ctx.set(
          'link',
          '<http://testUrl/api/chats?journeyId=1&entity=SUBMISSION&sort=messages.createdDate%2Cdesc&page=1&size=20>; rel="next",' +
            '<http://testUrl/api/chats?journeyId=1&entity=SUBMISSION&sort=messages.createdDate%2Cdesc&page=1&size=20>; rel="last",' +
            '<http://testUrl/api/chats?journeyId=1&entity=SUBMISSION&sort=messages.createdDate%2Cdesc&page=0&size=20>; rel="first"'
        )
      );
    }
  }),
  rest.get('/api/chats?journeyId=1&entity=JOURNEY', (req, res, ctx) => {
    const searchType = req.url.searchParams.get('entity');
    if (searchType === JOURNEY) {
      return res(
        ctx.json([
          {
            id: 1,
            title: 'assignment title',
            journeyId: 1,
            people: [
              {
                firstName: 'firstName',
                lastName: 'lastName',
                id: 123,
                authorities: [AUTHORITIES.STUDENT],
              },
              {
                firstName: 'firstName2',
                lastName: 'lastName2',
                id: 1234,
                authorities: [AUTHORITIES.TEACHER],
              },
            ],
          },
        ])
      );
    }
  }),
  rest.get('/api/journeys', (req, res, ctx) => {
    const template: string = req.url.searchParams.get('template');
    const userId: string = req.url.searchParams.get('userId');
    const apiVersion = req.headers.get('JOURNEY-JSON-VERSION');

    if (template === 'true' && !!userId && apiVersion === '2') {
      return res(
        ctx.json([
          {
            id: 1,
            title: 'My journey',
            teacherName: 'Timo Tammepakk',
            yearsCount: 0,
            monthsCount: '2',
            daysCount: '21',
            assignmentsCount: 96,
            educationalLevel: {
              name: 'Gümnaasium',
            },
          },
          {
            id: 2,
            title: 'My second journey',
            teacherName: 'Timo Tammepakk',
            yearsCount: 0,
            monthsCount: '2',
            daysCount: '21',
            assignmentsCount: 96,
            educationalLevel: {
              name: 'Gümnaasium',
            },
          },
        ]),
        ctx.set(
          'link',
          '<http://testUrl/api/journeys/?state=&page=1&size=20>; rel="last",' +
            '<http://testUrl/api/journeys/?state=&page=1&size=20>; rel="next",' +
            '<http://testUrl/api/journeys/?state=&page=0&size=20>; rel="first"'
        )
      );
    } else if (template === 'true' && !userId && apiVersion === '2') {
      return res(
        ctx.json([
          {
            id: 1,
            title: 'My journey',
            teacherName: 'Timo Tammepakk',
            yearsCount: 0,
            monthsCount: '2',
            daysCount: '21',
            assignmentsCount: 96,
            educationalLevel: {
              name: 'Gümnaasium',
            },
          },
          {
            id: 2,
            title: 'My second journey',
            teacherName: 'Timo Tammepakk',
            yearsCount: 0,
            monthsCount: '2',
            daysCount: '21',
            assignmentsCount: 96,
            educationalLevel: {
              name: 'Gümnaasium',
            },
          },
          {
            id: 3,
            title: 'Other teacher journey',
            teacherName: 'Timo Tammepakk',
            yearsCount: 0,
            monthsCount: '2',
            daysCount: '21',
            assignmentsCount: 96,
            educationalLevel: {
              name: 'Gümnaasium',
            },
          },
          {
            id: 4,
            title: 'Other teacher second journey',
            teacherName: 'Timo Tammepakk',
            yearsCount: 0,
            monthsCount: '2',
            daysCount: '21',
            assignmentsCount: 96,
            educationalLevel: {
              name: 'Gümnaasium',
            },
          },
        ]),
        ctx.set(
          'link',
          '<http://testUrl/api/journeys/?state=&page=1&size=20>; rel="last",' +
            '<http://testUrl/api/journeys/?state=&page=1&size=20>; rel="next",' +
            '<http://testUrl/api/journeys/?state=&page=0&size=20>; rel="first"'
        )
      );
    } else if (template === 'false' && !userId) {
      return res(
        ctx.json([]),
        ctx.set(
          'link',
          '<http://testUrl/api/journeys/?state=&page=1&size=20>; rel="last",' +
            '<http://testUrl/api/journeys/?state=&page=1&size=20>; rel="next",' +
            '<http://testUrl/api/journeys/?state=&page=0&size=20>; rel="first"'
        )
      );
    }

    return res(
      ctx.json([
        {
          creatorId: 6,
          description: 'Matemaatika 8. klassile',
          title: 'Journey title',
          teacherName: 'teacher teacher',
          progress: 50,
          educationalLevel: [
            {
              id: 1,
              name: 'I kooliaste',
            },
          ],
          endDate: '2021-03-26T09:04:11.448932Z',
          id: 1,
          subject: { id: 2, label: 'schoolabyApp.subject.russian' },
          teachers: [
            {
              user: {
                authorities: ['ROLE_TEACHER'],
                email: 'student@localhost.com',
                firstName: 'teacher',
                id: 7,
                lastName: 'teacher',
              },
            },
          ],
          school: {
            name: 'Test school',
          },
          signUpCode: '12341234',
          startDate: '2020-10-15T09:04:11.448932Z',
        },
      ]),
      ctx.set(
        'link',
        '<http://testUrl/api/journeys/?state=&page=1&size=20>; rel="last",' +
          '<http://testUrl/api/journeys/?state=&page=1&size=20>; rel="next",' +
          '<http://testUrl/api/journeys/?state=&page=0&size=20>; rel="first"'
      )
    );
  }),
  rest.get('/api/journeys/1', (req, res, ctx) => {
    return res(ctx.json({ ...journey }));
  }),
  rest.get('/api/journeys/2', (req, res, ctx) => {
    return res(ctx.json({ ...journey, owner: false, videoConferenceUrl: null, schools: [], students: [] }));
  }),
  rest.get('/api/journeys/3', (req, res, ctx) => {
    return res(ctx.json({ ...journey, teachers: [] }));
  }),
  rest.put('/api/journeys', (req, res, ctx) => {
    return res(
      ctx.json({
        id: 1,
      })
    );
  }),
  rest.get('/api/journeys/assignment/1234', (req, res, ctx) => {
    return res(ctx.json([]));
  }),
  rest.get('/api/messages/submission-feedback/6', (req, res, ctx) => {
    return res(
      ctx.json({
        value: '',
        creatorId: 1,
        chatId: 1,
      })
    );
  }),
  rest.get('/api/submission-feedback', (req, res, ctx) => {
    return res(
      ctx.json([
        {
          id: 2,
          grade: 'A',
          value: 'Feedback value',
          studentId: 5,
          submissionId: 6,
          feedbackDate: '2022-02-05T15:00:57.801Z',
          uploadedFiles: [
            {
              id: 2,
              uniqueName: 'Feedback file 2',
              originalName: 'Feedback file 2',
            },
          ],
          selectedCriterionLevels: [
            {
              id: 1,
              criterionLevel: {
                id: 1,
                title: 'Level 1',
                description: 'Level description',
                sequenceNumber: 0,
                points: 5,
                criterionId: 1,
              },
            },
            {
              id: 2,
              criterionLevel: {
                id: 4,
                title: 'Level 2',
                description: 'Level description',
                sequenceNumber: 1,
                points: 1,
                criterionId: 2,
              },
            },
          ],
        },
        {
          id: 1,
          grade: 'B',
          value: 'Feedback value 2',
          studentId: 5,
          submissionId: 6,
          feedbackDate: '2022-02-04T14:00:57.801Z',
          uploadedFiles: [
            {
              id: 1,
              uniqueName: 'Feedback file 1',
              originalName: 'Feedback file 1',
            },
          ],
        },
      ])
    );
  }),
  rest.get('/api/lti/config', (req, res, ctx) => {
    return res(
      ctx.json([
        {
          type: 'PLATFORM',
          consumerKey: null,
          sharedSecret: null,
          ltiApp: {
            id: 1,
            name: 'Test app',
            description: 'Test description',
            launchUrl: 'http://test.url',
            imageUrl: 'http://test.url',
            version: 'version',
          },
        },
        {
          type: 'JOURNEY',
          consumerKey: 'key',
          sharedSecret: 'secret',
          ltiApp: {
            id: 2,
            name: 'Test app',
            description: 'Test description',
            launchUrl: 'http://test.url',
            imageUrl: 'http://test.url',
            version: 'version',
          },
        },
      ])
    );
  }),
  rest.get('/api/lti/app', (req, res, ctx) => {
    return res(
      ctx.json([
        {
          id: 1,
          name: 'Test App Name',
          description: 'Test App Description',
          imageUrl: '/test.png',
          version: 'LTI-1p0',
          launchUrl: 'https://localhost/lti',
          clientId: null,
          configs: [],
        },
      ])
    );
  }),
  rest.get('/api/milestones/1', (req, res, ctx) => {
    return res(
      ctx.json({
        id: 1,
        title: 'Milestone title',
        materials: [],
        journeyId: 1,
        educationalAlignments: [
          {
            id: 529,
            title: 'Teadustekst (Eesti keel)',
            alignmentType: 'subject',
            educationalFramework: 'Estonian National Curriculum',
            targetName: 'Teadustekst (Eesti keel)',
            targetUrl: null,
            startDate: '2020-07-01T05:00:12Z',
            endDate: null,
            taxonId: 60021,
            children: [],
            subject: {
              id: 1,
              label: 'schoolabyApp.subject.estonian',
            },
          },
        ],
      })
    );
  }),
  rest.get('/api/files/unique_name_1', (req, res, ctx) => {
    return res();
  }),
  rest.get('/api/milestones?journeyId=1', (req, res, ctx) => {
    return res(
      ctx.json([
        {
          id: 1,
          title: 'Milestone title',
          description: 'Milestone description',
          journeyId: 1,
          creatorId: 6,
          assignments: [
            {
              id: 1234,
              title: 'Assignment title',
              state: EntityState.NOT_STARTED,
            },
          ],
        },
      ])
    );
  }),
  rest.get('/api/journeys/1/students', (req, res, ctx) => {
    return res(
      ctx.json([
        {
          user: {
            id: 5,
            login: 'student',
            firstName: 'Student1',
            lastName: 'Student1',
          },
          pinnedDate: '2021-11-10T14:32:27.353605Z',
        },
        {
          user: {
            id: 6,
            login: 'student-2',
            firstName: 'Student2',
            lastName: 'Student2',
          },
        },
      ])
    );
  }),
  rest.get('/api/journeys/2/students', (req, res, ctx) => {
    return res(ctx.json([]));
  }),
  rest.get('/management/audits?principals=student1', (req, res, ctx) => {
    return res(
      ctx.json([
        {
          timestamp: '2021-02-19T12:30:51.724179Z',
          principal: 'student1',
          type: 'AUTHENTICATION_SUCCESS',
        },
      ])
    );
  }),
  rest.get('/api/journeys/1/average-grade/', (req, res, ctx) => {
    return res(
      ctx.json({
        '5': 81.5673,
      })
    );
  }),
  rest.get('/api/notifications', (req, res, ctx) => {
    const type: string = req.url.searchParams.get('type');

    if (type === 'MESSAGE') {
      return res(
        ctx.json([
          {
            id: 1,
            message: '{RECEIVED_MESSAGE}',
            type: 'MESSAGE',
            link: '/assignment/1?journeyId=1',
            read: null,
            journeyName: 'Test journey',
            journeyId: 1,
            assignmentName: 'test1',
            creatorName: 'Test Kasutaja',
            educationalAlignmentTargetNames: ['Test kursus'],
            createdDate: '2021-02-01T10:38:07.153971Z',
            subject: {
              id: 1,
              label: 'Eesti keel',
            },
          },
        ])
      );
    }

    if (type === 'GENERAL') {
      return res(
        ctx.json([
          {
            id: 1,
            message: '{HAS_REJECTED}',
            type: 'GENERAL',
            link: '/assignment/1?journeyId=1',
            read: null,
            journeyName: 'Test journey',
            journeyId: 1,
            assignmentName: 'test1',
            creatorName: 'Test Kasutaja',
            educationalAlignmentTargetNames: ['Test kursus'],
            createdDate: '2021-02-01T10:38:07.153971Z',
            subject: {
              id: 1,
              label: 'Eesti keel',
            },
          },
          {
            id: 1,
            message: '{JOINED_JOURNEY}',
            type: 'GENERAL',
            link: '/journey/1',
            read: null,
            journeyName: 'Test journey',
            journeyId: 1,
            assignmentName: 'test1',
            creatorName: 'Uus õpilane',
            educationalAlignmentTargetNames: ['Test kursus'],
            createdDate: '2021-02-01T10:38:07.153971Z',
            subject: {
              id: 1,
              label: 'Eesti keel',
            },
          },
        ])
      );
    }
  }),

  rest.get('/api/materials/suggested', (req, res, ctx) => {
    return res(
      ctx.json([
        {
          id: 4651,
          title: 'a',
          type: 'LINK',
          description: 'a',
          externalId: null,
          url: 'https://www.youtube.com/watch?v=RtkHZfcDtpY',
          uploadedFile: null,
          educationalAlignments: [
            {
              id: 3,
              title: 'Matemaatika',
              alignmentType: 'subjectArea',
              educationalFramework: 'Estonian National Curriculum',
              targetName: 'Matemaatika',
              targetUrl: null,
              startDate: '2020-07-01T05:00:12Z',
              endDate: null,
              taxonId: 109,
              children: [],
            },
          ],
          restricted: false,
          createdBy: 'teacher',
        },
      ])
    );
  }),

  rest.get('/api/suggestions/suggested', (req, res, ctx) => {
    const source = req.url.searchParams.get('source');
    if (source === OPIQ) {
      return res(ctx.json([]));
    } else if (source === EKOOLIKOTT) {
      return res(
        ctx.json([
          {
            id: null,
            title: 'Kümnendmurru koostis. Kümnendmurru kujutamine arvkiirel 5. klass',
            type: '.EkoolikottMaterial',
            description: null,
            externalId: '20827',
            url: null,
            imageUrl: null,
            uploadedFile: null,
            educationalAlignments: [],
            restricted: null,
            createdBy: null,
            sequenceNumber: null,
          },
        ])
      );
    }
    return res(
      ctx.json([
        {
          id: null,
          title: 'Inimesed ajas. Ajalugu 5. klassile',
          type: '.OpiqMaterial',
          description: 'Sõnastik',
          externalId: '31',
          url: 'https://astra-test.azurewebsites.net/kit/31/chapter/1221#s2113',
          imageUrl:
            'https://astratest.blob.core.windows.net/kitcontent/9c93beeb-623c-4768-8c69-8a5d813a4824/f035d7f3-d369-44d8-8840-be6a8f22bc53/avatud_raamat_Pixabay_xssq.jpg',
          uploadedFile: null,
          educationalAlignments: [],
          restricted: null,
          createdBy: null,
          sequenceNumber: null,
        },
        {
          id: null,
          title: 'Bioloogia 9. klassile',
          type: '.OpiqMaterial',
          description: 'Mõistete seletused',
          externalId: '44',
          url: 'https://astra-test.azurewebsites.net/kit/44/chapter/1858#s6920',
          imageUrl:
            'https://astratest.blob.core.windows.net/kitcontent/fc24fdd1-a175-4e56-855d-b5c7701dafe8/8ce2684a-ac59-406d-8742-af54b3e335ce/519acc48-c484-4ac7-ab8b-7e1b0c9cf632_xssq.jpg',
          uploadedFile: null,
          educationalAlignments: [],
          restricted: null,
          createdBy: null,
          sequenceNumber: null,
        },
        {
          id: null,
          title: 'Kümnendmurru koostis. Kümnendmurru kujutamine arvkiirel 5. klass',
          type: '.EkoolikottMaterial',
          description: null,
          externalId: '20827',
          url: null,
          imageUrl: null,
          uploadedFile: null,
          educationalAlignments: [],
          restricted: null,
          createdBy: null,
          sequenceNumber: null,
        },
        {
          id: null,
          title: 'Massiühikud. Massiühikute teisendamine 4. klass',
          type: '.EkoolikottMaterial',
          description: null,
          externalId: '20880',
          url: null,
          imageUrl: null,
          uploadedFile: null,
          educationalAlignments: [],
          restricted: null,
          createdBy: null,
          sequenceNumber: null,
        },
      ])
    );
  }),

  rest.get('/api/suggestions/search', (req, res, ctx) => {
    const source = req.url.searchParams.get('source');
    if (source === OPIQ) {
      return res(
        ctx.json([
          {
            id: null,
            title: 'Inimesed ajas. Ajalugu 5. klassile',
            type: '.OpiqMaterial',
            description: 'Sõnastik',
            externalId: '31',
            url: 'https://astra-test.azurewebsites.net/kit/31/chapter/1221#s2113',
            imageUrl:
              'https://astratest.blob.core.windows.net/kitcontent/9c93beeb-623c-4768-8c69-8a5d813a4824/f035d7f3-d369-44d8-8840-be6a8f22bc53/avatud_raamat_Pixabay_xssq.jpg',
            uploadedFile: null,
            educationalAlignments: [],
            restricted: null,
            createdBy: null,
            sequenceNumber: null,
          },
          {
            id: null,
            title: 'Bioloogia 9. klassile',
            type: '.OpiqMaterial',
            description: 'Mõistete seletused',
            externalId: '44',
            url: 'https://astra-test.azurewebsites.net/kit/44/chapter/1858#s6920',
            imageUrl:
              'https://astratest.blob.core.windows.net/kitcontent/fc24fdd1-a175-4e56-855d-b5c7701dafe8/8ce2684a-ac59-406d-8742-af54b3e335ce/519acc48-c484-4ac7-ab8b-7e1b0c9cf632_xssq.jpg',
            uploadedFile: null,
            educationalAlignments: [],
            restricted: null,
            createdBy: null,
            sequenceNumber: null,
          },
        ])
      );
    } else if (source === EKOOLIKOTT) {
      return res(
        ctx.json([
          {
            id: null,
            title: 'Kümnendmurru koostis. Kümnendmurru kujutamine arvkiirel 5. klass',
            type: '.EkoolikottMaterial',
            description: null,
            externalId: '20827',
            url: null,
            imageUrl: null,
            uploadedFile: null,
            educationalAlignments: [],
            restricted: null,
            createdBy: null,
            sequenceNumber: null,
          },
          {
            id: null,
            title: 'Massiühikud. Massiühikute teisendamine 4. klass',
            type: '.EkoolikottMaterial',
            description: null,
            externalId: '20880',
            url: null,
            imageUrl: null,
            uploadedFile: null,
            educationalAlignments: [],
            restricted: null,
            createdBy: null,
            sequenceNumber: null,
          },
        ])
      );
    }
    return res(
      ctx.json([
        {
          id: null,
          title: 'Inimesed ajas. Ajalugu 5. klassile',
          type: '.OpiqMaterial',
          description: 'Sõnastik',
          externalId: '31',
          url: 'https://astra-test.azurewebsites.net/kit/31/chapter/1221#s2113',
          imageUrl:
            'https://astratest.blob.core.windows.net/kitcontent/9c93beeb-623c-4768-8c69-8a5d813a4824/f035d7f3-d369-44d8-8840-be6a8f22bc53/avatud_raamat_Pixabay_xssq.jpg',
          uploadedFile: null,
          educationalAlignments: [],
          restricted: null,
          createdBy: null,
          sequenceNumber: null,
        },
        {
          id: null,
          title: 'Bioloogia 9. klassile',
          type: '.OpiqMaterial',
          description: 'Mõistete seletused',
          externalId: '44',
          url: 'https://astra-test.azurewebsites.net/kit/44/chapter/1858#s6920',
          imageUrl:
            'https://astratest.blob.core.windows.net/kitcontent/fc24fdd1-a175-4e56-855d-b5c7701dafe8/8ce2684a-ac59-406d-8742-af54b3e335ce/519acc48-c484-4ac7-ab8b-7e1b0c9cf632_xssq.jpg',
          uploadedFile: null,
          educationalAlignments: [],
          restricted: null,
          createdBy: null,
          sequenceNumber: null,
        },
        {
          id: null,
          title: 'Kümnendmurru koostis. Kümnendmurru kujutamine arvkiirel 5. klass',
          type: '.EkoolikottMaterial',
          description: null,
          externalId: '20827',
          url: null,
          imageUrl: null,
          uploadedFile: null,
          educationalAlignments: [],
          restricted: null,
          createdBy: null,
          sequenceNumber: null,
        },
        {
          id: null,
          title: 'Massiühikud. Massiühikute teisendamine 4. klass',
          type: '.EkoolikottMaterial',
          description: null,
          externalId: '20880',
          url: null,
          imageUrl: null,
          uploadedFile: null,
          educationalAlignments: [],
          restricted: null,
          createdBy: null,
          sequenceNumber: null,
        },
      ])
    );
  }),
  rest.get('/api/materials/search', (req, res, ctx) => {
    return res(ctx.json([]));
  }),
  rest.get('/api/subjects', (req, res, ctx) => {
    return res(
      ctx.json([
        {
          id: 1,
          label: 'schoolabyApp.subject.estonian',
        },
        {
          id: 2,
          label: 'schoolabyApp.subject.estonianForeign',
        },
      ])
    );
  }),

  rest.get('/api/educational-alignments', (req, res, ctx) => {
    return res(
      ctx.json([
        {
          id: 615,
          title: 'Arvsõna',
          alignmentType: 'subject',
          educationalFramework: 'Estonian National Curriculum',
          targetName: 'Arvsõna',
          targetUrl: null,
          startDate: '2020-07-01T05:00:12Z',
          endDate: null,
          taxonId: 40191,
          children: [],
        },
        {
          id: 11,
          title: 'Kirjutamine',
          alignmentType: 'subject',
          educationalFramework: 'Estonian National Curriculum',
          targetName: 'Kirjutamine',
          targetUrl: null,
          startDate: '2020-07-01T05:00:12Z',
          endDate: null,
          taxonId: 30001,
          children: [],
        },
      ])
    );
  }),

  rest.get('/api/rubrics/templates', (req, res, ctx) => {
    return res(
      ctx.json([
        {
          id: 30351,
          title: 'Test',
          isTemplate: true,
          minPoints: 0,
          maxPoints: 0,
          assignmentId: null,
          createdBy: 'teacher',
          criterions: [
            {
              id: 30401,
              title: '1',
              description: '-',
              sequenceNumber: 0,
              minPoints: 0,
              maxPoints: 0,
              levels: [
                {
                  id: 30452,
                  title: '1',
                  points: 0,
                  description: '-',
                  sequenceNumber: 0,
                  criterionId: 30401,
                },
                {
                  id: 30451,
                  title: '2',
                  points: 0,
                  description: '-',
                  sequenceNumber: 1,
                  criterionId: 30401,
                },
                {
                  id: 30453,
                  title: '3',
                  points: 0,
                  description: '-',
                  sequenceNumber: 2,
                  criterionId: 30401,
                },
              ],
            },
            {
              id: 30402,
              title: '2',
              description: '-',
              sequenceNumber: 1,
              minPoints: 0,
              maxPoints: 0,
              levels: [
                {
                  id: 30454,
                  title: '1',
                  points: 0,
                  description: '-',
                  sequenceNumber: 0,
                  criterionId: 30402,
                },
                {
                  id: 30455,
                  title: '2',
                  points: 0,
                  description: '-',
                  sequenceNumber: 1,
                  criterionId: 30402,
                },
                {
                  id: 30456,
                  title: '3',
                  points: 0,
                  description: '-',
                  sequenceNumber: 2,
                  criterionId: 30402,
                },
              ],
            },
            {
              id: 30403,
              title: '3',
              description: '-',
              sequenceNumber: 2,
              minPoints: 0,
              maxPoints: 0,
              levels: [
                {
                  id: 30459,
                  title: '1',
                  points: 0,
                  description: '-',
                  sequenceNumber: 0,
                  criterionId: 30403,
                },
                {
                  id: 30458,
                  title: '2',
                  points: 0,
                  description: '-',
                  sequenceNumber: 1,
                  criterionId: 30403,
                },
                {
                  id: 30457,
                  title: '3',
                  points: 0,
                  description: '-',
                  sequenceNumber: 2,
                  criterionId: 30403,
                },
              ],
            },
          ],
        },
      ])
    );
  }),
  rest.get('/api/schools', (req, res, ctx) => {
    const searchString = req.url.searchParams.get('search');
    const schools = [
      {
        id: 401,
        regNr: '75018897',
        ehisId: '511',
        name: 'Tallinna Saksa Gümnaasium',
      },
      {
        id: 364,
        regNr: '75018420',
        ehisId: '498',
        name: 'Tallinna 32. Keskkool',
      },
    ];
    return res(ctx.json(schools.filter(school => school.name.toLowerCase().includes(searchString.toLowerCase()))));
  }),

  rest.get('/api/submissions', (req, res, ctx) => {
    return res(
      ctx.json([
        {
          id: 6,
          value: 'Hey this **editor** rocks \uD83D\uDE00',
          resubmittable: false,
          submittedForGradingDate: '2021-03-15T10:01:47.890798Z',
          assignmentId: 1234,
          assignmentTitle: 'Lahendada ülesanne',
          highestGrade: true,
          authors: [
            {
              id: 5,
              login: 'student',
              firstName: 'student',
              lastName: 'student',
            },
          ],
        },
      ])
    );
  }),

  rest.post('/api/submissions', (req, res, ctx) => {
    return res(
      ctx.json({
        id: 1,
      })
    );
  }),

  rest.get('/api/group-assignments', (req, res, ctx) => {
    const searchString = req.url.searchParams.get('search');
    const groupAssignments = [
      {
        id: 1,
        title: 'Group assignment 1',
        createdDate: '2021-08-13T13:50:57.859816Z',
        groups: [
          {
            id: 3551,
            name: 'Group 1',
            students: [
              {
                login: 'student1',
              },
            ],
          },
          {
            id: 3552,
            name: 'Group 2',
            students: [
              {
                login: 'student2',
              },
            ],
          },
        ],
      },
      {
        id: 2,
        title: 'Group assignment 2',
        createdDate: '2021-08-15T13:50:57.859816Z',
        groups: [
          {
            id: 3553,
            name: 'Group 3',
            students: [
              {
                login: 'student1',
              },
            ],
          },
          {
            id: 3554,
            name: 'Group 4',
            students: [
              {
                login: 'student2',
              },
            ],
          },
        ],
      },
    ];
    return res(ctx.json(groupAssignments.filter(assignment => assignment.title.toLowerCase().includes(searchString.toLowerCase()))));
  }),

  rest.get('/api/curricula', (req, res, ctx) => {
    return res(
      ctx.json([
        {
          title: 'NATIONAL_CURRICULUM',
          sequenceNumber: 1,
        },
        {
          title: 'EXTRACURRICULAR',
          sequenceNumber: 2,
        },
      ])
    );
  }),

  rest.get('/api/rubrics', (req, res, ctx) => {
    return res(ctx.json(createRubric()));
  }),

  rest.post('/api/support', (req, res, ctx) => {
    return res();
  }),

  rest.get('/api/authenticate/refresh', (req, res, ctx) => {
    return res(ctx.json({}));
  }),
];

const server = setupServer(...handlers);

beforeAll(() => server.listen({ onUnhandledRequest: 'error' }));
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

export { server, rest, scoreGradable, ltiResource };
