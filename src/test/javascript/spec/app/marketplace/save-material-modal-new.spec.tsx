import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { EntityUpdateProvider } from 'app/shared/contexts/entity-update-context';
import { MaterialProvider } from 'app/shared/contexts/material-context';
import configureMockStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import SaveMaterialModalNew, {
  CAMERA,
  FILE,
  LINK,
  SaveMaterialModalType,
} from 'app/shared/layout/save-material-modal-new/save-material-modal-new';
import { COUNTRY } from 'app/config/constants';

const mockStore = configureMockStore();

describe('SaveMaterialModalNew', () => {
  const store = mockStore({
    locale: {
      currentLocale: '',
    },
    applicationProfile: {
      isInProduction: false,
    },
    authentication: {
      account: {
        country: COUNTRY.ESTONIA,
      },
    },
  });

  const renderComponent = (type: SaveMaterialModalType = LINK) =>
    render(
      <Provider store={store}>
        <EntityUpdateProvider>
          <MaterialProvider>
            <SaveMaterialModalNew type={type} isOpen={true} toggleModal={() => {}} />
          </MaterialProvider>
        </EntityUpdateProvider>
      </Provider>
    );

  it('should render form when type is LINK', () => {
    renderComponent();

    screen.getByText('schoolabyApp.marketplace.modal.add.link');
    screen.getByText('schoolabyApp.material.title');
    screen.getByText('schoolabyApp.material.description');
    screen.getByText('schoolabyApp.material.url');
    screen.getByText('schoolabyApp.material.privateMaterial');

    const titleInput = screen.getByRole('textbox', { name: 'schoolabyApp.material.title' });
    expect(titleInput).toHaveAttribute('placeholder', 'schoolabyApp.marketplace.modal.titlePlaceholder');

    const descriptionInput = screen.getByRole('textbox', { name: 'schoolabyApp.material.description' });
    expect(descriptionInput).toHaveAttribute('placeholder', 'schoolabyApp.marketplace.modal.descriptionPlaceholder');

    const urlInput = screen.getByRole('textbox', { name: 'schoolabyApp.material.url' });
    expect(urlInput).toHaveAttribute('placeholder', 'schoolabyApp.marketplace.modal.linkPlaceholder');

    const checkbox = screen.getByRole('checkbox', { name: 'schoolabyApp.material.privateMaterial' });
    expect(checkbox).not.toBeChecked();

    screen.getByRole('button', { name: 'entity.action.cancel' });
    screen.getByRole('button', { name: 'entity.action.add' });
  });

  it('should render form when type is FILE', () => {
    renderComponent(FILE);

    screen.getByText('schoolabyApp.marketplace.modal.add.file');
    screen.getByText('schoolabyApp.material.title');
    screen.getByText('schoolabyApp.material.description');
    screen.getByText('schoolabyApp.material.privateMaterial');

    const titleInput = screen.getByRole('textbox', { name: 'schoolabyApp.material.title' });
    expect(titleInput).toHaveAttribute('placeholder', 'schoolabyApp.marketplace.modal.titlePlaceholder');

    const descriptionInput = screen.getByRole('textbox', { name: 'schoolabyApp.material.description' });
    expect(descriptionInput).toHaveAttribute('placeholder', 'schoolabyApp.marketplace.modal.descriptionPlaceholder');

    const urlInput = screen.queryByRole('textbox', { name: 'schoolabyApp.material.url' });
    expect(urlInput).not.toBeInTheDocument();

    screen.getByText('global.upload.dropPasteMaterials');
    screen.getByRole('button', { name: /global.upload.upload/ });

    const checkbox = screen.getByRole('checkbox', { name: 'schoolabyApp.material.privateMaterial' });
    expect(checkbox).not.toBeChecked();

    screen.getByRole('button', { name: 'entity.action.cancel' });
    screen.getByRole('button', { name: 'entity.action.add' });
  });

  it('should render form when type is CAMERA', () => {
    renderComponent(CAMERA);

    screen.getByText('schoolabyApp.marketplace.modal.add.camera');
    screen.getByText('schoolabyApp.material.title');
    screen.getByText('schoolabyApp.material.description');
    screen.getByText('schoolabyApp.material.privateMaterial');

    const titleInput = screen.getByRole('textbox', { name: 'schoolabyApp.material.title' });
    expect(titleInput).toHaveAttribute('placeholder', 'schoolabyApp.marketplace.modal.titlePlaceholder');

    const descriptionInput = screen.getByRole('textbox', { name: 'schoolabyApp.material.description' });
    expect(descriptionInput).toHaveAttribute('placeholder', 'schoolabyApp.marketplace.modal.descriptionPlaceholder');

    const urlInput = screen.queryByRole('textbox', { name: 'schoolabyApp.material.url' });
    expect(urlInput).not.toBeInTheDocument();

    screen.getByRole('button', { name: /global.upload.useCamera/ });

    const checkbox = screen.getByRole('checkbox', { name: 'schoolabyApp.material.privateMaterial' });
    expect(checkbox).not.toBeChecked();

    screen.getByRole('button', { name: 'entity.action.cancel' });
    screen.getByRole('button', { name: 'entity.action.add' });
  });

  it('should render errors if title and url fields empty and add button clicked', async () => {
    renderComponent();

    const addButton = screen.getByRole('button', { name: 'entity.action.add' });
    fireEvent.click(addButton);

    const errors = await screen.findAllByText('entity.validation.required');
    expect(errors).toHaveLength(2);
  });

  it('should render errors if title is empty and file not uploaded and add button clicked', async () => {
    renderComponent(FILE);

    const addButton = screen.getByRole('button', { name: 'entity.action.add' });
    fireEvent.click(addButton);

    await screen.findByText('entity.validation.required');
    await screen.findByText('schoolabyApp.marketplace.modal.errorMessage.file');
  });

  it('should render errors if title is empty and picture/video not taken and add button clicked', async () => {
    renderComponent(CAMERA);

    const addButton = screen.getByRole('button', { name: 'entity.action.add' });
    fireEvent.click(addButton);

    await screen.findByText('entity.validation.required');
    await screen.findByText('schoolabyApp.marketplace.modal.errorMessage.camera');
  });
});
