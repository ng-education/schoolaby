import { render, screen } from '@testing-library/react';
import { Router } from 'react-router-dom';
import { Dashboard } from 'app/dashboard/dashboard';
import { createBrowserHistory } from 'history';
import React from 'react';
import { ThroughProvider } from 'react-through';
import configureMockStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { AUTHORITIES } from 'app/config/constants';
import { RootProvider } from 'app/shared/contexts/root-context';
import { QueryClient, QueryClientProvider } from 'react-query';

describe('Dashboard', () => {
  const mockStore = configureMockStore();
  const history = createBrowserHistory();
  const queryClient = new QueryClient();
  let store: any;

  const defaultDashboardProps = () => {
    return {
      isStudent: false,
      isAllowedToModify: true,
      account: { firstName: 'jaan' },
      isInProduction: false,
    };
  };

  beforeEach(() => {
    const initialState = {
      locale: {
        currentLocale: 'en',
      },
      authentication: {
        account: {
          authorities: [AUTHORITIES.TEACHER],
        },
      },
      applicationProfile: {
        inProduction: true,
      },
    };
    store = mockStore(initialState);
  });

  const renderComponent = dashboardProps =>
    render(
      <RootProvider>
        <Provider store={store}>
          <QueryClientProvider client={queryClient}>
            <ThroughProvider>
              <Router history={history}>
                <Dashboard {...dashboardProps} />
              </Router>
            </ThroughProvider>
          </QueryClientProvider>
        </Provider>
      </RootProvider>
    );

  it('should not show DashboardBreadcrumbItem when in production', () => {
    renderComponent(defaultDashboardProps());

    const breadcrumbs = screen.queryByLabelText('breadcrumb');
    expect(breadcrumbs).not.toBeInTheDocument();
  });

  it('should show new dashboard contents', async () => {
    renderComponent(defaultDashboardProps());

    await screen.findByText('schoolabyApp.dashboard.myJourneys');
    expect(await screen.findAllByRole('button', { name: 'entity.add.journey' })).toHaveLength(2);
    await screen.findByText('schoolabyApp.dashboard.noJourneysToDisplay');
  });
});
