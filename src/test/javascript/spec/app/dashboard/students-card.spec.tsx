import React from 'react';
import { render, screen } from '@testing-library/react';
import { createBrowserHistory } from 'history';
import { QueryClient, QueryClientProvider } from 'react-query';
import { Router } from 'react-router-dom';
import StudentsCard from 'app/dashboard/journey-list-card/inner-card/students-card/students-card';

describe('Students card', () => {
  const queryClient = new QueryClient();
  const history = createBrowserHistory();

  const component = () =>
    render(
      <QueryClientProvider client={queryClient}>
        <Router history={history}>
          <StudentsCard journeyId={1} />
        </Router>
      </QueryClientProvider>
    );

  it('should show students data', async () => {
    component();

    await screen.findByText('Student1 Student1');
    await screen.findByText('Student2 Student2');

    const avatars = await screen.findAllByText('SS');
    expect(avatars).toHaveLength(2);
  });

  it('should pinned and unpinned students', async () => {
    component();

    expect(await screen.findAllByLabelText('schoolabyApp.journey.students.pin')).toHaveLength(1);
    expect(await screen.findAllByLabelText('schoolabyApp.journey.students.unpin')).toHaveLength(1);
  });
});
