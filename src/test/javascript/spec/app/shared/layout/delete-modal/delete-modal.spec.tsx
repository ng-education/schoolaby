import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import { QueryClient, QueryClientProvider } from 'react-query';
import DeleteModal from 'app/shared/layout/delete-modal/delete-modal';

describe('Delete modal', () => {
  const history = createBrowserHistory();
  const queryClient = new QueryClient();

  const renderComponent = props =>
    render(
      <Router history={history}>
        <QueryClientProvider client={queryClient}>
          <DeleteModal {...props} />
        </QueryClientProvider>
      </Router>
    );

  it('should render', () => {
    renderComponent({ entityId: 1, entityTitle: 'My Journey', isOpen: true, journeyId: 1 });

    screen.getByText('entity.delete.title');
    screen.getByText('entity.action.cancel');
    screen.getByText('entity.action.delete');
    screen.getByText('entity.delete.description My Journey');
  });

  it('should call toggle fn. when cancelling', () => {
    const mockToggle = jest.fn();

    renderComponent({ id: 1, isOpen: true, journeyId: 1, toggle: mockToggle });

    const cancelButton = screen.getByText('entity.action.cancel');
    fireEvent.click(cancelButton);

    expect(mockToggle).toHaveBeenCalled();
  });
});
