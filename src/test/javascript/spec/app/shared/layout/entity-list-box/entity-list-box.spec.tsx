import React from 'react';
import { EntityUpdateProvider } from 'app/shared/contexts/entity-update-context';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import { createBrowserHistory } from 'history';
import { Router } from 'react-router-dom';
import EntityListBox from 'app/shared/layout/entity-list-box/entity-list-box';
import Avatar from 'app/shared/layout/header/avatar';
import { IUser } from 'app/shared/model/user.model';
import { getFullName } from 'app/shared/util/string-utils';

describe('EntityListBox', () => {
  let store: any;

  const mockStore = configureMockStore();
  const history = createBrowserHistory();

  beforeEach(() => {
    store = mockStore(undefined);
  });

  const mockUser: IUser = {
    firstName: 'first',
    lastName: 'last',
  };

  const renderWithRedux = () => {
    return {
      ...render(
        <Provider store={store}>
          <EntityUpdateProvider>
            <Router history={history}>
              <EntityListBox
                avatar={<Avatar fullName={getFullName(mockUser)} showName />}
                additionalComponent={<p>Additional component</p>}
              />
            </Router>
          </EntityUpdateProvider>
        </Provider>
      ),
      store,
    };
  };

  it('Renders component with avatar and additional component', () => {
    renderWithRedux();
    screen.getByText('FL');
    screen.getByText('Additional component');
  });
});
