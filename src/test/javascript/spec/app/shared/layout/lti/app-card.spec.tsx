import React from 'react';
import { DropdownMenu } from 'reactstrap';
import { render, screen, waitFor } from '@testing-library/react';
import AppCard, { IAppCardProps } from 'app/shared/layout/lti/app-card';

describe('AppCard', () => {
  let mountedWrapper;

  const appProps: IAppCardProps = {
    title: 'Test app',
    description: 'Test description',
    imageSrc: 'http://test.url',
    dropDown: <DropdownMenu>Test</DropdownMenu>,
    xs: '1',
    md: '3',
    lg: '4',
    xl: '5',
    gradable: true,
  };

  const ltiAppCardProps: IAppCardProps = {
    title: 'Test app',
    description: 'Test description',
    imageSrc: 'http://test.url',
    consumerKey: 'consumer key',
    sharedSecret: 'shared secret',
  };

  beforeEach(() => {
    mountedWrapper = undefined;
  });

  const wrapper = (appCardProps: IAppCardProps) => {
    if (!mountedWrapper) {
      mountedWrapper = render(<AppCard {...appCardProps} />);
    }
    return mountedWrapper;
  };

  it('Renders component with dropdown', async () => {
    const props = { ...appProps, dropDown: <DropdownMenu>Test Dropdown Menu</DropdownMenu> };
    wrapper(props);

    await screen.findByText('Test Dropdown Menu');
    screen.getByAltText(props.title);
    await screen.findByText(props.description);
    await screen.findByText(props.title);
  });

  it('Renders component without dropdown', () => {
    wrapper(appProps);
    const element = screen.queryByText('Test Dropdown Menu');
    expect(element).not.toBeInTheDocument();
  });

  it('Does not render LTI app not configured warning sign when consumer key and shared secret are defined', async () => {
    wrapper(ltiAppCardProps);
    await waitFor(() => {
      const exclamationMark = screen.queryByRole('exclamation-mark-icon');
      expect(exclamationMark).toBeNull();
    });
  });

  it('Renders LTI app not configured warning when consumer key or shared secret are null', async () => {
    ltiAppCardProps.consumerKey = null;
    ltiAppCardProps.sharedSecret = null;
    wrapper(appProps);
    await waitFor(() => {
      const exclamationMark = screen.getByRole('exclamation-mark-icon');
      expect(exclamationMark).not.toBeNull();
    });
  });
});
