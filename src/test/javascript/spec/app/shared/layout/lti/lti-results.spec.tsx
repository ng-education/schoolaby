import React from 'react';
import { render, screen, waitFor } from '@testing-library/react';
import ContextProvider from '../../../../helper/context-mock/context-provider';
import configureMockStore from 'redux-mock-store';
import MockEntityDetailProvider from '../../../../helper/context-mock/mock-entity-detail-provider';
import { scoreGradable, ltiResource } from '../../../../rtl-setup';
import LtiResults from 'app/shared/layout/lti/lti-results';

describe('LtiResults', () => {
  let store: any;
  const mockStore = configureMockStore();

  beforeEach(() => {
    const initialState = {
      authentication: {
        account: {
          id: 1,
        },
      },
    };
    store = mockStore(initialState);
  });

  const providerProps = {
    selectedUser: {
      id: 1,
    },
  };

  function component() {
    return (
      <ContextProvider store={store} contextProvider={MockEntityDetailProvider} props={providerProps}>
        <LtiResults scores={[scoreGradable]} ltiResources={[ltiResource]} />
      </ContextProvider>
    );
  }

  it('Renders component', async () => {
    render(component());
    await waitFor(() => {
      screen.getByText('50%');
      screen.getByText(scoreGradable.title);
    });
  });
});
