import React from 'react';
import { AUTHORITIES } from 'app/config/constants';
import configureMockStore from 'redux-mock-store';
import { render, screen, waitFor } from '@testing-library/react';
import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';
import MessageNotificationDropdown from 'app/shared/layout/header/notification/message-notification-dropdown';
import { QueryClient, QueryClientProvider } from 'react-query';
import { UserNotificationProvider } from 'app/shared/contexts/user-notification-context';

describe('Message notification dropdown', () => {
  let store: any;
  const mockStore = configureMockStore();
  const queryClient = new QueryClient();

  beforeAll(() => {
    store = mockStore({
      authentication: {
        account: {
          authorities: [AUTHORITIES.STUDENT],
        },
      },
      applicationProfile: {
        inProduction: false,
      },
    });
  });

  const renderComponent = () => {
    return render(
      <QueryClientProvider client={queryClient}>
        <UserNotificationProvider>
          <Provider store={store}>
            <MessageNotificationDropdown />
          </Provider>
        </UserNotificationProvider>
      </QueryClientProvider>,
      { wrapper: MemoryRouter }
    );
  };

  test('user can see link details', async () => {
    renderComponent();

    await waitFor(() => {
      expect(screen.getByRole('button', { name: /mail/i }));

      expect(screen.getByText('Test journey'));
      expect(screen.getByText('schoolabyApp.notification.{RECEIVED_MESSAGE}'));
      expect(screen.getByText('Test Kasutaja'));
    });
  });

  test('user cannot see "Mark all as seen" button"', async () => {
    renderComponent();

    await waitFor(() => {
      const markAllReadButton = screen.queryByText('schoolabyApp.notification.markAllAsRead');
      expect(markAllReadButton).toBeNull();
    });
  });
});
