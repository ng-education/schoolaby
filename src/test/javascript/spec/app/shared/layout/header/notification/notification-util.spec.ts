import { filterNotifications, getDiffFromToday } from 'app/shared/layout/header/notification/notification-util';
import { DateTime } from 'luxon';
import * as reactJhipsterExports from 'react-jhipster';
import { INotification } from 'app/shared/model/notification.model';

const today = DateTime.local();

const notification1: INotification = {
  creatorName: 'author1',
  assignmentName: 'assignment1',
};

const notification2: INotification = {
  creatorName: 'author2',
  assignmentName: 'assignment2',
};

describe('Navigation utils', () => {
  let notifications: INotification[];

  beforeEach(() => {
    jest.spyOn(reactJhipsterExports, 'translate').mockImplementation(value => value);
    notifications = [notification1, notification2];
  });

  it('should map correct time translations ', () => {
    let result = getDiffFromToday(today.minus({ days: 2 }).toISO());
    expect(result.time).toEqual(2);
    expect(result.translationKey).toEqual('schoolabyApp.notification.time.daysAgo');

    result = getDiffFromToday(today.minus({ months: 2 }).toISO());
    expect(result.time).toEqual(2);
    expect(result.translationKey).toEqual('schoolabyApp.notification.time.monthsAgo');

    result = getDiffFromToday(today.minus({ minutes: 2 }).toISO());
    expect(result.time).toEqual(2);
    expect(result.translationKey).toEqual('schoolabyApp.notification.time.minutesAgo');

    result = getDiffFromToday(today.minus({ hours: 2 }).toISO());
    expect(result.time).toEqual(2);
    expect(result.translationKey).toEqual('schoolabyApp.notification.time.hoursAgo');

    result = getDiffFromToday(today.toISO());
    expect(result.translationKey).toEqual('schoolabyApp.notification.time.now');
  });

  it('should return messages by author name', () => {
    expect(filterNotifications(notifications, 'author1')).toStrictEqual([notification1]);
  });
  it('should return messages by chat title', () => {
    expect(filterNotifications(notifications, 'assignment2')).toStrictEqual([notification2]);
  });
  it('should return nothing when no result found', () => {
    expect(filterNotifications(notifications, 'random string')).toStrictEqual([]);
  });
});
