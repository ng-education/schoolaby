import React from 'react';
import { AUTHORITIES } from 'app/config/constants';
import configureMockStore from 'redux-mock-store';
import { render, screen, waitFor } from '@testing-library/react';
import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';
import UserNotificationDropdown from 'app/shared/layout/header/notification/user-notification-dropdown';
import { QueryClient, QueryClientProvider } from 'react-query';
import { UserNotificationProvider } from 'app/shared/contexts/user-notification-context';

describe('User notification dropdown', () => {
  let store: any;
  const mockStore = configureMockStore();
  const queryClient = new QueryClient();

  beforeAll(() => {
    store = mockStore({
      authentication: {
        account: {
          authorities: [AUTHORITIES.STUDENT],
        },
      },
      applicationProfile: {
        inProduction: false,
      },
    });
  });

  const renderComponent = () => {
    return render(
      <QueryClientProvider client={queryClient}>
        <Provider store={store}>
          <UserNotificationProvider>
            <UserNotificationDropdown />
          </UserNotificationProvider>
        </Provider>
      </QueryClientProvider>,
      { wrapper: MemoryRouter }
    );
  };

  test('user can see link details', async () => {
    renderComponent();

    screen.getByRole('button', { name: /bell/i });
    await waitFor(
      () => {
        screen.getByText('Test journey');
        screen.getByText('schoolabyApp.notification.{HAS_REJECTED}');
        screen.getByText('Test Kasutaja');
      },
      { timeout: 1500 }
    );
  });

  test('user can see "Mark all as seen" button"', async () => {
    renderComponent();

    await waitFor(() => {
      const markAllReadButton = screen.queryByText('schoolabyApp.notification.markAllAsRead');
      expect(markAllReadButton).toBeDefined();
    });
  });
});
