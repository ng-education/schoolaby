import React from 'react';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import { QueryClient, QueryClientProvider } from 'react-query';
import JourneyTemplate from 'app/shared/layout/journey-template/journey-template';
import MockJourneyTemplateProvider from '../../../../helper/context-mock/mock-journey-template-provider';
import { AUTHORITIES } from 'app/config/constants';
import configureMockStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { ThroughProvider } from 'react-through';
import { RootProvider } from 'app/shared/contexts/root-context';
import { EntityDetailProvider } from 'app/shared/contexts/entity-detail-context';

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useHistory: () => ({
    push: jest.fn(),
    replace: jest.fn(),
    location: {
      pathname: '',
    },
  }),
  useLocation: () => ({
    search: '',
  }),
}));

describe('Journey templates view', () => {
  let store: any;
  const mockStore = configureMockStore();

  beforeAll(() => {
    store = mockStore({
      authentication: {
        account: {
          id: 5,
          authorities: [AUTHORITIES.TEACHER],
        },
      },
      locale: {
        currentLocale: 'et',
      },
      applicationProfile: {
        isInProduction: false,
        isInTestcafe: false,
      },
    });
  });

  const getContextProps = () => ({
    templateSubject: { id: 1 },
    showTemplatesView: true,
    selectedTemplateId: undefined,
  });

  const renderComponent = contextProps =>
    render(
      <Provider store={store}>
        <RootProvider>
          <ThroughProvider>
            <QueryClientProvider client={new QueryClient()}>
              <EntityDetailProvider>
                <MockJourneyTemplateProvider {...contextProps}>
                  <JourneyTemplate />
                </MockJourneyTemplateProvider>
              </EntityDetailProvider>
            </QueryClientProvider>
          </ThroughProvider>
        </RootProvider>
      </Provider>
    );

  it('should display templates view titles', async () => {
    renderComponent(getContextProps());

    await screen.findByText('schoolabyApp.journey.templates.title');
    await screen.findByText('schoolabyApp.journey.templates.description');
    await screen.findByText('schoolabyApp.journey.templates.previewInstructions');
    await screen.findByPlaceholderText('global.search');
  });

  it('should display template tabs', async () => {
    renderComponent(getContextProps());

    await screen.findByText('schoolabyApp.journey.templates.myTemplatesTab');
    await screen.findByText('schoolabyApp.journey.templates.allTemplatesTab');
  });

  it('should display my journeys', async () => {
    renderComponent(getContextProps());

    await screen.findByText('My journey');
    await screen.findByText('My second journey');
  });

  it('should display all journeys', async () => {
    renderComponent(getContextProps());

    fireEvent.click(await screen.findByText('schoolabyApp.journey.templates.allTemplatesTab'));
    await screen.findByText('My journey');
    await screen.findByText('My second journey');
    await screen.findByText('Other teacher journey');
    await screen.findByText('Other teacher second journey');
  });

  it('should display journey details', async () => {
    renderComponent(getContextProps());

    await screen.findByText('My journey');
    await screen.findByText('My second journey');
    expect(await screen.findAllByText('Timo Tammepakk')).toHaveLength(2);
    expect(await screen.findAllByText('schoolabyApp.journey.templates.months')).toHaveLength(2);
    expect(await screen.findAllByText('schoolabyApp.journey.templates.days')).toHaveLength(2);
    expect(await screen.findAllByText('schoolabyApp.journey.templates.assignments')).toHaveLength(2);
    expect(await screen.findAllByText('Gümnaasium')).toHaveLength(2);
  });

  it('should display cancel and select buttons', async () => {
    renderComponent(getContextProps());

    await screen.findByText('entity.action.select');
    await screen.findByText('entity.action.back');
  });

  it('should display warning modal on cancel', async () => {
    renderComponent(getContextProps());
    fireEvent.click(await screen.findByText('entity.action.back'));

    await screen.findByText('schoolabyApp.journey.warningModal.title');
    await screen.findByText('schoolabyApp.journey.warningModal.description');
  });

  it('should display journey in template preview', async () => {
    const contextProps = getContextProps();
    contextProps.selectedTemplateId = 1;
    renderComponent(contextProps);

    expect(screen.queryByText('schoolabyApp.journey.templates.previewInstructions')).not.toBeInTheDocument();

    await screen.findByText('Journey title');
    await screen.findByText('Milestone title');
    await screen.findByText('Milestone description');
    await screen.findByText('Assignment title');
  });

  it('should have a disabled submit button when no template selected', async () => {
    renderComponent(getContextProps());

    await waitFor(() => {
      expect(screen.queryByText('entity.action.select')).toHaveAttribute('disabled');
    });
  });

  it('should display load more button', async () => {
    renderComponent(getContextProps());

    await screen.findByText('schoolabyApp.journey.home.loadMore');
  });
});
