import React from 'react';
import AssignmentPreview, { IAssignmentPreviewProps } from 'app/shared/layout/journey-template/assignment-preview/assignment-preview';
import { render, screen, waitFor, fireEvent } from '@testing-library/react';
import { QueryClient, QueryClientProvider } from 'react-query';
import configureMockStore from 'redux-mock-store';
import { AUTHORITIES } from 'app/config/constants';
import { Provider } from 'react-redux';
import { RootProvider } from 'app/shared/contexts/root-context';
import { EntityUpdateProvider } from 'app/shared/contexts/entity-update-context';
import { MaterialProvider } from 'app/shared/contexts/material-context';
import { AssignmentContext, IAssignmentContext } from 'app/journey/journey-detail/journey-detail-new';

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useHistory: () => ({
    push: jest.fn(),
    replace: jest.fn(),
    location: {
      pathname: '',
    },
  }),
}));

describe('Assignment preview', () => {
  let store: any;
  const mockStore = configureMockStore();

  const getAssignmentPreviewProps = (): IAssignmentPreviewProps => ({
    assignmentId: 1234,
    onBackClicked() {},
    openedPreviewTab: 'ASSIGNMENT',
    setOpenedPreviewTab() {},
  });

  beforeEach(() => {
    const initialState = {
      locale: {
        currentLocale: 'en',
      },
      authentication: {
        account: {
          authorities: [AUTHORITIES.TEACHER],
        },
      },
      applicationProfile: {
        inProduction: false,
        inTestcafe: false,
      },
    };
    store = mockStore(initialState);
  });

  const createDefaultAssignmentContext = (): IAssignmentContext => ({
    assignment: {
      id: 1234,
      title: 'Assignment title',
    },
    journey: {
      id: 1,
    },
    isAllowedToModify: false,
  });

  const renderComponent = (
    previewProps: IAssignmentPreviewProps,
    assignmentContext: IAssignmentContext = createDefaultAssignmentContext()
  ) =>
    render(
      <RootProvider>
        <Provider store={store}>
          <QueryClientProvider client={new QueryClient()}>
            <EntityUpdateProvider>
              <MaterialProvider>
                <AssignmentContext.Provider value={assignmentContext}>
                  <AssignmentPreview {...previewProps} />
                </AssignmentContext.Provider>
              </MaterialProvider>
            </EntityUpdateProvider>
          </QueryClientProvider>
        </Provider>
      </RootProvider>
    );

  it('should display tabs', async () => {
    renderComponent(getAssignmentPreviewProps());

    await screen.findByText('schoolabyApp.assignment.detail.tabs.assignment');
    await screen.findByText('schoolabyApp.assignment.detail.tabs.backpack');
  });

  it('should display assignment contents', async () => {
    renderComponent(getAssignmentPreviewProps());

    await screen.findByText('schoolabyApp.assignment.detail.title');
    await screen.findByText('Assignment title');
    await screen.findByText('schoolabyApp.assignment.deadline');
    await screen.findByText('07.09.2020 09:24');
    await screen.findByText('schoolabyApp.assignment.gradingScheme');
    await screen.findByText('schoolabyApp.gradingScheme.code.ALPHABETICAL_A_F');

    await screen.findByText('Description text');
    await screen.findByText('schoolabyApp.assignment.rubric.viewTitle');
    await screen.findByText('schoolabyApp.assignment.detail.assets');
    await screen.findByText('LTI Test title');
    await screen.findByText('LTI Test description');
  });

  it('should not display hidden AssignmentInfo contents', async () => {
    renderComponent(getAssignmentPreviewProps());

    await waitFor(() => {
      expect(screen.queryByText('entity.publishedState.hidden')).not.toBeInTheDocument();
      expect(screen.queryByText('schoolabyApp.assignment.shareToStuudium')).not.toBeInTheDocument();
      expect(screen.queryByText('schoolabyApp.assignment.detail.tabs.analytics')).not.toBeInTheDocument();
    });
  });

  it('should display rubric', async () => {
    renderComponent(getAssignmentPreviewProps());

    fireEvent.click(await screen.findByText('schoolabyApp.assignment.rubric.viewTitle'));

    await screen.findByText('Test rubric');
    await screen.findByText('Criterion 1');
    await screen.findByText('Criterion 2');

    expect(await screen.findAllByText('Level 1')).toHaveLength(2);
    expect(await screen.findAllByText('Level 2')).toHaveLength(2);

    expect(await screen.findAllByText('Criterion description')).toHaveLength(2);
    expect(await screen.findAllByText('Level description')).toHaveLength(4);
  });

  it('should display back button', async () => {
    renderComponent(getAssignmentPreviewProps());

    await screen.findByText('entity.action.back');
  });

  it('should display assignment backpack', async () => {
    const previewProps = getAssignmentPreviewProps();
    previewProps.openedPreviewTab = 'ASSIGNMENT_BACKPACK';
    renderComponent(previewProps);

    await screen.findByText('Assignment title');
    await screen.findByText('schoolabyApp.assignment.detail.tabs.assignment');
    await screen.findByText('schoolabyApp.assignment.detail.tabs.backpack');
    await screen.findByText('entity.action.back');
  });
});
