import React from 'react';
import { act, render, screen, waitFor } from '@testing-library/react';
import { QueryClient, QueryClientProvider } from 'react-query';
import configureMockStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { RootProvider } from 'app/shared/contexts/root-context';
import SchoolSearchDropdown from 'app/shared/layout/school-search-dropdown/school-search-dropdown';
import userEvent from '@testing-library/user-event';
import { Form } from 'react-final-form';

describe('School search dropdown', function () {
  let store;
  const queryClient = new QueryClient();
  const mockStore = configureMockStore();

  const renderComponent = props => {
    return render(
      <RootProvider>
        <Provider store={store}>
          <QueryClientProvider client={queryClient}>
            <Form onSubmit={() => {}}>{() => <SchoolSearchDropdown {...props} />}</Form>
          </QueryClientProvider>
        </Provider>
      </RootProvider>
    );
  };

  beforeEach(() => {
    const initialState = {
      authentication: {
        account: {
          country: 'Estonia',
        },
      },
    };
    store = mockStore(initialState);
  });

  it('Should render schools when typing', async () => {
    renderComponent({ disabled: false });

    const input = screen.getByRole('textbox');
    act(() => userEvent.type(input, 'Tallinna Saksa'));

    await waitFor(() => {
      screen.getByText('Tallinna Saksa Gümnaasium');
    });
  });

  it('Should not render schools text when no results', async () => {
    renderComponent({ disabled: false });

    const input = screen.getByRole('textbox');

    userEvent.type(input, 'no schools text');

    await waitFor(() => {
      screen.getByText('entity.action.noResults');
    });
  });

  it('Should render with options when schools previously selected', async () => {
    renderComponent({
      disabled: false,
      input: {
        value: {
          id: 401,
          regNr: '75018897',
          ehisId: '511',
          name: 'Tallinna Saksa Gümnaasium',
        },
      },
    });

    await waitFor(() => {
      screen.getByText('Tallinna Saksa Gümnaasium');
    });
  });
});
