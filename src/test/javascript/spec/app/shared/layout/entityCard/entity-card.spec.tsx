import React from 'react';
import EntityCard, { IEntityCardProps } from 'app/shared/layout/entity-card/entity-card';
import MenuDropdown from 'app/shared/layout/menu-dropdown/menu-dropdown';
import { JOURNEY } from 'app/shared/util/entity-utils';
import ISubject from 'app/shared/model/subject.model';
import { render, screen } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import MockEntityUpdateProvider from '../../../../helper/context-mock/mock-entity-update-provider';
import configureMockStore from 'redux-mock-store';
import { AUTHORITIES } from 'app/config/constants';
import ContextProvider from '../../../../helper/context-mock/context-provider';
import { DateTime } from 'luxon';
import { QueryClientProvider, QueryClient } from 'react-query';

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useHistory: () => ({
    push: jest.fn(),
    location: {
      pathname: '/journey/2/',
    },
  }),
}));

describe('EntityCard', () => {
  let store: any;
  const mockStore = configureMockStore();

  const subject: ISubject = {
    id: 1,
    label: 'schoolabyApp.subject.physicalEducation',
  };

  const entityCardProps: IEntityCardProps = {
    color: 'success',
    deadline: DateTime.fromISO('2020-07-01T02:31:41', { zone: 'local' }),
    entityId: 1,
    href: '#',
    title: 'Card title',
    subject,
    isAssignment: false,
    teacherName: 'Max Payne',
    studentCount: 420,
  };

  const initialState = {
    locale: {
      currentLocale: 'en',
    },
    authentication: {
      account: {
        authorities: [AUTHORITIES.TEACHER],
      },
    },
  };

  beforeEach(() => {
    store = mockStore(initialState);
  });

  const renderComponent = (props: IEntityCardProps) => {
    render(
      <QueryClientProvider client={new QueryClient()}>
        <ContextProvider contextProvider={MockEntityUpdateProvider} store={store}>
          <MemoryRouter>
            <EntityCard {...props} />
          </MemoryRouter>
        </ContextProvider>
      </QueryClientProvider>
    );
  };

  it('Should render EntityCard component with correct values', () => {
    renderComponent(entityCardProps);

    screen.getByLabelText('basketball');
    screen.getByText('schoolabyApp.subject.physicalEducation');
    screen.getByText('Card title');
    screen.getByText('Max Payne');
    const teacherNameInitials = 'MP';
    screen.getByText(teacherNameInitials);
  });

  it('Should render EntityCard component with correct students count', () => {
    renderComponent(entityCardProps);

    const studentCount = '420';
    screen.getByText(studentCount);
  });

  it('Should render EntityCard component with date if not assignment card', () => {
    renderComponent(entityCardProps);

    screen.getByText('01.07.2020');
  });

  it('Should render EntityCard component with datetime if assignment card', () => {
    entityCardProps.isAssignment = true;
    renderComponent(entityCardProps);

    screen.getByText('01.07.2020 02:31');
  });

  it('Should render EntityCard component with date and time if assignment card', async () => {
    entityCardProps.dropdown = <MenuDropdown entityId={123} entityType={JOURNEY} entityTitle={'journey'} />;
    renderComponent(entityCardProps);

    await screen.findByLabelText('global.heading.dropdown');
  });
});
