import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { QueryClient, QueryClientProvider } from 'react-query';
import JourneyMobileMenu from 'app/shared/layout/mobile-menus/journey-mobile-menu/journey-mobile-menu';
import { AppContext } from 'app/app';
import { JourneyDetailContext } from 'app/journey/journey-detail/journey-detail-new';

jest.mock('react-router-dom', () => ({
  __esModule: true,
  ...jest.requireActual('react-router-dom'),
  useHistory: () => ({
    push: jest.fn(),
    location: {
      pathname: '/journey/1/',
    },
  }),
}));

describe('Journey mobile menu', () => {
  const queryClient = new QueryClient();
  const defaultProps = () => ({
    journey: {
      id: 1,
    },
    isAllowedToModify: true,
  });

  const renderComponent = props =>
    render(
      <AppContext.Provider value={{ setModal() {} }}>
        <QueryClientProvider client={queryClient}>
          <JourneyDetailContext.Provider value={props}>
            <JourneyMobileMenu journey={props.journey} />
          </JourneyDetailContext.Provider>
        </QueryClientProvider>
      </AppContext.Provider>
    );

  it('should display tabs and more options button', async () => {
    renderComponent(defaultProps());

    await screen.findByText('schoolabyApp.journey.detail.tabs.journey');
    await screen.findByText('schoolabyApp.journey.detail.tabs.grades');
    await screen.findByText('schoolabyApp.journey.detail.tabs.apps');
    await screen.findByText('global.heading.tabs.students');
    await screen.findByText('global.menu.more');
  });

  it('should display options', async () => {
    renderComponent(defaultProps());

    const moreOptionsButton = await screen.findByText('global.menu.more');
    fireEvent.click(moreOptionsButton);

    await screen.findByText('entity.action.edit');
    await screen.findByText('entity.action.delete');
    await screen.findByText('schoolabyApp.journey.detail.inviteStudents');
    await screen.findByText('schoolabyApp.journey.detail.viewLive');
  });

  it('should not show tabs if not allowed to modify', () => {
    const props = { ...defaultProps(), isAllowedToModify: false };
    renderComponent(props);

    expect(screen.queryByText('schoolabyApp.journey.detail.tabs.journey')).not.toBeInTheDocument();
    expect(screen.queryByText('schoolabyApp.journey.detail.tabs.grades')).not.toBeInTheDocument();
    expect(screen.queryByText('schoolabyApp.journey.detail.tabs.apps')).not.toBeInTheDocument();
    expect(screen.queryByText('global.heading.tabs.students')).not.toBeInTheDocument();
  });
});
