import './material-card-modal.scss';
import React from 'react';
import MaterialCardModal from 'app/shared/layout/material-card/material-card-modal';
import { fireEvent, render, screen } from '@testing-library/react';
import MockMaterialProvider from '../../../../helper/context-mock/mock-material-provider';
import { Material } from 'app/shared/model/material.model';

describe('MaterialCardModal', () => {
  const material = Material.instantiate({
    title: 'Material title',
    description: 'Test description',
  });

  const chosenMaterial = Material.instantiate({
    title: 'Chosen material title',
    description: 'Test description',
  });

  const renderComponent = materialCardProps =>
    render(
      <MockMaterialProvider materials={[chosenMaterial]}>
        <MaterialCardModal {...materialCardProps} />
      </MockMaterialProvider>
    );

  it('should render without buttons', () => {
    renderComponent({ toggleModal: jest.fn(), material });

    screen.getByText(material.title);
    screen.getByText(material.description);

    expect(screen.queryByText('global.form.removeMaterial')).toBeNull();
    expect(screen.queryByText('global.form.chooseMaterial')).toBeNull();
  });

  it('should render with choose material button when material is not chosen', () => {
    renderComponent({ toggleModal: jest.fn(), material, addable: true, removable: true });

    screen.getByText('global.form.chooseMaterial');
  });

  it('should render with remove material button when material is chosen', () => {
    renderComponent({ toggleModal: jest.fn(), material: chosenMaterial, addable: true, removable: true });

    screen.getByText('global.form.removeMaterial');
  });

  it('should call toggle', () => {
    const mockedToggle = jest.fn();
    renderComponent({ toggleModal: mockedToggle, material: chosenMaterial, addable: true, removable: true });

    const removeButton = screen.getByText('global.form.removeMaterial');

    fireEvent.click(removeButton);

    expect(mockedToggle).toHaveBeenCalled();
  });
});
