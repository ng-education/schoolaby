import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import UrlInput from 'app/shared/layout/url-input/url-input';

Object.defineProperty(navigator, 'clipboard', {
  value: {
    writeText() {},
  },
});

describe('UrlInput', () => {
  it('Should copy url when copy icon is pressed', () => {
    jest.spyOn(navigator.clipboard, 'writeText');
    render(<UrlInput buttonAction={'copy'} defaultValue={'default value'} />);
    fireEvent.click(screen.getByText('entity.action.copy'));
    expect(navigator.clipboard.writeText).toHaveBeenCalledWith('default value');
  });

  it('Should copy url when copy button is pressed', () => {
    jest.spyOn(navigator.clipboard, 'writeText');
    render(<UrlInput buttonAction={'copy'} defaultValue={'default value'} />);
    fireEvent.click(screen.getByRole('button'));
    expect(navigator.clipboard.writeText).toHaveBeenCalledWith('default value');
  });

  it('Should open new tab when open button is pressed', () => {
    window.open = jest.fn();
    render(<UrlInput buttonAction={'open'} defaultValue="eesti.ee" />);
    fireEvent.click(screen.getByRole('button'));
    expect(window.open).toBeCalledTimes(1);
  });
});
