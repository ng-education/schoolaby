import React from 'react';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import { QueryClient, QueryClientProvider } from 'react-query';
import configureMockStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { RootProvider } from 'app/shared/contexts/root-context';
import { Form } from 'react-final-form';
import GroupSearchDropdown from 'app/shared/layout/group-search-dropdown/group-search-dropdown';
import { EntityUpdateProvider } from 'app/shared/contexts/entity-update-context';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';

describe('Group search dropdown', () => {
  const queryClient = new QueryClient();
  const history = createBrowserHistory();

  const renderComponent = () => {
    return render(
      <RootProvider>
        <Provider store={configureMockStore()()}>
          <QueryClientProvider client={queryClient}>
            <EntityUpdateProvider>
              <Router history={history}>
                <Form onSubmit={() => {}}>{() => <GroupSearchDropdown disabled={false} />}</Form>
              </Router>
            </EntityUpdateProvider>
          </QueryClientProvider>
        </Provider>
      </RootProvider>
    );
  };

  it('Should render group assignments when typing', async () => {
    renderComponent();
    fireEvent.change(screen.getByRole('textbox'), { target: { value: 'assignment' } });

    await screen.findByText('Group assignment 1');
    expect(await screen.findAllByText(/2 schoolabyApp.assignment.groupsSelected/)).toHaveLength(2);
    expect(await screen.findAllByText(/schoolabyApp.assignment.createdDate/)).toHaveLength(2);
    await screen.findByText(/13.08.2021/);
    await screen.findByText(/15.08.2021/);
    await screen.findByText('Group assignment 2');
  });

  it('Should not render schools text when no results', async () => {
    renderComponent();
    fireEvent.change(screen.getByRole('textbox'), { target: { value: 'random text' } });

    await waitFor(() => {
      screen.getByText('entity.action.noResults');
    });
  });
});
