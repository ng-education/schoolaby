import React from 'react';
import { fireEvent, render, screen, waitFor, within } from '@testing-library/react';
import { FieldInputProps } from 'react-final-form';
import { EntityUpdateProvider } from 'app/shared/contexts/entity-update-context';
import { rest, server } from '../../../../rtl-setup';
import { IEducationalAlignment } from 'app/shared/model/educational-alignment.model';
import { QueryClient, QueryClientProvider } from 'react-query';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import EducationalAlignmentMultiselect from 'app/shared/layout/educational-alignment-multiselect/educational-alignment-multiselect';
import userEvent from '@testing-library/user-event';

describe('EducationalAlignmentMultiSelect', () => {
  const queryClient = new QueryClient();
  const mockStore = configureMockStore();
  let store;

  const educationalAlignments: IEducationalAlignment[] = [
    {
      id: 615,
      title: 'Arvsõna',
      alignmentType: 'subject',
      country: 'Estonia',
      targetName: 'Arvsõna',
      targetUrl: null,
      startDate: '2020-07-01T05:00:12Z',
      endDate: null,
      taxonId: 40191,
      children: [],
    },
    {
      id: 11,
      title: 'Kirjutamine',
      alignmentType: 'subject',
      country: 'Estonia',
      targetName: 'Kirjutamine',
      targetUrl: null,
      startDate: '2020-07-01T05:00:12Z',
      endDate: null,
      taxonId: 30001,
      children: [],
    },
  ];

  const renderComponent = (existingEducationalAlignments: IEducationalAlignment[] = []) => {
    const input: FieldInputProps<any, any> = {
      name: 'educationalAlignments',
      onChange() {},
      onBlur() {},
      onFocus() {},
      value: existingEducationalAlignments.length ? existingEducationalAlignments : [{}],
    };
    render(
      <EntityUpdateProvider>
        <Provider store={store}>
          <QueryClientProvider client={queryClient}>
            <EducationalAlignmentMultiselect input={input} meta={{ touched: false, error: undefined }} />
          </QueryClientProvider>
        </Provider>
      </EntityUpdateProvider>
    );
  };

  beforeEach(() => {
    const initialState = {
      authentication: {
        account: {
          country: 'Estonia',
        },
      },
    };
    store = mockStore(initialState);
  });

  it('should show placeholder when input value empty', () => {
    renderComponent();
    screen.getByPlaceholderText('schoolabyApp.educationalAlignment.detail.searchFromNationalCurriculum');
  });

  it('should show suggested educationalAlignments', async () => {
    renderComponent();

    fireEvent.change(screen.getByRole('textbox'), { target: { value: 'value' } });

    await waitFor(() => {
      screen.getByText('Kirjutamine');
      screen.getByText('Arvsõna');
    });
  });

  it('should show no results found text when no educationalAlignments found', async () => {
    renderComponent();

    server.use(
      rest.get('/api/educational-alignments', (req, res, ctx) => {
        return res(ctx.json([]));
      })
    );

    const textBox = screen.getByRole('textbox');
    fireEvent.change(textBox, { target: { value: 'value' } });
    fireEvent.focus(textBox);

    await waitFor(() => {
      screen.getByText('entity.action.noResults');
    });
  });

  it('should show selected educational alignments with existing educationalAlignment values', () => {
    renderComponent(educationalAlignments);

    screen.getByText('Kirjutamine');
    screen.getByText('Arvsõna');
  });

  it('should remove selected educational alignment when cross clicked', () => {
    renderComponent(educationalAlignments);

    const selectedAlignment = screen.getByText('Kirjutamine');
    const selectedAlignmentContainer = selectedAlignment.closest('div');
    const removeButton = within(selectedAlignmentContainer).getByRole('close-button');

    fireEvent.click(removeButton);

    screen.getByText('Arvsõna');
    expect(screen.queryByText('Kirjutamine')).not.toBeInTheDocument();
  });

  it('should select educational alignment when clicked in the dropdown', async () => {
    renderComponent();

    fireEvent.change(screen.getByRole('textbox'), { target: { value: 'value' } });

    const alignmentToSelect = await screen.findByRole('option', { name: 'Kirjutamine' });
    fireEvent.click(alignmentToSelect);

    const selectedAlignmentContainer = screen.getByText('Kirjutamine').closest('div');
    within(selectedAlignmentContainer).getByRole('close-button');
  });

  it('should not blur textbox when option clicked', async () => {
    renderComponent();

    const textbox = screen.getByRole('textbox');

    userEvent.click(textbox);
    userEvent.keyboard('value');

    const alignmentToSelect = await screen.findByRole('option', { name: 'Kirjutamine' });
    userEvent.click(alignmentToSelect);

    expect(textbox).toHaveFocus();
  });

  it('should not blur textbox when remove selected alignment clicked', () => {
    renderComponent(educationalAlignments);

    const textbox = screen.getByRole('textbox');
    userEvent.click(textbox);

    const selectedAlignment = screen.getByText('Kirjutamine');
    const selectedAlignmentContainer = selectedAlignment.closest('div');
    const removeButton = within(selectedAlignmentContainer).getByRole('close-button');

    userEvent.click(removeButton);

    expect(textbox).toHaveFocus();
  });
});
