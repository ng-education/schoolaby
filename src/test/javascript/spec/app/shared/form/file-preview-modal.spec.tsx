import React from 'react';
import configureMockStore from 'redux-mock-store';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import { AUTHORITIES } from 'app/config/constants';
import * as MockedUploadedFileContext from 'app/shared/contexts/uploaded-file-context';
import FilePreviewModal from 'app/shared/form/file-preview-modal';
import * as MockedFileUtils from 'app/shared/util/file-utils';
import MockEntityDetailProvider from '../../../helper/context-mock/mock-entity-detail-provider';
import { SubmissionGradingProvider } from 'app/shared/contexts/submission-grading-context';
import { QueryClientProvider, QueryClient } from 'react-query';
import ContextProvider from '../../../helper/context-mock/context-provider';
import { RootProvider } from 'app/shared/contexts/root-context';

describe('FilePreviewModal', () => {
  let store: any;
  const mockStore = configureMockStore();
  const queryClient = new QueryClient();

  beforeEach(() => {
    const initialState = {
      locale: {
        currentLocale: 'en',
      },
      authentication: {
        account: {
          authorities: [AUTHORITIES.STUDENT],
        },
      },
      applicationProfile: {
        inProduction: false,
      },
    };
    store = mockStore(initialState);
    window.URL.createObjectURL = jest.fn().mockImplementation();
  });

  afterEach(() => {
    jest.restoreAllMocks();
  });

  const pdfFile = {
    id: 1,
    uniqueName: 'unique_name_1',
    originalName: 'original_name_1',
    extension: 'pdf',
  };

  const pngFile = {
    id: 2,
    uniqueName: 'unique_name_2',
    originalName: 'original_name_2',
    extension: 'png',
  };

  const webmFile = {
    id: 3,
    uniqueName: 'unique_name_3',
    originalName: 'original_name_3',
    type: 'webm',
  };

  const mockPreviewFile = (file: any) =>
    jest.spyOn(MockedUploadedFileContext, 'usePreviewFileState').mockImplementation(() => {
      return {
        previewFile: file,
        setPreviewFile: jest.fn(),
      };
    });

  function component(files) {
    return (
      <RootProvider>
        <ContextProvider contextProvider={MockEntityDetailProvider} store={store}>
          <QueryClientProvider client={queryClient}>
            <SubmissionGradingProvider>
              <MockedUploadedFileContext.UploadedFileProvider>
                <FilePreviewModal files={files} canEdit={false} />
              </MockedUploadedFileContext.UploadedFileProvider>
            </SubmissionGradingProvider>
          </QueryClientProvider>
        </ContextProvider>
      </RootProvider>
    );
  }

  it('should render modal when previewFile is set', () => {
    mockPreviewFile(pdfFile);

    render(component([pdfFile]));

    const fileNames = screen.getAllByText(pdfFile.originalName);
    expect(fileNames).toHaveLength(2);
  });

  it('should not render modal when previewFile has default value', () => {
    render(component([pdfFile]));

    const fileName = screen.queryByText(pdfFile.originalName);
    expect(fileName).not.toBeInTheDocument();
  });

  it('should render iframe when file extension is .pdf', () => {
    mockPreviewFile(pdfFile);

    render(component([pdfFile]));

    const iframe = screen.getByRole('pdf-content');
    expect(iframe);
    const image = screen.queryByRole('img-content');
    expect(image).not.toBeInTheDocument();
  });

  it('should render images in detail and navigation slider when file extension is .png', () => {
    mockPreviewFile(pngFile);

    render(component([pngFile]));

    const images = screen.getAllByRole('img-content');
    expect(images).toHaveLength(2);
    const iframe = screen.queryByRole('pdf-content');
    expect(iframe).not.toBeInTheDocument();
  });

  it('should download file', async () => {
    const createDownloadLink = jest.spyOn(MockedFileUtils, 'createDownloadLink');
    mockPreviewFile(pdfFile);

    render(component([pdfFile]));
    const downloadButton = screen.getByRole('button', {
      name: /global.download/i,
    });
    fireEvent.click(downloadButton);

    await waitFor(() => expect(createDownloadLink).toHaveBeenCalled());
  });

  it('should not show webm info modal in Safari when there are no webm files', () => {
    jest.mock('react-device-detect', () => ({
      isSafari: true,
    }));

    mockPreviewFile(pdfFile);

    render(component([pdfFile]));

    expect(screen.queryByText('schoolabyApp.uploadedFile.webmInfo')).not.toBeInTheDocument();
  });

  it('should show webm info modal in Safari when there are webm files', () => {
    jest.mock('react-device-detect', () => ({
      isSafari: true,
    }));

    mockPreviewFile(webmFile);

    render(component([webmFile]));

    screen.findByText('schoolabyApp.uploadedFile.webmInfo');
    screen.findByText('global.dontShowAgain');
    screen.findByText('global.ok');
  });

  it('should not show webm info modal in other browsers when there are webm files', () => {
    mockPreviewFile(webmFile);

    render(component([webmFile]));

    expect(screen.queryByText('schoolabyApp.uploadedFile.webmInfo')).not.toBeInTheDocument();
  });
});
