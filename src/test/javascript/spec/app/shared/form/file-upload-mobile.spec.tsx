import React from 'react';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import configureMockStore from 'redux-mock-store';
import ContextProvider from '../../../helper/context-mock/context-provider';
import MockEntityDetailProvider from '../../../helper/context-mock/mock-entity-detail-provider';
import FileUpload from 'app/shared/form/file-upload';
import { IAssignment } from 'app/shared/model/assignment.model';
import { EntityState } from 'app/shared/model/enumerations/entity-state';
import { FileUploadProvider } from 'app/shared/form/file-upload-context';

jest.mock('react-device-detect', () => ({
  isMobile: true,
}));

describe('File upload mobile', function () {
  let store: any;
  const mockStore = configureMockStore();

  beforeEach(() => {
    const initialState = {
      locale: {
        currentLocale: 'en',
      },
      applicationProfile: {
        isInProduction: false,
      },
    };
    store = mockStore(initialState);
  });

  const assignment: IAssignment = {
    id: 11,
    state: EntityState.IN_PROGRESS,
  };

  const renderComponent = () =>
    render(
      <ContextProvider contextProvider={MockEntityDetailProvider} store={store} props={{ entity: assignment }}>
        <FileUploadProvider>
          <FileUpload existingFiles={[]} onUpload={() => undefined} onDelete={() => undefined} uploadDisabled={false} />
        </FileUploadProvider>
      </ContextProvider>
    );

  const openCameraModal = async () => {
    const cameraButton = await screen.findByText('global.upload.camera');
    fireEvent.click(cameraButton);
  };

  const closeCameraModal = async () => {
    const modalCloseButton = await screen.findByRole('close-button');
    fireEvent.click(modalCloseButton);
  };

  it('should show camera facing mode toggle button on mobile/tablet view', async () => {
    renderComponent();
    await openCameraModal();

    await screen.findByLabelText('schoolabyApp.submission.camera.toggleFacingMode');
  });

  it('should change camera recording button when capturing', async () => {
    renderComponent();
    await openCameraModal();

    const startRecordingButton = await screen.findByLabelText('schoolabyApp.submission.camera.startRecording');
    expect(screen.queryByLabelText('schoolabyApp.submission.camera.stopRecording')).not.toBeInTheDocument();
    fireEvent.click(startRecordingButton);

    const stopRecordingButton = await screen.findByLabelText('schoolabyApp.submission.camera.stopRecording');
    expect(screen.queryByLabelText('schoolabyApp.submission.camera.startRecording')).not.toBeInTheDocument();
    fireEvent.click(stopRecordingButton);

    await screen.findByLabelText('schoolabyApp.submission.camera.startRecording');
    expect(screen.queryByLabelText('schoolabyApp.submission.camera.stopRecording')).not.toBeInTheDocument();
  });

  it('should minimize modal on close if capturing', async () => {
    renderComponent();
    await openCameraModal();

    await screen.findByLabelText('schoolabyApp.submission.camera.toggleFacingMode');

    const startRecording = await screen.findByLabelText('schoolabyApp.submission.camera.startRecording');
    fireEvent.click(startRecording);

    await closeCameraModal();

    await screen.findByLabelText('schoolabyApp.submission.camera.close');
    await screen.findByLabelText('schoolabyApp.submission.camera.toggleFacingMode');
    expect(screen.queryByText('schoolabyApp.uploadedFile.cameraModal.title')).not.toBeInTheDocument();
    expect(screen.queryByText('global.upload.browse')).not.toBeInTheDocument();
    expect(screen.queryByText('global.upload.camera')).not.toBeInTheDocument();
  });

  it('should close modal if not capturing', async () => {
    renderComponent();
    await openCameraModal();
    await screen.findByLabelText('schoolabyApp.submission.camera.toggleFacingMode');
    await closeCameraModal();

    await screen.findByText('global.upload.browse');
    await screen.findByText('global.upload.camera');

    await waitFor(() => {
      expect(screen.queryByText('schoolabyApp.uploadedFile.cameraModal.title')).not.toBeInTheDocument();
    });
  });
});
