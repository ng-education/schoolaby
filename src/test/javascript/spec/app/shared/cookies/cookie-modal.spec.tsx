import CookieConsentModal from 'app/shared/cookies/cookie-consent-modal';
import { render, screen, fireEvent } from '@testing-library/react';
import React from 'react';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';

describe('CookieConsentModal', () => {
  const onAccept = jest.fn();
  const mockStore = configureMockStore();
  let store;

  beforeEach(() => {
    const initialState = {
      applicationProfile: {
        inProduction: false,
      },
    };

    store = mockStore(initialState);
  });

  const renderComponent = () => {
    render(
      <Provider store={store}>
        <CookieConsentModal onAccept={onAccept} />
      </Provider>
    );
  };

  it('Should render', () => {
    renderComponent();

    screen.getByText('global.cookieConsent');
    screen.getByText('global.termsAndPrivacy');

    fireEvent.click(screen.getByLabelText('global.necessaryCookies'));
    expect(screen.getByLabelText('global.necessaryCookies')).not.toBeChecked();

    fireEvent.click(screen.getByLabelText('global.analyticalCookies'));
    expect(screen.getByLabelText('global.analyticalCookies')).toBeChecked();

    fireEvent.click(screen.getByText('global.accept'));
    expect(onAccept).toHaveBeenCalled();
  });
});
