import React from 'react';

import { render, screen } from '@testing-library/react';
import { NoData } from 'app/shared/chart/no-data';

describe('NoData', () => {
  it('Should render text', () => {
    render(<NoData show={true} />);

    screen.getByText('schoolabyApp.assignment.analytics.barChart.noData');
  });

  it('Should not render text', () => {
    render(<NoData show={false} />);

    const text = screen.queryByText('schoolabyApp.assignment.analytics.barChart.noData');
    expect(text).toBeNull();
  });
});
