import { getExtension } from 'app/shared/util/file-utils';

describe('File utils', () => {
  describe('getExtension', () => {
    it('should return extension', () => {
      expect(getExtension('https://netgroup.com/resource.pdf')).toBe('pdf');
    });

    it('should return null if no extension', () => {
      expect(getExtension('https://netgroup.com')).toBeNull();
    });

    it('should ignore extension casing', () => {
      expect(getExtension('https://netgroup.com/resource.pDf')).toBe('pdf');
    });

    it('should handle multiple dots in url', () => {
      expect(getExtension('https://netgroup.com/resource.ext.pdf')).toBe('pdf');
    });

    it('should return null if url is null', () => {
      expect(getExtension(null)).toBeNull();
    });
  });
});
