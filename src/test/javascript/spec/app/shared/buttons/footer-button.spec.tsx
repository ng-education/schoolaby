import FooterButton from 'app/shared/layout/buttons/footer-button';
import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';

describe('FooterButton', () => {
  const defaultProps = {
    disabled: false,
    label: 'Test button',
    type: 'cancel',
  };

  const component = (props?) => {
    return <FooterButton {...defaultProps} {...props} />;
  };

  it('Should call onClick handler', () => {
    const mockedClickFn = jest.fn();
    render(component({ onClick: mockedClickFn }));

    const button = screen.getByText('Test button');

    fireEvent.click(button);

    expect(mockedClickFn).toHaveBeenCalled();
  });

  it('Should not call onClick handler when button is disabled', () => {
    const mockedClickFn = jest.fn();
    render(component({ onClick: mockedClickFn, disabled: true }));

    const button = screen.getByText('Test button');

    fireEvent.click(button);

    expect(mockedClickFn).not.toHaveBeenCalled();
  });
});
