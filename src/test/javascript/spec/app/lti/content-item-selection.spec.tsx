import { render, screen } from '@testing-library/react';
import React from 'react';
import configureMockStore from 'redux-mock-store';
import { AUTHORITIES } from 'app/config/constants';
import { ContentItemSelection } from 'app/lti/content-item-selection';

describe('ContentItemSelection', () => {
  let store: any;
  const mockStore = configureMockStore();

  beforeEach(() => {
    const initialState = {
      locale: {
        currentLocale: 'en',
      },
      authentication: {
        account: {
          authorities: [AUTHORITIES.TEACHER],
        },
      },
    };
    store = mockStore(initialState);
  });

  it('Should show failure text when not sucessful', () => {
    render(<ContentItemSelection />);
    screen.getByText('schoolabyApp.ltiContentItem.failure');
  });

  it('Should show success text when successful', () => {
    render(<ContentItemSelection successful />);
    screen.getByText('schoolabyApp.ltiContentItem.success');
  });
});
