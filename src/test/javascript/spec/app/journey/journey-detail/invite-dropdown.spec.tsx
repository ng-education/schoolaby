import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import InviteDropdown, { IInviteDropdown } from 'app/journey/journey-detail/invite-dropdown';
import { RootProvider } from 'app/shared/contexts/root-context';
import MockEntityDetailProvider from '../../../helper/context-mock/mock-entity-detail-provider';
import ContextProvider from '../../../helper/context-mock/context-provider';
import { AUTHORITIES } from 'app/config/constants';
import configureMockStore from 'redux-mock-store';

const mockStore = configureMockStore();

describe('InviteDropdown', () => {
  let store: any;

  beforeEach(() => {
    const initialState = {
      applicationProfile: {
        inProduction: false,
      },
      isAllowedToModify: true,
      locale: {
        currentLocale: 'en',
      },
      authentication: {
        account: {
          authorities: [AUTHORITIES.TEACHER],
        },
      },
    };
    store = mockStore(initialState);
  });

  const defaultProps: IInviteDropdown = {
    journey: {
      teacherSignupCode: 'teacherSignupCode',
      studentSignupCode: 'studentSignupCode',
    },
  };

  const renderComponent = (props?) =>
    render(
      <ContextProvider contextProvider={MockEntityDetailProvider} store={store} props={{}}>
        <RootProvider>
          <InviteDropdown {...props} />
        </RootProvider>
      </ContextProvider>
    );

  it('should render invite students and invite teachers when both signupCodes exist', () => {
    renderComponent(defaultProps);

    const inviteButton = screen.getByText('schoolabyApp.journey.detail.invite');

    fireEvent.click(inviteButton);

    screen.getByText('schoolabyApp.journey.detail.inviteStudents');
    screen.getByText('schoolabyApp.journey.detail.inviteTeachers');
  });

  it('should render only invite students when teacherSignupCode is missing', () => {
    renderComponent({
      ...defaultProps,
      journey: {
        studentSignupCode: 'studentSignupCode',
        teacherSignupCode: null,
      },
    });

    const inviteButton = screen.getByText('schoolabyApp.journey.detail.invite');

    fireEvent.click(inviteButton);

    screen.getByText('schoolabyApp.journey.detail.inviteStudents');
    expect(screen.queryByText('schoolabyApp.journey.detail.inviteTeachers')).toBeNull();
  });
});
