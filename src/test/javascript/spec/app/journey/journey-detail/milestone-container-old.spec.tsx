import { render, screen, fireEvent } from '@testing-library/react';
import React from 'react';
import { MemoryRouter } from 'react-router-dom';
import { MilestoneContext } from 'app/journey/journey-detail/journey-detail-old';
import { MilestoneContainerOld } from 'app/journey/journey-detail/journey-board/milestone/milestone-container-old';

const mockHistoryPush = jest.fn();

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useHistory: () => ({
    push: mockHistoryPush,
  }),
}));

describe('MilestoneContainerOld', () => {
  test('user can open material details in new tab', async () => {
    render(
      <MilestoneContext.Provider value={{ milestone: { id: 4, title: 'Test milestone' }, isAllowedToModify: false, journey: { id: 1 } }}>
        <MilestoneContainerOld />
      </MilestoneContext.Provider>,
      { wrapper: MemoryRouter }
    );

    fireEvent.click(await screen.findByText('Test milestone'));

    expect(mockHistoryPush).toBeCalledWith('/milestone/4?journeyId=1');
  });
});
