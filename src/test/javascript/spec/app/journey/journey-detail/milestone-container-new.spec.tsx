import { render, screen, fireEvent, within, waitFor } from '@testing-library/react';
import React from 'react';
import { MemoryRouter, Router } from 'react-router-dom';
import { MilestoneContext } from 'app/journey/journey-detail/journey-detail-new';
import { MilestoneContainerNew } from 'app/journey/journey-detail/journey-board/milestone/milestone-container-new';
import { createBrowserHistory } from 'history';
import { EntityUpdateProvider } from 'app/shared/contexts/entity-update-context';
import { QueryClientProvider, QueryClient } from 'react-query';

describe('MilestoneContainerNew', () => {
  const history = createBrowserHistory();
  const queryClient = new QueryClient();

  const component = (isAllowedToModify = true) =>
    render(
      <QueryClientProvider client={queryClient}>
        <Router history={history}>
          <EntityUpdateProvider>
            <MilestoneContext.Provider
              value={{
                milestone: { id: 4, title: 'New Milestone', endDate: '2021-12-03T11:21:26.788644Z' },
                isAllowedToModify,
                journey: { id: 1 },
                scrollToMilestoneId: 0,
              }}
            >
              <MilestoneContainerNew />
            </MilestoneContext.Provider>
          </EntityUpdateProvider>
        </Router>
      </QueryClientProvider>,
      { wrapper: MemoryRouter }
    );

  it('should render', () => {
    component();

    screen.getByText('New Milestone');
    expect(screen.getByText('global.deadline'));
    expect(screen.getByText('03.12.2021'));
  });

  it('should show edit button if user is allowed to modify', () => {
    component();
    screen.getByRole('button', { name: 'entity.action.edit' });
  });

  it('should not show edit button if user is not allowed to modify', () => {
    component(false);
    const editButton = screen.queryByRole('button', { name: 'entity.action.edit' });
    expect(editButton).not.toBeInTheDocument();
  });

  it('should open dropdown menu on edit button click', async () => {
    history.push = jest.fn();
    component();

    const editButton = screen.getByRole('button', { name: 'entity.action.edit' });

    await waitFor(() => {
      const menu = screen.queryByRole('menu');
      expect(menu).not.toBeInTheDocument();
    });

    fireEvent.click(editButton);

    const menu = screen.getByRole('menu');
    within(menu).getByRole('menuitem', { name: 'entity.action.edit' });
    within(menu).getByRole('menuitem', { name: /entity.action.delete/ });
  });
});
