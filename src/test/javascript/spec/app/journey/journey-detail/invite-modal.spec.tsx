import React from 'react';
import { render, screen } from '@testing-library/react';
import InviteModal, { InviteModalProps } from 'app/journey/journey-detail/invite-modal/invite-modal';

describe('Invite modal', () => {
  const defaultProps: InviteModalProps = {
    journey: {
      teacherSignupCode: 'teacherSignupCode',
      studentSignupCode: 'studentSignupCode',
    },
    modal: true,
    toggle() {},
    type: 'STUDENT',
  };

  const renderComponent = (props?) => render(<InviteModal {...props} />);

  it('should display invite modal contents for student', async () => {
    renderComponent({
      ...defaultProps,
      type: 'STUDENT',
    });

    await screen.findByLabelText('schoolabyApp.journey.signUpCode');
    await screen.findByDisplayValue('studentSignupCode');
    await screen.findByText('global.or');
    await screen.findByLabelText('schoolabyApp.journey.sharableLink');
    await screen.findByDisplayValue(`http://localhost/journey/join?signUpCode=studentSignupCode`);
    expect(await screen.findAllByText('entity.action.copy')).toHaveLength(2);
  });

  it('should display invite modal contents for teacher', async () => {
    renderComponent({
      ...defaultProps,
      type: 'TEACHER',
    });

    await screen.findByLabelText('schoolabyApp.journey.signUpCode');
    await screen.findByDisplayValue('teacherSignupCode');
    await screen.findByText('global.or');
    await screen.findByLabelText('schoolabyApp.journey.sharableLink');
    await screen.findByDisplayValue(`http://localhost/journey/join?signUpCode=teacherSignupCode`);
    expect(await screen.findAllByText('entity.action.copy')).toHaveLength(2);
  });
});
