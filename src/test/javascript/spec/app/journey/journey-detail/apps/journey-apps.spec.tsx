import { render, screen, waitFor, within } from '@testing-library/react';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import React from 'react';
import { ThroughProvider } from 'react-through';
import configureMockStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { AUTHORITIES } from 'app/config/constants';
import { RootProvider } from 'app/shared/contexts/root-context';
import { IJourneyAppsProps, JourneyAppsView } from 'app/journey/journey-detail/apps/journey-apps-view';
import { QueryClient, QueryClientProvider } from 'react-query';

describe('JourneyApps', () => {
  let store: any;
  const history = createBrowserHistory();
  const mockStore = configureMockStore();
  const queryClient = new QueryClient();

  const journeyAppsProps = (): IJourneyAppsProps => {
    return {
      history: undefined,
      location: undefined,
      match: {
        params: {
          id: '1',
        },
        isExact: true,
        path: 'path',
        url: 'url',
      },
    };
  };

  beforeEach(() => {
    const initialState = {
      locale: {
        currentLocale: 'en',
      },
      authentication: {
        account: {
          authorities: [AUTHORITIES.TEACHER],
        },
      },
      applicationProfile: {
        inProduction: false,
      },
    };
    store = mockStore(initialState);
  });

  const component = () => {
    return (
      <RootProvider>
        <Provider store={store}>
          <ThroughProvider>
            <QueryClientProvider client={queryClient}>
              <Router history={history}>
                <JourneyAppsView {...journeyAppsProps()} />
              </Router>
            </QueryClientProvider>
          </ThroughProvider>
        </Provider>
      </RootProvider>
    );
  };

  it('Should show MyJourneysBreadcrumbItem and JourneyTitleBreadcrumbItem and AppsBreadcrumbItem', async () => {
    render(component());
    await waitFor(() => {
      const breadcrumbs = screen.getByLabelText('breadcrumb');
      const myJourneysBreadcrumbItem = within(breadcrumbs).getByText('global.menu.journeys');
      const journeyTitleBreadcrumbItem = within(breadcrumbs).getByText('Journey title');
      const appsBreadcrumbItem = within(breadcrumbs).getByText('schoolabyApp.journey.detail.tabs.apps');

      expect(myJourneysBreadcrumbItem).toHaveAttribute('href', '/journey');
      expect(journeyTitleBreadcrumbItem).toHaveAttribute('href', '/journey/1');
      expect(appsBreadcrumbItem).toHaveAttribute('href', '/journey/1/apps');
    });
  });

  it('Should show only one dropdown based on config type', async () => {
    render(component());
    expect(await screen.findAllByLabelText('global.heading.dropdown')).toHaveLength(1);
  });
});
