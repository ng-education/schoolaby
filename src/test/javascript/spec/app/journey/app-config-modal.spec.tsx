import React from 'react';
import { ILtiApp } from 'app/shared/model/lti-app.model';
import { ILtiConfig } from 'app/shared/model/lti-config.model';
import { RootProvider } from 'app/shared/contexts/root-context';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import { AUTHORITIES } from 'app/config/constants';
import { render, screen } from '@testing-library/react';
import { IAppConfigModal, AppConfigModal } from 'app/journey/journey-detail/app-config-modal/app-config-modal';

describe('AppConfigModal', () => {
  let store;
  const mockStore = configureMockStore();

  const ltiApp: ILtiApp = {
    id: 1,
    name: 'Test app',
    description: 'Test description',
    launchUrl: 'http://test.url',
    imageUrl: 'http://test.url',
    version: 'version',
    clientId: null,
  };

  const ltiConfig: ILtiConfig = {
    id: 1,
    consumerKey: 'Test consumer key',
    sharedSecret: 'Test shared secret',
    deploymentId: null,
    ltiApp,
    type: 'JOURNEY',
  };

  const appConfigModalProps: IAppConfigModal = {
    toggle() {},
    showModal: true,
    onSubmit() {},
    ltiApp,
  };

  beforeEach(() => {
    const initialState = {
      locale: {
        currentLocale: 'en',
      },
      authentication: {
        account: {
          authorities: [AUTHORITIES.TEACHER],
        },
      },
    };
    store = mockStore(initialState);
  });

  const component = () => {
    return (
      <RootProvider>
        <Provider store={store}>
          <AppConfigModal {...appConfigModalProps} />
        </Provider>
      </RootProvider>
    );
  };

  it('Renders component for creating new app config', () => {
    appConfigModalProps.ltiConfig = null;
    render(component());

    const name = screen.getByText(ltiApp.name);
    expect(name);
    const image = screen.getByRole('img', { name: ltiApp.name });
    expect(image);
    const description = screen.getByText(ltiApp.description);
    expect(description);

    const consumerKey = screen.getByRole('textbox', { name: /schoolabyApp.journey.app.register.consumerKey/i });
    expect(consumerKey).toHaveValue('');
    const sharedSecret = screen.getByRole('textbox', { name: /schoolabyApp.journey.app.register.sharedSecret/i });
    expect(sharedSecret).toHaveValue('');
  });

  it('Renders component for updating config', () => {
    appConfigModalProps.ltiConfig = ltiConfig;
    render(component());

    const name = screen.getByText(ltiApp.name);
    expect(name);
    const image = screen.getByRole('img', { name: ltiApp.name });
    expect(image);
    const description = screen.getByText(ltiApp.description);
    expect(description);

    const consumerKey = screen.getByRole('textbox', { name: /schoolabyApp.journey.app.register.consumerKey/i });
    expect(consumerKey).toHaveValue(ltiConfig.consumerKey);
    const sharedSecret = screen.getByRole('textbox', { name: /schoolabyApp.journey.app.register.sharedSecret/i });
    expect(sharedSecret).toHaveValue(ltiConfig.sharedSecret);
  });

  it('Should have readonly fields and no button when lti config type is PLATFORM', () => {
    appConfigModalProps.ltiConfig.type = 'PLATFORM';
    render(component());

    const consumerKeyField = screen.getByRole('textbox', { name: /schoolabyApp.journey.app.register.consumerKey/i });
    expect(consumerKeyField).toBeDisabled();
    const sharedSecretField = screen.getByRole('textbox', { name: /schoolabyApp.journey.app.register.sharedSecret/i });
    expect(sharedSecretField).toBeDisabled();
    const submitButton = screen.queryByText('schoolabyApp.journey.app.register.update');
    expect(submitButton).toBeNull();
  });

  it('Should have writeable fields and button when lti config type is JOURNEY', () => {
    appConfigModalProps.ltiConfig.type = 'JOURNEY';
    render(component());

    const consumerKeyField = screen.getByRole('textbox', { name: /schoolabyApp.journey.app.register.consumerKey/i });
    expect(consumerKeyField).not.toBeDisabled();
    const sharedSecretField = screen.getByRole('textbox', { name: /schoolabyApp.journey.app.register.sharedSecret/i });
    expect(sharedSecretField).not.toBeDisabled();
    const submitButton = screen.queryByText('schoolabyApp.journey.app.register.update');
    expect(submitButton).toBeDefined();
  });
});
