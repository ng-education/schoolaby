import { render, screen, waitFor, within, fireEvent } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import React from 'react';
import { ThroughProvider } from 'react-through';
import configureMockStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { AUTHORITIES } from 'app/config/constants';
import { RootProvider } from 'app/shared/contexts/root-context';
import { QueryClient, QueryClientProvider } from 'react-query';
import { IMilestoneDetailProps, MilestoneDetail } from 'app/milestone/milestone-detail';
import { EntityDetailProvider } from 'app/shared/contexts/entity-detail-context';
import { EntityUpdateProvider } from 'app/shared/contexts/entity-update-context';
import { MaterialProvider } from 'app/shared/contexts/material-context';

describe('MilestoneDetail', () => {
  let store: any;
  const mockStore = configureMockStore();
  const queryClient = new QueryClient();

  const milestoneDetailProps = (): IMilestoneDetailProps => {
    return {
      history: undefined,
      location: {
        pathname: undefined,
        search: 'journeyId=1',
        state: undefined,
        hash: undefined,
      },
      match: {
        params: {
          id: '1',
        },
        isExact: true,
        path: 'path',
        url: 'url',
      },
      isTeacherOrAdmin: true,
      account: {
        authorities: ['ROLE_TEACHER'],
        email: 'student@localhost.com',
        firstName: 'teacher',
        id: 7,
        lastName: 'teacher',
      },
    };
  };

  beforeEach(() => {
    const initialState = {
      locale: {
        currentLocale: 'en',
      },
      authentication: {
        account: {
          authorities: [AUTHORITIES.TEACHER],
        },
      },
    };
    store = mockStore(initialState);
  });

  const renderComponent = (detailProps: IMilestoneDetailProps) =>
    render(
      <RootProvider>
        <Provider store={store}>
          <EntityDetailProvider>
            <EntityUpdateProvider>
              <MaterialProvider>
                <ThroughProvider>
                  <QueryClientProvider client={queryClient}>
                    <MemoryRouter initialEntries={['/milestone/1?journeyId=1']}>
                      <MilestoneDetail {...detailProps} />
                    </MemoryRouter>
                  </QueryClientProvider>
                </ThroughProvider>
              </MaterialProvider>
            </EntityUpdateProvider>
          </EntityDetailProvider>
        </Provider>
      </RootProvider>
    );

  it('should show MyJourneysBreadcrumbItem and JourneyTitleBreadcrumbItem and MilestoneTitleBreadcrumbItem', async () => {
    renderComponent(milestoneDetailProps());
    await waitFor(() => {
      const breadcrumbs = screen.getByLabelText('breadcrumb');
      const myJourneysBreadcrumbItem = within(breadcrumbs).getByText('global.menu.journeys');
      const journeyTitleBreadcrumbItem = within(breadcrumbs).getByText('Journey title');
      const milestoneTitleBreadcrumbItem = within(breadcrumbs).getByText('Milestone title');

      expect(myJourneysBreadcrumbItem).toHaveAttribute('href', '/journey');
      expect(journeyTitleBreadcrumbItem).toHaveAttribute('href', '/journey/1');
      expect(milestoneTitleBreadcrumbItem).toHaveAttribute('href', '/milestone/1?journeyId=1');
    });
  });

  it('should not display breadcrumb and actions menu button for journey template previewers', async () => {
    const detailProps = milestoneDetailProps();
    detailProps.milestoneId = 1;
    detailProps.previewEnabled = true;
    detailProps.closeMilestone = () => {};

    renderComponent(detailProps);

    await waitFor(() => {
      expect(screen.queryByLabelText('global.heading.dropdown')).not.toBeInTheDocument();
      expect(screen.queryByLabelText('breadcrumb')).not.toBeInTheDocument();
      expect(screen.queryByText('global.menu.journeys')).not.toBeInTheDocument();
      expect(screen.queryByText('Journey title')).not.toBeInTheDocument();
    });
  });

  it('should dipslay back button when previewing a journey template', async () => {
    const detailProps = milestoneDetailProps();
    detailProps.milestoneId = 1;
    detailProps.previewEnabled = true;
    detailProps.closeMilestone = () => {};

    renderComponent(detailProps);

    await screen.findByText('entity.action.back');
  });

  it('should open milestone edit form', async () => {
    renderComponent(milestoneDetailProps());

    fireEvent.click(await screen.findByText('entity.dropdown.edit'));
    fireEvent.click(screen.getByText('entity.action.edit'));
    screen.getByText('entity.edit.milestone');
    expect(screen.getByLabelText('schoolabyApp.milestone.title')).toHaveValue('Milestone title');
  });
});
