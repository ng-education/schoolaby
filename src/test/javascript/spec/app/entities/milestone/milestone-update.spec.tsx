import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { RootProvider } from 'app/shared/contexts/root-context';
import { MaterialProvider } from 'app/shared/contexts/material-context';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import { QueryClient, QueryClientProvider } from 'react-query';
import MilestoneUpdate from 'app/milestone/milestone-update';
import MockEntityUpdateProvider, { IMockEntityUpdateProvider } from '../../../helper/context-mock/mock-entity-update-provider';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import { COUNTRY } from 'app/config/constants';

jest.mock('react-router-dom', () => ({
  __esModule: true,
  ...jest.requireActual('react-router-dom'),
  useHistory: () => ({
    push: jest.fn(),
  }),
  useLocation: () => ({
    search: 'journeyId=1',
  }),
}));

describe('Milestone update', () => {
  const history = createBrowserHistory();
  const mockStore = configureMockStore();

  const getStore = () => ({
    locale: {
      currentLocale: 'en',
    },
    applicationProfile: {
      inProduction: true,
    },
    authentication: {
      account: {
        country: 'Estonia',
      },
    },
  });

  const getUpdateProviderProps = () => ({
    milestoneUpdateId: 1,
  });

  const renderComponent = (updateProviderProps: IMockEntityUpdateProvider, store = getStore()) =>
    render(
      <Router history={history}>
        <RootProvider>
          <Provider store={mockStore(store)}>
            <QueryClientProvider client={new QueryClient()}>
              <MockEntityUpdateProvider selectedEducationalAlignments={[]} {...updateProviderProps}>
                <MaterialProvider>
                  <MilestoneUpdate />
                </MaterialProvider>
              </MockEntityUpdateProvider>
            </QueryClientProvider>
          </Provider>
        </RootProvider>
      </Router>
    );

  it('should display milestone form', async () => {
    const updateProviderProps = getUpdateProviderProps();
    updateProviderProps.milestoneUpdateId = undefined;
    renderComponent(updateProviderProps);

    await screen.findByText('entity.add.milestone');
    await screen.findByLabelText('schoolabyApp.milestone.title');
    await screen.findByPlaceholderText('schoolabyApp.milestone.titlePlaceholder');
    await screen.findByLabelText('schoolabyApp.milestone.description');
    await screen.findByLabelText('schoolabyApp.milestone.deadline');
    await screen.findByText('schoolabyApp.educationalAlignment.detail.title');
    expect(screen.queryByLabelText('entity.publishedState.published')).toBeChecked();
    expect(screen.queryByLabelText('entity.publishedState.unpublished')).not.toBeChecked();
    expect(screen.queryByLabelText('entity.publishedState.publishAt')).not.toBeChecked();
    await screen.findByText('entity.action.cancel');
    await screen.findByText('entity.action.save');
  });

  it('should display milestone edit title when milestoneUpdateId exists', async () => {
    renderComponent(getUpdateProviderProps());

    await screen.findByText('entity.edit.milestone');
  });

  it('should display validation errors', async () => {
    const updateProviderProps = getUpdateProviderProps();
    updateProviderProps.milestoneUpdateId = undefined;
    renderComponent(updateProviderProps);

    fireEvent.click(await screen.findByText('entity.action.save'));

    await screen.findByText('entity.validation.required');
    await screen.findByText('schoolabyApp.educationalAlignment.errors.inputSelector');
  });

  it('should make milestone educational alignments optional if user is Ukrainian', async () => {
    const updateProviderProps = { ...getUpdateProviderProps(), milestoneUpdateId: undefined };
    const store = getStore();
    store.authentication.account.country = COUNTRY.UKRAINE;
    renderComponent(updateProviderProps, store);

    const submitButton = await screen.findByText('entity.action.save');
    fireEvent.click(submitButton);

    await screen.findByText('entity.validation.required');
    expect(screen.queryByText('schoolabyApp.educationalAlignment.errors.inputSelector')).not.toBeInTheDocument();
  });
});
