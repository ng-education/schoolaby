import React from 'react';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import configureMockStore from 'redux-mock-store';
import { AUTHORITIES } from 'app/config/constants';
import { createBrowserHistory } from 'history';
import { Conversations, IConversations } from 'app/conversations/conversations';
import { IUser } from 'app/shared/model/user.model';
import { RootProvider } from 'app/shared/contexts/root-context';
import { ThroughProvider } from 'react-through';
import { IChat } from 'app/shared/model/chat.model';
import ContextProvider from '../../../helper/context-mock/context-provider';
import MockChatProvider, { IMockChatProvider } from '../../../helper/context-mock/mock-chat-provider';
import { QueryClient, QueryClientProvider } from 'react-query';
import userEvent from '@testing-library/user-event';

const mockStudent: IUser = {
  firstName: 'student first name',
  lastName: 'student last name',
  id: 123,
  authorities: [AUTHORITIES.STUDENT],
};

const mockTeacher: IUser = {
  firstName: 'teacher first name',
  lastName: 'teacher last name',
  id: 1234,
  authorities: [AUTHORITIES.TEACHER],
};

const mockChat: IChat = {
  journeyId: 1,
  submissionId: 6,
  people: [mockStudent, mockTeacher],
};

const mockStore = configureMockStore();
const history = createBrowserHistory();

describe('Conversations', () => {
  let store: any;
  const queryClient = new QueryClient();

  beforeEach(() => {
    Element.prototype.scrollIntoView = jest.fn();
    const initialState = {
      locale: {
        currentLocale: 'et',
      },
      authentication: {
        account: mockStudent,
      },
    };
    store = mockStore(initialState);
  });

  const props: IConversations = {
    history,
    location: {
      pathname: undefined,
      search: undefined,
      state: undefined,
      hash: undefined,
    },
    match: {
      params: {
        url: '',
      },
      isExact: true,
      path: 'path',
      url: 'url',
    },
    account: mockStudent,
    isStudent: false,
  };

  const mockChatProviderProps: IMockChatProvider = {
    chat: mockChat,
  };

  const renderComponent = () => {
    return {
      ...render(
        <RootProvider>
          <ContextProvider contextProvider={MockChatProvider} routeProps={{ history }} store={store} props={mockChatProviderProps}>
            <QueryClientProvider client={queryClient}>
              <ThroughProvider>
                <Conversations {...props} isStudent={false} />
              </ThroughProvider>
            </QueryClientProvider>
          </ContextProvider>
        </RootProvider>
      ),
      store,
    };
  };

  it('Should render submission conversation with message', async () => {
    const { container } = renderComponent();
    let conversation = undefined;
    await screen.findByText('Journey title');
    await waitFor(() => {
      conversation = container.querySelector('div[class="list-box-wrapper d-flex"]');
    });
    fireEvent.click(conversation);
    await screen.findAllByText('FL');
    await screen.findAllByText('FirstName2 LastName2');

    await screen.findAllByText('assignment title');
    await screen.findByRole('button');
    await screen.findByText('message value');
  });

  it('Should always display journey message', async () => {
    history.push('/chats?journeyId=1');
    const { container } = renderComponent();
    let conversation = undefined;
    await waitFor(() => {
      screen.getAllByText('Journey title');
      screen.getAllByText('schoolabyApp.chat.generalChat');
      conversation = container.querySelector('div[class="selected list-box-wrapper d-flex"]');
    });

    fireEvent.click(conversation);
    await waitFor(async () => {
      screen.getAllByText('assignment title');
      screen.getByRole('button');
      await waitFor(() => {
        screen.getByText('message value');
      });
    });
  });

  it('Should render custom generated chat from ChatProvider', async () => {
    history.push(`/chats?journeyId=${mockChat.journeyId}&submissionId=${mockChat.submissionId}`);
    renderComponent();

    await screen.findByText('FirstName2 LastName2 (Assignment title)');

    await waitFor(() => {
      const journeyElement = screen.getByText('Journey title');
      userEvent.click(journeyElement);
    });

    await waitFor(() => {
      screen.getAllByText('assignment title');
    });
  });

  it('Should change URL on click journey selection and activate journey chat', async () => {
    history.push(`/chats`);
    history.push = jest.fn();
    renderComponent();
    let journey = undefined;

    await waitFor(() => {
      journey = screen.getByText('Journey title');
    });
    fireEvent.click(journey);
    await waitFor(() => {
      expect(history.push).toBeCalledWith(`/chats?journeyId=1`);
    });
  });

  it('Should change URL when navigating between chats', async () => {
    history.push(`/chats`);
    history.push = jest.fn();

    renderComponent();
    let journey = undefined;
    let chat = undefined;
    await waitFor(() => {
      journey = screen.getByText('Journey title');
    });
    fireEvent.click(journey);
    await waitFor(() => {
      expect(history.push).toBeCalledWith(`/chats?journeyId=${mockChat.journeyId}`);
    });
    await waitFor(() => {
      chat = screen.getAllByText('FirstName2 LastName2')[0];
    });
    fireEvent.click(chat);
    await waitFor(() => {
      expect(history.push).toBeCalledWith(`/chats?journeyId=${mockChat.journeyId}&submissionId=6`);
    });
  });
});
