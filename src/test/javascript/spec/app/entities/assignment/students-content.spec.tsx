import React from 'react';
import { QueryClient, QueryClientProvider } from 'react-query';
import { SubmissionGradingProvider } from 'app/shared/contexts/submission-grading-context';
import { fireEvent, render, screen, within, waitFor } from '@testing-library/react';
import SubmissionsContent from 'app/assignment/assignment-detail/students/submissions-content';
import { IAssignment } from 'app/shared/model/assignment.model';
import { EntityDetailProvider } from 'app/shared/contexts/entity-detail-context';
import { AssignmentContext } from 'app/journey/journey-detail/journey-detail-new';
import { UploadedFileProvider } from 'app/shared/contexts/uploaded-file-context';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import { RootProvider } from 'app/shared/contexts/root-context';
import { EntityUpdateProvider } from 'app/shared/contexts/entity-update-context';
import { IRubric } from 'app/shared/model/rubric/rubric.model';
import { createRubric } from 'app/shared/util/rubric';

jest.mock('react-router-dom', () => ({
  __esModule: true,
  ...jest.requireActual('react-router-dom'),
  useHistory: () => ({
    replace: jest.fn(),
  }),
  useLocation: () => ({
    search: '',
  }),
}));

describe('Assignment students view', () => {
  const mockStore = configureMockStore();
  const queryClient = new QueryClient();

  const getDefaultAssignment = (): IAssignment => ({
    id: 1234,
    journeyId: 2,
    title: 'Assignment title',
    students: [
      {
        id: 5,
        firstName: 'Student1',
        lastName: 'Student2',
      },
      {
        id: 6,
        firstName: 'Student2',
        lastName: 'Student1',
      },
    ],
  });

  const renderComponent = (assignment: IAssignment, rubric?: IRubric) =>
    render(
      <RootProvider>
        <Provider
          store={mockStore({
            authentication: {
              account: {
                authorities: [],
              },
            },
            locale: {
              currentLocale: 'et',
            },
          })}
        >
          <QueryClientProvider client={queryClient}>
            <EntityDetailProvider>
              <EntityUpdateProvider>
                <SubmissionGradingProvider>
                  <UploadedFileProvider>
                    <AssignmentContext.Provider value={{ assignment, isAllowedToModify: true, journey: undefined, rubric }}>
                      <SubmissionsContent />
                    </AssignmentContext.Provider>
                  </UploadedFileProvider>
                </SubmissionGradingProvider>
              </EntityUpdateProvider>
            </EntityDetailProvider>
          </QueryClientProvider>
        </Provider>
      </RootProvider>
    );

  it('should display students view content', async () => {
    renderComponent(getDefaultAssignment());

    await screen.findByText('Assignment title');
    await screen.findByText('schoolabyApp.assignment.member.students');
    await screen.findByText('schoolabyApp.assignment.detail.sortBy');
    await screen.findByText('global.student');
    await screen.findByText('schoolabyApp.assignment.detail.grade');
    await screen.findByText('schoolabyApp.assignment.detail.submitted');
  });

  it('should display students in list', async () => {
    renderComponent(getDefaultAssignment());

    expect(await screen.findAllByText('SS')).toHaveLength(2);
    await screen.findByText('Student1 Student2');
    await screen.findByText('Student2 Student1');
    await screen.findByText('A');
    await screen.findByText(/03.02.2022/);
  });

  it('should sort students by name', async () => {
    renderComponent(getDefaultAssignment());

    const sortedByFirstName = await screen.findAllByTestId('user-fullname');
    expect(sortedByFirstName).toHaveLength(2);
    within(sortedByFirstName[0]).getByText('Student1 Student2');
    within(sortedByFirstName[1]).getByText('Student2 Student1');

    const sortButton = screen.getByText('schoolabyApp.assignment.detail.sortBy');
    fireEvent.click(sortButton);
    screen.getByText('schoolabyApp.assignment.detail.sortByFirstName');
    fireEvent.click(screen.getByText('schoolabyApp.assignment.detail.sortByLastName'));

    const sortedByLastName = screen.getAllByTestId('user-fullname');
    within(sortedByLastName[0]).getByText('Student2 Student1');
    within(sortedByLastName[1]).getByText('Student1 Student2');
  });

  it('should indicate teacher to select a student for feedback', async () => {
    renderComponent(getDefaultAssignment());

    await screen.findByText('schoolabyApp.assignment.detail.selectStudent');
  });

  it('should display invite students when no students in assignment nor in journey', async () => {
    const assignment = getDefaultAssignment();
    assignment.students = [];
    renderComponent(assignment);

    await screen.findByText('schoolabyApp.journey.detail.inviteStudents');
  });

  it('should display selected student submission', async () => {
    const assignment = getDefaultAssignment();
    renderComponent(assignment);

    await waitFor(() => {
      fireEvent.click(screen.getByText('Student1 Student2'));
    });

    await screen.findByText('schoolabyApp.submission.submissionInfo.studentNameSubmission Student1 Student2');
    await screen.findByText('value');
    await screen.findByText('schoolabyApp.submission.submissionInfo.studentNameUploads Student1 Student2');
    await screen.findByText('schoolabyApp.submission.testResults');
    await screen.findByText('schoolabyApp.submission.submittedLTI.asset');
    await screen.findByText('schoolabyApp.submission.submittedLTI.score');
    await screen.findByText('schoolabyApp.submission.submittedLTI.scoreDate');
    await screen.findByText('schoolabyApp.submission.submittedLTI.launches');
    await screen.findByText('LTI Test title (launch)');
    await screen.findByText('50%');
    await screen.findByText(/5.11.2020/);
    await screen.findByText('1');
  });

  it('should display no submission text', async () => {
    const assignment = getDefaultAssignment();
    renderComponent(assignment);

    await waitFor(() => {
      fireEvent.click(screen.getByText('Student2 Student1'));
    });

    await screen.findByText('schoolabyApp.submission.noSubmission');
  });

  it('should display files uploaded by students', async () => {
    renderComponent(getDefaultAssignment());

    fireEvent.click(await screen.findByText('Student1 Student2'));

    const studentSolution = await screen.findByTestId('student-solution');

    // Test does not know how wide the file grid is, for this reason it shows initially only 2 files,
    // rest can be seen by clicking on the last grid item, with the number of the remaining files
    const initialFileGridItems = await within(studentSolution).findAllByTestId('file-grid-item');
    expect(initialFileGridItems).toHaveLength(2);

    const loadMore = await screen.findByText('+ 4');
    fireEvent.click(loadMore);

    const fileGridItems = await within(studentSolution).findAllByTestId('file-grid-item');
    expect(fileGridItems).toHaveLength(6);

    const wordDoc = await screen.findByText('test.doc');
    within(wordDoc.closest('div')).getByText('schoolabyApp.uploadedFile.extensionName.word');

    const excelDoc = await screen.findByText('test.xls');
    within(excelDoc.closest('div')).getByText('schoolabyApp.uploadedFile.extensionName.excel');

    const pdfDoc = await screen.findByText('test.pdf');
    within(pdfDoc.closest('div')).getByText('schoolabyApp.uploadedFile.extensionName.pdf');

    const generic = await screen.findByText('generic.sh');
    within(generic.closest('div')).getByText('schoolabyApp.uploadedFile.extensionName.generic');

    expect(screen.queryByText('test.png')).not.toBeInTheDocument();
    expect(screen.queryByText('test.mp4')).not.toBeInTheDocument();

    await screen.findByAltText('test.png');
    await screen.findByAltText('test.png');
  });

  it('should display feedback history of a student', async () => {
    const assignment = getDefaultAssignment();
    renderComponent(assignment);

    await waitFor(() => {
      fireEvent.click(screen.getByText('Student1 Student2'));
    });

    expect(await screen.findAllByText(/schoolabyApp.submission.feedbackGiven/)).toHaveLength(2);
    await screen.findByText(/04.02.2022/);
    await screen.findByText(/05.02.2022/);
    await screen.findByText(/schoolabyApp\.assignment\.detail\.grade\W+A/);
    await screen.findByText(/schoolabyApp\.assignment\.detail\.grade\W+B/);
    await screen.findByText('Feedback value');
    await screen.findByText('Feedback value 2');
    await screen.findByText('Feedback file 1');
    await screen.findByText('Feedback file 2');
  });

  it('should display rubric points when teacher has selected criterion levels in a feedback', async () => {
    const assignment = getDefaultAssignment();
    const rubric = createRubric();
    renderComponent(assignment, rubric);

    await waitFor(() => {
      fireEvent.click(screen.getByText('Student1 Student2'));
    });

    expect(await screen.findAllByText(/schoolabyApp.submission.feedbackGiven/)).toHaveLength(2);
    await screen.findByText('schoolabyApp.assignment.detail.criterion');
    await screen.findByText('schoolabyApp.assignment.detail.points');

    await screen.findByText('Criterion 1');
    await screen.findByText('5/5');
    await screen.findByText('Criterion 2');
    await screen.findByText('1/3');
    await screen.findByText('schoolabyApp.assignment.rubric.assessmentRubricTotalPoints');
    await screen.findByText('6/8');
  });
});
