import React from 'react';
import configureMockStore from 'redux-mock-store';
import { render, screen, waitFor } from '@testing-library/react';
import { Provider } from 'react-redux';
import { Router } from 'react-router-dom';
import { QueryClient, QueryClientProvider } from 'react-query';
import { createBrowserHistory } from 'history';
import StudentAssignmentView from 'app/assignment/assignment-detail/students/student-assignment-view/student-assignment-view';
import { IAssignment } from 'app/shared/model/assignment.model';
import { AssignmentContext } from 'app/journey/journey-detail/journey-detail-new';
import { MaterialProvider } from 'app/shared/contexts/material-context';
import { RootProvider } from 'app/shared/contexts/root-context';
import { DateTime } from 'luxon';

describe('Student assigment view', () => {
  const mockStore = configureMockStore();
  const store = {
    locale: {
      currentLocale: 'en',
    },
    authentication: {
      account: {
        id: 5,
      },
    },
  };
  let queryClient;
  let history;

  const getDefaultAssignment = () => ({
    id: 1234,
    requiresSubmission: true,
  });

  beforeEach(() => {
    queryClient = new QueryClient();
    history = createBrowserHistory();
  });

  const component = (assignment: IAssignment = getDefaultAssignment()) =>
    render(
      <RootProvider>
        <Provider store={mockStore(store)}>
          <QueryClientProvider client={queryClient}>
            <Router history={history}>
              <AssignmentContext.Provider value={{ assignment, isAllowedToModify: false, milestone: {}, journey: {} }}>
                <MaterialProvider>
                  <StudentAssignmentView />
                </MaterialProvider>
              </AssignmentContext.Provider>
            </Router>
          </QueryClientProvider>
        </Provider>
      </RootProvider>
    );

  it('should not show test results section if test results does not exist', async () => {
    component({ ...getDefaultAssignment(), id: 12345 });

    await screen.findByText('schoolabyApp.submission.submissionInfo.yourSubmission');
    await screen.findByText('value');

    const testResultsHeading = screen.queryByRole('heading', { name: 'schoolabyApp.submission.testResults' });
    expect(testResultsHeading).not.toBeInTheDocument();
  });

  it('should show submission not required card if assignment does not require a submission', async () => {
    component({ ...getDefaultAssignment(), requiresSubmission: false });

    await screen.findByText('schoolabyApp.assignment.detail.requiresSubmissionTitle');
    await screen.findByText('schoolabyApp.assignment.detail.requiresSubmissionDescription');
  });

  it('should not show submission not required card if assignment does require a submission', () => {
    component();

    const cardTitle = screen.queryByText('schoolabyApp.assignment.detail.requiresSubmissionTitle');
    const cardSubTitle = screen.queryByText('schoolabyApp.assignment.detail.requiresSubmissionDescription');

    expect(cardTitle).not.toBeInTheDocument();
    expect(cardSubTitle).not.toBeInTheDocument();
  });

  it('should show submission data if submission exists', async () => {
    component();

    await waitFor(
      () => {
        screen.getByRole('heading', { name: 'schoolabyApp.submission.submissionInfo.yourSubmission' });
        screen.getByText('value');

        const submittedContainer = screen.getByText('schoolabyApp.submission.submitted').closest('span');
        expect(submittedContainer).toHaveTextContent('schoolabyApp.submission.submitted');
        expect(submittedContainer).toHaveTextContent(/03.02.2022/);

        const initialFileGridItems = screen.getAllByTestId('material-card');
        expect(initialFileGridItems).toHaveLength(2);
      },
      { timeout: 15000 }
    );
  });

  it('should show test results if test results exist', async () => {
    component();

    await waitFor(
      () => {
        screen.getByRole('heading', { name: 'schoolabyApp.submission.testResults' });
        screen.getByText('LTI Test title (launch)');
        screen.getByText('50%');
        screen.getByText(/5.11.2020/);
        screen.getByText('1');
      },
      { timeout: 10000 }
    );
  });

  it('should show edit button on student solution section if resubmittable and overdue', async () => {
    const deadline = new Date(DateTime.local());
    deadline.setMinutes(deadline.getMinutes() - 60);
    component({ ...getDefaultAssignment(), deadline: deadline.toISOString(), flexibleDeadline: true });

    await waitFor(
      () => {
        screen.getByRole('button', { name: 'entity.action.edit' });
      },
      { timeout: 5000 }
    );
  });

  it('should not show edit button on student solution section if not resubmittable and overdue', async () => {
    const deadline = new Date(DateTime.local());
    deadline.setMinutes(deadline.getMinutes() - 60);
    component({ ...getDefaultAssignment(), deadline: deadline.toISOString(), id: 12345 });

    await waitFor(
      () => {
        screen.getByText('schoolabyApp.submission.submissionInfo.yourSubmission');

        const editButton = screen.queryByRole('button', { name: 'entity.action.edit' });
        expect(editButton).not.toBeInTheDocument();
      },
      { timeout: 10000 }
    );
  });

  it('should show deadline passed placeholder on student solution section if not resubmittable and overdue and no submissions exist', async () => {
    const deadline = new Date(DateTime.local());
    deadline.setMinutes(deadline.getMinutes() - 60);
    component({ ...getDefaultAssignment(), deadline: deadline.toISOString(), id: 123456 });

    await screen.findByText('schoolabyApp.assignment.detail.oops');
    await screen.findByText('schoolabyApp.assignment.detail.submissionNotFlexibleAndOverdue');
  });

  it('should show submission not required placeholder on student solution section if not resubmittable and overdue and no submissions exist', async () => {
    const deadline = new Date(DateTime.local());
    deadline.setMinutes(deadline.getMinutes() - 60);
    component({ ...getDefaultAssignment(), requiresSubmission: false, id: 123456 });

    await screen.findByText('schoolabyApp.assignment.detail.requiresSubmissionTitle');
    await screen.findByText('schoolabyApp.assignment.detail.requiresSubmissionDescription');
  });
});
