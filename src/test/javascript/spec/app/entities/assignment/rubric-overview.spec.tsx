import React from 'react';
import { render, screen } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import configureMockStore from 'redux-mock-store';
import { QueryClient, QueryClientProvider } from 'react-query';
import { Provider } from 'react-redux';
import { AUTHORITIES } from 'app/config/constants';
import MockEntityUpdateProvider from '../../../helper/context-mock/mock-entity-update-provider';
import { createRubric, createSelectedCriterionLevels } from 'app/shared/util/rubric';
import RubricOverview from 'app/assignment/assessment-rubric-new/rubric-overview/rubric-overview';
import RubricViewModes from 'app/assignment/assessment-rubric-new/rubric-overview/rubric-view-modes';
import { IRubric } from 'app/shared/model/rubric/rubric.model';
import { ISelectedCritreionLevel } from 'app/shared/model/rubric/selected-critreion-level.model';
import { AssignmentContext, IAssignmentContext } from 'app/journey/journey-detail/journey-detail-new';

describe('Rubric overview', () => {
  const initialState = {
    locale: {
      currentLocale: 'et',
    },
    authentication: {
      account: {
        authorities: [AUTHORITIES.TEACHER],
        login: 'teacher',
      },
    },
  };
  const store = configureMockStore()(initialState);

  const createDefaultAssignmentContext = (): IAssignmentContext => ({
    assignment: {
      id: 1234,
      title: 'Assignment title',
    },
    journey: {
      id: 1,
    },
    isAllowedToModify: true,
  });

  const renderComponent = (
    rubric: IRubric,
    selectedCriterionLevels: ISelectedCritreionLevel[],
    viewMode: RubricViewModes,
    assignmentContext: IAssignmentContext,
    studentFullName?: string
  ) => {
    return render(
      <Provider store={store}>
        <MockEntityUpdateProvider selectedRubric={rubric}>
          <QueryClientProvider client={new QueryClient()}>
            <AssignmentContext.Provider value={assignmentContext}>
              <RubricOverview viewMode={viewMode} selectedLevels={selectedCriterionLevels} studentFullName={studentFullName} />
            </AssignmentContext.Provider>
          </QueryClientProvider>
        </MockEntityUpdateProvider>
      </Provider>,
      { wrapper: MemoryRouter }
    );
  };

  it('should display title with student name when name given and view mode is GRADE', async () => {
    const studentFullName = 'FirstName LastName';
    renderComponent(
      createRubric(),
      createSelectedCriterionLevels(),
      RubricViewModes.GRADE,
      createDefaultAssignmentContext(),
      studentFullName
    );

    await screen.findByText(`schoolabyApp.assignment.rubric.gradingTitle ${studentFullName}`);
  });

  it('should display title with student name when name given and view mode is GRADE', async () => {
    const studentFullName = 'FirstName LastName';
    renderComponent(
      createRubric(),
      createSelectedCriterionLevels(),
      RubricViewModes.GRADE,
      createDefaultAssignmentContext(),
      studentFullName
    );

    await screen.findByText(`schoolabyApp.assignment.rubric.gradingTitle ${studentFullName}`);
  });

  it('should display rubric contents with modified descriptions for selected levels', async () => {
    renderComponent(createRubric(), createSelectedCriterionLevels(), RubricViewModes.GRADE, createDefaultAssignmentContext());

    await screen.findByText('Test rubric');
    await screen.findByText('Criterion 1');
    await screen.findByText('Criterion 2');
    expect(await screen.findAllByText('Criterion description')).toHaveLength(2);
    expect(await screen.findAllByText('Level description')).toHaveLength(2);
    expect(await screen.findAllByText('Modified description')).toHaveLength(2);
  });

  it('should display save and cancel button when viewMode is GRADE', async () => {
    renderComponent(createRubric(), createSelectedCriterionLevels(), RubricViewModes.GRADE, createDefaultAssignmentContext());

    await screen.findByText('entity.action.cancel');
    await screen.findByText('entity.action.save');
  });

  it('should display close button when view mode is VIEW', async () => {
    renderComponent(createRubric(), createSelectedCriterionLevels(), RubricViewModes.VIEW, createDefaultAssignmentContext());

    await screen.findByText('global.close');
  });

  it('should display modified description values when view mode is VIEW', async () => {
    renderComponent(createRubric(), createSelectedCriterionLevels(), RubricViewModes.VIEW, createDefaultAssignmentContext());

    await screen.findByText('Test rubric');
    expect(await screen.findAllByText('Modified description')).toHaveLength(2);
  });
});
