import React from 'react';
import configureMockStore from 'redux-mock-store';
import { fireEvent, render, screen, waitFor, within } from '@testing-library/react';
import { AUTHORITIES } from 'app/config/constants';
import { QueryClient, QueryClientProvider } from 'react-query';
import { ThroughProvider } from 'react-through';
import { Router } from 'react-router-dom';
import { ChatProvider } from 'app/shared/contexts/chat-context';
import { EntityUpdateProvider } from 'app/shared/contexts/entity-update-context';
import { MaterialProvider } from 'app/shared/contexts/material-context';
import AssignmentDetailNew from 'app/assignment/assignment-detail-new/assignment-detail-new';
import { createBrowserHistory } from 'history';
import { Provider } from 'react-redux';
import { AppContext } from 'app/app';
import { EntityDetailProvider } from 'app/shared/contexts/entity-detail-context';
import { RootProvider } from 'app/shared/contexts/root-context';

const historyReplace = jest.fn();
const mockedHistory = {
  push: jest.fn(),
  replace: historyReplace,
};
jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useHistory: () => ({
    ...mockedHistory,
    location: {
      pathname: '/assignment/newDesign/1234?journeyId=1',
    },
  }),
}));
describe('Assignment detail new', () => {
  const mockStore = configureMockStore();
  const queryClient = new QueryClient();

  const history = createBrowserHistory();

  const assignmentDetailProps = {
    history: undefined,
    match: {
      params: {
        id: '1234',
      },
      isExact: true,
      path: 'path',
      url: 'custom/url',
    },
  };

  const getTeacherState = () => ({
    locale: {
      currentLocale: 'en',
    },
    authentication: {
      account: {
        id: 7,
        authorities: [AUTHORITIES.TEACHER],
      },
    },
    submission: {
      id: 123,
      assignmentId: 1234,
    },
    applicationProfile: {
      inProduction: false,
      inTestcafe: false,
    },
  });

  const getStudentState = () => ({
    ...getTeacherState(),
    authentication: {
      account: {
        id: 6,
        authorities: [AUTHORITIES.STUDENT],
      },
    },
  });

  const component = (storeState, props?) =>
    render(
      <Provider store={mockStore(storeState)}>
        <AppContext.Provider
          value={{
            setModal() {},
          }}
        >
          <RootProvider>
            <QueryClientProvider client={queryClient}>
              <ThroughProvider>
                <ChatProvider>
                  <EntityDetailProvider>
                    <EntityUpdateProvider>
                      <MaterialProvider>
                        <Router history={history}>
                          <AssignmentDetailNew {...assignmentDetailProps} {...props} />
                        </Router>
                      </MaterialProvider>
                    </EntityUpdateProvider>
                  </EntityDetailProvider>
                </ChatProvider>
              </ThroughProvider>
            </QueryClientProvider>
          </RootProvider>
        </AppContext.Provider>
      </Provider>
    );

  it('should show MyJourneysBreadcrumbItem and JourneyTitleBreadcrumbItem and MilestoneTitleBreadcrumbItem and AssignmentTitleBreadcrumbItem', async () => {
    component(getStudentState());
    await waitFor(() => {
      const breadcrumbs = screen.getByLabelText('breadcrumb');
      const myJourneysBreadcrumbItem = within(breadcrumbs).getByText('global.menu.journeys');
      const journeyTitleBreadcrumbItem = within(breadcrumbs).getByText('Journey title');
      const milestoneTitleBreadcrumbItem = within(breadcrumbs).getByText('Milestone title');
      const assignmentTitleBreadcrumbItem = within(breadcrumbs).getByText('Assignment title');

      expect(myJourneysBreadcrumbItem).toHaveAttribute('href', '/journey');
      expect(journeyTitleBreadcrumbItem).toHaveAttribute('href', '/journey/1');
      expect(milestoneTitleBreadcrumbItem).toHaveAttribute('href', '/milestone/1?journeyId=1');
      expect(assignmentTitleBreadcrumbItem).toHaveAttribute('href', '/assignment/1234?journeyId=1');
    });
  });

  it('should show teacher tabs when in teacher role', async () => {
    component(getTeacherState());

    await screen.findAllByText('schoolabyApp.assignment.detail.tabs.assignment');
    await screen.findAllByText('schoolabyApp.assignment.submissions');
    await screen.findAllByText('schoolabyApp.assignment.detail.tabs.backpack');
    await screen.findAllByText('schoolabyApp.assignment.detail.tabs.analytics');
  });

  it('should show student tabs when in student role', async () => {
    component(getStudentState());

    await screen.findAllByText('schoolabyApp.assignment.detail.tabs.assignment');
    await screen.findAllByText('schoolabyApp.assignment.detail.tabs.backpack');

    const studentsTab = screen.queryByText('global.heading.tabs.students');
    const analyticsTab = screen.queryByText('schoolabyApp.assignment.detail.tabs.analytics');

    expect(studentsTab).not.toBeInTheDocument();
    expect(analyticsTab).not.toBeInTheDocument();
  });

  it('should show navigation buttons if user is teacher in journey', async () => {
    component(getTeacherState());
    const previousAssignmentButtons = await screen.findAllByText('Previous assignment');
    const nextAssignmentButtons = await screen.findAllByText('Next assignment');

    expect(previousAssignmentButtons).toHaveLength(2);
    expect(nextAssignmentButtons).toHaveLength(2);
  });

  it('should not show navigation buttons if user is student', async () => {
    component(getStudentState());
    // This one is here to prevent false positive test results
    await screen.findAllByText('Assignment title');
    const previousAssignmentButtons = screen.queryByText('Previous assignment');
    const nextAssignmentButtons = screen.queryByText('Next assignment');

    expect(previousAssignmentButtons).not.toBeInTheDocument();
    expect(nextAssignmentButtons).not.toBeInTheDocument();
  });

  it('should show "Add task to Stuudium" button when in teacher role', async () => {
    component(getTeacherState());
    await screen.findByText('schoolabyApp.assignment.shareToStuudium');
  });

  it('should not show "Add task to Stuudium" button when in student role', async () => {
    component(getStudentState());
    // This one is here to prevent false positive test results
    await screen.findAllByText('Assignment title');
    const stuudiumButton = screen.queryByText('schoolabyApp.assignment.shareToStuudium');
    expect(stuudiumButton).not.toBeInTheDocument();
  });

  it('should show checkbox indicating whether assignment is hidden or not when in teacher role', async () => {
    component(getTeacherState());
    const checkbox = await screen.findByRole('checkbox');
    expect(checkbox).toBeDisabled();
  });

  it('should not show checkbox indicating whether assignment is hidden or not when in student role', async () => {
    component(getStudentState());
    // This one is here to prevent false positive test results
    await screen.findAllByText('Assignment title');
    const checkbox = screen.queryByRole('checkbox');
    expect(checkbox).not.toBeInTheDocument();
  });

  it('should show assignment info', async () => {
    component(getTeacherState());

    await screen.findByRole('heading', { name: 'Assignment title' });
  });

  it('should show materials and open delete dialog on trash click', async () => {
    component(getTeacherState(), {
      match: {
        params: {
          id: 12345,
        },
      },
    });

    await screen.findByRole('heading', { name: 'Assignment title' });

    fireEvent.mouseOver(screen.getByText('schoolabyApp.marketplace.materials'));

    const trashButton = await screen.findByTestId('material-delete-button');

    fireEvent.click(trashButton);

    await screen.findByText('entity.delete.description Test material');
  });

  it('should not show trash icon on listed materials if user is student', async () => {
    component(getStudentState(), {
      match: {
        params: {
          id: 12345,
        },
      },
    });

    await screen.findByRole('heading', { name: 'Assignment title' });

    fireEvent.mouseOver(screen.getByText('schoolabyApp.marketplace.materials'));

    const trashButton = screen.queryByTestId('material-delete-button');
    expect(trashButton).not.toBeInTheDocument();
  });

  it('should not show add material button if user is student', async () => {
    component(getStudentState());

    await screen.findByRole('heading', { name: 'Assignment title' });

    fireEvent.mouseOver(screen.getByText('schoolabyApp.marketplace.materials'));

    const addMaterialBtn = screen.queryByText('global.form.addNewMaterialBtn');
    expect(addMaterialBtn).not.toBeInTheDocument();
  });

  it('should show material dropdown with marketplace when add button clicked', async () => {
    component(getTeacherState());

    await screen.findByRole('heading', { name: 'Assignment title' });

    fireEvent.mouseOver(screen.getByText('schoolabyApp.marketplace.materials'));

    const addMaterialBtn = await screen.findByText('global.form.addNewMaterialBtn');

    fireEvent.click(addMaterialBtn);

    screen.getByText('schoolabyApp.marketplace.add.marketplace');
    screen.getByText('schoolabyApp.marketplace.add.link');
    screen.getByText('schoolabyApp.marketplace.add.file');
    screen.getByText('schoolabyApp.marketplace.add.camera');
  });
});
