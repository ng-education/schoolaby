import React from 'react';
import configureMockStore from 'redux-mock-store';
import { render, screen, waitFor } from '@testing-library/react';
import { AUTHORITIES } from 'app/config/constants';
import { RootProvider } from 'app/shared/contexts/root-context';
import { QueryClient, QueryClientProvider } from 'react-query';
import { ThroughProvider } from 'react-through';
import MockEntityDetailProvider, { IMockEntityDetailProvider } from '../../../../../helper/context-mock/mock-entity-detail-provider';
import ContextProvider from '../../../../../helper/context-mock/context-provider';
import MockSubmissionGradingProvider from '../../../../../helper/context-mock/mock-submission-grading-provider';
import SubmissionGradingDetailsView from 'app/assignment/assignment-detail/submission/submission-grading-body/submission-grading-details-view';
import { ISubmission } from 'app/shared/model/submission/submission.model';
import { MaterialProvider } from 'app/shared/contexts/material-context';

const mockStore = configureMockStore();
const queryClient = new QueryClient();

describe('Submission grading details view', () => {
  let store: any;

  beforeEach(() => {
    const initialState = {
      isAllowedToModify: true,
      locale: {
        currentLocale: 'en',
      },
      authentication: {
        account: {
          authorities: [AUTHORITIES.TEACHER],
        },
      },
      applicationProfile: {
        isInProduction: false,
      },
    };
    store = mockStore(initialState);
  });

  const contextProps: IMockEntityDetailProvider = {
    selectedUser: {
      id: 5,
      login: 'student',
      firstName: 'student',
      lastName: 'student',
      email: 'student@localhost.com',
      personRoles: [],
      authorities: ['ROLE_STUDENT'],
    },
    entity: {
      id: 1234,
      title: 'Assignment title',
      description: 'Description text',
      gradingSchemeId: 10,
      deadline: '2020-09-07T09:24:00Z',
      flexibleDeadline: true,
      journeyId: 1,
      materials: [],
    },
    gradingSchemeEntity: {
      id: 10,
      name: 'Alphabetical (A-F)',
      code: 'ALPHABETICAL_A_F',

      isNonGradable(): boolean {
        return true;
      },
    },
    submissions: [
      {
        id: 6,
        value: 'Students comment',
        submissionFeedbacks: [
          {
            grade: 'A',
            feedbackDate: '2020-09-07T09:24:00Z',
            studentId: '5',
          },
        ],
        authors: [
          {
            id: 5,
            firstName: 'student',
            lastName: 'student',
            email: 'student@localhost.com',
            personRoles: [],
          },
        ],
        assignmentId: 1234,
      },
    ],
  };

  const studentSubmission: ISubmission = {
    id: 6,
    value: 'Students comment',
    submissionFeedbacks: [
      {
        grade: 'A',
        feedbackDate: '2020-09-07T09:24:00Z',
        studentId: '5',
      },
    ],
    submittedForGradingDate: '2020-09-07T09:24:00Z',
    authors: [
      {
        id: 5,
        firstName: 'student',
        lastName: 'student',
        email: 'student@localhost.com',
        personRoles: [],
      },
    ],
    assignmentId: 1234,
  };

  function component() {
    return {
      ...render(
        <ContextProvider contextProvider={MockEntityDetailProvider} store={store} props={contextProps}>
          <RootProvider>
            <QueryClientProvider client={queryClient}>
              <ThroughProvider>
                <MockSubmissionGradingProvider ltiResources={[]}>
                  <MaterialProvider>
                    <SubmissionGradingDetailsView submission={studentSubmission} isAllowedToModify />
                  </MaterialProvider>
                </MockSubmissionGradingProvider>
              </ThroughProvider>
            </QueryClientProvider>
          </RootProvider>
        </ContextProvider>
      ),
      store,
    };
  }

  it('Should display assignment information', async () => {
    component();
    await waitFor(() => {
      screen.getByText('Description text');
      screen.getAllByText('07.09.2020');
      screen.getAllByText('Assignment title');
      screen.getAllByText('schoolabyApp.gradingScheme.code.ALPHABETICAL_A_F');
    });
  });

  it('Should display submission information', async () => {
    component();
    await waitFor(() => {
      screen.getByText('Student Student schoolabyApp.submission.submissionInfo.studentSubmission');
      screen.getByText('Students comment');
      screen.getByText('schoolabyApp.submission.submissionInfo.studentAssets');
    });
  });
});
