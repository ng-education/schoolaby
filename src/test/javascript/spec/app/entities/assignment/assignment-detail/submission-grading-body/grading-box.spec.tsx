import React from 'react';
import configureMockStore from 'redux-mock-store';
import { render, screen, waitFor } from '@testing-library/react';
import { AUTHORITIES } from 'app/config/constants';
import { RootProvider } from 'app/shared/contexts/root-context';
import { QueryClient, QueryClientProvider } from 'react-query';
import { ThroughProvider } from 'react-through';
import MockEntityDetailProvider, { IMockEntityDetailProvider } from '../../../../../helper/context-mock/mock-entity-detail-provider';
import ContextProvider from '../../../../../helper/context-mock/context-provider';
import { ISubmission } from 'app/shared/model/submission/submission.model';
import GradingBox from 'app/assignment/assignment-detail/submission/submission-grading-body/grading-box';
import { ISubmissionFeedback } from 'app/shared/model/submission/submission-feedback.model';

const mockStore = configureMockStore();
const queryClient = new QueryClient();

describe('Student submission details', () => {
  let store: any;
  const SUBMITTED_FOR_GRADING_DATE = '2020-09-07T09:24:00Z';
  const FEEDBACK_DATE = '2020-09-07T09:24:00Z';

  beforeEach(() => {
    const initialState = {
      isAllowedToModify: true,
      locale: {
        currentLocale: 'en',
      },
      authentication: {
        account: {
          authorities: [AUTHORITIES.TEACHER],
        },
      },
    };
    store = mockStore(initialState);
  });

  const contextProps: IMockEntityDetailProvider = {
    selectedUser: {
      id: 5,
      login: 'student',
      firstName: 'student',
      lastName: 'student',
      email: 'student@localhost.com',
      personRoles: [],
      authorities: ['ROLE_STUDENT'],
    },
    entity: {
      id: 1234,
      title: 'Assignment title',
      description: 'Description text',
      gradingSchemeId: 10,
      deadline: '2020-09-07T09:24:00Z',
      flexibleDeadline: true,
      journeyId: 1,
      materials: [],
    },
    gradingSchemeEntity: {
      id: 10,
      name: 'Alphabetical (A-F)',
      code: 'ALPHABETICAL_A_F',

      isNonGradable(): boolean {
        return true;
      },
    },
    submissions: [
      {
        id: 6,
        value: 'Students comment',
        authors: [
          {
            id: 5,
            firstName: 'student',
            lastName: 'student',
            email: 'student@localhost.com',
            personRoles: [],
          },
        ],
        assignmentId: 1234,
      },
    ],
  };

  const createFeedback = (feedbackDate: string): ISubmissionFeedback => {
    return {
      grade: 'A',
      feedbackDate,
      value: '',
    };
  };

  const createSubmission = (submittedForGradingDate?: string): ISubmission => {
    return {
      id: 6,
      value: 'Students comment',
      submittedForGradingDate,
      authors: [
        {
          id: 5,
          firstName: 'student',
          lastName: 'student',
          email: 'student@localhost.com',
          personRoles: [],
        },
      ],
      assignmentId: 1234,
    };
  };

  function component(studentSubmission: ISubmission, submissionFeedback?: ISubmissionFeedback) {
    return {
      ...render(
        <ContextProvider contextProvider={MockEntityDetailProvider} store={store} props={contextProps}>
          <RootProvider>
            <QueryClientProvider client={queryClient}>
              <ThroughProvider>
                <GradingBox feedback={submissionFeedback} submission={studentSubmission} />
              </ThroughProvider>
            </QueryClientProvider>
          </RootProvider>
        </ContextProvider>
      ),
      store,
    };
  }

  it('Should display icon when new submission has been made', async () => {
    component(createSubmission(SUBMITTED_FOR_GRADING_DATE));
    await waitFor(() => {
      screen.getByRole('student-submitted-icon-span');
    });
  });

  it('Should not display icon when new submission is graded', async () => {
    component(createSubmission(SUBMITTED_FOR_GRADING_DATE), createFeedback(FEEDBACK_DATE));
    await waitFor(() => {
      expect(screen.queryByRole('student-submitted-icon-span')).toBeNull();
    });
  });

  it('Should not display icon when student has not submitted their solution', async () => {
    component(undefined);
    await waitFor(() => {
      expect(screen.queryByRole('student-submitted-icon-span')).toBeNull();
    });
  });
});
