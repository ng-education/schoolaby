import { AUTHORITIES } from 'app/config/constants';
import React from 'react';
import { RootProvider } from 'app/shared/contexts/root-context';
import { Provider } from 'react-redux';
import { render, screen } from '@testing-library/react';
import configureMockStore from 'redux-mock-store';
import SubmissionTimesBarChart from 'app/assignment/assignment-detail/analytics/submission-times-bar-chart';

describe('SubmissionTimesBarChart', () => {
  let store: any;
  const mockStore = configureMockStore();

  const submissionTimesBarChartProps = {
    students: [{}, {}, {}],
    journey: {},
    assignment: {
      deadline: '2021-05-09T13:17:49.929545Z',
    },
    submissions: [
      { submittedForGradingDate: '2021-05-05T13:17:49.929545Z' },
      { submittedForGradingDate: '2021-05-09T13:17:49.929545Z' },
      { submittedForGradingDate: '2021-05-14T13:17:49.929545Z' },
    ],
  };

  beforeEach(() => {
    const initialState = {
      locale: {
        currentLocale: 'en',
      },
      authentication: {
        account: {
          authorities: [AUTHORITIES.TEACHER],
        },
      },
      applicationProfile: {
        inProduction: false,
      },
    };
    store = mockStore(initialState);
  });

  it('Should render BarChartFooter, BarChartHeader and DateBarChart', () => {
    render(
      <RootProvider>
        <Provider store={store}>
          <SubmissionTimesBarChart {...submissionTimesBarChartProps} />
        </Provider>
      </RootProvider>
    );

    screen.getByText('schoolabyApp.assignment.analytics.submissionTimes.title');
    const barNumbers = screen.getAllByLabelText('schoolabyApp.assignment.analytics.barChart.barNumberLabel');
    expect(barNumbers.length).toBe(10);
    const studentCount = screen.getByLabelText('schoolabyApp.assignment.analytics.barChart.studentCount');
    expect(studentCount.textContent).toBe('3');

    screen.getByLabelText('schoolabyApp.submission.onTime');
    screen.getByLabelText('schoolabyApp.submission.overdue');
    screen.getByLabelText('schoolabyApp.submission.notSubmitted');

    screen.getByText('05.05.2021');
    screen.getByText('14.05.2021');
  });
});
