import { render, screen, waitFor, within } from '@testing-library/react';
import { MemoryRouter, RouteComponentProps } from 'react-router-dom';
import React from 'react';
import { ThroughProvider } from 'react-through';
import configureMockStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { AUTHORITIES } from 'app/config/constants';
import { RootProvider } from 'app/shared/contexts/root-context';
import { QueryClient, QueryClientProvider } from 'react-query';
import AssignmentAnalytics from 'app/assignment/assignment-detail/analytics/assignment-analytics';
import { EntityUpdateProvider } from 'app/shared/contexts/entity-update-context';
import { AppContext } from 'app/app';
import { MaterialProvider } from 'app/shared/contexts/material-context';
import { EntityDetailProvider } from 'app/shared/contexts/entity-detail-context';

describe('AssignmentAnalytics', () => {
  let store: any;
  const queryClient = new QueryClient();
  const mockStore = configureMockStore();

  const assignmentAnalyticsProps = (): RouteComponentProps<{ id: string }> => {
    return {
      history: undefined,
      location: undefined,
      match: {
        params: {
          id: '1234',
        },
        isExact: true,
        path: 'path',
        url: 'url',
      },
    };
  };

  beforeEach(() => {
    const initialState = {
      locale: {
        currentLocale: 'en',
      },
      authentication: {
        account: {
          authorities: [AUTHORITIES.TEACHER],
        },
      },
      applicationProfile: {
        inProduction: false,
      },
    };
    store = mockStore(initialState);
  });

  it('Should show MyJourneysBreadcrumbItem and JourneyTitleBreadcrumbItem and AssignmentTitleBreadcrumbItem and AnalyticsBreadcrumbItem', async () => {
    render(
      <AppContext.Provider
        value={{
          setModal() {},
        }}
      >
        <RootProvider>
          <Provider store={store}>
            <ThroughProvider>
              <QueryClientProvider client={queryClient}>
                <MemoryRouter initialEntries={['/assignment/1234/analytics?journeyId=1']}>
                  <EntityUpdateProvider>
                    <EntityDetailProvider>
                      <MaterialProvider>
                        <AssignmentAnalytics {...assignmentAnalyticsProps()} />
                      </MaterialProvider>
                    </EntityDetailProvider>
                  </EntityUpdateProvider>
                </MemoryRouter>
              </QueryClientProvider>
            </ThroughProvider>
          </Provider>
        </RootProvider>
      </AppContext.Provider>
    );
    await waitFor(
      () => {
        const breadcrumbs = screen.getByLabelText('breadcrumb');
        const myJourneysBreadcrumbItem = within(breadcrumbs).getByText('global.menu.journeys');
        const journeyTitleBreadcrumbItem = within(breadcrumbs).getByText('Journey title');
        const assignmentTitleBreadCrumbItem = within(breadcrumbs).getByText('Assignment title');
        const analyticsBreadCrumbItem = within(breadcrumbs).getByText('schoolabyApp.assignment.detail.tabs.analytics');

        expect(myJourneysBreadcrumbItem).toHaveAttribute('href', '/journey');
        expect(journeyTitleBreadcrumbItem).toHaveAttribute('href', '/journey/1');
        expect(assignmentTitleBreadCrumbItem).toHaveAttribute('href', '/assignment/1234?journeyId=1');
        expect(analyticsBreadCrumbItem).toHaveAttribute('href', '/assignment/1234/analytics?journeyId=1');
      },
      { timeout: 15000 }
    );
  });
});
