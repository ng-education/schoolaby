import {
  DANGER_STYLE,
  DEFAULT_STYLE,
  getGradeText,
  getGradingContainerClassNameByGradingType,
  getInputClassName,
  getInputMaxLength,
  SUCCESS_STYLE,
  WARNING_STYLE,
} from 'app/assignment/assignment-detail/submission/shared/grade-select-util';
import { ALPHABETICAL_A_F, FAIL, NARRATIVE, NUMERICAL_1_5, PASS, PASS_FAIL, PERCENTAGE_0_100 } from 'app/shared/model/grading-scheme.model';

describe('Grade select utils', () => {
  it('Should get correct grade text', () => {
    expect(getGradeText(NUMERICAL_1_5)).toEqual(' / 5');
    expect(getGradeText(ALPHABETICAL_A_F)).toEqual(' / A-F');
    expect(getGradeText(PERCENTAGE_0_100)).toEqual(' / 100%');
    expect(getGradeText(PASS_FAIL)).toEqual('');
    expect(getGradeText(NARRATIVE)).toEqual('');
  });

  it('Should restrict characters to certain number', () => {
    expect(getInputMaxLength(NUMERICAL_1_5)).toEqual(1);
    expect(getInputMaxLength(ALPHABETICAL_A_F)).toEqual(1);
    expect(getInputMaxLength(PERCENTAGE_0_100)).toEqual(3);
    expect(getInputMaxLength(NARRATIVE)).toEqual(undefined);
  });

  it('Should get correct input className', () => {
    expect(getInputClassName(NUMERICAL_1_5)).toEqual('input-one-character');
    expect(getInputClassName(ALPHABETICAL_A_F)).toEqual('input-one-character');
    expect(getInputClassName(PERCENTAGE_0_100)).toEqual('input-three-character');
    expect(getInputClassName(NARRATIVE)).toEqual(undefined);
  });

  it('Should get correct grading container className', () => {
    expect(getGradingContainerClassNameByGradingType(NUMERICAL_1_5, 2)).toEqual(DANGER_STYLE);
    expect(getGradingContainerClassNameByGradingType(NUMERICAL_1_5, 3)).toEqual(WARNING_STYLE);
    expect(getGradingContainerClassNameByGradingType(NUMERICAL_1_5, 5)).toEqual(SUCCESS_STYLE);
    expect(getGradingContainerClassNameByGradingType(NUMERICAL_1_5, undefined)).toEqual(DEFAULT_STYLE);

    expect(getGradingContainerClassNameByGradingType(ALPHABETICAL_A_F, 'f')).toEqual(DANGER_STYLE);
    expect(getGradingContainerClassNameByGradingType(ALPHABETICAL_A_F, 'c')).toEqual(WARNING_STYLE);
    expect(getGradingContainerClassNameByGradingType(ALPHABETICAL_A_F, 'a')).toEqual(SUCCESS_STYLE);
    expect(getGradingContainerClassNameByGradingType(ALPHABETICAL_A_F, undefined)).toEqual(DEFAULT_STYLE);

    expect(getGradingContainerClassNameByGradingType(PERCENTAGE_0_100, 23)).toEqual(DANGER_STYLE);
    expect(getGradingContainerClassNameByGradingType(PERCENTAGE_0_100, 56)).toEqual(WARNING_STYLE);
    expect(getGradingContainerClassNameByGradingType(PERCENTAGE_0_100, 80)).toEqual(SUCCESS_STYLE);
    expect(getGradingContainerClassNameByGradingType(PERCENTAGE_0_100, undefined)).toEqual(DEFAULT_STYLE);

    expect(getGradingContainerClassNameByGradingType(PASS_FAIL, PASS)).toEqual(SUCCESS_STYLE);
    expect(getGradingContainerClassNameByGradingType(PASS_FAIL, FAIL)).toEqual(DANGER_STYLE);
    expect(getGradingContainerClassNameByGradingType(PASS_FAIL, undefined)).toEqual(DEFAULT_STYLE);
  });
});
