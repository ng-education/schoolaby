import React from 'react';
import configureMockStore from 'redux-mock-store';
import { fireEvent, render, screen } from '@testing-library/react';
import ContextProvider from '../../../../../helper/context-mock/context-provider';
import MockEntityDetailProvider, { IMockEntityDetailProvider } from '../../../../../helper/context-mock/mock-entity-detail-provider';
import { QueryClient, QueryClientProvider } from 'react-query';
import MockSubmissionGradingProvider, {
  IMockSubmissionGradingProvider,
} from '../../../../../helper/context-mock/mock-submission-grading-provider';
import SubmissionGroupList from 'app/assignment/assignment-detail/submission/submission-grading-body/submission-group-list';
import { AUTHORITIES } from 'app/config/constants';

import fs from 'fs';
import path from 'path';

const mockStore = configureMockStore();
const queryClient = new QueryClient();

describe('Submission group list', () => {
  let store: any;

  function component() {
    const groups = [
      {
        id: 1,
        name: 'Group 1',
        students: [
          {
            id: 5,
            firstName: 'student',
            lastName: 'student',
            email: 'student@localhost.com',
            personRoles: [],
          },
          {
            id: 6,
            firstName: 'student2',
            lastName: 'student2',
            email: 'student2@localhost.com',
            personRoles: [],
          },
        ],
      },
      {
        id: 2,
        name: 'Group 2',
        students: [
          {
            id: 7,
            firstName: 'student3',
            lastName: 'student3',
            email: 'student3@localhost.com',
            personRoles: [],
          },
          {
            id: 8,
            firstName: 'student4',
            lastName: 'student4',
            email: 'student4@localhost.com',
            personRoles: [],
          },
        ],
      },
    ];

    const contextProps: IMockEntityDetailProvider = {
      entity: {
        id: 1234,
        title: 'Assignment title',
        description: 'Description text',
        gradingSchemeId: 10,
        deadline: '2020-09-07T09:24:00Z',
        flexibleDeadline: true,
        journeyId: 1,
        materials: [],
        groups,
      },
      gradingSchemeEntity: {
        id: 10,
        name: 'Alphabetical (A-F)',
        code: 'ALPHABETICAL_A_F',

        isNonGradable(): boolean {
          return true;
        },
      },
      submissions: [
        {
          id: 6,
          value: 'Students comment',
          submittedForGradingDate: '2020-09-06T09:24:00Z',
          authors: [
            {
              id: 5,
              firstName: 'student',
              lastName: 'student',
              email: 'student@localhost.com',
              personRoles: [],
            },
          ],
          assignmentId: 1234,
        },
      ],
    };

    const submissionGradingProps: IMockSubmissionGradingProvider = {
      submittedSubmissions: [],
    };
    return {
      ...render(
        <ContextProvider contextProvider={MockEntityDetailProvider} store={store} props={contextProps}>
          <QueryClientProvider client={queryClient}>
            <ContextProvider contextProvider={MockSubmissionGradingProvider} store={store} props={submissionGradingProps}>
              <SubmissionGroupList />
            </ContextProvider>
          </QueryClientProvider>
        </ContextProvider>
      ),
      store,
    };
  }

  const addStyleToComponent = (componentToTest: HTMLElement) => {
    const cssFile = fs.readFileSync(path.resolve(__dirname, 'test.css'), 'utf8');
    const style = document.createElement('style');
    style.innerHTML = cssFile;
    componentToTest.append(style);
  };

  beforeEach(() => {
    const initialState = {
      isAllowedToModify: true,
      locale: {
        currentLocale: 'en',
      },
      authentication: {
        account: {
          authorities: [AUTHORITIES.TEACHER],
        },
      },
    };

    store = mockStore(initialState);

    const { container } = component();
    addStyleToComponent(container);
  });

  const clickGroup = (groupName = 'Group 1') => {
    const groupDropdown = screen.getByText(groupName);
    fireEvent.click(groupDropdown);
  };

  it('should display collapsed groups', () => {
    screen.getByText('Group 1');
    screen.getByText('Group 2');

    expect(screen.queryByText('Student Student')).not.toBeVisible();
    expect(screen.queryByText('Student2 Student2')).not.toBeVisible();
    expect(screen.queryByText('Student3 Student3')).not.toBeVisible();
    expect(screen.queryByText('Student4 Student4')).not.toBeVisible();
  });

  it('should expand group on click', () => {
    clickGroup();

    expect(screen.queryByText('Student Student')).toBeVisible();
    expect(screen.queryByText('Student2 Student2')).toBeVisible();
    expect(screen.queryByText('Student3 Student3')).not.toBeVisible();
    expect(screen.queryByText('Student4 Student4')).not.toBeVisible();
  });

  it('should collapse group when clicked on chevron', () => {
    clickGroup();

    const chevron = screen.getByLabelText('global.dropdown.collapse');
    fireEvent.click(chevron);

    expect(screen.queryByText('Student Student')).not.toBeVisible();
    expect(screen.queryByText('Student2 Student2')).not.toBeVisible();
  });

  it('should not collapse group when clicked on group', () => {
    clickGroup();
    clickGroup();

    expect(screen.queryByText('Student Student')).toBeVisible();
    expect(screen.queryByText('Student2 Student2')).toBeVisible();
  });
});
