import React from 'react';
import configureMockStore from 'redux-mock-store';
import { fireEvent, render, screen, waitFor, within } from '@testing-library/react';
import ContextProvider from '../../../helper/context-mock/context-provider';
import { AUTHORITIES } from 'app/config/constants';
import { AssignmentDetail, IAssignmentDetailProps } from 'app/assignment/assignment-detail';
import { RootProvider } from 'app/shared/contexts/root-context';
import { TranslatorContext } from 'react-jhipster';
import { QueryClient, QueryClientProvider } from 'react-query';
import { ThroughProvider } from 'react-through';
import { MemoryRouter } from 'react-router-dom';
import { ChatProvider } from 'app/shared/contexts/chat-context';
import { EntityUpdateProvider } from 'app/shared/contexts/entity-update-context';
import { MaterialProvider } from 'app/shared/contexts/material-context';
import { EntityDetailProvider } from 'app/shared/contexts/entity-detail-context';
import { AppContext } from 'app/app';

Element.prototype.scrollIntoView = jest.fn();

describe('Assignment detail body', () => {
  const mockStore = configureMockStore();
  const queryClient = new QueryClient();

  const assignmentDetailProps: IAssignmentDetailProps = {
    isTeacherOrAdmin: true,
    account: { id: 7 },
    history: undefined,
    match: {
      params: {
        id: '1234',
      },
      isExact: true,
      path: 'path',
      url: 'custom/url',
    },
    location: undefined,
  };

  const getTeacherState = () => ({
    locale: {
      currentLocale: 'en',
    },
    authentication: {
      account: {
        id: 7,
        authorities: [AUTHORITIES.TEACHER],
      },
    },
    submission: {
      id: 123,
      assignmentId: 1234,
    },
  });

  const getStudentState = () => ({
    ...getTeacherState(),
    authentication: {
      account: {
        id: 6,
        authorities: [AUTHORITIES.STUDENT],
      },
    },
  });

  const component = storeState =>
    render(
      <AppContext.Provider value={{ setModal() {} }}>
        <RootProvider>
          <ContextProvider contextProvider={EntityDetailProvider} store={mockStore(storeState)}>
            <QueryClientProvider client={queryClient}>
              <ThroughProvider>
                <MemoryRouter initialEntries={['/assignment/1234?journeyId=1']}>
                  <ChatProvider>
                    <EntityUpdateProvider>
                      <MaterialProvider>
                        <AssignmentDetail {...assignmentDetailProps} />
                      </MaterialProvider>
                    </EntityUpdateProvider>
                  </ChatProvider>
                </MemoryRouter>
              </ThroughProvider>
            </QueryClientProvider>
          </ContextProvider>
        </RootProvider>
      </AppContext.Provider>
    );

  beforeAll(() => {
    TranslatorContext.registerTranslations('en', {});
  });

  it('should display submission grading body', async () => {
    component(getTeacherState());

    await screen.findByText('Description text');
    await screen.findByText(/07.09.2020/);
    await screen.findAllByText('Assignment title');
  });

  it('should display submission creation body', async () => {
    component(getStudentState());

    await screen.findByText(/07.09.2020/);
    await screen.findAllByText('Assignment title');
  });

  it('should show MyJourneysBreadcrumbItem and JourneyTitleBreadcrumbItem and AssignmentTitleBreadcrumbItem', async () => {
    component(getStudentState());
    await waitFor(() => {
      const breadcrumbs = screen.getByLabelText('breadcrumb');
      const myJourneysBreadcrumbItem = within(breadcrumbs).getByText('global.menu.journeys');
      const journeyTitleBreadcrumbItem = within(breadcrumbs).getByText('Journey title');
      const assignmentTitleBreadcrumbItem = within(breadcrumbs).getByText('Assignment title');

      expect(myJourneysBreadcrumbItem).toHaveAttribute('href', '/journey');
      expect(journeyTitleBreadcrumbItem).toHaveAttribute('href', '/journey/1');
      expect(assignmentTitleBreadcrumbItem).toHaveAttribute('href', '/assignment/1234?journeyId=1');
    });
  });

  it('should open assignment edit view', async () => {
    component(getTeacherState());

    const heading = await screen.findByTestId('heading-new');
    const headingEditButton = within(heading).getByText('entity.dropdown.edit');
    fireEvent.click(headingEditButton);

    within(heading).getByText('entity.action.edit');
    within(heading).getByText('Assignment title');
  });
});
