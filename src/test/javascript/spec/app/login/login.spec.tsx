import { fireEvent, render, screen } from '@testing-library/react';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import React from 'react';
import { ThroughProvider } from 'react-through';
import configureMockStore from 'redux-mock-store';
import { RootProvider } from 'app/shared/contexts/root-context';
import { QueryClient, QueryClientProvider } from 'react-query';
import { ILoginProps, Login } from 'app/login/login';

jest.mock('react-router-dom', () => ({
  __esModule: true,
  ...jest.requireActual('react-router-dom'),
  useHistory: () => ({
    push: jest.fn(),
  }),
}));

describe('Login page', () => {
  const history = createBrowserHistory();
  const queryClient = new QueryClient();
  const store = configureMockStore()();

  const getDefaultLoginProps = (): ILoginProps => ({
    locale: 'et',
    isAuthenticated: false,
    loginError: false,
    login() {},
    setLocale() {},
    history,
    match: null,
    location: {
      pathname: '/',
      search: '',
      state: {},
      hash: '',
    },
  });

  let loginProps;

  beforeEach(() => {
    loginProps = getDefaultLoginProps();
  });

  const component = () => {
    return {
      ...render(
        <RootProvider>
          <QueryClientProvider client={queryClient}>
            <ThroughProvider>
              <Router history={history}>
                <Login {...loginProps} />
              </Router>
            </ThroughProvider>
          </QueryClientProvider>
        </RootProvider>
      ),
      store,
    };
  };

  it('should display titles', async () => {
    component();
    await screen.findByText('login.title');
    await screen.findByText('login.loginMessage');
  });

  it('should display oauth login options', async () => {
    component();

    await screen.findByLabelText('login.oauth.googleLogin');
    await screen.findByLabelText('login.oauth.microsoftLogin');
    await screen.findByLabelText('login.oauth.yahooLogin');
    await screen.findByLabelText('login.oauth.harIdLogin');
  });

  it('should display username login form', async () => {
    component();

    const enableUsernameLoginBtn = await screen.findByText('login.signInWithUsername');
    fireEvent.click(enableUsernameLoginBtn);
    await screen.findByPlaceholderText('global.form.username.placeholder');
    await screen.findByPlaceholderText('login.form.password.placeholder');
    await screen.findByText('login.password.forgot');
    await screen.findByText('login.form.button');
    await screen.findByText('login.registerAccount');
    await screen.findByLabelText('login.form.rememberMe');
  });

  it('should display login error alert', async () => {
    loginProps.loginError = true;
    component();

    await screen.findByText('login.messages.error.authentication');
  });

  it('should display HarID login error alert', async () => {
    loginProps.location.search = 'harIdLoginFailed=true';
    component();

    await screen.findByText('login.messages.error.harIdLogin');
  });

  it('should display logout success alert', async () => {
    loginProps.location.search = 'logoutSuccessful=true';
    component();

    await screen.findByText('logout.success');
  });
});
