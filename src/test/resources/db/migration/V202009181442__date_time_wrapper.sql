CREATE TABLE date_time_wrapper
(
    id               bigint PRIMARY KEY,
    instant          timestamp,
    local_date_time  timestamp,
    offset_date_time timestamp,
    zoned_date_time  timestamp,
    local_time       time,
    offset_time      time,
    local_date       date
);
