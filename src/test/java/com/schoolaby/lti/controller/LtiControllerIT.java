package com.schoolaby.lti.controller;

import com.schoolaby.common.BaseIntegrationTest;
import com.schoolaby.common.socket.TestWebSocketClient;
import com.schoolaby.domain.*;
import com.schoolaby.domain.enumeration.LtiConfigType;
import com.schoolaby.domain.enumeration.LtiRole;
import com.schoolaby.repository.LtiConfigJpaRepository;
import com.schoolaby.service.dto.LtiConfigDTO;
import com.schoolaby.service.dto.lti.LtiScoreDTO;
import com.schoolaby.service.mapper.LtiConfigMapper;
import com.schoolaby.web.rest.WithUnauthenticatedMockUser;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.transaction.annotation.Transactional;
import org.xmlunit.builder.DiffBuilder;
import org.xmlunit.diff.Diff;

import java.util.List;
import java.util.concurrent.BlockingQueue;

import static com.schoolaby.common.FileUtil.readFile;
import static com.schoolaby.common.TestCases.*;
import static com.schoolaby.common.TestObjects.*;
import static com.schoolaby.config.socket.WebsocketSecurityConfiguration.SUBSCRIBE_LTI_SCORE_ENDPOINT;
import static com.schoolaby.domain.enumeration.LtiConfigType.JOURNEY;
import static com.schoolaby.domain.enumeration.LtiConfigType.PLATFORM;
import static com.schoolaby.domain.enumeration.LtiRole.*;
import static com.schoolaby.web.rest.LtiController.APPLICATION_XML_UTF_8;
import static com.schoolaby.web.rest.TestUtil.convertObjectToJsonBytes;
import static java.util.Collections.singletonList;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.http.MediaType.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.testcontainers.shaded.org.apache.commons.lang.ArrayUtils.contains;

class LtiControllerIT extends BaseIntegrationTest {
    private static final String UPDATED_CONSUMER_KEY = "Updated consumer key";
    private static final String UPDATED_SHARED_SECRET = "Updated shared secret";

    private static final String OAUTH_VERSION = "1.0";
    private static final String SIGNATURE_METHOD = "HMAC-SHA1";
    private static final String LTI_MESSAGE_TYPE = "basic-lti-launch-request";
    private static final LtiConfigType LTI_DEFAULT_TYPE = JOURNEY;
    private static final String LAUNCH_PRESENTATION_DOCUMENT_TARGET = "iframe";
    private static final String LIS_OUTCOMES_URL = "http://localhost/api/lti/results";
    private static final String RETURN_URL = "http://localhost/api/lti/return";
    private static final String LTI_CONTEXT_TYPE = "CourseSection";
    private static final String TOOL_CONSUMER_INSTANCE_GUID = "localhost";
    private static final String TOOL_CONSUMER_INFO_PRODUCT_FAMILY_CODE = "schoolaby";
    private static final String TOOL_CONSUMER_INFO_VERSION = "1.0";
    private static final String TOOL_CONSUMER_INSTANCE_NAME = "Schoolaby";
    private static final String TOOL_CONSUMER_INSTANCE_CONTACT_EMAIL = "test@localhost.com";

    public static final String API_LTI_RESULTS = "/api/lti/results";
    public static final String API_LTI_CONFIG = "/api/lti/config";

    @Autowired
    private LtiConfigJpaRepository ltiConfigRepository;
    @Autowired
    private LtiConfigMapper configMapper;
    @Autowired
    private TestWebSocketClient testWebSocketClient;
    @LocalServerPort
    private String portLocal;

    private Journey journey;
    private Milestone milestone;
    private Assignment assignment;
    private LtiConfig ltiConfig;

    @BeforeEach
    void setup() {
        transactionHelper.withTransaction(() -> {
            User teacher = getTeacher(entityManager);
            journey = createJourney()
                .creator(teacher);
            milestone = createMilestone()
                .creator(teacher);
            assignment = createAssignment()
                .creator(teacher)
                .gradingScheme(getGradingSchemeNumerical(entityManager));
            journey.addMilestone(milestone
                    .addAssignment(assignment))
                .educationalLevel(createEducationalLevel());
        });
    }

    @Test
    @Transactional
    @WithUnauthenticatedMockUser
    void shouldUpdateStudentResult() throws Exception {
        LtiResource assignmentLtiResource = createLtiResource()
            .setAssignment(assignment);
        LtiLaunch ltiLaunch = createLtiLaunch()
            .user(getStudent(entityManager))
            .ltiResource(assignmentLtiResource);
        persistCreatedEntities(entityManager);
        String updatedScore = "0.01";

        String response = mockMvc.perform(post(API_LTI_RESULTS)
                .accept(APPLICATION_XML)
                .contentType(APPLICATION_XML)
                .content(readFile("replace-result-request.xml", this.getClass())
                    .replaceFirst("\\{sourcedId}", ltiLaunch.getId().toString())
                    .replaceFirst("\\{result}", updatedScore)))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_XML_UTF_8))
            .andReturn()
            .getResponse()
            .getContentAsString();

        String expected = readFile("replace-result-response.xml", this.getClass())
            .replaceFirst("\\{sourcedId}", ltiLaunch.getId().toString())
            .replaceFirst("\\{score}", updatedScore);
        assertXmlEquals(expected, response, "imsx_messageIdentifier");
    }

    @Test
    @Transactional
    void shouldReturnUnsupportedResultForUnknownOperation() throws Exception {
        String response = mockMvc.perform(post(API_LTI_RESULTS)
                .accept(APPLICATION_XML)
                .contentType(APPLICATION_XML)
                .content(readFile("read-result-request.xml", this.getClass())))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_XML_UTF_8))
            .andReturn()
            .getResponse()
            .getContentAsString();

        String expected = readFile("unsupported-response.xml", this.getClass())
            .replaceAll("\\{operation}", "readResult");
        assertXmlEquals(expected, response, "imsx_messageIdentifier");
    }

    private static void assertXmlEquals(String expected, String actual, String... ignoredFields) {
        Diff xmlDiff = DiffBuilder
            .compare(expected)
            .withTest(actual)
            .withNodeFilter(node -> !contains(ignoredFields, node.getNodeName()))
            .build();
        assertFalse(xmlDiff.hasDifferences());
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void shouldAllowTeacherToAddLtiAppToJourney() throws Exception {
        LtiApp ltiApp = createLtiApp();
        persistCreatedEntities(entityManager);
        LtiConfig initialLtiConfig = createLtiConfig();
        initialLtiConfig.setLtiApp(ltiApp);
        initialLtiConfig.setJourney(journey);
        LtiConfigDTO ltiConfigDto = configMapper.toDto(initialLtiConfig);

        mockMvc.perform(post(API_LTI_CONFIG)
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(ltiConfigDto)))
            .andExpect(status().isCreated());

        List<LtiConfig> ltiConfigs = ltiConfigRepository.findAll();
        assertThat(ltiConfigs).hasSize(1);
        LtiConfig ltiConfig = ltiConfigs.get(0);
        assertThat(ltiConfig.getConsumerKey()).isEqualTo(LTI_CONFIG_CONSUMER_KEY);
        assertThat(ltiConfig.getSharedSecret()).isEqualTo(LTI_CONFIG_SHARED_SECRET);
        assertThat(ltiConfig.getType()).isEqualTo(LTI_DEFAULT_TYPE);

        LtiApp testApp = ltiConfig.getLtiApp();
        assertThat(testApp.getName()).isEqualTo(LTI_APP_NAME);
        assertThat(testApp.getDescription()).isEqualTo(LTI_APP_DESCRIPTION);
        assertThat(testApp.getVersion()).isEqualTo(LTI_APP_VERSION);
        assertThat(testApp.getLaunchUrl()).isEqualTo(LTI_APP_LAUNCH_URL);
        assertThat(testApp.getImageUrl()).isEqualTo(LTI_APP_IMAGE_URL);
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void shouldNotAllowStudentToAddLtiAppToJourney() throws Exception {
        LtiConfigDTO ltiConfigDTO = configMapper.toDto(createLtiConfig());

        mockMvc.perform(post(API_LTI_CONFIG)
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(ltiConfigDTO)))
            .andExpect(status().isForbidden());
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void shouldAllowTeacherToUpdateLtiConfig() throws Exception {
        LtiConfig ltiConfig = createLtiConfig();
        ltiConfig.setLtiApp(createLtiApp());
        persistCreatedEntities(entityManager);
        entityManager.detach(ltiConfig);
        ltiConfig.setConsumerKey(UPDATED_CONSUMER_KEY);
        ltiConfig.setSharedSecret(UPDATED_SHARED_SECRET);

        mockMvc.perform(put(API_LTI_CONFIG)
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(configMapper.toDto(ltiConfig))))
            .andExpect(status().isOk());

        List<LtiConfig> configs = ltiConfigRepository.findAll();
        assertThat(configs).hasSize(1);
        ltiConfig = configs.get(0);
        assertThat(ltiConfig.getConsumerKey()).isEqualTo(UPDATED_CONSUMER_KEY);
        assertThat(ltiConfig.getSharedSecret()).isEqualTo(UPDATED_SHARED_SECRET);
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void shouldNotAllowStudentToUpdateLtiConfig() throws Exception {
        mockMvc.perform(put(API_LTI_CONFIG)
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(configMapper.toDto(createLtiConfig()))))
            .andExpect(status().isForbidden());
    }

    @Test
    @Transactional
    @WithUserDetails(ADMIN_LOGIN)
    void getLtiLaunchParametersForAdmin() throws Exception {
        LtiApp ltiApp = createLtiApp();
        LtiConfig ltiConfig = createLtiConfig();
        ltiConfig.setType(JOURNEY);
        journey.addLtiConfig(ltiConfig);
        persistCreatedEntities(entityManager);
        ltiConfig.setJourney(journey);
        LtiResource assignmentLtiResource = createLtiResource()
            .setLtiApp(ltiApp.addConfig(ltiConfig))
            .setSyncGrade(true)
            .setAssignment(assignment);
        persistCreatedEntities(entityManager);

        ResultActions actions = mockMvc.perform(get("/api/lti/resource/launch/parameters")
            .param("ltiResourceId", assignmentLtiResource.getId().toString())
            .param("returnUrl", RETURN_URL));
        assertLtiBaseResponse(actions, ADMIN, true, true);
        assertBasicLtiLaunchResponse(actions);
        actions.andExpect(jsonPath("$.resource_link_id").value(LTI_RESOURCE_RESOURCE_LINK_ID));
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void getLtiLaunchParametersForTeacher() throws Exception {
        LtiApp ltiApp = createLtiApp();
        LtiConfig ltiConfig = createLtiConfig();
        ltiConfig.setType(JOURNEY);
        journey.addLtiConfig(ltiConfig);
        persistCreatedEntities(entityManager);
        ltiConfig.setJourney(journey);

        LtiResource assignmentLtiResource = createLtiResource()
            .setLtiApp(ltiApp.addConfig(ltiConfig))
            .setSyncGrade(true)
            .setAssignment(assignment);
        persistCreatedEntities(entityManager);

        ResultActions actions = mockMvc.perform(get("/api/lti/resource/launch/parameters")
            .param("ltiResourceId", assignmentLtiResource.getId().toString())
            .param("returnUrl", RETURN_URL));
        assertLtiBaseResponse(actions, INSTRUCTOR, true, true);
        assertBasicLtiLaunchResponse(actions);
        actions.andExpect(jsonPath("$.resource_link_id").value(LTI_RESOURCE_RESOURCE_LINK_ID));
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void getLtiLaunchParametersForPlatformConfig() throws Exception {
        LtiApp ltiApp = createLtiApp();
        LtiConfig ltiConfig = createLtiConfig();
        ltiConfig.setType(PLATFORM);
        persistCreatedEntities(entityManager);
        LtiResource assignmentLtiResource = createLtiResource()
            .setLtiApp(ltiApp.addConfig(ltiConfig))
            .setSyncGrade(true)
            .setAssignment(assignment);
        persistCreatedEntities(entityManager);

        ResultActions actions = mockMvc.perform(get("/api/lti/resource/launch/parameters")
            .param("ltiResourceId", assignmentLtiResource.getId().toString())
            .param("returnUrl", RETURN_URL));
        assertLtiBaseResponse(actions, INSTRUCTOR, true, true);
        assertBasicLtiLaunchResponse(actions);
        actions.andExpect(jsonPath("$.resource_link_id").value(LTI_RESOURCE_RESOURCE_LINK_ID));
    }

    @Test
    @WithUserDetails(value = STUDENT_LOGIN)
    void shouldSendWebsocketMessage() throws Exception {
        String authToken = testWebSocketClient.createStudentAuthToken();
        testWebSocketClient.connect(authToken, portLocal);
        BlockingQueue<LtiScoreDTO> blockingQueue = testWebSocketClient.subscribe(SUBSCRIBE_LTI_SCORE_ENDPOINT, LtiScoreDTO.class);

        LtiApp ltiApp = createLtiApp();
        LtiConfig ltiConfig = createLtiConfig();
        ltiConfig.setType(JOURNEY);
        ltiConfig.setJourney(journey);
        LtiResource assignmentLtiResource = createLtiResource()
            .setLtiApp(ltiApp.addConfig(ltiConfig))
            .setSyncGrade(true)
            .setAssignment(assignment);
        transactionHelper.withNewTransaction(() -> {
            journey
                .addStudent(getStudent(entityManager))
                .addTeacher(getTeacher(entityManager))
                .addLtiConfig(ltiConfig);
            persistCreatedEntities(entityManager);
        });

        ResultActions actions = mockMvc.perform(get("/api/lti/resource/launch/parameters")
            .param("ltiResourceId", assignmentLtiResource.getId().toString())
            .param("returnUrl", RETURN_URL)
        );

        assertLtiBaseResponse(actions, STUDENT, true, true);
        LtiScoreDTO ltiScoreDTO = blockingQueue.poll(1, SECONDS);
        assertNotNull(ltiScoreDTO);
        assertEquals(ltiScoreDTO.getResourceId(), assignmentLtiResource.getId());
        assertEquals(ltiScoreDTO.getTitle(), assignmentLtiResource.getTitle());
        assertEquals(STUDENT_ID, ltiScoreDTO.getUserId());
        assertNull(ltiScoreDTO.getScore());
        assertNull(ltiScoreDTO.getScoreMax());
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void getLtiLaunchParametersNotFoundForNonParticipant() throws Exception {
        LtiApp ltiApp = createLtiApp();
        LtiConfig ltiConfig = createLtiConfig();
        ltiConfig.setType(JOURNEY);
        journey.addLtiConfig(ltiConfig);
        persistCreatedEntities(entityManager);
        ltiConfig.setJourney(journey);
        LtiResource assignmentLtiResource = createLtiResource()
            .setLtiApp(ltiApp.addConfig(ltiConfig))
            .setSyncGrade(true)
            .setAssignment(assignment);
        persistCreatedEntities(entityManager);

        mockMvc.perform(get("/api/lti/resource/launch/parameters")
                .param("ltiResourceId", assignmentLtiResource.getId().toString())
                .param("returnUrl", RETURN_URL))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void getLtiLaunchParametersForParticipant() throws Exception {
        LtiApp ltiApp = createLtiApp();
        LtiConfig ltiConfig = createLtiConfig();
        ltiConfig.setType(JOURNEY);
        journey
            .addStudent(getStudent(entityManager))
            .addLtiConfig(ltiConfig);
        persistCreatedEntities(entityManager);
        ltiConfig.setJourney(journey);
        LtiResource assignmentLtiResource = createLtiResource()
            .setLtiApp(ltiApp.addConfig(ltiConfig))
            .setSyncGrade(true)
            .setAssignment(assignment);
        persistCreatedEntities(entityManager);

        ResultActions actions = mockMvc.perform(get("/api/lti/resource/launch/parameters")
            .param("ltiResourceId", assignmentLtiResource.getId().toString())
            .param("returnUrl", RETURN_URL)
        );
        assertLtiBaseResponse(actions, STUDENT, true, true);
        assertBasicLtiLaunchResponse(actions);
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void getAppLtiLaunchParametersForJourneyConfig() throws Exception {
        LtiApp ltiApp = createLtiApp();
        LtiConfig ltiConfig = createLtiConfig();
        ltiConfig.setType(JOURNEY);
        ltiConfig.setJourney(journey);
        ltiConfig.setLtiApp(ltiApp);

        transactionHelper.withNewTransaction(() -> {
            journey.addTeacher(getTeacher(entityManager));

            persistCreatedEntities(entityManager);
        });

        ResultActions actions = mockMvc.perform(get("/api/lti/app/launch/parameters")
            .param("ltiAppId", ltiApp.getId().toString())
            .param("journeyId", journey.getId().toString())
            .param("ltiResourceLinkId", LTI_RESOURCE_RESOURCE_LINK_ID)
            .param("returnUrl", RETURN_URL));
        assertLtiBaseResponse(actions, INSTRUCTOR, false, false);
        assertBasicLtiLaunchResponse(actions);
        actions.andExpect(jsonPath("$.resource_link_id").value(LTI_RESOURCE_RESOURCE_LINK_ID));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void getAppLtiLaunchParametersForContentItemSelection() throws Exception {
        LtiApp ltiApp = createLtiApp().setContentItemSelectionEnabled(true);
        LtiConfig ltiConfig = createLtiConfig();
        ltiConfig.setType(JOURNEY);
        ltiConfig.setJourney(journey);
        ltiConfig.setLtiApp(ltiApp);

        transactionHelper.withNewTransaction(() -> {
            journey.addTeacher(getTeacher(entityManager));

            persistCreatedEntities(entityManager);
        });

        ResultActions actions = mockMvc.perform(get("/api/lti/app/launch/parameters")
            .param("ltiAppId", ltiApp.getId().toString())
            .param("journeyId", journey.getId().toString())
            .param("ltiResourceLinkId", LTI_RESOURCE_RESOURCE_LINK_ID)
            .param("returnUrl", RETURN_URL));
        assertLtiBaseResponse(actions, INSTRUCTOR, false, false);
        assertContentItemSelectionResponse(actions);
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldGetAppLtiLaunchParametersWhenAnotherAppPlatformConfigExists() throws Exception {
        LtiApp ltiApp = createLtiApp();
        LtiConfig ltiConfig1 = createLtiConfig();
        ltiConfig1.setType(PLATFORM);
        ltiConfig1.setLtiApp(createLtiApp());

        LtiConfig ltiConfig = createLtiConfig();
        ltiConfig.setType(JOURNEY);
        ltiConfig.setJourney(journey);
        ltiConfig.setLtiApp(ltiApp);

        transactionHelper.withNewTransaction(() -> {
            journey.addTeacher(getTeacher(entityManager));

            persistCreatedEntities(entityManager);
        });

        ResultActions actions = mockMvc.perform(get("/api/lti/app/launch/parameters")
            .param("ltiAppId", ltiApp.getId().toString())
            .param("journeyId", journey.getId().toString())
            .param("ltiResourceLinkId", LTI_RESOURCE_RESOURCE_LINK_ID)
            .param("returnUrl", RETURN_URL));
        assertLtiBaseResponse(actions, INSTRUCTOR, false, false);
        assertBasicLtiLaunchResponse(actions);
        actions.andExpect(jsonPath("$.resource_link_id").value(LTI_RESOURCE_RESOURCE_LINK_ID));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void getAppLtiLaunchParametersForPlatformConfig() throws Exception {
        LtiApp ltiApp = createLtiApp();
        LtiConfig ltiConfig = createLtiConfig();
        ltiConfig.setType(PLATFORM);
        ltiConfig.setLtiApp(ltiApp);

        transactionHelper.withNewTransaction(() -> {
            journey.addTeacher(getTeacher(entityManager));

            persistCreatedEntities(entityManager);
        });

        ResultActions actions = mockMvc.perform(get("/api/lti/app/launch/parameters")
            .param("ltiAppId", ltiApp.getId().toString())
            .param("journeyId", journey.getId().toString())
            .param("ltiResourceLinkId", LTI_RESOURCE_RESOURCE_LINK_ID)
            .param("returnUrl", RETURN_URL));
        assertLtiBaseResponse(actions, INSTRUCTOR, false, false);
        assertBasicLtiLaunchResponse(actions);
        actions.andExpect(jsonPath("$.resource_link_id").value(LTI_RESOURCE_RESOURCE_LINK_ID));
    }

    @Test
    @Transactional
    void postDeepLinkingResponse() throws Exception {
        ResultActions actions = mockMvc.perform(post("/api/lti/deep-linking/response").contentType(APPLICATION_FORM_URLENCODED)
            .content(EntityUtils.toString(new UrlEncodedFormEntity(singletonList(
                new BasicNameValuePair("content_items", readFile("content-items.json", this.getClass()))
            )))));

        actions
            .andExpect(content().string(containsString("https://assignments.google.com/lti/a/123123123/223223223")))
            .andExpect(content().string(containsString("totalMaximum")));
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void getAppLtiLaunchParametersForbiddenForStudent() throws Exception {
        LtiApp ltiApp = createLtiApp();
        LtiConfig ltiConfig = createLtiConfig();
        ltiConfig.setType(JOURNEY);
        ltiConfig.setLtiApp(createLtiApp());
        journey.addLtiConfig(ltiConfig);
        persistCreatedEntities(entityManager);

        mockMvc.perform(get("/api/lti/app/launch/parameters")
                .param("ltiAppId", ltiApp.getId().toString())
                .param("journeyId", journey.getId().toString())
                .param("ltiResourceLinkId", LTI_RESOURCE_RESOURCE_LINK_ID)
                .param("returnUrl", RETURN_URL))
            .andExpect(status().isForbidden());
    }

    @Test
    @Transactional
    @WithUserDetails(ADMIN_LOGIN)
    void getLtiConfigForAdmin() throws Exception {
        LtiApp ltiApp = createLtiApp();
        LtiConfig journeyLtiConfig = createLtiConfig();
        journeyLtiConfig.setType(JOURNEY);
        journeyLtiConfig.setJourney(journey);
        journeyLtiConfig.setLtiApp(ltiApp);
        LtiConfig platformLtiConfig = createLtiConfig();
        platformLtiConfig.setType(PLATFORM);
        platformLtiConfig.setLtiApp(ltiApp);
        persistCreatedEntities(entityManager);

        mockMvc
            .perform(get(API_LTI_CONFIG).param("journeyId", journey.getId().toString()))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[0].consumerKey").value(journeyLtiConfig.getConsumerKey()))
            .andExpect(jsonPath("$[0].sharedSecret").value(journeyLtiConfig.getSharedSecret()))
            .andExpect(jsonPath("$[1].consumerKey").value(platformLtiConfig.getConsumerKey()))
            .andExpect(jsonPath("$[1].sharedSecret").value(platformLtiConfig.getSharedSecret()));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void getLtiConfigForJourneyTeacher() throws Exception {
        LtiApp ltiApp = createLtiApp();
        LtiConfig journeyLtiConfig = createLtiConfig();
        journeyLtiConfig.setType(JOURNEY);
        journeyLtiConfig.setJourney(journey);
        journeyLtiConfig.setLtiApp(ltiApp);
        LtiConfig platformLtiConfig = createLtiConfig();
        platformLtiConfig.setType(PLATFORM);
        platformLtiConfig.setLtiApp(ltiApp);


        transactionHelper.withNewTransaction(() -> {
            journey.addTeacher(getTeacher(entityManager));

            persistCreatedEntities(entityManager);
        });

        mockMvc
            .perform(get(API_LTI_CONFIG).param("journeyId", journey.getId().toString()))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[0].consumerKey").value(journeyLtiConfig.getConsumerKey()))
            .andExpect(jsonPath("$[0].sharedSecret").value(journeyLtiConfig.getSharedSecret()))
            .andExpect(jsonPath("$[1].consumerKey").value(platformLtiConfig.getConsumerKey()))
            .andExpect(jsonPath("$[1].sharedSecret").value(platformLtiConfig.getSharedSecret()));
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void getLtiConfigForbiddenForStudent() throws Exception {
        persistCreatedEntities(entityManager);

        mockMvc
            .perform(get(API_LTI_CONFIG, journey.getId()).param("journeyId", journey.getId().toString()))
            .andExpect(status().isForbidden());
    }

    @Test
    @Transactional
    @WithUserDetails(ADMIN_LOGIN)
    void getLtiAppsForAdmin() throws Exception {
        createLtiApp();
        persistCreatedEntities(entityManager);

        mockMvc
            .perform(get("/api/lti/app"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[0].name").value(LTI_APP_NAME))
            .andExpect(jsonPath("$[0].description").value(LTI_APP_DESCRIPTION))
            .andExpect(jsonPath("$[0].imageUrl").value(LTI_APP_IMAGE_URL))
            .andExpect(jsonPath("$[0].version").value(LTI_APP_VERSION));
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void getLtiAppsForTeacher() throws Exception {
        createLtiApp();
        persistCreatedEntities(entityManager);

        mockMvc
            .perform(get("/api/lti/app"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[0].name").value(LTI_APP_NAME))
            .andExpect(jsonPath("$[0].description").value(LTI_APP_DESCRIPTION))
            .andExpect(jsonPath("$[0].imageUrl").value(LTI_APP_IMAGE_URL))
            .andExpect(jsonPath("$[0].version").value(LTI_APP_VERSION));
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void getLtiAppsForStudentForbidden() throws Exception {
        mockMvc.perform(get("/api/lti/app")).andExpect(status().isForbidden());
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldDeleteLtiConfig() throws Exception {
        transactionHelper.withTransaction(() -> {
            journey.addTeacher(getTeacher(entityManager));
            ltiConfig = createLtiConfig();
            ltiConfig.setType(JOURNEY);
            ltiConfig.setJourney(journey);
            persistCreatedEntities(entityManager);
        });

        int databaseSizeBeforeCreate = ltiConfigRepository.findAll().size();

        mockMvc.perform(delete("/api/lti/config/{id}", ltiConfig.getId())).andExpect(status().isNoContent());

        List<LtiConfig> configList = ltiConfigRepository.findAll();
        assertThat(configList).hasSize(databaseSizeBeforeCreate - 1);
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void deleteLtiConfigForStudentForbidden() throws Exception {
        LtiConfig ltiConfig = createLtiConfig();
        persistCreatedEntities(entityManager);

        mockMvc.perform(delete("/api/lti/config/{id}", ltiConfig.getId())).andExpect(status().isForbidden());
    }

    @Test
    @Transactional
    @WithUserDetails(ADMIN_LOGIN)
    void getLtiResourcesForAssignment() throws Exception {
        LtiApp ltiApp = createLtiApp();
        LtiConfig ltiConfig = createLtiConfig();
        ltiConfig.setType(JOURNEY);
        ltiConfig.setJourney(journey);
        journey.addStudent(getStudent(entityManager))
            .addLtiConfig(ltiConfig);
        persistCreatedEntities(entityManager);
        LtiResource assignmentLtiResource = createLtiResource()
            .setLtiApp(ltiApp)
            .setSyncGrade(true)
            .setAssignment(assignment);
        persistCreatedEntities(entityManager);

        mockMvc
            .perform(get("/api/lti/resource?assignmentId", assignment.getId()))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[0].id").value(assignmentLtiResource.getId()))
            .andExpect(jsonPath("$[0].resourceLinkId").value(assignmentLtiResource.getResourceLinkId()))
            .andExpect(jsonPath("$[0].syncGrade").value(true));
    }

    @Test
    @Transactional
    @WithUserDetails(ADMIN_LOGIN)
    void getLtiResourcesForMilestone() throws Exception {
        LtiApp ltiApp = createLtiApp();
        LtiConfig ltiConfig = createLtiConfig();
        ltiConfig.setType(JOURNEY);
        ltiConfig.setJourney(journey);

        journey.addStudent(getStudent(entityManager))
            .addLtiConfig(ltiConfig);
        persistCreatedEntities(entityManager);
        LtiResource milestoneLtiResource = createLtiResource()
            .setLtiApp(ltiApp)
            .setSyncGrade(true)
            .setMilestone(milestone);
        persistCreatedEntities(entityManager);

        mockMvc
            .perform(get("/api/lti/resource?milestoneId", milestone.getId()))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[0].id").value(milestoneLtiResource.getId()))
            .andExpect(jsonPath("$[0].resourceLinkId").value(milestoneLtiResource.getResourceLinkId()));
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void getLtiLaunchesForTeacher() throws Exception {
        LtiApp ltiApp = createLtiApp();
        LtiConfig ltiConfig = createLtiConfig();
        ltiConfig.setType(JOURNEY);
        ltiConfig.setJourney(journey);
        journey.addStudent(getStudent(entityManager))
            .addLtiConfig(ltiConfig);
        persistCreatedEntities(entityManager);
        LtiResource assignmentLtiResource = createLtiResource()
            .setLtiApp(ltiApp)
            .setSyncGrade(true)
            .setAssignment(assignment);
        LtiLaunch ltiLaunch = createLtiLaunch()
            .user(getStudent(entityManager))
            .ltiResource(assignmentLtiResource);
        persistCreatedEntities(entityManager);

        mockMvc.perform(get("/api/lti/launch")
                .param("userId", STUDENT_ID.toString())
                .param("assignmentId", assignment.getId().toString()))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[0].id").value(ltiLaunch.getId()))
            .andExpect(jsonPath("$[0].result").value(ltiLaunch.getResult()));
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void getLtiLaunchesForParticipant() throws Exception {
        LtiApp ltiApp = createLtiApp();
        LtiConfig ltiConfig = createLtiConfig();
        ltiConfig.setType(JOURNEY);
        ltiConfig.setJourney(journey);
        journey.addLtiConfig(ltiConfig);
        journey.addStudent(getStudent(entityManager));
        persistCreatedEntities(entityManager);
        LtiResource assignmentLtiResource = createLtiResource()
            .setLtiApp(ltiApp)
            .setSyncGrade(true)
            .setAssignment(assignment);
        LtiLaunch ltiLaunch = createLtiLaunch()
            .user(getStudent(entityManager))
            .ltiResource(assignmentLtiResource);
        persistCreatedEntities(entityManager);

        mockMvc.perform(get("/api/lti/launch")
                .param("userId", STUDENT_ID.toString())
                .param("assignmentId", assignment.getId().toString()))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[0].id").value(ltiLaunch.getId()))
            .andExpect(jsonPath("$[0].result").value(ltiLaunch.getResult()));
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void getLtiLaunchesForNonParticipant() throws Exception {
        LtiApp ltiApp = createLtiApp();
        LtiConfig ltiConfig = createLtiConfig();
        ltiConfig.setType(JOURNEY);
        ltiConfig.setJourney(journey);
        journey.addLtiConfig(ltiConfig);
        journey.addStudent(getStudent(entityManager));
        persistCreatedEntities(entityManager);
        LtiResource assignmentLtiResource = createLtiResource()
            .setLtiApp(ltiApp)
            .setSyncGrade(true)
            .setAssignment(assignment);
        createLtiLaunch()
            .user(getTeacher(entityManager))
            .ltiResource(assignmentLtiResource);
        persistCreatedEntities(entityManager);

        mockMvc.perform(get("/api/lti/launch")
                .param("userId", TEACHER_ID.toString())
                .param("assignmentId", assignment.getId().toString()))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$").isEmpty());
    }

    private void assertContentItemSelectionResponse(ResultActions actions) throws Exception {
        actions
            .andExpect(jsonPath("$.content_item_return_url").value("https://localhost/api/lti/deep-linking/response"))
            .andExpect(jsonPath("$.accept_media_types").value("application/vnd.ims.lti.v1.ltilink"))
            .andExpect(jsonPath("$.accept_multiple").value(false))
            .andExpect(jsonPath("$.accept_presentation_document_targets").value("iframe"));
    }

    private void assertBasicLtiLaunchResponse(ResultActions actions) throws Exception {
        actions
            .andExpect(jsonPath("$.lti_message_type").value(LTI_MESSAGE_TYPE))
            .andExpect(jsonPath("$.launch_presentation_return_url").value(RETURN_URL))
            .andExpect(jsonPath("$.launch_presentation_document_target").value(LAUNCH_PRESENTATION_DOCUMENT_TARGET));
    }

    private void assertLtiBaseResponse(ResultActions actions, LtiRole role, boolean hasGradingParameters, boolean hasResourceTitle) throws Exception {
        actions
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.lti_version").value(LTI_APP_VERSION))
            .andExpect(jsonPath("$.oauth_signature").exists())
            .andExpect(jsonPath("$.roles").value(role.getRoleName()))
            .andExpect(jsonPath("$.oauth_consumer_key").value(LTI_CONFIG_CONSUMER_KEY))
            .andExpect(jsonPath("$.oauth_version").value(OAUTH_VERSION))
            .andExpect(jsonPath("$.oauth_timestamp").exists())
            .andExpect(jsonPath("$.oauth_signature_method").value(SIGNATURE_METHOD))
            .andExpect(jsonPath("$.launch_url").value(LTI_APP_LAUNCH_URL))
            .andExpect(jsonPath("$.oauth_nonce").exists())
            .andExpect(jsonPath("$.user_id").exists())
            .andExpect(jsonPath("$.context_id").value(journey.getId()))
            .andExpect(jsonPath("$.context_type").value(LTI_CONTEXT_TYPE))
            .andExpect(jsonPath("$.context_title").value(journey.getTitle()))
            .andExpect(jsonPath("$.context_label").value(String.format("J%s", journey.getId())))
            .andExpect(jsonPath("$.tool_consumer_instance_guid").value(TOOL_CONSUMER_INSTANCE_GUID))
            .andExpect(jsonPath("$.tool_consumer_info_product_family_code").value(TOOL_CONSUMER_INFO_PRODUCT_FAMILY_CODE))
            .andExpect(jsonPath("$.tool_consumer_info_version").value(TOOL_CONSUMER_INFO_VERSION))
            .andExpect(jsonPath("$.tool_consumer_instance_name").value(TOOL_CONSUMER_INSTANCE_NAME))
            .andExpect(jsonPath("$.tool_consumer_instance_contact_email").value(TOOL_CONSUMER_INSTANCE_CONTACT_EMAIL))
            .andExpect(jsonPath("$.lis_outcome_service_url").value(LIS_OUTCOMES_URL));

        if (hasResourceTitle) {
            actions
                .andExpect(jsonPath("$.resource_link_title").value(LTI_RESOURCE_TITLE));
        } else {
            actions
                .andExpect(jsonPath("$.resource_link_title").doesNotExist());
        }

        if (hasGradingParameters) {
            actions
                .andExpect(jsonPath("$.lis_result_sourcedid").isNotEmpty());
        } else {
            actions
                .andExpect(jsonPath("$.lis_result_sourcedid").doesNotExist());
        }
    }
}
