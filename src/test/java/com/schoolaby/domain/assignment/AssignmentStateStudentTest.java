package com.schoolaby.domain.assignment;

import com.schoolaby.domain.*;
import com.schoolaby.service.dto.states.EntityState;
import com.schoolaby.web.WithMockCustomUser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static com.schoolaby.common.TestObjects.*;
import static com.schoolaby.security.Role.Constants.STUDENT;
import static com.schoolaby.security.Role.Constants.TEACHER;
import static com.schoolaby.security.SecurityUtils.getCurrentUserId;
import static com.schoolaby.service.dto.states.EntityState.*;
import static com.schoolaby.web.rest.TestUtil.getDeadlineInDays;
import static java.util.Collections.singleton;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@WithMockCustomUser(authorities = {STUDENT}, userId = "1")
class AssignmentStateStudentTest {

    @Mock
    private Submission mockSubmission;
    private Assignment assignment;

    @BeforeEach
    void setup() {
        User teacher = createUser()
            .id(2L)
            .addAuthority(new Authority().name(TEACHER));
        Journey journey = createJourney();
        assignment = createAssignment()
            .id(1L);
        Milestone milestone = createMilestone();
        GradingScheme gradingScheme = createGradingScheme()
            .addValue(createGradingSchemeValue().setGrade("5").setPercentageRange(100))
            .addValue(createGradingSchemeValue().setGrade("1").setPercentageRange(1));
        journey
            .creator(teacher)
            .addTeacher(teacher)
            .addStudent(getCurrentUser())
            .addMilestone(milestone
                .creator(teacher)
                .addAssignment(assignment
                    .creator(teacher)
                    .gradingScheme(gradingScheme)));
        when(mockSubmission.getAuthors()).thenReturn(singleton(getCurrentUser()));
    }

    @Test
    void shouldBeNotStartedWhenDueByInFutureAndNoSubmission() {
        assignment
            .requiresSubmission(true)
            .deadline(getDeadlineInDays(2));

        assignment.calculateStates();
        assertEquals(NOT_STARTED, assignment.getState());
    }

    @Test
    void shouldBeUrgentWhenDueByTomorrowAndNoSubmission() {
        assignment
            .deadline(getDeadlineInDays(1))
            .getState();

        assignment.calculateStates();
        assertEquals(URGENT, assignment.getState());
    }

    @Test
    void shouldBeUrgentWhenDueByTomorrowAndSubmissionInProgress() {
        User student = new User().setId(1L);
        when(mockSubmission.isStudentSubmissionInProgress(student.getId())).thenReturn(true);
        assignment
            .deadline(getDeadlineInDays(1))
            .addSubmission(mockSubmission);

        assignment.calculateStates();
        assertEquals(URGENT, assignment.getState());
    }

    @Test
    void shouldBeFailedWhenSubmissionStatusFailed() {
        when(mockSubmission.hasStudentFailed(anyLong())).thenReturn(true);
        when(mockSubmission.hasAuthor(any())).thenReturn(true);
        assignment
            .requiresSubmission(true)
            .deadline(getDeadlineInDays(1))
            .addSubmission(mockSubmission);

        assignment.calculateStates();
        assertEquals(FAILED, assignment.getState());
    }

    @Test
    void shouldBeCompletedWhenSubmissionCompleted() {
        when(mockSubmission.hasStudentCompleted(anyLong())).thenReturn(true);
        when(mockSubmission.hasAuthor(any())).thenReturn(true);
        assignment
            .requiresSubmission(true)
            .deadline(getDeadlineInDays(1))
            .addSubmission(mockSubmission);

        assignment.calculateStates();
        assertEquals(COMPLETED, assignment.getState());
    }

    @Test
    void shouldBeInProgressWhenSubmissionSubmitted() {
        when(mockSubmission.hasStudentSubmitted(anyLong())).thenReturn(true);
        when(mockSubmission.hasAuthor(any())).thenReturn(true);
        assignment
            .requiresSubmission(true)
            .deadline(getDeadlineInDays(1))
            .addSubmission(mockSubmission);

        assignment.calculateStates();
        assertEquals(IN_PROGRESS, assignment.getState());
    }

    @Test
    void shouldBeOverdueWhenStudentHasNoSubmissionAndDeadlineIsCrossed() {
        assignment
            .deadline(getDeadlineInDays(-1))
            .flexibleDeadline(true)
            .requiresSubmission(true);

        assignment.calculateStates();
        assertEquals(OVERDUE, assignment.getState());
    }

    @Test
    void shouldBeFailedWhenStudentHasNoSubmissionAndDeadlineIsCrossed() {
        assignment
            .requiresSubmission(true)
            .deadline(getDeadlineInDays(-1))
            .flexibleDeadline(false);

        assignment.calculateStates();
        assertEquals(FAILED, assignment.getState());
    }

    @Test
    void shouldBeOverdueWhenSubmissionInProgressAndDeadlineIsCrossed() {
        User student = new User().setId(1L);
        when(mockSubmission.isStudentSubmissionInProgress(student.getId())).thenReturn(true);
        assignment
            .deadline(getDeadlineInDays(-1))
            .addSubmission(mockSubmission)
            .flexibleDeadline(true)
            .requiresSubmission(true);

        assignment.calculateStates();
        assertEquals(OVERDUE, assignment.getState());
    }

    @Test
    void shouldBeRejectedWhenSubmissionIsRejected() {
        when(mockSubmission.isStudentSubmissionRejected(anyLong())).thenReturn(true);
        when(mockSubmission.hasAuthor(any())).thenReturn(true);
        assignment
            .deadline(getDeadlineInDays(-1))
            .addSubmission(mockSubmission)
            .flexibleDeadline(true)
            .requiresSubmission(true);

        assignment.calculateStates();
        assertEquals(REJECTED, assignment.getState());
    }

    @Test
    void shouldBeNotStartedWhenSubmissionsNotRequiredAndDeadlineInTheFuture() {
        Submission submission = createSubmission()
            .addSubmissionFeedback(createSubmissionFeedback().grade("1"));
        assignment
            .requiresSubmission(false)
            .deadline(getDeadlineInDays(10))
            .addSubmission(submission);

        assignment.calculateStates();
        assertEquals(NOT_STARTED, assignment.getState());
    }

    @Test
    void shouldBeUrgentWhenSubmissionsNotRequiredAndDeadlineTomorrow() {
        Submission submission = createSubmission()
            .addSubmissionFeedback(createSubmissionFeedback().grade("1"));
        assignment
            .requiresSubmission(false)
            .deadline(getDeadlineInDays(1))
            .addSubmission(submission);

        assignment.calculateStates();
        assertEquals(URGENT, assignment.getState());
    }

    @Test
    void shouldBeCompletedWhenSubmissionsNotRequiredAndDeadlinePassed() {
        Submission submission = createSubmission()
            .addSubmissionFeedback(createSubmissionFeedback().grade("1"));
        assignment
            .requiresSubmission(false)
            .deadline(getDeadlineInDays(-1))
            .addSubmission(submission);

        assignment.calculateStates();
        assertEquals(COMPLETED, assignment.getState());
    }

    @Test
    void shouldBeFailedWhenNegativeGradeGivenAndReSubmittableTrueAndJourneyEndDatePassed() {
        assignment.getMilestone().getJourney().setEndDate(getDeadlineInDays(-1));
        Submission submission = createSubmission()
            .resubmittable(true)
            .addSubmissionFeedback(createSubmissionFeedback().grade("1"));
        assignment
            .requiresSubmission(true)
            .deadline(getDeadlineInDays(-1))
            .addSubmission(submission);

        assignment.calculateStates();
        assertEquals(FAILED, assignment.getState());
    }

    private User getCurrentUser() {
        return createUser().id(1L);
    }
}
