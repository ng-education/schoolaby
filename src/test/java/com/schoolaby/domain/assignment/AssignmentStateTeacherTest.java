package com.schoolaby.domain.assignment;

import com.schoolaby.domain.*;
import com.schoolaby.service.dto.states.EntityState;
import com.schoolaby.web.WithMockCustomUser;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Spy;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Set;

import static com.schoolaby.common.TestObjects.*;
import static com.schoolaby.security.Role.Constants.STUDENT;
import static com.schoolaby.security.Role.Constants.TEACHER;
import static com.schoolaby.service.dto.states.EntityState.*;
import static com.schoolaby.web.rest.TestUtil.getDeadlineInDays;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@WithMockCustomUser(authorities = {TEACHER})
class AssignmentStateTeacherTest {

    @Spy
    private Submission submissionPositiveGrade;
    @Spy
    private Submission submissionSubmittedAndNotGraded;
    @Spy
    private Submission submissionFilesAddedButNotSubmitted;
    @Spy
    private Submission submissionNegativeGradeAndNotResubmittable;
    @Spy
    private Submission submissionNegativeGradeAndResubmittable;

    private void setupGradedSubmissions() {
        when(submissionPositiveGrade.isCompleted()).thenReturn(true);
        when(submissionNegativeGradeAndNotResubmittable.isFailed()).thenReturn(true);
        when(submissionNegativeGradeAndResubmittable.isRejected()).thenReturn(true);
    }

    @Test
    void shouldBeCompletedWhenDueByInFutureAndAllSubmissionsAreGraded() {
        Assignment assignment = generateIndividualAssignment(-2,
            false,
            submissionPositiveGrade,
            submissionNegativeGradeAndNotResubmittable,
            submissionNegativeGradeAndResubmittable
        );

        setupGradedSubmissions();

        assignment.calculateStates();
        assertEquals(COMPLETED, assignment.getState());
    }

    @Test
    void shouldBeCompletedWhenDeadlinePassedAndAllSubmissionsAreGraded() {
        Assignment assignment = generateIndividualAssignment(-1,
            false,
            submissionPositiveGrade,
            submissionNegativeGradeAndNotResubmittable,
            submissionNegativeGradeAndResubmittable
        );

        setupGradedSubmissions();
        assignment.calculateStates();
        assertEquals(COMPLETED, assignment.getState());
    }

    @Test
    void shouldBeCompletedWhenDeadlinePassedAndNoUsersInJourney() {
        Assignment assignment = generateIndividualAssignment(-1, false);

        assignment.calculateStates();
        assertEquals(COMPLETED, assignment.getState());
    }

    @Test
    void shouldBeUrgentWhenDueByTomorrowAndAtLeastOneSubmissionMissing() {
        Assignment assignment = generateIndividualAssignment(1,
            true,
            submissionPositiveGrade);

        assignment.calculateStates();
        assertEquals(URGENT, assignment.getState());
    }

    @Test
    void shouldBeUrgentWhenDueByTomorrowAndAtLeastOneSubmissionInProgress() {
        Assignment assignment = generateIndividualAssignment(1,
            false,
            submissionPositiveGrade,
            submissionFilesAddedButNotSubmitted);

        assignment.calculateStates();
        assertEquals(URGENT, assignment.getState());
    }

    @Test
    void shouldBeUrgentWhenDueByTomorrowAndAtLeastOneSubmissionSubmitted() {
        Assignment assignment = generateIndividualAssignment(1,
            false,
            submissionPositiveGrade,
            submissionSubmittedAndNotGraded);

        assignment.calculateStates();
        assertEquals(URGENT, assignment.getState());
    }

    @Test
    void shouldBeOverdueWhenDeadlinePassedAndAtLeastOneSubmissionMissing() {
        Assignment assignment = generateIndividualAssignment(-1,
            true,
            submissionPositiveGrade);

        assignment.calculateStates();
        assertEquals(OVERDUE, assignment.getState());
    }

    @Test
    void shouldBeOverdueWhenDeadlinePassedAndAtLeastOneSubmissionInProgress() {
        Assignment assignment = generateIndividualAssignment(-1,
            false,
            submissionPositiveGrade,
            submissionFilesAddedButNotSubmitted
        );

        assignment.calculateStates();
        assertEquals(OVERDUE, assignment.getState());
    }

    @Test
    void shouldBeOverdueWhenDeadlinePassedAndAtLeastOneSubmissionSubmitted() {
        Assignment assignment = generateIndividualAssignment(-1,
            false,
            submissionPositiveGrade,
            submissionSubmittedAndNotGraded);

        assignment.calculateStates();
        assertEquals(OVERDUE, assignment.getState());
    }

    @Test
    void shouldBeNotStartedWhenDueByInFutureAndNoUsersInJourney() {
        Assignment assignment = generateIndividualAssignment(2, false);

        assignment.calculateStates();
        assertEquals(NOT_STARTED, assignment.getState());
    }

    @Test
    void shouldBeNotStartedWhenDueByTomorrowAndNoUsersInJourney() {
        Assignment assignment = generateIndividualAssignment(1, false);

        assignment.calculateStates();
        assertEquals(NOT_STARTED, assignment.getState());
    }

    @Test
    void shouldBeNotStartedWhenDueByInFutureAndSubmissionStatusesAreCompletedAndSubmittedAndFailed() {
        Assignment assignment = generateIndividualAssignment(2,
            false,
            submissionPositiveGrade,
            submissionSubmittedAndNotGraded,
            submissionNegativeGradeAndNotResubmittable);

        assignment.calculateStates();
        assertEquals(NOT_STARTED, assignment.getState());
    }

    @Test
    void shouldBeNotStartedWhenDueByInFutureAndAllSubmissionStatesArePresent() {
        Assignment assignment = generateIndividualAssignment(2,
            false,
            submissionPositiveGrade,
            submissionSubmittedAndNotGraded,
            submissionNegativeGradeAndNotResubmittable,
            submissionFilesAddedButNotSubmitted,
            submissionNegativeGradeAndResubmittable
        );

        assignment.calculateStates();
        assertEquals(NOT_STARTED, assignment.getState());
    }

    @Test
    void shouldBeInProgressWhenDueByInFutureAndOneSubmissionMissing() {
        Assignment assignment = generateIndividualAssignment(2,
            false,
            submissionPositiveGrade,
            submissionSubmittedAndNotGraded,
            submissionNegativeGradeAndNotResubmittable,
            submissionFilesAddedButNotSubmitted,
            submissionNegativeGradeAndResubmittable
        );

        assignment.calculateStates();
        assertEquals(NOT_STARTED, assignment.getState());
    }

    @Test
    void shouldBeUrgentWhenDueByTomorrowAndAllGroupsNotGraded() {
        Submission positiveSubmission = createSubmission();
        Assignment assignment = generateGroupAssignment(1,
            true,
            false,
            positiveSubmission
        );

        assignment.calculateStates();
        assertEquals(URGENT, assignment.getState());
    }

    @Test
    void shouldBeCompletedWhenDueByInFutureAndAllGroupsGraded() {
        Submission positiveSubmission = createSubmissionWithPositiveFeedback();
        Assignment assignment = generateGroupAssignment(1,
            false,
            false,
            positiveSubmission
        );

        assignment.calculateStates();
        assertEquals(COMPLETED, assignment.getState());
    }

    @Test
    void shouldBeCompletedWhenDueByInPastAndAllGroupsGraded() {
        Submission positiveSubmission = createSubmissionWithPositiveFeedback();
        Assignment assignment = generateGroupAssignment(-1,
            false,
            false,
            positiveSubmission
        );

        assignment.calculateStates();
        assertEquals(COMPLETED, assignment.getState());
    }

    @Test
    void shouldBeCompletedWhenDueByInPastAndAllGroupMembersAreGradedIndividually() {
        Submission positiveSubmission = createSubmission();
        Submission negativeSubmission = createSubmission();
        Assignment assignment = generateGroupAssignment(-1,
            false,
            true,
            positiveSubmission,
            negativeSubmission
        ).requiresSubmission(false);

        assignment.calculateStates();
        assertEquals(COMPLETED, assignment.getState());
    }

    @Test
    void shouldBeOverdueWhenDueByInPastAndOneGroupNotGradedAndOneGroupMembersGradedIndividually() {
        Submission positiveSubmission = createSubmission();
        Assignment assignment = generateGroupAssignment(-1,
            true,
            true,
            positiveSubmission
        ).flexibleDeadline(true);

        assignment.calculateStates();
        assertEquals(OVERDUE, assignment.getState());
    }

    @Test
    void shouldBeOverdueWhenDueByInPastAndAllGroupsNotGraded() {
        Submission positiveSubmission = createSubmission();
        Assignment assignment = generateGroupAssignment(-1,
            true,
            false,
            positiveSubmission
        ).flexibleDeadline(true);

        assignment.calculateStates();
        assertEquals(OVERDUE, assignment.getState());
    }

    @Test
    void shouldBeNotStartedWhenSubmissionsNotRequiredAndDeadlineInTheFuture() {
        Submission negativeSubmission = createSubmissionWithNegativeFeedback();
        Assignment assignment = generateIndividualAssignment(10, false, negativeSubmission)
            .requiresSubmission(false);

        assignment.calculateStates();
        assertEquals(NOT_STARTED, assignment.getState());
    }

    @Test
    void shouldBeUrgentWhenSubmissionsNotRequiredAndDeadlineTomorrow() {
        Submission negativeSubmission = createSubmissionWithNegativeFeedback();
        Assignment assignment = generateIndividualAssignment(1, false, negativeSubmission).requiresSubmission(false);

        assignment.calculateStates();
        assertEquals(URGENT, assignment.getState());
    }

    @Test
    void shouldBeCompletedWhenSubmissionsNotRequiredAndDeadlinePassed() {
        Submission negativeSubmission = createSubmissionWithNegativeFeedback();
        Assignment assignment = generateIndividualAssignment(-1, false, negativeSubmission)
            .requiresSubmission(false);

        assignment.calculateStates();
        assertEquals(COMPLETED, assignment.getState());
    }

    private Assignment generateAssignmentInJourney(int daysAwayFromDeadline,
                                                   Submission... submissions) {
        User teacher = createUser()
            .id(2L)
            .addAuthority(new Authority().name(TEACHER));
        Journey journey = createJourney();
        Assignment assignment = createAssignment()
            .id(1L)
            .requiresSubmission(true);
        Milestone milestone = createMilestone();
        journey
            .creator(teacher)
            .addTeacher(teacher)
            .addMilestone(milestone
                .creator(teacher)
                .addAssignment(assignment
                    .creator(teacher)
                    .deadline(getDeadlineInDays(daysAwayFromDeadline))
                    .gradingScheme(createGradingScheme().addValue(createGradingSchemeValue()))
                    .submissions(Set.of(submissions))));

        return assignment;
    }

    private Assignment generateIndividualAssignment(int daysAwayFromDeadline, boolean isOnePersonWithoutSubmission, Submission... submissions) {
        Assignment assignment = generateAssignmentInJourney(daysAwayFromDeadline, submissions);
        JourneyTeacher journeyTeacher = createJourneyTeacher()
            .journey(assignment.getMilestone().getJourney())
            .user(createUser().id(2L));
        Journey journey = assignment.getMilestone().getJourney().teachers(Set.of(journeyTeacher));

        long userId = 3L;
        for (Submission ignored : submissions) {
            User student = createUser().setId(userId++).addAuthority(new Authority().name(STUDENT));
            journey.addStudent(student);
            ignored.addAuthor(student);
            ignored.assignment(assignment);
        }

        if (isOnePersonWithoutSubmission) {
            journey.addStudent(new User().setId(1L)
                .addAuthority(new Authority().name(STUDENT)));
        }

        return assignment;
    }

    private Assignment generateGroupAssignment(int daysAwayFromDeadline, boolean oneGroupNotGraded, boolean allGradedIndividually, Submission... submissions) {
        Assignment assignment = generateAssignmentInJourney(daysAwayFromDeadline, submissions);
        Journey journey = assignment.getMilestone().getJourney();

        for (Submission ignored : submissions) {
            User studentOne = createUser().setId(1L).addAuthority(new Authority().name(STUDENT));
            User studentTwo = createUser().setId(2L).addAuthority(new Authority().name(STUDENT));

            Group group = createGroup();
            group.setStudents(Set.of(studentOne, studentTwo));
            group.setAssignment(assignment);
            group.setSubmissions(Set.of(ignored));

            assignment.addGroup(group);

            journey.addStudent(studentOne);
            journey.addStudent(studentTwo);

            ignored.setGroup(group);
            if (!allGradedIndividually) {
                ignored.getSubmissionFeedbacks().stream().findFirst().ifPresent(feedback -> feedback.group(group));
            } else {
                Set.of(studentOne, studentTwo).forEach(student -> {
                    SubmissionFeedback submissionFeedback = new SubmissionFeedback().student(student);
                    ignored.addSubmissionFeedback(submissionFeedback);
                    submissionFeedback.submission(ignored);
                });
            }
            ignored.addAuthor(studentOne);
            ignored.addAuthor(studentTwo);
            ignored.assignment(assignment);
        }

        if (oneGroupNotGraded) {
            Group group = createGroup();
            assignment.addGroup(group);
            group.setAssignment(assignment);
        }

        return assignment;
    }

    private Submission createSubmissionWithPositiveFeedback() {
        Submission positiveSubmission = createSubmission().resubmittable(false);
        SubmissionFeedback positiveFeedback = new SubmissionFeedback().grade("5");
        positiveSubmission.addSubmissionFeedback(positiveFeedback);
        positiveFeedback.submission(positiveSubmission);
        return positiveSubmission;
    }

    private Submission createSubmissionWithNegativeFeedback() {
        Submission positiveSubmission = createSubmission().resubmittable(false);
        SubmissionFeedback positiveFeedback = new SubmissionFeedback().grade("1");
        positiveSubmission.addSubmissionFeedback(positiveFeedback);
        positiveFeedback.submission(positiveSubmission);
        return positiveSubmission;
    }
}
