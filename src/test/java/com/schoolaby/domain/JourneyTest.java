package com.schoolaby.domain;

import com.schoolaby.web.WithMockCustomUser;
import com.schoolaby.web.rest.TestUtil;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Instant;

import static com.schoolaby.common.TestObjects.*;
import static com.schoolaby.security.Role.Constants.STUDENT;
import static com.schoolaby.service.dto.states.EntityState.*;
import static java.time.temporal.ChronoUnit.DAYS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@WithMockCustomUser(authorities = {STUDENT}, userId = "1")
public class JourneyTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Journey.class);
        Journey journey1 = new Journey();
        journey1.setId(1L);
        Journey journey2 = new Journey();
        journey2.setId(journey1.getId());
        assertThat(journey1).isEqualTo(journey2);
        journey2.setId(2L);
        assertThat(journey1).isNotEqualTo(journey2);
        journey1.setId(null);
        assertThat(journey1).isNotEqualTo(journey2);
    }

    @Test
    void shouldHaveNotStartedState() {
        User student = createUser()
            .setId(1L)
            .addAuthority(new Authority().name(STUDENT));
        Assignment assignment = createAssignment()
            .requiresSubmission(true)
            .deadline(Instant.now().plus(2L, DAYS));
        Milestone milestone = createMilestone()
            .endDate(Instant.now().plus(2L, DAYS))
            .addAssignment(assignment);
        Journey journey = createJourney()
            .addMilestone(milestone)
            .addStudent(student)
            .startDate(Instant.now()
                .plus(1L, DAYS));

        assignment.calculateStates();
        milestone.calculateStates();
        journey.calculateStates();

        assertEquals(NOT_STARTED, journey.getState());
    }

    @Test
    void shouldHaveInProgressState() {
        User student = createUser()
            .setId(1L)
            .addAuthority(new Authority()
                .name(STUDENT));
        Assignment assignment = createAssignment();
        Milestone milestone = createMilestone().addAssignment(assignment);
        Journey journey = createJourney()
            .addMilestone(milestone)
            .addStudent(student);

        assignment.calculateStates();
        milestone.calculateStates();
        journey.calculateStates();

        assertEquals(IN_PROGRESS, journey.getState());
    }

    @Test
    void shouldHaveCompletedState() {
        User student = createUser()
            .setId(1L)
            .addAuthority(new Authority().name(STUDENT));
        Submission positiveSubmission = createSubmissionWithPositiveFeedback(student).addAuthor(student);
        Assignment assignment = createAssignment()
            .requiresSubmission(true)
            .addSubmission(positiveSubmission)
            .gradingScheme(createGradingScheme()
                .addValue(createGradingSchemeValue()));
        Milestone milestone = createMilestone().addAssignment(assignment);
        Journey journey = createJourney()
            .startDate(Instant.now().minus(2L, DAYS))
            .endDate(Instant.now().minus(1L, DAYS))
            .addMilestone(milestone)
            .addStudent(student);

        assignment.calculateStates();
        milestone.calculateStates();
        journey.calculateStates();

        assertEquals(COMPLETED, journey.getState());
    }

    private Submission createSubmissionWithPositiveFeedback(User student) {
        Submission positiveSubmission = createSubmission()
            .resubmittable(false)
            .addAuthor(student);
        SubmissionFeedback positiveFeedback = new SubmissionFeedback()
            .grade("5")
            .student(student);
        positiveSubmission.addSubmissionFeedback(positiveFeedback);
        positiveFeedback.submission(positiveSubmission);
        return positiveSubmission;
    }
}
