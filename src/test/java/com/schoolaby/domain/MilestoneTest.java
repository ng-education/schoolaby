package com.schoolaby.domain;

import com.schoolaby.service.dto.states.EntityState;
import com.schoolaby.web.WithMockCustomUser;
import com.schoolaby.web.rest.TestUtil;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.schoolaby.common.TestObjects.*;
import static com.schoolaby.security.Role.Constants.STUDENT;
import static com.schoolaby.security.Role.Constants.TEACHER;
import static com.schoolaby.service.dto.states.EntityState.*;
import static com.schoolaby.web.rest.TestUtil.getDeadlineInDays;
import static java.util.stream.Collectors.toSet;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@WithMockCustomUser(authorities = {STUDENT})
class MilestoneTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Milestone.class);
        Milestone milestone1 = new Milestone();
        milestone1.setId(1L);
        Milestone milestone2 = new Milestone();
        milestone2.setId(milestone1.getId());
        assertThat(milestone1).isEqualTo(milestone2);
        milestone2.setId(2L);
        assertThat(milestone1).isNotEqualTo(milestone2);
        milestone1.setId(null);
        assertThat(milestone1).isNotEqualTo(milestone2);
    }

    @Test
    void shouldHaveStateNotStartedWhenNoAssignmentsAndEndDateInFuture() {
        Milestone milestone = new Milestone()
            .journey(createJourney().addStudent(createUser().id(2L)))
            .endDate(getDeadlineInDays(0));
        milestone.calculateStates();
        assertEquals(NOT_STARTED, milestone.getState());
    }

    @Test
    void shouldHaveStateCompletedWhenNoAssignmentsAndEndDateInPast() {
        Milestone milestone = new Milestone()
            .journey(createJourney().addStudent(createUser().id(2L)))
            .endDate(getDeadlineInDays(-1));
        milestone.calculateStates();
        assertEquals(COMPLETED, milestone.getState());
    }

    @Test
    void shouldHaveStateInProgressWhenSomeAssignmentsCompletedAndSomeNotStartedAsStudent() {
        assertEntityState(IN_PROGRESS, List.of(COMPLETED, NOT_STARTED), STUDENT);
    }

    @Test
    @WithMockCustomUser(authorities = {TEACHER})
    void shouldHaveStateNotStartedWhenSomeAssignmentsCompletedAndSomeNotStartedAsTeacher() {
        assertEntityState(NOT_STARTED, List.of(COMPLETED, NOT_STARTED), TEACHER);
    }


    @Test
    void shouldHaveStateInProgressWhenAnyAssignmentIsFailedAndNoneAreUrgentOrOverdue() {
        assertEntityState(IN_PROGRESS, List.of(NOT_STARTED, IN_PROGRESS, FAILED));
    }

    @Test
    void shouldHaveStateUrgentWhenAnyAssignmentIsUrgentAndNoneOverdue() {
        assertEntityState(
            URGENT,
            List.of(
                URGENT,
                IN_PROGRESS,
                FAILED,
                COMPLETED,
                NOT_STARTED
            ));
    }

    @Test
    void shouldHaveStateOverdueWhenAnyAssignmentIsOverdue() {
        assertEntityState(OVERDUE, List.of(EntityState.values()));
    }

    @Test
    void shouldHaveStateFailed() {
        assertEntityState(FAILED, List.of(COMPLETED, NOT_STARTED, FAILED));
    }

    @Test
    void shouldHaveStateRejectedWhenAnyAssignmentRejectedAndNoneAreOverdue() {
        assertEntityState(
            REJECTED,
            List.of(
                COMPLETED,
                NOT_STARTED,
                FAILED,
                REJECTED,
                URGENT,
                IN_PROGRESS
            ));
    }

    @Test
    void shouldHaveStateCompletedWhenAllAssignmentsAreCompleted() {
        assertEntityState(COMPLETED, List.of(COMPLETED));
    }

    private void assertEntityState(EntityState expectedEntityState, Collection<EntityState> assignmentStates, String authorityName) {
        Milestone milestone = new Milestone()
            .journey(createJourney().addStudent(createUser().id(2L).addAuthority(new Authority().name(authorityName))))
            .assignments(assignmentStates.stream().map(entityState -> {
                Assignment assignment = createAssignment();
                assignment
                    .states(Set.of(createAssignmentState().assignment(assignment).setUser(createUser().id(2L)).setAuthority(new Authority().name(STUDENT)).setState(entityState)));
                return assignment;
            }).collect(Collectors.toSet()));
        milestone.calculateStates();
        assertEquals(expectedEntityState, milestone.getState());
    }

    private void assertEntityState(EntityState expectedEntityState, Collection<EntityState> assignmentStates) {
        assertEntityState(expectedEntityState, assignmentStates, null);
    }
}
