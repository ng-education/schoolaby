package com.schoolaby.domain;

import com.schoolaby.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class PersonRoleTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PersonRole.class);
        PersonRole personRole1 = new PersonRole();
        personRole1.setId(1L);
        PersonRole personRole2 = new PersonRole();
        personRole2.setId(personRole1.getId());
        assertThat(personRole1).isEqualTo(personRole2);
        personRole2.setId(2L);
        assertThat(personRole1).isNotEqualTo(personRole2);
        personRole1.setId(null);
        assertThat(personRole1).isNotEqualTo(personRole2);
    }
}
