package com.schoolaby.domain;

import com.schoolaby.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class UploadedFileTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UploadedFile.class);
        UploadedFile uploadedFile1 = new UploadedFile();
        uploadedFile1.setId(1L);
        UploadedFile uploadedFile2 = new UploadedFile();
        uploadedFile2.setId(uploadedFile1.getId());
        assertThat(uploadedFile1).isEqualTo(uploadedFile2);
        uploadedFile2.setId(2L);
        assertThat(uploadedFile1).isNotEqualTo(uploadedFile2);
        uploadedFile1.setId(null);
        assertThat(uploadedFile1).isNotEqualTo(uploadedFile2);
    }
}
