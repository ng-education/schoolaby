package com.schoolaby.domain;

import com.schoolaby.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class EducationalAlignmentTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(EducationalAlignment.class);
        EducationalAlignment educationalAlignment1 = new EducationalAlignment();
        educationalAlignment1.setId(1L);
        EducationalAlignment educationalAlignment2 = new EducationalAlignment();
        educationalAlignment2.setId(educationalAlignment1.getId());
        assertThat(educationalAlignment1).isEqualTo(educationalAlignment2);
        educationalAlignment2.setId(2L);
        assertThat(educationalAlignment1).isNotEqualTo(educationalAlignment2);
        educationalAlignment1.setId(null);
        assertThat(educationalAlignment1).isNotEqualTo(educationalAlignment2);
    }
}
