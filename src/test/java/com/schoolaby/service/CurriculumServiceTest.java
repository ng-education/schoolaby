package com.schoolaby.service;

import com.schoolaby.repository.CurriculumJpaRepository;
import com.schoolaby.service.mapper.CurriculumMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CurriculumServiceTest {

    @Mock
    private CurriculumJpaRepository curriculumJpaRepository;
    @Mock
    private CurriculumMapper curriculumMapper;

    @InjectMocks
    private CurriculumService curriculumService;

    @Test
    void shouldQueryForEstonianCurriculums() {
        curriculumService.findAllByCountry("Estonia");
        curriculumService.findAllByCountry("United States");
        curriculumService.findAllByCountry(null);
        verify(curriculumJpaRepository, times(3)).findAllByCountryOrCountryIsNull(eq("Estonia"));
    }

    @Test
    void shouldQueryForTanzanianCurriculums() {
        curriculumService.findAllByCountry("Tanzania");
        verify(curriculumJpaRepository, times(1)).findAllByCountryOrCountryIsNull(eq("Tanzania"));
        verify(curriculumJpaRepository, times(0)).findAllByCountryOrCountryIsNull(eq("Estonia"));
    }
}
