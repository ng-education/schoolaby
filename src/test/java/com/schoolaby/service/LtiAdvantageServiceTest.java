package com.schoolaby.service;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.RSAKey;
import com.schoolaby.config.lti.LtiAdvantageProperties;
import com.schoolaby.domain.*;
import com.schoolaby.repository.LtiLaunchRepository;
import com.schoolaby.service.dto.lti.LtiScoreDTO;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.List;

import static com.nimbusds.jose.jwk.KeyUse.SIGNATURE;
import static com.schoolaby.common.TestObjects.*;
import static com.schoolaby.security.Role.Constants.STUDENT;
import static com.schoolaby.security.SecurityUtils.createSpringSecurityUser;
import static java.util.UUID.randomUUID;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class LtiAdvantageServiceTest {
    @Mock
    private SimpMessagingTemplate simpMessagingTemplate;
    @Mock
    private SecurityContext securityContext;
    @Mock
    private Authentication authentication;
    @Mock
    private UserService userService;
    @Mock
    private LtiAdvantageProperties ltiAdvantageProperties;
    @Mock
    private JWK ltiJwk;
    @Mock
    private LtiResourceService ltiResourceService;
    @Mock
    private LtiConfigService ltiConfigService;
    @Mock
    private LtiLaunchRepository ltiLaunchRepository;

    @InjectMocks
    private LtiAdvantageService ltiAdvantageService;

    @ParameterizedTest
    @ValueSource(booleans = {true, false})
    void shouldRedirectResourceLaunchToTool(boolean syncGrade) throws JOSEException {
        User user = createUser().setId(3L);
        Journey journey = createJourney().id(1L).addStudent(user);
        Assignment assignment = createAssignment().setMilestone(createMilestone().journey(journey));
        LtiResource ltiResource = createLtiResource().setId(1L).setAssignment(assignment).setLtiApp(createLtiApp().setId(1L)).setSyncGrade(syncGrade);
        LtiConfig ltiConfig = createLtiConfig();
        LtiLaunch ltiLaunch = createLtiLaunch().setLtiResource(ltiResource).setUser(user);
        com.schoolaby.security.User securityUser = createSpringSecurityUser(user);

        doReturn(ltiResource).when(ltiResourceService).getOne(eq(ltiResource.getId()));
        doReturn(ltiConfig).when(ltiConfigService).getOne(argThat(ltiConfigFilter ->
            ltiConfigFilter
                .getAppId()
                .equals(
                    ltiResource
                        .getLtiApp()
                        .getId()) &&
                ltiConfigFilter
                    .getJourneyId()
                    .equals(journey.getId())));
        doReturn("localhost").when(ltiAdvantageProperties).getIss();
        doReturn(123).when(ltiAdvantageProperties).getTokenValidityInSeconds();
        doReturn(List.of(new SimpleGrantedAuthority(STUDENT))).when(authentication).getAuthorities();
        doReturn(securityUser).when(authentication).getPrincipal();
        doReturn(authentication).when(securityContext).getAuthentication();
        doReturn(generateRSAKey()).when(ltiJwk).toRSAKey();
        SecurityContextHolder.setContext(securityContext);

        if (syncGrade) {
            doReturn(ltiLaunch).when(ltiLaunchRepository).save(argThat((launch) -> launch.getLtiResource().equals(ltiResource)));
            doReturn(List.of(ltiLaunch)).when(ltiLaunchRepository).findAll(eq(ltiResource.getAssignment().getId()), eq(securityUser.getId()), eq(null));
            doReturn(user).when(userService).getOneById(eq(user.getId()));
        }

        ltiAdvantageService.redirectResourceLaunchToTool(ltiResource.getId(), null, new RedirectAttributesModelMap());

        if (syncGrade) {
            verify(simpMessagingTemplate).convertAndSendToUser(eq(securityUser.getUsername()), eq("/queue/lti-scores"), argThat((payload) -> {
                LtiScoreDTO ltiScoreDTO = (LtiScoreDTO) payload;
                return ltiScoreDTO.getResourceId().equals(ltiResource.getId()) && ltiScoreDTO.getScore() == null;
            }));
        } else {
            verifyNoInteractions(simpMessagingTemplate);
        }
    }

    private RSAKey generateRSAKey() {
        KeyPair keyPair = null;
        try {
            keyPair = KeyPairGenerator.getInstance("RSA")
                .genKeyPair();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return new RSAKey.Builder((RSAPublicKey) keyPair.getPublic())
            .privateKey((RSAPrivateKey) keyPair.getPrivate())
            .keyUse(SIGNATURE)
            .keyID(randomUUID().toString())
            .build();
    }
}
