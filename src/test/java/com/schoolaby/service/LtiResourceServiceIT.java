package com.schoolaby.service;

import com.schoolaby.common.BaseIntegrationTest;
import com.schoolaby.domain.*;
import com.schoolaby.service.dto.lti.LtiResourceDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithUserDetails;

import javax.transaction.Transactional;
import java.util.List;

import static com.schoolaby.common.TestCases.*;
import static com.schoolaby.common.TestObjects.*;
import static com.schoolaby.domain.enumeration.LtiConfigType.JOURNEY;
import static org.junit.jupiter.api.Assertions.*;


class LtiResourceServiceIT extends BaseIntegrationTest {
    @Autowired
    private LtiResourceService service;

    public LtiConfig ltiConfig;
    public LtiApp ltiApp;
    public Assignment assignment;
    public Milestone milestone;
    public Journey journey;
    public LtiResource assignmentLtiResource;
    public LtiResource milestoneLtiResource;

    @BeforeEach
    void setup() {
        User teacher = getTeacher(entityManager);
        journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(teacher);
        milestone = createMilestone()
            .journey(journey)
            .creator(teacher);
        assignment = createAssignment()
            .creator(teacher)
            .gradingScheme(getGradingSchemeNumerical(entityManager))
            .milestone(milestone);
        ltiApp = createLtiApp();
        ltiConfig = createLtiConfig();
        ltiConfig.setType(JOURNEY);
        assignmentLtiResource = createLtiResource().setLtiApp(ltiApp);
        milestoneLtiResource = createLtiResource().setLtiApp(ltiApp);
        ltiApp.addConfig(ltiConfig);
        journey.addLtiConfig(ltiConfig);
        milestone.addLtiResource(milestoneLtiResource);
        assignment.addLtiResource(assignmentLtiResource);
        persistCreatedEntities(entityManager);
    }

    @Test
    @Transactional
    @WithUserDetails(ADMIN_LOGIN)
    void shouldFindById() {
        LtiResource result = service.getOne(milestoneLtiResource.getId());

        assertNotNull(result.getId());
        assertEquals(LTI_RESOURCE_TITLE, result.getTitle());
        assertEquals(LTI_RESOURCE_DESCRIPTION, result.getDescription());
        assertEquals(LTI_RESOURCE_RESOURCE_LINK_ID, result.getResourceLinkId());
        assertEquals(LTI_RESOURCE_SYNC_GRADE, result.isSyncGrade());
        assertNotNull(result.getLtiApp());
        assertNotNull(result.getMilestone());
        assertNull(result.getAssignment());
        assertEquals(milestoneLtiResource, result);
    }

    @Test
    @Transactional
    @WithUserDetails(ADMIN_LOGIN)
    void shouldFindMilestoneLtiResources() {
        List<LtiResourceDTO> result = service.findAll(milestone.getId(), null);
        assertEquals(1, result.size());
        assertEquals(milestoneLtiResource.getId(), result.get(0).getId());
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void shouldFindLtiResourceWhenStudentIsJourneyParticipant() {
        journey.addStudent(getStudent(entityManager));

        List<LtiResourceDTO> result = service.findAll(null, assignment.getId());
        assertEquals(1, result.size());
        assertEquals(assignmentLtiResource.getId(), result.get(0).getId());
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void shouldNotFindLtiResourceWhenStudentIsNonParticipantInJourney() {
        List<LtiResourceDTO> result = service.findAll(null, assignment.getId());
        assertEquals(0, result.size());
    }
}
