package com.schoolaby.service;

import com.schoolaby.config.ApplicationProperties;
import com.schoolaby.domain.Authority;
import com.schoolaby.domain.Journey;
import com.schoolaby.domain.JourneySignupCode;
import com.schoolaby.repository.JourneySignupCodeRepository;
import com.schoolaby.service.dto.JourneySignupCodeDTO;
import com.schoolaby.service.mapper.JourneySignupCodeMapper;
import com.schoolaby.web.WithMockCustomUser;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

import static com.schoolaby.security.Role.Constants.STUDENT;
import static com.schoolaby.security.Role.Constants.TEACHER;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.doReturn;

@ExtendWith(SpringExtension.class)
@WithMockCustomUser(authorities = {TEACHER}, userId = "1")
class JourneySignupCodeServiceTest {
    public static final String SIGN_UP_CODE = "123";
    @Mock
    private JourneySignupCodeRepository repository;
    @Mock
    private ApplicationProperties applicationProperties;
    @Spy
    private JourneySignupCodeMapper journeySignupCodeMapper;

    @InjectMocks
    private JourneySignupCodeService service;

    @Test
    void shouldFindOne() {
        JourneySignupCode signupCode = new JourneySignupCode()
            .setSignUpCode(SIGN_UP_CODE);
        doReturn(Optional.of(signupCode)).when(repository).findBySignupCode(SIGN_UP_CODE);

        JourneySignupCode result = service.findOne(SIGN_UP_CODE).orElseThrow();

        assertEquals(SIGN_UP_CODE, result.getSignUpCode());
    }

    @Test
    void shouldCreateOneTimeSignupCode() {
        JourneySignupCode signupCode = new JourneySignupCode()
            .setSignUpCode(SIGN_UP_CODE)
            .authority(new Authority().name(TEACHER))
            .journey(new Journey().id(1L));
        doReturn(Optional.of(signupCode)).when(repository).findBySignupCode(SIGN_UP_CODE);

        ApplicationProperties.Endpoints endpoints = new ApplicationProperties.Endpoints();
        endpoints.getJourneySignupCodeGeneration().setAllowedUsers(List.of("test"));
        doReturn(endpoints).when(applicationProperties).getEndpoints();
        doReturn(new JourneySignupCodeDTO()
            .setSignUpCode(signupCode.getSignUpCode())
        ).when(journeySignupCodeMapper).toDto(any(JourneySignupCode.class));
        doReturn(signupCode).when(repository).save(
            argThat(argument -> argument.getJourney().getId().equals(signupCode.getJourney().getId()))
        );

        JourneySignupCodeDTO result = service.createOneTimeSignupCode(new JourneySignupCodeDTO().setSignUpCode(SIGN_UP_CODE));

        assertEquals(SIGN_UP_CODE, result.getSignUpCode());
    }

    @Test
    void shouldGiveForbiddenWhenCreatingOneTimeSignupCodeAsWrongUser() {
        JourneySignupCode signupCode = new JourneySignupCode()
            .setSignUpCode(SIGN_UP_CODE)
            .authority(new Authority().name(TEACHER))
            .journey(new Journey());
        doReturn(Optional.of(signupCode)).when(repository).findBySignupCode(SIGN_UP_CODE);

        ApplicationProperties.Endpoints endpoints = new ApplicationProperties.Endpoints();
        endpoints.getJourneySignupCodeGeneration().setAllowedUsers(List.of("wrongUser"));
        doReturn(endpoints).when(applicationProperties).getEndpoints();

        assertThrows(ResponseStatusException.class, () -> service.createOneTimeSignupCode(new JourneySignupCodeDTO().setSignUpCode(SIGN_UP_CODE)), "This user is not allowed to make requests to this endpoint");
    }

    @Test
    void shouldGiveBadRequestWhenCreatingOneTimeSignupCodeWithStudentSignupCode() {
        JourneySignupCode signupCode = new JourneySignupCode()
            .setSignUpCode(SIGN_UP_CODE)
            .authority(new Authority().name(STUDENT))
            .journey(new Journey());
        doReturn(Optional.of(signupCode)).when(repository).findBySignupCode(SIGN_UP_CODE);

        assertThrows(ResponseStatusException.class, () -> service.createOneTimeSignupCode(new JourneySignupCodeDTO().setSignUpCode(SIGN_UP_CODE)), "Non teacher signup code");
    }
}
