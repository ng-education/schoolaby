package com.schoolaby.service;

import com.schoolaby.domain.*;
import com.schoolaby.repository.AssignmentRepository;
import com.schoolaby.service.dto.GroupAssignmentDTO;
import com.schoolaby.service.dto.GroupDTO;
import com.schoolaby.service.mapper.AssignmentMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Set;

import static com.schoolaby.common.TestCases.createTeacher;
import static com.schoolaby.common.TestObjects.*;
import static java.util.stream.Collectors.toSet;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.eq;

@ExtendWith(MockitoExtension.class)
class AssignmentReadServiceTest {
    @Mock
    private AssignmentRepository assignmentRepository;
    @Mock
    private AssignmentMapper assignmentMapper;

    @InjectMocks
    private AssignmentReadService assignmentReadService;

    @Test
    void shouldGetGroupAssignmentsBySearch() {
        String searchQuery = "assignment";
        Journey journey = createJourney();
        journey.setId(1L);
        journey.addTeacher(createTeacher().setId(1L));
        Milestone milestone = createMilestone().id(1L);
        milestone.setJourney(journey);
        Assignment firstAssignment = createAssignment()
            .milestone(milestone)
            .title("First assignment")
            .setGroups(Set.of(createGroup().setName("First group")));
        Assignment secondAssignment = createAssignment()
            .milestone(milestone)
            .title("Second assignment")
            .setGroups(Set.of(createGroup().setName("Second group")));
        List<Assignment> assignments = List.of(firstAssignment, secondAssignment);
        List<GroupAssignmentDTO> assignmentDTOs = List.of(getGroupAssignmentDTO(firstAssignment), getGroupAssignmentDTO(secondAssignment));

        doReturn(assignments).when(assignmentRepository).findBySearch(eq(journey.getId()), eq(searchQuery));
        doReturn(assignmentDTOs).when(assignmentMapper).toGroupAssignmentDto(eq(assignments));

        List<GroupAssignmentDTO> assignmentsBySearch = assignmentReadService.getGroupAssignmentsBySearch(journey.getId(), searchQuery);
        assertEquals(assignmentDTOs, assignmentsBySearch);
        assertEquals(2, assignmentsBySearch.size());
    }

    @Test
    void shouldRemoveStudentFromAssignment() {
        User studentToDelete = createUser().setId(-1L);
        User student = createUser();
        Assignment assignment = getAssignment().addStudent(student).addStudent(studentToDelete);

        assignmentReadService.removeStudentFromAssignment(assignment, studentToDelete.getId());

        assertEquals(1, assignment.getStudents().size());
        assertNotEquals(studentToDelete.getId(), assignment.getStudents().stream().findFirst().orElseThrow().getId());
    }

    @Test
    void shouldRemoveStudentFromGroupWhenDeletingStudentFromAssignment() {
        User studentToDelete = createUser().setId(-1L);
        User student = createUser();
        Group group = createGroup().addStudent(studentToDelete).addStudent(student);
        Assignment assignment = getAssignment().addGroup(group);

        assignmentReadService.removeStudentFromAssignment(assignment, studentToDelete.getId());

        Set<User> students = assignment.getGroups().stream().findFirst().orElseThrow().getStudents();
        assertEquals(1, students.size());
        assertNotEquals(studentToDelete.getId(), students.stream().findFirst().orElseThrow().getId());
    }

    @Test
    void shouldDeleteGroupWhenGroupEmptyAfterDeletingStudentFromAssignment() {
        User studentToDelete = createUser().setId(-1L);
        Group group = createGroup().addStudent(studentToDelete);
        Assignment assignment = getAssignment().addGroup(group);

        assignmentReadService.removeStudentFromAssignment(assignment, studentToDelete.getId());

        assertTrue(assignment.getGroups().isEmpty());
    }

    private Assignment getAssignment() {
        Journey journey = createJourney();
        journey.setId(1L);
        journey.addTeacher(createTeacher().setId(1L));
        Milestone milestone = createMilestone().id(1L);
        milestone.setJourney(journey);
        return createAssignment().setId(1L).setMilestone(milestone).addMaterial(createMaterial());
    }

    private GroupAssignmentDTO getGroupAssignmentDTO(Assignment assignment) {
        return new GroupAssignmentDTO()
            .setId(assignment.getId())
            .setTitle(assignment.getTitle())
            .setCreatedDate(assignment.getCreatedDate())
            .setGroups(assignment.getGroups().stream().map(this::getGroupDTO).collect(toSet()));
    }

    private GroupDTO getGroupDTO(Group group) {
        return new GroupDTO()
            .setId(group.getId())
            .setName(group.getName());
    }

}
