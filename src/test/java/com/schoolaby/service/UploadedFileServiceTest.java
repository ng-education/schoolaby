package com.schoolaby.service;

import com.schoolaby.domain.UploadedFile;
import com.schoolaby.repository.UploadedFileRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

import static com.schoolaby.common.TestCases.createTeacher;
import static com.schoolaby.common.TestObjects.createUploadedFile;
import static com.schoolaby.security.SecurityUtils.createSpringSecurityUser;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UploadedFileServiceTest {

    @Mock
    private UploadedFileRepository uploadedFileRepository;
    @Mock
    private Authentication authentication;
    @Mock
    private SecurityContext securityContext;

    @InjectMocks
    private UploadedFileService uploadedFileService;

    @Test
    void shouldAllowToDeleteWhenUserIsTheCreatorOfTheFile() {
        com.schoolaby.security.User securityUser = createSpringSecurityUser(createTeacher().setId(1L));
        UploadedFile uploadedFile = createUploadedFile().setId(20L);
        uploadedFile.setCreatedBy(securityUser.getUsername());

        SecurityContextHolder.setContext(securityContext);
        doReturn(authentication).when(securityContext).getAuthentication();
        when(SecurityContextHolder.getContext().getAuthentication().getPrincipal()).thenReturn(securityUser);
        doReturn(Optional.of(uploadedFile)).when(uploadedFileRepository).findById(eq(uploadedFile.getId()));

        assertDoesNotThrow(() -> uploadedFileService.delete(uploadedFile.getId()));
    }

    @Test
    void shouldNotAllowToDeleteWhenUserNotTheCreatorOfTheFile() {
        com.schoolaby.security.User securityUser = createSpringSecurityUser(createTeacher().setId(1L));
        UploadedFile uploadedFile = createUploadedFile().setId(20L);
        uploadedFile.setCreatedBy("test");

        SecurityContextHolder.setContext(securityContext);
        doReturn(authentication).when(securityContext).getAuthentication();
        when(SecurityContextHolder.getContext().getAuthentication().getPrincipal()).thenReturn(securityUser);
        doReturn(Optional.of(uploadedFile)).when(uploadedFileRepository).findById(eq(uploadedFile.getId()));
        assertThrows(ResponseStatusException.class, () -> uploadedFileService.delete(uploadedFile.getId()));
    }
}
