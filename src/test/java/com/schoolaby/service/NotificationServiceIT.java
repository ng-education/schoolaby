package com.schoolaby.service;

import com.schoolaby.common.BaseIntegrationTest;
import com.schoolaby.domain.Journey;
import com.schoolaby.domain.Notification;
import com.schoolaby.domain.PatchNotificationDTO;
import com.schoolaby.domain.User;
import com.schoolaby.service.dto.NotificationDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;

import static com.schoolaby.common.TestCases.getStudent;
import static com.schoolaby.common.TestCases.getTeacher;
import static com.schoolaby.common.TestObjects.*;
import static com.schoolaby.domain.NotificationType.GENERAL;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class NotificationServiceIT extends BaseIntegrationTest {
    @Autowired
    private NotificationService notificationService;

    private User student;

    @BeforeEach
    void initTest() {
        student = getStudent(entityManager);
    }

    @Test
    @Transactional
    void shouldCreate() {
        Notification notification = new Notification()
            .message("test")
            .link("link.com/1")
            .recipient(student);
        notification.message("test").link("link.com/1").recipient(student);

        Notification result = notificationService.save(notification);
        assertNotNull(result.getId());
        assertEquals(notification.getMessage(), result.getMessage());
        assertEquals(notification.getLink(), result.getLink());
        assertEquals(notification.getRecipient(), result.getRecipient());
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void shouldFindByUser() {
        Notification notification = new Notification();
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(getTeacher(entityManager))
            .addStudent(getStudent(entityManager));
        notification
            .message("test")
            .link("link.com/1")
            .type(GENERAL)
            .recipient(student)
            .journey(journey);
        persistCreatedEntities(entityManager);

        notificationService.save(notification);

        final PageRequest pageable = PageRequest.of(0, 100);
        Page<NotificationDTO> notifications = notificationService.findAllByNotificationType(pageable, GENERAL);

        assertEquals(notifications.getTotalElements(), 1L);
        NotificationDTO result = notifications.getContent().get(0);
        assertNotNull(result.getId());
        assertEquals(notification.getType(), result.getType());
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void shouldUpdate() {
        User student = getStudent(entityManager);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(getTeacher(entityManager))
            .addStudent(getStudent(entityManager));
        persistCreatedEntities(entityManager);
        Notification notification1 = createNotification().recipient(student).journey(journey);
        Notification notification2 = createNotification().recipient(student).journey(journey);
        persistCreatedEntities(entityManager);

        PatchNotificationDTO patchNotificationDTO1 = new PatchNotificationDTO();
        Instant now1 = Instant.now();
        patchNotificationDTO1.setId(notification1.getId());
        patchNotificationDTO1.setRead(now1);

        PatchNotificationDTO patchNotificationDTO2 = new PatchNotificationDTO();
        Instant now2 = Instant.now();
        patchNotificationDTO2.setId(notification2.getId());
        patchNotificationDTO2.setRead(now2);

        List<NotificationDTO> result = notificationService.update(List.of(patchNotificationDTO1, patchNotificationDTO2));
        assertEquals(2, result.size());
        assertEquals(notification1.getId(), result.get(0).getId());
        assertEquals(notification1.getType(), result.get(0).getType());
        assertEquals(notification2.getId(), result.get(1).getId());
        assertEquals(notification2.getType(), result.get(1).getType());
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void shouldNotFindByUserWhenNotInJourney() {
        Notification notification = new Notification();
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(getTeacher(entityManager));
        notification
            .message("test")
            .link("link.com/1")
            .type(GENERAL)
            .recipient(student)
            .journey(journey);
        persistCreatedEntities(entityManager);

        notificationService.save(notification);

        final PageRequest pageable = PageRequest.of(0, 100);
        Page<NotificationDTO> notifications = notificationService.findAllByNotificationType(pageable, GENERAL);

        assertEquals(notifications.getTotalElements(), 0L);
    }
}
