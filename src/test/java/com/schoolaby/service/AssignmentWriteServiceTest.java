package com.schoolaby.service;

import com.schoolaby.domain.*;
import com.schoolaby.repository.AssignmentRepository;
import com.schoolaby.service.dto.*;
import com.schoolaby.service.dto.lti.LtiResourceDTO;
import com.schoolaby.service.mapper.AssignmentMapper;
import com.schoolaby.service.mapper.AssignmentMaterialMapper;
import com.schoolaby.service.mapper.LtiResourceMapper;
import com.schoolaby.service.mapper.RubricMapper;
import com.schoolaby.web.WithMockCustomUser;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.schoolaby.common.TestCases.createStudent;
import static com.schoolaby.common.TestCases.createTeacher;
import static com.schoolaby.common.TestObjects.*;
import static com.schoolaby.security.Role.Constants.STUDENT;
import static com.schoolaby.security.SecurityUtils.createSpringSecurityUser;
import static java.util.stream.Collectors.toSet;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class})
@WithMockCustomUser(authorities = STUDENT)
class AssignmentWriteServiceTest {
    @Mock
    private AssignmentRepository assignmentRepository;
    @Mock
    private NotificationService notificationService;
    @Mock
    private AssignmentMapper assignmentMapper;
    @Mock
    private RubricMapper rubricMapper;
    @Mock
    private RubricService rubricService;
    @Mock
    private Authentication authentication;
    @Mock
    private SecurityContext securityContext;
    @Mock
    private MilestoneService milestoneService;
    @Mock
    private AssignmentReadService assignmentReadService;
    @Mock
    private LtiResourceService ltiResourceService;
    @Mock
    private LtiResourceMapper ltiResourceMapper;
    @Mock
    private MaterialService materialService;
    @Mock
    private ApplicationEventPublisher applicationEventPublisher;
    @Mock
    private AssignmentMaterialMapper assignmentMaterialMapper;

    @InjectMocks
    private AssignmentWriteService assignmentWriteService;

    @Test
    void shouldCreateDeadlineNotifications() {
        User student = createStudent().id(2L);
        Journey journey = createJourney();
        journey.addStudent(student);
        Milestone milestone = createMilestone();
        milestone.setJourney(journey);
        Assignment assignment1 = createAssignment();
        assignment1.setMilestone(milestone);
        assignment1.calculateStates();
        Assignment assignment2 = createAssignment();
        assignment2.setMilestone(milestone);
        assignment2.calculateStates();

        when(assignmentRepository.findAllByDeadlineBetween(any(), any())).thenReturn(List.of(assignment1, assignment2));

        assignmentWriteService.createDeadlineNotifications();

        verify(assignmentRepository).findAllByDeadlineBetween(any(), any());
        verify(notificationService, times(2)).save(any());
    }

    @Test
    void shouldSoftlyUpdateAssignmentRubricTemplateWhenRubricRemoved() {
        com.schoolaby.security.User securityUser = createSpringSecurityUser(createTeacher().setId(1L));
        Assignment assignment = getAssignment();
        Rubric rubricWithAssignment = createRubric().setAssignment(assignment).setIsTemplate(true);
        RubricDTO rubricWithAssignmentDTO = new RubricDTO()
            .setTitle("Test rubric")
            .setAssignmentId(assignment.getId())
            .setIsTemplate(true);
        AssignmentUpdateDTO assignmentUpdateDTO = getAssignmentUpdateDTO(assignment);

        doReturn(authentication).when(securityContext).getAuthentication();
        SecurityContextHolder.setContext(securityContext);
        doReturn(securityUser).when(authentication).getPrincipal();
        doReturn(assignment).when(assignmentMapper).toEntity(eq(assignmentUpdateDTO));
        doReturn(assignment).when(assignmentRepository).save(eq(assignment));
        doReturn(Optional.of(rubricWithAssignment)).when(rubricService).findByAssignmentId(eq(assignment.getId()));
        doReturn(rubricWithAssignmentDTO).when(rubricMapper).toDto(eq(rubricWithAssignment));
        doReturn(assignment).when(assignmentReadService).getOne(1L, false);
        doReturn(assignment.getMilestone()).when(milestoneService).getOne(1L);

        assignmentWriteService.update(assignmentUpdateDTO);

        verify(assignmentRepository, times(1)).save(eq(assignment));
        verify(rubricService, times(1)).saveDTO(eq(rubricWithAssignmentDTO.setAssignmentId(null).setId(null)));
        verify(rubricService, times(1)).delete(eq(rubricWithAssignment));
    }

    @Test
    void shouldSoftlyUpdateAssignmentRubricTemplateWhenRubricChanged() {
        Assignment assignment = getAssignment();
        com.schoolaby.security.User securityUser = createSpringSecurityUser(createTeacher().setId(1L));
        Rubric rubric = createRubric()
            .setIsTemplate(true)
            .setAssignment(assignment);
        Rubric changedRubric = createRubric()
            .setTitle("Changed title")
            .setAssignment(assignment);
        RubricDTO rubricDTO = new RubricDTO()
            .setIsTemplate(true)
            .setAssignmentId(assignment.getId());
        RubricDTO changedRubricDTO = new RubricDTO()
            .setTitle("Changed title")
            .setAssignmentId(assignment.getId());
        AssignmentUpdateDTO assignmentUpdateDTO = getAssignmentUpdateDTO(assignment).setRubric(changedRubricDTO);

        doReturn(authentication).when(securityContext).getAuthentication();
        SecurityContextHolder.setContext(securityContext);
        doReturn(securityUser).when(authentication).getPrincipal();
        doReturn(assignment).when(assignmentMapper).toEntity(eq(assignmentUpdateDTO));
        doReturn(assignment).when(assignmentRepository).save(eq(assignment));
        doReturn(assignmentUpdateDTO).when(assignmentMapper).toDto(eq(assignment));
        doReturn(Optional.of(rubric)).when(rubricService).findByAssignmentId(eq(assignment.getId()));
        doReturn(rubricDTO).when(rubricMapper).toDto(eq(rubric));
        doReturn(changedRubric).when(rubricMapper).toEntity(eq(changedRubricDTO));
        doReturn(assignment.getMilestone()).when(milestoneService).getOne(1L);
        doReturn(assignment).when(assignmentReadService).getOne(1L, false);

        assignmentWriteService.update(assignmentUpdateDTO);

        verify(rubricService, times(1)).saveDTO(eq(rubricDTO.setAssignmentId(null).setId(null)));
        verify(rubricService, times(1)).delete(eq(rubric));
        verify(rubricService, times(1)).save(eq(changedRubric));
        verify(assignmentRepository, times(1)).save(eq(assignment));
    }

    @Test
    void shouldSoftlyUpdateAssignmentRubricTemplateWhenAssignmentDeleted() {
        Assignment assignment = getAssignment();
        com.schoolaby.security.User securityUser = createSpringSecurityUser(createTeacher().setId(1L));
        Rubric rubric = createRubric()
            .setIsTemplate(true)
            .setAssignment(assignment);
        RubricDTO rubricDTO = new RubricDTO()
            .setIsTemplate(true)
            .setAssignmentId(assignment.getId());

        doReturn(authentication).when(securityContext).getAuthentication();
        SecurityContextHolder.setContext(securityContext);
        doReturn(securityUser).when(authentication).getPrincipal();
        doReturn(Optional.of(rubric)).when(rubricService).findByAssignmentId(eq(assignment.getId()));
        doReturn(rubricDTO).when(rubricMapper).toDto(eq(rubric));
        doReturn(assignment).when(assignmentReadService).getOne(1L, false);

        assignmentWriteService.delete(assignment.getId());

        verify(assignmentRepository, times(1)).deleteById(assignment.getId());
        verify(rubricService, times(1)).saveDTO(eq(rubricDTO.setAssignmentId(null).setId(null)));
    }

    @Test
    void shouldDeleteRubricWhenAssignmentDeleted() {
        Assignment assignment = getAssignment();
        com.schoolaby.security.User securityUser = createSpringSecurityUser(createTeacher().setId(1L));
        Rubric rubric = createRubric().setAssignment(assignment);

        doReturn(authentication).when(securityContext).getAuthentication();
        SecurityContextHolder.setContext(securityContext);
        doReturn(securityUser).when(authentication).getPrincipal();
        doReturn(Optional.of(rubric)).when(rubricService).findByAssignmentId(eq(assignment.getId()));
        doReturn(assignment).when(assignmentReadService).getOne(1L, false);

        assignmentWriteService.delete(assignment.getId());

        verify(assignmentRepository, times(1)).deleteById(assignment.getId());
        verify(rubricService, times(1)).delete(eq(rubric));
    }

    @Test
    void shouldPatchAssignment() {
        Assignment assignment = getAssignment();
        doReturn(Optional.of(assignment)).when(assignmentRepository).findOne(1L, false);
        when(assignmentMapper.toDto(any(Assignment.class))).thenAnswer(i -> {
            Assignment a = (Assignment) i.getArguments()[0];
            return new AssignmentDTO()
                .setTitle(a.getTitle())
                .setDescription(a.getDescription())
                .setDeadline(a.getDeadline())
                .setLtiResources(a.getLtiResources().stream().map(ltiResource -> new LtiResourceDTO()).collect(toSet()))
                .setMaterials(a.getMaterials().stream().map(ltiResource -> new MaterialDTO()).collect(toSet()));
        });
        when(ltiResourceMapper.toEntity(any(Set.class))).thenAnswer(i -> {
            Set<LtiResourceDTO> ltiResources = (Set<LtiResourceDTO>) i.getArguments()[0];
            return ltiResources.stream().map(ltiResource -> new LtiResource()).collect(toSet());
        });
        when(assignmentMaterialMapper.toEntity(any(Set.class))).thenAnswer(i -> {
            Set<MaterialDTO> materials = (Set<MaterialDTO>) i.getArguments()[0];
            return materials.stream().map(materialDTO -> new AssignmentMaterial()
                .setMaterial(new Material().setId(materialDTO.getId()))).collect(toSet());
        });

        Instant newDeadline = Instant.now();

        AssignmentPatchDTO assignmentPatchDTO = new AssignmentPatchDTO()
            .setTitle("New test title")
            .setDescription("New test description")
            .setDeadline(newDeadline)
            .setLtiResources(Set.of(new LtiResourceDTO()))
            .setMaterials(Set.of(new MaterialDTO().id(1L), new MaterialDTO().id(2L)));

        AssignmentDTO result = assignmentWriteService.patch(1L, assignmentPatchDTO);

        assertEquals("New test title", result.getTitle());
        assertEquals("New test description", result.getDescription());
        assertEquals(newDeadline, result.getDeadline());
        assertEquals(1, result.getLtiResources().size());
        assertEquals(2, result.getMaterials().size());
    }

    @Test
    void shouldPatchAssignmentOnlyNonNullFields() {
        Assignment assignment = getAssignment()
            .addLtiResource(createLtiResource());
        doReturn(Optional.of(assignment)).when(assignmentRepository).findOne(1L, false);
        when(assignmentMapper.toDto(any(Assignment.class))).thenAnswer(i -> {
            Assignment a = (Assignment) i.getArguments()[0];
            return new AssignmentDTO()
                .setTitle(a.getTitle())
                .setDescription(a.getDescription())
                .setDeadline(a.getDeadline())
                .setLtiResources(a.getLtiResources().stream().map(ltiResource -> new LtiResourceDTO()).collect(toSet()))
                .setMaterials(a.getMaterials().stream().map(ltiResource -> new MaterialDTO()).collect(toSet()));
        });

        AssignmentPatchDTO assignmentPatchDTO = new AssignmentPatchDTO()
            .setTitle("New test title");

        AssignmentDTO result = assignmentWriteService.patch(1L, assignmentPatchDTO);

        assertEquals("New test title", result.getTitle());
        assertEquals(assignment.getDescription(), result.getDescription());
        assertEquals(assignment.getDeadline(), result.getDeadline());
        assertEquals(assignment.getLtiResources().size(), result.getLtiResources().size());
        assertEquals(assignment.getMaterials().size(), result.getMaterials().size());
    }

    private Assignment getAssignment() {
        Journey journey = createJourney();
        journey.setId(1L);
        journey.addTeacher(createTeacher().setId(1L));
        Milestone milestone = createMilestone().id(1L);
        milestone.setJourney(journey);
        return createAssignment().setId(1L).setMilestone(milestone).addMaterial(createMaterial());
    }

    private AssignmentUpdateDTO getAssignmentUpdateDTO(Assignment assignment) {
        return new AssignmentUpdateDTO()
            .setId(assignment.getId())
            .setTitle(assignment.getTitle())
            .setDescription(assignment.getDescription())
            .setMilestoneId(assignment.getMilestone().getId())
            .setJourneyId(assignment.getMilestone().getJourney().getId());
    }

    private GroupDTO getGroupDTO(Group group) {
        return new GroupDTO()
            .setId(group.getId())
            .setName(group.getName());
    }

}
