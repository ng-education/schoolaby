package com.schoolaby.service;

import com.schoolaby.domain.Authority;
import com.schoolaby.domain.PersonRole;
import com.schoolaby.domain.School;
import com.schoolaby.domain.User;
import com.schoolaby.repository.AuthorityRepository;
import com.schoolaby.repository.PersonRoleRepository;
import com.schoolaby.repository.UserRepository;
import com.schoolaby.security.Role;
import com.schoolaby.service.dto.EkoolRolesDTO;
import com.schoolaby.service.dto.EkoolSchoolsDTO;
import com.schoolaby.service.dto.PersonRoleDTO;
import com.schoolaby.service.dto.UserDetailedDTO;
import com.schoolaby.service.mapper.EkoolRoleMapper;
import com.schoolaby.service.mapper.PersonRoleMapper;
import com.schoolaby.service.mapper.UserDetailedMapper;
import io.undertow.util.BadRequestException;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.caffeine.CaffeineCache;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.core.user.DefaultOAuth2User;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.schoolaby.common.TestObjects.*;
import static com.schoolaby.common.TestUtils.mapPersonRoleToDTO;
import static com.schoolaby.common.TestUtils.mapUserToDetailedDTO;
import static com.schoolaby.domain.ExternalAuthentication.ExternalAuthenticationType.EKOOL;
import static com.schoolaby.security.Role.Constants.ADMIN;
import static com.schoolaby.security.Role.Constants.STUDENT;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {
    private static final String DEFAULT_LOGIN = "johndoe";
    private static final String DEFAULT_EMAIL = "johndoe@localhost";
    private static final String DEFAULT_FIRSTNAME = "john";
    private static final String DEFAULT_LASTNAME = "doe";
    private static final String DEFAULT_IMAGEURL = "http://placehold.it/50x50";
    private static final String DEFAULT_LANGKEY = "dummy";

    @Mock
    private Authentication authentication;
    @Mock
    private SecurityContext securityContext;
    @Mock
    private UserRepository userRepository;
    @Mock
    private PersonRoleRepository personRoleRepository;
    @Mock
    private UserDetailedMapper userDetailedMapper;
    @Mock
    private PersonRoleMapper personRoleMapper;
    @Mock
    private PasswordEncoder passwordEncoder;
    @Mock
    private AuthorityRepository authorityRepository;
    @Mock
    private SchoolService schoolService;
    @Mock
    private EkoolService ekoolService;
    @Mock
    private CacheManager cacheManager;
    @Mock
    private EkoolRoleMapper ekoolRoleMapper;
    @Mock
    private com.github.benmanes.caffeine.cache.Cache<Object, Object> mockedCache;

    @InjectMocks
    private UserService userService;

    @Test
    void shouldCreateUserWithSchools() {
        School school = createSchool().id(1L);
        PersonRole personRole = createPersonRole()
            .id(1L)
            .school(school);
        User user = createUser().personRoles(Set.of(personRole));
        UserDetailedDTO userDTO = mapUserToDetailedDTO(user);
        PersonRoleDTO personRoleDTO = mapPersonRoleToDTO(personRole);
        Cache loginCache = createCacheMock(UserRepository.USERS_BY_LOGIN_CACHE);
        Cache emailCache = createCacheMock(UserRepository.USERS_BY_EMAIL_CACHE);

        doReturn(user).when(userDetailedMapper).userDTOToUser(eq(userDTO));
        doReturn(Set.of(personRole)).when(personRoleMapper).toEntity(eq(Set.of(personRoleDTO)));
        doReturn(Set.of(personRole)).when(personRoleRepository).findAllByIdIn(eq(Set.of(personRole.getId())));
        doReturn(loginCache).when(cacheManager).getCache(eq(UserRepository.USERS_BY_LOGIN_CACHE));
        doReturn(emailCache).when(cacheManager).getCache(eq(UserRepository.USERS_BY_EMAIL_CACHE));

        User savedUser = userService.createUser(userDTO);

        assertEquals(Set.of(personRole), savedUser.getPersonRoles());
    }

    @Test
    void shouldRegisterUserWithSchools() {
        School school = createSchool().id(1L);
        PersonRole personRole = createPersonRole()
            .id(1L)
            .school(school);
        User user = createUser()
            .personRoles(Set.of(personRole))
            .setAuthorities(Set.of(new Authority().name(STUDENT)));
        UserDetailedDTO userDTO = mapUserToDetailedDTO(user);
        PersonRoleDTO personRoleDTO = mapPersonRoleToDTO(personRole);
        Cache loginCache = createCacheMock(UserRepository.USERS_BY_LOGIN_CACHE);
        Cache emailCache = createCacheMock(UserRepository.USERS_BY_EMAIL_CACHE);

        doReturn(loginCache).when(cacheManager).getCache(eq(UserRepository.USERS_BY_LOGIN_CACHE));
        doReturn(emailCache).when(cacheManager).getCache(eq(UserRepository.USERS_BY_EMAIL_CACHE));
        doReturn(user).when(userDetailedMapper).userDTOToUser(eq(userDTO));
        doReturn(Set.of(personRole)).when(personRoleMapper).toEntity(eq(Set.of(personRoleDTO)));
        doReturn(Set.of(personRole)).when(personRoleRepository).findAllByIdIn(eq(Set.of(personRole.getId())));

        User savedUser = userService.registerUser(userDTO, user.getPassword());

        assertEquals(Set.of(personRole), savedUser.getPersonRoles());
    }

    @Test
    void shouldUpdateUserSchools() {
        School school = createSchool().id(1L);
        PersonRole personRole = createPersonRole().id(1L).school(school);
        PersonRole secondPersonRole = createPersonRole().id(2L).school(school);
        PersonRoleDTO secondPersonRoleDTO = mapPersonRoleToDTO(secondPersonRole);
        User existingUser = createUser()
            .setId(1L)
            .personRoles(Set.of(personRole));
        User modifiedUser = existingUser.personRoles(Set.of(secondPersonRole)).setTermsAgreed(true);
        UserDetailedDTO modifiedUserDTO = mapUserToDetailedDTO(modifiedUser);
        Cache loginCache = createCacheMock(UserRepository.USERS_BY_LOGIN_CACHE);
        Cache emailCache = createCacheMock(UserRepository.USERS_BY_EMAIL_CACHE);

        doReturn(Optional.of(existingUser)).when(userRepository).findById(eq(existingUser.getId()));
        doReturn(loginCache).when(cacheManager).getCache(eq(UserRepository.USERS_BY_LOGIN_CACHE));
        doReturn(emailCache).when(cacheManager).getCache(eq(UserRepository.USERS_BY_EMAIL_CACHE));
        doReturn(modifiedUserDTO).when(userDetailedMapper).userToUserDTO(eq(modifiedUser));


        Optional<UserDetailedDTO> savedUserOptional = userService.updateUser(modifiedUserDTO);

        assertTrue(savedUserOptional.isPresent());
        UserDetailedDTO savedUser = savedUserOptional.get();
        assertEquals(Set.of(secondPersonRoleDTO), savedUser.getPersonRoles());
        assertTrue(savedUser.isTermsAgreed());
    }

    @Test
    void shouldCreateEkoolUser() throws BadRequestException {
        long personRoleId = 1L;
        String externalId = "1";
        String expectedLogin = "ekoolFirstnameLastname1";

        HashMap<String, Object> attributes = new HashMap<>();
        attributes.put("id", externalId);
        attributes.put("firstName", USER_FIRST_NAME);
        attributes.put("lastName", USER_LAST_NAME);
        DefaultOAuth2User principal = new DefaultOAuth2User(List.of(new SimpleGrantedAuthority(ADMIN)), attributes, "id");

        Set<EkoolSchoolsDTO.EkoolSchoolDTO> ekoolSchools = Set.of(
            new EkoolSchoolsDTO.EkoolSchoolDTO()
                .setName(SCHOOL_NAME)
                .setId(SCHOOL_ID)
        );
        doReturn(ekoolSchools).when(ekoolService).getSchools(externalId);
        doReturn(USER_EMAIL).when(ekoolService).getEmail(externalId);
        doReturn(List.of(new School().name(SCHOOL_NAME))).when(schoolService).saveEkoolSchools(ekoolSchools);

        EkoolRolesDTO.EkoolRoleDTO ekoolRoleDTO = new EkoolRolesDTO.EkoolRoleDTO()
            .setSchoolId(SCHOOL_ID)
            .setRoleName(ROLE_NAME);
        doReturn(Set.of(ekoolRoleDTO)).when(ekoolService).getRoles(externalId);
        doReturn(new PersonRole().id(personRoleId)).when(ekoolRoleMapper).toEntity(ekoolRoleDTO);
        doAnswer(returnsFirstArg()).when(userRepository).save(argThat(argument -> argument.getLogin().equals(expectedLogin)));

        User ekoolUser = userService.save(principal, "ekool", null);

        assertEquals(USER_FIRST_NAME, ekoolUser.getFirstName());
        assertEquals(USER_LAST_NAME, ekoolUser.getLastName());
        assertEquals(USER_EMAIL, ekoolUser.getEmail());
        assertEquals("et", ekoolUser.getLangKey());
        assertTrue(ekoolUser.isActivated());
        assertTrue(ekoolUser.isFirstLogin());
        assertFalse(ekoolUser.isTermsAgreed());
        assertTrue(ekoolUser.getExternalAuthentications().stream().allMatch(externalAuthentication ->
            externalAuthentication.getExternalId().equals(externalId) && externalAuthentication.getType().equals(EKOOL)
        ));
        assertTrue(ekoolUser.getPersonRoles().stream().allMatch(personRole -> personRole.getId().equals(personRoleId)));
        assertEquals(expectedLogin, ekoolUser.getLogin());
    }

    @Test
    void shouldNotAddDuplicateSchoolRolesFromEkool() throws BadRequestException {
        long personRoleId = 1L;
        String externalId = "1";
        String expectedLogin = "ekoolFirstnameLastname1";

        HashMap<String, Object> attributes = new HashMap<>();
        attributes.put("id", externalId);
        attributes.put("firstName", USER_FIRST_NAME);
        attributes.put("lastName", USER_LAST_NAME);
        DefaultOAuth2User principal = new DefaultOAuth2User(List.of(new SimpleGrantedAuthority(ADMIN)), attributes, "id");

        Set<EkoolSchoolsDTO.EkoolSchoolDTO> ekoolSchools = Set.of(
            new EkoolSchoolsDTO.EkoolSchoolDTO()
                .setName(SCHOOL_NAME)
                .setId(SCHOOL_ID)
                .setEhisId("1")
        );
        doReturn(ekoolSchools).when(ekoolService).getSchools(externalId);
        doReturn(USER_EMAIL).when(ekoolService).getEmail(externalId);
        doReturn(List.of(new School().name(SCHOOL_NAME).externalId(SCHOOL_ID).ehisId("1"))).when(schoolService).saveEkoolSchools(ekoolSchools);

        EkoolRolesDTO.EkoolRoleDTO ekoolRoleDTO = new EkoolRolesDTO.EkoolRoleDTO()
            .setSchoolId(SCHOOL_ID)
            .setRoleName(ROLE_NAME);
        doReturn(Set.of(ekoolRoleDTO)).when(ekoolService).getRoles(externalId);
        doReturn(new PersonRole().id(personRoleId).role(Role.TEACHER)).when(ekoolRoleMapper).toEntity(ekoolRoleDTO);
        doAnswer(returnsFirstArg()).when(userRepository).save(argThat(argument -> argument.getLogin().equals(expectedLogin)));
        User existingUser = new User()
            .setLogin(expectedLogin)
            .personRoles(
                Set.of(new PersonRole().role(Role.TEACHER).school(new School().ehisId("1")))
            );
        doReturn(Optional.of(existingUser)).when(userRepository).findById(1L);

        User ekoolUser = userService.save(principal, "ekool", "1");

        assertTrue(ekoolUser.getExternalAuthentications().stream().allMatch(externalAuthentication ->
            externalAuthentication.getExternalId().equals(externalId) && externalAuthentication.getType().equals(EKOOL)
        ));
        assertEquals(1, ekoolUser.getPersonRoles().size());
    }

    private User createUser() {
        User user = new User();
        user.setLogin(DEFAULT_LOGIN);
        user.setPassword(RandomStringUtils.random(60));
        user.setActivated(true);
        user.setEmail(DEFAULT_EMAIL);
        user.setFirstName(DEFAULT_FIRSTNAME);
        user.setLastName(DEFAULT_LASTNAME);
        user.setImageUrl(DEFAULT_IMAGEURL);
        user.setLangKey(DEFAULT_LANGKEY);
        return user;
    }

    private Cache createCacheMock(String key) {
        return new CaffeineCache(key, mockedCache);
    }
}
