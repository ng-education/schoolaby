package com.schoolaby.service;

import com.schoolaby.config.ApplicationProperties;
import com.schoolaby.domain.User;
import com.schoolaby.service.dto.SupportEmailDTO;
import io.github.jhipster.config.JHipsterProperties;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.MessageSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.Locale;

import static com.schoolaby.common.TestCases.createTeacher;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class MailServiceTest {
    @Mock
    private JHipsterProperties jHipsterProperties;
    @Mock
    private ApplicationProperties applicationProperties;
    @Mock
    private MessageSource messageSource;
    @Spy
    private final JavaMailSender javaMailSender = new JavaMailSenderImpl();

    private MailService mailService;

    @BeforeEach
    public void setUp() {
        mailService = new MailService(jHipsterProperties, applicationProperties, javaMailSender, messageSource, new SpringTemplateEngine());
    }

    @Test
    void shouldSendSupportEmail() {
        User teacher = createTeacher().setId(1L);
        JHipsterProperties.Mail jhipsterMailProperties = new JHipsterProperties.Mail();
        jhipsterMailProperties.setFrom("teacher@localhost.com");

        ApplicationProperties.Mail appMailProperties = new ApplicationProperties.Mail();
        appMailProperties.setSupportEmail("support@localhost.com");

        SupportEmailDTO supportEmailDTO = new SupportEmailDTO()
            .setSubject("Test subject")
            .setContent("Test content");

        doReturn(jhipsterMailProperties).when(jHipsterProperties).getMail();
        doReturn(appMailProperties).when(applicationProperties).getMail();
        doReturn(supportEmailDTO.getSubject()).when(messageSource).getMessage(
            eq("email.support.title"),
            eq(new Object[]{supportEmailDTO.getSubject()}),
            eq(Locale.ENGLISH)
        );

        mailService.sendSupportEmail(supportEmailDTO, teacher);

        verify(javaMailSender, times(1)).send(argThat((MimeMessage mimeMessage) -> {
            try {
                return mimeMessage.getSubject().equals(supportEmailDTO.getSubject()) &&
                    mimeMessage.getContent().toString().equals("mail/supportEmail");
            } catch (MessagingException | IOException e) {
                e.printStackTrace();
            }
            return false;
        }));
    }
}
