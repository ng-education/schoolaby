package com.schoolaby.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class GradingSchemeValueMapperTest {
    private GradingSchemeValueMapper gradingSchemeValueMapper;

    @BeforeEach
    public void setUp() {
        gradingSchemeValueMapper = new GradingSchemeValueMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(gradingSchemeValueMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(gradingSchemeValueMapper.fromId(null)).isNull();
    }
}
