package com.schoolaby.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class UploadedFileMapperTest {
    private UploadedFileMapper uploadedFileMapper;

    @BeforeEach
    public void setUp() {
        uploadedFileMapper = new UploadedFileMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(uploadedFileMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(uploadedFileMapper.fromId(null)).isNull();
    }
}
