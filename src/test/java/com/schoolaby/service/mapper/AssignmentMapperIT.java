package com.schoolaby.service.mapper;

import com.schoolaby.common.BaseIntegrationTest;
import com.schoolaby.domain.Assignment;
import com.schoolaby.domain.Milestone;
import com.schoolaby.domain.User;
import com.schoolaby.service.dto.AssignmentDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithUserDetails;

import static com.schoolaby.common.TestObjects.*;
import static org.assertj.core.api.Assertions.assertThat;

class AssignmentMapperIT extends BaseIntegrationTest {
    private final String CREATOR_FIRST_NAME = "John";
    private final String CREATOR_LAST_NAME = "Doe";

    @Autowired
    private AssignmentMapper assignmentMapper;

    @Test
    void shouldCreateEntityFromId() {
        Long id = 1L;
        assertThat(assignmentMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(assignmentMapper.fromId(null)).isNull();
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldMapCreatorFirstnameAndLastnameToTeacherName() {
        AssignmentDTO assignmentDTO = assignmentMapper.toDto(createAssignmentWithJourney());
        assertThat(assignmentDTO.getTeacherName()).isEqualTo("John Doe");
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldMapHasGrades() {
        Assignment assignment = createAssignmentWithJourney();
        AssignmentDTO assignmentWithoutGrades = assignmentMapper.toDto(assignment);
        assertThat(assignmentWithoutGrades.isHasGrades()).isFalse();

        assignment.addSubmission(createSubmission().addSubmissionFeedback(createSubmissionFeedback().grade("5")));
        AssignmentDTO assignmentWithGrades = assignmentMapper.toDto(assignment);
        assertThat(assignmentWithGrades.isHasGrades()).isTrue();
    }

    private Assignment createAssignmentWithJourney() {
        User user1 = createUser();
        user1.setFirstName(CREATOR_FIRST_NAME);
        user1.setLastName(CREATOR_LAST_NAME);
        Assignment assignment = createAssignment();
        Milestone milestone = createMilestone().addAssignment(assignment);
        createJourney()
            .addMilestone(milestone)
            .setCreator(user1);

        return assignment;
    }
}
