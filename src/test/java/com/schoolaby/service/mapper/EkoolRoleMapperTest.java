package com.schoolaby.service.mapper;

import com.schoolaby.domain.PersonRole;
import com.schoolaby.security.Role;
import com.schoolaby.service.dto.EkoolRolesDTO;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

class EkoolRoleMapperTest {
    private final EkoolRoleMapper ekoolRoleMapper = new EkoolRoleMapperImpl();

    @Test
    void toEntity() {
        EkoolRolesDTO.EkoolRoleDTO ekoolRoleDTO = new EkoolRolesDTO.EkoolRoleDTO()
            .setRoleName(Role.STUDENT.getEkoolMarker());

        PersonRole personRole = ekoolRoleMapper.toEntity(ekoolRoleDTO);

        assertEquals(Role.STUDENT, personRole.getRole());
        assertFalse(personRole.isActive());
    }

    @Test
    void toDto() {
        PersonRole personRole = new PersonRole()
            .role(Role.STUDENT);

        EkoolRolesDTO.EkoolRoleDTO ekoolRoleDTO = ekoolRoleMapper.toDto(personRole);

        assertEquals(Role.STUDENT.getEkoolMarker(), ekoolRoleDTO.getRoleName());
    }
}
