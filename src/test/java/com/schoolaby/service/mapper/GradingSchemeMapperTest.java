package com.schoolaby.service.mapper;

import com.schoolaby.domain.GradingSchemeValue;
import com.schoolaby.service.dto.GradingSchemeValueDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static java.util.Collections.emptySet;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class GradingSchemeMapperTest {
    private GradingSchemeMapper gradingSchemeMapper;

    @BeforeEach
    public void setUp() {
        gradingSchemeMapper = new GradingSchemeMapperImpl();
    }

    @Test
    void testEntityFromId() {
        Long id = 1L;
        assertThat(gradingSchemeMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(gradingSchemeMapper.fromId(null)).isNull();
    }

    @Test
    void shouldMapGradingSchemeValues() {
        Set<GradingSchemeValue> gradingSchemeValues = Set.of(
            new GradingSchemeValue().setPercentageRange(0),
            new GradingSchemeValue().setPercentageRange(20),
            new GradingSchemeValue().setPercentageRange(40),
            new GradingSchemeValue().setPercentageRange(60),
            new GradingSchemeValue().setPercentageRange(80)
        );

        Set<GradingSchemeValueDTO> result = gradingSchemeMapper.mapGradingSchemeValues(gradingSchemeValues);

        assertEquals(5, result.size());
        assertTrue(result.stream().anyMatch(gradingSchemeValueDTO ->
            gradingSchemeValueDTO.getPercentageRangeStart().equals(0) &&
                gradingSchemeValueDTO.getPercentageRangeEnd().equals(20)
        ));
        assertTrue(result.stream().anyMatch(gradingSchemeValueDTO ->
            gradingSchemeValueDTO.getPercentageRangeStart().equals(20) &&
                gradingSchemeValueDTO.getPercentageRangeEnd().equals(40)
        ));
        assertTrue(result.stream().anyMatch(gradingSchemeValueDTO ->
            gradingSchemeValueDTO.getPercentageRangeStart().equals(40) &&
                gradingSchemeValueDTO.getPercentageRangeEnd().equals(60)
        ));
        assertTrue(result.stream().anyMatch(gradingSchemeValueDTO ->
            gradingSchemeValueDTO.getPercentageRangeStart().equals(60) &&
                gradingSchemeValueDTO.getPercentageRangeEnd().equals(80)
        ));
    }

    @Test
    void shouldMapGradingSchemeValuesWithOneEntry() {
        Set<GradingSchemeValue> gradingSchemeValues = Set.of(
            new GradingSchemeValue().setPercentageRange(0)
        );

        Set<GradingSchemeValueDTO> result = gradingSchemeMapper.mapGradingSchemeValues(gradingSchemeValues);

        assertEquals(1, result.size());
        assertTrue(result.stream().anyMatch(gradingSchemeValueDTO ->
            gradingSchemeValueDTO.getPercentageRangeStart().equals(0) &&
                gradingSchemeValueDTO.getPercentageRangeEnd().equals(100)
        ));
    }

    @Test
    void shouldMapGradingSchemeValuesWithNoResults() {
        Set<GradingSchemeValueDTO> result = gradingSchemeMapper.mapGradingSchemeValues(emptySet());

        assertEquals(0, result.size());
    }
}
