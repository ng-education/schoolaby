package com.schoolaby.service.mapper;

import com.schoolaby.common.BaseIntegrationTest;
import com.schoolaby.domain.Group;
import com.schoolaby.domain.Notification;
import com.schoolaby.domain.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;

import static com.schoolaby.common.TestObjects.*;
import static org.assertj.core.api.Assertions.assertThat;

class NotificationMapperIT extends BaseIntegrationTest {
    @Autowired
    private NotificationMapper notificationMapper;

    @Test
    public void shouldMapGroupName() {
        User creator = createUser();
        Group group = createGroup().setStudents(Set.of(creator));
        Notification notification = createNotification()
            .creator(creator)
            .message("{SUBMITTED_SOLUTION}")
            .assignment(createAssignment()
                .setGroups(Set.of(group)));

        assertThat(notificationMapper.mapGroupName(notification)).isEqualTo(group.getName());
    }

    @Test
    public void shouldNotMapGroupName() {
        User creator = createUser();
        Group group = createGroup().setStudents(Set.of(creator));
        Notification notification = createNotification()
            .creator(creator)
            .assignment(createAssignment()
                .setGradeGroupMembersIndividually(true)
                .setGroups(Set.of(group)));

        assertThat(notificationMapper.mapGroupName(notification)).isNull();
    }
}
