package com.schoolaby.service.mapper;

import com.schoolaby.domain.PersonRole;
import com.schoolaby.security.Role;
import com.schoolaby.security.harid.HarIdUser;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HarIdRoleMapperTest {
    private final HarIdRoleMapper harIdRoleMapper = new HarIdRoleMapperImpl();

    @Test
    void shouldMapRole() {
        HarIdUser.Role haridUserRole = new HarIdUser.Role()
            .marker(Role.STUDENT.getHarIdMarker());

        PersonRole personRole = harIdRoleMapper.toEntity(haridUserRole);

        assertEquals(Role.STUDENT, personRole.getRole());
    }

    @Test
    void shouldMapMarker() {
        PersonRole personRole = new PersonRole().role(Role.STUDENT);

        HarIdUser.Role haridRole = harIdRoleMapper.toDto(personRole);

        assertEquals(Role.STUDENT.getHarIdMarker(), haridRole.getMarker());
    }
}
