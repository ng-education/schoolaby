package com.schoolaby.service;

import com.schoolaby.domain.Journey;
import com.schoolaby.domain.User;
import com.schoolaby.event.JourneyStateCalculationEvent;
import com.schoolaby.repository.JourneyRepository;
import com.schoolaby.service.mapper.JourneyMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

import static com.schoolaby.common.TestCases.createTeacher;
import static com.schoolaby.common.TestObjects.createJourney;
import static com.schoolaby.common.TestObjects.createUser;
import static com.schoolaby.security.SecurityUtils.createSpringSecurityUser;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class JourneyServiceTest {
    @Mock
    private JourneyRepository journeyRepository;
    @Mock
    private JourneyStudentService journeyStudentService;
    @Mock
    private JourneyMapper journeyMapper;
    @Mock
    private JourneyStateService journeyStateService;
    @Mock
    private UserService userService;
    @Mock
    private ApplicationEventPublisher applicationEventPublisher;
    @Mock
    private Authentication authentication;
    @Mock
    private SecurityContext securityContext;


    @InjectMocks
    private JourneyService journeyService;

    @Test
    void shouldRemoveStudent() {
        User student = createUser().setId(1L);
        User teacher = createTeacher().setId(2L);
        com.schoolaby.security.User securityUser = createSpringSecurityUser(teacher);
        Journey journey = createJourney()
            .id(2L)
            .addStudent(student)
            .addTeacher(teacher);
        SecurityContextHolder.setContext(securityContext);

        doReturn(authentication).when(securityContext).getAuthentication();
        when(SecurityContextHolder.getContext().getAuthentication().getPrincipal()).thenReturn(securityUser);
        doReturn(Optional.of(journey)).when(journeyRepository).findOne(eq(journey.getId()), eq(false));
        journeyService.removeStudent(journey.getId(), student.getId());

        verify(journeyStudentService).delete(eq(journey.getId()), eq(student.getId()));
        verify(applicationEventPublisher).publishEvent(argThat(event -> ((JourneyStateCalculationEvent) event).getJourney().equals(journey)));
    }

    @Test
    void shouldFindUserTemplatesBySearch() {
        String search = "test";
        Long subjectId = 1L;
        Long userId = 5L;
        Pageable pageable = Pageable.unpaged();

        doReturn(Page.empty()).when(journeyRepository).findUserTemplatesBySearchAndSubject(eq(pageable), eq(search), eq(subjectId), eq(userId));
        journeyService.getJourneyTemplates(pageable, search, subjectId, userId);

        verify(journeyRepository).findUserTemplatesBySearchAndSubject(eq(pageable), eq(search), eq(subjectId), eq(userId));
    }

    @Test
    void shouldFindAllTemplatesBySearch() {
        String search = "test";
        Long subjectId = 1L;
        String country = "Estonia";
        User user = createUser().setCountry(country);
        Pageable pageable = Pageable.unpaged();

        doReturn(Optional.of(user)).when(userService).getUserWithAuthorities();
        doReturn(Page.empty()).when(journeyRepository).findAllTemplatesBySearchAndSubjectAndCountry(eq(pageable), eq(search), eq(subjectId), eq(country));
        journeyService.getJourneyTemplates(pageable, search, subjectId, null);

        verify(journeyRepository).findAllTemplatesBySearchAndSubjectAndCountry(eq(pageable), eq(search), eq(subjectId), eq(country));
    }
}
