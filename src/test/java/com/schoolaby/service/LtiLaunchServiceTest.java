package com.schoolaby.service;

import com.schoolaby.config.lti.LtiProperties;
import com.schoolaby.domain.*;
import com.schoolaby.repository.LtiLaunchRepository;
import com.schoolaby.service.dto.lti.LtiScoreDTO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.List;

import static com.schoolaby.common.TestObjects.*;
import static com.schoolaby.security.Role.Constants.STUDENT;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class LtiLaunchServiceTest {

    @Mock
    private SimpMessagingTemplate simpMessagingTemplate;
    @Mock
    private LtiResourceService ltiResourceService;
    @Mock
    private UserService userService;
    @Mock
    private LtiLaunchRepository ltiLaunchRepository;
    @Mock
    private LtiProperties ltiProperties;
    @Mock
    private SecurityContext securityContext;
    @Mock
    private Authentication authentication;

    @InjectMocks
    private LtiLaunchService ltiLaunchService;

    @ParameterizedTest
    @ValueSource(booleans = {true, false})
    void shouldLaunchResource(boolean syncGrade) {
        User user = createUser();
        LtiConfig ltiConfig = createLtiConfig();
        Assignment assignment = createAssignment().setMilestone(createMilestone().journey(createJourney()));
        LtiResource ltiResource = createLtiResource().setId(1L).setLaunchUrl("http://localhost/test_launch").setLtiApp(createLtiApp().addConfig(ltiConfig)).setAssignment(assignment).setSyncGrade(syncGrade);
        LtiLaunch ltiLaunch = createLtiLaunch().setLtiResource(ltiResource).setUser(user).setId(1L);

        doReturn(ltiResource).when(ltiResourceService).getOne(eq(ltiResource.getId()));
        doReturn(ltiLaunch).when(ltiLaunchRepository).save(argThat((launch) -> launch.getLtiResource().equals(ltiResource)));
        doReturn("iframe").when(ltiProperties).getLaunchPresentationDocumentTarget();
        doReturn(List.of(new SimpleGrantedAuthority(STUDENT))).when(authentication).getAuthorities();
        doReturn(authentication).when(securityContext).getAuthentication();
        SecurityContextHolder.setContext(securityContext);

        if(syncGrade) {
            doReturn(List.of(ltiLaunch)).when(ltiLaunchRepository).findAll(eq(assignment.getId()), eq(user.getId()), eq(null));
            doReturn(user).when(userService).getOneById(eq(user.getId()));
        }

        ltiLaunchService.launchResource(ltiResource.getId(), ltiResource.getLaunchUrl());

        if(syncGrade) {
            verify(simpMessagingTemplate).convertAndSendToUser(eq(user.getLogin()), eq("/queue/lti-scores"), argThat((payload) -> {
                LtiScoreDTO ltiScoreDTO = (LtiScoreDTO) payload;
                return ltiScoreDTO.getResourceId().equals(ltiResource.getId()) && ltiScoreDTO.getScore() == null;
            }));
        } else {
            verifyNoInteractions(simpMessagingTemplate);
        }
    }
}
