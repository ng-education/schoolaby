package com.schoolaby.service;

import com.schoolaby.domain.Subject;
import com.schoolaby.repository.SubjectJpaRepository;
import com.schoolaby.service.dto.SubjectDTO;
import com.schoolaby.service.mapper.SubjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SubjectServiceTest {
    @Mock
    private SubjectJpaRepository subjectJpaRepository;
    @Mock
    private SubjectMapper subjectMapper;

    @InjectMocks
    private SubjectService subjectService;

    @Test
    void shouldFindAllByCountry() {
        Set<Subject> subjectRepositoryResponse = Set.of(
            new Subject().setCountry("Estonia").setLabel("Test label").setId(1L)
        );
        when(subjectJpaRepository.findAllByCountry(eq("Estonia"))).thenReturn(subjectRepositoryResponse);
        when(subjectMapper.toDto(eq(subjectRepositoryResponse))).thenAnswer(invocation -> List.of(
            new SubjectDTO().setId(1L).setLabel("Test label")
        ));

        List<SubjectDTO> result = new ArrayList<>(subjectService.findAllByCountry("Estonia"));

        assertEquals(1, result.size());
        assertEquals(1L, result.get(0).getId());
        assertEquals("Test label", result.get(0).getLabel());
    }
}
