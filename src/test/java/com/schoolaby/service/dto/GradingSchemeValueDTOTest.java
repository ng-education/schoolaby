package com.schoolaby.service.dto;

import com.schoolaby.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class GradingSchemeValueDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(GradingSchemeValueDTO.class);
        GradingSchemeValueDTO gradingSchemeValueDTO1 = new GradingSchemeValueDTO();
        gradingSchemeValueDTO1.setId(1L);
        GradingSchemeValueDTO gradingSchemeValueDTO2 = new GradingSchemeValueDTO();
        assertThat(gradingSchemeValueDTO1).isNotEqualTo(gradingSchemeValueDTO2);
        gradingSchemeValueDTO2.setId(gradingSchemeValueDTO1.getId());
        assertThat(gradingSchemeValueDTO1).isEqualTo(gradingSchemeValueDTO2);
        gradingSchemeValueDTO2.setId(2L);
        assertThat(gradingSchemeValueDTO1).isNotEqualTo(gradingSchemeValueDTO2);
        gradingSchemeValueDTO1.setId(null);
        assertThat(gradingSchemeValueDTO1).isNotEqualTo(gradingSchemeValueDTO2);
    }
}
