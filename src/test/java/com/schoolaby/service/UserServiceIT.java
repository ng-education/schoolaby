package com.schoolaby.service;

import com.schoolaby.SchoolabyApp;
import com.schoolaby.config.Constants;
import com.schoolaby.domain.Authority;
import com.schoolaby.domain.PersonRole;
import com.schoolaby.domain.User;
import com.schoolaby.repository.UserRepository;
import com.schoolaby.security.Role;
import com.schoolaby.security.harid.HarIdUser;
import com.schoolaby.service.dto.UserDetailedDTO;
import io.github.jhipster.security.RandomUtil;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.auditing.AuditingHandler;
import org.springframework.data.auditing.DateTimeProvider;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = SchoolabyApp.class)
@Transactional
class UserServiceIT {
    private static final String DEFAULT_LOGIN = "johndoe";
    private static final String DEFAULT_EMAIL = "johndoe@localhost";
    private static final String DEFAULT_FIRSTNAME = "john";
    private static final String DEFAULT_LASTNAME = "doe";
    private static final String DEFAULT_IMAGEURL = "http://placehold.it/50x50";
    private static final String DEFAULT_LANGKEY = "dummy";

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private AuditingHandler auditingHandler;
    @Mock
    private DateTimeProvider dateTimeProvider;

    private User user;

    @BeforeEach
    void init() {
        user = new User();
        user.setLogin(DEFAULT_LOGIN);
        user.setPassword(RandomStringUtils.random(60));
        user.setActivated(true);
        user.setEmail(DEFAULT_EMAIL);
        user.setFirstName(DEFAULT_FIRSTNAME);
        user.setLastName(DEFAULT_LASTNAME);
        user.setImageUrl(DEFAULT_IMAGEURL);
        user.setLangKey(DEFAULT_LANGKEY);

        when(dateTimeProvider.getNow()).thenReturn(Optional.of(LocalDateTime.now()));
        auditingHandler.setDateTimeProvider(dateTimeProvider);
    }

    @Test
    @Transactional
    void assertThatUserMustExistToResetPassword() {
        userRepository.saveAndFlush(user);
        Optional<User> maybeUser = userService.requestPasswordReset("invalid.login@localhost");
        assertThat(maybeUser).isNotPresent();

        maybeUser = userService.requestPasswordReset(user.getEmail());
        assertThat(maybeUser).isPresent();
        assertThat(maybeUser.orElse(null).getEmail()).isEqualTo(user.getEmail());
        assertThat(maybeUser.orElse(null).getResetDate()).isNotNull();
        assertThat(maybeUser.orElse(null).getResetKey()).isNotNull();
    }

    @Test
    @Transactional
    void assertThatOnlyActivatedUserCanRequestPasswordReset() {
        user.setActivated(false);
        userRepository.saveAndFlush(user);

        Optional<User> maybeUser = userService.requestPasswordReset(user.getLogin());
        assertThat(maybeUser).isNotPresent();
        userRepository.delete(user);
    }

    @Test
    @Transactional
    void assertThatResetKeyMustNotBeOlderThan24Hours() {
        Instant daysAgo = Instant.now().minus(25, ChronoUnit.HOURS);
        String resetKey = RandomUtil.generateResetKey();
        user.setActivated(true);
        user.setResetDate(daysAgo);
        user.setResetKey(resetKey);
        userRepository.saveAndFlush(user);

        Optional<User> maybeUser = userService.completePasswordReset("johndoe2", user.getResetKey());
        assertThat(maybeUser).isNotPresent();
        userRepository.delete(user);
    }

    @Test
    @Transactional
    void assertThatResetKeyMustBeValid() {
        Instant daysAgo = Instant.now().minus(25, ChronoUnit.HOURS);
        user.setActivated(true);
        user.setResetDate(daysAgo);
        user.setResetKey("1234");
        userRepository.saveAndFlush(user);

        Optional<User> maybeUser = userService.completePasswordReset("johndoe2", user.getResetKey());
        assertThat(maybeUser).isNotPresent();
        userRepository.delete(user);
    }

    @Test
    @Transactional
    void assertThatUserCanResetPassword() {
        String oldPassword = user.getPassword();
        Instant daysAgo = Instant.now().minus(2, ChronoUnit.HOURS);
        String resetKey = RandomUtil.generateResetKey();
        user.setActivated(true);
        user.setResetDate(daysAgo);
        user.setResetKey(resetKey);
        userRepository.saveAndFlush(user);

        Optional<User> maybeUser = userService.completePasswordReset("johndoe2", user.getResetKey());
        assertThat(maybeUser).isPresent();
        assertThat(maybeUser.orElse(null).getResetDate()).isNull();
        assertThat(maybeUser.orElse(null).getResetKey()).isNull();
        assertThat(maybeUser.orElse(null).getPassword()).isNotEqualTo(oldPassword);

        userRepository.delete(user);
    }

    @Test
    @Transactional
    void assertThatNotActivatedUsersWithNotNullActivationKeyCreatedBefore14DaysAreDeleted() {
        Instant now = Instant.now();
        when(dateTimeProvider.getNow()).thenReturn(Optional.of(now.minus(15, ChronoUnit.DAYS)));
        user.setActivated(false);
        user.setActivationKey(RandomStringUtils.random(20));
        User dbUser = userRepository.saveAndFlush(user);
        dbUser.setCreatedDate(now.minus(4, ChronoUnit.DAYS));
        userRepository.saveAndFlush(user);
        Instant threeDaysAgo = now.minus(3, ChronoUnit.DAYS);
        List<User> users = userRepository.findAllByActivatedIsFalseAndActivationKeyIsNotNullAndCreatedDateBefore(threeDaysAgo);
        assertThat(users).isNotEmpty();
        userService.removeNotActivatedUsers();
        users = userRepository.findAllByActivatedIsFalseAndActivationKeyIsNotNullAndCreatedDateBefore(threeDaysAgo);
        assertThat(users).isEmpty();
    }

    @Test
    @Transactional
    void assertThatNotActivatedUsersWithNullActivationKeyCreatedBefore3DaysAreNotDeleted() {
        Instant now = Instant.now();
        when(dateTimeProvider.getNow()).thenReturn(Optional.of(now.minus(4, ChronoUnit.DAYS)));
        user.setActivated(false);
        User dbUser = userRepository.saveAndFlush(user);
        dbUser.setCreatedDate(now.minus(4, ChronoUnit.DAYS));
        userRepository.saveAndFlush(user);
        Instant threeDaysAgo = now.minus(3, ChronoUnit.DAYS);
        List<User> users = userRepository.findAllByActivatedIsFalseAndActivationKeyIsNotNullAndCreatedDateBefore(threeDaysAgo);
        assertThat(users).isEmpty();
        userService.removeNotActivatedUsers();
        Optional<User> maybeDbUser = userRepository.findById(dbUser.getId());
        assertThat(maybeDbUser).contains(dbUser);
    }

    @Test
    @Transactional
    void assertThatAnonymousUserIsNotGet() {
        user.setLogin(Constants.ANONYMOUS_USER);
        if (userRepository.findOneByLogin(Constants.ANONYMOUS_USER).isEmpty()) {
            userRepository.saveAndFlush(user);
        }
        final PageRequest pageable = PageRequest.of(0, (int) userRepository.count());
        final Page<UserDetailedDTO> allManagedUsers = userService.findAllManagedUsers(pageable);
        assertThat(allManagedUsers.getContent().stream().noneMatch(user -> Constants.ANONYMOUS_USER.equals(user.getLogin()))).isTrue();
    }

    @Test
    @Transactional
    void shouldFindOneByEmailIgnoreCase() {
        userRepository.saveAndFlush(user);
        Optional<User> result = userService.findOneByEmailIgnoreCase(this.user.getEmail());
        assertThat(result.orElseThrow().getEmail()).isEqualTo(DEFAULT_EMAIL);
    }

    @Test
    @Transactional
    void shouldFindOneByLogin() {
        userRepository.saveAndFlush(user);
        Optional<User> result = userService.findOneByLogin(this.user.getLogin());
        assertThat(result.orElseThrow().getLogin()).isEqualTo(DEFAULT_LOGIN);
    }

    @Test
    @Transactional
    void shouldOverrideWithHarIdAuthoritiesWhenNoneExist() {
        user.setEmail("testerguy@localhost");
        userRepository.saveAndFlush(user);

        HarIdUser harIdUser = new HarIdUser();
        Set<HarIdUser.Role> harIdRoles = new HashSet<>();
        harIdRoles.add(new HarIdUser.Role().marker("faculty"));
        harIdUser.setRoles(harIdRoles);
        harIdUser.setEmail("testerguy@localhost");

        User result = userService.save(harIdUser);
        assertEquals(1, result.getAuthorities().size());
        assertEquals(1, result.getPersonRoles().size());
        assertThat(result.getAuthorities()).allMatch(authority -> authority.getName().equals("ROLE_TEACHER"));
        assertThat(result.getPersonRoles()).allMatch(personRole -> personRole.getRole().equals(Role.TEACHER));
    }

    @Test
    @Transactional
    void shouldNotOverrideWithHarIdAuthoritiesWhenTheyExist() {
        Authority studentAuthority = new Authority().name("ROLE_STUDENT");
        PersonRole studentRole = new PersonRole().role(Role.STUDENT);
        Set<Authority> userAuthorities = new HashSet<>();
        userAuthorities.add(studentAuthority);
        Set<PersonRole> userRoles = new HashSet<>();
        userRoles.add(studentRole);
        user.setEmail("testerguy@localhost");
        user.setAuthorities(userAuthorities);
        user.setPersonRoles(userRoles);
        userRepository.saveAndFlush(user);

        HarIdUser harIdUser = new HarIdUser();
        Set<HarIdUser.Role> harIdRoles = new HashSet<>();
        harIdRoles.add(new HarIdUser.Role().marker("faculty"));
        harIdUser.setRoles(harIdRoles);
        harIdUser.setEmail("testerguy@localhost");

        User result = userService.save(harIdUser);
        assertEquals(1, result.getAuthorities().size());
        assertEquals(1, result.getPersonRoles().size());
        assertThat(result.getAuthorities()).allMatch(authority -> authority.getName().equals("ROLE_STUDENT"));
        assertThat(result.getPersonRoles()).allMatch(personRole -> personRole.getRole().equals(Role.TEACHER));
    }
}
