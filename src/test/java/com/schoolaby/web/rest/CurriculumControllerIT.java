package com.schoolaby.web.rest;

import com.schoolaby.common.BaseIntegrationTest;
import org.junit.jupiter.api.Test;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CurriculumControllerIT extends BaseIntegrationTest {
    private final String TANZANIAN_CURRICULUM = "TANZANIAN_CURRICULUM";
    private final String ESTONIAN_CURRICULUM = "ESTONIAN_CURRICULUM";
    private final String NATIONAL_CURRICULUM = "NATIONAL_CURRICULUM";

    @Test
    void shouldGetAllTanzanianCurriculums() throws Exception {
        mockMvc.perform(get("/api/curricula")
                .param("country", "Tanzania")
                .contentType(APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(2)))
            .andExpect(jsonPath("$[*].title").value(containsInAnyOrder(TANZANIAN_CURRICULUM, NATIONAL_CURRICULUM)));
    }

    @Test
    void shouldGetAllEstonianCurriculums() throws Exception {
        mockMvc.perform(get("/api/curricula")
                .param("country", "Estonia")
                .contentType(APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(2)))
            .andExpect(jsonPath("$[*].title").value(containsInAnyOrder(ESTONIAN_CURRICULUM, NATIONAL_CURRICULUM)));
    }

    @Test
    void shouldGetAllEstonianCurriculumsEvenIfCountryUnitedStates() throws Exception {
        mockMvc.perform(get("/api/curricula")
                .param("country", "United States")
                .contentType(APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(2)))
            .andExpect(jsonPath("$[*].title").value(containsInAnyOrder(ESTONIAN_CURRICULUM, NATIONAL_CURRICULUM)));
    }

    @Test
    void shouldGetAllEstonianCurriculumsEvenIfCountryNull() throws Exception {
        mockMvc.perform(get("/api/curricula")
                .contentType(APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(2)))
            .andExpect(jsonPath("$[*].title").value(containsInAnyOrder(ESTONIAN_CURRICULUM, NATIONAL_CURRICULUM)));
    }
}
