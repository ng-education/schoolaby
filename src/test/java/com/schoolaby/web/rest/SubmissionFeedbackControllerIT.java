package com.schoolaby.web.rest;

import com.schoolaby.common.BaseIntegrationTest;
import com.schoolaby.domain.*;
import com.schoolaby.repository.JourneyStudentJpaRepository;
import com.schoolaby.repository.NotificationJpaRepository;
import com.schoolaby.repository.SubmissionJpaRepository;
import com.schoolaby.service.dto.SelectedCriterionLevelDTO;
import com.schoolaby.service.dto.submission.SubmissionDTO;
import com.schoolaby.service.dto.submission.SubmissionFeedbackDTO;
import com.schoolaby.service.dto.submission.SubmissionGradingPatchDTO;
import com.schoolaby.service.dto.submission.SubmissionPatchDTO;
import com.schoolaby.service.mapper.CriterionLevelMapper;
import com.schoolaby.service.mapper.SelectedCriterionLevelMapper;
import com.schoolaby.service.mapper.SubmissionMapper;
import com.schoolaby.web.WithMockCustomUser;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.schoolaby.common.TestCases.*;
import static com.schoolaby.common.TestObjects.*;
import static com.schoolaby.security.Role.Constants.STUDENT;
import static com.schoolaby.web.rest.TestUtil.convertObjectToJsonBytes;
import static java.time.Instant.now;
import static java.time.temporal.ChronoUnit.DAYS;
import static java.time.temporal.ChronoUnit.MILLIS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class SubmissionFeedbackControllerIT extends BaseIntegrationTest {
    private static final String FAILING_GRADE = "2";
    private static final String PATCH_GRADE = "3";
    private static final String DEFAULT_GRADE_LOWER_CASE = "a";
    private static final String DEFAULT_GRADE_UPPER_CASE = "A";
    private static final Instant PATCH_FEEDBACK_DATE = now().plus(1, DAYS);
    private static final boolean PATCH_RESUBMITTABLE = !SUBMISSION_RESUBMITTABLE;
    private static final Instant PATCH_SUBMITTED_FOR_GRADING_DATE = now().plus(1, DAYS);

    @Autowired
    private SubmissionJpaRepository submissionJpaRepository;
    @Autowired
    private NotificationJpaRepository notificationRepository;
    @Autowired
    private JourneyStudentJpaRepository journeyStudentRepository;
    @Autowired
    private SelectedCriterionLevelMapper selectedCriterionLevelMapper;
    @Autowired
    private CriterionLevelMapper criterionLevelMapper;
    @Autowired
    private SubmissionMapper submissionMapper;

    private Submission submission;
    private Journey journey;
    private User student;
    private Group group;
    private Assignment assignment;
    private SubmissionFeedback submissionFeedback;

    private SubmissionGradingPatchDTO getSubmissionGradingPatchDTO(Long studentId, Long groupId, Submission submission, String grade) {
        return getSubmissionGradingPatchDTO(studentId, groupId, submission, grade, null);
    }

    private SubmissionGradingPatchDTO getSubmissionGradingPatchDTO(Long studentId, Long groupId, Submission submission, String grade, Set<SelectedCriterionLevelDTO> selectedCriterionLevels) {
        SubmissionFeedbackDTO submissionFeedbackDTO = new SubmissionFeedbackDTO()
            .studentId(studentId)
            .groupId(groupId)
            .grade(grade)
            .feedbackDate(PATCH_FEEDBACK_DATE)
            .setSelectedCriterionLevels(selectedCriterionLevels);

        if (submission.getId() != null) {
            submissionFeedbackDTO.submissionId(submission.getId());
        } else {
            submissionFeedbackDTO.submission(submissionMapper.toDto(submission));
        }

        return new SubmissionGradingPatchDTO()
            .submissionFeedback(submissionFeedbackDTO)
            .resubmittable(PATCH_RESUBMITTABLE)
            .submittedForGradingDate(submission.getSubmittedForGradingDate());
    }

    private Assignment assignmentWithReferences() {
        User teacher = getTeacher(entityManager);
        EducationalLevel educationalLevel = createEducationalLevel();
        journey = createJourney();
        Milestone milestone = createMilestone();
        Assignment assignment = createAssignment();
        journey.creator(teacher)
            .addTeacher(teacher)
            .addStudent(getStudent(entityManager))
            .educationalLevel(educationalLevel)
            .addMilestone(milestone
                .creator(teacher)
                .addAssignment(assignment
                    .creator(teacher)
                    .gradingScheme(getGradingSchemeNumerical(entityManager))));
        return assignment;
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldAllowTeacherToCreateSubmissionWithGrade() {
        transactionHelper.withTransaction(() -> {
            student = getStudent(entityManager);
            Assignment assignment = assignmentWithReferences();
            Rubric rubric = createRubric();
            Criterion criterion = createCriterion().setRubric(rubric);
            Criterion secondCriterion = createCriterion().setRubric(rubric);
            CriterionLevel criterionLevel = createCriterionLevel().setCriterion(criterion);
            CriterionLevel secondCriterionLevel = createCriterionLevel().setCriterion(secondCriterion);
            persistCreatedEntities(entityManager);
            submission = createSubmission();
            submission.setAssignment(assignment);
            submission.addAuthor(student);
            Set<SelectedCriterionLevelDTO> selectedCriterionLevelDTOs = Set.of(
                getSelectedCriterionLevelDTO().setCriterionLevel(criterionLevelMapper.toDto(criterionLevel)),
                getSelectedCriterionLevelDTO().setCriterionLevel(criterionLevelMapper.toDto(secondCriterionLevel)));
            SubmissionGradingPatchDTO submissionGradingPatchDTO = getSubmissionGradingPatchDTO(student.getId(), null, submission, FAILING_GRADE, selectedCriterionLevelDTOs).resubmittable(SUBMISSION_RESUBMITTABLE);

            try {
                mockMvc.perform(post("/api/submission-feedback").contentType(APPLICATION_JSON).content(convertObjectToJsonBytes(submissionGradingPatchDTO)))
                    .andExpect(status().isCreated());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        transactionHelper.withTransaction(() -> {
            // Validate the Submission in the database
            List<Submission> submissions = submissionJpaRepository.findAll();
            assertThat(submissions).hasSize(1);
            Submission testSubmission = submissions.get(submissions.size() - 1);
            assertThat(testSubmission.getValue()).isEqualTo(SUBMISSION_VALUE);
            assertThat(testSubmission.getSubmissionFeedbacks()).hasSize(1);
            SubmissionFeedback submissionFeedback = testSubmission.getSubmissionFeedbacks().stream().findFirst().orElseThrow();
            assertThat(submissionFeedback.getGrade()).isEqualTo(FAILING_GRADE);
            assertThat(submissionFeedback.getSelectedCriterionLevels()).hasSize(2);

            List<Notification> notifications = notificationRepository.findAll();
            assertThat(notifications).hasSize(1);
            assertThat(notifications.get(0).getMessage()).contains("{HAS_REJECTED}");
        });
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldNotAllowToPostWhenUserNotATeacherInJourney() throws Exception {
        User teacher = createTeacher().setLogin("journeyCreator");
        transactionHelper.withTransaction(() -> {
            persistCreatedEntities(entityManager);
            Assignment assignment = assignmentWithReferences();
            assignment
                .creator(teacher)
                .getMilestone()
                .creator(teacher)
                .getJourney()
                .creator(teacher)
                .teachers(new HashSet<>())
                .addTeacher(teacher);
            student = getTeacher(entityManager);
            journey.addStudent(student);
            persistCreatedEntities(entityManager);
            assignment.calculateStates();
            persistCreatedEntities(entityManager);
            submission = createSubmission()
                .assignment(assignment)
                .addAuthor(student);
        });

        SubmissionGradingPatchDTO submissionGradingPatchDTO = getSubmissionGradingPatchDTO(student.getId(), null, submission, FAILING_GRADE)
            .resubmittable(SUBMISSION_RESUBMITTABLE);

        mockMvc.perform(post("/api/submission-feedback", submission.getId())
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(submissionGradingPatchDTO))
            )
            .andExpect(status().isForbidden())
            .andExpect(jsonPath("$.detail").value("403 FORBIDDEN \"Current user is not a teacher in the journey!\""));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldAllowTeacherToUpdateSubmissionGrade() throws Exception {
        transactionHelper.withTransaction(() -> {
            student = createUser();
            Assignment assignment = assignmentWithReferences();
            submission = createSubmission()
                .assignment(assignment)
                .addAuthor(student);
            journey.addStudent(student);
            persistCreatedEntities(entityManager);
            assignment.calculateStates();
            persistCreatedEntities(entityManager);
        });

        SubmissionGradingPatchDTO submissionPatchDTO = getSubmissionGradingPatchDTO(student.getId(), null, submission, PATCH_GRADE)
            .resubmittable(PATCH_RESUBMITTABLE)
            .submittedForGradingDate(submission.getSubmittedForGradingDate());


        mockMvc.perform(patch("/api/submission-feedback?submissionId={id}", submission.getId())
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(submissionPatchDTO))
            )
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.grade").value(PATCH_GRADE))
            .andExpect(jsonPath("$.feedbackDate").value(PATCH_FEEDBACK_DATE.toString()))
            .andExpect(jsonPath("$.submissionId").value(submission.getId()))
            .andExpect(jsonPath("$.studentId").value(student.getId()));

        List<Notification> notifications = notificationRepository.findAll();
        assertThat(notifications).hasSize(1);
        assertThat(notifications.get(0).getMessage()).contains("{GIVEN_FEEDBACK}");

        List<Submission> submissions = submissionJpaRepository.findAll();
        assertThat(submissions.size()).isEqualTo(1);
        Submission savedSubmission = submissions.stream().findFirst().get();
        assertThat(savedSubmission.getValue()).isEqualTo(SUBMISSION_VALUE);
        assertThat(savedSubmission.isResubmittable()).isEqualTo(PATCH_RESUBMITTABLE);
        assertThat(savedSubmission.getSubmittedForGradingDate()).isEqualTo(SUBMISSION_SUBMITTED_FOR_GRADING_DATE.toString());
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldNotAllowToPatchWhenUserNotATeacherInJourney() throws Exception {
        User teacher = createTeacher().setLogin("journeyCreator");
        transactionHelper.withTransaction(() -> {
            persistCreatedEntities(entityManager);
            student = getTeacher(entityManager);
            Assignment assignment = assignmentWithReferences();
            assignment
                .creator(teacher)
                .getMilestone()
                .creator(teacher)
                .getJourney()
                .creator(teacher)
                .teachers(new HashSet<>())
                .addTeacher(teacher);
            submission = createSubmission()
                .assignment(assignment)
                .addAuthor(student);
            journey.addStudent(student);
            persistCreatedEntities(entityManager);
            assignment.calculateStates();
            persistCreatedEntities(entityManager);
        });

        SubmissionGradingPatchDTO submissionPatchDTO = getSubmissionGradingPatchDTO(student.getId(), null, submission, PATCH_GRADE)
            .resubmittable(PATCH_RESUBMITTABLE)
            .submittedForGradingDate(submission.getSubmittedForGradingDate());

        mockMvc.perform(patch("/api/submission-feedback?submissionId={id}", submission.getId())
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(submissionPatchDTO))
            )
            .andExpect(status().isForbidden())
            .andExpect(jsonPath("$.detail").value("403 FORBIDDEN \"Current user is not a teacher in the journey!\""));
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void shouldSaveUpperCaseGradeOnPatch() throws Exception {
        Assignment assignment = assignmentWithReferences();
        assignment.setGradingScheme(getGradingSchemeAlphabetical(entityManager));
        User student = createUser();
        journey.addStudent(student);
        submission = createSubmission()
            .assignment(assignment)
            .addAuthor(student);
        persistCreatedEntities(entityManager);

        SubmissionGradingPatchDTO submissionPatchDTO = getSubmissionGradingPatchDTO(student.getId(), null, submission, DEFAULT_GRADE_LOWER_CASE)
            .resubmittable(PATCH_RESUBMITTABLE)
            .submittedForGradingDate(submission.getSubmittedForGradingDate());

        mockMvc.perform(patch("/api/submission-feedback?submissionId={id}", submission.getId())
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(submissionPatchDTO)))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.grade").value(DEFAULT_GRADE_UPPER_CASE))
            .andExpect(jsonPath("$.feedbackDate").value(PATCH_FEEDBACK_DATE.toString()))
            .andExpect(jsonPath("$.submissionId").value(submission.getId()))
            .andExpect(jsonPath("$.studentId").value(student.getId()));

        List<Submission> submissions = submissionJpaRepository.findAll();
        assertThat(submissions.size()).isEqualTo(1);
        Submission savedSubmission = submissions.stream().findFirst().get();
        assertThat(savedSubmission.getValue()).isEqualTo(SUBMISSION_VALUE);
        assertThat(savedSubmission.isResubmittable()).isEqualTo(PATCH_RESUBMITTABLE);
        assertThat(savedSubmission.getSubmittedForGradingDate()).isEqualTo(SUBMISSION_SUBMITTED_FOR_GRADING_DATE.toString());
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldSaveUpperCaseGradeOnPost() throws Exception {
        final SubmissionGradingPatchDTO[] submissionGradingPatchDTO = new SubmissionGradingPatchDTO[1];
        transactionHelper.withNewTransaction(() -> {
            User student = getStudent(entityManager);
            Assignment assignment = assignmentWithReferences();
            assignment.setGradingScheme(getGradingSchemeAlphabetical(entityManager));
            persistCreatedEntities(entityManager);

            submission = createSubmission()
                .assignment(assignment)
                .addAuthor(student);

            assignment.calculateStates();
            persistCreatedEntities(entityManager);

            SubmissionDTO submissionDTO = submissionMapper.toDto(submission);

            submissionGradingPatchDTO[0] = new SubmissionGradingPatchDTO()
                .resubmittable(PATCH_RESUBMITTABLE)
                .submittedForGradingDate(submission.getSubmittedForGradingDate())
                .submissionFeedback(new SubmissionFeedbackDTO()
                    .grade(DEFAULT_GRADE_LOWER_CASE)
                    .feedbackDate(now())
                    .studentId(student.getId())
                    .submission(submissionDTO)
                );
        });

        mockMvc.perform(post("/api/submission-feedback")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(submissionGradingPatchDTO[0])))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.grade").value(DEFAULT_GRADE_UPPER_CASE));
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void shouldThrowOnUpdateSubmissionGradeWhenSubmissionNotPresent() throws Exception {
        User student = getStudent(entityManager);
        SubmissionPatchDTO submissionPatchDTO = getSubmissionGradingPatchDTO(student.getId(), null, createSubmission(), PATCH_GRADE);

        mockMvc.perform(patch("/api/submission-feedback?submissionId={id}", 1)
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(submissionPatchDTO))
            )
            .andExpect(status().isNotFound());

        List<Notification> notifications = notificationRepository.findAll();
        assertThat(notifications).isEmpty();
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void shouldThrowOnUpdateSubmissionGradeWhenTeacherNotInJourney() throws Exception {
        User student = createUser();
        submission = createSubmission()
            .assignment(assignmentWithReferences())
            .addAuthor(student);
        submission
            .getAssignment()
            .getMilestone()
            .getJourney()
            .teachers(Collections.emptySet())
            .addStudent(student);
        persistCreatedEntities(entityManager);

        SubmissionPatchDTO submissionPatchDTO = getSubmissionGradingPatchDTO(student.getId(), null, submission, PATCH_GRADE);

        mockMvc.perform(patch("/api/submission-feedback?submissionId={id}", submission.getId())
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(submissionPatchDTO))
            )
            .andExpect(status().isNotFound());

        List<Notification> notifications = notificationRepository.findAll();
        assertThat(notifications).isEmpty();
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldCreateFeedbackNotificationForAllGroupMembers() throws Exception {
        transactionHelper.withNewTransaction(() -> {
            User student = createStudent();
            group = createGroup().setStudents(Set.of(getStudent(entityManager), student));
            Assignment assignment = assignmentWithReferences().setGroups(Set.of(group));
            journey.addStudent(student);
            submission = createSubmission()
                .assignment(assignment)
                .group(group)
                .submittedForGradingDate(PATCH_SUBMITTED_FOR_GRADING_DATE)
                .authors(group.getStudents());
            persistCreatedEntities(entityManager);
            assignment.calculateStates();
            persistCreatedEntities(entityManager);
        });

        SubmissionGradingPatchDTO submissionGradingPatchDTO = getSubmissionGradingPatchDTO(null, group.getId(), submission, PATCH_GRADE)
            .resubmittable(PATCH_RESUBMITTABLE)
            .submittedForGradingDate(PATCH_SUBMITTED_FOR_GRADING_DATE);

        mockMvc.perform(patch("/api/submission-feedback?submissionId={id}", submission.getId())
            .contentType(APPLICATION_JSON)
            .content(convertObjectToJsonBytes(submissionGradingPatchDTO))
        ).andExpect(status().isOk());

        transactionHelper.withTransaction(() -> {
                List<Notification> notifications = notificationRepository.findAll();
                assertThat(notifications).hasSize(2);
                notifications.forEach(notification -> assertThat(notification.getMessage()).contains("{GIVEN_FEEDBACK}"));
            }
        );
    }

    @Test
    @WithMockCustomUser(authorities = {STUDENT})
    void shouldThrowForbiddenWhenGradingSubmissionAsStudent() throws Exception {
        transactionHelper.withTransaction(() -> {
            student = getStudent(entityManager);
            submission = createSubmission()
                .assignment(assignmentWithReferences())
                .addAuthor(student);
            persistCreatedEntities(entityManager);
        });

        SubmissionGradingPatchDTO submissionPatchDTO = getSubmissionGradingPatchDTO(student.getId(), null, submission, PATCH_GRADE)
            .resubmittable(PATCH_RESUBMITTABLE);

        mockMvc.perform(patch("/api/submission-feedback?submissionId={id}", submission.getId())
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(submissionPatchDTO)))
            .andExpect(status().isForbidden());

        mockMvc.perform(post("/api/submission-feedback")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(submissionPatchDTO)))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldThrowConflictWhenGradingSubmissionThatHasChanged() throws Exception {
        transactionHelper.withTransaction(() -> {
            student = getStudent(entityManager);
            submission = createSubmission()
                .assignment(assignmentWithReferences())
                .addAuthor(student);
            persistCreatedEntities(entityManager);
        });

        SubmissionPatchDTO submissionPatchDTO = getSubmissionGradingPatchDTO(student.getId(), null, submission, PATCH_GRADE)
            .resubmittable(PATCH_RESUBMITTABLE)
            .submittedForGradingDate(null);

        mockMvc.perform(patch("/api/submission-feedback?submissionId={id}", submission.getId())
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(submissionPatchDTO))
            )
            .andExpect(status().isBadRequest());
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldThrowConflictWhenGradingWithOutdatedSubmittedForGradingDate() throws Exception {
        transactionHelper.withTransaction(() -> {
            student = getStudent(entityManager);
            submission = createSubmission()
                .assignment(assignmentWithReferences())
                .addAuthor(student);
            persistCreatedEntities(entityManager);
        });

        SubmissionPatchDTO submissionPatchDTO = getSubmissionGradingPatchDTO(student.getId(), null, submission, PATCH_GRADE)
            .resubmittable(PATCH_RESUBMITTABLE)
            .submittedForGradingDate(submission.getSubmittedForGradingDate().minus(1, MILLIS));

        mockMvc.perform(patch("/api/submission-feedback?submissionId={id}", submission.getId())
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(submissionPatchDTO)))
            .andExpect(status().isBadRequest());
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldAllowTeacherToCreateSubmissionFeedbackWithSelectedCriterionLevels() throws Exception {
        final SubmissionGradingPatchDTO[] submissionGradingPatchDTO = new SubmissionGradingPatchDTO[1];
        final CriterionLevel[] criterionLevel1 = new CriterionLevel[1];
        final CriterionLevel[] criterionLevel2 = new CriterionLevel[1];
        transactionHelper.withTransaction(() -> {
            student = createUser();
            Assignment assignment = assignmentWithReferences();
            submission = createSubmission()
                .assignment(assignment)
                .addAuthor(student);
            journey.addStudent(student);

            Rubric rubric = createRubric();
            Criterion criterion = createCriterion().setRubric(rubric);
            criterionLevel2[0] = createCriterionLevel().setCriterion(criterion);
            criterionLevel1[0] = createCriterionLevel().setCriterion(criterion);
            persistCreatedEntities(entityManager);
            assignment.calculateStates();
            persistCreatedEntities(entityManager);
        });

        Set<SelectedCriterionLevelDTO> selectedCriterionLevelDTOs = Set.of(
            getSelectedCriterionLevelDTO().setCriterionLevel(criterionLevelMapper.toDto(criterionLevel1[0])),
            getSelectedCriterionLevelDTO().setCriterionLevel(criterionLevelMapper.toDto(criterionLevel2[0])));

        submissionGradingPatchDTO[0] = getSubmissionGradingPatchDTO(student.getId(), null, submission, PATCH_GRADE, selectedCriterionLevelDTOs)
            .submittedForGradingDate(submission.getSubmittedForGradingDate())
            .resubmittable(PATCH_RESUBMITTABLE);

        mockMvc.perform(patch("/api/submission-feedback?submissionId={id}", submission.getId()).contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(submissionGradingPatchDTO[0])))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.selectedCriterionLevels", hasSize(2)));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldThrowWhenGradingStudentThatIsNotAuthorOfTheSubmission() throws Exception {
        transactionHelper.withTransaction(() -> {
            User submissionAuthor = getStudent(entityManager);
            student = createUser();

            submission = createSubmission()
                .assignment(assignmentWithReferences())
                .addAuthor(submissionAuthor);

            persistCreatedEntities(entityManager);
        });

        SubmissionGradingPatchDTO submissionGradingPatchDTO = getSubmissionGradingPatchDTO(student.getId(), null, submission, SUBMISSION_GRADE);

        mockMvc.perform(patch("/api/submission-feedback?submissionId={id}", submission.getId()).contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(submissionGradingPatchDTO)))
            .andExpect(status().isBadRequest());
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldThrowWhenGradingGroupThatIsNotAuthorOfTheSubmission() throws Exception {
        transactionHelper.withTransaction(() -> {
            Group submissionAuthor = createGroup();
            group = createGroup();

            submission = createSubmission()
                .assignment(assignmentWithReferences())
                .group(submissionAuthor);

            persistCreatedEntities(entityManager);
        });

        SubmissionGradingPatchDTO submissionGradingPatchDTO = getSubmissionGradingPatchDTO(null, group.getId(), submission, SUBMISSION_GRADE);

        mockMvc.perform(patch("/api/submission-feedback?submissionId={id}", submission.getId()).contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(submissionGradingPatchDTO)))
            .andExpect(status().isBadRequest());
    }

    private SelectedCriterionLevelDTO getSelectedCriterionLevelDTO() {
        return new SelectedCriterionLevelDTO()
            .setModifiedDescription(SELECTED_CRITERION_LEVEL_DESCRIPTION);
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldCalculateStudentsAverageGradeWhenFeedbackGiven() throws Exception {
        transactionHelper.withTransaction(() -> {
            student = createUser();
            Assignment assignment = assignmentWithReferences();
            submission = createSubmission()
                .assignment(assignment)
                .addAuthor(student);
            journey.addStudent(student);
            persistCreatedEntities(entityManager);
            assignment.calculateStates();
            persistCreatedEntities(entityManager);
        });

        SubmissionGradingPatchDTO submissionPatchDTO = getSubmissionGradingPatchDTO(student.getId(), null, submission, PATCH_GRADE)
            .resubmittable(PATCH_RESUBMITTABLE)
            .submittedForGradingDate(submission.getSubmittedForGradingDate());


        mockMvc.perform(patch("/api/submission-feedback?submissionId={id}", submission.getId())
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(submissionPatchDTO))
            )
            .andExpect(status().isOk());

        transactionHelper.withTransaction(() -> {
            JourneyStudent journeyStudent = journeyStudentRepository.findByJourneyIdAndUserId(journey.getId(), student.getId());
            JourneyStudent journeyStudent2 = journeyStudentRepository.findByJourneyIdAndUserId(journey.getId(), getStudent(entityManager).getId());
            assertThat(journeyStudent.getAverageGrade()).isEqualTo(getGradingSchemeNumerical(entityManager).getPercentage(PATCH_GRADE));
            assertThat(journeyStudent2.getAverageGrade()).isNull();
        });
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldCalculateStudentsAverageGradeByGroupGradeWhenIndividualGradeIsNotGiven() throws Exception {
        transactionHelper.withNewTransaction(() -> {
            student = createStudent();
            group = createGroup().setStudents(Set.of(getStudent(entityManager), student));
            Assignment assignment = assignmentWithReferences().setGroups(Set.of(group));
            journey.addStudent(student);
            submission = createSubmission()
                .assignment(assignment)
                .group(group)
                .submittedForGradingDate(PATCH_SUBMITTED_FOR_GRADING_DATE)
                .authors(group.getStudents());
            persistCreatedEntities(entityManager);
            assignment.calculateStates();
            persistCreatedEntities(entityManager);
        });

        SubmissionGradingPatchDTO submissionGradingPatchDTO = getSubmissionGradingPatchDTO(null, group.getId(), submission, PATCH_GRADE)
            .resubmittable(PATCH_RESUBMITTABLE)
            .submittedForGradingDate(PATCH_SUBMITTED_FOR_GRADING_DATE);

        mockMvc.perform(patch("/api/submission-feedback?submissionId={id}", submission.getId())
            .contentType(APPLICATION_JSON)
            .content(convertObjectToJsonBytes(submissionGradingPatchDTO))
        ).andExpect(status().isOk());

        transactionHelper.withTransaction(() -> {
            JourneyStudent journeyStudent = journeyStudentRepository.findByJourneyIdAndUserId(journey.getId(), student.getId());
            assertThat(journeyStudent.getAverageGrade()).isEqualTo(getGradingSchemeNumerical(entityManager).getPercentage(PATCH_GRADE));
        });
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldCalculateStudentsAverageGradeByIndividualGradeWhenIndividualGradeIsGiven() throws Exception {
        transactionHelper.withNewTransaction(() -> {
            student = createStudent();
            group = createGroup().setStudents(Set.of(getStudent(entityManager), student));
            Assignment assignment = assignmentWithReferences().setGroups(Set.of(group));
            journey.addStudent(student);
            submission = createSubmission()
                .assignment(assignment)
                .group(group)
                .submittedForGradingDate(PATCH_SUBMITTED_FOR_GRADING_DATE)
                .authors(group.getStudents());
            persistCreatedEntities(entityManager);
            assignment.calculateStates();
            persistCreatedEntities(entityManager);
        });

        SubmissionGradingPatchDTO submissionGroupGradingPatchDTO = getSubmissionGradingPatchDTO(null, group.getId(), submission, PATCH_GRADE)
            .resubmittable(PATCH_RESUBMITTABLE)
            .submittedForGradingDate(PATCH_SUBMITTED_FOR_GRADING_DATE);

        mockMvc.perform(patch("/api/submission-feedback?submissionId={id}", submission.getId())
            .contentType(APPLICATION_JSON)
            .content(convertObjectToJsonBytes(submissionGroupGradingPatchDTO))
        ).andExpect(status().isOk());

        SubmissionGradingPatchDTO submissionGradingPatchDTO = getSubmissionGradingPatchDTO(student.getId(), null, submission, "5")
            .resubmittable(PATCH_RESUBMITTABLE)
            .submittedForGradingDate(PATCH_SUBMITTED_FOR_GRADING_DATE);

        mockMvc.perform(patch("/api/submission-feedback?submissionId={id}", submission.getId())
            .contentType(APPLICATION_JSON)
            .content(convertObjectToJsonBytes(submissionGradingPatchDTO))
        ).andExpect(status().isOk());

        transactionHelper.withTransaction(() -> {
            JourneyStudent journeyStudent = journeyStudentRepository.findByJourneyIdAndUserId(journey.getId(), student.getId());
            assertThat(journeyStudent.getAverageGrade()).isEqualTo(getGradingSchemeNumerical(entityManager).getPercentage("5"));
        });
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldGradeAnExistingSubmissionWhenPostingFeedbackForStudentWhoAlreadyHasSubmissionInGivenAssignment() throws Exception {
        transactionHelper.withNewTransaction(() -> {
            student = createStudent();
            assignment = assignmentWithReferences();
            journey.addStudent(student);
            submission = createSubmission()
                .submittedForGradingDate(null)
                .assignment(assignment)
                .authors(Set.of(student));
            persistCreatedEntities(entityManager);
            assignment.calculateStates();
            persistCreatedEntities(entityManager);
        });

        SubmissionGradingPatchDTO submissionGradingPatchDTO = getSubmissionGradingPatchDTO(student.getId(), null, submission.id(null), PATCH_GRADE)
            .resubmittable(PATCH_RESUBMITTABLE)
            .submittedForGradingDate(PATCH_SUBMITTED_FOR_GRADING_DATE);

        mockMvc.perform(post("/api/submission-feedback")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(submissionGradingPatchDTO)))
            .andExpect(status().isCreated());

        transactionHelper.withTransaction(() -> {
            assertThat(submissionJpaRepository.findAll().size()).isEqualTo(1);
        });
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldGradeAnExistingSubmissionWhenPostingFeedbackForGroupWhichAlreadyHasSubmissionInGivenAssignment() throws Exception {
        transactionHelper.withNewTransaction(() -> {
            student = createStudent();
            group = createGroup().setStudents(Set.of(getStudent(entityManager), student));
            assignment = assignmentWithReferences().setGroups(Set.of(group));
            journey.addStudent(student);
            submission = createSubmission()
                .assignment(assignment)
                .group(group)
                .submittedForGradingDate(PATCH_SUBMITTED_FOR_GRADING_DATE)
                .authors(group.getStudents());
            persistCreatedEntities(entityManager);
            assignment.calculateStates();
            persistCreatedEntities(entityManager);
        });

        SubmissionGradingPatchDTO submissionGradingPatchDTO = getSubmissionGradingPatchDTO(null, group.getId(), submission.id(null), PATCH_GRADE)
            .resubmittable(PATCH_RESUBMITTABLE)
            .submittedForGradingDate(PATCH_SUBMITTED_FOR_GRADING_DATE);

        mockMvc.perform(post("/api/submission-feedback")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(submissionGradingPatchDTO)))
            .andExpect(status().isCreated());

        transactionHelper.withTransaction(() -> {
            assertThat(submissionJpaRepository.findAll().size()).isEqualTo(1);
        });
    }


    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldGetSubmissionFeedbacks() throws Exception {
        transactionHelper.withNewTransaction(() -> {
            student = createStudent();
            assignment = assignmentWithReferences();
            journey.addStudent(student);
            submission = createSubmission()
                .assignment(assignment)
                .submittedForGradingDate(PATCH_SUBMITTED_FOR_GRADING_DATE)
                .authors(Set.of(student));
            submissionFeedback = createSubmissionFeedback()
                .submission(submission)
                .student(student)
                .creator(getTeacher(entityManager));
            submission.addSubmissionFeedback(submissionFeedback);
            persistCreatedEntities(entityManager);
            assignment.calculateStates();
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(get("/api/submission-feedback")
                .param("studentId", student.getId().toString())
                .param("submissionId", submission.getId().toString())
            )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.[0].id").value(submissionFeedback.getId()));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldThrowWhenGradeInvalid() throws Exception {
        transactionHelper.withNewTransaction(() -> {
            student = createStudent();
            group = createGroup().setStudents(Set.of(getStudent(entityManager), student));
            assignment = assignmentWithReferences().setGroups(Set.of(group));
            journey.addStudent(student);
            submission = createSubmission()
                .assignment(assignment)
                .group(group)
                .submittedForGradingDate(PATCH_SUBMITTED_FOR_GRADING_DATE)
                .authors(group.getStudents());
            persistCreatedEntities(entityManager);
            assignment.calculateStates();
            persistCreatedEntities(entityManager);
        });

        SubmissionGradingPatchDTO submissionGradingPatchDTO = getSubmissionGradingPatchDTO(null, group.getId(), submission.id(null), "6")
            .resubmittable(PATCH_RESUBMITTABLE)
            .submittedForGradingDate(PATCH_SUBMITTED_FOR_GRADING_DATE);

        mockMvc.perform(post("/api/submission-feedback")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(submissionGradingPatchDTO)))
            .andExpect(status().isBadRequest());
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldAddCorrectCreatorToSubmissionFeedback() throws Exception {
        transactionHelper.withNewTransaction(() -> {
            student = createStudent();
            User journeyCreator = createTeacher().setLogin("journeyCreator").setEmail("journeyCreator@example.com");
            assignment = assignmentWithReferences();
            journey.addStudent(student)
                .addTeacher(journeyCreator)
                .creator(journeyCreator);
            submission = createSubmission()
                .assignment(assignment)
                .submittedForGradingDate(PATCH_SUBMITTED_FOR_GRADING_DATE)
                .addAuthor(student);
            persistCreatedEntities(entityManager);
            assignment.calculateStates();
            persistCreatedEntities(entityManager);
        });

        SubmissionGradingPatchDTO submissionGradingPatchDTO = getSubmissionGradingPatchDTO(student.getId(), null, submission.id(null), "5")
            .resubmittable(PATCH_RESUBMITTABLE)
            .submittedForGradingDate(PATCH_SUBMITTED_FOR_GRADING_DATE);

        mockMvc.perform(post("/api/submission-feedback")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(submissionGradingPatchDTO)))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.creatorId").value(TEACHER_ID));
    }
}
