package com.schoolaby.web.rest;

import com.schoolaby.common.BaseIntegrationTest;
import org.junit.jupiter.api.Test;

import javax.transaction.Transactional;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

class SubjectControllerIT extends BaseIntegrationTest {

    public static final String API_SUBJECTS = "/api/subjects";

    @Test
    @Transactional
    void shouldGetSubjectsByCountryDefault() throws Exception {
        mockMvc.perform(get(API_SUBJECTS))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("length()").value(32))
            .andExpect(jsonPath("$.[0].id").value(1))
            .andExpect(jsonPath("$.[0].label").value("schoolabyApp.subject.estonian"));
    }

    @Test
    @Transactional
    void shouldGetSubjectsByCountryTanzania() throws Exception {
        mockMvc.perform(get(API_SUBJECTS).param("country", "Tanzania"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("length()").value(20))
            .andExpect(jsonPath("$.[0].id").value(33))
            .andExpect(jsonPath("$.[0].label").value("schoolabyApp.subject.kiswahili"));
    }

    @Test
    @Transactional
    void shouldGetSubjectsByCountryDefaultWhenUnsupported() throws Exception {
        mockMvc.perform(get(API_SUBJECTS).param("country", "Finland"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("length()").value(32))
            .andExpect(jsonPath("$.[0].id").value(1))
            .andExpect(jsonPath("$.[0].label").value("schoolabyApp.subject.estonian"));
    }
}
