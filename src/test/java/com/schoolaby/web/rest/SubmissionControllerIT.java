package com.schoolaby.web.rest;

import com.schoolaby.common.BaseIntegrationTest;
import com.schoolaby.domain.*;
import com.schoolaby.repository.NotificationJpaRepository;
import com.schoolaby.repository.SubmissionFeedbackJpaRepository;
import com.schoolaby.repository.SubmissionJpaRepository;
import com.schoolaby.service.dto.UploadedFileDTO;
import com.schoolaby.service.dto.UserDTO;
import com.schoolaby.service.dto.submission.SubmissionDTO;
import com.schoolaby.service.dto.submission.SubmissionFeedbackDTO;
import com.schoolaby.service.dto.submission.SubmissionPatchDTO;
import com.schoolaby.service.dto.submission.SubmissionResubmitPatchDTO;
import com.schoolaby.service.mapper.SubmissionMapper;
import com.schoolaby.service.mapper.UserMapper;
import com.schoolaby.web.WithMockCustomUser;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.*;

import static com.schoolaby.common.TestCases.*;
import static com.schoolaby.common.TestObjects.*;
import static com.schoolaby.security.Role.Constants.*;
import static com.schoolaby.web.rest.TestUtil.convertObjectToJsonBytes;
import static java.lang.String.format;
import static java.time.Instant.now;
import static java.time.temporal.ChronoUnit.DAYS;
import static java.util.Collections.emptySet;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

class SubmissionControllerIT extends BaseIntegrationTest {
    private static final String DEFAULT_VALUE = "AAAAAAAAAA";
    private static final String UPDATED_VALUE = "BBBBBBBBBB";

    private static final Instant PATCH_FEEDBACK_DATE = now().plus(1, DAYS);
    private static final Instant PATCH_SUBMITTED_FOR_GRADING_DATE = now().plus(1, DAYS);
    private static final Set<UploadedFileDTO> PATCH_UPLOADED_FILES = Set.of(new UploadedFileDTO(), new UploadedFileDTO());
    private static final String PATCH_VALUE = "Updated value";
    private static final boolean PATCH_RESUBMITTABLE = !SUBMISSION_RESUBMITTABLE;

    @Autowired
    private SubmissionJpaRepository submissionJpaRepository;
    @Autowired
    private SubmissionMapper submissionMapper;
    @Autowired
    private NotificationJpaRepository notificationRepository;

    @Autowired
    private SubmissionFeedbackJpaRepository submissionFeedbackJpaRepository;

    @Autowired
    private UserMapper userMapper;

    private Submission submission;

    private User student;

    private Assignment assignment;

    private User otherTeacher;

    static Submission createEntity() {
        return new Submission()
            .value(DEFAULT_VALUE);
    }

    private Assignment assignmentWithReferences() {
        User teacher = getTeacher(entityManager);
        EducationalLevel educationalLevel = createEducationalLevel();
        Journey journey = createJourney();
        Milestone milestone = createMilestone();
        Assignment assignment = createAssignment();
        journey.creator(teacher)
            .addTeacher(teacher)
            .educationalLevel(educationalLevel)
            .addMilestone(milestone
                .creator(teacher)
                .addAssignment(assignment
                    .creator(teacher)
                    .gradingScheme(getGradingSchemeNumerical(entityManager))));
        return assignment;
    }

    private SubmissionDTO createSubmissionDTO(Long groupId) {
        SubmissionDTO submissionDTO = new SubmissionDTO();
        submissionDTO.setValue("test value");
        submissionDTO.setGroupId(groupId);
        submissionDTO.setAssignmentId(assignment.getId());
        submissionDTO.setSubmittedForGradingDate(PATCH_SUBMITTED_FOR_GRADING_DATE);
        return submissionDTO;
    }

    @BeforeEach
    void beforeEach() {
        otherTeacher = createTeacher()
            .setLogin("otherTeacher")
            .setEmail("otherTeacher@example.com");
    }

    @Test
    @Transactional
    void shouldThrowBadRequestWhenValueIsNull() throws Exception {
        submission = createEntity();
        Assignment assignment = createAssignment();
        assignment.setGradingScheme(getGradingSchemeNumerical(entityManager));
        submission.assignment(assignment);
        int databaseSizeBeforeTest = submissionJpaRepository.findAll().size();
        submission.setValue(null);

        SubmissionDTO submissionDTO = submissionMapper.toDto(submission);

        mockMvc.perform(post("/api/submissions")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(submissionDTO)))
            .andExpect(status().isBadRequest());

        List<Submission> submissions = submissionJpaRepository.findAll();
        assertThat(submissions).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldGetAllSubmissions() throws Exception {
        transactionHelper.withTransaction(() -> {
            Assignment assignment = assignmentWithReferences();
            submission = createSubmission();
            assignment.addSubmission(submission);
            createSubmissionFeedback()
                .submission(submission)
                .grade(SUBMISSION_GRADE)
                .student(getStudent(entityManager))
                .creator(getTeacher(entityManager));
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(get("/api/submissions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[0].id").value(submission.getId().intValue()))
            .andExpect(jsonPath("$.[0].value").value(SUBMISSION_VALUE))
            .andExpect(jsonPath("$.[0].submissionFeedbacks.[0].grade").value(SUBMISSION_GRADE))
            .andExpect(jsonPath("$.[0].submissionFeedbacks.[0].feedbackDate").value(SUBMISSION_FEEDBACK_DATE.toString()))
            .andExpect(jsonPath("$.[0].resubmittable").value(SUBMISSION_RESUBMITTABLE))
            .andExpect(jsonPath("$.[0].submittedForGradingDate").value(SUBMISSION_SUBMITTED_FOR_GRADING_DATE.toString()));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldGetAllSubmissionsByJourneyId() throws Exception {
        final Assignment[] assignment1 = new Assignment[1];
        transactionHelper.withTransaction(() -> {
            User student = getStudent(entityManager);
            assignment1[0] = assignmentWithReferences();
            Assignment assignment2 = assignmentWithReferences();
            assignment2.getMilestone().getJourney().setJourneySignupCodes(emptySet());
            submission = createSubmission().addAuthor(student);
            assignment2.addSubmission(createSubmission());
            assignment1[0].addSubmission(submission);

            createSubmissionFeedback()
                .submission(submission)
                .grade(SUBMISSION_GRADE)
                .student(student)
                .creator(getTeacher(entityManager));

            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(get("/api/submissions?journeyId={id}&sort=id,desc", assignment1[0].getMilestone().getJourney().getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.size()").value(1))
            .andExpect(jsonPath("$.[0].id").value(submission.getId().intValue()))
            .andExpect(jsonPath("$.[0].value").value(SUBMISSION_VALUE))
            .andExpect(jsonPath("$.[0].submissionFeedbacks.[0].grade").value(SUBMISSION_GRADE))
            .andExpect(jsonPath("$.[0].resubmittable").value(SUBMISSION_RESUBMITTABLE))
            .andExpect(jsonPath("$.[0].submissionFeedbacks.[0].feedbackDate").value(SUBMISSION_FEEDBACK_DATE.toString()))
            .andExpect(jsonPath("$.[0].submittedForGradingDate").value(SUBMISSION_SUBMITTED_FOR_GRADING_DATE.toString()));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldGetSubmission() throws Exception {
        transactionHelper.withTransaction(() -> {
            User student = getStudent(entityManager);
            User teacher = getTeacher(entityManager);
            Assignment assignment = assignmentWithReferences();
            submission = createSubmission().addAuthor(student);
            assignment.addSubmission(submission);

            createSubmissionFeedback()
                .submission(submission)
                .grade(SUBMISSION_GRADE)
                .student(student)
                .creator(teacher);

            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(get("/api/submissions/{id}", submission.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(submission.getId().intValue()))
            .andExpect(jsonPath("$.value").value(SUBMISSION_VALUE))
            .andExpect(jsonPath("$.submissionFeedbacks.[0].grade").value(SUBMISSION_GRADE))
            .andExpect(jsonPath("$.resubmittable").value(SUBMISSION_RESUBMITTABLE))
            .andExpect(jsonPath("$.submissionFeedbacks.[0].feedbackDate").value(SUBMISSION_FEEDBACK_DATE.toString()))
            .andExpect(jsonPath("$.submittedForGradingDate").value(SUBMISSION_SUBMITTED_FOR_GRADING_DATE.toString()));
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void shouldThrowNotFoundWhenFetchingNonExistingSubmission() throws Exception {
        mockMvc.perform(get("/api/submissions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void shouldThrowBadRequestWhenUpdatingNonExistingSubmission() throws Exception {
        Assignment assignment = createAssignment();
        assignment.setGradingScheme(getGradingSchemeNumerical(entityManager));
        submission = createEntity();
        submission.assignment(assignment);
        int databaseSizeBeforeUpdate = submissionJpaRepository.findAll().size();
        SubmissionDTO submissionDTO = submissionMapper.toDto(submission);

        mockMvc.perform(put("/api/submissions")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(submissionDTO)))
            .andExpect(status().isBadRequest());

        List<Submission> submissions = submissionJpaRepository.findAll();
        assertThat(submissions).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    @WithUserDetails(ADMIN_LOGIN)
    void shouldDeleteSubmission() throws Exception {
        Assignment assignment = assignmentWithReferences();
        submission = createSubmission();
        assignment.addSubmission(submission);
        persistCreatedEntities(entityManager);

        int databaseSizeBeforeDelete = submissionJpaRepository.findAll().size();

        mockMvc.perform(delete("/api/submissions/{id}", submission.getId())
                .accept(APPLICATION_JSON))
            .andExpect(status().isNoContent());

        List<Submission> submissions = submissionJpaRepository.findAll();
        assertThat(submissions).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    @WithMockCustomUser(authorities = {ANONYMOUS})
    void shouldForbidAnonymousFromCreatingSubmission() throws Exception {
        Assignment assignment = assignmentWithReferences();
        persistCreatedEntities(entityManager);
        submission = createSubmission()
            .assignment(assignment)
            .addAuthor(getStudent(entityManager));
        SubmissionDTO submissionDTO = submissionMapper.toDto(submission);

        mockMvc.perform(post("/api/submissions")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(submissionDTO)))
            .andExpect(status().isForbidden());
    }

    @Test
    @Transactional
    @WithMockCustomUser(authorities = {STUDENT})
    void shouldThrowWhenPostingSubmissionWithFeedbacks() throws Exception {
        Assignment assignment = assignmentWithReferences();
        persistCreatedEntities(entityManager);
        submission = createSubmission()
            .assignment(assignment);
        SubmissionDTO submissionDTO = submissionMapper.toDto(submission).setSubmissionFeedbacks(Set.of(new SubmissionFeedbackDTO()));

        mockMvc.perform(post("/api/submissions")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(submissionDTO)))
            .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    @WithMockCustomUser(authorities = {STUDENT})
    void shouldThrowBadRequestWhenCreatingSubmissionWithExistingId() throws Exception {
        Assignment assignment = createAssignment();
        assignment.setGradingScheme(getGradingSchemeNumerical(entityManager));
        submission = createEntity();
        submission.assignment(assignment);
        int databaseSizeBeforeCreate = submissionJpaRepository.findAll().size();
        submission.setId(1L);
        SubmissionDTO submissionDTO = submissionMapper.toDto(submission);

        mockMvc.perform(post("/api/submissions")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(submissionDTO)))
            .andExpect(status().isBadRequest());

        List<Submission> submissions = submissionJpaRepository.findAll();
        assertThat(submissions).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @WithUserDetails(STUDENT_LOGIN)
    void shouldThrowConflictWhenResubmittingAlreadyGradedAndNotResubmittableSubmission() throws Exception {
        transactionHelper.withTransaction(() -> {
            Assignment assignment = assignmentWithReferences();
            student = getStudent(entityManager);
            assignment.getMilestone().getJourney().addStudent(student);
            submission = createSubmission()
                .resubmittable(false)
                .assignment(assignment)
                .addAuthor(student);
            createSubmissionFeedback()
                .student(student)
                .submission(submission)
                .grade(SUBMISSION_GRADE)
                .creator(getTeacher(entityManager));
            persistCreatedEntities(entityManager);
        });

        SubmissionPatchDTO submissionPatchDTO = new SubmissionResubmitPatchDTO()
            .value(PATCH_VALUE)
            .submittedForGradingDate(PATCH_SUBMITTED_FOR_GRADING_DATE)
            .uploadedFiles(PATCH_UPLOADED_FILES);

        mockMvc.perform(patch("/api/submissions/{id}", submission.getId())
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(submissionPatchDTO))
            )
            .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockCustomUser(authorities = {STUDENT}, userId = "-3")
    void shouldThrowForbiddenWhenCurrentUserNotInSubmissionGroup() throws Exception {
        User student = createStudent();
        User student2 = createStudent()
            .setEmail("student2@netgroup.com")
            .setLogin("student2");
        Group group = createGroup()
            .setStudents(Set.of(student, student2));


        transactionHelper.withNewTransaction(() -> {
            assignment = assignmentWithReferences().setGroups(Set.of(group));
            assignment.getMilestone().getJourney().addStudent(getStudent(entityManager));
            persistCreatedEntities(entityManager);
        });
        SubmissionDTO submissionDTO = createSubmissionDTO(group.getId());
        submissionDTO.setAuthors(new HashSet<>(userMapper.toDto(Set.of(student, student2))));

        mockMvc.perform(post("/api/submissions")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(submissionDTO)))
            .andExpect(status().isForbidden())
            .andExpect(jsonPath("$.detail").value("Student '-3' is not an author of the submission!"));
    }

    @Test
    @WithMockCustomUser(authorities = {STUDENT}, userId = "-3")
    void shouldThrowForbiddenWhenOneOfAuthorsNotInSubmissionGroup() throws Exception {
        User studentInGroup = createStudent();
        User studentNotInGroup = createStudent()
            .setEmail("student2@netgroup.com")
            .setLogin("studentNotInGroup");
        Group group = createGroup();

        transactionHelper.withNewTransaction(() -> {
            User currentUser = getStudent(entityManager);
            group.setStudents(Set.of(currentUser, studentInGroup));
            assignment = assignmentWithReferences().setGroups(Set.of(group));
            assignment.getMilestone()
                .getJourney()
                .addStudent(currentUser)
                .addStudent(studentInGroup)
                .addStudent(studentNotInGroup);
            persistCreatedEntities(entityManager);
        });
        UserDTO currentUserDTO = new UserDTO();
        currentUserDTO.setId(-3L);
        SubmissionDTO submissionDTO = createSubmissionDTO(group.getId());
        submissionDTO.setAuthors(Set.of(currentUserDTO, userMapper.toDto(studentInGroup), userMapper.toDto(studentNotInGroup)));

        mockMvc.perform(post("/api/submissions")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(submissionDTO)))
            .andExpect(status().isForbidden())
            .andExpect(jsonPath("$.detail").value(format("User '%s' is not a group member of the submission!", studentNotInGroup.getId())));
    }

    @Test
    @WithMockCustomUser(authorities = {STUDENT}, userId = "-3")
    void shouldAllowStudentToCreateGroupSubmissionGradedByGroup() throws Exception {
        User studentInGroup = createStudent();
        Group group = createGroup();

        transactionHelper.withNewTransaction(() -> {
            User currentUser = getStudent(entityManager);
            group.setStudents(Set.of(currentUser, studentInGroup));
            assignment = assignmentWithReferences()
                .setGroups(Set.of(group));
            assignment.getMilestone()
                .getJourney()
                .addStudent(currentUser)
                .addTeacher(otherTeacher);
            persistCreatedEntities(entityManager);
        });

        UserDTO currentUserDTO = new UserDTO()
            .setId(-3L);

        SubmissionDTO submissionDTO = createSubmissionDTO(group.getId());
        submissionDTO.setAuthors(Set.of(currentUserDTO, userMapper.toDto(studentInGroup)));

        mockMvc.perform(post("/api/submissions")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(submissionDTO)))
            .andExpect(status().isCreated());

        List<Notification> notifications = notificationRepository.findAll();
        assertThat(notifications).hasSize(2);
        notifications.forEach(notification -> {
            assertThat(notification.getMessage()).contains("{SUBMITTED_SOLUTION}");
            assertThat(notification.getLink()).contains(format("&groupId=%d", group.getId()));
        });
    }

    @Test
    @WithMockCustomUser(authorities = {STUDENT}, userId = "-3")
    void shouldAllowStudentToCreateGroupSubmissionGradedIndividually() throws Exception {
        User studentInGroup = createStudent();
        Group group = createGroup();

        transactionHelper.withNewTransaction(() -> {
            User currentUser = getStudent(entityManager);
            group.setStudents(Set.of(currentUser, studentInGroup));
            assignment = assignmentWithReferences()
                .setGradeGroupMembersIndividually(true)
                .setGroups(Set.of(group));
            assignment.getMilestone()
                .getJourney()
                .addStudent(currentUser)
                .addTeacher(otherTeacher);
            persistCreatedEntities(entityManager);
        });

        UserDTO currentUserDTO = new UserDTO()
            .setId(-3L);

        SubmissionDTO submissionDTO = createSubmissionDTO(group.getId());
        submissionDTO.setAuthors(Set.of(currentUserDTO, userMapper.toDto(studentInGroup)));

        mockMvc.perform(post("/api/submissions")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(submissionDTO)))
            .andExpect(status().isCreated());

        List<Notification> notifications = notificationRepository.findAll();
        assertThat(notifications).hasSize(4);
        notifications.forEach(notification -> {
            assertThat(notification.getMessage()).contains("{SUBMITTED_SOLUTION}");
            assertThat(notification.getLink()).contains(format("&groupId=%d&studentId=%d", group.getId(), notification.getCreator().getId()));
        });
    }

    @Test
    @WithUserDetails(STUDENT_LOGIN)
    void shouldAllowStudentToCreateSubmissionWithoutFeedbacks() throws Exception {
        transactionHelper.withNewTransaction(() -> {
            assignment = assignmentWithReferences();
            assignment.getMilestone().getJourney().addStudent(getStudent(entityManager));
            persistCreatedEntities(entityManager);
        });

        submission = createSubmission()
            .assignment(assignment);

        transactionHelper.withNewTransaction(() -> {
            submission.addAuthor(getStudent(entityManager));
        });

        mockMvc.perform(post("/api/submissions")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(submissionMapper.toDto(submission))))
            .andExpect(status().isCreated());

        transactionHelper.withNewTransaction(() -> {
            List<Submission> submissions = submissionJpaRepository.findAll();
            assertThat(submissions).hasSize(1);
            Submission testSubmission = submissions.get(0);
            assertEquals(SUBMISSION_VALUE, testSubmission.getValue());

            List<Notification> notifications = notificationRepository.findAll();
            assertThat(notifications).hasSize(1);
            assertThat(notifications.get(0).getMessage()).contains("{SUBMITTED_SOLUTION}");
            assertThat(notifications.get(0).getLink()).contains("&studentId=");
        });
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void shouldAllowStudentToResubmitSubmission() throws Exception {
        final SubmissionFeedback[] submissionFeedback = new SubmissionFeedback[1];
        transactionHelper.withTransaction(() -> {
            User student = getStudent(entityManager);
            User teacher = getTeacher(entityManager);
            Assignment assignment = assignmentWithReferences();
            assignment.getMilestone().getJourney().addStudent(student);
            submissionFeedback[0] = createSubmissionFeedback()
                .student(student)
                .submission(createSubmission()
                    .assignment(assignment)
                    .addAuthor(student))
                .grade(SUBMISSION_GRADE)
                .creator(teacher);
            persistCreatedEntities(entityManager);
        });

        SubmissionPatchDTO submissionPatchDTO = new SubmissionResubmitPatchDTO()
            .value(PATCH_VALUE)
            .submittedForGradingDate(PATCH_SUBMITTED_FOR_GRADING_DATE)
            .uploadedFiles(PATCH_UPLOADED_FILES);

        mockMvc.perform(patch("/api/submissions/{id}", submissionFeedback[0].getSubmission().getId())
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(submissionPatchDTO))
            )
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(submissionFeedback[0].getSubmission().getId()))
            .andExpect(jsonPath("$.value").value(PATCH_VALUE))
            .andExpect(jsonPath("$.resubmittable").value(SUBMISSION_RESUBMITTABLE))
            .andExpect(jsonPath("$.submittedForGradingDate").value(PATCH_SUBMITTED_FOR_GRADING_DATE.toString()));

        List<SubmissionFeedback> submissionFeedbacks = submissionFeedbackJpaRepository.findAll();
        assertThat(submissionFeedbacks.size()).isEqualTo(1);

        SubmissionFeedback submissionFeedback1 = Arrays.stream(submissionFeedback).findFirst().get();

        assertThat(submissionFeedback1.getGrade()).isEqualTo(SUBMISSION_GRADE);
        assertThat(submissionFeedback1.getSubmission().getId()).isEqualTo(submissionFeedback[0].getSubmission().getId());
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldAddGroupToSubmissionOnGroupAssignmentAndGroupIdMissing() throws Exception {
        User studentInGroup = createStudent();
        Group group = createGroup();

        transactionHelper.withNewTransaction(() -> {
            User currentUser = getStudent(entityManager);
            group.setStudents(Set.of(currentUser, studentInGroup));
            assignment = assignmentWithReferences()
                .setGradeGroupMembersIndividually(true)
                .setGroups(Set.of(group));
            assignment.getMilestone()
                .getJourney()
                .addStudent(currentUser);
            persistCreatedEntities(entityManager);
        });

        UserDTO currentUserDTO = new UserDTO()
            .setId(-3L);

        SubmissionDTO submissionDTO = createSubmissionDTO(null);
        submissionDTO.setAuthors(Set.of(currentUserDTO, userMapper.toDto(studentInGroup)));

        mockMvc.perform(post("/api/submissions")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(submissionDTO)))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.groupId").value(group.getId()));

        List<Notification> notifications = notificationRepository.findAll();
        assertThat(notifications).hasSize(2);
        notifications.forEach(notification -> {
            assertThat(notification.getMessage()).contains("{SUBMITTED_SOLUTION}");
            assertThat(notification.getLink()).contains(format("&groupId=%d&studentId=%d", group.getId(), notification.getCreator().getId()));
        });
    }

    @Test
    @WithUserDetails(STUDENT_LOGIN)
    void shouldUpdateSubmissionAuthorsOnResubmit() throws Exception {
        transactionHelper.withNewTransaction(() -> {
            User student = getStudent(entityManager);
            User student2 = createStudent().setFirstName(STUDENT_LOGIN);
            Group group = createGroup().setStudents(Set.of(student, student2));
            Assignment assignment = assignmentWithReferences().setGroups(Set.of(group));
            assignment.getMilestone().getJourney().addStudent(student);
            submission = createSubmission()
                .assignment(assignment)
                .group(group)
                .addAuthor(student);
            persistCreatedEntities(entityManager);
        });

        SubmissionPatchDTO submissionPatchDTO = new SubmissionResubmitPatchDTO()
            .value(PATCH_VALUE)
            .submittedForGradingDate(PATCH_SUBMITTED_FOR_GRADING_DATE);

        mockMvc.perform(patch("/api/submissions/{id}", submission.getId())
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(submissionPatchDTO))
            )
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(submission.getId().intValue()))
            .andExpect(jsonPath("$.authors", hasSize(2)))
            .andExpect(jsonPath("$.authors[*].firstName", Matchers.containsInAnyOrder(STUDENT_LOGIN, "Student")));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldAllowTeacherToGetAllSubmissionsByAssignmentId() throws Exception {
        final Submission[] submissionNotForGrading = new Submission[1];
        transactionHelper.withTransaction(() -> {
            student = createStudent();
            submission = createSubmission().addAuthor(student);
            createSubmissionFeedback()
                .student(student)
                .submission(submission)
                .grade(SUBMISSION_GRADE)
                .creator(getTeacher(entityManager));
            submissionNotForGrading[0] = createSubmission()
                .addAuthor(student)
                .submittedForGradingDate(null);
            assignment = assignmentWithReferences()
                .addSubmission(submission)
                .addSubmission(submissionNotForGrading[0]);
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(get(format("/api/submissions?assignmentId=%s", assignment.getId())))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.size()").value(2))
            .andExpect(jsonPath("$.[0].id").value(submission.getId().intValue()))
            .andExpect(jsonPath("$.[0].value").value(SUBMISSION_VALUE))
            .andExpect(jsonPath("$.[0].submissionFeedbacks.[0].grade").value(SUBMISSION_GRADE))
            .andExpect(jsonPath("$.[0].resubmittable").value(SUBMISSION_RESUBMITTABLE))
            .andExpect(jsonPath("$.[0].submissionFeedbacks.[0].feedbackDate").value(SUBMISSION_FEEDBACK_DATE.toString()))
            .andExpect(jsonPath("$.[0].submittedForGradingDate").value(SUBMISSION_SUBMITTED_FOR_GRADING_DATE.toString()))
            .andExpect(jsonPath("$.[1].id").value(submissionNotForGrading[0].getId().intValue()));
    }

    @Test
    @Transactional
    @WithMockCustomUser(authorities = {ADMIN})
    void shouldAllowAdminToUpdateSubmission() throws Exception {
        // TODO: Test allow admin to grade submission
        submission = createSubmission()
            .addAuthor(getStudent(entityManager))
            .assignment(assignmentWithReferences()
                .creator(getTeacher(entityManager))
                .gradingScheme(getGradingSchemeNumerical(entityManager)));
        persistCreatedEntities(entityManager);

        Submission updatedSubmission = submissionJpaRepository.getById(submission.getId());
        entityManager.detach(updatedSubmission);
        updatedSubmission.value(UPDATED_VALUE);
        SubmissionDTO submissionDTO = submissionMapper.toDto(updatedSubmission);

        mockMvc.perform(put("/api/submissions")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(submissionDTO)))
            .andExpect(status().isOk());

        List<Submission> submissions = submissionJpaRepository.findAll();
        assertThat(submissions).hasSize(1);
        Submission submission = submissions.get(0);
        assertThat(submission.getValue()).isEqualTo(UPDATED_VALUE);
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void shouldHideSubmissionsOfHiddenAssignmentsFromStudent() throws Exception {
        submission = createSubmission();
        assignmentWithReferences()
            .published(null)
            .addSubmission(submission
                .addAuthor(getStudent(entityManager)));
        persistCreatedEntities(entityManager);

        mockMvc.perform(get("/api/submissions"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.length()").value(0));
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void shouldNotHideSubmissionsOfHiddenAssignmentsFromTeacher() throws Exception {
        submission = createSubmission();
        assignmentWithReferences()
            .published(null)
            .addSubmission(submission
                .addAuthor(getStudent(entityManager)));
        persistCreatedEntities(entityManager);

        mockMvc.perform(get("/api/submissions"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.length()").value(1));
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void shouldNotAllowSubmittingToHiddenAssignment() throws Exception {
        Assignment assignment = assignmentWithReferences()
            .published(null);
        persistCreatedEntities(entityManager);
        submission = createSubmission()
            .addAuthor(getStudent(entityManager))
            .assignment(assignment);

        mockMvc.perform(post("/api/submissions")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(submissionMapper.toDto(submission))))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void shouldThrowOnGetSubmissionWhenTeacherNotInJourney() throws Exception {
        Assignment assignment = assignmentWithReferences();
        assignment.getMilestone().getJourney().setTeachers(emptySet());
        submission = createSubmission();
        assignment.addSubmission(submission);
        persistCreatedEntities(entityManager);

        mockMvc.perform(get("/api/submissions/{id}", submission.getId()))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void shouldThrowOnGetSubmissionWhenStudentNotInJourney() throws Exception {
        Assignment assignment = assignmentWithReferences();
        assignment.getMilestone().getJourney().setStudents(emptySet());
        submission = createSubmission();
        assignment.addSubmission(submission);
        persistCreatedEntities(entityManager);

        mockMvc.perform(get("/api/submissions/{id}", submission.getId()))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void shouldGetEmptyResponseOnGetAllSubmissionsWhenStudentNotInJourney() throws Exception {
        Assignment assignment = assignmentWithReferences();
        assignment.getMilestone().getJourney().setStudents(emptySet());
        submission = createSubmission();
        assignment.addSubmission(submission);
        persistCreatedEntities(entityManager);

        mockMvc.perform(get("/api/submissions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("length()").value(0));
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void shouldGetEmptyResponseOnGetAllSubmissionsWhenTeacherNotInJourney() throws Exception {
        Assignment assignment = assignmentWithReferences();
        assignment.getMilestone().getJourney().setTeachers(emptySet());
        submission = createSubmission();
        assignment.addSubmission(submission);
        persistCreatedEntities(entityManager);

        mockMvc.perform(get("/api/submissions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("length()").value(0));
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void shouldThrowOnResubmitSubmissionWhenSubmissionNotPresent() throws Exception {
        SubmissionPatchDTO submissionPatchDTO = new SubmissionResubmitPatchDTO()
            .value(PATCH_VALUE)
            .submittedForGradingDate(PATCH_SUBMITTED_FOR_GRADING_DATE)
            .uploadedFiles(PATCH_UPLOADED_FILES);

        mockMvc.perform(patch("/api/submissions/{id}", 1)
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(submissionPatchDTO))
            )
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void shouldThrowOnResubmitSubmissionWhenStudentNotInJourney() throws Exception {
        submission = createSubmission()
            .assignment(assignmentWithReferences())
            .addAuthor(getStudent(entityManager));
        persistCreatedEntities(entityManager);
        submission.getAssignment().getMilestone().getJourney().setStudents(Collections.emptySet());

        SubmissionPatchDTO submissionPatchDTO = new SubmissionResubmitPatchDTO()
            .value(PATCH_VALUE)
            .submittedForGradingDate(PATCH_SUBMITTED_FOR_GRADING_DATE)
            .uploadedFiles(PATCH_UPLOADED_FILES);

        mockMvc.perform(patch("/api/submissions/{id}", submission.getId())
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(submissionPatchDTO))
            )
            .andExpect(status().isNotFound());
    }

    @Test
    @WithUserDetails(STUDENT_LOGIN)
    void shouldSendSubmissionSubmittedNotificationWhenStudentResubmitsGradedSubmission() throws Exception {
        transactionHelper.withTransaction(() -> {
            student = getStudent(entityManager);
            Assignment assignment = assignmentWithReferences();
            assignment.getMilestone().getJourney().addStudent(student).addTeacher(otherTeacher);
            submission = createSubmission().addAuthor(student);
            assignment.addSubmission(submission);
            createSubmissionFeedback()
                .submission(submission)
                .grade(SUBMISSION_GRADE)
                .student(student)
                .creator(getTeacher(entityManager));
            persistCreatedEntities(entityManager);
        });

        SubmissionPatchDTO submissionPatchDTO = new SubmissionResubmitPatchDTO()
            .value(PATCH_VALUE)
            .submittedForGradingDate(PATCH_SUBMITTED_FOR_GRADING_DATE);

        mockMvc.perform(patch("/api/submissions/{id}", submission.getId())
                .contentType(APPLICATION_JSON).content(convertObjectToJsonBytes(submissionPatchDTO)))
            .andExpect(status().isOk());

        List<Notification> notifications = notificationRepository.findAll();
        assertThat(notifications).hasSize(2);
        notifications.forEach(notification -> {
            assertThat(notification.getMessage()).contains("{SUBMITTED_SOLUTION}");
            assertThat(notification.getLink()).contains(format("&studentId=%d", notification.getCreator().getId()));
            String login = notification.getRecipient().getLogin();
            assertTrue(
                login.equals(otherTeacher.getLogin()) ||
                    login.equals(TEACHER_LOGIN)
            );
        });
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldOrderFeedbackByDateInDescendingOrderForTeacher() throws Exception {
        shouldOrderFeedbackByDateInDescendingOrder();
    }

    @Test
    @WithUserDetails(STUDENT_LOGIN)
    void shouldOrderFeedbackByDateInDescendingOrderForStudent() throws Exception {
        shouldOrderFeedbackByDateInDescendingOrder();
    }

    private void shouldOrderFeedbackByDateInDescendingOrder() throws Exception {
        Instant firstFeedbackDate = Instant.now().minus(10, DAYS);
        Instant secondFeedbackDate = Instant.now();

        transactionHelper.withTransaction(() -> {
            student = getStudent(entityManager);
            User teacher = getTeacher(entityManager);
            Assignment assignment = assignmentWithReferences();
            assignment.getMilestone().getJourney().addStudent(student);
            submission = createSubmission().addAuthor(student);
            assignment.addSubmission(submission);
            createSubmissionFeedback()
                .submission(submission)
                .grade(SUBMISSION_GRADE)
                .student(student)
                .creator(teacher)
                .setFeedbackDate(firstFeedbackDate);
            createSubmissionFeedback()
                .submission(submission)
                .grade(SUBMISSION_GRADE)
                .student(student)
                .creator(teacher)
                .setFeedbackDate(secondFeedbackDate);
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(get("/api/submissions/{id}", submission.getId()))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.submissionFeedbacks[0].feedbackDate").value(secondFeedbackDate.toString()))
            .andExpect(jsonPath("$.submissionFeedbacks[1].feedbackDate").value(firstFeedbackDate.toString()));
    }
}
