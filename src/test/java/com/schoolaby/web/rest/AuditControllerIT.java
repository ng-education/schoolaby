package com.schoolaby.web.rest;

import com.schoolaby.SchoolabyApp;
import com.schoolaby.common.BaseIntegrationTest;
import com.schoolaby.domain.Journey;
import com.schoolaby.domain.PersistentAuditEvent;
import com.schoolaby.repository.PersistenceAuditEventRepository;
import com.schoolaby.security.Role;
import com.schoolaby.web.WithMockCustomUser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;

import static com.schoolaby.common.TestCases.getStudent;
import static com.schoolaby.common.TestCases.getTeacher;
import static com.schoolaby.common.TestObjects.*;
import static com.schoolaby.security.Role.Constants.TEACHER;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.boot.actuate.security.AuthenticationAuditListener.AUTHENTICATION_SUCCESS;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@AutoConfigureMockMvc
@WithMockUser(authorities = Role.Constants.ADMIN)
@SpringBootTest(classes = SchoolabyApp.class)
@Transactional
public class AuditControllerIT extends BaseIntegrationTest {
    private static final String SAMPLE_PRINCIPAL = "SAMPLE_PRINCIPAL";
    private static final Instant SAMPLE_TIMESTAMP = Instant.parse("2015-08-04T10:11:30Z");
    private static final long SECONDS_PER_DAY = 60 * 60 * 24;

    @Autowired
    private PersistenceAuditEventRepository auditEventRepository;
    @Autowired
    private MockMvc restAuditMockMvc;

    private PersistentAuditEvent auditEvent;
    private Journey journey;

    @BeforeEach
    public void initTest() {
        transactionHelper.withTransaction(() -> {
            journey = createJourney()
                .addStudent(getStudent(entityManager))
                .creator(getTeacher(entityManager))
                .addTeacher(getTeacher(entityManager))
                .educationalLevel(createEducationalLevel());
            persistCreatedEntities(entityManager);
        });

        auditEventRepository.deleteAll();
        auditEvent = new PersistentAuditEvent();
        auditEvent.setAuditEventType(AUTHENTICATION_SUCCESS);
        auditEvent.setPrincipal(SAMPLE_PRINCIPAL);
        auditEvent.setUserId(TEACHER_ID);
        auditEvent.setAuditEventDate(SAMPLE_TIMESTAMP);
    }

    @Test
    @WithMockCustomUser(userId = "-2", authorities = {TEACHER})
    public void getAllAudits() throws Exception {
        // Initialize the database
        auditEventRepository.save(auditEvent);

        // Get all the audits
        restAuditMockMvc
            .perform(get("/management/audits"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].principal").value(hasItem(SAMPLE_PRINCIPAL)))
            .andExpect(jsonPath("$.[0].data.userId").value(TEACHER_ID));
    }

    @Test
    @WithMockCustomUser(userId = "-2", authorities = {TEACHER})
    public void getAllAuditsByJourneyId() throws Exception {
        String principle = "principle";
        PersistentAuditEvent event = new PersistentAuditEvent();
        event.setAuditEventType(AUTHENTICATION_SUCCESS);
        event.setPrincipal(principle);
        event.setAuditEventDate(SAMPLE_TIMESTAMP);
        event.setUserId(STUDENT_ID);
        // Initialize the database
        auditEventRepository.save(auditEvent);
        auditEventRepository.save(event);

        // Get all the audits
        restAuditMockMvc
            .perform(get("/management/audits?journeyId={p}", journey.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$", hasSize(1)))
            .andExpect(jsonPath("$.[0].principal").value(principle))
            .andExpect(jsonPath("$.[0].data.userId").value(STUDENT_ID));
    }

    @Test
    public void getAudit() throws Exception {
        // Initialize the database
        auditEventRepository.save(auditEvent);

        // Get the audit
        restAuditMockMvc
            .perform(get("/management/audits/{id}", auditEvent.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.principal").value(SAMPLE_PRINCIPAL));
    }

    @Test
    public void getAuditsByDate() throws Exception {
        // Initialize the database
        auditEventRepository.save(auditEvent);

        // Generate dates for selecting audits by date, making sure the period will contain the audit
        String fromDate = SAMPLE_TIMESTAMP.minusSeconds(SECONDS_PER_DAY).toString().substring(0, 10);
        String toDate = SAMPLE_TIMESTAMP.plusSeconds(SECONDS_PER_DAY).toString().substring(0, 10);

        // Get the audit
        restAuditMockMvc
            .perform(get("/management/audits?fromDate=" + fromDate + "&toDate=" + toDate))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].principal").value(hasItem(SAMPLE_PRINCIPAL)));
    }

    @Test
    public void getNonExistingAuditsByDate() throws Exception {
        // Initialize the database
        auditEventRepository.save(auditEvent);

        // Generate dates for selecting audits by date, making sure the period will not contain the sample audit
        String fromDate = SAMPLE_TIMESTAMP.minusSeconds(2 * SECONDS_PER_DAY).toString().substring(0, 10);
        String toDate = SAMPLE_TIMESTAMP.minusSeconds(SECONDS_PER_DAY).toString().substring(0, 10);

        // Query audits but expect no results
        restAuditMockMvc
            .perform(get("/management/audits?fromDate=" + fromDate + "&toDate=" + toDate))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(header().string("X-Total-Count", "0"));
    }

    @Test
    public void getNonExistingAudit() throws Exception {
        // Get the audit
        restAuditMockMvc.perform(get("/management/audits/{id}", Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    public void testPersistentAuditEventEquals() throws Exception {
        TestUtil.equalsVerifier(PersistentAuditEvent.class);
        PersistentAuditEvent auditEvent1 = new PersistentAuditEvent();
        auditEvent1.setId(1L);
        PersistentAuditEvent auditEvent2 = new PersistentAuditEvent();
        auditEvent2.setId(auditEvent1.getId());
        assertThat(auditEvent1).isEqualTo(auditEvent2);
        auditEvent2.setId(2L);
        assertThat(auditEvent1).isNotEqualTo(auditEvent2);
        auditEvent1.setId(null);
        assertThat(auditEvent1).isNotEqualTo(auditEvent2);
    }
}
