package com.schoolaby.web.rest;

import com.schoolaby.common.BaseIntegrationTest;
import com.schoolaby.domain.PersonRole;
import com.schoolaby.repository.PersonRoleRepository;
import com.schoolaby.security.Role;
import com.schoolaby.service.dto.PersonRoleDTO;
import com.schoolaby.service.mapper.PersonRoleMapper;
import com.schoolaby.web.WithMockCustomUser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDateTime;
import java.util.List;

import static com.schoolaby.security.Role.Constants.ADMIN;
import static com.schoolaby.web.rest.TestUtil.convertObjectToJsonBytes;
import static java.time.LocalDateTime.now;
import static java.time.LocalDateTime.ofEpochSecond;
import static java.time.ZoneOffset.UTC;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WithMockCustomUser(authorities = {ADMIN})
class PersonRoleControllerIT extends BaseIntegrationTest {
    private static final Role DEFAULT_ROLE = Role.STUDENT;
    private static final Role UPDATED_ROLE = Role.TEACHER;

    private static final Boolean DEFAULT_ACTIVE = false;
    private static final Boolean UPDATED_ACTIVE = true;

    private static final LocalDateTime DEFAULT_START_DATE = ofEpochSecond(0, 0, UTC);
    private static final LocalDateTime UPDATED_START_DATE = now();

    private static final LocalDateTime DEFAULT_END_DATE = ofEpochSecond(0, 0, UTC);
    private static final LocalDateTime UPDATED_END_DATE = now();

    private static final String DEFAULT_GRADE = "AAAAAAAAAA";
    private static final String UPDATED_GRADE = "BBBBBBBBBB";

    private static final String DEFAULT_PARALLEL = "AAAAAAAAAA";
    private static final String UPDATED_PARALLEL = "BBBBBBBBBB";

    @Autowired
    private PersonRoleRepository personRoleRepository;
    @Autowired
    private PersonRoleMapper personRoleMapper;

    private PersonRole personRole;

    public static PersonRole createEntity(EntityManager em) {
        return new PersonRole()
            .role(DEFAULT_ROLE)
            .active(DEFAULT_ACTIVE)
            .startDate(DEFAULT_START_DATE)
            .endDate(DEFAULT_END_DATE)
            .grade(DEFAULT_GRADE)
            .parallel(DEFAULT_PARALLEL);
    }

    @BeforeEach
    void initTest() {
        personRole = createEntity(entityManager);
    }

    @Test
    @Transactional
    void createPersonRole() throws Exception {
        int databaseSizeBeforeCreate = personRoleRepository.findAll().size();
        PersonRoleDTO personRoleDTO = personRoleMapper.toDto(personRole);

        mockMvc.perform(post("/api/person-roles")
            .contentType(APPLICATION_JSON)
            .content(convertObjectToJsonBytes(personRoleDTO)))
            .andExpect(status().isCreated());

        List<PersonRole> personRoles = personRoleRepository.findAll();
        assertThat(personRoles).hasSize(databaseSizeBeforeCreate + 1);
        PersonRole personRole = personRoles.get(personRoles.size() - 1);
        assertThat(personRole.getRole()).isEqualTo(DEFAULT_ROLE);
        assertThat(personRole.isActive()).isEqualTo(DEFAULT_ACTIVE);
        assertThat(personRole.getStartDate()).isEqualTo(DEFAULT_START_DATE);
        assertThat(personRole.getEndDate()).isEqualTo(DEFAULT_END_DATE);
        assertThat(personRole.getGrade()).isEqualTo(DEFAULT_GRADE);
        assertThat(personRole.getParallel()).isEqualTo(DEFAULT_PARALLEL);
    }

    @Test
    @Transactional
    void createPersonRoleWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = personRoleRepository.findAll().size();
        personRole.setId(1L);
        PersonRoleDTO personRoleDTO = personRoleMapper.toDto(personRole);

        mockMvc.perform(post("/api/person-roles")
            .contentType(APPLICATION_JSON)
            .content(convertObjectToJsonBytes(personRoleDTO)))
            .andExpect(status().isBadRequest());

        List<PersonRole> personRoles = personRoleRepository.findAll();
        assertThat(personRoles).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllPersonRoles() throws Exception {
        personRoleRepository.saveAndFlush(personRole);

        mockMvc.perform(get("/api/person-roles?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content()
                .contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(personRole.getId().intValue())))
            .andExpect(jsonPath("$.[*].role").value(hasItem(DEFAULT_ROLE.toString())))
            .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE)))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem("1970-01-01T00:00:00")))
            .andExpect(jsonPath("$.[*].endDate").value(hasItem("1970-01-01T00:00:00")))
            .andExpect(jsonPath("$.[*].grade").value(hasItem(DEFAULT_GRADE)))
            .andExpect(jsonPath("$.[*].parallel").value(hasItem(DEFAULT_PARALLEL)));
    }

    @Test
    @Transactional
    void getPersonRole() throws Exception {
        personRoleRepository.saveAndFlush(personRole);

        mockMvc.perform(get("/api/person-roles/{id}", personRole.getId()))
            .andExpect(status().isOk())
            .andExpect(content()
                .contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(personRole.getId().intValue()))
            .andExpect(jsonPath("$.role").value(DEFAULT_ROLE.toString()))
            .andExpect(jsonPath("$.active").value(DEFAULT_ACTIVE))
            .andExpect(jsonPath("$.startDate").value("1970-01-01T00:00:00"))
            .andExpect(jsonPath("$.endDate").value("1970-01-01T00:00:00"))
            .andExpect(jsonPath("$.grade").value(DEFAULT_GRADE))
            .andExpect(jsonPath("$.parallel").value(DEFAULT_PARALLEL));
    }

    @Test
    @Transactional
    void getNonExistingPersonRole() throws Exception {
        mockMvc.perform(get("/api/person-roles/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void updatePersonRole() throws Exception {
        personRoleRepository.saveAndFlush(personRole);
        int databaseSizeBeforeUpdate = personRoleRepository.findAll().size();
        PersonRole updatedPersonRole = personRoleRepository.findById(personRole.getId()).get();
        entityManager.detach(updatedPersonRole);
        updatedPersonRole
            .role(UPDATED_ROLE)
            .active(UPDATED_ACTIVE)
            .startDate(UPDATED_START_DATE)
            .endDate(UPDATED_END_DATE)
            .grade(UPDATED_GRADE)
            .parallel(UPDATED_PARALLEL);
        PersonRoleDTO personRoleDTO = personRoleMapper.toDto(updatedPersonRole);

        mockMvc.perform(put("/api/person-roles")
            .contentType(APPLICATION_JSON)
            .content(convertObjectToJsonBytes(personRoleDTO)))
            .andExpect(status().isOk());

        List<PersonRole> personRoles = personRoleRepository.findAll();
        assertThat(personRoles).hasSize(databaseSizeBeforeUpdate);
        PersonRole personRole = personRoles.get(personRoles.size() - 1);
        assertThat(personRole.getRole()).isEqualTo(UPDATED_ROLE);
        assertThat(personRole.isActive()).isEqualTo(UPDATED_ACTIVE);
        assertThat(personRole.getStartDate()).isEqualTo(UPDATED_START_DATE);
        assertThat(personRole.getEndDate()).isEqualTo(UPDATED_END_DATE);
        assertThat(personRole.getGrade()).isEqualTo(UPDATED_GRADE);
        assertThat(personRole.getParallel()).isEqualTo(UPDATED_PARALLEL);
    }

    @Test
    @Transactional
    void updateNonExistingPersonRole() throws Exception {
        int databaseSizeBeforeUpdate = personRoleRepository.findAll().size();
        PersonRoleDTO personRoleDTO = personRoleMapper.toDto(personRole);

        mockMvc.perform(put("/api/person-roles")
            .contentType(APPLICATION_JSON)
            .content(convertObjectToJsonBytes(personRoleDTO)))
            .andExpect(status().isBadRequest());

        List<PersonRole> personRoles = personRoleRepository.findAll();
        assertThat(personRoles).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deletePersonRole() throws Exception {
        personRoleRepository.saveAndFlush(personRole);
        int databaseSizeBeforeDelete = personRoleRepository.findAll().size();

        mockMvc.perform(delete("/api/person-roles/{id}", personRole.getId())
            .accept(APPLICATION_JSON))
            .andExpect(status().isNoContent());

        List<PersonRole> personRoles = personRoleRepository.findAll();
        assertThat(personRoles).hasSize(databaseSizeBeforeDelete - 1);
    }
}
