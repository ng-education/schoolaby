package com.schoolaby.web.rest;

import com.schoolaby.common.BaseIntegrationTest;
import com.schoolaby.domain.*;
import com.schoolaby.domain.keys.JourneyUserKey;
import com.schoolaby.repository.JourneyJpaRepository;
import com.schoolaby.service.JoinJourneyDTO;
import com.schoolaby.service.dto.JourneyDTO;
import com.schoolaby.service.mapper.JourneyMapper;
import com.schoolaby.service.mapper.MilestoneMapper;
import com.schoolaby.service.mapper.SchoolMapper;
import com.schoolaby.service.mapper.SubmissionMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static com.schoolaby.common.TestCases.*;
import static com.schoolaby.common.TestObjects.*;
import static com.schoolaby.domain.enumeration.LtiConfigType.JOURNEY;
import static com.schoolaby.security.Role.Constants.STUDENT;
import static com.schoolaby.security.Role.Constants.TEACHER;
import static com.schoolaby.service.dto.states.EntityState.IN_PROGRESS;
import static com.schoolaby.web.rest.TestUtil.convertObjectToJsonBytes;
import static java.lang.Long.MAX_VALUE;
import static java.lang.String.format;
import static java.time.temporal.ChronoUnit.DAYS;
import static java.util.Collections.emptySet;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.http.MediaType.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

class JourneyControllerIT extends BaseIntegrationTest {
    public static final String NEW_JOURNEY_TITLE = "New journey title";
    @Autowired
    private JourneyJpaRepository journeyJpaRepository;
    @Autowired
    private JourneyMapper journeyMapper;
    @Autowired
    private SubmissionMapper submissionMapper;
    @Autowired
    private MilestoneMapper milestoneMapper;
    @Autowired
    private SchoolMapper schoolMapper;

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldCreate() throws Exception {
        EducationalLevel educationalLevel = createEducationalLevel();
        transactionHelper.withNewTransaction(() -> {
            persistCreatedEntities(entityManager);
        });

        Journey journey = createJourney()
            .educationalLevel(educationalLevel)
            .curriculum(getCurriculumById(entityManager, ESTONIAN_CURRICULUM_ID))
            .creator(getTeacher(entityManager));

        JourneyDTO journeyDTO = journeyMapper.toDto(journey);

        mockMvc.perform(post("/api/journeys")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(journeyDTO)))
            .andExpect(status().isCreated());

        transactionHelper.withNewTransaction(() -> {
            List<Journey> journeyList = journeyJpaRepository.findAll();
            assertThat(journeyList).hasSize(1);
            Journey testJourney = journeyList.get(0);
            assertThat(testJourney.getTitle()).isEqualTo(JOURNEY_TITLE);
            assertThat(testJourney.getDescription()).isEqualTo(JOURNEY_DESCRIPTION);
            assertThat(testJourney.getVideoConferenceUrl()).isEqualTo(JOURNEY_VIDEO_CONFERENCE_URL);
            assertThat(testJourney.getStartDate()).isEqualTo(JOURNEY_START_DATE);
            assertThat(testJourney.getEndDate()).isEqualTo(JOURNEY_END_DATE);
            // sign up code is generated
            assertThat(testJourney.getSignupCode(STUDENT)).isNotEmpty();
            assertThat(testJourney.getSignupCode(STUDENT)).isNotEqualTo(STUDENT_JOURNEY_SIGN_UP_CODE);
            assertThat(testJourney.getCurriculum().getTitle()).isEqualTo(ESTONIAN_CURRICULUM);
        });
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldCreateFromTemplate() throws Exception {
        final Journey journey = createJourney();
        final Milestone milestone = createMilestone();
        final EducationalLevel educationalLevel = createEducationalLevel();
        transactionHelper.withNewTransaction(() -> {
            final User teacher = getTeacher(entityManager);
            journey.addStudent(getStudent(entityManager))
                .addTeacher(teacher)
                .addMilestone(milestone
                    .creator(teacher))
                .educationalLevel(educationalLevel)
                .creator(teacher);
            milestone.addAssignment(
                createAssignment()
                    .gradingScheme(getGradingSchemeNumerical(entityManager))
                    .creator(teacher)
            );
            milestone.addAssignment(
                createAssignment()
                    .gradingScheme(getGradingSchemeNumerical(entityManager))
                    .creator(teacher)
            );
            persistCreatedEntities(entityManager);
        });

        assertTrue(milestone.getStates().isEmpty());

        journey.setId(null);
        journey.setTitle(NEW_JOURNEY_TITLE);
        journey.getMilestones().forEach(m -> m.setId(null));
        journey.getMilestones().stream().flatMap(m -> m.getAssignments().stream()).forEach(assignment -> assignment.setId(null));
        JourneyDTO journeyDTO = journeyMapper.toDto(journey);

        journeyDTO.setTeachers(List.of());
        journeyDTO.setStudents(Set.of());
        journeyDTO.setMilestones(new ArrayList<>(milestoneMapper.toDto(journey.getMilestones())));
        mockMvc.perform(post("/api/journeys")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(journeyDTO, true)))
            .andExpect(status().isCreated());

        transactionHelper.withNewTransaction(() -> {
            List<Journey> journeyList = journeyJpaRepository.findAll();
            assertThat(journeyList).hasSize(2);
            Journey testJourney = journeyList.stream().filter(journey1 -> journey1.getTitle().equals(NEW_JOURNEY_TITLE)).findFirst().orElseThrow();
            assertThat(testJourney.getDescription()).isEqualTo(JOURNEY_DESCRIPTION);
            assertThat(testJourney.getVideoConferenceUrl()).isEqualTo(JOURNEY_VIDEO_CONFERENCE_URL);
            assertThat(testJourney.getStartDate()).isEqualTo(JOURNEY_START_DATE);
            assertThat(testJourney.getEndDate()).isEqualTo(JOURNEY_END_DATE);
            // sign up code is generated
            assertThat(testJourney.getSignupCode(STUDENT)).isNotEmpty();
            assertThat(testJourney.getSignupCode(STUDENT)).isNotEqualTo(STUDENT_JOURNEY_SIGN_UP_CODE);
            assertThat(testJourney.getMilestones().stream().findFirst().orElseThrow().getStates().size()).isEqualTo(1);
        });
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void shouldThrowOnCreateWithId() throws Exception {
        User teacher = getTeacher(entityManager);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(teacher)
            .addTeacher(teacher);
        journey.setId(1L);
        JourneyDTO journeyDTO = journeyMapper.toDto(journey);

        mockMvc.perform(post("/api/journeys")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(journeyDTO)))
            .andExpect(status().isBadRequest());

        assertThat(journeyJpaRepository.findAll()).isEmpty();
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void shouldRequireTitleOnCreate() throws Exception {
        User teacher = getTeacher(entityManager);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(teacher).title(null)
            .addTeacher(teacher);

        mockMvc.perform(post("/api/journeys")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(journeyMapper.toDto(journey))))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("fieldErrors[0].objectName").value("journey"))
            .andExpect(jsonPath("fieldErrors[0].field").value("title"))
            .andExpect(jsonPath("fieldErrors[0].message").value("NotNull"));
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void shouldRequireStartDateOnCreate() throws Exception {
        User teacher = getTeacher(entityManager);
        JourneyDTO journeyDto = journeyMapper.toDto(createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(teacher)
            .addTeacher(teacher)
        );
        journeyDto.setStartDate(null);

        mockMvc.perform(post("/api/journeys")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(journeyDto)))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("fieldErrors[0].objectName").value("journey"))
            .andExpect(jsonPath("fieldErrors[0].field").value("startDate"))
            .andExpect(jsonPath("fieldErrors[0].message").value("NotNull"));
    }

    @Test
    @Transactional
    @WithUserDetails(ADMIN_LOGIN)
    void shouldGetAllJourneysAsAdmin() throws Exception {
        School school = createSchool();
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(getTeacher(entityManager))
            .addSchool(school)
            .addTeacher(getAdmin(entityManager));
        JourneyState journeyState = new JourneyState()
            .journey(journey)
            .state(IN_PROGRESS)
            .user(journey.getCreator())
            .authority(new Authority().name(TEACHER));
        journey.addState(journeyState);
        LtiConfig ltiConfig = createLtiConfig();
        ltiConfig.setType(JOURNEY);
        ltiConfig.setJourney(journey);
        journey.setLtiConfigs(Collections.singleton(ltiConfig));
        persistCreatedEntities(entityManager);

        mockMvc.perform(get("/api/journeys?sort=id,desc&state=IN_PROGRESS")
                .header("JOURNEY-JSON-VERSION", "1"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$", hasSize(1)))
            .andExpect(jsonPath("$.[0].id").value(journey.getId().intValue()))
            .andExpect(jsonPath("$.[0].title").value(JOURNEY_TITLE))
            .andExpect(jsonPath("$.[0].description").value(JOURNEY_DESCRIPTION))
            .andExpect(jsonPath("$.[0].videoConferenceUrl").value(JOURNEY_VIDEO_CONFERENCE_URL))
            .andExpect(jsonPath("$.[0].studentSignupCode").value(STUDENT_JOURNEY_SIGN_UP_CODE))
            .andExpect(jsonPath("$.[0].teacherSignupCode").value(TEACHER_JOURNEY_SIGN_UP_CODE))
            .andExpect(jsonPath("$.[0].startDate").value(JOURNEY_START_DATE.toString()))
            .andExpect(jsonPath("$.[0].endDate").value(JOURNEY_END_DATE.toString()))
            .andExpect(jsonPath("$.[0].template").value(JOURNEY_TEMPLATE))
            .andExpect(jsonPath("$.[0].schools[0].name").value(SCHOOL_NAME))
            .andExpect(jsonPath("$.[0].ltiConfigs.size()").value(1));
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void shouldGetAllJourneysAsTeacher() throws Exception {
        User teacher = getTeacher(entityManager);
        School school = createSchool();
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(teacher)
            .addTeacher(teacher)
            .addSchool(school);
        JourneyState journeyState = new JourneyState()
            .user(teacher)
            .journey(journey)
            .state(IN_PROGRESS)
            .authority(new Authority().name(TEACHER));
        journey.addState(journeyState);
        LtiConfig ltiConfig = createLtiConfig();
        ltiConfig.setType(JOURNEY);
        journey.setLtiConfigs(Set.of(ltiConfig));
        persistCreatedEntities(entityManager);

        mockMvc.perform(get("/api/journeys?sort=id,desc&state=IN_PROGRESS")
                .header("JOURNEY-JSON-VERSION", "1"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$", hasSize(1)))
            .andExpect(jsonPath("$.[0].id").value(journey.getId().intValue()))
            .andExpect(jsonPath("$.[0].title").value(JOURNEY_TITLE))
            .andExpect(jsonPath("$.[0].description").value(JOURNEY_DESCRIPTION))
            .andExpect(jsonPath("$.[0].videoConferenceUrl").value(JOURNEY_VIDEO_CONFERENCE_URL))
            .andExpect(jsonPath("$.[0].studentSignupCode").value(STUDENT_JOURNEY_SIGN_UP_CODE))
            .andExpect(jsonPath("$.[0].teacherSignupCode").value(TEACHER_JOURNEY_SIGN_UP_CODE))
            .andExpect(jsonPath("$.[0].startDate").value(JOURNEY_START_DATE.toString()))
            .andExpect(jsonPath("$.[0].endDate").value(JOURNEY_END_DATE.toString()))
            .andExpect(jsonPath("$.[0].template").value(JOURNEY_TEMPLATE))
            .andExpect(jsonPath("$.[0].schools[0].name").value(SCHOOL_NAME))
            .andExpect(jsonPath("$.[0].ltiConfigs.size()").value(1));
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void shouldGetById() throws Exception {
        Journey journey = createJourney()
            .addStudent(getStudent(entityManager))
            .educationalLevel(createEducationalLevel())
            .creator(getTeacher(entityManager));
        LtiConfig ltiConfig = createLtiConfig();
        ltiConfig.setType(JOURNEY);

        journey.setLtiConfigs(Set.of(ltiConfig));

        persistCreatedEntities(entityManager);

        mockMvc.perform(get("/api/journeys/{id}", journey.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(journey.getId().intValue()))
            .andExpect(jsonPath("$.title").value(JOURNEY_TITLE))
            .andExpect(jsonPath("$.description").value(JOURNEY_DESCRIPTION))
            .andExpect(jsonPath("$.videoConferenceUrl").value(JOURNEY_VIDEO_CONFERENCE_URL))
            .andExpect(jsonPath("$.studentSignupCode").doesNotExist())
            .andExpect(jsonPath("$.teacherSignupCode").doesNotExist())
            .andExpect(jsonPath("$.startDate").value(JOURNEY_START_DATE.toString()))
            .andExpect(jsonPath("$.endDate").value(JOURNEY_END_DATE.toString()))
            .andExpect(jsonPath("$.template").value(JOURNEY_TEMPLATE))
            .andExpect(jsonPath("$.ltiConfigs.size()").value(1));
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void shouldThrowWhenNotFound() throws Exception {
        mockMvc.perform(get("/api/journeys/{id}", MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldUpdate() throws Exception {
        School school = createSchool();
        Journey journey = createJourney().educationalLevel(createEducationalLevel());

        transactionHelper.withNewTransaction(() -> {
            User teacher = getTeacher(entityManager);
            journey
                .creator(teacher)
                .addTeacher(teacher);
            persistCreatedEntities(entityManager);
        });

        JourneyDTO journeyDTO = journeyMapper.toDto(journey)
            .setTitle("Updated journey title")
            .setDescription("Updated journey description")
            .setVideoConferenceUrl("http://localhost:8080/updated-conference-url")
            .setStudentSignupCode("Updated sign up code")
            .setTemplate(!journey.getTemplate())
            .setStartDate(journey.getStartDate().plus(1, DAYS))
            .setEndDate(journey.getEndDate().plus(1, DAYS))
            .setSchools(Set.of(schoolMapper.toDto(school)));

        mockMvc.perform(put("/api/journeys")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(journeyDTO)))
            .andExpect(status().isOk());

        transactionHelper.withNewTransaction(() -> {
            List<Journey> journeyList = journeyJpaRepository.findAll();
            assertThat(journeyList).hasSize(1);
            Journey persisted = journeyList.get(0);
            assertThat(persisted.getTitle()).isEqualTo(journeyDTO.getTitle());
            assertThat(persisted.getDescription()).isEqualTo(journeyDTO.getDescription());
            assertThat(persisted.getVideoConferenceUrl()).isEqualTo(journeyDTO.getVideoConferenceUrl());
            assertThat(persisted.getTemplate()).isEqualTo(journeyDTO.isTemplate());
            assertThat(persisted.getStartDate()).isEqualTo(journeyDTO.getStartDate());
            assertThat(persisted.getEndDate()).isEqualTo(journeyDTO.getEndDate());
            // sign up code is generated on create and can not be updated
            assertThat(persisted.getSignupCode(STUDENT)).isEqualTo(STUDENT_JOURNEY_SIGN_UP_CODE);
            assertThat(persisted.getSchools()).isEqualTo(Set.of(school));
        });
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void shouldFailToUpdateWhenJourneyDoesNotExist() throws Exception {
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(getTeacher(entityManager));

        mockMvc.perform(put("/api/journeys")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(journeyMapper.toDto(journey))))
            .andExpect(status().isBadRequest());

        assertThat(journeyJpaRepository.findAll()).isEmpty();
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldDelete() throws Exception {
        final Journey journey = createJourney();
        final Milestone milestone = createMilestone();
        final EducationalAlignment educationalAlignment = createEducationalAlignment();
        final Chat chat = createChat();
        final Notification notification = createNotification()
            .recipient(getTeacher(entityManager));
        LtiConfig ltiConfig = createLtiConfig();
        ltiConfig.setType(JOURNEY);
        final EducationalLevel educationalLevel = createEducationalLevel();
        transactionHelper.withNewTransaction(() -> {
            final User teacher = getTeacher(entityManager);
            journey.addStudent(getStudent(entityManager))
                .addTeacher(teacher)
                .addMilestone(milestone
                    .creator(teacher))
                .addLtiConfig(ltiConfig)
                .educationalLevel(educationalLevel)
                .creator(teacher)
                .addNotification(notification)
                .addChat(chat);
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(delete("/api/journeys/{id}", journey.getId())
                .accept(APPLICATION_JSON))
            .andExpect(status().isNoContent());

        transactionHelper.withNewTransaction(() -> {
            // entities that should be soft deleted
            assertNotNull(entityManager.createNativeQuery("SELECT deleted FROM journey WHERE id = ?").setParameter(1, journey.getId()).getSingleResult());
            assertNotNull(entityManager.createNativeQuery("SELECT deleted FROM milestone WHERE id = ?").setParameter(1, milestone.getId()).getSingleResult());
            assertNotNull(entityManager.createNativeQuery("SELECT deleted FROM chat WHERE id = ?").setParameter(1, chat.getId()).getSingleResult());
            assertNotNull(entityManager.createNativeQuery("SELECT deleted FROM notification WHERE id = ?").setParameter(1, notification.getId()).getSingleResult());
            assertNotNull(entityManager.createNativeQuery("SELECT deleted FROM lti_config WHERE id = ?").setParameter(1, ltiConfig.getId()).getSingleResult());

            // entities that should only have relation deleted
            assertEquals(0, getTeacher(entityManager).getCreatedJourneys().size());
            assertNotNull(entityManager.find(EducationalLevel.class, educationalLevel.getId()));
        });
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldReturnForbiddenDeletingWhileNotCreator() throws Exception {
        final Journey journey = createJourney();
        final Milestone milestone = createMilestone();
        createEducationalAlignment();
        final Chat chat = createChat();
        final Notification notification = createNotification()
            .recipient(getTeacher(entityManager));
        LtiConfig ltiConfig = createLtiConfig();
        ltiConfig.setType(JOURNEY);
        final EducationalLevel educationalLevel = createEducationalLevel();
        transactionHelper.withNewTransaction(() -> {
            final User teacher = getTeacher(entityManager);
            journey.addStudent(getStudent(entityManager))
                .addTeacher(teacher)
                .addMilestone(milestone
                    .creator(teacher))
                .addLtiConfig(ltiConfig)
                .educationalLevel(educationalLevel)
                .creator(getStudent(entityManager))
                .addNotification(notification)
                .addChat(chat);
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(delete("/api/journeys/{id}", journey.getId())
                .accept(APPLICATION_JSON))
            .andExpect(status().isForbidden());
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void shouldAllowToJoinWithSignUpCodeAndCreateAlert() throws Exception {
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(getTeacher(entityManager));
        persistCreatedEntities(entityManager);

        assertThat(journey.getStudents()).isEmpty();

        mockMvc.perform(post("/api/journeys/join")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(new JoinJourneyDTO().signUpCode(journey.getSignupCode(STUDENT))))
                .accept(APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(header()
                .stringValues("X-schoolabyApp-alert", "schoolabyApp.journey.students.created")
            );

        assertThat(journey.getStudents()).hasSize(1);
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void shouldReturn404WhenJoiningWithInvalidSignUpCode() throws Exception {
        mockMvc.perform(post("/api/journeys/join")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(new JoinJourneyDTO().signUpCode("qwerty123123")))
                .accept(APPLICATION_JSON))
            .andExpect(status().isBadRequest())
            .andExpect(content().contentType(APPLICATION_PROBLEM_JSON))
            .andExpect(jsonPath("$.detail").value(containsString("qwerty123123")));
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void shouldAllowToJoinWithSignUpCodeWhenUserIsTeacherInSpecifiedJourney() throws Exception {
        User student = getStudent(entityManager);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(getTeacher(entityManager))
            .addTeacher(student);
        persistCreatedEntities(entityManager);

        assertThat(journey.getStudents()).isEmpty();

        mockMvc.perform(post("/api/journeys/join")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(new JoinJourneyDTO().signUpCode(journey.getSignupCode(STUDENT))))
                .accept(APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(header()
                .stringValues("X-schoolabyApp-alert", "schoolabyApp.journey.students.created"));

        assertThat(journey.getStudents()).hasSize(1);
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void shouldNotCreateEntityCreationAlertWhenStudentHasAlreadyJoined() throws Exception {
        User student = getStudent(entityManager);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(getTeacher(entityManager))
            .addTeacher(getTeacher(entityManager))
            .addStudent(student);
        persistCreatedEntities(entityManager);

        mockMvc.perform(post("/api/journeys/join")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(new JoinJourneyDTO().signUpCode(journey.getSignupCode(STUDENT))))
                .accept(APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(header().doesNotExist("X-schoolabyApp-alert"));

        assertThat(journey.getStudents()).hasSize(1);
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void shouldNotCreateEntityCreationAlertWhenTeacherHasAlreadyJoined() throws Exception {
        User teacher = getTeacher(entityManager);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(teacher)
            .addTeacher(teacher);
        persistCreatedEntities(entityManager);

        mockMvc.perform(post("/api/journeys/join")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(new JoinJourneyDTO().signUpCode(journey.getSignupCode(TEACHER))))
                .accept(APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(header().doesNotExist("X-schoolabyApp-alert"));

        assertThat(journey.getTeachers()).hasSize(1);
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void shouldThrowBadRequestWithAlertWhenJoiningWithAlreadyUsedOneTimeSignupCode() throws Exception {
        User teacher = getTeacher(entityManager);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(teacher)
            .addTeacher(teacher);
        journey.setJourneySignupCodes(Set.of(new JourneySignupCode()
            .journey(journey)
            .signUpCode("random")
            .setOneTime(true)
            .setUsed(true)
            .authority(new Authority().name(STUDENT))));
        persistCreatedEntities(entityManager);

        mockMvc.perform(post("/api/journeys/join")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(new JoinJourneyDTO().signUpCode("random")))
                .accept(APPLICATION_JSON))
            .andExpect(status().isBadRequest())
            .andExpect(header().stringValues("X-schoolabyApp-error", "error.signupCodeAlreadyUsed"));

        assertThat(journey.getTeachers()).hasSize(1);
    }

    @Test
    @WithUserDetails(STUDENT_LOGIN)
    void shouldHideHiddenMilestonesAndAssignmentsFromStudent() throws Exception {
        User teacher = getTeacher(entityManager);
        User student = getStudent(entityManager);
        Journey journey = createJourney();
        transactionHelper.withNewTransaction(() -> {
            JourneyState journeyState = new JourneyState()
                .state(IN_PROGRESS)
                .journey(journey)
                .user(student)
                .authority(new Authority().name(STUDENT));
            journey.creator(teacher)
                .educationalLevel(createEducationalLevel())
                .addState(journeyState)
                .addMilestone(createMilestone()
                    .creator(teacher)
                    .addAssignment(createAssignment()
                        .creator(teacher)
                        .gradingScheme(getGradingSchemeNumerical(entityManager)))
                    .addAssignment(createAssignment()
                        .creator(teacher)
                        .gradingScheme(getGradingSchemeNumerical(entityManager))
                        .published(null)))
                .addMilestone(createMilestone()
                    .published(null)
                    .addAssignment(createAssignment()
                        .creator(teacher)
                        .gradingScheme(getGradingSchemeNumerical(entityManager)))
                    .creator(teacher))
                .addStudent(getStudent(entityManager));
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(get("/api/journeys?state=IN_PROGRESS")
                .header("JOURNEY-JSON-VERSION", "1"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("length()").value(1));
        mockMvc.perform(get("/api/journeys/{id}", journey.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldNotHideHiddenMilestonesAndAssignmentsFromTeacher() throws Exception {
        Journey journey = createJourney();
        transactionHelper.withNewTransaction(() -> {
            User teacher = getTeacher(entityManager);
            JourneyState journeyState = new JourneyState()
                .state(IN_PROGRESS)
                .journey(journey)
                .user(teacher)
                .authority(new Authority().name(TEACHER));
            journey.creator(teacher)
                .addTeacher(teacher)
                .educationalLevel(createEducationalLevel())
                .addState(journeyState)
                .addMilestone(createMilestone()
                    .creator(teacher)
                    .published(null)
                    .addAssignment(createAssignment()
                        .creator(teacher)
                        .gradingScheme(getGradingSchemeNumerical(entityManager))
                        .published(null)));
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(get("/api/journeys?state=IN_PROGRESS")
                .header("JOURNEY-JSON-VERSION", "1"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("length()").value(1));
        mockMvc.perform(get("/api/journeys/{id}", journey.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE));
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void shouldReturnNewAssignmentStateAfterCreatingSubmission() throws Exception {
        User student = getStudent(entityManager);
        User teacher = getTeacher(entityManager);
        Journey journey = createJourney();
        Milestone milestone = createMilestone();
        Assignment assignment = createAssignment();
        journey
            .educationalLevel(createEducationalLevel())
            .creator(teacher)
            .addTeacher(teacher)
            .addStudent(student)
            .addMilestone(milestone
                .journey(journey)
                .creator(teacher)
                .addAssignment(assignment
                    .creator(teacher)
                    .gradingScheme(getGradingSchemeNumerical(entityManager))));

        JourneyState journeyState = new JourneyState()
            .user(student)
            .journey(journey)
            .state(IN_PROGRESS)
            .authority(new Authority().name(STUDENT));
        journey.addState(journeyState);
        persistCreatedEntities(entityManager);

        mockMvc.perform(get("/api/journeys/{id}", journey.getId())
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.state").value(IN_PROGRESS.toString()));

        Submission submission = createSubmission()
            .assignment(assignment)
            .addAuthor(student);
        mockMvc.perform(post("/api/submissions")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(submissionMapper.toDto(submission))))
            .andExpect(status().isCreated());

        mockMvc.perform(get("/api/journeys/{id}", journey.getId())
                .contentType(APPLICATION_JSON).accept(APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.state").value(IN_PROGRESS.toString()));
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void shouldThrowOnGetJourneyWhenTeacherNotInJourney() throws Exception {
        User teacher = getTeacher(entityManager);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(teacher);
        LtiConfig ltiConfig = createLtiConfig();
        ltiConfig.setType(JOURNEY);

        journey.setLtiConfigs(Set.of(ltiConfig));

        persistCreatedEntities(entityManager);

        mockMvc.perform(get("/api/journeys/{id}", journey.getId()))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void shouldGetJourneysByTemplateWhenTeacherNotInJourney() throws Exception {
        User teacher = getTeacher(entityManager);
        teacher.setCountry("Estonia");
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(teacher);
        LtiConfig ltiConfig = createLtiConfig();
        ltiConfig.setType(JOURNEY);

        journey.setLtiConfigs(Set.of(ltiConfig));

        persistCreatedEntities(entityManager);

        mockMvc.perform(get("/api/journeys?sort=id,desc&template=true")
                .header("JOURNEY-JSON-VERSION", "1"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$", hasSize(1)))
            .andExpect(jsonPath("$.[0].id").value(journey.getId().intValue()))
            .andExpect(jsonPath("$.[0].title").value(JOURNEY_TITLE))
            .andExpect(jsonPath("$.[0].description").value(JOURNEY_DESCRIPTION))
            .andExpect(jsonPath("$.[0].videoConferenceUrl").value(JOURNEY_VIDEO_CONFERENCE_URL))
            .andExpect(jsonPath("$.[0].studentSignupCode").doesNotExist())
            .andExpect(jsonPath("$.[0].startDate").value(JOURNEY_START_DATE.toString()))
            .andExpect(jsonPath("$.[0].endDate").value(JOURNEY_END_DATE.toString()))
            .andExpect(jsonPath("$.[0].template").value(JOURNEY_TEMPLATE))
            .andExpect(jsonPath("$.[0].ltiConfigs.size()").value(1));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldGetCreatedJourneysAsTemplates() throws Exception {
        User teacher = getTeacher(entityManager);
        User otherTeacher = createTeacher()
            .setLogin("teacher.second")
            .setCountry("Estonia");
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .template(false)
            .creator(teacher);
        Journey otherTeacherJourney = createJourney()
            .educationalLevel(createEducationalLevel())
            .title("Other journey")
            .creator(otherTeacher)
            .setJourneySignupCodes(emptySet());
        LtiConfig ltiConfig = createLtiConfig();
        ltiConfig.setType(JOURNEY);

        journey.setLtiConfigs(Set.of(ltiConfig));

        transactionHelper.withNewTransaction(() -> persistCreatedEntities(entityManager));

        mockMvc.perform(get("/api/journeys?sort=id,desc&template=true")
                .header("JOURNEY-JSON-VERSION", "1"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$", hasSize(2)))
            .andExpect(jsonPath("$.[*].title").value(containsInAnyOrder(journey.getTitle(), otherTeacherJourney.getTitle())))
            .andExpect(jsonPath("$.[*].template").value(containsInAnyOrder(true, false)));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldNotGetOtherTeachersJourneysAsTemplatesWhenTemplateIsSetToFalse() throws Exception {
        User teacher = getTeacher(entityManager);
        User otherTeacher = createTeacher()
            .setLogin("teacher.second")
            .setCountry("Estonia");
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .template(false)
            .creator(teacher);
        Journey otherTeacherJourney = createJourney()
            .educationalLevel(createEducationalLevel())
            .title("Other journey")
            .template(false)
            .creator(otherTeacher)
            .setJourneySignupCodes(emptySet());
        LtiConfig ltiConfig = createLtiConfig();
        ltiConfig.setType(JOURNEY);

        journey.setLtiConfigs(Set.of(ltiConfig));

        transactionHelper.withNewTransaction(() -> persistCreatedEntities(entityManager));

        mockMvc.perform(get("/api/journeys?sort=id,desc&template=true")
                .header("JOURNEY-JSON-VERSION", "1"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$", hasSize(1)))
            .andExpect(jsonPath("$.[0].title").value(journey.getTitle()));
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void shouldThrowOnGetJourneyWhenStudentNotInJourney() throws Exception {
        User teacher = getTeacher(entityManager);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(teacher);
        LtiConfig ltiConfig = createLtiConfig();
        ltiConfig.setType(JOURNEY);
        journey.setLtiConfigs(Set.of(ltiConfig));
        persistCreatedEntities(entityManager);

        mockMvc.perform(get("/api/journeys/{id}", journey.getId()))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void shouldGetEmptyResponseOnGetAllJourneysWhenStudentNotInJourney() throws Exception {
        User teacher = getTeacher(entityManager);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(teacher);
        LtiConfig ltiConfig = createLtiConfig();
        ltiConfig.setType(JOURNEY);
        journey.setLtiConfigs(Set.of(ltiConfig));

        persistCreatedEntities(entityManager);

        mockMvc.perform(get("/api/journeys?sort=id,desc&state=IN_PROGRESS")
                .header("JOURNEY-JSON-VERSION", "1"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("length()").value(0));
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void shouldGetEmptyResponseOnGetAllJourneysWhenTeacherNotInJourney() throws Exception {
        User teacher = getTeacher(entityManager);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(teacher);
        LtiConfig ltiConfig = createLtiConfig();
        ltiConfig.setType(JOURNEY);
        journey.setLtiConfigs(Set.of(ltiConfig));

        persistCreatedEntities(entityManager);

        mockMvc.perform(get("/api/journeys?sort=id,desc&state=IN_PROGRESS")
                .header("JOURNEY-JSON-VERSION", "1"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("length()").value(0));
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void shouldThrowOnUpdateWhenTeacherNotInJourney() throws Exception {
        User teacher = getTeacher(entityManager);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(teacher);
        persistCreatedEntities(entityManager);
        entityManager.detach(journey);

        journey
            .title("Updated journey title")
            .description("Updated journey description")
            .videoConferenceUrl("http://localhost:8080/updated-conference-url")
            .setJourneySignupCodes(Set.of(
                new JourneySignupCode()
                    .authority(new Authority().name(STUDENT))
                    .journey(journey)
                    .signUpCode("Updated sign up code")
            ))
            .template(!journey.getTemplate())
            .startDate(journey.getStartDate().plus(1, DAYS))
            .endDate(journey.getEndDate().plus(1, DAYS));

        mockMvc.perform(put("/api/journeys")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(journeyMapper.toDto(journey))))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void shouldThrowOnUpdateWhenJourneyNotPresent() throws Exception {
        User teacher = getTeacher(entityManager);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(teacher);
        journey.setId(1L);
        entityManager.detach(journey);

        journey
            .title("Updated journey title")
            .description("Updated journey description")
            .videoConferenceUrl("http://localhost:8080/updated-conference-url")
            .setJourneySignupCodes(Set.of(
                new JourneySignupCode()
                    .authority(new Authority().name(STUDENT))
                    .journey(journey)
                    .signUpCode("Updated sign up code")
            ))
            .template(!journey.getTemplate())
            .startDate(journey.getStartDate().plus(1, DAYS))
            .endDate(journey.getEndDate().plus(1, DAYS));

        mockMvc.perform(put("/api/journeys")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(journeyMapper.toDto(journey))))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void shouldThrowOnDeleteWhenTeacherNotInJourney() throws Exception {
        User teacher = getTeacher(entityManager);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(teacher);
        persistCreatedEntities(entityManager);

        mockMvc.perform(delete("/api/journeys/{id}", journey.getId())
                .accept(APPLICATION_JSON))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void shouldThrowOnDeleteWhenJourneyNotPresent() throws Exception {
        mockMvc.perform(delete("/api/journeys/{id}", 1)
                .accept(APPLICATION_JSON))
            .andExpect(status().isNotFound());
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldGetStudents() throws Exception {
        User student = createStudent().setLogin("student2");
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel());

        transactionHelper.withNewTransaction(() -> {
            User teacher = getTeacher(entityManager);
            journey
                .creator(teacher)
                .addTeacher(teacher)
                .addStudent(student)
                .addStudent(getStudent(entityManager));
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(get(format("/api/journeys/%d/students", journey.getId())))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(2)))
            .andExpect(jsonPath("$.[*].user.firstName").value(containsInAnyOrder("Student", student.getFirstName())));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldThrowOnGettingStudentsWhenUserNotATeacherInTheJourney() throws Exception {
        User teacher = createTeacher();
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(teacher)
            .addTeacher(teacher);

        transactionHelper.withNewTransaction(() -> {
            journey
                .addStudent(getStudent(entityManager))
                .addStudent(getTeacher(entityManager));
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(get(format("/api/journeys/%d/students", journey.getId())))
            .andExpect(status().isForbidden())
            .andExpect(jsonPath("$.detail").value("403 FORBIDDEN \"Current user is not a teacher in the journey!\""));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldPinStudent() throws Exception {
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel());

        transactionHelper.withNewTransaction(() -> {
            User teacher = getTeacher(entityManager);
            journey
                .creator(teacher)
                .addTeacher(teacher)
                .addStudent(getStudent(entityManager));
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(put(format("/api/journeys/%d/students/%d/pin", journey.getId(), STUDENT_ID)))
            .andExpect(status().isNoContent());

        transactionHelper.withNewTransaction(() -> {
            JourneyUserKey journeyUserKey = new JourneyUserKey().setJourney(journey.getId()).setUser(STUDENT_ID);
            JourneyStudent journeyStudent = entityManager.find(JourneyStudent.class, journeyUserKey);
            assertNotNull(journeyStudent.getPinnedDate());
        });
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldUnPinStudent() throws Exception {
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel());

        transactionHelper.withNewTransaction(() -> {
            User teacher = getTeacher(entityManager);
            JourneyStudent journeyStudent = new JourneyStudent()
                .journey(journey)
                .user(getStudent(entityManager))
                .setPinnedDate(Instant.now());
            journey
                .creator(teacher)
                .addTeacher(teacher)
                .addStudent(journeyStudent);
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(put(format("/api/journeys/%d/students/%d/pin", journey.getId(), STUDENT_ID)))
            .andExpect(status().isNoContent());

        transactionHelper.withNewTransaction(() -> {
            JourneyUserKey journeyUserKey = new JourneyUserKey().setJourney(journey.getId()).setUser(STUDENT_ID);
            JourneyStudent journeyStudent = entityManager.find(JourneyStudent.class, journeyUserKey);
            assertNull(journeyStudent.getPinnedDate());
        });
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldThrowOnPinningStudentWhenUserNotATeacherInJourney() throws Exception {
        User teacher = createTeacher();
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(teacher)
            .addTeacher(teacher);

        transactionHelper.withNewTransaction(() -> {
            journey
                .addStudent(getStudent(entityManager))
                .addStudent(getTeacher(entityManager));
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(put(format("/api/journeys/%d/students/%d/pin", journey.getId(), STUDENT_ID)))
            .andExpect(status().isForbidden())
            .andExpect(jsonPath("$.detail").value("403 FORBIDDEN \"Current user is not a teacher in the journey!\""));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldFindUserJourneysBySearch() throws Exception {
        User otherTeacher = createTeacher();
        EducationalLevel educationalLevel = createEducationalLevel();
        Journey userJourney = createJourney()
            .title("My journey")
            .educationalLevel(educationalLevel);
        Journey secondUserJourney = createJourney()
            .title("My other journey")
            .educationalLevel(educationalLevel)
            .setJourneySignupCodes(emptySet());
        Journey otherJourneyTemplate = createJourney()
            .template(true)
            .educationalLevel(educationalLevel)
            .setJourneySignupCodes(emptySet());
        Journey otherJourneyTemplateWithOtherSubject = createJourney()
            .title("Other subject journey")
            .template(true)
            .educationalLevel(educationalLevel)
            .setJourneySignupCodes(emptySet());

        transactionHelper.withNewTransaction(() -> {
            Subject firstSubject = getSubject(entityManager);
            Subject secondSubject = getSubjectById(entityManager, 2L);

            User teacher = getTeacher(entityManager);
            userJourney
                .creator(teacher)
                .addTeacher(teacher)
                .setSubject(firstSubject);
            secondUserJourney
                .creator(teacher)
                .addTeacher(teacher)
                .setSubject(firstSubject);
            otherJourneyTemplate
                .creator(otherTeacher)
                .addTeacher(otherTeacher)
                .setSubject(firstSubject);
            otherJourneyTemplateWithOtherSubject
                .creator(teacher)
                .addTeacher(teacher)
                .setSubject(secondSubject);
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(get("/api/journeys/")
                .header("JOURNEY-JSON-VERSION", "2")
                .param("userId", String.valueOf(TEACHER_ID))
                .param("template", "true")
                .param("search", "journey")
                .param("subjectId", String.valueOf(SUBJECT_ID)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(2)))
            .andExpect(jsonPath("$[*].title", containsInAnyOrder("My journey", "My other journey")));

        mockMvc.perform(get("/api/journeys/")
                .header("JOURNEY-JSON-VERSION", "2")
                .param("userId", String.valueOf(TEACHER_ID))
                .param("template", "true")
                .param("search", "other")
                .param("subjectId", "2"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(1)))
            .andExpect(jsonPath("$[0].title").value("Other subject journey"));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldFindAllJourneysBySearch() throws Exception {
        User otherTeacher = createTeacher().setCountry("Estonia");
        User otherCountryTeacher = createTeacher()
            .setCountry("Tanzania")
            .setLogin("tanzanian")
            .setEmail("tanzanian@example.com");
        EducationalLevel educationalLevel = createEducationalLevel();
        Journey userJourney = createJourney()
            .setTitle("My journey")
            .educationalLevel(educationalLevel)
            .setJourneySignupCodes(emptySet());
        Journey secondUserJourney = createJourney()
            .setTitle("My other journey")
            .educationalLevel(educationalLevel)
            .setJourneySignupCodes(emptySet());
        Journey otherTeacherJourneyTemplate = createJourney()
            .setTemplate(true)
            .educationalLevel(educationalLevel)
            .setJourneySignupCodes(emptySet());
        Journey otherJourneyTemplateWithOtherSubject = createJourney()
            .setTemplate(true)
            .educationalLevel(educationalLevel)
            .setJourneySignupCodes(emptySet());
        Journey otherCountryTemplateJourney = createJourney()
            .setTitle("Other country journey")
            .setTemplate(true)
            .educationalLevel(educationalLevel)
            .setJourneySignupCodes(emptySet());
        Journey otherNonTemplateJourney = createJourney()
            .setTitle("Not a journey template")
            .setTemplate(false)
            .educationalLevel(educationalLevel)
            .setJourneySignupCodes(emptySet());

        transactionHelper.withNewTransaction(() -> {
            Subject firstSubject = getSubject(entityManager);
            Subject secondSubject = getSubjectById(entityManager, 2L);
            User teacher = getTeacher(entityManager);
            userJourney
                .setCreator(teacher)
                .addTeacher(teacher)
                .setSubject(firstSubject);
            secondUserJourney
                .setCreator(teacher)
                .addTeacher(teacher)
                .setSubject(firstSubject);
            otherTeacherJourneyTemplate
                .setCreator(otherTeacher)
                .addTeacher(otherTeacher)
                .setSubject(firstSubject);
            otherJourneyTemplateWithOtherSubject
                .setCreator(otherTeacher)
                .addTeacher(otherTeacher)
                .setSubject(secondSubject);
            otherCountryTemplateJourney
                .setCreator(otherCountryTeacher)
                .addTeacher(otherCountryTeacher)
                .setSubject(firstSubject);
            otherNonTemplateJourney
                .setCreator(otherTeacher)
                .addTeacher(otherTeacher)
                .setSubject(firstSubject);
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(get("/api/journeys/")
                .param("template", "true")
                .param("search", "journey")
                .param("subjectId", "1")
                .header("JOURNEY-JSON-VERSION", "2"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(3)))
            .andExpect(jsonPath("$[*].title", containsInAnyOrder("My journey", "My other journey", JOURNEY_TITLE)))
            .andExpect(jsonPath("$[*].subject.id", contains(1, 1, 1)));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldGetTemplateJourney() throws Exception {
        User teacher = createTeacher();
        EducationalLevel educationalLevel = createEducationalLevel();
        Journey journey = createJourney()
            .setTemplate(true)
            .setEducationalLevel(educationalLevel)
            .setCreator(teacher)
            .addTeacher(teacher);

        transactionHelper.withNewTransaction(() -> persistCreatedEntities(entityManager));
        mockMvc.perform(get("/api/journeys/" + journey.getId())
                .param("template", "true"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.title").value(journey.getTitle()));

    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldNotGetTemplateJourneyWhenTemplateUndefinedInQuery() throws Exception {
        User teacher = createTeacher();
        EducationalLevel educationalLevel = createEducationalLevel();
        Journey journey = createJourney()
            .setTemplate(true)
            .setEducationalLevel(educationalLevel)
            .setCreator(teacher)
            .addTeacher(teacher);

        transactionHelper.withNewTransaction(() -> persistCreatedEntities(entityManager));
        mockMvc.perform(get("/api/journeys/" + journey.getId()))
            .andExpect(status().isNotFound());
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldNotGetOtherTeacherJourneyThatIsNotATemplate() throws Exception {
        User teacher = createTeacher();
        EducationalLevel educationalLevel = createEducationalLevel();
        Journey journey = createJourney()
            .setTemplate(false)
            .setEducationalLevel(educationalLevel)
            .setCreator(teacher)
            .addTeacher(teacher);

        transactionHelper.withNewTransaction(() -> persistCreatedEntities(entityManager));
        mockMvc.perform(get("/api/journeys/" + journey.getId())
                .param("template", "true"))
            .andExpect(status().isNotFound());
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldThrowErrorWhenSearchAndStateExistInJourneysQuery() throws Exception {
        mockMvc.perform(get("/api/journeys/")
                .param("template", "true")
                .param("search", "journey")
                .param("state", "COMPLETED")
                .header("JOURNEY-JSON-VERSION", "2"))
            .andExpect(status().isBadRequest());
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldRemoveTeacherFromJourney() throws Exception {
        User otherTeacher = createTeacher()
            .setLogin("otherTeacher")
            .setEmail("otherTeacher@localhost");
        EducationalLevel educationalLevel = createEducationalLevel();
        Journey journey = createJourney()
            .setTemplate(false)
            .setEducationalLevel(educationalLevel)
            .addTeacher(otherTeacher);

        transactionHelper.withTransaction(() -> {
            User teacher = getTeacher(entityManager);
            journey
                .setCreator(teacher)
                .addTeacher(teacher);

            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(delete(format("/api/journeys/%d/teachers/%d", journey.getId(), otherTeacher.getId())))
            .andExpect(status().isNoContent());

        transactionHelper.withNewTransaction(() -> {
            Journey receivedJourney = journeyJpaRepository.findById(journey.getId()).orElseThrow();
            assertEquals(1, receivedJourney.getTeachers().size());
            User journeyTeacher = receivedJourney
                .getTeachers()
                .stream()
                .findFirst()
                .orElseThrow()
                .getUser();
            assertEquals(TEACHER_LOGIN, journeyTeacher.getLogin());
        });
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldNotBeAllowedToRemoveTeachersFromJourneyIfTeacherIsNotTheCreator() throws Exception {
        User otherTeacher = createTeacher()
            .setLogin("otherTeacher")
            .setEmail("otherTeacher@localhost");
        EducationalLevel educationalLevel = createEducationalLevel();
        Journey journey = createJourney()
            .setTemplate(false)
            .setEducationalLevel(educationalLevel)
            .addTeacher(otherTeacher);

        transactionHelper.withTransaction(() -> {
            journey
                .setCreator(otherTeacher)
                .addTeacher(getTeacher(entityManager));

            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(delete(format("/api/journeys/%d/teachers/%d", journey.getId(), otherTeacher.getId())))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldNotBeAllowedToRemoveStudentsIfUserIsNotATeacherInTheJourney() throws Exception {
        User teacher = createTeacher().setLogin("journeyTeacher");
        User student = createStudent()
            .setLogin("journeyStudent")
            .setEmail("journeyStudent@localhost");
        EducationalLevel educationalLevel = createEducationalLevel();
        Journey journey = createJourney()
            .setTemplate(false)
            .setEducationalLevel(educationalLevel)
            .setCreator(teacher)
            .addStudent(student)
            .addTeacher(teacher);

        transactionHelper.withTransaction(() -> {
            journey.addStudent(getTeacher(entityManager));

            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(delete(format("/api/journeys/%d/students/%d", journey.getId(), student.getId())))
            .andExpect(status().isForbidden())
            .andExpect(jsonPath("$.detail").value("403 FORBIDDEN \"Current user is not a teacher in the journey!\""));
    }
}
