package com.schoolaby.web.rest;

import com.schoolaby.common.BaseIntegrationTest;
import com.schoolaby.domain.*;
import com.schoolaby.service.JoinJourneyDTO;
import com.schoolaby.service.dto.AssignmentDTO;
import com.schoolaby.service.dto.JourneyDTO;
import com.schoolaby.service.dto.MilestoneDTO;
import com.schoolaby.service.dto.submission.SubmissionFeedbackDTO;
import com.schoolaby.service.dto.submission.SubmissionGradingPatchDTO;
import com.schoolaby.service.mapper.*;
import com.schoolaby.web.WithMockCustomUser;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithUserDetails;

import java.time.Instant;
import java.util.List;
import java.util.Set;

import static com.schoolaby.common.TestCases.*;
import static com.schoolaby.common.TestObjects.*;
import static com.schoolaby.security.Role.Constants.*;
import static com.schoolaby.service.dto.states.EntityState.*;
import static com.schoolaby.web.rest.TestUtil.convertObjectToJsonBytes;
import static java.lang.String.format;
import static java.time.temporal.ChronoUnit.DAYS;
import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockCustomUser(authorities = {TEACHER})
public class JourneyStateIT extends BaseIntegrationTest {
    private static final String API_JOURNEYS = "/api/journeys";
    private static final String API_JOURNEYS_JOIN = "/api/journeys/join";
    private static final String API_MILESTONES = "/api/milestones";
    private static final String API_ASSIGNMENTS = "/api/assignments";
    private static final String API_SUBMISSIONS = "/api/submissions";
    private static final String API_SUBMISSION_FEEDBACKS = "/api/submission-feedback";

    @Autowired
    private JourneyMapper journeyMapper;
    @Autowired
    private MilestoneMapper milestoneMapper;
    @Autowired
    private AssignmentMapper assignmentMapper;
    @Autowired
    private SubmissionMapper submissionMapper;
    @Autowired
    private SubmissionFeedbackMapper submissionFeedbackMapper;

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldBeNotStartedWhenJourneyCreated() throws Exception {
        EducationalLevel educationalLevel = createEducationalLevel();
        transactionHelper.withNewTransaction(() -> persistCreatedEntities(entityManager));
        Journey journey = createJourney()
            .startDate(Instant.now().plus(1L, DAYS))
            .endDate(Instant.now().plus(2L, DAYS));
        transactionHelper.withNewTransaction(() -> {
            User teacher = getTeacher(entityManager);
            Subject subject = getSubject(entityManager);
            journey.educationalLevel(educationalLevel)
                .creator(teacher)
                .setSubject(subject);
        });

        mockMvc.perform(post(API_JOURNEYS)
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(journeyMapper.toDto(journey), true)))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.state").value(NOT_STARTED.toString()));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldBeInProgressWhenJourneyUpdated() throws Exception {
        EducationalLevel educationalLevel = createEducationalLevel();
        Journey journey = createJourney()
            .startDate(Instant.now().plus(1L, DAYS))
            .endDate(Instant.now().plus(2L, DAYS));
        transactionHelper.withNewTransaction(() -> {
            User teacher = getTeacher(entityManager);
            Subject subject = getSubject(entityManager);
            journey.educationalLevel(educationalLevel)
                .creator(teacher)
                .setSubject(subject)
                .addTeacher(teacher);
            persistCreatedEntities(entityManager);
        });

        JourneyDTO journeyDTO = journeyMapper.toDto(journey)
            .setStartDate(Instant.now().minus(1L, DAYS));

        mockMvc.perform(put(API_JOURNEYS)
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(journeyDTO, true)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.state").value(IN_PROGRESS.toString()));
    }

    @Test
    @WithMockCustomUser(authorities = {STUDENT}, userId = "-3")
    @WithUserDetails(STUDENT_LOGIN)
    void shouldHaveTwoStatesWhenStudentJoinsJourney() throws Exception {
        EducationalLevel educationalLevel = createEducationalLevel();
        Journey journey = createJourney()
            .startDate(Instant.now().plus(1L, DAYS))
            .endDate(Instant.now().plus(2L, DAYS));
        transactionHelper.withNewTransaction(() -> {
            User teacher = getTeacher(entityManager);
            Subject subject = getSubject(entityManager);
            journey.educationalLevel(educationalLevel)
                .creator(teacher)
                .setSubject(subject)
                .addTeacher(teacher);
            persistCreatedEntities(entityManager);
        });

        JoinJourneyDTO joinJourneyDTO = new JoinJourneyDTO().signUpCode(journey.getSignupCode(STUDENT));

        mockMvc.perform(post(API_JOURNEYS_JOIN)
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(joinJourneyDTO, true)))
            .andExpect(status().isOk());

        transactionHelper.withNewTransaction(() -> {
            List<JourneyState> result = entityManager.createNativeQuery("SELECT * FROM journey_state WHERE journey_id = ?", JourneyState.class)
                .setParameter(1, journey.getId())
                .getResultList();
            assertEquals(2, result.size());
            assertEquals(List.of(NOT_STARTED, NOT_STARTED), result.stream().map(JourneyState::getState).collect(toList()));
        });
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldHaveStudentStateRemovedWhenStudentRemovedFromJourney() throws Exception {
        EducationalLevel educationalLevel = createEducationalLevel();
        Journey journey = createJourney()
            .startDate(Instant.now().plus(1L, DAYS))
            .endDate(Instant.now().plus(2L, DAYS));
        transactionHelper.withNewTransaction(() -> {
            User teacher = getTeacher(entityManager);
            User student = getStudent(entityManager);
            Subject subject = getSubject(entityManager);
            JourneyState teacherState = new JourneyState()
                .journey(journey)
                .user(teacher)
                .state(NOT_STARTED)
                .authority(new Authority().name(TEACHER));
            JourneyState studentState = new JourneyState()
                .journey(journey)
                .user(student)
                .state(NOT_STARTED)
                .authority(new Authority().name(STUDENT));

            journey.educationalLevel(educationalLevel)
                .creator(teacher)
                .setSubject(subject)
                .addTeacher(teacher)
                .addStudent(student)
                .addState(teacherState)
                .addState(studentState);
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(delete(format("%s/%s/students/-3", API_JOURNEYS, journey.getId()))
                .contentType(APPLICATION_JSON))
            .andExpect(status().isNoContent());

        transactionHelper.withNewTransaction(() -> {
            int journeyStatesSize = entityManager.createNativeQuery("SELECT * FROM journey_state WHERE journey_id = ?")
                .setParameter(1, journey.getId())
                .getResultList().size();
            assertEquals(1, journeyStatesSize);
        });
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldHaveJourneyStatesDeletedWhenJourneyDeleted() throws Exception {
        EducationalLevel educationalLevel = createEducationalLevel();
        Journey journey = createJourney()
            .startDate(Instant.now().plus(1L, DAYS))
            .endDate(Instant.now().plus(2L, DAYS));
        transactionHelper.withNewTransaction(() -> {
            User teacher = getTeacher(entityManager);
            Subject subject = getSubject(entityManager);
            journey.educationalLevel(educationalLevel)
                .creator(teacher)
                .setSubject(subject)
                .addTeacher(teacher);
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(delete(format("%s/%s", API_JOURNEYS, journey.getId()))
                .contentType(APPLICATION_JSON))
            .andExpect(status().isNoContent());

        transactionHelper.withNewTransaction(() -> {
            int resultSize = entityManager.createNativeQuery("SELECT * FROM journey_state WHERE journey_id = ?")
                .setParameter(1, journey.getId())
                .getResultList().size();
            assertEquals(0, resultSize);
        });
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldBeNotStartedWhenMilestoneCreated() throws Exception {
        EducationalLevel educationalLevel = createEducationalLevel();
        EducationalAlignment educationalAlignment = createEducationalAlignment();
        Journey journey = createJourney()
            .startDate(Instant.now().plus(1L, DAYS))
            .endDate(Instant.now().plus(2L, DAYS));
        transactionHelper.withNewTransaction(() -> {
            User teacher = getTeacher(entityManager);
            Subject subject = getSubject(entityManager);
            JourneyState journeyState = new JourneyState()
                .journey(journey)
                .user(teacher)
                .state(NOT_STARTED)
                .authority(new Authority().name(TEACHER));
            journey.educationalLevel(educationalLevel)
                .creator(teacher)
                .setSubject(subject)
                .addTeacher(teacher)
                .addState(journeyState);
            persistCreatedEntities(entityManager);
        });

        Milestone milestone = createMilestone()
            .addEducationalAlignment(educationalAlignment)
            .endDate(Instant.now().plus(2L, DAYS))
            .journey(journey)
            .creator(getTeacher(entityManager));

        mockMvc.perform(post(API_MILESTONES)
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(milestoneMapper.toDto(milestone), true)))
            .andExpect(status().isCreated());

        transactionHelper.withNewTransaction(() -> {
            Set<JourneyState> states = entityManager
                .find(Journey.class, milestone
                    .getJourney()
                    .getId())
                .getStates();
            assertEquals(1, states.size());
            assertTrue(states
                .stream()
                .allMatch(result -> result
                    .getState()
                    .equals(NOT_STARTED)));
        });
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldBeInProgressWhenMilestoneUpdated() throws Exception {
        Milestone milestone = createMilestoneTree();
        transactionHelper.withNewTransaction(() -> {
            User student = getStudent(entityManager);
            milestone.getJourney()
                .addStudent(student)
                .addTeacher(getTeacher(entityManager));
            milestone.setStates(Set.of(createMilestoneState().setUser(student).milestone(milestone).authority(new Authority().name(STUDENT))));
            persistCreatedEntities(entityManager);
        });
        MilestoneDTO milestoneDTO = milestoneMapper.toDto(milestone)
            .setEndDate(Instant.now().minus(1L, DAYS));

        mockMvc.perform(put(API_MILESTONES)
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(milestoneDTO, true)))
            .andExpect(status().isOk());

        transactionHelper.withNewTransaction(() -> {
            Set<JourneyState> states = entityManager
                .find(Journey.class, milestone
                    .getJourney()
                    .getId())
                .getStates();
            assertEquals(2, states.size());
            assertTrue(states
                .stream()
                .allMatch(result -> result
                    .getState()
                    .equals(IN_PROGRESS)));
        });
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldBeInProgressWhenMilestoneDeleted() throws Exception {
        Milestone milestone = createMilestoneTree();
        transactionHelper.withNewTransaction(() -> {
            milestone.getJourney()
                .addStudent(getStudent(entityManager))
                .addTeacher(getTeacher(entityManager));
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(delete(format("%s/%s", API_MILESTONES, milestone.getId()))
                .contentType(APPLICATION_JSON))
            .andExpect(status().isNoContent());

        transactionHelper.withNewTransaction(() -> {
            Set<JourneyState> states = entityManager
                .find(Journey.class, milestone
                    .getJourney()
                    .getId())
                .getStates();
            assertEquals(2, states.size());
            assertTrue(states
                .stream()
                .allMatch(result -> result
                    .getState()
                    .equals(IN_PROGRESS)));
        });
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldBeInProgressWhenAssignmentCreated() throws Exception {
        Milestone milestone = createMilestoneTree();
        transactionHelper.withNewTransaction(() -> {
            milestone.getJourney()
                .addStudent(getStudent(entityManager))
                .addTeacher(getTeacher(entityManager));
            persistCreatedEntities(entityManager);
        });
        Assignment assignment = createAssignment()
            .requiresSubmission(true)
            .creator(getTeacher(entityManager))
            .gradingScheme(getGradingSchemeNumerical(entityManager))
            .milestone(milestone);

        transactionHelper.withNewTransaction(() -> {
            Set<JourneyState> states = entityManager
                .find(Journey.class, milestone
                    .getJourney()
                    .getId())
                .getStates();
            assertEquals(0, states.size());
        });

        AssignmentDTO assignmentDTO = assignmentMapper.toDto(assignment);
        mockMvc.perform(post(API_ASSIGNMENTS)
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(assignmentDTO, true)))
            .andExpect(status().isCreated());

        transactionHelper.withNewTransaction(() -> {
            Set<JourneyState> states = entityManager
                .find(Journey.class, milestone
                    .getJourney()
                    .getId())
                .getStates();
            assertEquals(2, states.size());
            assertTrue(states
                .stream()
                .allMatch(result -> result
                    .getState()
                    .equals(IN_PROGRESS)));
        });
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldBeCompletedWhenAssignmentUpdatedAndAssignmentStateIsCompleted() throws Exception {
        Milestone milestone = createMilestoneTree();
        milestone.getJourney().startDate(Instant.now().minus(2L, DAYS))
            .endDate(Instant.now().minus(1L, DAYS));
        Assignment assignment = createAssignment()
            .requiresSubmission(true)
            .creator(getTeacher(entityManager))
            .gradingScheme(getGradingSchemeNumerical(entityManager))
            .milestone(milestone)
            .deadline(Instant.now().minus(1L, DAYS));
        transactionHelper.withNewTransaction(() -> {
            milestone.getJourney()
                .addStudent(getStudent(entityManager))
                .addTeacher(getTeacher(entityManager));
            persistCreatedEntities(entityManager);
        });

        transactionHelper.withNewTransaction(() -> {
            Set<JourneyState> states = entityManager
                .find(Journey.class, milestone
                    .getJourney()
                    .getId())
                .getStates();
            assertEquals(0, states.size());
        });

        AssignmentDTO assignmentDTO = assignmentMapper.toDto(assignment).setRequiresSubmission(false);
        mockMvc.perform(put(API_ASSIGNMENTS)
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(assignmentDTO, true)))
            .andExpect(status().isOk());

        transactionHelper.withNewTransaction(() -> {
            Set<JourneyState> states = entityManager
                .find(Journey.class, milestone
                    .getJourney()
                    .getId())
                .getStates();
            assertEquals(2, states.size());
            assertTrue(states
                .stream()
                .allMatch(result -> result
                    .getState()
                    .equals(COMPLETED)));
        });
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldBeCompletedWhenAssignmentDeleted() throws Exception {
        Milestone milestone = createMilestoneTree()
            .endDate(Instant.now().minus(1L, DAYS));
        milestone.getJourney().startDate(Instant.now().minus(2L, DAYS))
            .endDate(Instant.now().minus(1L, DAYS));
        Assignment assignment = createAssignment()
            .requiresSubmission(true)
            .creator(getTeacher(entityManager))
            .gradingScheme(getGradingSchemeNumerical(entityManager))
            .milestone(milestone)
            .deadline(Instant.now().minus(1L, DAYS));
        transactionHelper.withNewTransaction(() -> {
            milestone.getJourney()
                .addStudent(getStudent(entityManager))
                .addTeacher(getTeacher(entityManager));
            persistCreatedEntities(entityManager);
        });

        transactionHelper.withNewTransaction(() -> {
            Set<JourneyState> states = entityManager
                .find(Journey.class, milestone
                    .getJourney()
                    .getId())
                .getStates();
            assertEquals(0, states.size());
        });
        mockMvc.perform(delete(format("%s/%s", API_ASSIGNMENTS, assignment.getId()))
                .contentType(APPLICATION_JSON))
            .andExpect(status().isNoContent());

        transactionHelper.withNewTransaction(() -> {
            Set<JourneyState> states = entityManager
                .find(Journey.class, milestone
                    .getJourney()
                    .getId())
                .getStates();
            assertEquals(2, states.size());
            assertTrue(states
                .stream()
                .allMatch(result -> result
                    .getState()
                    .equals(COMPLETED)));
        });
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldBeCompletedWhenSubmissionFeedbackGradedWithoutSubmission() throws Exception {
        Assignment assignment = createAssignmentTree()
            .requiresSubmission(true);
        assignment.getMilestone().endDate(Instant.now().minus(1L, DAYS));
        assignment.getMilestone().getJourney()
            .startDate(Instant.now().minus(2L, DAYS))
            .endDate(Instant.now().minus(1L, DAYS));
        transactionHelper.withNewTransaction(() -> {
            assignment.getMilestone()
                .getJourney()
                .addStudent(getStudent(entityManager))
                .addTeacher(getTeacher(entityManager));
            persistCreatedEntities(entityManager);
        });
        Submission submission = createSubmission().assignment(assignment);
        SubmissionFeedback submissionFeedback = createSubmissionFeedback()
            .grade("5");
        transactionHelper.withNewTransaction(() -> {
            User student = getStudent(entityManager);
            submission.addAuthor(student);
            submissionFeedback.student(student);
        });

        SubmissionFeedbackDTO submissionFeedbackDTO = submissionFeedbackMapper.toDto(submissionFeedback)
            .submission(submissionMapper.toDto(submission));
        SubmissionGradingPatchDTO submissionGradingPatchDTO = new SubmissionGradingPatchDTO()
            .submissionFeedback(submissionFeedbackDTO)
            .resubmittable(false)
            .submittedForGradingDate(submission.getSubmittedForGradingDate());

        mockMvc.perform(post(API_SUBMISSION_FEEDBACKS)
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(submissionGradingPatchDTO)))
            .andExpect(status().isCreated());

        transactionHelper.withNewTransaction(() -> {
            Set<JourneyState> states = entityManager
                .find(Journey.class, assignment
                    .getMilestone()
                    .getJourney()
                    .getId())
                .getStates();
            assertEquals(2, states.size());
            assertTrue(states
                .stream()
                .allMatch(result -> result
                    .getState()
                    .equals(COMPLETED)));
        });
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldBeCompletedWhenSubmissionFeedbackGraded() throws Exception {
        Assignment assignment = createAssignmentTree()
            .requiresSubmission(true);
        assignment.getMilestone().endDate(Instant.now().minus(1L, DAYS));
        assignment.getMilestone().getJourney()
            .startDate(Instant.now().minus(2L, DAYS))
            .endDate(Instant.now().minus(1L, DAYS));
        Submission submission = createSubmission().assignment(assignment);
        transactionHelper.withNewTransaction(() -> {
            assignment.getMilestone()
                .getJourney()
                .addTeacher(getTeacher(entityManager));
            submission.addAuthor(getStudent(entityManager));
            persistCreatedEntities(entityManager);
        });
        SubmissionFeedback submissionFeedback = createSubmissionFeedback()
            .grade("5")
            .submission(submission);

        transactionHelper.withNewTransaction(() -> {
            submissionFeedback.student(getStudent(entityManager));
        });

        SubmissionFeedbackDTO submissionFeedbackDTO = submissionFeedbackMapper.toDto(submissionFeedback);
        SubmissionGradingPatchDTO submissionGradingPatchDTO = new SubmissionGradingPatchDTO()
            .submissionFeedback(submissionFeedbackDTO)
            .resubmittable(false)
            .submittedForGradingDate(submission.getSubmittedForGradingDate());

        mockMvc.perform(patch(format("%s?submissionId=%s", API_SUBMISSION_FEEDBACKS, submission.getId()))
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(submissionGradingPatchDTO)))
            .andExpect(status().isOk());

        transactionHelper.withNewTransaction(() -> {
            Set<JourneyState> states = entityManager
                .find(Journey.class, assignment
                    .getMilestone()
                    .getJourney()
                    .getId())
                .getStates();
            assertEquals(2, states.size());
            assertTrue(states
                .stream()
                .allMatch(result -> result
                    .getState()
                    .equals(COMPLETED)));
        });
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldBeInProgressAndCompletedWhenSubmissionFeedbackGradeChanged() throws Exception {
        Assignment assignment = createAssignmentTree()
            .requiresSubmission(true);
        assignment.getMilestone().endDate(Instant.now().minus(1L, DAYS));
        assignment.getMilestone().getJourney()
            .startDate(Instant.now().minus(2L, DAYS))
            .endDate(Instant.now().minus(1L, DAYS));
        Submission submission = createSubmission().assignment(assignment);
        SubmissionFeedback submissionFeedback = createSubmissionFeedback()
            .grade("5")
            .submission(submission)
            .creator(getTeacher(entityManager));
        transactionHelper.withNewTransaction(() -> {
            User student = getStudent(entityManager);
            submission.addAuthor(student);
            submissionFeedback.student(student);
            assignment.getMilestone()
                .getJourney()
                .addTeacher(getTeacher(entityManager));
            persistCreatedEntities(entityManager);
        });

        SubmissionFeedbackDTO submissionFeedbackDTO = submissionFeedbackMapper.toDto(submissionFeedback)
            .grade("1");
        SubmissionGradingPatchDTO submissionGradingPatchDTO = new SubmissionGradingPatchDTO()
            .submissionFeedback(submissionFeedbackDTO)
            .resubmittable(false)
            .submittedForGradingDate(submission.getSubmittedForGradingDate());

        mockMvc.perform(patch(format("%s?submissionId=%s", API_SUBMISSION_FEEDBACKS, submission.getId()))
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(submissionGradingPatchDTO)))
            .andExpect(status().isOk());

        transactionHelper.withNewTransaction(() -> {
            Set<JourneyState> states = entityManager
                .find(Journey.class, assignment
                    .getMilestone()
                    .getJourney()
                    .getId())
                .getStates();
            assertEquals(2, states.size());
            assertTrue(states
                .stream()
                .allMatch(result -> result
                    .getState()
                    .equals(COMPLETED)));
        });
    }

    @Test
    @WithMockCustomUser(authorities = {ADMIN})
    void shouldBeInProgressWhenGradedSubmissionDeleted() throws Exception {
        Assignment assignment = createAssignmentTree();
        assignment.getMilestone().endDate(Instant.now().minus(1L, DAYS));
        assignment.getMilestone().getJourney()
            .startDate(Instant.now().minus(2L, DAYS))
            .endDate(Instant.now().minus(1L, DAYS));
        Submission submission = createSubmission().assignment(assignment);
        SubmissionFeedback submissionFeedback = createSubmissionFeedback()
            .grade("5")
            .submission(submission)
            .creator(getTeacher(entityManager));
        transactionHelper.withNewTransaction(() -> {
            assignment.getMilestone()
                .getJourney()
                .addTeacher(getTeacher(entityManager));
            User student = getStudent(entityManager);
            submission.addAuthor(student);
            submissionFeedback.student(student);
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(delete(format("%s/%s", API_SUBMISSIONS, submission.getId()))
                .contentType(APPLICATION_JSON))
            .andExpect(status().isNoContent());

        transactionHelper.withNewTransaction(() -> {
            Set<JourneyState> states = entityManager
                .find(Journey.class, assignment
                    .getMilestone()
                    .getJourney()
                    .getId())
                .getStates();
            assertEquals(2, states.size());
            assertTrue(states
                .stream()
                .allMatch(result -> result
                    .getState()
                    .equals(IN_PROGRESS)));
        });
    }

    private Assignment createAssignmentTree() {
        Milestone milestone = createMilestoneTree();
        Assignment assignment = createAssignment().milestone(milestone);
        transactionHelper.withNewTransaction(() -> {
            assignment
                .creator(getTeacher(entityManager))
                .gradingScheme(getGradingSchemeNumerical(entityManager));
        });
        return assignment;
    }

    private Milestone createMilestoneTree() {
        EducationalLevel educationalLevel = createEducationalLevel();
        Journey journey = createJourney();
        Milestone milestone = createMilestone();
        transactionHelper.withNewTransaction(() -> {
            User teacher = getTeacher(entityManager);
            Subject subject = getSubject(entityManager);
            journey.educationalLevel(educationalLevel)
                .addStudent(getStudent(entityManager))
                .creator(teacher)
                .setSubject(subject)
                .addMilestone(milestone
                    .creator(teacher));
        });
        return milestone;
    }
}
