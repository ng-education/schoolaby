package com.schoolaby.web.rest;

import com.schoolaby.common.BaseIntegrationTest;
import com.schoolaby.common.TestObjects;
import com.schoolaby.domain.*;
import com.schoolaby.repository.MilestoneJpaRepository;
import com.schoolaby.repository.MilestoneRepository;
import com.schoolaby.security.SpringSecurityAuditorAware;
import com.schoolaby.service.dto.MilestoneDTO;
import com.schoolaby.service.mapper.MilestoneMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.schoolaby.common.TestCases.*;
import static com.schoolaby.common.TestObjects.*;
import static com.schoolaby.web.rest.TestUtil.convertObjectToJsonBytes;
import static java.lang.Long.MAX_VALUE;
import static java.time.Instant.now;
import static java.time.Instant.ofEpochMilli;
import static java.time.temporal.ChronoUnit.DAYS;
import static java.time.temporal.ChronoUnit.MILLIS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

class MilestoneControllerIT extends BaseIntegrationTest {
    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Instant DEFAULT_END_DATE = ofEpochMilli(0L);
    private static final Instant UPDATED_END_DATE = now().truncatedTo(MILLIS);

    @Autowired
    private MilestoneRepository milestoneRepository;
    @Autowired
    private MilestoneJpaRepository milestoneJpaRepository;
    @Autowired
    private MilestoneMapper milestoneMapper;
    @SpyBean
    private SpringSecurityAuditorAware springSecurityAuditorAware;

    public static Milestone createEntity(EntityManager em) {
        return new Milestone()
            .title(DEFAULT_TITLE)
            .description(DEFAULT_DESCRIPTION)
            .endDate(DEFAULT_END_DATE)
            .addAssignment(TestObjects.createAssignment());
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void shouldCreate() throws Exception {
        User teacher = getTeacher(entityManager);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(teacher)
            .addTeacher(teacher);
        persistCreatedEntities(entityManager);
        MilestoneDTO milestoneDTO = milestoneMapper.toDto(createMilestone().journey(journey));

        mockMvc.perform(post("/api/milestones")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(milestoneDTO)))
            .andExpect(status().isCreated());

        List<Milestone> milestones = milestoneJpaRepository.findAll();
        assertEquals(1, milestones.size());
        Milestone milestone = milestones.get(0);
        assertEquals(MILESTONE_TITLE, milestone.getTitle());
        assertEquals(MILESTONE_DESCRIPTION, milestone.getDescription());
        assertEquals(MILESTONE_END_DATE, milestone.getEndDate());
        assertFalse(milestone.isHidden());
        assertEquals(TEACHER_ID, milestone.getCreator().getId());
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void createMilestoneWithExistingId() throws Exception {
        MilestoneDTO milestoneDTO = milestoneMapper.toDto(createMilestone().id(1L).journey(createJourney().addTeacher(getTeacher(entityManager))));

        mockMvc.perform(post("/api/milestones")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(milestoneDTO)))
            .andExpect(status().isBadRequest());

        List<Milestone> milestoneList = milestoneJpaRepository.findAll();
        assertTrue(milestoneList.isEmpty());
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void checkTitleIsRequired() throws Exception {
        MilestoneDTO milestoneDTO = milestoneMapper.toDto(createMilestone().title(null).journey(createJourney().addTeacher(getTeacher(entityManager))));

        mockMvc.perform(post("/api/milestones")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(milestoneDTO)))
            .andExpect(status().isBadRequest());

        List<Milestone> milestoneList = milestoneJpaRepository.findAll();
        assertTrue(milestoneList.isEmpty());
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldNotAllowToCreateWhenUserNotATeacherInJourney() throws Exception {
        User teacher = createTeacher();
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(teacher)
            .addTeacher(teacher);
        transactionHelper.withTransaction(() -> {
            journey.addStudent(getTeacher(entityManager));
            persistCreatedEntities(entityManager);
        });
        MilestoneDTO milestoneDTO = milestoneMapper.toDto(createMilestone().journey(journey));

        mockMvc.perform(post("/api/milestones")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(milestoneDTO)))
            .andExpect(status().isForbidden())
            .andExpect(jsonPath("$.detail").value("403 FORBIDDEN \"Current user is not a teacher in the journey!\""));
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void getAllMilestones() throws Exception {
        User student = getStudent(entityManager);
        User teacher = getTeacher(entityManager);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(teacher)
            .addStudent(student);
        Milestone milestone = createMilestone()
            .journey(journey)
            .creator(teacher);
        persistCreatedEntities(entityManager);

        mockMvc.perform(get("/api/milestones?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(milestone.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(MILESTONE_TITLE)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(MILESTONE_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].endDate").value(hasItem(MILESTONE_END_DATE.toString())));
    }

    @Test
    @WithUserDetails(STUDENT_LOGIN)
    void shouldGetMilestonesOrderedByEndDate() throws Exception {
        String firstMilestoneTitle = "First milestone";
        String secondMilestoneTitle = "Second milestone";
        String thirdMilestoneTitle = "Third milestone";

        transactionHelper.withNewTransaction(() -> {
            User student = getStudent(entityManager);
            User teacher = getTeacher(entityManager);
            Journey journey = createJourney()
                .educationalLevel(createEducationalLevel())
                .creator(teacher)
                .addStudent(student);

            createMilestone(thirdMilestoneTitle)
                .journey(journey)
                .creator(teacher)
                .endDate(MILESTONE_END_DATE.plus(1, DAYS));

            createMilestone(secondMilestoneTitle)
                .journey(journey)
                .creator(teacher);

            createMilestone(firstMilestoneTitle)
                .journey(journey)
                .creator(teacher)
                .endDate(MILESTONE_END_DATE.minus(1, DAYS));
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(get("/api/milestones"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$", hasSize(3)))
            .andExpect(jsonPath("$.[0].title").value(firstMilestoneTitle))
            .andExpect(jsonPath("$.[1].title").value(secondMilestoneTitle))
            .andExpect(jsonPath("$.[2].title").value(thirdMilestoneTitle));
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void getUnlimitedMilestones() throws Exception {
        User student = getStudent(entityManager);
        User teacher = getTeacher(entityManager);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(teacher)
            .addStudent(student);

        int maxMilestonesPerPage = 20;
        int totalMilestones = maxMilestonesPerPage + 5;

        createMilestones(totalMilestones, journey, teacher);

        persistCreatedEntities(entityManager);

        mockMvc.perform(get("/api/milestones?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.length()").value(totalMilestones));
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void getMilestone() throws Exception {
        User student = getStudent(entityManager);
        User teacher = getTeacher(entityManager);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(teacher)
            .addTeacher(teacher)
            .addStudent(student);
        Milestone milestone = createMilestone()
            .journey(journey)
            .creator(teacher);
        persistCreatedEntities(entityManager);

        mockMvc.perform(get("/api/milestones/{id}", milestone.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(milestone.getId().intValue()))
            .andExpect(jsonPath("$.title").value(MILESTONE_TITLE))
            .andExpect(jsonPath("$.description").value(MILESTONE_DESCRIPTION))
            .andExpect(jsonPath("$.endDate").value(MILESTONE_END_DATE.toString()));
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void getNonExistingMilestone() throws Exception {
        mockMvc.perform(get("/api/milestones/{id}", MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void updateMilestone() throws Exception {
        User teacher = getTeacher(entityManager);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(teacher)
            .addTeacher(teacher);
        Milestone milestone = createMilestone()
            .journey(journey)
            .creator(teacher);
        persistCreatedEntities(entityManager);

        int databaseSizeBeforeUpdate = milestoneJpaRepository.findAll().size();

        Milestone updatedMilestone = milestoneRepository.findOne(milestone.getId(), false).orElseThrow();
        entityManager.detach(updatedMilestone);
        updatedMilestone.title(UPDATED_TITLE).description(UPDATED_DESCRIPTION).endDate(UPDATED_END_DATE);
        MilestoneDTO milestoneDTO = milestoneMapper.toDto(updatedMilestone);

        mockMvc.perform(put("/api/milestones")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(milestoneDTO)))
            .andExpect(status().isOk());

        List<Milestone> milestoneList = milestoneJpaRepository.findAll();
        assertThat(milestoneList).hasSize(databaseSizeBeforeUpdate);
        Milestone testMilestone = milestoneList.get(milestoneList.size() - 1);
        assertThat(testMilestone.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testMilestone.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testMilestone.getEndDate()).isEqualTo(UPDATED_END_DATE);
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void updateNonExistingMilestone() throws Exception {
        MilestoneDTO milestoneDTO = milestoneMapper.toDto(createMilestone().journey(createJourney().addTeacher(getTeacher(entityManager))));

        mockMvc.perform(put("/api/milestones")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(milestoneDTO)))
            .andExpect(status().isBadRequest());

        List<Milestone> milestoneList = milestoneJpaRepository.findAll();
        assertTrue(milestoneList.isEmpty());
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldNotAllowToUpdateWhenUserNotATeacherInJourney() throws Exception {
        User teacher = createTeacher();
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(teacher)
            .addTeacher(teacher);
        Milestone milestone = createMilestone()
            .journey(journey)
            .creator(teacher);
        transactionHelper.withTransaction(() -> {
            journey.addStudent(getTeacher(entityManager));
            persistCreatedEntities(entityManager);
        });
        MilestoneDTO milestoneDTO = milestoneMapper.toDto(milestone);

        mockMvc.perform(put("/api/milestones")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(milestoneDTO)))
            .andExpect(status().isForbidden())
            .andExpect(jsonPath("$.detail").value("403 FORBIDDEN \"Current user is not a teacher in the journey!\""));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldDelete() throws Exception {
        final Journey journey = createJourney();
        final Milestone milestone = createMilestone();
        final Assignment assignment = createAssignment();
        final EducationalAlignment educationalAlignment = createEducationalAlignment();
        final EducationalLevel educationalLevel = createEducationalLevel();
        final Material material = createMaterial();
        final LtiResource milestoneLtiResource = createLtiResource();
        transactionHelper.withNewTransaction(() -> {
            final User teacher = getTeacher(entityManager);
            journey.educationalLevel(educationalLevel)
                .creator(teacher)
                .addTeacher(teacher)
                .addMilestone(milestone
                    .addAssignment(assignment
                        .creator(teacher)
                        .gradingScheme(getGradingSchemeNumerical(entityManager)))
                    .addMaterial(material)
                    .addLtiResource(milestoneLtiResource)
                    .addEducationalAlignment(educationalAlignment)
                    .creator(teacher));
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(delete("/api/milestones/{id}", milestone.getId())
                .accept(APPLICATION_JSON))
            .andExpect(status().isNoContent());

        transactionHelper.withNewTransaction(() -> {
            // entities that should be soft deleted
            assertNotNull(entityManager.createNativeQuery("SELECT deleted FROM milestone WHERE id = ?").setParameter(1, milestone.getId()).getSingleResult());
            assertNotNull(entityManager.createNativeQuery("SELECT deleted FROM assignment WHERE id = ?").setParameter(1, assignment.getId()).getSingleResult());
            assertNotNull(entityManager.createNativeQuery("SELECT deleted FROM lti_resource WHERE id = ?").setParameter(1, milestoneLtiResource.getId()).getSingleResult());

            // entities that should only have relation deleted
            assertEquals(0, entityManager.find(Journey.class, journey.getId()).getMilestones().size());
            assertEquals(0, entityManager.find(EducationalAlignment.class, educationalAlignment.getId()).getMilestones().size());
            assertEquals(0, entityManager.find(Material.class, material.getId()).getMilestones().size());
        });
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldNotAllowToDeleteWhenUserNotATeacherInJourney() throws Exception {
        User teacher = createTeacher();
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(teacher)
            .addTeacher(teacher);
        Milestone milestone = createMilestone()
            .journey(journey)
            .creator(teacher);
        transactionHelper.withTransaction(() -> {
            journey.addStudent(getTeacher(entityManager));
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(delete("/api/milestones/{id}", milestone.getId()))
            .andExpect(status().isForbidden())
            .andExpect(jsonPath("$.detail").value("403 FORBIDDEN \"Current user is not a teacher in the journey!\""));
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void shouldHideHiddenMilestonesFromStudent() throws Exception {
        User teacher = getTeacher(entityManager);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .addMilestone(createMilestone()
                .creator(teacher))
            .addStudent(getStudent(entityManager))
            .creator(teacher);
        Milestone hiddenMilestone = createMilestone()
            .published(null)
            .journey(journey)
            .creator(teacher);
        persistCreatedEntities(entityManager);

        mockMvc.perform(get("/api/milestones"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("length()").value(1))
            .andExpect(jsonPath("$.[0].hidden").value(false));
        mockMvc.perform(get("/api/milestones/{id}", hiddenMilestone.getId()))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void shouldNotHideHiddenMilestonesFromTeacher() throws Exception {
        User teacher = getTeacher(entityManager);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(teacher)
            .addTeacher(teacher)
            .addMilestone(createMilestone().creator(teacher));
        Milestone hiddenMilestone = createMilestone()
            .published(null)
            .journey(journey)
            .creator(teacher);
        persistCreatedEntities(entityManager);

        mockMvc.perform(get("/api/milestones"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("length()").value(2))
            .andExpect(jsonPath("$.[*].hidden").value(hasItems(true, false)));
        mockMvc.perform(get("/api/milestones/{id}", hiddenMilestone.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("hidden").value(true));
    }

    @Test
    @WithUserDetails(STUDENT_LOGIN)
    void shouldHideHiddenAssignmentFromStudent() throws Exception {
        final Journey journey = createJourney();
        final Milestone milestone = createMilestone();
        transactionHelper.withNewTransaction(() -> {
            User teacher = getTeacher(entityManager);
            User student = getStudent(entityManager);
            journey
                .educationalLevel(createEducationalLevel())
                .addMilestone(milestone
                    .creator(teacher)
                    .addAssignment(createAssignment()
                        .creator(teacher)
                        .gradingScheme(getGradingSchemeNumerical(entityManager))
                        .addStudent(student))
                    .addAssignment(createAssignment()
                        .creator(teacher)
                        .gradingScheme(getGradingSchemeNumerical(entityManager))
                        .addStudent(student)
                        .published(null))
                    .addAssignment(createAssignment()
                        .creator(teacher)
                        .gradingScheme(getGradingSchemeNumerical(entityManager))
                        .addStudent(student)
                        .published(Instant.now().plusSeconds(360000L))))
                .addStudent(student)
                .creator(teacher);
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(get("/api/milestones"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("length()").value(1))
            .andExpect(jsonPath("$.[0].assignments.length()").value(1))
            .andExpect(jsonPath("$.[0].assignments[0].hidden").value(false));
        mockMvc.perform(get("/api/milestones/{id}", milestone.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("assignments.length()").value(1))
            .andExpect(jsonPath("assignments[0].hidden").value(false));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldNotHideHiddenAssignmentFromTeacher() throws Exception {
        Journey journey = createJourney();
        Milestone milestone = createMilestone();
        transactionHelper.withNewTransaction(() -> {
            User teacher = getTeacher(entityManager);
            journey
                .educationalLevel(createEducationalLevel())
                .addMilestone(milestone
                    .creator(teacher)
                    .addAssignment(createAssignment()
                        .creator(teacher)
                        .gradingScheme(getGradingSchemeNumerical(entityManager)))
                    .addAssignment(createAssignment()
                        .creator(teacher)
                        .gradingScheme(getGradingSchemeNumerical(entityManager))
                        .published(null))
                    .addAssignment(createAssignment()
                        .creator(teacher)
                        .gradingScheme(getGradingSchemeNumerical(entityManager))
                        .published(Instant.now().plusSeconds(360000L)))
                )
                .creator(teacher)
                .addTeacher(teacher);
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(get("/api/milestones"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("length()").value(1))
            .andExpect(jsonPath("$.[0].assignments.length()").value(3))
            .andExpect(jsonPath("$.[0].assignments[*].hidden").value(hasItems(true, false)));
        mockMvc.perform(get("/api/milestones/{id}", milestone.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("assignments.length()").value(3))
            .andExpect(jsonPath("assignments[*].hidden").value(hasItems(true, false)));
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void shouldThrowOnGetMilestoneWhenTeacherNotInJourney() throws Exception {
        User teacher = getTeacher(entityManager);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(teacher);
        Milestone milestone = createMilestone()
            .journey(journey)
            .creator(teacher);
        persistCreatedEntities(entityManager);

        mockMvc.perform(get("/api/milestones/{id}", milestone.getId()))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void shouldThrowOnGetMilestoneWhenStudentNotInJourney() throws Exception {
        User teacher = getTeacher(entityManager);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(teacher);
        Milestone milestone = createMilestone()
            .journey(journey)
            .creator(teacher);
        persistCreatedEntities(entityManager);

        mockMvc.perform(get("/api/milestones/{id}", milestone.getId()))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void shouldGetEmptyResponseOnGetAllMilestonesWhenStudentNotInJourney() throws Exception {
        User teacher = getTeacher(entityManager);
        createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(teacher);
        persistCreatedEntities(entityManager);

        mockMvc.perform(get("/api/milestones?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("length()").value(0));
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void shouldGetEmptyResponseOnGetAllMilestonesWhenTeacherNotInJourney() throws Exception {
        User teacher = getTeacher(entityManager);
        createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(teacher);
        persistCreatedEntities(entityManager);

        mockMvc.perform(get("/api/milestones?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("length()").value(0));
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void shouldThrowOnUpdateWhenMilestoneNotFound() throws Exception {
        User teacher = getTeacher(entityManager);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(teacher)
            .addTeacher(teacher);
        persistCreatedEntities(entityManager);
        Milestone milestone = createMilestone()
            .id(1L)
            .journey(journey)
            .creator(teacher);

        milestone.title(UPDATED_TITLE).description(UPDATED_DESCRIPTION).endDate(UPDATED_END_DATE);
        MilestoneDTO milestoneDTO = milestoneMapper.toDto(milestone);

        mockMvc.perform(put("/api/milestones")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(milestoneDTO)))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void shouldThrowOnDeleteWhenTeacherNotInJourney() throws Exception {
        User teacher = getTeacher(entityManager);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(teacher);
        Milestone milestone = createMilestone()
            .journey(journey)
            .creator(teacher);
        persistCreatedEntities(entityManager);

        mockMvc.perform(delete("/api/milestones/{id}", milestone.getId())
                .accept(APPLICATION_JSON))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void shouldThrowOnDeleteWhenMilestoneNotPresent() throws Exception {
        mockMvc.perform(delete("/api/milestones/{id}", 1)
                .accept(APPLICATION_JSON))
            .andExpect(status().isNotFound());
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldUpdateNonRestrictedMaterialOnUpdateWhenCreatedByUser() throws Exception {
        String updatedMaterialTitle = "Updated material title";
        String updatedMaterialDescription = "Updated material description";
        Boolean updatedMaterialRestricted = true;
        String updatedMaterialUrl = "Updated material url";

        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel());
        Material material = createMaterial().restricted(false);
        Milestone milestone = createMilestone();

        transactionHelper.withTransaction(() -> {
            User teacher = getTeacher(entityManager);
            milestone
                .journey(journey)
                .creator(teacher)
                .addMaterial(material);
            journey
                .educationalLevel(createEducationalLevel())
                .creator(teacher)
                .addTeacher(teacher);
            persistCreatedEntities(entityManager);
        });

        material
            .title(updatedMaterialTitle)
            .description(updatedMaterialDescription)
            .restricted(updatedMaterialRestricted)
            .url(updatedMaterialUrl);
        milestone.materials(Set.of(material));

        mockMvc.perform(put("/api/milestones")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(milestoneMapper.toDto(milestone))))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.materials.size()").value(1))
            .andExpect(jsonPath("$.materials[0].title").value(updatedMaterialTitle))
            .andExpect(jsonPath("$.materials[0].description").value(updatedMaterialDescription))
            .andExpect(jsonPath("$.materials[0].restricted").value(updatedMaterialRestricted))
            .andExpect(jsonPath("$.materials[0].url").value(updatedMaterialUrl));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldNotUpdateNonRestrictedMaterialOnUpdateWhenNotCreatedByUser() throws Exception {
        when(springSecurityAuditorAware.getCurrentAuditor()).thenReturn(Optional.of("teacher2"));

        String updatedMaterialTitle = "Updated material title";
        String updatedMaterialDescription = "Updated material description";
        Boolean updatedMaterialRestricted = true;
        String updatedMaterialUrl = "Updated material url";

        Journey journey = createJourney();
        Material material = createMaterial().restricted(false);
        Milestone milestone = createMilestone();

        transactionHelper.withTransaction(() -> {
            User teacher = getTeacher(entityManager);
            milestone
                .journey(journey)
                .creator(teacher)
                .addMaterial(material);
            journey
                .educationalLevel(createEducationalLevel())
                .creator(teacher)
                .addTeacher(teacher);
            persistCreatedEntities(entityManager);
        });

        material
            .title(updatedMaterialTitle)
            .description(updatedMaterialDescription)
            .restricted(updatedMaterialRestricted)
            .url(updatedMaterialUrl);
        milestone.materials(Set.of(material));

        mockMvc.perform(put("/api/milestones")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(milestoneMapper.toDto(milestone))))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.materials.size()").value(1))
            .andExpect(jsonPath("$.materials[0].title").value(MATERIAL_TITLE))
            .andExpect(jsonPath("$.materials[0].description").value(MATERIAL_DESCRIPTION))
            .andExpect(jsonPath("$.materials[0].restricted").value(false))
            .andExpect(jsonPath("$.materials[0].url").value(MATERIAL_URL));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldCreateWithExistingMaterialCreatedByUser() throws Exception {
        Material material = createMaterial().restricted(false);
        Journey journey = createJourney();

        transactionHelper.withTransaction(() -> {
            User teacher = getTeacher(entityManager);
            journey
                .educationalLevel(createEducationalLevel())
                .creator(teacher)
                .addTeacher(teacher);
            persistCreatedEntities(entityManager);
        });

        Milestone milestone = createMilestone()
            .journey(journey)
            .addMaterial(material);

        mockMvc.perform(post("/api/milestones")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(milestoneMapper.toDto(milestone))))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.materials.size()").value(1))
            .andExpect(jsonPath("$.materials[0].title").value(MATERIAL_TITLE))
            .andExpect(jsonPath("$.materials[0].description").value(MATERIAL_DESCRIPTION))
            .andExpect(jsonPath("$.materials[0].restricted").value(false))
            .andExpect(jsonPath("$.materials[0].url").value(MATERIAL_URL));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldNotUpdateNonRestrictedMaterialOnCreateWhenNotCreatedByUser() throws Exception {
        when(springSecurityAuditorAware.getCurrentAuditor()).thenReturn(Optional.of("teacher2"));

        String updatedMaterialTitle = "Updated material title";
        String updatedMaterialDescription = "Updated material description";
        Boolean updatedMaterialRestricted = true;
        String updatedMaterialUrl = "Updated material url";

        Material material = createMaterial().restricted(false);
        Journey journey = createJourney();

        transactionHelper.withTransaction(() -> {
            User teacher = getTeacher(entityManager);
            journey
                .educationalLevel(createEducationalLevel())
                .creator(teacher)
                .addTeacher(teacher);
            persistCreatedEntities(entityManager);
        });

        material
            .title(updatedMaterialTitle)
            .description(updatedMaterialDescription)
            .restricted(updatedMaterialRestricted)
            .url(updatedMaterialUrl);
        Milestone milestone = createMilestone()
            .journey(journey)
            .addMaterial(material);

        mockMvc.perform(post("/api/milestones")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(milestoneMapper.toDto(milestone))))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.materials.size()").value(1))
            .andExpect(jsonPath("$.materials[0].title").value(MATERIAL_TITLE))
            .andExpect(jsonPath("$.materials[0].description").value(MATERIAL_DESCRIPTION))
            .andExpect(jsonPath("$.materials[0].restricted").value(false))
            .andExpect(jsonPath("$.materials[0].url").value(MATERIAL_URL));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldUpdateNonRestrictedMaterialOnCreateWhenCreatedByUser() throws Exception {
        String updatedMaterialTitle = "Updated material title";
        String updatedMaterialDescription = "Updated material description";
        Boolean updatedMaterialRestricted = true;
        String updatedMaterialUrl = "Updated material url";

        Material material = createMaterial().restricted(false);
        Journey journey = createJourney();

        transactionHelper.withTransaction(() -> {
            User teacher = getTeacher(entityManager);
            journey
                .educationalLevel(createEducationalLevel())
                .creator(teacher)
                .addTeacher(teacher);
            persistCreatedEntities(entityManager);
        });

        material
            .title(updatedMaterialTitle)
            .description(updatedMaterialDescription)
            .restricted(updatedMaterialRestricted)
            .url(updatedMaterialUrl);

        Milestone milestone = createMilestone()
            .journey(journey)
            .addMaterial(material);

        mockMvc.perform(post("/api/milestones")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(milestoneMapper.toDto(milestone))))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.materials.size()").value(1))
            .andExpect(jsonPath("$.materials[0].title").value(updatedMaterialTitle))
            .andExpect(jsonPath("$.materials[0].description").value(updatedMaterialDescription))
            .andExpect(jsonPath("$.materials[0].restricted").value(updatedMaterialRestricted))
            .andExpect(jsonPath("$.materials[0].url").value(updatedMaterialUrl));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldGetTemplateJourneyMilestones() throws Exception {
        EducationalLevel educationalLevel = createEducationalLevel();
        Journey journey = createJourney()
            .educationalLevel(educationalLevel);
        Milestone firstMilestone = createMilestone()
            .title("First milestone");
        Milestone secondMilestone = createMilestone()
            .title("Second milestone");

        transactionHelper.withTransaction(() -> {
            User teacher = getTeacher(entityManager);
            firstMilestone.creator(teacher);
            secondMilestone.creator(teacher);
            journey
                .creator(teacher)
                .addTeacher(teacher)
                .addMilestone(firstMilestone)
                .addMilestone(secondMilestone);

            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(get("/api/milestones")
                .param("journeyId", String.valueOf(journey.getId()))
                .param("template", "true"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(2)))
            .andExpect(jsonPath("$[*].title", containsInAnyOrder(firstMilestone.getTitle(), secondMilestone.getTitle())));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldNotGetNonTemplateJourneyMilestones() throws Exception {
        User teacher = createTeacher();
        EducationalLevel educationalLevel = createEducationalLevel();
        Journey journey = createJourney()
            .template(false)
            .educationalLevel(educationalLevel)
            .creator(teacher)
            .addTeacher(teacher);
        Milestone firstMilestone = createMilestone()
            .title("First milestone")
            .creator(teacher);
        Milestone secondMilestone = createMilestone()
            .title("Second milestone")
            .creator(teacher);

        journey
            .addMilestone(firstMilestone)
            .addMilestone(secondMilestone);

        transactionHelper.withTransaction(() -> persistCreatedEntities(entityManager));

        mockMvc.perform(get("/api/milestones")
                .param("journeyId", String.valueOf(journey.getId()))
                .param("template", "true"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldNotGetTemplateJourneyMilestonesWhenTemplateUndefinedInQuery() throws Exception {
        User teacher = createTeacher();
        EducationalLevel educationalLevel = createEducationalLevel();
        Journey journey = createJourney()
            .template(false)
            .educationalLevel(educationalLevel)
            .creator(teacher)
            .addTeacher(teacher);
        Milestone firstMilestone = createMilestone()
            .title("First milestone")
            .creator(teacher);
        Milestone secondMilestone = createMilestone()
            .title("Second milestone")
            .creator(teacher);

        journey
            .addMilestone(firstMilestone)
            .addMilestone(secondMilestone);

        transactionHelper.withTransaction(() -> persistCreatedEntities(entityManager));

        mockMvc.perform(get("/api/milestones")
                .param("journeyId", String.valueOf(journey.getId())))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldGetTemplateJourneyMilestone() throws Exception {
        EducationalLevel educationalLevel = createEducationalLevel();
        Journey journey = createJourney()
            .educationalLevel(educationalLevel);
        Milestone milestone = createMilestone();

        transactionHelper.withTransaction(() -> {
            User teacher = getTeacher(entityManager);
            milestone.creator(teacher);
            journey
                .creator(teacher)
                .addTeacher(teacher)
                .addMilestone(milestone);

            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(get("/api/milestones/" + milestone.getId())
                .param("template", "true"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.title").value(milestone.getTitle()));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldNotGetNonTemplateJourneyMilestone() throws Exception {
        User teacher = createTeacher();
        EducationalLevel educationalLevel = createEducationalLevel();
        Journey journey = createJourney()
            .template(false)
            .educationalLevel(educationalLevel)
            .creator(teacher)
            .addTeacher(teacher);
        Milestone milestone = createMilestone()
            .creator(teacher);

        journey.addMilestone(milestone);
        transactionHelper.withTransaction(() -> persistCreatedEntities(entityManager));

        mockMvc.perform(get("/api/milestones/" + milestone.getId())
                .param("template", "true"))
            .andExpect(status().isNotFound());
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldNotGetTemplateJourneyMilestoneWhenTemplateUndefinedInQuery() throws Exception {
        User teacher = createTeacher();
        EducationalLevel educationalLevel = createEducationalLevel();
        Journey journey = createJourney()
            .template(false)
            .educationalLevel(educationalLevel)
            .creator(teacher)
            .addTeacher(teacher);
        Milestone milestone = createMilestone()
            .creator(teacher);

        journey.addMilestone(milestone);
        transactionHelper.withTransaction(() -> persistCreatedEntities(entityManager));

        mockMvc.perform(get("/api/milestones/" + milestone.getId()))
            .andExpect(status().isNotFound());
    }
}
