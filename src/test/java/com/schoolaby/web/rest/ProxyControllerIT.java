package com.schoolaby.web.rest;

import com.schoolaby.common.BaseIntegrationTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.web.header.writers.frameoptions.XFrameOptionsHeaderWriter;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.springframework.http.HttpMethod.HEAD;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.client.ExpectedCount.once;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withBadRequest;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class ProxyControllerIT extends BaseIntegrationTest {
    public static final String API_PROXY_EMBEDDABLE = "/api/proxy/embeddable";
    public static final String URL = "https://www.netgroup.com";
    public static final String PARAM_URL = "url";

    @Autowired
    private RestTemplate externalHeaderCheckTemplate;
    private MockRestServiceServer server;

    @BeforeEach
    void beforeEach() {
        server = MockRestServiceServer.bindTo(externalHeaderCheckTemplate).build();
    }

    @AfterEach
    void afterEach() {
        server.verify();
    }

    @Test
    void shouldNotBeEmbeddableWhenHeadersCheckThrowsClientError() throws Exception {
        server.expect(once(), requestTo(URL))
            .andExpect(method(HEAD))
            .andRespond(withBadRequest());

        mockMvc.perform(get(API_PROXY_EMBEDDABLE).param(PARAM_URL, URL))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(content().string(FALSE.toString()));
    }

    @Test
    void shouldNotBeEmbeddableWhenUrlHasXFrameOptions() throws Exception {
        HttpHeaders headers = new HttpHeaders();
        headers.set(XFrameOptionsHeaderWriter.XFRAME_OPTIONS_HEADER, "ALLOW-FROM https://www.netgroup.com");
        server.expect(once(), requestTo(URL))
            .andExpect(method(HEAD))
            .andRespond(withSuccess().headers(headers));

        mockMvc.perform(get(API_PROXY_EMBEDDABLE).param(PARAM_URL, URL))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(content().string(FALSE.toString()));
    }

    @Test
    void shouldBeEmbeddable() throws Exception {
        server.expect(once(), requestTo(URL))
            .andExpect(method(HEAD))
            .andRespond(withSuccess());

        mockMvc.perform(get(API_PROXY_EMBEDDABLE).param(PARAM_URL, URL))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(content().string(TRUE.toString()));
    }
}
