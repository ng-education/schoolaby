package com.schoolaby.web.rest;

import com.schoolaby.common.BaseIntegrationTest;
import com.schoolaby.domain.*;
import com.schoolaby.repository.*;
import com.schoolaby.service.ChatService;
import com.schoolaby.service.dto.MessageDTO;
import com.schoolaby.service.mapper.MessageMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

import static com.schoolaby.common.TestCases.*;
import static com.schoolaby.common.TestObjects.*;
import static com.schoolaby.web.rest.TestUtil.convertObjectToJsonBytes;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.*;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

class MessageControllerIT extends BaseIntegrationTest {
    private static final String UPDATED_VALUE = "BBBBBBBBBB";

    @Autowired
    private MessageJpaRepository messageRepository;
    @Autowired
    private ChatRepository chatJpaRepository;
    @Autowired
    private ChatService chatService;
    @Autowired
    private MessageMapper messageMapper;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private JourneyRepository journeyRepository;
    @Autowired
    private NotificationJpaRepository notificationJpaRepository;


    private Message message;

    @BeforeEach
    void initTest() {
        message = createMessage();
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void shouldCreateMessageAndNotifications() throws Exception {
        int databaseSizeBeforeCreate = messageRepository.findAll().size();
        User student = createStudent();
        student.setId(STUDENT_ID);
        student = userRepository.save(student);
        Journey journey = createJourney()
            .creator(student)
            .addStudent(student)
            .educationalLevel(new EducationalLevel().id(1L));
        journey = journeyRepository.save(journey);
        Chat chat = createChat()
            .journey(journey)
            .people(Set.of(student));
        chatJpaRepository.save(chat);

        User teacher = createTeacher();
        teacher = userRepository.save(teacher);
        message.chat(chat).creator(teacher);
        MessageDTO messageDTO = messageMapper.toDto(message);

        mockMvc.perform(post("/api/messages")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(messageDTO)))
            .andExpect(status().isCreated());

        List<Message> messageList = messageRepository.findAll();
        assertThat(messageList).hasSize(databaseSizeBeforeCreate + 1);
        Message testMessage = messageList.get(messageList.size() - 1);
        assertThat(testMessage.getValue()).isEqualTo(MESSAGE_VALUE);

        List<Notification> notifications = notificationJpaRepository.findAll();
        assertThat(notifications.size()).isEqualTo(1);
        Notification notification = notifications.get(0);
        assertThat(notification.getLink()).isEqualTo(String.format("/chats?journeyId=%s", journey.getId()));
        assertThat(notification.getMessage()).isEqualTo("{RECEIVED_MESSAGE}");
    }

    @Test
    @Transactional
    void createMessageWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = messageRepository.findAll().size();
        message.setId(1L);
        MessageDTO messageDTO = messageMapper.toDto(message);

        mockMvc.perform(post("/api/messages")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(messageDTO)))
            .andExpect(status().isBadRequest());

        List<Message> messages = messageRepository.findAll();
        assertThat(messages).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkValueIsRequired() throws Exception {
        int databaseSizeBeforeTest = messageRepository.findAll().size();
        message.setValue(null);
        MessageDTO messageDTO = messageMapper.toDto(message);

        mockMvc.perform(post("/api/messages")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(messageDTO)))
            .andExpect(status().isBadRequest());

        List<Message> messages = messageRepository.findAll();
        assertThat(messages).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllMessages() throws Exception {
        messageRepository.saveAndFlush(message);

        mockMvc
            .perform(get("/api/messages?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*]").value(empty()));
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void getAllMessagesByJourneyId() throws Exception {
        User student = getStudent(entityManager);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(getTeacher(entityManager))
            .addStudent(student);
        Chat chat = createChat()
            .journey(journey)
            .addPerson(student);

        message.chat(chat)
            .creator(student);

        persistCreatedEntities(entityManager);

        mockMvc
            .perform(get("/api/messages?sort=id,desc&journeyId={id}", journey.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(message.getId().intValue())))
            .andExpect(jsonPath("$.[*].value").value(hasItem(MESSAGE_VALUE)));
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void getAllMessagesBySubmissionId() throws Exception {
        Submission submission = createSubmission();
        Chat chat = createChat();
        User student = getStudent(entityManager);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(getTeacher(entityManager))
            .addStudent(student);

        chat.setJourney(journey);
        chat.setSubmission(submission);
        chat.addPerson(student);
        message.setChat(chat);
        message.setCreator(student);

        persistCreatedEntities(entityManager);

        mockMvc
            .perform(get("/api/messages?sort=id,desc&submissionId={id}", submission.getId()))
            .andExpect(status().isOk())
            .andExpect(content()
                .contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(message.getId().intValue())))
            .andExpect(jsonPath("$.[*].value").value(hasItem(MESSAGE_VALUE)));
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void shouldIgnoreSubmissionChatMessagesWhenRequestingJourneyChatMessages() throws Exception {
        User student = getStudent(entityManager);
        Submission submission = createSubmission();
        Message submissionMessage = createMessage();
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(getTeacher(entityManager))
            .addStudent(student);
        Chat journeyChat = createChat()
            .journey(journey)
            .addPerson(student);

        Chat submissionChat = createChat()
            .journey(journey)
            .submission(submission)
            .addPerson(student);

        message.chat(journeyChat)
            .creator(student);
        submissionMessage.chat(submissionChat)
            .creator(student);

        persistCreatedEntities(entityManager);

        mockMvc
            .perform(get("/api/messages?sort=id,desc&journeyId={id}", journey.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.size()").value(1))
            .andExpect(jsonPath("$.[*].id").value(hasItem(message.getId().intValue())))
            .andExpect(jsonPath("$.[*].value").value(hasItem(MESSAGE_VALUE)));
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void getAllJourneyMessageViews() throws Exception {
        User student = getStudent(entityManager);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .title(JOURNEY_TITLE)
            .creator(getTeacher(entityManager))
            .setSubject(getSubject(entityManager))
            .addStudent(student);
        Chat chat = createChat()
            .journey(journey)
            .addPerson(student);

        message.chat(chat)
            .creator(getTeacher(entityManager));

        persistCreatedEntities(entityManager);

        mockMvc
            .perform(get("/api/messages/views?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content()
                .contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].messageValue").value(hasItem(MESSAGE_VALUE)))
            .andExpect(jsonPath("$.[*].journeyName").value(hasItem(JOURNEY_TITLE)))
            .andExpect(jsonPath("$.[*].assignmentName").value(hasItem(nullValue())))
            .andExpect(jsonPath("$.[*].read").value(hasItem(nullValue())))
            .andExpect(jsonPath("$.[*].subject.label").value(SUBJECT_LABEL))
            .andExpect(jsonPath("$.[*].authorName").value(hasItem("Teacher Teacher")));
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void getAllSubmissionMessageViews() throws Exception {
        User student = getStudent(entityManager);
        Submission submission = createSubmission();
        User teacher = getTeacher(entityManager);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .title(JOURNEY_TITLE)
            .creator(teacher)
            .addStudent(student)
            .setSubject(getSubject(entityManager))
            .addMilestone(createMilestone()
                .creator(teacher)
                .addAssignment(createAssignment()
                    .gradingScheme(getGradingSchemeNumerical(entityManager))
                    .creator(teacher)
                    .addSubmission(submission)));

        Chat chat = createChat()
            .journey(journey)
            .submission(submission)
            .addPerson(student);

        message.chat(chat)
            .creator(teacher);

        persistCreatedEntities(entityManager);

        mockMvc
            .perform(get("/api/messages/views?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content()
                .contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].messageValue").value(hasItem(MESSAGE_VALUE)))
            .andExpect(jsonPath("$.[*].journeyName").value(hasItem(JOURNEY_TITLE)))
            .andExpect(jsonPath("$.[*].assignmentName").value(ASSIGNMENT_TITLE))
            .andExpect(jsonPath("$.[*].read").value(hasItem(nullValue())))
            .andExpect(jsonPath("$.[*].subject.label").value(SUBJECT_LABEL))
            .andExpect(jsonPath("$.[*].authorName").value(hasItem("Teacher Teacher")));
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void getSubmissionFeedbackMessage() throws Exception {
        Chat chat = createChat();
        User student = getStudent(entityManager);
        Submission submission = createSubmission();
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(getTeacher(entityManager))
            .addStudent(student);

        chat.setJourney(journey);
        chat.addMessage(message);
        chat.setSubmission(submission);
        message.setCreator(student);
        message.setFeedback(true);
        chat.addPerson(student);
        persistCreatedEntities(entityManager);

        mockMvc
            .perform(get("/api/messages?submissionId={id}&feedback={feedback}", submission.getId(), true))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.[0].id").value(message.getId().intValue()))
            .andExpect(jsonPath("$.[0].value").value(MESSAGE_VALUE));
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void getMessage() throws Exception {
        User student = getStudent(entityManager);
        Chat chat = createChat()
            .addPerson(student);
        message.creator(student)
            .chat(chat);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(getTeacher(entityManager))
            .addStudent(student);
        persistCreatedEntities(entityManager);

        chat.setJourney(journey);
        mockMvc.perform(get("/api/messages/{id}", message.getId()))
            .andExpect(status().isOk())
            .andExpect(content()
                .contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(message.getId().intValue()))
            .andExpect(jsonPath("$.value").value(MESSAGE_VALUE));
    }

    @Test
    @Transactional
    void getNonExistingMessage() throws Exception {
        mockMvc.perform(get("/api/messages/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void updateMessageAndCreateNotification() throws Exception {
        User student = createStudent().setId(STUDENT_ID);
        userRepository.save(student);
        User teacher = getTeacher(entityManager);
        userRepository.save(teacher);
        Journey journey = createJourney()
            .creator(teacher)
            .addStudent(student)
            .addTeacher(teacher)
            .educationalLevel(new EducationalLevel().id(1L));
        journeyRepository.save(journey);
        Chat chat = createChat()
            .people(Set.of(student, teacher))
            .journey(journey);
        chatJpaRepository.save(chat);
        Message message = createMessage()
            .creator(teacher)
            .chat(chat);
        messageRepository.save(message);
        int databaseSizeBeforeUpdate = messageRepository.findAll().size();
        Message updatedMessage = messageRepository.findById(message.getId()).get();
        entityManager.detach(updatedMessage);
        updatedMessage.value(UPDATED_VALUE);
        MessageDTO messageDTO = messageMapper.toDto(updatedMessage);

        mockMvc.perform(put("/api/messages")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(messageDTO)))
            .andExpect(status().isOk());

        List<Message> messages = messageRepository.findAll();
        assertThat(messages).hasSize(databaseSizeBeforeUpdate);
        message = messages.get(messages.size() - 1);
        assertThat(message.getValue()).isEqualTo(UPDATED_VALUE);

        List<Notification> notifications = notificationJpaRepository.findAll();
        assertThat(notifications.size()).isEqualTo(1);
        Notification notification = notifications.get(0);
        assertThat(notification.getLink()).isEqualTo(String.format("/chats?journeyId=%s", journey.getId()));
        assertThat(notification.getMessage()).isEqualTo("{RECEIVED_MESSAGE}");
    }

    @Test
    @Transactional
    void updateNonExistingMessage() throws Exception {
        int databaseSizeBeforeUpdate = messageRepository.findAll().size();
        MessageDTO messageDTO = messageMapper.toDto(message);

        mockMvc.perform(put("/api/messages")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(messageDTO)))
            .andExpect(status().isBadRequest());

        List<Message> messages = messageRepository.findAll();
        assertThat(messages).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldNotAllowToUpdateWhenUserNotTheCreatorOfTheMessage() throws Exception {
        User messageCreator = createTeacher().setLogin("messageCreator");
        Journey journey = createJourney()
            .educationalLevel(new EducationalLevel().id(1L));
        Chat chat = createChat().journey(journey);
        message
            .chat(chat)
            .setCreator(messageCreator);
        transactionHelper.withTransaction(() -> {
            User teacher = getTeacher(entityManager);
            journey
                .addTeacher(teacher)
                .addTeacher(messageCreator)
                .creator(messageCreator);
            chat
                .addPerson(teacher)
                .addPerson(messageCreator);
            persistCreatedEntities(entityManager);
        });
        MessageDTO messageDTO = messageMapper.toDto(message);

        mockMvc.perform(put("/api/messages")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(messageDTO)))
            .andExpect(status().isForbidden())
            .andExpect(jsonPath("$.detail").value("403 FORBIDDEN \"User is not the creator of the message!\""));
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void deleteMessage() throws Exception {
        User student = getStudent(entityManager);
        Chat chat = createChat()
            .addPerson(student);
        message.creator(student)
            .chat(chat);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(getTeacher(entityManager))
            .addStudent(student);
        persistCreatedEntities(entityManager);
        int databaseSizeBeforeDelete = messageRepository.findAll().size();

        chat.setJourney(journey);
        mockMvc.perform(delete("/api/messages/{id}", message.getId())
                .accept(APPLICATION_JSON))
            .andExpect(status().isNoContent());

        List<Message> messages = messageRepository.findAll();
        assertThat(messages).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldNotAllowToDeleteWhenUserNotTheCreatorOfTheMessage() throws Exception {
        User messageCreator = createTeacher().setLogin("messageCreator");
        Journey journey = createJourney()
            .educationalLevel(new EducationalLevel().id(1L));
        Chat chat = createChat().journey(journey);
        message
            .chat(chat)
            .setCreator(messageCreator);
        transactionHelper.withTransaction(() -> {
            User teacher = getTeacher(entityManager);
            journey
                .addTeacher(teacher)
                .addTeacher(messageCreator)
                .creator(messageCreator);
            chat
                .addPerson(teacher)
                .addPerson(messageCreator);
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(delete("/api/messages/{id}", message.getId()))
            .andExpect(status().isForbidden())
            .andExpect(jsonPath("$.detail").value("403 FORBIDDEN \"User is not the creator of the message!\""));
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void shouldThrowOnGetMessageWhenTeacherNotInJourney() throws Exception {
        User student = getStudent(entityManager);
        Chat chat = createChat()
            .addPerson(student);
        message.creator(student)
            .chat(chat);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(getTeacher(entityManager));
        persistCreatedEntities(entityManager);

        chat.setJourney(journey);

        mockMvc.perform(get("/api/messages/{id}", message.getId()))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void shouldThrowOnGetMessageWhenStudentNotInJourney() throws Exception {
        User student = getStudent(entityManager);
        Chat chat = createChat()
            .addPerson(student);
        message.creator(student)
            .chat(chat);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(getTeacher(entityManager));
        persistCreatedEntities(entityManager);

        chat.setJourney(journey);

        mockMvc.perform(get("/api/messages/{id}", message.getId()))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void shouldGetEmptyResponseOnGetAllMessagesByJourneyIdWhenStudentNotInJourney() throws Exception {
        User student = getStudent(entityManager);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(getTeacher(entityManager));
        Chat chat = createChat()
            .journey(journey)
            .addPerson(student);

        message.chat(chat)
            .creator(student);

        persistCreatedEntities(entityManager);

        mockMvc
            .perform(get("/api/messages?sort=id,desc&journeyId={id}", journey.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("length()").value(0));
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void shouldGetEmptyResponseOnGetAllMessagesByJourneyIdWhenTeacherNotInJourney() throws Exception {
        User student = getStudent(entityManager);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(getTeacher(entityManager));
        Chat chat = createChat()
            .journey(journey)
            .addPerson(student);

        message.chat(chat)
            .creator(student);

        persistCreatedEntities(entityManager);

        mockMvc
            .perform(get("/api/messages?sort=id,desc&journeyId={id}", journey.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("length()").value(0));
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void shouldThrowOnUpdateWhenTeacherNotInJourney() throws Exception {
        User teacher = getTeacher(entityManager);
        Chat chat = createChat();
        message.setChat(chat);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(teacher);
        chat.setJourney(journey);
        persistCreatedEntities(entityManager);
        messageRepository.saveAndFlush(message);

        Message updatedMessage = messageRepository.findById(message.getId()).get();
        entityManager.detach(updatedMessage);
        updatedMessage.value(UPDATED_VALUE);
        MessageDTO messageDTO = messageMapper.toDto(updatedMessage);

        mockMvc.perform(put("/api/messages")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(messageDTO)))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void shouldThrowOnUpdateWhenMessageNotPresent() throws Exception {
        User teacher = getTeacher(entityManager);
        Chat chat = createChat();
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(teacher);
        chat.setJourney(journey);

        persistCreatedEntities(entityManager);

        MessageDTO messageDTO = new MessageDTO();
        messageDTO.setId(1L);
        messageDTO.setChatId(chat.getId());
        messageDTO.setValue(UPDATED_VALUE);
        mockMvc.perform(put("/api/messages")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(messageDTO)))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void shouldThrowOnDeleteWhenTeacherNotInJourney() throws Exception {
        User student = getStudent(entityManager);
        Chat chat = createChat()
            .addPerson(student);
        message.creator(student)
            .chat(chat);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(getTeacher(entityManager))
            .addStudent(student);
        persistCreatedEntities(entityManager);

        chat.setJourney(journey);
        mockMvc.perform(delete("/api/messages/{id}", message.getId())
                .accept(APPLICATION_JSON))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void shouldThrowOnDeleteWhenMessageNotPresent() throws Exception {
        mockMvc.perform(delete("/api/messages/{id}", 1)
                .accept(APPLICATION_JSON))
            .andExpect(status().isNotFound());
    }
}
