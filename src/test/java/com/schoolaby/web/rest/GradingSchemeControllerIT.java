package com.schoolaby.web.rest;

import com.schoolaby.common.BaseIntegrationTest;
import com.schoolaby.domain.GradingScheme;
import com.schoolaby.repository.GradingSchemeRepository;
import com.schoolaby.service.dto.GradingSchemeDTO;
import com.schoolaby.service.mapper.GradingSchemeMapper;
import com.schoolaby.web.WithMockCustomUser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.schoolaby.security.Role.Constants.ADMIN;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WithMockCustomUser(authorities = {ADMIN})
public class GradingSchemeControllerIT extends BaseIntegrationTest {
    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";
    private static final String DEFAULT_CODE = "ALPHABETICAL";
    private static final String UPDATED_CODE = "NUMERICAL";
    private static final Integer DEFAULT_SEQUENCE_NUMBER = 1;

    @Autowired
    private GradingSchemeRepository gradingSchemeRepository;
    @Autowired
    private GradingSchemeMapper gradingSchemeMapper;

    private GradingScheme gradingScheme;

    public static GradingScheme createEntity(EntityManager em) {
        return new GradingScheme()
            .name(DEFAULT_NAME)
            .code(DEFAULT_CODE)
            .setSequenceNumber(DEFAULT_SEQUENCE_NUMBER)
            .addValue(GradingSchemeValueControllerIT.createEntity(em));
    }

    @BeforeEach
    public void initTest() {
        gradingScheme = createEntity(entityManager);
    }

    @Test
    @Transactional
    public void createGradingScheme() throws Exception {
        int databaseSizeBeforeCreate = gradingSchemeRepository.findAll().size();
        GradingSchemeDTO gradingSchemeDTO = gradingSchemeMapper.toDto(gradingScheme);

        mockMvc.perform(post("/api/grading-schemes")
            .contentType(APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(gradingSchemeDTO)))
            .andExpect(status().isCreated());

        List<GradingScheme> gradingSchemes = gradingSchemeRepository.findAll();
        assertThat(gradingSchemes).hasSize(databaseSizeBeforeCreate + 1);
        GradingScheme gradingScheme = gradingSchemes.get(gradingSchemes.size() - 1);
        assertThat(gradingScheme.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createGradingSchemeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = gradingSchemeRepository.findAll().size();
        gradingScheme.setId(1L);
        GradingSchemeDTO gradingSchemeDTO = gradingSchemeMapper.toDto(gradingScheme);

        mockMvc.perform(post("/api/grading-schemes")
            .contentType(APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(gradingSchemeDTO)))
            .andExpect(status().isBadRequest());

        List<GradingScheme> gradingSchemes = gradingSchemeRepository.findAll();
        assertThat(gradingSchemes).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = gradingSchemeRepository.findAll().size();
        gradingScheme.setName(null);
        GradingSchemeDTO gradingSchemeDTO = gradingSchemeMapper.toDto(gradingScheme);

        mockMvc.perform(post("/api/grading-schemes")
            .contentType(APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(gradingSchemeDTO)))
            .andExpect(status().isBadRequest());

        List<GradingScheme> gradingSchemes = gradingSchemeRepository.findAll();
        assertThat(gradingSchemes).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllGradingSchemes() throws Exception {
        gradingSchemeRepository.saveAndFlush(gradingScheme);

        mockMvc.perform(get("/api/grading-schemes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(gradingScheme.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
    }

    @Test
    @Transactional
    public void getGradingScheme() throws Exception {
        gradingSchemeRepository.saveAndFlush(gradingScheme);

        mockMvc.perform(get("/api/grading-schemes/{id}", gradingScheme.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(gradingScheme.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
    }

    @Test
    @Transactional
    public void getNonExistingGradingScheme() throws Exception {
        mockMvc.perform(get("/api/grading-schemes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateGradingScheme() throws Exception {
        gradingSchemeRepository.saveAndFlush(gradingScheme);
        int databaseSizeBeforeUpdate = gradingSchemeRepository.findAll().size();

        GradingScheme updatedGradingScheme = gradingSchemeRepository.findById(gradingScheme.getId()).get();
        entityManager.detach(updatedGradingScheme);
        updatedGradingScheme.name(UPDATED_NAME)
            .code(UPDATED_CODE);
        GradingSchemeDTO gradingSchemeDTO = gradingSchemeMapper.toDto(updatedGradingScheme);

        mockMvc.perform(put("/api/grading-schemes")
            .contentType(APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(gradingSchemeDTO)))
            .andExpect(status().isOk());

        List<GradingScheme> gradingSchemes = gradingSchemeRepository.findAll();
        assertThat(gradingSchemes).hasSize(databaseSizeBeforeUpdate);
        GradingScheme gradingScheme = gradingSchemes.get(gradingSchemes.size() - 1);
        assertThat(gradingScheme.getName()).isEqualTo(UPDATED_NAME);
        assertThat(gradingScheme.getCode()).isEqualTo(UPDATED_CODE);
    }

    @Test
    @Transactional
    public void updateNonExistingGradingScheme() throws Exception {
        int databaseSizeBeforeUpdate = gradingSchemeRepository.findAll().size();
        GradingSchemeDTO gradingSchemeDTO = gradingSchemeMapper.toDto(gradingScheme);

        mockMvc.perform(put("/api/grading-schemes")
            .contentType(APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(gradingSchemeDTO)))
            .andExpect(status().isBadRequest());

        List<GradingScheme> gradingSchemes = gradingSchemeRepository.findAll();
        assertThat(gradingSchemes).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteGradingScheme() throws Exception {
        gradingSchemeRepository.saveAndFlush(gradingScheme);
        int databaseSizeBeforeDelete = gradingSchemeRepository.findAll().size();

        mockMvc.perform(delete("/api/grading-schemes/{id}", gradingScheme.getId())
            .accept(APPLICATION_JSON))
            .andExpect(status().isNoContent());

        List<GradingScheme> gradingSchemes = gradingSchemeRepository.findAll();
        assertThat(gradingSchemes).hasSize(databaseSizeBeforeDelete - 1);
    }
}
