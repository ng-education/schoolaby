package com.schoolaby.web.rest;

import com.schoolaby.common.BaseIntegrationTest;
import com.schoolaby.domain.Authority;
import com.schoolaby.domain.PersonRole;
import com.schoolaby.domain.School;
import com.schoolaby.domain.User;
import com.schoolaby.repository.UserRepository;
import com.schoolaby.security.Role;
import com.schoolaby.service.dto.SchoolDTO;
import com.schoolaby.service.dto.UserDetailedDTO;
import com.schoolaby.service.mapper.UserDetailedMapper;
import com.schoolaby.web.rest.vm.ManagedUserVM;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.*;
import java.util.function.Consumer;

import static com.schoolaby.common.TestObjects.*;
import static com.schoolaby.common.TestUtils.mapPersonRoleToDTO;
import static com.schoolaby.repository.UserRepository.USERS_BY_LOGIN_CACHE;
import static com.schoolaby.security.Role.Constants.ADMIN;
import static com.schoolaby.security.Role.Constants.STUDENT;
import static com.schoolaby.web.rest.TestUtil.convertObjectToJsonBytes;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WithMockUser(authorities = ADMIN)
class UserControllerIT extends BaseIntegrationTest {
    private static final String DEFAULT_LOGIN = "johndoe";
    private static final String UPDATED_LOGIN = "jhipster";

    private static final Long DEFAULT_ID = 1L;

    private static final String DEFAULT_PASSWORD = "passjohndoe";
    private static final String UPDATED_PASSWORD = "passjhipster";

    private static final String DEFAULT_EMAIL = "johndoe@localhost";
    private static final String UPDATED_EMAIL = "jhipster@localhost";

    private static final String DEFAULT_FIRSTNAME = "john";
    private static final String UPDATED_FIRSTNAME = "jhipsterFirstName";

    private static final String DEFAULT_LASTNAME = "doe";
    private static final String UPDATED_LASTNAME = "jhipsterLastName";

    private static final String DEFAULT_IMAGEURL = "http://placehold.it/50x50";
    private static final String UPDATED_IMAGEURL = "http://placehold.it/40x40";

    private static final String DEFAULT_LANGKEY = "en";
    private static final String UPDATED_LANGKEY = "et";

    private static final String DEFAULT_COUNTRY = "Estonia";

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserDetailedMapper userDetailedMapper;

    private User user;

    public static User createEntity() {
        return User.builder()
            .login(DEFAULT_LOGIN + RandomStringUtils.randomAlphabetic(5))
            .password(RandomStringUtils.random(60))
            .activated(true)
            .email(RandomStringUtils.randomAlphabetic(5) + DEFAULT_EMAIL)
            .firstName(DEFAULT_FIRSTNAME)
            .lastName(DEFAULT_LASTNAME)
            .imageUrl(DEFAULT_IMAGEURL)
            .langKey(DEFAULT_LANGKEY).build();
    }

    @BeforeEach
    void initTest() {
        user = createEntity();
        user.setLogin(DEFAULT_LOGIN);
        user.setEmail(DEFAULT_EMAIL);
    }

    @Test
    void createUser() throws Exception {
        int databaseSizeBeforeCreate = userRepository.findAll().size();
        School school = createSchool();
        transactionHelper.withNewTransaction(() -> persistCreatedEntities(entityManager));
        PersonRole personRole = createPersonRole().school(school).role(Role.STUDENT);

        ManagedUserVM managedUserVM = new ManagedUserVM();
        managedUserVM.setLogin(DEFAULT_LOGIN);
        managedUserVM.setPassword(DEFAULT_PASSWORD);
        managedUserVM.setFirstName(DEFAULT_FIRSTNAME);
        managedUserVM.setLastName(DEFAULT_LASTNAME);
        managedUserVM.setEmail(DEFAULT_EMAIL);
        managedUserVM.setCountry(DEFAULT_COUNTRY);
        managedUserVM.setActivated(true);
        managedUserVM.setImageUrl(DEFAULT_IMAGEURL);
        managedUserVM.setLangKey(DEFAULT_LANGKEY);
        managedUserVM.setAuthorities(Collections.singleton(STUDENT));
        managedUserVM.setPersonRoles(Set.of(mapPersonRoleToDTO(personRole)));

        mockMvc.perform(post("/api/users")
            .contentType(APPLICATION_JSON)
            .content(convertObjectToJsonBytes(managedUserVM)))
            .andExpect(status().isCreated());

        transactionHelper.withNewTransaction(() ->
            assertPersistedUsers(
                users -> {
                    assertThat(users).hasSize(databaseSizeBeforeCreate + 1);
                    User testUser = users.get(users.size() - 1);
                    assertThat(testUser.getLogin()).isEqualTo(DEFAULT_LOGIN);
                    assertThat(testUser.getFirstName()).isEqualTo(DEFAULT_FIRSTNAME);
                    assertThat(testUser.getLastName()).isEqualTo(DEFAULT_LASTNAME);
                    assertThat(testUser.getEmail()).isEqualTo(DEFAULT_EMAIL);
                    assertThat(testUser.getCountry()).isEqualTo(DEFAULT_COUNTRY);
                    assertThat(testUser.getImageUrl()).isEqualTo(DEFAULT_IMAGEURL);
                    assertThat(testUser.getLangKey()).isEqualTo(DEFAULT_LANGKEY);
                    assertThat(testUser.getPersonRoles().size()).isEqualTo(1);
                    assertNotNull(new ArrayList<>(testUser.getPersonRoles()).get(0).getId());
                }
            ));
    }

    @Test
    @Transactional
    void createUserWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = userRepository.findAll().size();

        ManagedUserVM managedUserVM = new ManagedUserVM();
        managedUserVM.setId(1L);
        managedUserVM.setLogin(DEFAULT_LOGIN);
        managedUserVM.setPassword(DEFAULT_PASSWORD);
        managedUserVM.setFirstName(DEFAULT_FIRSTNAME);
        managedUserVM.setLastName(DEFAULT_LASTNAME);
        managedUserVM.setEmail(DEFAULT_EMAIL);
        managedUserVM.setActivated(true);
        managedUserVM.setImageUrl(DEFAULT_IMAGEURL);
        managedUserVM.setLangKey(DEFAULT_LANGKEY);
        managedUserVM.setAuthorities(Collections.singleton(STUDENT));

        mockMvc.perform(post("/api/users")
            .contentType(APPLICATION_JSON)
            .content(convertObjectToJsonBytes(managedUserVM)))
            .andExpect(status().isBadRequest());

        assertPersistedUsers(users -> assertThat(users).hasSize(databaseSizeBeforeCreate));
    }

    @Test
    @Transactional
    void createUserWithExistingLogin() throws Exception {
        userRepository.saveAndFlush(user);
        int databaseSizeBeforeCreate = userRepository.findAll().size();

        ManagedUserVM managedUserVM = new ManagedUserVM();
        managedUserVM.setLogin(DEFAULT_LOGIN); // this login should already be used
        managedUserVM.setPassword(DEFAULT_PASSWORD);
        managedUserVM.setFirstName(DEFAULT_FIRSTNAME);
        managedUserVM.setLastName(DEFAULT_LASTNAME);
        managedUserVM.setEmail("anothermail@localhost");
        managedUserVM.setActivated(true);
        managedUserVM.setImageUrl(DEFAULT_IMAGEURL);
        managedUserVM.setLangKey(DEFAULT_LANGKEY);
        managedUserVM.setAuthorities(Collections.singleton(STUDENT));

        mockMvc.perform(post("/api/users")
            .contentType(APPLICATION_JSON)
            .content(convertObjectToJsonBytes(managedUserVM)))
            .andExpect(status().isBadRequest());

        assertPersistedUsers(users -> assertThat(users).hasSize(databaseSizeBeforeCreate));
    }

    @Test
    @Transactional
    void createUserWithExistingEmail() throws Exception {
        userRepository.saveAndFlush(user);
        int databaseSizeBeforeCreate = userRepository.findAll().size();

        ManagedUserVM managedUserVM = new ManagedUserVM();
        managedUserVM.setLogin("anotherlogin");
        managedUserVM.setPassword(DEFAULT_PASSWORD);
        managedUserVM.setFirstName(DEFAULT_FIRSTNAME);
        managedUserVM.setLastName(DEFAULT_LASTNAME);
        managedUserVM.setEmail(DEFAULT_EMAIL); // this email should already be used
        managedUserVM.setActivated(true);
        managedUserVM.setImageUrl(DEFAULT_IMAGEURL);
        managedUserVM.setLangKey(DEFAULT_LANGKEY);
        managedUserVM.setAuthorities(Collections.singleton(STUDENT));

        mockMvc.perform(post("/api/users")
            .contentType(APPLICATION_JSON)
            .content(convertObjectToJsonBytes(managedUserVM)))
            .andExpect(status().isBadRequest());

        assertPersistedUsers(users -> assertThat(users).hasSize(databaseSizeBeforeCreate));
    }

    @Test
    @Transactional
    void getAllUsers() throws Exception {
        userRepository.saveAndFlush(user);

        mockMvc.perform(get("/api/users?sort=id,desc").accept(APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content()
                .contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].login").value(hasItem(DEFAULT_LOGIN)))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRSTNAME)))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LASTNAME)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].imageUrl").value(hasItem(DEFAULT_IMAGEURL)))
            .andExpect(jsonPath("$.[*].langKey").value(hasItem(DEFAULT_LANGKEY)));
    }

    @Test
    @Transactional
    void getUser() throws Exception {
        userRepository.saveAndFlush(user);

        assertThat(cacheManager.getCache(USERS_BY_LOGIN_CACHE).get(user.getLogin())).isNull();

        mockMvc.perform(get("/api/users/{login}", user.getLogin()))
            .andExpect(status().isOk())
            .andExpect(content()
                .contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.login").value(user.getLogin()))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRSTNAME))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LASTNAME))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.imageUrl").value(DEFAULT_IMAGEURL))
            .andExpect(jsonPath("$.langKey").value(DEFAULT_LANGKEY));

        assertThat(cacheManager.getCache(USERS_BY_LOGIN_CACHE).get(user.getLogin())).isNotNull();
    }

    @Test
    @Transactional
    void getNonExistingUser() throws Exception {
        mockMvc.perform(get("/api/users/unknown"))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void updateUser() throws Exception {
        userRepository.saveAndFlush(user);
        int databaseSizeBeforeUpdate = userRepository.findAll().size();
        User updatedUser = userRepository.findById(user.getId()).get();

        ManagedUserVM managedUserVM = new ManagedUserVM();
        managedUserVM.setId(updatedUser.getId());
        managedUserVM.setLogin(updatedUser.getLogin());
        managedUserVM.setPassword(UPDATED_PASSWORD);
        managedUserVM.setFirstName(UPDATED_FIRSTNAME);
        managedUserVM.setLastName(UPDATED_LASTNAME);
        managedUserVM.setEmail(UPDATED_EMAIL);
        managedUserVM.setActivated(updatedUser.isActivated());
        managedUserVM.setImageUrl(UPDATED_IMAGEURL);
        managedUserVM.setLangKey(UPDATED_LANGKEY);
        managedUserVM.setAuthorities(Collections.singleton(STUDENT));

        mockMvc.perform(put("/api/users")
            .contentType(APPLICATION_JSON)
            .content(convertObjectToJsonBytes(managedUserVM)))
            .andExpect(status().isOk());

        assertPersistedUsers(
            users -> {
                assertThat(users).hasSize(databaseSizeBeforeUpdate);
                User testUser = users.get(users.size() - 1);
                assertThat(testUser.getFirstName()).isEqualTo(UPDATED_FIRSTNAME);
                assertThat(testUser.getLastName()).isEqualTo(UPDATED_LASTNAME);
                assertThat(testUser.getEmail()).isEqualTo(UPDATED_EMAIL);
                assertThat(testUser.getImageUrl()).isEqualTo(UPDATED_IMAGEURL);
                assertThat(testUser.getLangKey()).isEqualTo(UPDATED_LANGKEY);
            }
        );
    }

    @Test
    void updateUserPersonRoles() throws Exception {
        School school = createSchool();
        School secondSchool = createSchool()
            .ehisId("ehis_id_2")
            .name("Second school");
        PersonRole personRole = createPersonRole().school(school);
        transactionHelper.withNewTransaction(() -> {
            persistCreatedEntities(entityManager);
            user.setPersonRoles(Set.of(personRole));
            userRepository.saveAndFlush(user);
        });
        PersonRole secondPersonRole = createPersonRole().school(secondSchool).role(Role.STUDENT);

        SchoolDTO secondSchoolDTO = new SchoolDTO()
            .setId(secondSchool.getId())
            .setEhisId(secondSchool.getEhisId())
            .setRegNr(secondSchool.getRegNr())
            .setName(secondSchool.getName());
        ManagedUserVM managedUserVM = new ManagedUserVM();
        managedUserVM.setId(user.getId());
        managedUserVM.setLogin(user.getLogin());
        managedUserVM.setPassword(user.getPassword());
        managedUserVM.setFirstName(user.getFirstName());
        managedUserVM.setLastName(user.getLastName());
        managedUserVM.setEmail(user.getEmail());
        managedUserVM.setActivated(user.isActivated());
        managedUserVM.setImageUrl(user.getImageUrl());
        managedUserVM.setLangKey(user.getLangKey());
        managedUserVM.setAuthorities(Collections.singleton(STUDENT));
        managedUserVM.setPersonRoles(Set.of(mapPersonRoleToDTO(secondPersonRole)));

        mockMvc.perform(put("/api/users")
            .contentType(APPLICATION_JSON)
            .content(convertObjectToJsonBytes(managedUserVM)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.personRoles[*]", hasSize(1)))
            .andExpect(jsonPath("$.personRoles[0].role").value(secondPersonRole.getRole().toString()))
            .andExpect(jsonPath("$.personRoles[0].active").value(secondPersonRole.isActive()))
            .andExpect(jsonPath("$.personRoles[0].grade").value(secondPersonRole.getGrade()))
            .andExpect(jsonPath("$.personRoles[0].school.ehisId").value(secondSchoolDTO.getEhisId()))
            .andExpect(jsonPath("$.personRoles[0].school.regNr").value(secondSchoolDTO.getRegNr()))
            .andExpect(jsonPath("$.personRoles[0].school.name").value(secondSchoolDTO.getName()));
    }

    @Test
    @Transactional
    void updateUserLogin() throws Exception {
        userRepository.saveAndFlush(user);
        int databaseSizeBeforeUpdate = userRepository.findAll().size();
        User updatedUser = userRepository.findById(user.getId()).get();

        ManagedUserVM managedUserVM = new ManagedUserVM();
        managedUserVM.setId(updatedUser.getId());
        managedUserVM.setLogin(UPDATED_LOGIN);
        managedUserVM.setPassword(UPDATED_PASSWORD);
        managedUserVM.setFirstName(UPDATED_FIRSTNAME);
        managedUserVM.setLastName(UPDATED_LASTNAME);
        managedUserVM.setEmail(UPDATED_EMAIL);
        managedUserVM.setActivated(updatedUser.isActivated());
        managedUserVM.setImageUrl(UPDATED_IMAGEURL);
        managedUserVM.setLangKey(UPDATED_LANGKEY);
        managedUserVM.setAuthorities(Collections.singleton(STUDENT));

        mockMvc.perform(put("/api/users")
            .contentType(APPLICATION_JSON)
            .content(convertObjectToJsonBytes(managedUserVM)))
            .andExpect(status().isOk());

        assertPersistedUsers(
            users -> {
                assertThat(users).hasSize(databaseSizeBeforeUpdate);
                User testUser = users.get(users.size() - 1);
                assertThat(testUser.getLogin()).isEqualTo(UPDATED_LOGIN);
                assertThat(testUser.getFirstName()).isEqualTo(UPDATED_FIRSTNAME);
                assertThat(testUser.getLastName()).isEqualTo(UPDATED_LASTNAME);
                assertThat(testUser.getEmail()).isEqualTo(UPDATED_EMAIL);
                assertThat(testUser.getImageUrl()).isEqualTo(UPDATED_IMAGEURL);
                assertThat(testUser.getLangKey()).isEqualTo(UPDATED_LANGKEY);
            }
        );
    }

    @Test
    @Transactional
    void updateUserExistingEmail() throws Exception {
        // Initialize the database with 2 users
        userRepository.saveAndFlush(user);

        User anotherUser = new User();
        anotherUser.setLogin("jhipster");
        anotherUser.setPassword(RandomStringUtils.random(60));
        anotherUser.setActivated(true);
        anotherUser.setEmail("jhipster@localhost");
        anotherUser.setFirstName("java");
        anotherUser.setLastName("hipster");
        anotherUser.setImageUrl("");
        anotherUser.setLangKey("en");
        userRepository.saveAndFlush(anotherUser);

        User updatedUser = userRepository.findById(user.getId()).get();

        ManagedUserVM managedUserVM = new ManagedUserVM();
        managedUserVM.setId(updatedUser.getId());
        managedUserVM.setLogin(updatedUser.getLogin());
        managedUserVM.setPassword(updatedUser.getPassword());
        managedUserVM.setFirstName(updatedUser.getFirstName());
        managedUserVM.setLastName(updatedUser.getLastName());
        managedUserVM.setEmail("jhipster@localhost"); // this email should already be used by anotherUser
        managedUserVM.setActivated(updatedUser.isActivated());
        managedUserVM.setImageUrl(updatedUser.getImageUrl());
        managedUserVM.setLangKey(updatedUser.getLangKey());
        managedUserVM.setAuthorities(Collections.singleton(STUDENT));

        mockMvc.perform(put("/api/users")
            .contentType(APPLICATION_JSON)
            .content(convertObjectToJsonBytes(managedUserVM)))
            .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    void updateUserExistingLogin() throws Exception {
        userRepository.saveAndFlush(user);

        User anotherUser = new User();
        anotherUser.setLogin("jhipster");
        anotherUser.setPassword(RandomStringUtils.random(60));
        anotherUser.setActivated(true);
        anotherUser.setEmail("jhipster@localhost");
        anotherUser.setFirstName("java");
        anotherUser.setLastName("hipster");
        anotherUser.setImageUrl("");
        anotherUser.setLangKey("en");
        userRepository.saveAndFlush(anotherUser);

        User updatedUser = userRepository.findById(user.getId()).get();

        ManagedUserVM managedUserVM = new ManagedUserVM();
        managedUserVM.setId(updatedUser.getId());
        managedUserVM.setLogin("jhipster"); // this login should already be used by anotherUser
        managedUserVM.setPassword(updatedUser.getPassword());
        managedUserVM.setFirstName(updatedUser.getFirstName());
        managedUserVM.setLastName(updatedUser.getLastName());
        managedUserVM.setEmail(updatedUser.getEmail());
        managedUserVM.setActivated(updatedUser.isActivated());
        managedUserVM.setImageUrl(updatedUser.getImageUrl());
        managedUserVM.setLangKey(updatedUser.getLangKey());
        managedUserVM.setAuthorities(Collections.singleton(STUDENT));

        mockMvc.perform(put("/api/users")
            .contentType(APPLICATION_JSON)
            .content(convertObjectToJsonBytes(managedUserVM)))
            .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    void deleteUser() throws Exception {
        userRepository.saveAndFlush(user);
        int databaseSizeBeforeDelete = userRepository.findAll().size();

        mockMvc.perform(delete("/api/users/{login}", user.getLogin())
            .accept(APPLICATION_JSON))
            .andExpect(status().isNoContent());

        assertThat(cacheManager.getCache(USERS_BY_LOGIN_CACHE).get(user.getLogin())).isNull();

        assertPersistedUsers(users -> assertThat(users).hasSize(databaseSizeBeforeDelete - 1));
    }

    @Test
    @Transactional
    void getAllAuthorities() throws Exception {
        mockMvc.perform(get("/api/users/authorities").accept(APPLICATION_JSON)
            .contentType(APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content()
                .contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").value(hasItems(ADMIN, Role.Constants.TEACHER, STUDENT)));
    }

    @Test
    @Transactional
    void testUserEquals() throws Exception {
        TestUtil.equalsVerifier(User.class);
        User user1 = new User();
        user1.setId(1L);
        User user2 = new User();
        user2.setId(user1.getId());
        assertThat(user1).isEqualTo(user2);
        user2.setId(2L);
        assertThat(user1).isNotEqualTo(user2);
        user1.setId(null);
        assertThat(user1).isNotEqualTo(user2);
    }

    @Test
    void testUserDTOtoUser() {
        UserDetailedDTO userDTO = new UserDetailedDTO();
        userDTO.setId(DEFAULT_ID);
        userDTO.setLogin(DEFAULT_LOGIN);
        userDTO.setFirstName(DEFAULT_FIRSTNAME);
        userDTO.setLastName(DEFAULT_LASTNAME);
        userDTO.setEmail(DEFAULT_EMAIL);
        userDTO.setCountry(DEFAULT_COUNTRY);
        userDTO.setActivated(true);
        userDTO.setImageUrl(DEFAULT_IMAGEURL);
        userDTO.setLangKey(DEFAULT_LANGKEY);
        userDTO.setAuthorities(Collections.singleton(STUDENT));

        User user = userDetailedMapper.userDTOToUser(userDTO);
        assertThat(user.getId()).isEqualTo(DEFAULT_ID);
        assertThat(user.getLogin()).isEqualTo(DEFAULT_LOGIN);
        assertThat(user.getFirstName()).isEqualTo(DEFAULT_FIRSTNAME);
        assertThat(user.getLastName()).isEqualTo(DEFAULT_LASTNAME);
        assertThat(user.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(user.getCountry()).isEqualTo(DEFAULT_COUNTRY);
        assertThat(user.isActivated()).isTrue();
        assertThat(user.getImageUrl()).isEqualTo(DEFAULT_IMAGEURL);
        assertThat(user.getLangKey()).isEqualTo(DEFAULT_LANGKEY);
        assertThat(user.getCreatedBy()).isNull();
        assertThat(user.getCreatedDate()).isNotNull();
        assertThat(user.getLastModifiedBy()).isNull();
        assertThat(user.getLastModifiedDate()).isNotNull();
        assertThat(user.getAuthorities()).extracting("name").containsExactly(STUDENT);
    }

    @Test
    @Transactional
    void testUserToUserDTO() {
        user.setId(DEFAULT_ID);
        user.setCreatedBy(DEFAULT_LOGIN);
        user.setCreatedDate(Instant.now());
        user.setLastModifiedBy(DEFAULT_LOGIN);
        user.setLastModifiedDate(Instant.now());
        user.setCountry(DEFAULT_COUNTRY);
        Set<Authority> authorities = new HashSet<>();
        Authority authority = new Authority();
        authority.setName(STUDENT);
        authorities.add(authority);
        user.setAuthorities(authorities);

        UserDetailedDTO userDTO = userDetailedMapper.userToUserDTO(user);

        assertThat(userDTO.getId()).isEqualTo(DEFAULT_ID);
        assertThat(userDTO.getLogin()).isEqualTo(DEFAULT_LOGIN);
        assertThat(userDTO.getFirstName()).isEqualTo(DEFAULT_FIRSTNAME);
        assertThat(userDTO.getLastName()).isEqualTo(DEFAULT_LASTNAME);
        assertThat(userDTO.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(userDTO.isActivated()).isTrue();
        assertThat(userDTO.getImageUrl()).isEqualTo(DEFAULT_IMAGEURL);
        assertThat(userDTO.getLangKey()).isEqualTo(DEFAULT_LANGKEY);
        assertThat(userDTO.getAuthorities()).containsExactly(STUDENT);
        assertThat(userDTO.getCountry()).isEqualTo(DEFAULT_COUNTRY);
        assertThat(userDTO.toString()).isNotNull();
    }

    @Test
    @Transactional
    void testAuthorityEquals() {
        Authority authorityA = new Authority();
        assertThat(authorityA).isEqualTo(authorityA);
        assertThat(authorityA).isNotEqualTo(null);
        assertThat(authorityA).isNotEqualTo(new Object());
        assertThat(authorityA.hashCode()).isZero();
        assertThat(authorityA.toString()).isNotNull();

        Authority authorityB = new Authority();
        assertThat(authorityA).isEqualTo(authorityB);

        authorityB.setName(ADMIN);
        assertThat(authorityA).isNotEqualTo(authorityB);

        authorityA.setName(STUDENT);
        assertThat(authorityA).isNotEqualTo(authorityB);

        authorityB.setName(STUDENT);
        assertThat(authorityA).isEqualTo(authorityB);
        assertThat(authorityA).hasSameHashCodeAs(authorityB);
    }

    private void assertPersistedUsers(Consumer<List<User>> userAssertion) {
        userAssertion.accept(userRepository.findAll());
    }
}
