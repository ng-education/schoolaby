package com.schoolaby.web.rest;

import com.schoolaby.common.BaseIntegrationTest;
import com.schoolaby.domain.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.test.context.support.WithUserDetails;

import static com.schoolaby.common.TestCases.*;
import static com.schoolaby.common.TestObjects.*;
import static com.schoolaby.service.dto.LtiVersion.LTI_ADVANTAGE_VERSION;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

class LtiScoreControllerIT extends BaseIntegrationTest {
    private EducationalLevel educationalLevel;
    private EducationalAlignment educationalAlignment;
    private Journey journey;
    private Milestone milestone;
    private Assignment assignment;
    private LtiApp ltiApp = createLtiApp();
    private User teacher;
    private User student;

    @BeforeEach
    void setUp() {
        transactionHelper.withTransaction(() -> {
            educationalLevel = createEducationalLevel();
            educationalAlignment = createEducationalAlignment();
            journey = createJourney();
            milestone = createMilestone();
            assignment = createAssignment();
            ltiApp = createLtiApp();

            teacher = getTeacher(entityManager);
            student = getStudent(entityManager);
            journey.educationalLevel(educationalLevel)
                .creator(teacher)
                .addMilestone(milestone
                    .creator(teacher)
                    .addEducationalAlignment(educationalAlignment)
                    .addAssignment(assignment
                        .creator(teacher)
                        .addStudent(student)
                        .addEducationalAlignment(educationalAlignment)
                        .gradingScheme(getGradingSchemeNumerical(entityManager))));
        });
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldGroupScores_LTI_1_1() throws Exception {
        String r1Title = "r1Title";
        String r2Title = "r2Title";
        LtiResource r1 = createLtiResource();
        LtiResource r2 = createLtiResource();
        LtiLaunch r1LaunchWithoutResult = createLtiLaunch();
        LtiLaunch r2LaunchWithResult = createLtiLaunch();
        LtiLaunch r1LaunchWithResult = createLtiLaunch();
        LtiLaunch r2LaunchWithoutResult = createLtiLaunch();

        assignment
            .addLtiResource(r1
                .setLtiApp(ltiApp)
                .setSyncGrade(true)
                .setTitle(r1Title)
                .addLtiLaunch(r1LaunchWithoutResult
                    .setUser(student)
                    .setResult(null))
                .addLtiLaunch(r1LaunchWithResult
                    .setUser(student)))
            .addLtiResource(r2
                .setLtiApp(ltiApp)
                .setSyncGrade(true)
                .setTitle(r2Title)
                .addLtiLaunch(r2LaunchWithResult
                    .setUser(student))
                .addLtiLaunch(r2LaunchWithoutResult
                    .setUser(student)
                    .setResult(null)));
        transactionHelper.withTransaction(() -> {
            assignment
                .getMilestone()
                .getJourney()
                .addTeacher(getTeacher(entityManager));
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(get("/api/lti-scores")
            .param("assignmentId", assignment.getId().toString())
            .param("userId", STUDENT_ID.toString()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON))
            .andExpect(jsonPath("length()").value(3))

            .andExpect(jsonPath("$.[0].title").value(r2Title))
            .andExpect(jsonPath("$.[0].userId").value(STUDENT_ID))
            .andExpect(jsonPath("$.[0].score").isEmpty())
            .andExpect(jsonPath("$.[0].scoreMax").isEmpty())
            .andExpect(jsonPath("$.[0].createdDate").value(r2LaunchWithoutResult.getCreatedDate().toString()))
            .andExpect(jsonPath("$.[0].launches.length()").value(2))
            .andExpect(jsonPath("$.[0].launches.[*]").value(hasItem(r2LaunchWithoutResult.getCreatedDate().toString())))
            .andExpect(jsonPath("$.[0].launches.[*]").value(hasItem(r2LaunchWithResult.getCreatedDate().toString())))

            .andExpect(jsonPath("$.[1].title").value(r1Title))
            .andExpect(jsonPath("$.[1].userId").value(STUDENT_ID))
            .andExpect(jsonPath("$.[1].score").value(LTI_LAUNCH_RESULT))
            .andExpect(jsonPath("$.[1].scoreMax").isEmpty())
            .andExpect(jsonPath("$.[1].createdDate").value(r1LaunchWithResult.getCreatedDate().toString()))
            .andExpect(jsonPath("$.[1].launches.length()").value(2))
            .andExpect(jsonPath("$.[1].launches.[*]").value(hasItem(r1LaunchWithoutResult.getCreatedDate().toString())))
            .andExpect(jsonPath("$.[1].launches.[*]").value(hasItem(r1LaunchWithResult.getCreatedDate().toString())))

            .andExpect(jsonPath("$.[2].title").value(r2Title))
            .andExpect(jsonPath("$.[2].userId").value(STUDENT_ID))
            .andExpect(jsonPath("$.[2].score").value(LTI_LAUNCH_RESULT))
            .andExpect(jsonPath("$.[2].scoreMax").isEmpty())
            .andExpect(jsonPath("$.[2].createdDate").value(r2LaunchWithResult.getCreatedDate().toString()))
            .andExpect(jsonPath("$.[2].launches.length()").value(1))
            .andExpect(jsonPath("$.[2].launches.[*]").value(hasItem(r2LaunchWithResult.getCreatedDate().toString())));
    }


    @Test
    @WithUserDetails(STUDENT_LOGIN)
    void shouldGroupScores_LTI_1_3() throws Exception {
        String r1Title = "r1Title";
        String r2Title = "r2Title";
        ltiApp.setVersion(LTI_ADVANTAGE_VERSION.getValue());
        LtiResource r1 = createLtiResource();
        LtiResource r2 = createLtiResource();
        LtiLineItem r1LineItem = createLtiLineItem();
        LtiLaunch r1Launch1 = createLtiLaunch();
        LtiLaunch r1Launch2 = createLtiLaunch();
        LtiScore r1Launch2Score = createLtiScore();
        LtiLineItem r2LineItem = createLtiLineItem();
        LtiLaunch r2Launch1 = createLtiLaunch();
        LtiLaunch r2Launch2 = createLtiLaunch();
        LtiScore r2Launch1Score = createLtiScore();


        transactionHelper.withTransaction(() -> {
            assignment
                .addLtiResource(r1
                    .setLtiApp(ltiApp)
                    .setSyncGrade(true)
                    .setTitle(r1Title)
                    .addLtiLineItem(r1LineItem
                        .addScore(r1Launch2Score
                            .setUser(student)))
                    .addLtiLaunch(r1Launch1
                        .setUser(student)
                        .setResult(null))
                    .addLtiLaunch(r1Launch2
                        .setUser(student)
                        .setResult(null)))
                .addLtiResource(r2
                    .setLtiApp(ltiApp)
                    .setSyncGrade(true)
                    .setTitle(r2Title)
                    .addLtiLineItem(r2LineItem
                        .addScore(r2Launch1Score
                            .setUser(student)))
                    .addLtiLaunch(r2Launch1
                        .setUser(student)
                        .setResult(null))
                    .addLtiLaunch(r2Launch2
                        .setUser(student)
                        .setResult(null)));
            assignment
                .getMilestone()
                .getJourney()
                .addTeacher(getTeacher(entityManager));
            persistCreatedEntities(entityManager);
            r1Launch2Score.setTimestamp(r1Launch2.getCreatedDate());
            r2Launch1Score.setTimestamp(r2Launch1.getCreatedDate().plusNanos(1000));
        });

        mockMvc.perform(get("/api/lti-scores")
            .param("assignmentId", assignment.getId().toString())
            .param("userId", STUDENT_ID.toString()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON))
            .andExpect(jsonPath("length()").value(3))

            .andExpect(jsonPath("$.[0].title").value(r2Title))
            .andExpect(jsonPath("$.[0].userId").value(STUDENT_ID))
            .andExpect(jsonPath("$.[0].score").isEmpty())
            .andExpect(jsonPath("$.[0].scoreMax").isEmpty())
            .andExpect(jsonPath("$.[0].createdDate").value(r2Launch2.getCreatedDate().toString()))
            .andExpect(jsonPath("$.[0].launches.length()").value(2))
            .andExpect(jsonPath("$.[0].launches.[*]").value(hasItem(r2Launch1.getCreatedDate().toString())))
            .andExpect(jsonPath("$.[0].launches.[*]").value(hasItem(r2Launch2.getCreatedDate().toString())))

            .andExpect(jsonPath("$.[1].title").value(r2Title))
            .andExpect(jsonPath("$.[1].userId").value(STUDENT_ID))
            .andExpect(jsonPath("$.[1].score").value(LTI_SCORE_SCORE_GIVEN))
            .andExpect(jsonPath("$.[1].scoreMax").value(LTI_SCORE_SCORE_MAXIMUM))
            .andExpect(jsonPath("$.[1].createdDate").value(r2Launch1Score.getTimestamp().toString()))
            .andExpect(jsonPath("$.[1].launches.length()").value(1))
            .andExpect(jsonPath("$.[1].launches.[0]").value(r2Launch1.getCreatedDate().toString()))

            .andExpect(jsonPath("$.[2].title").value(r1Title))
            .andExpect(jsonPath("$.[2].userId").value(STUDENT_ID))
            .andExpect(jsonPath("$.[2].score").value(LTI_SCORE_SCORE_GIVEN))
            .andExpect(jsonPath("$.[2].scoreMax").value(LTI_SCORE_SCORE_MAXIMUM))
            .andExpect(jsonPath("$.[2].createdDate").value(r1Launch2Score.getTimestamp().toString()))
            .andExpect(jsonPath("$.[2].launches.length()").value(2))
            .andExpect(jsonPath("$.[2].launches.[*]").value(hasItem(r1Launch1.getCreatedDate().toString())))
            .andExpect(jsonPath("$.[2].launches.[*]").value(hasItem(r1Launch2.getCreatedDate().toString())));
    }

}
