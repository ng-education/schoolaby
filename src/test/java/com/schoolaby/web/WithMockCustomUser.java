package com.schoolaby.web;

import org.springframework.security.test.context.support.WithSecurityContext;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Inherited
@Retention(RetentionPolicy.RUNTIME)
@WithSecurityContext(factory = WithMockCustomUserSecurityContextFactory.class)
public @interface WithMockCustomUser {
    String value() default "test";

    String password() default "password";

    String[] authorities() default {};

    String userId() default "2";
}
