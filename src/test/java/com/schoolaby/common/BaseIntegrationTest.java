package com.schoolaby.common;

import com.schoolaby.SchoolabyApp;
import com.schoolaby.web.WithMockCustomUser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.CacheManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import javax.persistence.EntityManager;
import java.util.Objects;

@SpringBootTest(classes = SchoolabyApp.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
@Sql("/init-test-data.sql")
@WithMockCustomUser
@ActiveProfiles("testcontainers")
public abstract class BaseIntegrationTest {
    public static final String ADMIN_LOGIN = "admin";
    public static final String TEACHER_LOGIN = "teacher";
    public static final String STUDENT_LOGIN = "student";
    public static final Long ADMIN_ID = -1L;
    public static final Long TEACHER_ID = -2L;
    public static final Long STUDENT_ID = -3L;
    public static final Long SUBJECT_ID = 1L;

    @Autowired
    protected EntityManager entityManager;
    @Autowired
    protected TransactionHelper transactionHelper;
    @Autowired
    protected MockMvc mockMvc;
    @Autowired
    protected CacheManager cacheManager;

    @BeforeEach
    void baseSetup() {
        evictAllCaches();
        TestObjects.clear();
    }

    public void evictAllCaches() {
        for (String name : cacheManager.getCacheNames()) {
            Objects.requireNonNull(cacheManager.getCache(name)).invalidate();
        }
    }
}
