package com.schoolaby.common;

import com.schoolaby.domain.Authority;
import com.schoolaby.domain.PersonRole;
import com.schoolaby.domain.School;
import com.schoolaby.domain.User;
import com.schoolaby.service.dto.PersonRoleDTO;
import com.schoolaby.service.dto.SchoolDTO;
import com.schoolaby.service.dto.UserDTO;
import com.schoolaby.service.dto.UserDetailedDTO;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.OAuth2AccessToken;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static java.util.stream.Collectors.toSet;

public class TestUtils {
    public static final String EXTERNAL_ID = "11111111";
    public static final String EKOOL_REGISTRATION_ID = "ekool";

    public static UserDetailedDTO mapUserToDetailedDTO(User user) {
        return new UserDetailedDTO()
            .setId(user.getId())
            .setLogin(user.getLogin())
            .setFirstName(user.getFirstName())
            .setLastName(user.getLastName())
            .setEmail(user.getEmail())
            .setPhoneNumber(user.getPhoneNumber())
            .setPersonalCode(user.getPersonalCode())
            .setActivated(user.isActivated())
            .setImageUrl(user.getImageUrl())
            .setLangKey(user.getLangKey())
            .setCountry(user.getCountry())
            .setPersonRoles(user
                .getPersonRoles()
                .stream()
                .map(TestUtils::mapPersonRoleToDTO)
                .collect(toSet()))
            .setAuthorities(user
                .getAuthorities()
                .stream()
                .map(Authority::getName)
                .collect(toSet()))
            .setExternalAuthentications(user
                .getExternalAuthentications()
                .stream()
                .map(auth ->
                    auth.getType()
                        .toString()
                        .toLowerCase())
                .collect(toSet()))
            .setTermsAgreed(user.isTermsAgreed());
    }

    public static UserDTO mapUserToDTO(User user) {
        return new UserDTO()
            .setId(user.getId())
            .setFirstName(user.getFirstName())
            .setLastName(user.getLastName());
    }

    public static PersonRoleDTO mapPersonRoleToDTO(PersonRole personRole) {
        return new PersonRoleDTO()
            .setId(personRole.getId())
            .setRole(personRole.getRole())
            .setActive(personRole.isActive())
            .setGrade(personRole.getGrade())
            .setSchool(mapSchoolToDTO(personRole.getSchool()));
    }

    public static SchoolDTO mapSchoolToDTO(School school) {
        return new SchoolDTO()
            .setId(school.getId())
            .setEhisId(school.getEhisId())
            .setRegNr(school.getRegNr())
            .setName(school.getName());
    }

    public static OAuth2AuthorizedClient getEkoolOauth2AuthorizedClient() {
        OAuth2AccessToken oAuth2AccessToken = new OAuth2AccessToken(
            OAuth2AccessToken.TokenType.BEARER,
            "123123123123123123123123",
            Instant.now(),
            Instant.now().plus(1, ChronoUnit.HOURS)
        );
        ClientRegistration clientRegistration = ClientRegistration
            .withRegistrationId(EKOOL_REGISTRATION_ID)
            .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
            .clientId(EKOOL_REGISTRATION_ID)
            .redirectUriTemplate("https://redirect")
            .authorizationUri("https://authorization")
            .tokenUri("https://token")
            .build();
        return new OAuth2AuthorizedClient(clientRegistration, EXTERNAL_ID, oAuth2AccessToken);
    }
}
