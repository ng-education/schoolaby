package com.schoolaby.common;

import com.schoolaby.domain.*;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import static com.schoolaby.common.BaseIntegrationTest.*;
import static com.schoolaby.security.Role.Constants.STUDENT;
import static com.schoolaby.security.Role.Constants.TEACHER;

public class TestCases {

    public static final String STUDENT_LOGIN = "testloginstudent";
    public static final String TEACHER_LOGIN = "testloginteacher";
    public static final String STUDENT_EMAIL = "student@netgroup.com";
    public static final String TEACHER_EMAIL = "teacher@netgroup.com";
    public static final String ESTONIAN_CURRICULUM = "ESTONIAN_CURRICULUM";
    public static final Long ESTONIAN_CURRICULUM_ID = 2L;

    private TestCases() {
    }

    public static User createStudent() {
        User user = TestObjects.createUser();

        user.setLogin(STUDENT_LOGIN);
        user.setEmail(STUDENT_EMAIL);
        user.addAuthority(new Authority().name(STUDENT));

        return user;
    }

    public static User createTeacher() {
        User user = TestObjects.createUser();

        user.setLogin(TEACHER_LOGIN);
        user.setEmail(TEACHER_EMAIL);
        user.addAuthority(new Authority().name(TEACHER));
        return user;
    }

    public static User getAdmin(EntityManager em) {
        return em.find(User.class, ADMIN_ID);
    }

    public static User getTeacher(EntityManager em) {
        return em.find(User.class, TEACHER_ID);
    }

    public static User getStudent(EntityManager em) {
        return em.find(User.class, STUDENT_ID);
    }

    public static Subject getSubject(EntityManager em) {
        return em.find(Subject.class, SUBJECT_ID);
    }

    public static Subject getSubjectById(EntityManager em, Long subjectId) {
        return em.find(Subject.class, subjectId);
    }

    public static GradingScheme getGradingSchemeNumerical(EntityManager em) {
        CriteriaBuilder builder = em.getCriteriaBuilder();

        CriteriaQuery<GradingScheme> criteria = builder.createQuery(GradingScheme.class);
        Root<GradingScheme> root = criteria.from(GradingScheme.class);
        criteria.select(root)
            .where(builder.equal(root.get(GradingScheme_.code), "NUMERICAL_1_5"));

        return em.createQuery(criteria).getSingleResult();
    }

    public static GradingScheme getGradingSchemeNone(EntityManager em) {
        CriteriaBuilder builder = em.getCriteriaBuilder();

        CriteriaQuery<GradingScheme> criteria = builder.createQuery(GradingScheme.class);
        Root<GradingScheme> root = criteria.from(GradingScheme.class);
        criteria.select(root)
            .where(builder.equal(root.get(GradingScheme_.code), GradingScheme.NONE));

        return em.createQuery(criteria).getSingleResult();
    }

    public static GradingScheme getGradingSchemeAlphabetical(EntityManager em) {
        CriteriaBuilder builder = em.getCriteriaBuilder();

        CriteriaQuery<GradingScheme> criteria = builder.createQuery(GradingScheme.class);
        Root<GradingScheme> root = criteria.from(GradingScheme.class);
        criteria.select(root)
            .where(builder.equal(root.get(GradingScheme_.code), "ALPHABETICAL_A_F"));

        return em.createQuery(criteria).getSingleResult();
    }

    public static Curriculum getCurriculumById(EntityManager em, Long id) {
        return em.find(Curriculum.class, id);
    }
}
