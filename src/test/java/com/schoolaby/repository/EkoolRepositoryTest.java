package com.schoolaby.repository;

import com.schoolaby.config.EkoolProperties;
import com.schoolaby.service.dto.EkoolPersonalDatasDTO;
import com.schoolaby.service.dto.EkoolRolesDTO;
import com.schoolaby.service.dto.EkoolSchoolsDTO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.schoolaby.common.TestObjects.*;
import static com.schoolaby.common.TestUtils.getEkoolOauth2AuthorizedClient;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@ExtendWith(MockitoExtension.class)
class EkoolRepositoryTest {
    private final static String USER_SCHOOLS_URI = "https://schools";
    private final static String USER_ROLES_URI = "https://roles";
    private final static String USER_BASIC_URI = "https://basic";

    private final OAuth2AccessToken oAuth2AccessToken = getEkoolOauth2AuthorizedClient().getAccessToken();

    @Mock
    private EkoolProperties ekoolProperties;
    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private EkoolRepository ekoolRepository;

    @Test
    void shouldGetUserSchools() {
        EkoolSchoolsDTO ekoolSchoolsDTO = new EkoolSchoolsDTO()
            .setData(Set.of(
                new EkoolSchoolsDTO.EkoolSchoolDTO()
                    .setName(SCHOOL_NAME)
            ));
        doReturn(USER_SCHOOLS_URI).when(ekoolProperties).getUserSchoolsUri();
        mockEndpoint(USER_SCHOOLS_URI, ekoolSchoolsDTO);

        List<EkoolSchoolsDTO.EkoolSchoolDTO> schools = new ArrayList<>(ekoolRepository.getUserSchools(oAuth2AccessToken).getData());

        assertEquals(SCHOOL_NAME, schools.get(0).getName());
    }

    @Test
    void shouldGetUserRoles() {
        EkoolRolesDTO ekoolRolesDTO = new EkoolRolesDTO()
            .setData(Set.of(
                new EkoolRolesDTO.EkoolRoleDTO()
                    .setRoleName(ROLE_NAME)
            ));
        doReturn(USER_ROLES_URI).when(ekoolProperties).getUserRolesUri();
        mockEndpoint(USER_ROLES_URI, ekoolRolesDTO);

        List<EkoolRolesDTO.EkoolRoleDTO> roles = new ArrayList<>(ekoolRepository.getUserRoles(oAuth2AccessToken).getData());

        assertEquals(ROLE_NAME, roles.get(0).getRoleName());
    }

    @Test
    void shouldGetUserBasic() {
        EkoolPersonalDatasDTO ekoolPersonalDatasDTO = new EkoolPersonalDatasDTO()
            .setData(new EkoolPersonalDatasDTO.EkoolPersonalDataDTO().setEmail(USER_EMAIL));
        doReturn(USER_BASIC_URI).when(ekoolProperties).getUserBasicUri();
        mockEndpoint(USER_BASIC_URI, ekoolPersonalDatasDTO);

        EkoolPersonalDatasDTO.EkoolPersonalDataDTO ekoolPersonalDataDTO = ekoolRepository.getUserBasic(oAuth2AccessToken).getData();

        assertEquals(USER_EMAIL, ekoolPersonalDataDTO.getEmail());
    }

    private <T> void mockEndpoint(String uri, T response) {
        doReturn(ResponseEntity.of(Optional.of(response))).when(restTemplate).exchange(
            eq(uri),
            eq(HttpMethod.GET),
            argThat(argument -> argument
                .getHeaders()
                .get(AUTHORIZATION)
                .get(0)
                .equals("Bearer " + oAuth2AccessToken.getTokenValue())
            ),
            eq(response.getClass())
        );
    }
}
