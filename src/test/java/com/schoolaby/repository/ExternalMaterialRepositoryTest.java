package com.schoolaby.repository;

import com.schoolaby.config.external_material.EkoolikottProperties;
import com.schoolaby.service.dto.MaterialDTO;
import com.squareup.okhttp.mockwebserver.MockResponse;
import com.squareup.okhttp.mockwebserver.MockWebServer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.ServletWebRequest;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;

import static com.schoolaby.common.FileUtil.readFile;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;


@ExtendWith(MockitoExtension.class)
class ExternalMaterialRepositoryTest {

    @Mock
    private EkoolikottProperties ekoolikottProperties;

    @InjectMocks
    private ExternalMaterialRepository repository;

    private static MockWebServer mockBackEnd;

    @BeforeAll
    static void setUp() throws IOException {
        mockBackEnd = new MockWebServer();
        mockBackEnd.start();
    }

    @Test
    void shouldGetEkoolikottMaterialsWithTaxon() {
        doReturn("localhost").when(ekoolikottProperties).getHost();
        doReturn(mockBackEnd.getPort()).when(ekoolikottProperties).getPort();
        doReturn("http").when(ekoolikottProperties).getScheme();
        mockBackEnd.enqueue(new MockResponse()
            .setBody(readFile("reducedMaterial.json", this.getClass()))
            .addHeader("Content-Type", "application/json"));
        NativeWebRequest webRequest = new ServletWebRequest(new MockHttpServletRequest());

        List<MaterialDTO> materials = repository.getEkoolikottMaterials(webRequest, "Matemaatika", 1, "1009");

        MaterialDTO materialDTO = materials.get(0);
        assertEquals(1, materials.size());
        assertEquals("5691", materialDTO.getExternalId());
        assertEquals(".EkoolikottMaterial", materialDTO.getType());
        assertEquals("Matemaatika valemid", materialDTO.getTitle());
    }

    @Test
    void shouldGetEkoolikottMaterialsWithMultipleTaxons() {
        doReturn("localhost").when(ekoolikottProperties).getHost();
        doReturn(mockBackEnd.getPort()).when(ekoolikottProperties).getPort();
        doReturn("http").when(ekoolikottProperties).getScheme();
        mockBackEnd.enqueue(new MockResponse()
            .setBody(readFile("reducedMaterial.json", this.getClass()))
            .addHeader("Content-Type", "application/json"));
        mockBackEnd.enqueue(new MockResponse()
            .setBody(readFile("reducedMaterial2.json", this.getClass()))
            .addHeader("Content-Type", "application/json"));
        NativeWebRequest webRequest = new ServletWebRequest(new MockHttpServletRequest());

        List<MaterialDTO> materials = repository.getEkoolikottMaterials(webRequest, "Matemaatika", 1, "1009,1010");

        assertEquals(2, materials.size());

        materials.sort(Comparator.comparing(MaterialDTO::getExternalId));

        MaterialDTO materialDTO = materials.get(0);
        assertEquals("5691", materialDTO.getExternalId());
        assertEquals(".EkoolikottMaterial", materialDTO.getType());
        assertEquals("Matemaatika valemid", materialDTO.getTitle());
        materialDTO = materials.get(1);
        assertEquals("5692", materialDTO.getExternalId());
        assertEquals(".EkoolikottMaterial", materialDTO.getType());
        assertEquals("Matemaatika valemid 2", materialDTO.getTitle());
    }

    @Test
    void shouldGetEkoolikottMaterialsWithoutTaxon() {
        doReturn("localhost").when(ekoolikottProperties).getHost();
        doReturn(mockBackEnd.getPort()).when(ekoolikottProperties).getPort();
        doReturn("http").when(ekoolikottProperties).getScheme();
        mockBackEnd.enqueue(new MockResponse()
            .setBody(readFile("reducedMaterial.json", this.getClass()))
            .addHeader("Content-Type", "application/json"));
        NativeWebRequest webRequest = new ServletWebRequest(new MockHttpServletRequest());

        List<MaterialDTO> materials = repository.getEkoolikottMaterials(webRequest, "Matemaatika", 1, null);

        assertEquals(1, materials.size());

        MaterialDTO materialDTO = materials.get(0);
        assertEquals("5691", materialDTO.getExternalId());
        assertEquals(".EkoolikottMaterial", materialDTO.getType());
        assertEquals("Matemaatika valemid", materialDTO.getTitle());
    }

    @AfterAll
    static void tearDown() throws IOException {
        mockBackEnd.shutdown();
    }
}
